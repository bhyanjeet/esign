-- MySQL dump 10.13  Distrib 8.0.18, for Linux (x86_64)
--
-- Host: localhost    Database: esignharyana
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `application_master`
--

DROP TABLE IF EXISTS `application_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `application_master` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `application_name` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `last_updated_by` varchar(255) DEFAULT NULL,
  `last_updated_on` date DEFAULT NULL,
  `verified_by` varchar(255) DEFAULT NULL,
  `verified_on` date DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `application_url` varchar(255) DEFAULT NULL,
  `application_status` varchar(255) DEFAULT NULL,
  `application_id_code` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `organisation_master_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_application_master_application_name` (`application_name`),
  UNIQUE KEY `ux_application_master_application_id_code` (`application_id_code`),
  KEY `fk_application_master_organisation_master_id` (`organisation_master_id`),
  CONSTRAINT `fk_application_master_organisation_master_id` FOREIGN KEY (`organisation_master_id`) REFERENCES `organisation_master` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application_master`
--

LOCK TABLES `application_master` WRITE;
/*!40000 ALTER TABLE `application_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `application_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `block_master`
--

DROP TABLE IF EXISTS `block_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `block_master` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `block_code` varchar(255) DEFAULT NULL,
  `block_name` varchar(255) NOT NULL,
  `created_on` date DEFAULT NULL,
  `last_updated_by` varchar(255) DEFAULT NULL,
  `last_updated_on` date DEFAULT NULL,
  `verified_by` varchar(255) DEFAULT NULL,
  `verified_on` date DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `state_master_id` bigint(20) DEFAULT NULL,
  `district_master_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_block_master_state_master_id` (`state_master_id`),
  KEY `fk_block_master_district_master_id` (`district_master_id`),
  CONSTRAINT `fk_block_master_district_master_id` FOREIGN KEY (`district_master_id`) REFERENCES `district_master` (`id`),
  CONSTRAINT `fk_block_master_state_master_id` FOREIGN KEY (`state_master_id`) REFERENCES `state_master` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block_master`
--

LOCK TABLES `block_master` WRITE;
/*!40000 ALTER TABLE `block_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `block_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `databasechangelog`
--

DROP TABLE IF EXISTS `databasechangelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `databasechangelog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databasechangelog`
--

LOCK TABLES `databasechangelog` WRITE;
/*!40000 ALTER TABLE `databasechangelog` DISABLE KEYS */;
INSERT INTO `databasechangelog` VALUES ('00000000000001','jhipster','config/liquibase/changelog/00000000000000_initial_schema.xml','2019-12-06 10:02:21',1,'EXECUTED','8:21653d8a08d40a79e49fcc34981d6dcf','createTable tableName=jhi_user; createTable tableName=jhi_authority; createTable tableName=jhi_user_authority; addPrimaryKey tableName=jhi_user_authority; addForeignKeyConstraint baseTableName=jhi_user_authority, constraintName=fk_authority_name, ...','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191120121428-1','jhipster','config/liquibase/changelog/20191120121428_added_entity_StateMaster.xml','2019-12-06 10:02:22',2,'EXECUTED','8:1eed57552dc52238b0d2764aa8c52c08','createTable tableName=state_master','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191120121428-1-relations','jhipster','config/liquibase/changelog/20191120121428_added_entity_StateMaster.xml','2019-12-06 10:02:22',3,'EXECUTED','8:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121050623-1','jhipster','config/liquibase/changelog/20191121050623_added_entity_DistrictMaster.xml','2019-12-06 10:02:22',4,'EXECUTED','8:b6ffb1959f444fb79b84d1525071418b','createTable tableName=district_master','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121050623-1-relations','jhipster','config/liquibase/changelog/20191121050623_added_entity_DistrictMaster.xml','2019-12-06 10:02:23',5,'EXECUTED','8:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121054521-1','jhipster','config/liquibase/changelog/20191121054521_added_entity_SubDistrictMaster.xml','2019-12-06 10:02:23',6,'EXECUTED','8:3279084dca23d012d3012a71fbe58311','createTable tableName=sub_district_master','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121054521-1-relations','jhipster','config/liquibase/changelog/20191121054521_added_entity_SubDistrictMaster.xml','2019-12-06 10:02:23',7,'EXECUTED','8:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121055606-1','jhipster','config/liquibase/changelog/20191121055606_added_entity_BlockMaster.xml','2019-12-06 10:02:24',8,'EXECUTED','8:70bc9661e9db5eda0d745d2373ff15b0','createTable tableName=block_master','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121055606-1-relations','jhipster','config/liquibase/changelog/20191121055606_added_entity_BlockMaster.xml','2019-12-06 10:02:24',9,'EXECUTED','8:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121061000-1','jhipster','config/liquibase/changelog/20191121061000_added_entity_OrganisationTypeMaster.xml','2019-12-06 10:02:24',10,'EXECUTED','8:28b5308708eaae22c8155cf5ccae1290','createTable tableName=organisation_type_master','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121061000-1-relations','jhipster','config/liquibase/changelog/20191121061000_added_entity_OrganisationTypeMaster.xml','2019-12-06 10:02:25',11,'EXECUTED','8:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121061912-1','jhipster','config/liquibase/changelog/20191121061912_added_entity_DesignationMaster.xml','2019-12-06 10:02:25',12,'EXECUTED','8:eb2fc7a4bd372b36924d6c2cf94be8c1','createTable tableName=designation_master','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121061912-1-relations','jhipster','config/liquibase/changelog/20191121061912_added_entity_DesignationMaster.xml','2019-12-06 10:02:25',13,'EXECUTED','8:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121062809-1','jhipster','config/liquibase/changelog/20191121062809_added_entity_EsignRole.xml','2019-12-06 10:02:26',14,'EXECUTED','8:56212171fce87aec7a6422e5687cb09c','createTable tableName=esign_role','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121062809-1-relations','jhipster','config/liquibase/changelog/20191121062809_added_entity_EsignRole.xml','2019-12-06 10:02:26',15,'EXECUTED','8:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121070116-1','jhipster','config/liquibase/changelog/20191121070116_added_entity_OrganisationMaster.xml','2019-12-06 10:02:26',16,'EXECUTED','8:7ab0a8ed632b51a1daafd14949d47200','createTable tableName=organisation_master','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121070116-1-relations','jhipster','config/liquibase/changelog/20191121070116_added_entity_OrganisationMaster.xml','2019-12-06 10:02:27',17,'EXECUTED','8:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121074700-1','jhipster','config/liquibase/changelog/20191121074700_added_entity_UserDetails.xml','2019-12-06 10:02:27',18,'EXECUTED','8:8bd373473d48e179b4c9246b7662f621','createTable tableName=user_details','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121074700-1-relations','jhipster','config/liquibase/changelog/20191121074700_added_entity_UserDetails.xml','2019-12-06 10:02:27',19,'EXECUTED','8:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191122061603-1','jhipster','config/liquibase/changelog/20191122061603_added_entity_OrganisationDocument.xml','2019-12-06 10:02:28',20,'EXECUTED','8:9213f0ac1a23367d6d67b95f7f129e6d','createTable tableName=organisation_document','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191122061603-1-relations','jhipster','config/liquibase/changelog/20191122061603_added_entity_OrganisationDocument.xml','2019-12-06 10:02:28',21,'EXECUTED','8:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191122090913-1','jhipster','config/liquibase/changelog/20191122090913_added_entity_ApplicationMaster.xml','2019-12-06 10:02:29',22,'EXECUTED','8:7c0433678f869030325cbd6f2cb48979','createTable tableName=application_master','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191122090913-1-relations','jhipster','config/liquibase/changelog/20191122090913_added_entity_ApplicationMaster.xml','2019-12-06 10:02:29',23,'EXECUTED','8:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191122100829-1','jhipster','config/liquibase/changelog/20191122100829_added_entity_Otp.xml','2019-12-06 10:02:30',24,'EXECUTED','8:b346fe54914d44c54b1a6d1f910990fc','createTable tableName=otp; dropDefaultValue columnName=request_time, tableName=otp','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191122100829-1-relations','jhipster','config/liquibase/changelog/20191122100829_added_entity_Otp.xml','2019-12-06 10:02:30',25,'EXECUTED','8:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191126074721-1','jhipster','config/liquibase/changelog/20191126074721_added_entity_OrganisationLog.xml','2019-12-06 10:02:31',26,'EXECUTED','8:eb291e50fff513de938b19b4d0ee74d0','createTable tableName=organisation_log; dropDefaultValue columnName=created_date, tableName=organisation_log; dropDefaultValue columnName=verify_date, tableName=organisation_log','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191126074721-1-relations','jhipster','config/liquibase/changelog/20191126074721_added_entity_OrganisationLog.xml','2019-12-06 10:02:31',27,'EXECUTED','8:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191125061605-1','jhipster','config/liquibase/changelog/20191125061605_added_entity_OrganisationAttachment.xml','2019-12-06 10:02:31',28,'EXECUTED','8:4a7438a9f9e8ff695f6b07dc8535315c','createTable tableName=organisation_attachment','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191125061605-1-relations','jhipster','config/liquibase/changelog/20191125061605_added_entity_OrganisationAttachment.xml','2019-12-06 10:02:32',29,'EXECUTED','8:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121050623-2','jhipster','config/liquibase/changelog/20191121050623_added_entity_constraints_DistrictMaster.xml','2019-12-06 10:02:33',30,'EXECUTED','8:e144c1660a25065bed72fdaec1c061dc','addForeignKeyConstraint baseTableName=district_master, constraintName=fk_district_master_state_master_id, referencedTableName=state_master','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121054521-2','jhipster','config/liquibase/changelog/20191121054521_added_entity_constraints_SubDistrictMaster.xml','2019-12-06 10:02:36',31,'EXECUTED','8:b19e13c86ebc304c8ae87fb607324ba3','addForeignKeyConstraint baseTableName=sub_district_master, constraintName=fk_sub_district_master_state_master_id, referencedTableName=state_master; addForeignKeyConstraint baseTableName=sub_district_master, constraintName=fk_sub_district_master_di...','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121055606-2','jhipster','config/liquibase/changelog/20191121055606_added_entity_constraints_BlockMaster.xml','2019-12-06 10:02:39',32,'EXECUTED','8:207916b689d25eb982d0eeb1a3cc3c07','addForeignKeyConstraint baseTableName=block_master, constraintName=fk_block_master_state_master_id, referencedTableName=state_master; addForeignKeyConstraint baseTableName=block_master, constraintName=fk_block_master_district_master_id, referenced...','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121070116-2','jhipster','config/liquibase/changelog/20191121070116_added_entity_constraints_OrganisationMaster.xml','2019-12-06 10:02:47',33,'EXECUTED','8:1d617999798a3afc47541f00d70e9c27','addForeignKeyConstraint baseTableName=organisation_master, constraintName=fk_organisation_master_organisation_type_master_id, referencedTableName=organisation_type_master; addForeignKeyConstraint baseTableName=organisation_master, constraintName=f...','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191121074700-2','jhipster','config/liquibase/changelog/20191121074700_added_entity_constraints_UserDetails.xml','2019-12-06 10:02:55',34,'EXECUTED','8:c34389bed09e71f451b4b65337e2578e','addForeignKeyConstraint baseTableName=user_details, constraintName=fk_user_details_designation_master_id, referencedTableName=designation_master; addForeignKeyConstraint baseTableName=user_details, constraintName=fk_user_details_organisation_maste...','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191122061603-2','jhipster','config/liquibase/changelog/20191122061603_added_entity_constraints_OrganisationDocument.xml','2019-12-06 10:02:56',35,'EXECUTED','8:0886d649f46a1b15b48ef08115310911','addForeignKeyConstraint baseTableName=organisation_document, constraintName=fk_organisation_document_organisation_type_master_id, referencedTableName=organisation_type_master','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191122090913-2','jhipster','config/liquibase/changelog/20191122090913_added_entity_constraints_ApplicationMaster.xml','2019-12-06 10:02:58',36,'EXECUTED','8:35f79468f889b6b4488f4890a16c163f','addForeignKeyConstraint baseTableName=application_master, constraintName=fk_application_master_organisation_master_id, referencedTableName=organisation_master','',NULL,'3.6.3',NULL,NULL,'5626524728'),('20191125061605-2','jhipster','config/liquibase/changelog/20191125061605_added_entity_constraints_OrganisationAttachment.xml','2019-12-06 10:03:02',37,'EXECUTED','8:1da30961c69afcd29949974649450a69','addForeignKeyConstraint baseTableName=organisation_attachment, constraintName=fk_organisation_attachment_organisation_master_id, referencedTableName=organisation_master; addForeignKeyConstraint baseTableName=organisation_attachment, constraintName...','',NULL,'3.6.3',NULL,NULL,'5626524728');
/*!40000 ALTER TABLE `databasechangelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `databasechangeloglock`
--

DROP TABLE IF EXISTS `databasechangeloglock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `databasechangeloglock` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databasechangeloglock`
--

LOCK TABLES `databasechangeloglock` WRITE;
/*!40000 ALTER TABLE `databasechangeloglock` DISABLE KEYS */;
INSERT INTO `databasechangeloglock` VALUES (1,_binary '\0',NULL,NULL);
/*!40000 ALTER TABLE `databasechangeloglock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `designation_master`
--

DROP TABLE IF EXISTS `designation_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `designation_master` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `designation_name` varchar(255) DEFAULT NULL,
  `designation_abbreviation` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `last_updated_by` varchar(255) DEFAULT NULL,
  `last_updated_on` date DEFAULT NULL,
  `verified_by` varchar(255) DEFAULT NULL,
  `verified_on` date DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `designation_master`
--

LOCK TABLES `designation_master` WRITE;
/*!40000 ALTER TABLE `designation_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `designation_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `district_master`
--

DROP TABLE IF EXISTS `district_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `district_master` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `district_name` varchar(255) DEFAULT NULL,
  `district_code` varchar(255) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `last_updated_by` varchar(255) DEFAULT NULL,
  `last_updated_on` date DEFAULT NULL,
  `verified_by` varchar(255) DEFAULT NULL,
  `verified_on` date DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `state_master_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_district_master_state_master_id` (`state_master_id`),
  CONSTRAINT `fk_district_master_state_master_id` FOREIGN KEY (`state_master_id`) REFERENCES `state_master` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `district_master`
--

LOCK TABLES `district_master` WRITE;
/*!40000 ALTER TABLE `district_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `district_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `esign_role`
--

DROP TABLE IF EXISTS `esign_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `esign_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `e_sign_role_detail` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `last_updated_by` varchar(255) DEFAULT NULL,
  `last_updated_on` date DEFAULT NULL,
  `verified_by` varchar(255) DEFAULT NULL,
  `verified_on` date DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `esign_role`
--

LOCK TABLES `esign_role` WRITE;
/*!40000 ALTER TABLE `esign_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `esign_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_authority`
--

DROP TABLE IF EXISTS `jhi_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_authority` (
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_authority`
--

LOCK TABLES `jhi_authority` WRITE;
/*!40000 ALTER TABLE `jhi_authority` DISABLE KEYS */;
INSERT INTO `jhi_authority` VALUES ('ROLE_ADMIN'),('ROLE_ASP_NODAL'),('ROLE_ORG_NODAL'),('ROLE_ORG_USER'),('ROLE_USER');
/*!40000 ALTER TABLE `jhi_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_persistent_audit_event`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_persistent_audit_event` (
  `event_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `principal` varchar(50) NOT NULL,
  `event_date` timestamp NULL DEFAULT NULL,
  `event_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`),
  KEY `idx_persistent_audit_event` (`principal`,`event_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_audit_event`
--

LOCK TABLES `jhi_persistent_audit_event` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_audit_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `jhi_persistent_audit_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_persistent_audit_evt_data`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_evt_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_persistent_audit_evt_data` (
  `event_id` bigint(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`,`name`),
  KEY `idx_persistent_audit_evt_data` (`event_id`),
  CONSTRAINT `fk_evt_pers_audit_evt_data` FOREIGN KEY (`event_id`) REFERENCES `jhi_persistent_audit_event` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_audit_evt_data`
--

LOCK TABLES `jhi_persistent_audit_evt_data` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_audit_evt_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `jhi_persistent_audit_evt_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_user`
--

DROP TABLE IF EXISTS `jhi_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `image_url` varchar(256) DEFAULT NULL,
  `activated` bit(1) NOT NULL,
  `lang_key` varchar(10) DEFAULT NULL,
  `activation_key` varchar(20) DEFAULT NULL,
  `reset_key` varchar(20) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` timestamp NULL,
  `reset_date` timestamp NULL DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_user_login` (`login`),
  UNIQUE KEY `ux_user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user`
--

LOCK TABLES `jhi_user` WRITE;
/*!40000 ALTER TABLE `jhi_user` DISABLE KEYS */;
INSERT INTO `jhi_user` VALUES (1,'system','$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG','System','System','system@localhost','',_binary '','en',NULL,NULL,'system',NULL,NULL,'system',NULL),(2,'anonymoususer','$2a$10$j8S5d7Sr7.8VTOYNviDPOeWX8KcYILUVJBsYV83Y5NtECayypx9lO','Anonymous','User','anonymous@localhost','',_binary '','en',NULL,NULL,'system',NULL,NULL,'system',NULL),(3,'admin','$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC','Administrator','Administrator','admin@localhost','',_binary '','en',NULL,NULL,'system',NULL,NULL,'system',NULL),(4,'user','$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K','User','User','user@localhost','',_binary '','en',NULL,NULL,'system',NULL,NULL,'system',NULL);
/*!40000 ALTER TABLE `jhi_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_user_authority`
--

DROP TABLE IF EXISTS `jhi_user_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_user_authority` (
  `user_id` bigint(20) NOT NULL,
  `authority_name` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`,`authority_name`),
  KEY `fk_authority_name` (`authority_name`),
  CONSTRAINT `fk_authority_name` FOREIGN KEY (`authority_name`) REFERENCES `jhi_authority` (`name`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user_authority`
--

LOCK TABLES `jhi_user_authority` WRITE;
/*!40000 ALTER TABLE `jhi_user_authority` DISABLE KEYS */;
INSERT INTO `jhi_user_authority` VALUES (1,'ROLE_ADMIN'),(3,'ROLE_ADMIN'),(1,'ROLE_USER'),(3,'ROLE_USER'),(4,'ROLE_USER');
/*!40000 ALTER TABLE `jhi_user_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organisation_attachment`
--

DROP TABLE IF EXISTS `organisation_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organisation_attachment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attachment` varchar(255) DEFAULT NULL,
  `organisation_master_id` bigint(20) DEFAULT NULL,
  `organisation_document_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_organisation_attachment_organisation_master_id` (`organisation_master_id`),
  KEY `fk_organisation_attachment_organisation_document_id` (`organisation_document_id`),
  CONSTRAINT `fk_organisation_attachment_organisation_document_id` FOREIGN KEY (`organisation_document_id`) REFERENCES `organisation_document` (`id`),
  CONSTRAINT `fk_organisation_attachment_organisation_master_id` FOREIGN KEY (`organisation_master_id`) REFERENCES `organisation_master` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisation_attachment`
--

LOCK TABLES `organisation_attachment` WRITE;
/*!40000 ALTER TABLE `organisation_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `organisation_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organisation_document`
--

DROP TABLE IF EXISTS `organisation_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organisation_document` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `document_title` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `verified_by` varchar(255) DEFAULT NULL,
  `verified_on` date DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `document_related_to` varchar(255) DEFAULT NULL,
  `organisation_type_master_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_organisation_document_organisation_type_master_id` (`organisation_type_master_id`),
  CONSTRAINT `fk_organisation_document_organisation_type_master_id` FOREIGN KEY (`organisation_type_master_id`) REFERENCES `organisation_type_master` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisation_document`
--

LOCK TABLES `organisation_document` WRITE;
/*!40000 ALTER TABLE `organisation_document` DISABLE KEYS */;
/*!40000 ALTER TABLE `organisation_document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organisation_log`
--

DROP TABLE IF EXISTS `organisation_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organisation_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `organisation_id` bigint(20) DEFAULT NULL,
  `created_by_login` varchar(255) DEFAULT NULL,
  `created_date` datetime,
  `verify_by_name` varchar(255) DEFAULT NULL,
  `verify_date` datetime,
  `remarks` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `verify_by_login` varchar(255) DEFAULT NULL,
  `created_by_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisation_log`
--

LOCK TABLES `organisation_log` WRITE;
/*!40000 ALTER TABLE `organisation_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `organisation_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organisation_master`
--

DROP TABLE IF EXISTS `organisation_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organisation_master` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `organisation_name` varchar(255) NOT NULL,
  `organisation_abbreviation` varchar(255) NOT NULL,
  `organisation_correspondence_email` varchar(255) NOT NULL,
  `organisation_correspondence_phone_1` varchar(255) NOT NULL,
  `organisation_correspondence_phone_2` varchar(255) DEFAULT NULL,
  `organisation_correspondence_address` varchar(255) DEFAULT NULL,
  `organisation_website` varchar(255) DEFAULT NULL,
  `organisation_nodal_officer_name` varchar(255) DEFAULT NULL,
  `organisation_nodal_officer_phone_mobile` varchar(255) DEFAULT NULL,
  `organisation_nodal_officer_phone_landline` varchar(255) DEFAULT NULL,
  `nodal_officer_official_email` varchar(255) DEFAULT NULL,
  `organisation_nodal_officer_alternative_email` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `last_updated_by` varchar(255) DEFAULT NULL,
  `last_updated_on` date DEFAULT NULL,
  `verified_by` varchar(255) DEFAULT NULL,
  `verified_on` date DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `stage` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `organisation_type_master_id` bigint(20) DEFAULT NULL,
  `state_master_id` bigint(20) DEFAULT NULL,
  `district_master_id` bigint(20) DEFAULT NULL,
  `sub_district_master_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_organisation_master_organisation_type_master_id` (`organisation_type_master_id`),
  KEY `fk_organisation_master_state_master_id` (`state_master_id`),
  KEY `fk_organisation_master_district_master_id` (`district_master_id`),
  KEY `fk_organisation_master_sub_district_master_id` (`sub_district_master_id`),
  CONSTRAINT `fk_organisation_master_district_master_id` FOREIGN KEY (`district_master_id`) REFERENCES `district_master` (`id`),
  CONSTRAINT `fk_organisation_master_organisation_type_master_id` FOREIGN KEY (`organisation_type_master_id`) REFERENCES `organisation_type_master` (`id`),
  CONSTRAINT `fk_organisation_master_state_master_id` FOREIGN KEY (`state_master_id`) REFERENCES `state_master` (`id`),
  CONSTRAINT `fk_organisation_master_sub_district_master_id` FOREIGN KEY (`sub_district_master_id`) REFERENCES `sub_district_master` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisation_master`
--

LOCK TABLES `organisation_master` WRITE;
/*!40000 ALTER TABLE `organisation_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `organisation_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organisation_type_master`
--

DROP TABLE IF EXISTS `organisation_type_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organisation_type_master` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `organisation_type_detail` varchar(255) NOT NULL,
  `organisation_type_abbreviation` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `last_updated_by` varchar(255) DEFAULT NULL,
  `last_updated_on` date DEFAULT NULL,
  `verified_by` varchar(255) DEFAULT NULL,
  `verified_on` date DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisation_type_master`
--

LOCK TABLES `organisation_type_master` WRITE;
/*!40000 ALTER TABLE `organisation_type_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `organisation_type_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otp`
--

DROP TABLE IF EXISTS `otp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `otp` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `otp` int(11) DEFAULT NULL,
  `mobile_number` bigint(20) DEFAULT NULL,
  `request_time` datetime,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otp`
--

LOCK TABLES `otp` WRITE;
/*!40000 ALTER TABLE `otp` DISABLE KEYS */;
/*!40000 ALTER TABLE `otp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state_master`
--

DROP TABLE IF EXISTS `state_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `state_master` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `state_code` varchar(255) DEFAULT NULL,
  `state_name` varchar(255) NOT NULL,
  `created_on` date DEFAULT NULL,
  `last_updated_on` date DEFAULT NULL,
  `last_updated_by` varchar(255) DEFAULT NULL,
  `verified_by` varchar(255) DEFAULT NULL,
  `verified_on` date DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_state_master_state_name` (`state_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state_master`
--

LOCK TABLES `state_master` WRITE;
/*!40000 ALTER TABLE `state_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `state_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_district_master`
--

DROP TABLE IF EXISTS `sub_district_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sub_district_master` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sub_district_code` varchar(255) DEFAULT NULL,
  `sub_district_name` varchar(255) NOT NULL,
  `last_updated_by` varchar(255) DEFAULT NULL,
  `last_updated_on` date DEFAULT NULL,
  `verified_by` varchar(255) DEFAULT NULL,
  `verified_on` date DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `state_master_id` bigint(20) DEFAULT NULL,
  `district_master_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sub_district_master_state_master_id` (`state_master_id`),
  KEY `fk_sub_district_master_district_master_id` (`district_master_id`),
  CONSTRAINT `fk_sub_district_master_district_master_id` FOREIGN KEY (`district_master_id`) REFERENCES `district_master` (`id`),
  CONSTRAINT `fk_sub_district_master_state_master_id` FOREIGN KEY (`state_master_id`) REFERENCES `state_master` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_district_master`
--

LOCK TABLES `sub_district_master` WRITE;
/*!40000 ALTER TABLE `sub_district_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `sub_district_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_details`
--

DROP TABLE IF EXISTS `user_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_full_name` varchar(255) DEFAULT NULL,
  `user_phone_mobile` varchar(255) NOT NULL,
  `user_phone_landline` varchar(255) DEFAULT NULL,
  `user_official_email` varchar(255) NOT NULL,
  `user_alternative_email` varchar(255) DEFAULT NULL,
  `user_login_password` varchar(255) DEFAULT NULL,
  `user_login_password_salt` varchar(255) DEFAULT NULL,
  `user_transaction_password` varchar(255) DEFAULT NULL,
  `user_transaction_password_salt` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `last_updated_by` varchar(255) DEFAULT NULL,
  `last_updated_on` date DEFAULT NULL,
  `verified_by` varchar(255) DEFAULT NULL,
  `verified_on` date DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `user_id_code` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `designation_master_id` bigint(20) DEFAULT NULL,
  `organisation_master_id` bigint(20) DEFAULT NULL,
  `esign_role_id` bigint(20) DEFAULT NULL,
  `application_master_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_user_details_user_id_code` (`user_id_code`),
  KEY `fk_user_details_designation_master_id` (`designation_master_id`),
  KEY `fk_user_details_organisation_master_id` (`organisation_master_id`),
  KEY `fk_user_details_esign_role_id` (`esign_role_id`),
  KEY `fk_user_details_application_master_id` (`application_master_id`),
  CONSTRAINT `fk_user_details_application_master_id` FOREIGN KEY (`application_master_id`) REFERENCES `application_master` (`id`),
  CONSTRAINT `fk_user_details_designation_master_id` FOREIGN KEY (`designation_master_id`) REFERENCES `designation_master` (`id`),
  CONSTRAINT `fk_user_details_esign_role_id` FOREIGN KEY (`esign_role_id`) REFERENCES `esign_role` (`id`),
  CONSTRAINT `fk_user_details_organisation_master_id` FOREIGN KEY (`organisation_master_id`) REFERENCES `organisation_master` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_details`
--

LOCK TABLES `user_details` WRITE;
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-06 10:27:50
