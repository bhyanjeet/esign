import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { BlockMasterComponentsPage, BlockMasterDeleteDialog, BlockMasterUpdatePage } from './block-master.page-object';

const expect = chai.expect;

describe('BlockMaster e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let blockMasterComponentsPage: BlockMasterComponentsPage;
  let blockMasterUpdatePage: BlockMasterUpdatePage;
  let blockMasterDeleteDialog: BlockMasterDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load BlockMasters', async () => {
    await navBarPage.goToEntity('block-master');
    blockMasterComponentsPage = new BlockMasterComponentsPage();
    await browser.wait(ec.visibilityOf(blockMasterComponentsPage.title), 5000);
    expect(await blockMasterComponentsPage.getTitle()).to.eq('Block Masters');
  });

  it('should load create BlockMaster page', async () => {
    await blockMasterComponentsPage.clickOnCreateButton();
    blockMasterUpdatePage = new BlockMasterUpdatePage();
    expect(await blockMasterUpdatePage.getPageTitle()).to.eq('Create or edit a Block Master');
    await blockMasterUpdatePage.cancel();
  });

  it('should create and save BlockMasters', async () => {
    const nbButtonsBeforeCreate = await blockMasterComponentsPage.countDeleteButtons();

    await blockMasterComponentsPage.clickOnCreateButton();
    await promise.all([
      blockMasterUpdatePage.setBlockCodeInput('blockCode'),
      blockMasterUpdatePage.setBlockNameInput('blockName'),
      blockMasterUpdatePage.setCreatedOnInput('2000-12-31'),
      blockMasterUpdatePage.setLastUpdatedByInput('lastUpdatedBy'),
      blockMasterUpdatePage.setLastUpdatedOnInput('2000-12-31'),
      blockMasterUpdatePage.setVerifiedByInput('verifiedBy'),
      blockMasterUpdatePage.setVerifiedOnInput('2000-12-31'),
      blockMasterUpdatePage.setRemarksInput('remarks'),
      blockMasterUpdatePage.stateMasterSelectLastOption(),
      blockMasterUpdatePage.districtMasterSelectLastOption()
    ]);
    expect(await blockMasterUpdatePage.getBlockCodeInput()).to.eq('blockCode', 'Expected BlockCode value to be equals to blockCode');
    expect(await blockMasterUpdatePage.getBlockNameInput()).to.eq('blockName', 'Expected BlockName value to be equals to blockName');
    expect(await blockMasterUpdatePage.getCreatedOnInput()).to.eq('2000-12-31', 'Expected createdOn value to be equals to 2000-12-31');
    expect(await blockMasterUpdatePage.getLastUpdatedByInput()).to.eq(
      'lastUpdatedBy',
      'Expected LastUpdatedBy value to be equals to lastUpdatedBy'
    );
    expect(await blockMasterUpdatePage.getLastUpdatedOnInput()).to.eq(
      '2000-12-31',
      'Expected lastUpdatedOn value to be equals to 2000-12-31'
    );
    expect(await blockMasterUpdatePage.getVerifiedByInput()).to.eq('verifiedBy', 'Expected VerifiedBy value to be equals to verifiedBy');
    expect(await blockMasterUpdatePage.getVerifiedOnInput()).to.eq('2000-12-31', 'Expected verifiedOn value to be equals to 2000-12-31');
    expect(await blockMasterUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
    await blockMasterUpdatePage.save();
    expect(await blockMasterUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await blockMasterComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last BlockMaster', async () => {
    const nbButtonsBeforeDelete = await blockMasterComponentsPage.countDeleteButtons();
    await blockMasterComponentsPage.clickOnLastDeleteButton();

    blockMasterDeleteDialog = new BlockMasterDeleteDialog();
    expect(await blockMasterDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Block Master?');
    await blockMasterDeleteDialog.clickOnConfirmButton();

    expect(await blockMasterComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
