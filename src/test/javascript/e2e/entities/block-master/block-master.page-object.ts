import { element, by, ElementFinder } from 'protractor';

export class BlockMasterComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-block-master div table .btn-danger'));
  title = element.all(by.css('jhi-block-master div h2#page-heading span')).first();

  async clickOnCreateButton() {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton() {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getText();
  }
}

export class BlockMasterUpdatePage {
  pageTitle = element(by.id('jhi-block-master-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  blockCodeInput = element(by.id('field_blockCode'));
  blockNameInput = element(by.id('field_blockName'));
  createdOnInput = element(by.id('field_createdOn'));
  lastUpdatedByInput = element(by.id('field_lastUpdatedBy'));
  lastUpdatedOnInput = element(by.id('field_lastUpdatedOn'));
  verifiedByInput = element(by.id('field_verifiedBy'));
  verifiedOnInput = element(by.id('field_verifiedOn'));
  remarksInput = element(by.id('field_remarks'));
  stateMasterSelect = element(by.id('field_stateMaster'));
  districtMasterSelect = element(by.id('field_districtMaster'));

  async getPageTitle() {
    return this.pageTitle.getText();
  }

  async setBlockCodeInput(blockCode) {
    await this.blockCodeInput.sendKeys(blockCode);
  }

  async getBlockCodeInput() {
    return await this.blockCodeInput.getAttribute('value');
  }

  async setBlockNameInput(blockName) {
    await this.blockNameInput.sendKeys(blockName);
  }

  async getBlockNameInput() {
    return await this.blockNameInput.getAttribute('value');
  }

  async setCreatedOnInput(createdOn) {
    await this.createdOnInput.sendKeys(createdOn);
  }

  async getCreatedOnInput() {
    return await this.createdOnInput.getAttribute('value');
  }

  async setLastUpdatedByInput(lastUpdatedBy) {
    await this.lastUpdatedByInput.sendKeys(lastUpdatedBy);
  }

  async getLastUpdatedByInput() {
    return await this.lastUpdatedByInput.getAttribute('value');
  }

  async setLastUpdatedOnInput(lastUpdatedOn) {
    await this.lastUpdatedOnInput.sendKeys(lastUpdatedOn);
  }

  async getLastUpdatedOnInput() {
    return await this.lastUpdatedOnInput.getAttribute('value');
  }

  async setVerifiedByInput(verifiedBy) {
    await this.verifiedByInput.sendKeys(verifiedBy);
  }

  async getVerifiedByInput() {
    return await this.verifiedByInput.getAttribute('value');
  }

  async setVerifiedOnInput(verifiedOn) {
    await this.verifiedOnInput.sendKeys(verifiedOn);
  }

  async getVerifiedOnInput() {
    return await this.verifiedOnInput.getAttribute('value');
  }

  async setRemarksInput(remarks) {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput() {
    return await this.remarksInput.getAttribute('value');
  }

  async stateMasterSelectLastOption() {
    await this.stateMasterSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async stateMasterSelectOption(option) {
    await this.stateMasterSelect.sendKeys(option);
  }

  getStateMasterSelect(): ElementFinder {
    return this.stateMasterSelect;
  }

  async getStateMasterSelectedOption() {
    return await this.stateMasterSelect.element(by.css('option:checked')).getText();
  }

  async districtMasterSelectLastOption() {
    await this.districtMasterSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async districtMasterSelectOption(option) {
    await this.districtMasterSelect.sendKeys(option);
  }

  getDistrictMasterSelect(): ElementFinder {
    return this.districtMasterSelect;
  }

  async getDistrictMasterSelectedOption() {
    return await this.districtMasterSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class BlockMasterDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-blockMaster-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-blockMaster'));

  async getDialogTitle() {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}
