import { element, by, ElementFinder } from 'protractor';

export class OrganisationMasterComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-organisation-master div table .btn-danger'));
  title = element.all(by.css('jhi-organisation-master div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getText();
  }
}

export class OrganisationMasterUpdatePage {
  pageTitle = element(by.id('jhi-organisation-master-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  organisationNameInput = element(by.id('field_organisationName'));
  organisationAbbreviationInput = element(by.id('field_organisationAbbreviation'));
  organisationCorrespondenceEmailInput = element(by.id('field_organisationCorrespondenceEmail'));
  organisationCorrespondencePhone1Input = element(by.id('field_organisationCorrespondencePhone1'));
  organisationCorrespondencePhone2Input = element(by.id('field_organisationCorrespondencePhone2'));
  organisationCorrespondenceAddressInput = element(by.id('field_organisationCorrespondenceAddress'));
  organisationWebsiteInput = element(by.id('field_organisationWebsite'));
  organisationNodalOfficerNameInput = element(by.id('field_organisationNodalOfficerName'));
  organisationNodalOfficerPhoneMobileInput = element(by.id('field_organisationNodalOfficerPhoneMobile'));
  organisationNodalOfficerPhoneLandlineInput = element(by.id('field_organisationNodalOfficerPhoneLandline'));
  nodalOfficerOfficialEmailInput = element(by.id('field_nodalOfficerOfficialEmail'));
  organisationNodalOfficerAlternativeEmailInput = element(by.id('field_organisationNodalOfficerAlternativeEmail'));
  createdByInput = element(by.id('field_createdBy'));
  createdOnInput = element(by.id('field_createdOn'));
  lastUpdatedByInput = element(by.id('field_lastUpdatedBy'));
  lastUpdatedOnInput = element(by.id('field_lastUpdatedOn'));
  verifiedByInput = element(by.id('field_verifiedBy'));
  verifiedOnInput = element(by.id('field_verifiedOn'));
  remarksInput = element(by.id('field_remarks'));
  userIdInput = element(by.id('field_userId'));
  stageInput = element(by.id('field_stage'));
  statusInput = element(by.id('field_status'));
  organisationIdCodeInput = element(by.id('field_organisationIdCode'));
  organisationEmployeeIdInput = element(by.id('field_organisationEmployeeId'));
  dateOfBirthInput = element(by.id('field_dateOfBirth'));
  genderInput = element(by.id('field_gender'));
  panInput = element(by.id('field_pan'));
  aadhaarInput = element(by.id('field_aadhaar'));
  orgnisationEmpolyeeCardAttchmentInput = element(by.id('field_orgnisationEmpolyeeCardAttchment'));
  panAttachmentInput = element(by.id('field_panAttachment'));
  photographAttachmentInput = element(by.id('field_photographAttachment'));
  aadhaarAttachmentInput = element(by.id('field_aadhaarAttachment'));
  pinCodeInput = element(by.id('field_pinCode'));

  organisationTypeMasterSelect = element(by.id('field_organisationTypeMaster'));
  stateMasterSelect = element(by.id('field_stateMaster'));
  districtMasterSelect = element(by.id('field_districtMaster'));
  subDistrictMasterSelect = element(by.id('field_subDistrictMaster'));
  departmentSelect = element(by.id('field_department'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }

  async setOrganisationNameInput(organisationName: string): Promise<void> {
    await this.organisationNameInput.sendKeys(organisationName);
  }

  async getOrganisationNameInput(): Promise<string> {
    return await this.organisationNameInput.getAttribute('value');
  }

  async setOrganisationAbbreviationInput(organisationAbbreviation: string): Promise<void> {
    await this.organisationAbbreviationInput.sendKeys(organisationAbbreviation);
  }

  async getOrganisationAbbreviationInput(): Promise<string> {
    return await this.organisationAbbreviationInput.getAttribute('value');
  }

  async setOrganisationCorrespondenceEmailInput(organisationCorrespondenceEmail: string): Promise<void> {
    await this.organisationCorrespondenceEmailInput.sendKeys(organisationCorrespondenceEmail);
  }

  async getOrganisationCorrespondenceEmailInput(): Promise<string> {
    return await this.organisationCorrespondenceEmailInput.getAttribute('value');
  }

  async setOrganisationCorrespondencePhone1Input(organisationCorrespondencePhone1: string): Promise<void> {
    await this.organisationCorrespondencePhone1Input.sendKeys(organisationCorrespondencePhone1);
  }

  async getOrganisationCorrespondencePhone1Input(): Promise<string> {
    return await this.organisationCorrespondencePhone1Input.getAttribute('value');
  }

  async setOrganisationCorrespondencePhone2Input(organisationCorrespondencePhone2: string): Promise<void> {
    await this.organisationCorrespondencePhone2Input.sendKeys(organisationCorrespondencePhone2);
  }

  async getOrganisationCorrespondencePhone2Input(): Promise<string> {
    return await this.organisationCorrespondencePhone2Input.getAttribute('value');
  }

  async setOrganisationCorrespondenceAddressInput(organisationCorrespondenceAddress: string): Promise<void> {
    await this.organisationCorrespondenceAddressInput.sendKeys(organisationCorrespondenceAddress);
  }

  async getOrganisationCorrespondenceAddressInput(): Promise<string> {
    return await this.organisationCorrespondenceAddressInput.getAttribute('value');
  }

  async setOrganisationWebsiteInput(organisationWebsite: string): Promise<void> {
    await this.organisationWebsiteInput.sendKeys(organisationWebsite);
  }

  async getOrganisationWebsiteInput(): Promise<string> {
    return await this.organisationWebsiteInput.getAttribute('value');
  }

  async setOrganisationNodalOfficerNameInput(organisationNodalOfficerName: string): Promise<void> {
    await this.organisationNodalOfficerNameInput.sendKeys(organisationNodalOfficerName);
  }

  async getOrganisationNodalOfficerNameInput(): Promise<string> {
    return await this.organisationNodalOfficerNameInput.getAttribute('value');
  }

  async setOrganisationNodalOfficerPhoneMobileInput(organisationNodalOfficerPhoneMobile: string): Promise<void> {
    await this.organisationNodalOfficerPhoneMobileInput.sendKeys(organisationNodalOfficerPhoneMobile);
  }

  async getOrganisationNodalOfficerPhoneMobileInput(): Promise<string> {
    return await this.organisationNodalOfficerPhoneMobileInput.getAttribute('value');
  }

  async setOrganisationNodalOfficerPhoneLandlineInput(organisationNodalOfficerPhoneLandline: string): Promise<void> {
    await this.organisationNodalOfficerPhoneLandlineInput.sendKeys(organisationNodalOfficerPhoneLandline);
  }

  async getOrganisationNodalOfficerPhoneLandlineInput(): Promise<string> {
    return await this.organisationNodalOfficerPhoneLandlineInput.getAttribute('value');
  }

  async setNodalOfficerOfficialEmailInput(nodalOfficerOfficialEmail: string): Promise<void> {
    await this.nodalOfficerOfficialEmailInput.sendKeys(nodalOfficerOfficialEmail);
  }

  async getNodalOfficerOfficialEmailInput(): Promise<string> {
    return await this.nodalOfficerOfficialEmailInput.getAttribute('value');
  }

  async setOrganisationNodalOfficerAlternativeEmailInput(organisationNodalOfficerAlternativeEmail: string): Promise<void> {
    await this.organisationNodalOfficerAlternativeEmailInput.sendKeys(organisationNodalOfficerAlternativeEmail);
  }

  async getOrganisationNodalOfficerAlternativeEmailInput(): Promise<string> {
    return await this.organisationNodalOfficerAlternativeEmailInput.getAttribute('value');
  }

  async setCreatedByInput(createdBy: string): Promise<void> {
    await this.createdByInput.sendKeys(createdBy);
  }

  async getCreatedByInput(): Promise<string> {
    return await this.createdByInput.getAttribute('value');
  }

  async setCreatedOnInput(createdOn: string): Promise<void> {
    await this.createdOnInput.sendKeys(createdOn);
  }

  async getCreatedOnInput(): Promise<string> {
    return await this.createdOnInput.getAttribute('value');
  }

  async setLastUpdatedByInput(lastUpdatedBy: string): Promise<void> {
    await this.lastUpdatedByInput.sendKeys(lastUpdatedBy);
  }

  async getLastUpdatedByInput(): Promise<string> {
    return await this.lastUpdatedByInput.getAttribute('value');
  }

  async setLastUpdatedOnInput(lastUpdatedOn: string): Promise<void> {
    await this.lastUpdatedOnInput.sendKeys(lastUpdatedOn);
  }

  async getLastUpdatedOnInput(): Promise<string> {
    return await this.lastUpdatedOnInput.getAttribute('value');
  }

  async setVerifiedByInput(verifiedBy: string): Promise<void> {
    await this.verifiedByInput.sendKeys(verifiedBy);
  }

  async getVerifiedByInput(): Promise<string> {
    return await this.verifiedByInput.getAttribute('value');
  }

  async setVerifiedOnInput(verifiedOn: string): Promise<void> {
    await this.verifiedOnInput.sendKeys(verifiedOn);
  }

  async getVerifiedOnInput(): Promise<string> {
    return await this.verifiedOnInput.getAttribute('value');
  }

  async setRemarksInput(remarks: string): Promise<void> {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput(): Promise<string> {
    return await this.remarksInput.getAttribute('value');
  }

  async setUserIdInput(userId: string): Promise<void> {
    await this.userIdInput.sendKeys(userId);
  }

  async getUserIdInput(): Promise<string> {
    return await this.userIdInput.getAttribute('value');
  }

  async setStageInput(stage: string): Promise<void> {
    await this.stageInput.sendKeys(stage);
  }

  async getStageInput(): Promise<string> {
    return await this.stageInput.getAttribute('value');
  }

  async setStatusInput(status: string): Promise<void> {
    await this.statusInput.sendKeys(status);
  }

  async getStatusInput(): Promise<string> {
    return await this.statusInput.getAttribute('value');
  }

  async setOrganisationIdCodeInput(organisationIdCode: string): Promise<void> {
    await this.organisationIdCodeInput.sendKeys(organisationIdCode);
  }

  async getOrganisationIdCodeInput(): Promise<string> {
    return await this.organisationIdCodeInput.getAttribute('value');
  }

  async setOrganisationEmployeeIdInput(organisationEmployeeId: string): Promise<void> {
    await this.organisationEmployeeIdInput.sendKeys(organisationEmployeeId);
  }

  async getOrganisationEmployeeIdInput(): Promise<string> {
    return await this.organisationEmployeeIdInput.getAttribute('value');
  }

  async setDateOfBirthInput(dateOfBirth: string): Promise<void> {
    await this.dateOfBirthInput.sendKeys(dateOfBirth);
  }

  async getDateOfBirthInput(): Promise<string> {
    return await this.dateOfBirthInput.getAttribute('value');
  }

  async setGenderInput(gender: string): Promise<void> {
    await this.genderInput.sendKeys(gender);
  }

  async getGenderInput(): Promise<string> {
    return await this.genderInput.getAttribute('value');
  }

  async setPanInput(pan: string): Promise<void> {
    await this.panInput.sendKeys(pan);
  }

  async getPanInput(): Promise<string> {
    return await this.panInput.getAttribute('value');
  }

  async setAadhaarInput(aadhaar: string): Promise<void> {
    await this.aadhaarInput.sendKeys(aadhaar);
  }

  async getAadhaarInput(): Promise<string> {
    return await this.aadhaarInput.getAttribute('value');
  }

  async setOrgnisationEmpolyeeCardAttchmentInput(orgnisationEmpolyeeCardAttchment: string): Promise<void> {
    await this.orgnisationEmpolyeeCardAttchmentInput.sendKeys(orgnisationEmpolyeeCardAttchment);
  }

  async getOrgnisationEmpolyeeCardAttchmentInput(): Promise<string> {
    return await this.orgnisationEmpolyeeCardAttchmentInput.getAttribute('value');
  }

  async setPanAttachmentInput(panAttachment: string): Promise<void> {
    await this.panAttachmentInput.sendKeys(panAttachment);
  }

  async getPanAttachmentInput(): Promise<string> {
    return await this.panAttachmentInput.getAttribute('value');
  }

  async setPhotographAttachmentInput(photographAttachment: string): Promise<void> {
    await this.photographAttachmentInput.sendKeys(photographAttachment);
  }

  async getPhotographAttachmentInput(): Promise<string> {
    return await this.photographAttachmentInput.getAttribute('value');
  }

  async setAadhaarAttachmentInput(aadhaarAttachment: string): Promise<void> {
    await this.aadhaarAttachmentInput.sendKeys(aadhaarAttachment);
  }

  async getAadhaarAttachmentInput(): Promise<string> {
    return await this.aadhaarAttachmentInput.getAttribute('value');
  }

  async setPinCodeInput(pinCode: string): Promise<void> {
    await this.pinCodeInput.sendKeys(pinCode);
  }

  async getPinCodeInput(): Promise<string> {
    return await this.pinCodeInput.getAttribute('value');
  }

  async organisationTypeMasterSelectLastOption(): Promise<void> {
    await this.organisationTypeMasterSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async organisationTypeMasterSelectOption(option: string): Promise<void> {
    await this.organisationTypeMasterSelect.sendKeys(option);
  }

  getOrganisationTypeMasterSelect(): ElementFinder {
    return this.organisationTypeMasterSelect;
  }

  async getOrganisationTypeMasterSelectedOption(): Promise<string> {
    return await this.organisationTypeMasterSelect.element(by.css('option:checked')).getText();
  }

  async stateMasterSelectLastOption(): Promise<void> {
    await this.stateMasterSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async stateMasterSelectOption(option: string): Promise<void> {
    await this.stateMasterSelect.sendKeys(option);
  }

  getStateMasterSelect(): ElementFinder {
    return this.stateMasterSelect;
  }

  async getStateMasterSelectedOption(): Promise<string> {
    return await this.stateMasterSelect.element(by.css('option:checked')).getText();
  }

  async districtMasterSelectLastOption(): Promise<void> {
    await this.districtMasterSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async districtMasterSelectOption(option: string): Promise<void> {
    await this.districtMasterSelect.sendKeys(option);
  }

  getDistrictMasterSelect(): ElementFinder {
    return this.districtMasterSelect;
  }

  async getDistrictMasterSelectedOption(): Promise<string> {
    return await this.districtMasterSelect.element(by.css('option:checked')).getText();
  }

  async subDistrictMasterSelectLastOption(): Promise<void> {
    await this.subDistrictMasterSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async subDistrictMasterSelectOption(option: string): Promise<void> {
    await this.subDistrictMasterSelect.sendKeys(option);
  }

  getSubDistrictMasterSelect(): ElementFinder {
    return this.subDistrictMasterSelect;
  }

  async getSubDistrictMasterSelectedOption(): Promise<string> {
    return await this.subDistrictMasterSelect.element(by.css('option:checked')).getText();
  }

  async departmentSelectLastOption(): Promise<void> {
    await this.departmentSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async departmentSelectOption(option: string): Promise<void> {
    await this.departmentSelect.sendKeys(option);
  }

  getDepartmentSelect(): ElementFinder {
    return this.departmentSelect;
  }

  async getDepartmentSelectedOption(): Promise<string> {
    return await this.departmentSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class OrganisationMasterDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-organisationMaster-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-organisationMaster'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
