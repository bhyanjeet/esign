import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  OrganisationMasterComponentsPage,
  OrganisationMasterDeleteDialog,
  OrganisationMasterUpdatePage
} from './organisation-master.page-object';

const expect = chai.expect;

describe('OrganisationMaster e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let organisationMasterComponentsPage: OrganisationMasterComponentsPage;
  let organisationMasterUpdatePage: OrganisationMasterUpdatePage;
  let organisationMasterDeleteDialog: OrganisationMasterDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load OrganisationMasters', async () => {
    await navBarPage.goToEntity('organisation-master');
    organisationMasterComponentsPage = new OrganisationMasterComponentsPage();
    await browser.wait(ec.visibilityOf(organisationMasterComponentsPage.title), 5000);
    expect(await organisationMasterComponentsPage.getTitle()).to.eq('Organisation Masters');
    await browser.wait(
      ec.or(ec.visibilityOf(organisationMasterComponentsPage.entities), ec.visibilityOf(organisationMasterComponentsPage.noResult)),
      1000
    );
  });

  it('should load create OrganisationMaster page', async () => {
    await organisationMasterComponentsPage.clickOnCreateButton();
    organisationMasterUpdatePage = new OrganisationMasterUpdatePage();
    expect(await organisationMasterUpdatePage.getPageTitle()).to.eq('Create or edit a Organisation Master');
    await organisationMasterUpdatePage.cancel();
  });

  it('should create and save OrganisationMasters', async () => {
    const nbButtonsBeforeCreate = await organisationMasterComponentsPage.countDeleteButtons();

    await organisationMasterComponentsPage.clickOnCreateButton();

    await promise.all([
      organisationMasterUpdatePage.setOrganisationNameInput('organisationName'),
      organisationMasterUpdatePage.setOrganisationAbbreviationInput('organisationAbbreviation'),
      organisationMasterUpdatePage.setOrganisationCorrespondenceEmailInput('organisationCorrespondenceEmail'),
      organisationMasterUpdatePage.setOrganisationCorrespondencePhone1Input('organisationCorrespondencePhone1'),
      organisationMasterUpdatePage.setOrganisationCorrespondencePhone2Input('organisationCorrespondencePhone2'),
      organisationMasterUpdatePage.setOrganisationCorrespondenceAddressInput('organisationCorrespondenceAddress'),
      organisationMasterUpdatePage.setOrganisationWebsiteInput('organisationWebsite'),
      organisationMasterUpdatePage.setOrganisationNodalOfficerNameInput('organisationNodalOfficerName'),
      organisationMasterUpdatePage.setOrganisationNodalOfficerPhoneMobileInput('organisationNodalOfficerPhoneMobile'),
      organisationMasterUpdatePage.setOrganisationNodalOfficerPhoneLandlineInput('organisationNodalOfficerPhoneLandline'),
      organisationMasterUpdatePage.setNodalOfficerOfficialEmailInput('nodalOfficerOfficialEmail'),
      organisationMasterUpdatePage.setOrganisationNodalOfficerAlternativeEmailInput('organisationNodalOfficerAlternativeEmail'),
      organisationMasterUpdatePage.setCreatedByInput('createdBy'),
      organisationMasterUpdatePage.setCreatedOnInput('2000-12-31'),
      organisationMasterUpdatePage.setLastUpdatedByInput('lastUpdatedBy'),
      organisationMasterUpdatePage.setLastUpdatedOnInput('2000-12-31'),
      organisationMasterUpdatePage.setVerifiedByInput('verifiedBy'),
      organisationMasterUpdatePage.setVerifiedOnInput('2000-12-31'),
      organisationMasterUpdatePage.setRemarksInput('remarks'),
      organisationMasterUpdatePage.setUserIdInput('5'),
      organisationMasterUpdatePage.setStageInput('stage'),
      organisationMasterUpdatePage.setStatusInput('status'),
      organisationMasterUpdatePage.setOrganisationIdCodeInput('organisationIdCode'),
      organisationMasterUpdatePage.setOrganisationEmployeeIdInput('organisationEmployeeId'),
      organisationMasterUpdatePage.setDateOfBirthInput('2000-12-31'),
      organisationMasterUpdatePage.setGenderInput('gender'),
      organisationMasterUpdatePage.setPanInput('pan'),
      organisationMasterUpdatePage.setAadhaarInput('aadhaar'),
      organisationMasterUpdatePage.setOrgnisationEmpolyeeCardAttchmentInput('orgnisationEmpolyeeCardAttchment'),
      organisationMasterUpdatePage.setPanAttachmentInput('panAttachment'),
      organisationMasterUpdatePage.setPhotographAttachmentInput('photographAttachment'),
      organisationMasterUpdatePage.setAadhaarAttachmentInput('aadhaarAttachment'),
      organisationMasterUpdatePage.setPinCodeInput('pinCode'),
      organisationMasterUpdatePage.organisationTypeMasterSelectLastOption(),
      organisationMasterUpdatePage.stateMasterSelectLastOption(),
      organisationMasterUpdatePage.districtMasterSelectLastOption(),
      organisationMasterUpdatePage.subDistrictMasterSelectLastOption(),
      organisationMasterUpdatePage.departmentSelectLastOption()
    ]);

    expect(await organisationMasterUpdatePage.getOrganisationNameInput()).to.eq(
      'organisationName',
      'Expected OrganisationName value to be equals to organisationName'
    );
    expect(await organisationMasterUpdatePage.getOrganisationAbbreviationInput()).to.eq(
      'organisationAbbreviation',
      'Expected OrganisationAbbreviation value to be equals to organisationAbbreviation'
    );
    expect(await organisationMasterUpdatePage.getOrganisationCorrespondenceEmailInput()).to.eq(
      'organisationCorrespondenceEmail',
      'Expected OrganisationCorrespondenceEmail value to be equals to organisationCorrespondenceEmail'
    );
    expect(await organisationMasterUpdatePage.getOrganisationCorrespondencePhone1Input()).to.eq(
      'organisationCorrespondencePhone1',
      'Expected OrganisationCorrespondencePhone1 value to be equals to organisationCorrespondencePhone1'
    );
    expect(await organisationMasterUpdatePage.getOrganisationCorrespondencePhone2Input()).to.eq(
      'organisationCorrespondencePhone2',
      'Expected OrganisationCorrespondencePhone2 value to be equals to organisationCorrespondencePhone2'
    );
    expect(await organisationMasterUpdatePage.getOrganisationCorrespondenceAddressInput()).to.eq(
      'organisationCorrespondenceAddress',
      'Expected OrganisationCorrespondenceAddress value to be equals to organisationCorrespondenceAddress'
    );
    expect(await organisationMasterUpdatePage.getOrganisationWebsiteInput()).to.eq(
      'organisationWebsite',
      'Expected OrganisationWebsite value to be equals to organisationWebsite'
    );
    expect(await organisationMasterUpdatePage.getOrganisationNodalOfficerNameInput()).to.eq(
      'organisationNodalOfficerName',
      'Expected OrganisationNodalOfficerName value to be equals to organisationNodalOfficerName'
    );
    expect(await organisationMasterUpdatePage.getOrganisationNodalOfficerPhoneMobileInput()).to.eq(
      'organisationNodalOfficerPhoneMobile',
      'Expected OrganisationNodalOfficerPhoneMobile value to be equals to organisationNodalOfficerPhoneMobile'
    );
    expect(await organisationMasterUpdatePage.getOrganisationNodalOfficerPhoneLandlineInput()).to.eq(
      'organisationNodalOfficerPhoneLandline',
      'Expected OrganisationNodalOfficerPhoneLandline value to be equals to organisationNodalOfficerPhoneLandline'
    );
    expect(await organisationMasterUpdatePage.getNodalOfficerOfficialEmailInput()).to.eq(
      'nodalOfficerOfficialEmail',
      'Expected NodalOfficerOfficialEmail value to be equals to nodalOfficerOfficialEmail'
    );
    expect(await organisationMasterUpdatePage.getOrganisationNodalOfficerAlternativeEmailInput()).to.eq(
      'organisationNodalOfficerAlternativeEmail',
      'Expected OrganisationNodalOfficerAlternativeEmail value to be equals to organisationNodalOfficerAlternativeEmail'
    );
    expect(await organisationMasterUpdatePage.getCreatedByInput()).to.eq('createdBy', 'Expected CreatedBy value to be equals to createdBy');
    expect(await organisationMasterUpdatePage.getCreatedOnInput()).to.eq(
      '2000-12-31',
      'Expected createdOn value to be equals to 2000-12-31'
    );
    expect(await organisationMasterUpdatePage.getLastUpdatedByInput()).to.eq(
      'lastUpdatedBy',
      'Expected LastUpdatedBy value to be equals to lastUpdatedBy'
    );
    expect(await organisationMasterUpdatePage.getLastUpdatedOnInput()).to.eq(
      '2000-12-31',
      'Expected lastUpdatedOn value to be equals to 2000-12-31'
    );
    expect(await organisationMasterUpdatePage.getVerifiedByInput()).to.eq(
      'verifiedBy',
      'Expected VerifiedBy value to be equals to verifiedBy'
    );
    expect(await organisationMasterUpdatePage.getVerifiedOnInput()).to.eq(
      '2000-12-31',
      'Expected verifiedOn value to be equals to 2000-12-31'
    );
    expect(await organisationMasterUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
    expect(await organisationMasterUpdatePage.getUserIdInput()).to.eq('5', 'Expected userId value to be equals to 5');
    expect(await organisationMasterUpdatePage.getStageInput()).to.eq('stage', 'Expected Stage value to be equals to stage');
    expect(await organisationMasterUpdatePage.getStatusInput()).to.eq('status', 'Expected Status value to be equals to status');
    expect(await organisationMasterUpdatePage.getOrganisationIdCodeInput()).to.eq(
      'organisationIdCode',
      'Expected OrganisationIdCode value to be equals to organisationIdCode'
    );
    expect(await organisationMasterUpdatePage.getOrganisationEmployeeIdInput()).to.eq(
      'organisationEmployeeId',
      'Expected OrganisationEmployeeId value to be equals to organisationEmployeeId'
    );
    expect(await organisationMasterUpdatePage.getDateOfBirthInput()).to.eq(
      '2000-12-31',
      'Expected dateOfBirth value to be equals to 2000-12-31'
    );
    expect(await organisationMasterUpdatePage.getGenderInput()).to.eq('gender', 'Expected Gender value to be equals to gender');
    expect(await organisationMasterUpdatePage.getPanInput()).to.eq('pan', 'Expected Pan value to be equals to pan');
    expect(await organisationMasterUpdatePage.getAadhaarInput()).to.eq('aadhaar', 'Expected Aadhaar value to be equals to aadhaar');
    expect(await organisationMasterUpdatePage.getOrgnisationEmpolyeeCardAttchmentInput()).to.eq(
      'orgnisationEmpolyeeCardAttchment',
      'Expected OrgnisationEmpolyeeCardAttchment value to be equals to orgnisationEmpolyeeCardAttchment'
    );
    expect(await organisationMasterUpdatePage.getPanAttachmentInput()).to.eq(
      'panAttachment',
      'Expected PanAttachment value to be equals to panAttachment'
    );
    expect(await organisationMasterUpdatePage.getPhotographAttachmentInput()).to.eq(
      'photographAttachment',
      'Expected PhotographAttachment value to be equals to photographAttachment'
    );
    expect(await organisationMasterUpdatePage.getAadhaarAttachmentInput()).to.eq(
      'aadhaarAttachment',
      'Expected AadhaarAttachment value to be equals to aadhaarAttachment'
    );
    expect(await organisationMasterUpdatePage.getPinCodeInput()).to.eq('pinCode', 'Expected PinCode value to be equals to pinCode');

    await organisationMasterUpdatePage.save();
    expect(await organisationMasterUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await organisationMasterComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last OrganisationMaster', async () => {
    const nbButtonsBeforeDelete = await organisationMasterComponentsPage.countDeleteButtons();
    await organisationMasterComponentsPage.clickOnLastDeleteButton();

    organisationMasterDeleteDialog = new OrganisationMasterDeleteDialog();
    expect(await organisationMasterDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Organisation Master?');
    await organisationMasterDeleteDialog.clickOnConfirmButton();

    expect(await organisationMasterComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
