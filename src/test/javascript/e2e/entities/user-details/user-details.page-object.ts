import { element, by, ElementFinder } from 'protractor';

export class UserDetailsComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-user-details div table .btn-danger'));
  title = element.all(by.css('jhi-user-details div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getText();
  }
}

export class UserDetailsUpdatePage {
  pageTitle = element(by.id('jhi-user-details-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  userFullNameInput = element(by.id('field_userFullName'));
  userPhoneMobileInput = element(by.id('field_userPhoneMobile'));
  userPhoneLandlineInput = element(by.id('field_userPhoneLandline'));
  userOfficialEmailInput = element(by.id('field_userOfficialEmail'));
  userAlternativeEmailInput = element(by.id('field_userAlternativeEmail'));
  userLoginPasswordInput = element(by.id('field_userLoginPassword'));
  userLoginPasswordSaltInput = element(by.id('field_userLoginPasswordSalt'));
  userTransactionPasswordInput = element(by.id('field_userTransactionPassword'));
  userTransactionPasswordSaltInput = element(by.id('field_userTransactionPasswordSalt'));
  createdByInput = element(by.id('field_createdBy'));
  createdOnInput = element(by.id('field_createdOn'));
  lastUpdatedByInput = element(by.id('field_lastUpdatedBy'));
  lastUpdatedOnInput = element(by.id('field_lastUpdatedOn'));
  verifiedByInput = element(by.id('field_verifiedBy'));
  verifiedOnInput = element(by.id('field_verifiedOn'));
  remarksInput = element(by.id('field_remarks'));
  userIdCodeInput = element(by.id('field_userIdCode'));
  statusInput = element(by.id('field_status'));
  usernameInput = element(by.id('field_username'));
  userPINInput = element(by.id('field_userPIN'));
  organisationUnitInput = element(by.id('field_organisationUnit'));
  addressInput = element(by.id('field_address'));
  counrtyInput = element(by.id('field_counrty'));
  postalCodeInput = element(by.id('field_postalCode'));
  photographInput = element(by.id('field_photograph'));
  dobInput = element(by.id('field_dob'));
  genderInput = element(by.id('field_gender'));
  panInput = element(by.id('field_pan'));
  aadhaarInput = element(by.id('field_aadhaar'));
  panAttachmentInput = element(by.id('field_panAttachment'));
  employeeIdInput = element(by.id('field_employeeId'));
  employeeCardAttachmentInput = element(by.id('field_employeeCardAttachment'));
  signerIdInput = element(by.id('field_signerId'));
  lastActionInput = element(by.id('field_lastAction'));
  accountStatusInput = element(by.id('field_accountStatus'));

  designationMasterSelect = element(by.id('field_designationMaster'));
  organisationMasterSelect = element(by.id('field_organisationMaster'));
  esignRoleSelect = element(by.id('field_esignRole'));
  applicationMasterSelect = element(by.id('field_applicationMaster'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }

  async setUserFullNameInput(userFullName: string): Promise<void> {
    await this.userFullNameInput.sendKeys(userFullName);
  }

  async getUserFullNameInput(): Promise<string> {
    return await this.userFullNameInput.getAttribute('value');
  }

  async setUserPhoneMobileInput(userPhoneMobile: string): Promise<void> {
    await this.userPhoneMobileInput.sendKeys(userPhoneMobile);
  }

  async getUserPhoneMobileInput(): Promise<string> {
    return await this.userPhoneMobileInput.getAttribute('value');
  }

  async setUserPhoneLandlineInput(userPhoneLandline: string): Promise<void> {
    await this.userPhoneLandlineInput.sendKeys(userPhoneLandline);
  }

  async getUserPhoneLandlineInput(): Promise<string> {
    return await this.userPhoneLandlineInput.getAttribute('value');
  }

  async setUserOfficialEmailInput(userOfficialEmail: string): Promise<void> {
    await this.userOfficialEmailInput.sendKeys(userOfficialEmail);
  }

  async getUserOfficialEmailInput(): Promise<string> {
    return await this.userOfficialEmailInput.getAttribute('value');
  }

  async setUserAlternativeEmailInput(userAlternativeEmail: string): Promise<void> {
    await this.userAlternativeEmailInput.sendKeys(userAlternativeEmail);
  }

  async getUserAlternativeEmailInput(): Promise<string> {
    return await this.userAlternativeEmailInput.getAttribute('value');
  }

  async setUserLoginPasswordInput(userLoginPassword: string): Promise<void> {
    await this.userLoginPasswordInput.sendKeys(userLoginPassword);
  }

  async getUserLoginPasswordInput(): Promise<string> {
    return await this.userLoginPasswordInput.getAttribute('value');
  }

  async setUserLoginPasswordSaltInput(userLoginPasswordSalt: string): Promise<void> {
    await this.userLoginPasswordSaltInput.sendKeys(userLoginPasswordSalt);
  }

  async getUserLoginPasswordSaltInput(): Promise<string> {
    return await this.userLoginPasswordSaltInput.getAttribute('value');
  }

  async setUserTransactionPasswordInput(userTransactionPassword: string): Promise<void> {
    await this.userTransactionPasswordInput.sendKeys(userTransactionPassword);
  }

  async getUserTransactionPasswordInput(): Promise<string> {
    return await this.userTransactionPasswordInput.getAttribute('value');
  }

  async setUserTransactionPasswordSaltInput(userTransactionPasswordSalt: string): Promise<void> {
    await this.userTransactionPasswordSaltInput.sendKeys(userTransactionPasswordSalt);
  }

  async getUserTransactionPasswordSaltInput(): Promise<string> {
    return await this.userTransactionPasswordSaltInput.getAttribute('value');
  }

  async setCreatedByInput(createdBy: string): Promise<void> {
    await this.createdByInput.sendKeys(createdBy);
  }

  async getCreatedByInput(): Promise<string> {
    return await this.createdByInput.getAttribute('value');
  }

  async setCreatedOnInput(createdOn: string): Promise<void> {
    await this.createdOnInput.sendKeys(createdOn);
  }

  async getCreatedOnInput(): Promise<string> {
    return await this.createdOnInput.getAttribute('value');
  }

  async setLastUpdatedByInput(lastUpdatedBy: string): Promise<void> {
    await this.lastUpdatedByInput.sendKeys(lastUpdatedBy);
  }

  async getLastUpdatedByInput(): Promise<string> {
    return await this.lastUpdatedByInput.getAttribute('value');
  }

  async setLastUpdatedOnInput(lastUpdatedOn: string): Promise<void> {
    await this.lastUpdatedOnInput.sendKeys(lastUpdatedOn);
  }

  async getLastUpdatedOnInput(): Promise<string> {
    return await this.lastUpdatedOnInput.getAttribute('value');
  }

  async setVerifiedByInput(verifiedBy: string): Promise<void> {
    await this.verifiedByInput.sendKeys(verifiedBy);
  }

  async getVerifiedByInput(): Promise<string> {
    return await this.verifiedByInput.getAttribute('value');
  }

  async setVerifiedOnInput(verifiedOn: string): Promise<void> {
    await this.verifiedOnInput.sendKeys(verifiedOn);
  }

  async getVerifiedOnInput(): Promise<string> {
    return await this.verifiedOnInput.getAttribute('value');
  }

  async setRemarksInput(remarks: string): Promise<void> {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput(): Promise<string> {
    return await this.remarksInput.getAttribute('value');
  }

  async setUserIdCodeInput(userIdCode: string): Promise<void> {
    await this.userIdCodeInput.sendKeys(userIdCode);
  }

  async getUserIdCodeInput(): Promise<string> {
    return await this.userIdCodeInput.getAttribute('value');
  }

  async setStatusInput(status: string): Promise<void> {
    await this.statusInput.sendKeys(status);
  }

  async getStatusInput(): Promise<string> {
    return await this.statusInput.getAttribute('value');
  }

  async setUsernameInput(username: string): Promise<void> {
    await this.usernameInput.sendKeys(username);
  }

  async getUsernameInput(): Promise<string> {
    return await this.usernameInput.getAttribute('value');
  }

  async setUserPINInput(userPIN: string): Promise<void> {
    await this.userPINInput.sendKeys(userPIN);
  }

  async getUserPINInput(): Promise<string> {
    return await this.userPINInput.getAttribute('value');
  }

  async setOrganisationUnitInput(organisationUnit: string): Promise<void> {
    await this.organisationUnitInput.sendKeys(organisationUnit);
  }

  async getOrganisationUnitInput(): Promise<string> {
    return await this.organisationUnitInput.getAttribute('value');
  }

  async setAddressInput(address: string): Promise<void> {
    await this.addressInput.sendKeys(address);
  }

  async getAddressInput(): Promise<string> {
    return await this.addressInput.getAttribute('value');
  }

  async setCounrtyInput(counrty: string): Promise<void> {
    await this.counrtyInput.sendKeys(counrty);
  }

  async getCounrtyInput(): Promise<string> {
    return await this.counrtyInput.getAttribute('value');
  }

  async setPostalCodeInput(postalCode: string): Promise<void> {
    await this.postalCodeInput.sendKeys(postalCode);
  }

  async getPostalCodeInput(): Promise<string> {
    return await this.postalCodeInput.getAttribute('value');
  }

  async setPhotographInput(photograph: string): Promise<void> {
    await this.photographInput.sendKeys(photograph);
  }

  async getPhotographInput(): Promise<string> {
    return await this.photographInput.getAttribute('value');
  }

  async setDobInput(dob: string): Promise<void> {
    await this.dobInput.sendKeys(dob);
  }

  async getDobInput(): Promise<string> {
    return await this.dobInput.getAttribute('value');
  }

  async setGenderInput(gender: string): Promise<void> {
    await this.genderInput.sendKeys(gender);
  }

  async getGenderInput(): Promise<string> {
    return await this.genderInput.getAttribute('value');
  }

  async setPanInput(pan: string): Promise<void> {
    await this.panInput.sendKeys(pan);
  }

  async getPanInput(): Promise<string> {
    return await this.panInput.getAttribute('value');
  }

  async setAadhaarInput(aadhaar: string): Promise<void> {
    await this.aadhaarInput.sendKeys(aadhaar);
  }

  async getAadhaarInput(): Promise<string> {
    return await this.aadhaarInput.getAttribute('value');
  }

  async setPanAttachmentInput(panAttachment: string): Promise<void> {
    await this.panAttachmentInput.sendKeys(panAttachment);
  }

  async getPanAttachmentInput(): Promise<string> {
    return await this.panAttachmentInput.getAttribute('value');
  }

  async setEmployeeIdInput(employeeId: string): Promise<void> {
    await this.employeeIdInput.sendKeys(employeeId);
  }

  async getEmployeeIdInput(): Promise<string> {
    return await this.employeeIdInput.getAttribute('value');
  }

  async setEmployeeCardAttachmentInput(employeeCardAttachment: string): Promise<void> {
    await this.employeeCardAttachmentInput.sendKeys(employeeCardAttachment);
  }

  async getEmployeeCardAttachmentInput(): Promise<string> {
    return await this.employeeCardAttachmentInput.getAttribute('value');
  }

  async setSignerIdInput(signerId: string): Promise<void> {
    await this.signerIdInput.sendKeys(signerId);
  }

  async getSignerIdInput(): Promise<string> {
    return await this.signerIdInput.getAttribute('value');
  }

  async setLastActionInput(lastAction: string): Promise<void> {
    await this.lastActionInput.sendKeys(lastAction);
  }

  async getLastActionInput(): Promise<string> {
    return await this.lastActionInput.getAttribute('value');
  }

  async setAccountStatusInput(accountStatus: string): Promise<void> {
    await this.accountStatusInput.sendKeys(accountStatus);
  }

  async getAccountStatusInput(): Promise<string> {
    return await this.accountStatusInput.getAttribute('value');
  }

  async designationMasterSelectLastOption(): Promise<void> {
    await this.designationMasterSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async designationMasterSelectOption(option: string): Promise<void> {
    await this.designationMasterSelect.sendKeys(option);
  }

  getDesignationMasterSelect(): ElementFinder {
    return this.designationMasterSelect;
  }

  async getDesignationMasterSelectedOption(): Promise<string> {
    return await this.designationMasterSelect.element(by.css('option:checked')).getText();
  }

  async organisationMasterSelectLastOption(): Promise<void> {
    await this.organisationMasterSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async organisationMasterSelectOption(option: string): Promise<void> {
    await this.organisationMasterSelect.sendKeys(option);
  }

  getOrganisationMasterSelect(): ElementFinder {
    return this.organisationMasterSelect;
  }

  async getOrganisationMasterSelectedOption(): Promise<string> {
    return await this.organisationMasterSelect.element(by.css('option:checked')).getText();
  }

  async esignRoleSelectLastOption(): Promise<void> {
    await this.esignRoleSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async esignRoleSelectOption(option: string): Promise<void> {
    await this.esignRoleSelect.sendKeys(option);
  }

  getEsignRoleSelect(): ElementFinder {
    return this.esignRoleSelect;
  }

  async getEsignRoleSelectedOption(): Promise<string> {
    return await this.esignRoleSelect.element(by.css('option:checked')).getText();
  }

  async applicationMasterSelectLastOption(): Promise<void> {
    await this.applicationMasterSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async applicationMasterSelectOption(option: string): Promise<void> {
    await this.applicationMasterSelect.sendKeys(option);
  }

  getApplicationMasterSelect(): ElementFinder {
    return this.applicationMasterSelect;
  }

  async getApplicationMasterSelectedOption(): Promise<string> {
    return await this.applicationMasterSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class UserDetailsDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-userDetails-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-userDetails'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
