import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { UserDetailsComponentsPage, UserDetailsDeleteDialog, UserDetailsUpdatePage } from './user-details.page-object';

const expect = chai.expect;

describe('UserDetails e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let userDetailsComponentsPage: UserDetailsComponentsPage;
  let userDetailsUpdatePage: UserDetailsUpdatePage;
  let userDetailsDeleteDialog: UserDetailsDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load UserDetails', async () => {
    await navBarPage.goToEntity('user-details');
    userDetailsComponentsPage = new UserDetailsComponentsPage();
    await browser.wait(ec.visibilityOf(userDetailsComponentsPage.title), 5000);
    expect(await userDetailsComponentsPage.getTitle()).to.eq('User Details');
    await browser.wait(
      ec.or(ec.visibilityOf(userDetailsComponentsPage.entities), ec.visibilityOf(userDetailsComponentsPage.noResult)),
      1000
    );
  });

  it('should load create UserDetails page', async () => {
    await userDetailsComponentsPage.clickOnCreateButton();
    userDetailsUpdatePage = new UserDetailsUpdatePage();
    expect(await userDetailsUpdatePage.getPageTitle()).to.eq('Create or edit a User Details');
    await userDetailsUpdatePage.cancel();
  });

  it('should create and save UserDetails', async () => {
    const nbButtonsBeforeCreate = await userDetailsComponentsPage.countDeleteButtons();

    await userDetailsComponentsPage.clickOnCreateButton();

    await promise.all([
      userDetailsUpdatePage.setUserFullNameInput('userFullName'),
      userDetailsUpdatePage.setUserPhoneMobileInput('userPhoneMobile'),
      userDetailsUpdatePage.setUserPhoneLandlineInput('userPhoneLandline'),
      userDetailsUpdatePage.setUserOfficialEmailInput('userOfficialEmail'),
      userDetailsUpdatePage.setUserAlternativeEmailInput('userAlternativeEmail'),
      userDetailsUpdatePage.setUserLoginPasswordInput('userLoginPassword'),
      userDetailsUpdatePage.setUserLoginPasswordSaltInput('userLoginPasswordSalt'),
      userDetailsUpdatePage.setUserTransactionPasswordInput('userTransactionPassword'),
      userDetailsUpdatePage.setUserTransactionPasswordSaltInput('userTransactionPasswordSalt'),
      userDetailsUpdatePage.setCreatedByInput('createdBy'),
      userDetailsUpdatePage.setCreatedOnInput('2000-12-31'),
      userDetailsUpdatePage.setLastUpdatedByInput('lastUpdatedBy'),
      userDetailsUpdatePage.setLastUpdatedOnInput('2000-12-31'),
      userDetailsUpdatePage.setVerifiedByInput('verifiedBy'),
      userDetailsUpdatePage.setVerifiedOnInput('2000-12-31'),
      userDetailsUpdatePage.setRemarksInput('remarks'),
      userDetailsUpdatePage.setUserIdCodeInput('userIdCode'),
      userDetailsUpdatePage.setStatusInput('status'),
      userDetailsUpdatePage.setUsernameInput('username'),
      userDetailsUpdatePage.setUserPINInput('userPIN'),
      userDetailsUpdatePage.setOrganisationUnitInput('organisationUnit'),
      userDetailsUpdatePage.setAddressInput('address'),
      userDetailsUpdatePage.setCounrtyInput('counrty'),
      userDetailsUpdatePage.setPostalCodeInput('postalCode'),
      userDetailsUpdatePage.setPhotographInput('photograph'),
      userDetailsUpdatePage.setDobInput('2000-12-31'),
      userDetailsUpdatePage.setGenderInput('gender'),
      userDetailsUpdatePage.setPanInput('pan'),
      userDetailsUpdatePage.setAadhaarInput('aadhaar'),
      userDetailsUpdatePage.setPanAttachmentInput('panAttachment'),
      userDetailsUpdatePage.setEmployeeIdInput('employeeId'),
      userDetailsUpdatePage.setEmployeeCardAttachmentInput('employeeCardAttachment'),
      userDetailsUpdatePage.setSignerIdInput('signerId'),
      userDetailsUpdatePage.setLastActionInput('lastAction'),
      userDetailsUpdatePage.setAccountStatusInput('accountStatus'),
      userDetailsUpdatePage.designationMasterSelectLastOption(),
      userDetailsUpdatePage.organisationMasterSelectLastOption(),
      userDetailsUpdatePage.esignRoleSelectLastOption(),
      userDetailsUpdatePage.applicationMasterSelectLastOption()
    ]);

    expect(await userDetailsUpdatePage.getUserFullNameInput()).to.eq(
      'userFullName',
      'Expected UserFullName value to be equals to userFullName'
    );
    expect(await userDetailsUpdatePage.getUserPhoneMobileInput()).to.eq(
      'userPhoneMobile',
      'Expected UserPhoneMobile value to be equals to userPhoneMobile'
    );
    expect(await userDetailsUpdatePage.getUserPhoneLandlineInput()).to.eq(
      'userPhoneLandline',
      'Expected UserPhoneLandline value to be equals to userPhoneLandline'
    );
    expect(await userDetailsUpdatePage.getUserOfficialEmailInput()).to.eq(
      'userOfficialEmail',
      'Expected UserOfficialEmail value to be equals to userOfficialEmail'
    );
    expect(await userDetailsUpdatePage.getUserAlternativeEmailInput()).to.eq(
      'userAlternativeEmail',
      'Expected UserAlternativeEmail value to be equals to userAlternativeEmail'
    );
    expect(await userDetailsUpdatePage.getUserLoginPasswordInput()).to.eq(
      'userLoginPassword',
      'Expected UserLoginPassword value to be equals to userLoginPassword'
    );
    expect(await userDetailsUpdatePage.getUserLoginPasswordSaltInput()).to.eq(
      'userLoginPasswordSalt',
      'Expected UserLoginPasswordSalt value to be equals to userLoginPasswordSalt'
    );
    expect(await userDetailsUpdatePage.getUserTransactionPasswordInput()).to.eq(
      'userTransactionPassword',
      'Expected UserTransactionPassword value to be equals to userTransactionPassword'
    );
    expect(await userDetailsUpdatePage.getUserTransactionPasswordSaltInput()).to.eq(
      'userTransactionPasswordSalt',
      'Expected UserTransactionPasswordSalt value to be equals to userTransactionPasswordSalt'
    );
    expect(await userDetailsUpdatePage.getCreatedByInput()).to.eq('createdBy', 'Expected CreatedBy value to be equals to createdBy');
    expect(await userDetailsUpdatePage.getCreatedOnInput()).to.eq('2000-12-31', 'Expected createdOn value to be equals to 2000-12-31');
    expect(await userDetailsUpdatePage.getLastUpdatedByInput()).to.eq(
      'lastUpdatedBy',
      'Expected LastUpdatedBy value to be equals to lastUpdatedBy'
    );
    expect(await userDetailsUpdatePage.getLastUpdatedOnInput()).to.eq(
      '2000-12-31',
      'Expected lastUpdatedOn value to be equals to 2000-12-31'
    );
    expect(await userDetailsUpdatePage.getVerifiedByInput()).to.eq('verifiedBy', 'Expected VerifiedBy value to be equals to verifiedBy');
    expect(await userDetailsUpdatePage.getVerifiedOnInput()).to.eq('2000-12-31', 'Expected verifiedOn value to be equals to 2000-12-31');
    expect(await userDetailsUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
    expect(await userDetailsUpdatePage.getUserIdCodeInput()).to.eq('userIdCode', 'Expected UserIdCode value to be equals to userIdCode');
    expect(await userDetailsUpdatePage.getStatusInput()).to.eq('status', 'Expected Status value to be equals to status');
    expect(await userDetailsUpdatePage.getUsernameInput()).to.eq('username', 'Expected Username value to be equals to username');
    expect(await userDetailsUpdatePage.getUserPINInput()).to.eq('userPIN', 'Expected UserPIN value to be equals to userPIN');
    expect(await userDetailsUpdatePage.getOrganisationUnitInput()).to.eq(
      'organisationUnit',
      'Expected OrganisationUnit value to be equals to organisationUnit'
    );
    expect(await userDetailsUpdatePage.getAddressInput()).to.eq('address', 'Expected Address value to be equals to address');
    expect(await userDetailsUpdatePage.getCounrtyInput()).to.eq('counrty', 'Expected Counrty value to be equals to counrty');
    expect(await userDetailsUpdatePage.getPostalCodeInput()).to.eq('postalCode', 'Expected PostalCode value to be equals to postalCode');
    expect(await userDetailsUpdatePage.getPhotographInput()).to.eq('photograph', 'Expected Photograph value to be equals to photograph');
    expect(await userDetailsUpdatePage.getDobInput()).to.eq('2000-12-31', 'Expected dob value to be equals to 2000-12-31');
    expect(await userDetailsUpdatePage.getGenderInput()).to.eq('gender', 'Expected Gender value to be equals to gender');
    expect(await userDetailsUpdatePage.getPanInput()).to.eq('pan', 'Expected Pan value to be equals to pan');
    expect(await userDetailsUpdatePage.getAadhaarInput()).to.eq('aadhaar', 'Expected Aadhaar value to be equals to aadhaar');
    expect(await userDetailsUpdatePage.getPanAttachmentInput()).to.eq(
      'panAttachment',
      'Expected PanAttachment value to be equals to panAttachment'
    );
    expect(await userDetailsUpdatePage.getEmployeeIdInput()).to.eq('employeeId', 'Expected EmployeeId value to be equals to employeeId');
    expect(await userDetailsUpdatePage.getEmployeeCardAttachmentInput()).to.eq(
      'employeeCardAttachment',
      'Expected EmployeeCardAttachment value to be equals to employeeCardAttachment'
    );
    expect(await userDetailsUpdatePage.getSignerIdInput()).to.eq('signerId', 'Expected SignerId value to be equals to signerId');
    expect(await userDetailsUpdatePage.getLastActionInput()).to.eq('lastAction', 'Expected LastAction value to be equals to lastAction');
    expect(await userDetailsUpdatePage.getAccountStatusInput()).to.eq(
      'accountStatus',
      'Expected AccountStatus value to be equals to accountStatus'
    );

    await userDetailsUpdatePage.save();
    expect(await userDetailsUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await userDetailsComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last UserDetails', async () => {
    const nbButtonsBeforeDelete = await userDetailsComponentsPage.countDeleteButtons();
    await userDetailsComponentsPage.clickOnLastDeleteButton();

    userDetailsDeleteDialog = new UserDetailsDeleteDialog();
    expect(await userDetailsDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this User Details?');
    await userDetailsDeleteDialog.clickOnConfirmButton();

    expect(await userDetailsComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
