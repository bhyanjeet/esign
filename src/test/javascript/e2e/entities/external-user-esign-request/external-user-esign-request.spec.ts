import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  ExternalUserEsignRequestComponentsPage,
  ExternalUserEsignRequestDeleteDialog,
  ExternalUserEsignRequestUpdatePage
} from './external-user-esign-request.page-object';

const expect = chai.expect;

describe('ExternalUserEsignRequest e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let externalUserEsignRequestComponentsPage: ExternalUserEsignRequestComponentsPage;
  let externalUserEsignRequestUpdatePage: ExternalUserEsignRequestUpdatePage;
  let externalUserEsignRequestDeleteDialog: ExternalUserEsignRequestDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load ExternalUserEsignRequests', async () => {
    await navBarPage.goToEntity('external-user-esign-request');
    externalUserEsignRequestComponentsPage = new ExternalUserEsignRequestComponentsPage();
    await browser.wait(ec.visibilityOf(externalUserEsignRequestComponentsPage.title), 5000);
    expect(await externalUserEsignRequestComponentsPage.getTitle()).to.eq('External User Esign Requests');
    await browser.wait(
      ec.or(
        ec.visibilityOf(externalUserEsignRequestComponentsPage.entities),
        ec.visibilityOf(externalUserEsignRequestComponentsPage.noResult)
      ),
      1000
    );
  });

  it('should load create ExternalUserEsignRequest page', async () => {
    await externalUserEsignRequestComponentsPage.clickOnCreateButton();
    externalUserEsignRequestUpdatePage = new ExternalUserEsignRequestUpdatePage();
    expect(await externalUserEsignRequestUpdatePage.getPageTitle()).to.eq('Create or edit a External User Esign Request');
    await externalUserEsignRequestUpdatePage.cancel();
  });

  it('should create and save ExternalUserEsignRequests', async () => {
    const nbButtonsBeforeCreate = await externalUserEsignRequestComponentsPage.countDeleteButtons();

    await externalUserEsignRequestComponentsPage.clickOnCreateButton();

    await promise.all([
      externalUserEsignRequestUpdatePage.setUserCodeIdInput('userCodeId'),
      externalUserEsignRequestUpdatePage.setApplicationIdCodeInput('applicationIdCode'),
      externalUserEsignRequestUpdatePage.setResponseUrlInput('responseUrl'),
      externalUserEsignRequestUpdatePage.setRedirectUrlInput('redirectUrl'),
      externalUserEsignRequestUpdatePage.setTsInput('ts'),
      externalUserEsignRequestUpdatePage.setSignerIdInput('signerId'),
      externalUserEsignRequestUpdatePage.setDocInfoInput('docInfo'),
      externalUserEsignRequestUpdatePage.setDocUrlInput('docUrl'),
      externalUserEsignRequestUpdatePage.setDocHashInput('docHash'),
      externalUserEsignRequestUpdatePage.setRequestStatusInput('requestStatus'),
      externalUserEsignRequestUpdatePage.setRequestTimeInput('requestTime'),
      externalUserEsignRequestUpdatePage.setTxnInput('txn'),
      externalUserEsignRequestUpdatePage.setTxnRefInput('txnRef')
    ]);

    expect(await externalUserEsignRequestUpdatePage.getUserCodeIdInput()).to.eq(
      'userCodeId',
      'Expected UserCodeId value to be equals to userCodeId'
    );
    expect(await externalUserEsignRequestUpdatePage.getApplicationIdCodeInput()).to.eq(
      'applicationIdCode',
      'Expected ApplicationIdCode value to be equals to applicationIdCode'
    );
    expect(await externalUserEsignRequestUpdatePage.getResponseUrlInput()).to.eq(
      'responseUrl',
      'Expected ResponseUrl value to be equals to responseUrl'
    );
    expect(await externalUserEsignRequestUpdatePage.getRedirectUrlInput()).to.eq(
      'redirectUrl',
      'Expected RedirectUrl value to be equals to redirectUrl'
    );
    expect(await externalUserEsignRequestUpdatePage.getTsInput()).to.eq('ts', 'Expected Ts value to be equals to ts');
    expect(await externalUserEsignRequestUpdatePage.getSignerIdInput()).to.eq(
      'signerId',
      'Expected SignerId value to be equals to signerId'
    );
    expect(await externalUserEsignRequestUpdatePage.getDocInfoInput()).to.eq('docInfo', 'Expected DocInfo value to be equals to docInfo');
    expect(await externalUserEsignRequestUpdatePage.getDocUrlInput()).to.eq('docUrl', 'Expected DocUrl value to be equals to docUrl');
    expect(await externalUserEsignRequestUpdatePage.getDocHashInput()).to.eq('docHash', 'Expected DocHash value to be equals to docHash');
    expect(await externalUserEsignRequestUpdatePage.getRequestStatusInput()).to.eq(
      'requestStatus',
      'Expected RequestStatus value to be equals to requestStatus'
    );
    expect(await externalUserEsignRequestUpdatePage.getRequestTimeInput()).to.eq(
      'requestTime',
      'Expected RequestTime value to be equals to requestTime'
    );
    expect(await externalUserEsignRequestUpdatePage.getTxnInput()).to.eq('txn', 'Expected Txn value to be equals to txn');
    expect(await externalUserEsignRequestUpdatePage.getTxnRefInput()).to.eq('txnRef', 'Expected TxnRef value to be equals to txnRef');

    await externalUserEsignRequestUpdatePage.save();
    expect(await externalUserEsignRequestUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await externalUserEsignRequestComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last ExternalUserEsignRequest', async () => {
    const nbButtonsBeforeDelete = await externalUserEsignRequestComponentsPage.countDeleteButtons();
    await externalUserEsignRequestComponentsPage.clickOnLastDeleteButton();

    externalUserEsignRequestDeleteDialog = new ExternalUserEsignRequestDeleteDialog();
    expect(await externalUserEsignRequestDeleteDialog.getDialogTitle()).to.eq(
      'Are you sure you want to delete this External User Esign Request?'
    );
    await externalUserEsignRequestDeleteDialog.clickOnConfirmButton();

    expect(await externalUserEsignRequestComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
