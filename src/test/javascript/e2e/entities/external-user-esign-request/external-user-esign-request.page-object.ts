import { element, by, ElementFinder } from 'protractor';

export class ExternalUserEsignRequestComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-external-user-esign-request div table .btn-danger'));
  title = element.all(by.css('jhi-external-user-esign-request div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getText();
  }
}

export class ExternalUserEsignRequestUpdatePage {
  pageTitle = element(by.id('jhi-external-user-esign-request-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  userCodeIdInput = element(by.id('field_userCodeId'));
  applicationIdCodeInput = element(by.id('field_applicationIdCode'));
  responseUrlInput = element(by.id('field_responseUrl'));
  redirectUrlInput = element(by.id('field_redirectUrl'));
  tsInput = element(by.id('field_ts'));
  signerIdInput = element(by.id('field_signerId'));
  docInfoInput = element(by.id('field_docInfo'));
  docUrlInput = element(by.id('field_docUrl'));
  docHashInput = element(by.id('field_docHash'));
  requestStatusInput = element(by.id('field_requestStatus'));
  requestTimeInput = element(by.id('field_requestTime'));
  txnInput = element(by.id('field_txn'));
  txnRefInput = element(by.id('field_txnRef'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }

  async setUserCodeIdInput(userCodeId: string): Promise<void> {
    await this.userCodeIdInput.sendKeys(userCodeId);
  }

  async getUserCodeIdInput(): Promise<string> {
    return await this.userCodeIdInput.getAttribute('value');
  }

  async setApplicationIdCodeInput(applicationIdCode: string): Promise<void> {
    await this.applicationIdCodeInput.sendKeys(applicationIdCode);
  }

  async getApplicationIdCodeInput(): Promise<string> {
    return await this.applicationIdCodeInput.getAttribute('value');
  }

  async setResponseUrlInput(responseUrl: string): Promise<void> {
    await this.responseUrlInput.sendKeys(responseUrl);
  }

  async getResponseUrlInput(): Promise<string> {
    return await this.responseUrlInput.getAttribute('value');
  }

  async setRedirectUrlInput(redirectUrl: string): Promise<void> {
    await this.redirectUrlInput.sendKeys(redirectUrl);
  }

  async getRedirectUrlInput(): Promise<string> {
    return await this.redirectUrlInput.getAttribute('value');
  }

  async setTsInput(ts: string): Promise<void> {
    await this.tsInput.sendKeys(ts);
  }

  async getTsInput(): Promise<string> {
    return await this.tsInput.getAttribute('value');
  }

  async setSignerIdInput(signerId: string): Promise<void> {
    await this.signerIdInput.sendKeys(signerId);
  }

  async getSignerIdInput(): Promise<string> {
    return await this.signerIdInput.getAttribute('value');
  }

  async setDocInfoInput(docInfo: string): Promise<void> {
    await this.docInfoInput.sendKeys(docInfo);
  }

  async getDocInfoInput(): Promise<string> {
    return await this.docInfoInput.getAttribute('value');
  }

  async setDocUrlInput(docUrl: string): Promise<void> {
    await this.docUrlInput.sendKeys(docUrl);
  }

  async getDocUrlInput(): Promise<string> {
    return await this.docUrlInput.getAttribute('value');
  }

  async setDocHashInput(docHash: string): Promise<void> {
    await this.docHashInput.sendKeys(docHash);
  }

  async getDocHashInput(): Promise<string> {
    return await this.docHashInput.getAttribute('value');
  }

  async setRequestStatusInput(requestStatus: string): Promise<void> {
    await this.requestStatusInput.sendKeys(requestStatus);
  }

  async getRequestStatusInput(): Promise<string> {
    return await this.requestStatusInput.getAttribute('value');
  }

  async setRequestTimeInput(requestTime: string): Promise<void> {
    await this.requestTimeInput.sendKeys(requestTime);
  }

  async getRequestTimeInput(): Promise<string> {
    return await this.requestTimeInput.getAttribute('value');
  }

  async setTxnInput(txn: string): Promise<void> {
    await this.txnInput.sendKeys(txn);
  }

  async getTxnInput(): Promise<string> {
    return await this.txnInput.getAttribute('value');
  }

  async setTxnRefInput(txnRef: string): Promise<void> {
    await this.txnRefInput.sendKeys(txnRef);
  }

  async getTxnRefInput(): Promise<string> {
    return await this.txnRefInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ExternalUserEsignRequestDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-externalUserEsignRequest-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-externalUserEsignRequest'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
