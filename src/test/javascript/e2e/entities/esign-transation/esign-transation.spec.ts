import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { EsignTransationComponentsPage, EsignTransationDeleteDialog, EsignTransationUpdatePage } from './esign-transation.page-object';

const expect = chai.expect;

describe('EsignTransation e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let esignTransationComponentsPage: EsignTransationComponentsPage;
  let esignTransationUpdatePage: EsignTransationUpdatePage;
  let esignTransationDeleteDialog: EsignTransationDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load EsignTransations', async () => {
    await navBarPage.goToEntity('esign-transation');
    esignTransationComponentsPage = new EsignTransationComponentsPage();
    await browser.wait(ec.visibilityOf(esignTransationComponentsPage.title), 5000);
    expect(await esignTransationComponentsPage.getTitle()).to.eq('Esign Transations');
  });

  it('should load create EsignTransation page', async () => {
    await esignTransationComponentsPage.clickOnCreateButton();
    esignTransationUpdatePage = new EsignTransationUpdatePage();
    expect(await esignTransationUpdatePage.getPageTitle()).to.eq('Create or edit a Esign Transation');
    await esignTransationUpdatePage.cancel();
  });

  it('should create and save EsignTransations', async () => {
    const nbButtonsBeforeCreate = await esignTransationComponentsPage.countDeleteButtons();

    await esignTransationComponentsPage.clickOnCreateButton();
    await promise.all([
      esignTransationUpdatePage.setTxnIdInput('txnId'),
      esignTransationUpdatePage.setUserCodeIdInput('userCodeId'),
      esignTransationUpdatePage.setApplicationCodeIdInput('applicationCodeId'),
      esignTransationUpdatePage.setOrgResponseSuccessURLInput('orgResponseSuccessURL'),
      esignTransationUpdatePage.setOrgResponseFailURLInput('orgResponseFailURL'),
      esignTransationUpdatePage.setOrgStatusInput('orgStatus'),
      esignTransationUpdatePage.setRequestDateInput('2000-12-31'),
      esignTransationUpdatePage.setOrganisationIdCodeInput('organisationIdCode'),
      esignTransationUpdatePage.setSignerIdInput('signerId'),
      esignTransationUpdatePage.setRequestTypeInput('requestType'),
      esignTransationUpdatePage.setRequestStatusInput('requestStatus'),
      esignTransationUpdatePage.setResponseTypeInput('responseType'),
      esignTransationUpdatePage.setResponseStatusInput('responseStatus'),
      esignTransationUpdatePage.setResponseCodeInput('responseCode'),
      esignTransationUpdatePage.setRequestXmlInput('requestXml'),
      esignTransationUpdatePage.setResponseXmlInput('responseXml'),
      esignTransationUpdatePage.setResponseDateInput('2000-12-31'),
      esignTransationUpdatePage.setTsInput('ts')
    ]);
    expect(await esignTransationUpdatePage.getTxnIdInput()).to.eq('txnId', 'Expected TxnId value to be equals to txnId');
    expect(await esignTransationUpdatePage.getUserCodeIdInput()).to.eq(
      'userCodeId',
      'Expected UserCodeId value to be equals to userCodeId'
    );
    expect(await esignTransationUpdatePage.getApplicationCodeIdInput()).to.eq(
      'applicationCodeId',
      'Expected ApplicationCodeId value to be equals to applicationCodeId'
    );
    expect(await esignTransationUpdatePage.getOrgResponseSuccessURLInput()).to.eq(
      'orgResponseSuccessURL',
      'Expected OrgResponseSuccessURL value to be equals to orgResponseSuccessURL'
    );
    expect(await esignTransationUpdatePage.getOrgResponseFailURLInput()).to.eq(
      'orgResponseFailURL',
      'Expected OrgResponseFailURL value to be equals to orgResponseFailURL'
    );
    expect(await esignTransationUpdatePage.getOrgStatusInput()).to.eq('orgStatus', 'Expected OrgStatus value to be equals to orgStatus');
    expect(await esignTransationUpdatePage.getRequestDateInput()).to.eq(
      '2000-12-31',
      'Expected requestDate value to be equals to 2000-12-31'
    );
    expect(await esignTransationUpdatePage.getOrganisationIdCodeInput()).to.eq(
      'organisationIdCode',
      'Expected OrganisationIdCode value to be equals to organisationIdCode'
    );
    expect(await esignTransationUpdatePage.getSignerIdInput()).to.eq('signerId', 'Expected SignerId value to be equals to signerId');
    expect(await esignTransationUpdatePage.getRequestTypeInput()).to.eq(
      'requestType',
      'Expected RequestType value to be equals to requestType'
    );
    expect(await esignTransationUpdatePage.getRequestStatusInput()).to.eq(
      'requestStatus',
      'Expected RequestStatus value to be equals to requestStatus'
    );
    expect(await esignTransationUpdatePage.getResponseTypeInput()).to.eq(
      'responseType',
      'Expected ResponseType value to be equals to responseType'
    );
    expect(await esignTransationUpdatePage.getResponseStatusInput()).to.eq(
      'responseStatus',
      'Expected ResponseStatus value to be equals to responseStatus'
    );
    expect(await esignTransationUpdatePage.getResponseCodeInput()).to.eq(
      'responseCode',
      'Expected ResponseCode value to be equals to responseCode'
    );
    expect(await esignTransationUpdatePage.getRequestXmlInput()).to.eq(
      'requestXml',
      'Expected RequestXml value to be equals to requestXml'
    );
    expect(await esignTransationUpdatePage.getResponseXmlInput()).to.eq(
      'responseXml',
      'Expected ResponseXml value to be equals to responseXml'
    );
    expect(await esignTransationUpdatePage.getResponseDateInput()).to.eq(
      '2000-12-31',
      'Expected responseDate value to be equals to 2000-12-31'
    );
    expect(await esignTransationUpdatePage.getTsInput()).to.eq('ts', 'Expected Ts value to be equals to ts');
    await esignTransationUpdatePage.save();
    expect(await esignTransationUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await esignTransationComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last EsignTransation', async () => {
    const nbButtonsBeforeDelete = await esignTransationComponentsPage.countDeleteButtons();
    await esignTransationComponentsPage.clickOnLastDeleteButton();

    esignTransationDeleteDialog = new EsignTransationDeleteDialog();
    expect(await esignTransationDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Esign Transation?');
    await esignTransationDeleteDialog.clickOnConfirmButton();

    expect(await esignTransationComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
