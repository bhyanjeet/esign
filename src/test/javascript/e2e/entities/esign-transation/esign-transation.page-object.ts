import { element, by, ElementFinder } from 'protractor';

export class EsignTransationComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-esign-transation div table .btn-danger'));
  title = element.all(by.css('jhi-esign-transation div h2#page-heading span')).first();

  async clickOnCreateButton() {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton() {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getText();
  }
}

export class EsignTransationUpdatePage {
  pageTitle = element(by.id('jhi-esign-transation-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  txnIdInput = element(by.id('field_txnId'));
  userCodeIdInput = element(by.id('field_userCodeId'));
  applicationCodeIdInput = element(by.id('field_applicationCodeId'));
  orgResponseSuccessURLInput = element(by.id('field_orgResponseSuccessURL'));
  orgResponseFailURLInput = element(by.id('field_orgResponseFailURL'));
  orgStatusInput = element(by.id('field_orgStatus'));
  requestDateInput = element(by.id('field_requestDate'));
  organisationIdCodeInput = element(by.id('field_organisationIdCode'));
  signerIdInput = element(by.id('field_signerId'));
  requestTypeInput = element(by.id('field_requestType'));
  requestStatusInput = element(by.id('field_requestStatus'));
  responseTypeInput = element(by.id('field_responseType'));
  responseStatusInput = element(by.id('field_responseStatus'));
  responseCodeInput = element(by.id('field_responseCode'));
  requestXmlInput = element(by.id('field_requestXml'));
  responseXmlInput = element(by.id('field_responseXml'));
  responseDateInput = element(by.id('field_responseDate'));
  tsInput = element(by.id('field_ts'));

  async getPageTitle() {
    return this.pageTitle.getText();
  }

  async setTxnIdInput(txnId) {
    await this.txnIdInput.sendKeys(txnId);
  }

  async getTxnIdInput() {
    return await this.txnIdInput.getAttribute('value');
  }

  async setUserCodeIdInput(userCodeId) {
    await this.userCodeIdInput.sendKeys(userCodeId);
  }

  async getUserCodeIdInput() {
    return await this.userCodeIdInput.getAttribute('value');
  }

  async setApplicationCodeIdInput(applicationCodeId) {
    await this.applicationCodeIdInput.sendKeys(applicationCodeId);
  }

  async getApplicationCodeIdInput() {
    return await this.applicationCodeIdInput.getAttribute('value');
  }

  async setOrgResponseSuccessURLInput(orgResponseSuccessURL) {
    await this.orgResponseSuccessURLInput.sendKeys(orgResponseSuccessURL);
  }

  async getOrgResponseSuccessURLInput() {
    return await this.orgResponseSuccessURLInput.getAttribute('value');
  }

  async setOrgResponseFailURLInput(orgResponseFailURL) {
    await this.orgResponseFailURLInput.sendKeys(orgResponseFailURL);
  }

  async getOrgResponseFailURLInput() {
    return await this.orgResponseFailURLInput.getAttribute('value');
  }

  async setOrgStatusInput(orgStatus) {
    await this.orgStatusInput.sendKeys(orgStatus);
  }

  async getOrgStatusInput() {
    return await this.orgStatusInput.getAttribute('value');
  }

  async setRequestDateInput(requestDate) {
    await this.requestDateInput.sendKeys(requestDate);
  }

  async getRequestDateInput() {
    return await this.requestDateInput.getAttribute('value');
  }

  async setOrganisationIdCodeInput(organisationIdCode) {
    await this.organisationIdCodeInput.sendKeys(organisationIdCode);
  }

  async getOrganisationIdCodeInput() {
    return await this.organisationIdCodeInput.getAttribute('value');
  }

  async setSignerIdInput(signerId) {
    await this.signerIdInput.sendKeys(signerId);
  }

  async getSignerIdInput() {
    return await this.signerIdInput.getAttribute('value');
  }

  async setRequestTypeInput(requestType) {
    await this.requestTypeInput.sendKeys(requestType);
  }

  async getRequestTypeInput() {
    return await this.requestTypeInput.getAttribute('value');
  }

  async setRequestStatusInput(requestStatus) {
    await this.requestStatusInput.sendKeys(requestStatus);
  }

  async getRequestStatusInput() {
    return await this.requestStatusInput.getAttribute('value');
  }

  async setResponseTypeInput(responseType) {
    await this.responseTypeInput.sendKeys(responseType);
  }

  async getResponseTypeInput() {
    return await this.responseTypeInput.getAttribute('value');
  }

  async setResponseStatusInput(responseStatus) {
    await this.responseStatusInput.sendKeys(responseStatus);
  }

  async getResponseStatusInput() {
    return await this.responseStatusInput.getAttribute('value');
  }

  async setResponseCodeInput(responseCode) {
    await this.responseCodeInput.sendKeys(responseCode);
  }

  async getResponseCodeInput() {
    return await this.responseCodeInput.getAttribute('value');
  }

  async setRequestXmlInput(requestXml) {
    await this.requestXmlInput.sendKeys(requestXml);
  }

  async getRequestXmlInput() {
    return await this.requestXmlInput.getAttribute('value');
  }

  async setResponseXmlInput(responseXml) {
    await this.responseXmlInput.sendKeys(responseXml);
  }

  async getResponseXmlInput() {
    return await this.responseXmlInput.getAttribute('value');
  }

  async setResponseDateInput(responseDate) {
    await this.responseDateInput.sendKeys(responseDate);
  }

  async getResponseDateInput() {
    return await this.responseDateInput.getAttribute('value');
  }

  async setTsInput(ts) {
    await this.tsInput.sendKeys(ts);
  }

  async getTsInput() {
    return await this.tsInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class EsignTransationDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-esignTransation-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-esignTransation'));

  async getDialogTitle() {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}
