import { element, by, ElementFinder } from 'protractor';

export class UserApplicationComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-user-application div table .btn-danger'));
  title = element.all(by.css('jhi-user-application div h2#page-heading span')).first();

  async clickOnCreateButton() {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton() {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getText();
  }
}

export class UserApplicationUpdatePage {
  pageTitle = element(by.id('jhi-user-application-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  userIdInput = element(by.id('field_userId'));
  userLoginInput = element(by.id('field_userLogin'));
  createdByInput = element(by.id('field_createdBy'));
  createdOnInput = element(by.id('field_createdOn'));
  updatedByInput = element(by.id('field_updatedBy'));
  updatedOnInput = element(by.id('field_updatedOn'));
  verifiedByInput = element(by.id('field_verifiedBy'));
  verifiedOnInput = element(by.id('field_verifiedOn'));
  userCodeIdInput = element(by.id('field_userCodeId'));
  applicationCodeIdInput = element(by.id('field_applicationCodeId'));
  applicationMasterSelect = element(by.id('field_applicationMaster'));

  async getPageTitle() {
    return this.pageTitle.getText();
  }

  async setUserIdInput(userId) {
    await this.userIdInput.sendKeys(userId);
  }

  async getUserIdInput() {
    return await this.userIdInput.getAttribute('value');
  }

  async setUserLoginInput(userLogin) {
    await this.userLoginInput.sendKeys(userLogin);
  }

  async getUserLoginInput() {
    return await this.userLoginInput.getAttribute('value');
  }

  async setCreatedByInput(createdBy) {
    await this.createdByInput.sendKeys(createdBy);
  }

  async getCreatedByInput() {
    return await this.createdByInput.getAttribute('value');
  }

  async setCreatedOnInput(createdOn) {
    await this.createdOnInput.sendKeys(createdOn);
  }

  async getCreatedOnInput() {
    return await this.createdOnInput.getAttribute('value');
  }

  async setUpdatedByInput(updatedBy) {
    await this.updatedByInput.sendKeys(updatedBy);
  }

  async getUpdatedByInput() {
    return await this.updatedByInput.getAttribute('value');
  }

  async setUpdatedOnInput(updatedOn) {
    await this.updatedOnInput.sendKeys(updatedOn);
  }

  async getUpdatedOnInput() {
    return await this.updatedOnInput.getAttribute('value');
  }

  async setVerifiedByInput(verifiedBy) {
    await this.verifiedByInput.sendKeys(verifiedBy);
  }

  async getVerifiedByInput() {
    return await this.verifiedByInput.getAttribute('value');
  }

  async setVerifiedOnInput(verifiedOn) {
    await this.verifiedOnInput.sendKeys(verifiedOn);
  }

  async getVerifiedOnInput() {
    return await this.verifiedOnInput.getAttribute('value');
  }

  async setUserCodeIdInput(userCodeId) {
    await this.userCodeIdInput.sendKeys(userCodeId);
  }

  async getUserCodeIdInput() {
    return await this.userCodeIdInput.getAttribute('value');
  }

  async setApplicationCodeIdInput(applicationCodeId) {
    await this.applicationCodeIdInput.sendKeys(applicationCodeId);
  }

  async getApplicationCodeIdInput() {
    return await this.applicationCodeIdInput.getAttribute('value');
  }

  async applicationMasterSelectLastOption() {
    await this.applicationMasterSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async applicationMasterSelectOption(option) {
    await this.applicationMasterSelect.sendKeys(option);
  }

  getApplicationMasterSelect(): ElementFinder {
    return this.applicationMasterSelect;
  }

  async getApplicationMasterSelectedOption() {
    return await this.applicationMasterSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class UserApplicationDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-userApplication-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-userApplication'));

  async getDialogTitle() {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}
