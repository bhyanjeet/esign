import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { UserApplicationComponentsPage, UserApplicationDeleteDialog, UserApplicationUpdatePage } from './user-application.page-object';

const expect = chai.expect;

describe('UserApplication e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let userApplicationComponentsPage: UserApplicationComponentsPage;
  let userApplicationUpdatePage: UserApplicationUpdatePage;
  let userApplicationDeleteDialog: UserApplicationDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load UserApplications', async () => {
    await navBarPage.goToEntity('user-application');
    userApplicationComponentsPage = new UserApplicationComponentsPage();
    await browser.wait(ec.visibilityOf(userApplicationComponentsPage.title), 5000);
    expect(await userApplicationComponentsPage.getTitle()).to.eq('User Applications');
  });

  it('should load create UserApplication page', async () => {
    await userApplicationComponentsPage.clickOnCreateButton();
    userApplicationUpdatePage = new UserApplicationUpdatePage();
    expect(await userApplicationUpdatePage.getPageTitle()).to.eq('Create or edit a User Application');
    await userApplicationUpdatePage.cancel();
  });

  it('should create and save UserApplications', async () => {
    const nbButtonsBeforeCreate = await userApplicationComponentsPage.countDeleteButtons();

    await userApplicationComponentsPage.clickOnCreateButton();
    await promise.all([
      userApplicationUpdatePage.setUserIdInput('5'),
      userApplicationUpdatePage.setUserLoginInput('userLogin'),
      userApplicationUpdatePage.setCreatedByInput('createdBy'),
      userApplicationUpdatePage.setCreatedOnInput('2000-12-31'),
      userApplicationUpdatePage.setUpdatedByInput('updatedBy'),
      userApplicationUpdatePage.setUpdatedOnInput('2000-12-31'),
      userApplicationUpdatePage.setVerifiedByInput('verifiedBy'),
      userApplicationUpdatePage.setVerifiedOnInput('2000-12-31'),
      userApplicationUpdatePage.setUserCodeIdInput('userCodeId'),
      userApplicationUpdatePage.setApplicationCodeIdInput('applicationCodeId'),
      userApplicationUpdatePage.applicationMasterSelectLastOption()
    ]);
    expect(await userApplicationUpdatePage.getUserIdInput()).to.eq('5', 'Expected userId value to be equals to 5');
    expect(await userApplicationUpdatePage.getUserLoginInput()).to.eq('userLogin', 'Expected UserLogin value to be equals to userLogin');
    expect(await userApplicationUpdatePage.getCreatedByInput()).to.eq('createdBy', 'Expected CreatedBy value to be equals to createdBy');
    expect(await userApplicationUpdatePage.getCreatedOnInput()).to.eq('2000-12-31', 'Expected createdOn value to be equals to 2000-12-31');
    expect(await userApplicationUpdatePage.getUpdatedByInput()).to.eq('updatedBy', 'Expected UpdatedBy value to be equals to updatedBy');
    expect(await userApplicationUpdatePage.getUpdatedOnInput()).to.eq('2000-12-31', 'Expected updatedOn value to be equals to 2000-12-31');
    expect(await userApplicationUpdatePage.getVerifiedByInput()).to.eq(
      'verifiedBy',
      'Expected VerifiedBy value to be equals to verifiedBy'
    );
    expect(await userApplicationUpdatePage.getVerifiedOnInput()).to.eq(
      '2000-12-31',
      'Expected verifiedOn value to be equals to 2000-12-31'
    );
    expect(await userApplicationUpdatePage.getUserCodeIdInput()).to.eq(
      'userCodeId',
      'Expected UserCodeId value to be equals to userCodeId'
    );
    expect(await userApplicationUpdatePage.getApplicationCodeIdInput()).to.eq(
      'applicationCodeId',
      'Expected ApplicationCodeId value to be equals to applicationCodeId'
    );
    await userApplicationUpdatePage.save();
    expect(await userApplicationUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await userApplicationComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last UserApplication', async () => {
    const nbButtonsBeforeDelete = await userApplicationComponentsPage.countDeleteButtons();
    await userApplicationComponentsPage.clickOnLastDeleteButton();

    userApplicationDeleteDialog = new UserApplicationDeleteDialog();
    expect(await userApplicationDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this User Application?');
    await userApplicationDeleteDialog.clickOnConfirmButton();

    expect(await userApplicationComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
