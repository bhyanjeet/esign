import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  SubDistrictMasterComponentsPage,
  SubDistrictMasterDeleteDialog,
  SubDistrictMasterUpdatePage
} from './sub-district-master.page-object';

const expect = chai.expect;

describe('SubDistrictMaster e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let subDistrictMasterComponentsPage: SubDistrictMasterComponentsPage;
  let subDistrictMasterUpdatePage: SubDistrictMasterUpdatePage;
  let subDistrictMasterDeleteDialog: SubDistrictMasterDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load SubDistrictMasters', async () => {
    await navBarPage.goToEntity('sub-district-master');
    subDistrictMasterComponentsPage = new SubDistrictMasterComponentsPage();
    await browser.wait(ec.visibilityOf(subDistrictMasterComponentsPage.title), 5000);
    expect(await subDistrictMasterComponentsPage.getTitle()).to.eq('Sub District Masters');
  });

  it('should load create SubDistrictMaster page', async () => {
    await subDistrictMasterComponentsPage.clickOnCreateButton();
    subDistrictMasterUpdatePage = new SubDistrictMasterUpdatePage();
    expect(await subDistrictMasterUpdatePage.getPageTitle()).to.eq('Create or edit a Sub District Master');
    await subDistrictMasterUpdatePage.cancel();
  });

  it('should create and save SubDistrictMasters', async () => {
    const nbButtonsBeforeCreate = await subDistrictMasterComponentsPage.countDeleteButtons();

    await subDistrictMasterComponentsPage.clickOnCreateButton();
    await promise.all([
      subDistrictMasterUpdatePage.setSubDistrictCodeInput('subDistrictCode'),
      subDistrictMasterUpdatePage.setSubDistrictNameInput('subDistrictName'),
      subDistrictMasterUpdatePage.setLastUpdatedByInput('lastUpdatedBy'),
      subDistrictMasterUpdatePage.setLastUpdatedOnInput('2000-12-31'),
      subDistrictMasterUpdatePage.setVerifiedByInput('verifiedBy'),
      subDistrictMasterUpdatePage.setVerifiedOnInput('2000-12-31'),
      subDistrictMasterUpdatePage.setRemarksInput('remarks'),
      subDistrictMasterUpdatePage.stateMasterSelectLastOption(),
      subDistrictMasterUpdatePage.districtMasterSelectLastOption()
    ]);
    expect(await subDistrictMasterUpdatePage.getSubDistrictCodeInput()).to.eq(
      'subDistrictCode',
      'Expected SubDistrictCode value to be equals to subDistrictCode'
    );
    expect(await subDistrictMasterUpdatePage.getSubDistrictNameInput()).to.eq(
      'subDistrictName',
      'Expected SubDistrictName value to be equals to subDistrictName'
    );
    expect(await subDistrictMasterUpdatePage.getLastUpdatedByInput()).to.eq(
      'lastUpdatedBy',
      'Expected LastUpdatedBy value to be equals to lastUpdatedBy'
    );
    expect(await subDistrictMasterUpdatePage.getLastUpdatedOnInput()).to.eq(
      '2000-12-31',
      'Expected lastUpdatedOn value to be equals to 2000-12-31'
    );
    expect(await subDistrictMasterUpdatePage.getVerifiedByInput()).to.eq(
      'verifiedBy',
      'Expected VerifiedBy value to be equals to verifiedBy'
    );
    expect(await subDistrictMasterUpdatePage.getVerifiedOnInput()).to.eq(
      '2000-12-31',
      'Expected verifiedOn value to be equals to 2000-12-31'
    );
    expect(await subDistrictMasterUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
    await subDistrictMasterUpdatePage.save();
    expect(await subDistrictMasterUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await subDistrictMasterComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last SubDistrictMaster', async () => {
    const nbButtonsBeforeDelete = await subDistrictMasterComponentsPage.countDeleteButtons();
    await subDistrictMasterComponentsPage.clickOnLastDeleteButton();

    subDistrictMasterDeleteDialog = new SubDistrictMasterDeleteDialog();
    expect(await subDistrictMasterDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Sub District Master?');
    await subDistrictMasterDeleteDialog.clickOnConfirmButton();

    expect(await subDistrictMasterComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
