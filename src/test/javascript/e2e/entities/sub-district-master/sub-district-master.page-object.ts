import { element, by, ElementFinder } from 'protractor';

export class SubDistrictMasterComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-sub-district-master div table .btn-danger'));
  title = element.all(by.css('jhi-sub-district-master div h2#page-heading span')).first();

  async clickOnCreateButton() {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton() {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getText();
  }
}

export class SubDistrictMasterUpdatePage {
  pageTitle = element(by.id('jhi-sub-district-master-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  subDistrictCodeInput = element(by.id('field_subDistrictCode'));
  subDistrictNameInput = element(by.id('field_subDistrictName'));
  lastUpdatedByInput = element(by.id('field_lastUpdatedBy'));
  lastUpdatedOnInput = element(by.id('field_lastUpdatedOn'));
  verifiedByInput = element(by.id('field_verifiedBy'));
  verifiedOnInput = element(by.id('field_verifiedOn'));
  remarksInput = element(by.id('field_remarks'));
  stateMasterSelect = element(by.id('field_stateMaster'));
  districtMasterSelect = element(by.id('field_districtMaster'));

  async getPageTitle() {
    return this.pageTitle.getText();
  }

  async setSubDistrictCodeInput(subDistrictCode) {
    await this.subDistrictCodeInput.sendKeys(subDistrictCode);
  }

  async getSubDistrictCodeInput() {
    return await this.subDistrictCodeInput.getAttribute('value');
  }

  async setSubDistrictNameInput(subDistrictName) {
    await this.subDistrictNameInput.sendKeys(subDistrictName);
  }

  async getSubDistrictNameInput() {
    return await this.subDistrictNameInput.getAttribute('value');
  }

  async setLastUpdatedByInput(lastUpdatedBy) {
    await this.lastUpdatedByInput.sendKeys(lastUpdatedBy);
  }

  async getLastUpdatedByInput() {
    return await this.lastUpdatedByInput.getAttribute('value');
  }

  async setLastUpdatedOnInput(lastUpdatedOn) {
    await this.lastUpdatedOnInput.sendKeys(lastUpdatedOn);
  }

  async getLastUpdatedOnInput() {
    return await this.lastUpdatedOnInput.getAttribute('value');
  }

  async setVerifiedByInput(verifiedBy) {
    await this.verifiedByInput.sendKeys(verifiedBy);
  }

  async getVerifiedByInput() {
    return await this.verifiedByInput.getAttribute('value');
  }

  async setVerifiedOnInput(verifiedOn) {
    await this.verifiedOnInput.sendKeys(verifiedOn);
  }

  async getVerifiedOnInput() {
    return await this.verifiedOnInput.getAttribute('value');
  }

  async setRemarksInput(remarks) {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput() {
    return await this.remarksInput.getAttribute('value');
  }

  async stateMasterSelectLastOption() {
    await this.stateMasterSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async stateMasterSelectOption(option) {
    await this.stateMasterSelect.sendKeys(option);
  }

  getStateMasterSelect(): ElementFinder {
    return this.stateMasterSelect;
  }

  async getStateMasterSelectedOption() {
    return await this.stateMasterSelect.element(by.css('option:checked')).getText();
  }

  async districtMasterSelectLastOption() {
    await this.districtMasterSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async districtMasterSelectOption(option) {
    await this.districtMasterSelect.sendKeys(option);
  }

  getDistrictMasterSelect(): ElementFinder {
    return this.districtMasterSelect;
  }

  async getDistrictMasterSelectedOption() {
    return await this.districtMasterSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class SubDistrictMasterDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-subDistrictMaster-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-subDistrictMaster'));

  async getDialogTitle() {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}
