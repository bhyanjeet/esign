import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { EsignErrorComponentsPage, EsignErrorDeleteDialog, EsignErrorUpdatePage } from './esign-error.page-object';

const expect = chai.expect;

describe('EsignError e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let esignErrorComponentsPage: EsignErrorComponentsPage;
  let esignErrorUpdatePage: EsignErrorUpdatePage;
  let esignErrorDeleteDialog: EsignErrorDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load EsignErrors', async () => {
    await navBarPage.goToEntity('esign-error');
    esignErrorComponentsPage = new EsignErrorComponentsPage();
    await browser.wait(ec.visibilityOf(esignErrorComponentsPage.title), 5000);
    expect(await esignErrorComponentsPage.getTitle()).to.eq('Esign Errors');
    await browser.wait(ec.or(ec.visibilityOf(esignErrorComponentsPage.entities), ec.visibilityOf(esignErrorComponentsPage.noResult)), 1000);
  });

  it('should load create EsignError page', async () => {
    await esignErrorComponentsPage.clickOnCreateButton();
    esignErrorUpdatePage = new EsignErrorUpdatePage();
    expect(await esignErrorUpdatePage.getPageTitle()).to.eq('Create or edit a Esign Error');
    await esignErrorUpdatePage.cancel();
  });

  it('should create and save EsignErrors', async () => {
    const nbButtonsBeforeCreate = await esignErrorComponentsPage.countDeleteButtons();

    await esignErrorComponentsPage.clickOnCreateButton();

    await promise.all([
      esignErrorUpdatePage.setErrorCodeInput('errorCode'),
      esignErrorUpdatePage.setErrorMessageInput('errorMessage'),
      esignErrorUpdatePage.setErrorStageInput('errorStage')
    ]);

    expect(await esignErrorUpdatePage.getErrorCodeInput()).to.eq('errorCode', 'Expected ErrorCode value to be equals to errorCode');
    expect(await esignErrorUpdatePage.getErrorMessageInput()).to.eq(
      'errorMessage',
      'Expected ErrorMessage value to be equals to errorMessage'
    );
    expect(await esignErrorUpdatePage.getErrorStageInput()).to.eq('errorStage', 'Expected ErrorStage value to be equals to errorStage');

    await esignErrorUpdatePage.save();
    expect(await esignErrorUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await esignErrorComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last EsignError', async () => {
    const nbButtonsBeforeDelete = await esignErrorComponentsPage.countDeleteButtons();
    await esignErrorComponentsPage.clickOnLastDeleteButton();

    esignErrorDeleteDialog = new EsignErrorDeleteDialog();
    expect(await esignErrorDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Esign Error?');
    await esignErrorDeleteDialog.clickOnConfirmButton();

    expect(await esignErrorComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
