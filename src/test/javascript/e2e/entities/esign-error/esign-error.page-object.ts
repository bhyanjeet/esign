import { element, by, ElementFinder } from 'protractor';

export class EsignErrorComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-esign-error div table .btn-danger'));
  title = element.all(by.css('jhi-esign-error div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getText();
  }
}

export class EsignErrorUpdatePage {
  pageTitle = element(by.id('jhi-esign-error-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  errorCodeInput = element(by.id('field_errorCode'));
  errorMessageInput = element(by.id('field_errorMessage'));
  errorStageInput = element(by.id('field_errorStage'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }

  async setErrorCodeInput(errorCode: string): Promise<void> {
    await this.errorCodeInput.sendKeys(errorCode);
  }

  async getErrorCodeInput(): Promise<string> {
    return await this.errorCodeInput.getAttribute('value');
  }

  async setErrorMessageInput(errorMessage: string): Promise<void> {
    await this.errorMessageInput.sendKeys(errorMessage);
  }

  async getErrorMessageInput(): Promise<string> {
    return await this.errorMessageInput.getAttribute('value');
  }

  async setErrorStageInput(errorStage: string): Promise<void> {
    await this.errorStageInput.sendKeys(errorStage);
  }

  async getErrorStageInput(): Promise<string> {
    return await this.errorStageInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class EsignErrorDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-esignError-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-esignError'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
