import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { DepartmentComponentsPage, DepartmentDeleteDialog, DepartmentUpdatePage } from './department.page-object';

const expect = chai.expect;

describe('Department e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let departmentComponentsPage: DepartmentComponentsPage;
  let departmentUpdatePage: DepartmentUpdatePage;
  let departmentDeleteDialog: DepartmentDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Departments', async () => {
    await navBarPage.goToEntity('department');
    departmentComponentsPage = new DepartmentComponentsPage();
    await browser.wait(ec.visibilityOf(departmentComponentsPage.title), 5000);
    expect(await departmentComponentsPage.getTitle()).to.eq('Departments');
    await browser.wait(ec.or(ec.visibilityOf(departmentComponentsPage.entities), ec.visibilityOf(departmentComponentsPage.noResult)), 1000);
  });

  it('should load create Department page', async () => {
    await departmentComponentsPage.clickOnCreateButton();
    departmentUpdatePage = new DepartmentUpdatePage();
    expect(await departmentUpdatePage.getPageTitle()).to.eq('Create or edit a Department');
    await departmentUpdatePage.cancel();
  });

  it('should create and save Departments', async () => {
    const nbButtonsBeforeCreate = await departmentComponentsPage.countDeleteButtons();

    await departmentComponentsPage.clickOnCreateButton();

    await promise.all([
      departmentUpdatePage.setDepartmentNameInput('departmentName'),
      departmentUpdatePage.setCreatedOnInput('2000-12-31'),
      departmentUpdatePage.setCreatedByInput('createdBy'),
      departmentUpdatePage.setUpdatedOnInput('2000-12-31'),
      departmentUpdatePage.setUpdatedByInput('updatedBy'),
      departmentUpdatePage.setStatusInput('status'),
      departmentUpdatePage.setRemarkInput('remark'),
      departmentUpdatePage.setWhiteListIp1Input('whiteListIp1'),
      departmentUpdatePage.setWhiteListIp2Input('whiteListIp2')
    ]);

    expect(await departmentUpdatePage.getDepartmentNameInput()).to.eq(
      'departmentName',
      'Expected DepartmentName value to be equals to departmentName'
    );
    expect(await departmentUpdatePage.getCreatedOnInput()).to.eq('2000-12-31', 'Expected createdOn value to be equals to 2000-12-31');
    expect(await departmentUpdatePage.getCreatedByInput()).to.eq('createdBy', 'Expected CreatedBy value to be equals to createdBy');
    expect(await departmentUpdatePage.getUpdatedOnInput()).to.eq('2000-12-31', 'Expected updatedOn value to be equals to 2000-12-31');
    expect(await departmentUpdatePage.getUpdatedByInput()).to.eq('updatedBy', 'Expected UpdatedBy value to be equals to updatedBy');
    expect(await departmentUpdatePage.getStatusInput()).to.eq('status', 'Expected Status value to be equals to status');
    expect(await departmentUpdatePage.getRemarkInput()).to.eq('remark', 'Expected Remark value to be equals to remark');
    expect(await departmentUpdatePage.getWhiteListIp1Input()).to.eq(
      'whiteListIp1',
      'Expected WhiteListIp1 value to be equals to whiteListIp1'
    );
    expect(await departmentUpdatePage.getWhiteListIp2Input()).to.eq(
      'whiteListIp2',
      'Expected WhiteListIp2 value to be equals to whiteListIp2'
    );

    await departmentUpdatePage.save();
    expect(await departmentUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await departmentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Department', async () => {
    const nbButtonsBeforeDelete = await departmentComponentsPage.countDeleteButtons();
    await departmentComponentsPage.clickOnLastDeleteButton();

    departmentDeleteDialog = new DepartmentDeleteDialog();
    expect(await departmentDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Department?');
    await departmentDeleteDialog.clickOnConfirmButton();

    expect(await departmentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
