import { element, by, ElementFinder } from 'protractor';

export class DepartmentComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-department div table .btn-danger'));
  title = element.all(by.css('jhi-department div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getText();
  }
}

export class DepartmentUpdatePage {
  pageTitle = element(by.id('jhi-department-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  departmentNameInput = element(by.id('field_departmentName'));
  createdOnInput = element(by.id('field_createdOn'));
  createdByInput = element(by.id('field_createdBy'));
  updatedOnInput = element(by.id('field_updatedOn'));
  updatedByInput = element(by.id('field_updatedBy'));
  statusInput = element(by.id('field_status'));
  remarkInput = element(by.id('field_remark'));
  whiteListIp1Input = element(by.id('field_whiteListIp1'));
  whiteListIp2Input = element(by.id('field_whiteListIp2'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }

  async setDepartmentNameInput(departmentName: string): Promise<void> {
    await this.departmentNameInput.sendKeys(departmentName);
  }

  async getDepartmentNameInput(): Promise<string> {
    return await this.departmentNameInput.getAttribute('value');
  }

  async setCreatedOnInput(createdOn: string): Promise<void> {
    await this.createdOnInput.sendKeys(createdOn);
  }

  async getCreatedOnInput(): Promise<string> {
    return await this.createdOnInput.getAttribute('value');
  }

  async setCreatedByInput(createdBy: string): Promise<void> {
    await this.createdByInput.sendKeys(createdBy);
  }

  async getCreatedByInput(): Promise<string> {
    return await this.createdByInput.getAttribute('value');
  }

  async setUpdatedOnInput(updatedOn: string): Promise<void> {
    await this.updatedOnInput.sendKeys(updatedOn);
  }

  async getUpdatedOnInput(): Promise<string> {
    return await this.updatedOnInput.getAttribute('value');
  }

  async setUpdatedByInput(updatedBy: string): Promise<void> {
    await this.updatedByInput.sendKeys(updatedBy);
  }

  async getUpdatedByInput(): Promise<string> {
    return await this.updatedByInput.getAttribute('value');
  }

  async setStatusInput(status: string): Promise<void> {
    await this.statusInput.sendKeys(status);
  }

  async getStatusInput(): Promise<string> {
    return await this.statusInput.getAttribute('value');
  }

  async setRemarkInput(remark: string): Promise<void> {
    await this.remarkInput.sendKeys(remark);
  }

  async getRemarkInput(): Promise<string> {
    return await this.remarkInput.getAttribute('value');
  }

  async setWhiteListIp1Input(whiteListIp1: string): Promise<void> {
    await this.whiteListIp1Input.sendKeys(whiteListIp1);
  }

  async getWhiteListIp1Input(): Promise<string> {
    return await this.whiteListIp1Input.getAttribute('value');
  }

  async setWhiteListIp2Input(whiteListIp2: string): Promise<void> {
    await this.whiteListIp2Input.sendKeys(whiteListIp2);
  }

  async getWhiteListIp2Input(): Promise<string> {
    return await this.whiteListIp2Input.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class DepartmentDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-department-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-department'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
