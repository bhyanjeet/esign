import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ApplicationLogsComponentsPage, ApplicationLogsDeleteDialog, ApplicationLogsUpdatePage } from './application-logs.page-object';

const expect = chai.expect;

describe('ApplicationLogs e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let applicationLogsComponentsPage: ApplicationLogsComponentsPage;
  let applicationLogsUpdatePage: ApplicationLogsUpdatePage;
  let applicationLogsDeleteDialog: ApplicationLogsDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load ApplicationLogs', async () => {
    await navBarPage.goToEntity('application-logs');
    applicationLogsComponentsPage = new ApplicationLogsComponentsPage();
    await browser.wait(ec.visibilityOf(applicationLogsComponentsPage.title), 5000);
    expect(await applicationLogsComponentsPage.getTitle()).to.eq('Application Logs');
  });

  it('should load create ApplicationLogs page', async () => {
    await applicationLogsComponentsPage.clickOnCreateButton();
    applicationLogsUpdatePage = new ApplicationLogsUpdatePage();
    expect(await applicationLogsUpdatePage.getPageTitle()).to.eq('Create or edit a Application Logs');
    await applicationLogsUpdatePage.cancel();
  });

  it('should create and save ApplicationLogs', async () => {
    const nbButtonsBeforeCreate = await applicationLogsComponentsPage.countDeleteButtons();

    await applicationLogsComponentsPage.clickOnCreateButton();
    await promise.all([
      applicationLogsUpdatePage.setActionTakenInput('actionTaken'),
      applicationLogsUpdatePage.setActionTakenByInput('actionTakenBy'),
      applicationLogsUpdatePage.setActionTakenOnApplicationInput('5'),
      applicationLogsUpdatePage.setActionTakenOnDateInput('2000-12-31'),
      applicationLogsUpdatePage.setRemarksInput('remarks')
    ]);
    expect(await applicationLogsUpdatePage.getActionTakenInput()).to.eq(
      'actionTaken',
      'Expected ActionTaken value to be equals to actionTaken'
    );
    expect(await applicationLogsUpdatePage.getActionTakenByInput()).to.eq(
      'actionTakenBy',
      'Expected ActionTakenBy value to be equals to actionTakenBy'
    );
    expect(await applicationLogsUpdatePage.getActionTakenOnApplicationInput()).to.eq(
      '5',
      'Expected actionTakenOnApplication value to be equals to 5'
    );
    expect(await applicationLogsUpdatePage.getActionTakenOnDateInput()).to.eq(
      '2000-12-31',
      'Expected actionTakenOnDate value to be equals to 2000-12-31'
    );
    expect(await applicationLogsUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
    await applicationLogsUpdatePage.save();
    expect(await applicationLogsUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await applicationLogsComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last ApplicationLogs', async () => {
    const nbButtonsBeforeDelete = await applicationLogsComponentsPage.countDeleteButtons();
    await applicationLogsComponentsPage.clickOnLastDeleteButton();

    applicationLogsDeleteDialog = new ApplicationLogsDeleteDialog();
    expect(await applicationLogsDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Application Logs?');
    await applicationLogsDeleteDialog.clickOnConfirmButton();

    expect(await applicationLogsComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
