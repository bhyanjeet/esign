import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { OrganisationLogComponentsPage, OrganisationLogDeleteDialog, OrganisationLogUpdatePage } from './organisation-log.page-object';

const expect = chai.expect;

describe('OrganisationLog e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let organisationLogComponentsPage: OrganisationLogComponentsPage;
  let organisationLogUpdatePage: OrganisationLogUpdatePage;
  let organisationLogDeleteDialog: OrganisationLogDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load OrganisationLogs', async () => {
    await navBarPage.goToEntity('organisation-log');
    organisationLogComponentsPage = new OrganisationLogComponentsPage();
    await browser.wait(ec.visibilityOf(organisationLogComponentsPage.title), 5000);
    expect(await organisationLogComponentsPage.getTitle()).to.eq('Organisation Logs');
  });

  it('should load create OrganisationLog page', async () => {
    await organisationLogComponentsPage.clickOnCreateButton();
    organisationLogUpdatePage = new OrganisationLogUpdatePage();
    expect(await organisationLogUpdatePage.getPageTitle()).to.eq('Create or edit a Organisation Log');
    await organisationLogUpdatePage.cancel();
  });

  it('should create and save OrganisationLogs', async () => {
    const nbButtonsBeforeCreate = await organisationLogComponentsPage.countDeleteButtons();

    await organisationLogComponentsPage.clickOnCreateButton();
    await promise.all([
      organisationLogUpdatePage.setOrganisationIdInput('5'),
      organisationLogUpdatePage.setCreatedByLoginInput('createdByLogin'),
      organisationLogUpdatePage.setCreatedDateInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      organisationLogUpdatePage.setVerifyByNameInput('verifyByName'),
      organisationLogUpdatePage.setVerifyDateInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      organisationLogUpdatePage.setRemarksInput('remarks'),
      organisationLogUpdatePage.setStatusInput('status'),
      organisationLogUpdatePage.setVerifyByLoginInput('verifyByLogin'),
      organisationLogUpdatePage.setCreatedByNameInput('createdByName')
    ]);
    expect(await organisationLogUpdatePage.getOrganisationIdInput()).to.eq('5', 'Expected organisationId value to be equals to 5');
    expect(await organisationLogUpdatePage.getCreatedByLoginInput()).to.eq(
      'createdByLogin',
      'Expected CreatedByLogin value to be equals to createdByLogin'
    );
    expect(await organisationLogUpdatePage.getCreatedDateInput()).to.contain(
      '2001-01-01T02:30',
      'Expected createdDate value to be equals to 2000-12-31'
    );
    expect(await organisationLogUpdatePage.getVerifyByNameInput()).to.eq(
      'verifyByName',
      'Expected VerifyByName value to be equals to verifyByName'
    );
    expect(await organisationLogUpdatePage.getVerifyDateInput()).to.contain(
      '2001-01-01T02:30',
      'Expected verifyDate value to be equals to 2000-12-31'
    );
    expect(await organisationLogUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
    expect(await organisationLogUpdatePage.getStatusInput()).to.eq('status', 'Expected Status value to be equals to status');
    expect(await organisationLogUpdatePage.getVerifyByLoginInput()).to.eq(
      'verifyByLogin',
      'Expected VerifyByLogin value to be equals to verifyByLogin'
    );
    expect(await organisationLogUpdatePage.getCreatedByNameInput()).to.eq(
      'createdByName',
      'Expected CreatedByName value to be equals to createdByName'
    );
    await organisationLogUpdatePage.save();
    expect(await organisationLogUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await organisationLogComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last OrganisationLog', async () => {
    const nbButtonsBeforeDelete = await organisationLogComponentsPage.countDeleteButtons();
    await organisationLogComponentsPage.clickOnLastDeleteButton();

    organisationLogDeleteDialog = new OrganisationLogDeleteDialog();
    expect(await organisationLogDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Organisation Log?');
    await organisationLogDeleteDialog.clickOnConfirmButton();

    expect(await organisationLogComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
