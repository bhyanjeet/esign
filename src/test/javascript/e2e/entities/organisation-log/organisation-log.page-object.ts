import { element, by, ElementFinder } from 'protractor';

export class OrganisationLogComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-organisation-log div table .btn-danger'));
  title = element.all(by.css('jhi-organisation-log div h2#page-heading span')).first();

  async clickOnCreateButton() {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton() {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getText();
  }
}

export class OrganisationLogUpdatePage {
  pageTitle = element(by.id('jhi-organisation-log-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  organisationIdInput = element(by.id('field_organisationId'));
  createdByLoginInput = element(by.id('field_createdByLogin'));
  createdDateInput = element(by.id('field_createdDate'));
  verifyByNameInput = element(by.id('field_verifyByName'));
  verifyDateInput = element(by.id('field_verifyDate'));
  remarksInput = element(by.id('field_remarks'));
  statusInput = element(by.id('field_status'));
  verifyByLoginInput = element(by.id('field_verifyByLogin'));
  createdByNameInput = element(by.id('field_createdByName'));

  async getPageTitle() {
    return this.pageTitle.getText();
  }

  async setOrganisationIdInput(organisationId) {
    await this.organisationIdInput.sendKeys(organisationId);
  }

  async getOrganisationIdInput() {
    return await this.organisationIdInput.getAttribute('value');
  }

  async setCreatedByLoginInput(createdByLogin) {
    await this.createdByLoginInput.sendKeys(createdByLogin);
  }

  async getCreatedByLoginInput() {
    return await this.createdByLoginInput.getAttribute('value');
  }

  async setCreatedDateInput(createdDate) {
    await this.createdDateInput.sendKeys(createdDate);
  }

  async getCreatedDateInput() {
    return await this.createdDateInput.getAttribute('value');
  }

  async setVerifyByNameInput(verifyByName) {
    await this.verifyByNameInput.sendKeys(verifyByName);
  }

  async getVerifyByNameInput() {
    return await this.verifyByNameInput.getAttribute('value');
  }

  async setVerifyDateInput(verifyDate) {
    await this.verifyDateInput.sendKeys(verifyDate);
  }

  async getVerifyDateInput() {
    return await this.verifyDateInput.getAttribute('value');
  }

  async setRemarksInput(remarks) {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput() {
    return await this.remarksInput.getAttribute('value');
  }

  async setStatusInput(status) {
    await this.statusInput.sendKeys(status);
  }

  async getStatusInput() {
    return await this.statusInput.getAttribute('value');
  }

  async setVerifyByLoginInput(verifyByLogin) {
    await this.verifyByLoginInput.sendKeys(verifyByLogin);
  }

  async getVerifyByLoginInput() {
    return await this.verifyByLoginInput.getAttribute('value');
  }

  async setCreatedByNameInput(createdByName) {
    await this.createdByNameInput.sendKeys(createdByName);
  }

  async getCreatedByNameInput() {
    return await this.createdByNameInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class OrganisationLogDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-organisationLog-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-organisationLog'));

  async getDialogTitle() {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}
