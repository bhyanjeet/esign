import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { DistrictMasterComponentsPage, DistrictMasterDeleteDialog, DistrictMasterUpdatePage } from './district-master.page-object';

const expect = chai.expect;

describe('DistrictMaster e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let districtMasterComponentsPage: DistrictMasterComponentsPage;
  let districtMasterUpdatePage: DistrictMasterUpdatePage;
  let districtMasterDeleteDialog: DistrictMasterDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load DistrictMasters', async () => {
    await navBarPage.goToEntity('district-master');
    districtMasterComponentsPage = new DistrictMasterComponentsPage();
    await browser.wait(ec.visibilityOf(districtMasterComponentsPage.title), 5000);
    expect(await districtMasterComponentsPage.getTitle()).to.eq('District Masters');
  });

  it('should load create DistrictMaster page', async () => {
    await districtMasterComponentsPage.clickOnCreateButton();
    districtMasterUpdatePage = new DistrictMasterUpdatePage();
    expect(await districtMasterUpdatePage.getPageTitle()).to.eq('Create or edit a District Master');
    await districtMasterUpdatePage.cancel();
  });

  it('should create and save DistrictMasters', async () => {
    const nbButtonsBeforeCreate = await districtMasterComponentsPage.countDeleteButtons();

    await districtMasterComponentsPage.clickOnCreateButton();
    await promise.all([
      districtMasterUpdatePage.setDistrictNameInput('districtName'),
      districtMasterUpdatePage.setDistrictCodeInput('districtCode'),
      districtMasterUpdatePage.setCreatedOnInput('2000-12-31'),
      districtMasterUpdatePage.setLastUpdatedByInput('lastUpdatedBy'),
      districtMasterUpdatePage.setLastUpdatedOnInput('2000-12-31'),
      districtMasterUpdatePage.setVerifiedByInput('verifiedBy'),
      districtMasterUpdatePage.setVerifiedOnInput('2000-12-31'),
      districtMasterUpdatePage.setRemarksInput('remarks'),
      districtMasterUpdatePage.stateMasterSelectLastOption()
    ]);
    expect(await districtMasterUpdatePage.getDistrictNameInput()).to.eq(
      'districtName',
      'Expected DistrictName value to be equals to districtName'
    );
    expect(await districtMasterUpdatePage.getDistrictCodeInput()).to.eq(
      'districtCode',
      'Expected DistrictCode value to be equals to districtCode'
    );
    expect(await districtMasterUpdatePage.getCreatedOnInput()).to.eq('2000-12-31', 'Expected createdOn value to be equals to 2000-12-31');
    expect(await districtMasterUpdatePage.getLastUpdatedByInput()).to.eq(
      'lastUpdatedBy',
      'Expected LastUpdatedBy value to be equals to lastUpdatedBy'
    );
    expect(await districtMasterUpdatePage.getLastUpdatedOnInput()).to.eq(
      '2000-12-31',
      'Expected lastUpdatedOn value to be equals to 2000-12-31'
    );
    expect(await districtMasterUpdatePage.getVerifiedByInput()).to.eq('verifiedBy', 'Expected VerifiedBy value to be equals to verifiedBy');
    expect(await districtMasterUpdatePage.getVerifiedOnInput()).to.eq('2000-12-31', 'Expected verifiedOn value to be equals to 2000-12-31');
    expect(await districtMasterUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
    await districtMasterUpdatePage.save();
    expect(await districtMasterUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await districtMasterComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last DistrictMaster', async () => {
    const nbButtonsBeforeDelete = await districtMasterComponentsPage.countDeleteButtons();
    await districtMasterComponentsPage.clickOnLastDeleteButton();

    districtMasterDeleteDialog = new DistrictMasterDeleteDialog();
    expect(await districtMasterDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this District Master?');
    await districtMasterDeleteDialog.clickOnConfirmButton();

    expect(await districtMasterComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
