import { element, by, ElementFinder } from 'protractor';

export class UserLogsComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-user-logs div table .btn-danger'));
  title = element.all(by.css('jhi-user-logs div h2#page-heading span')).first();

  async clickOnCreateButton() {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton() {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getText();
  }
}

export class UserLogsUpdatePage {
  pageTitle = element(by.id('jhi-user-logs-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  actionTakenInput = element(by.id('field_actionTaken'));
  actionTakenByInput = element(by.id('field_actionTakenBy'));
  actionTakenOnUserInput = element(by.id('field_actionTakenOnUser'));
  actionTakenOnDateInput = element(by.id('field_actionTakenOnDate'));
  remarksInput = element(by.id('field_remarks'));

  async getPageTitle() {
    return this.pageTitle.getText();
  }

  async setActionTakenInput(actionTaken) {
    await this.actionTakenInput.sendKeys(actionTaken);
  }

  async getActionTakenInput() {
    return await this.actionTakenInput.getAttribute('value');
  }

  async setActionTakenByInput(actionTakenBy) {
    await this.actionTakenByInput.sendKeys(actionTakenBy);
  }

  async getActionTakenByInput() {
    return await this.actionTakenByInput.getAttribute('value');
  }

  async setActionTakenOnUserInput(actionTakenOnUser) {
    await this.actionTakenOnUserInput.sendKeys(actionTakenOnUser);
  }

  async getActionTakenOnUserInput() {
    return await this.actionTakenOnUserInput.getAttribute('value');
  }

  async setActionTakenOnDateInput(actionTakenOnDate) {
    await this.actionTakenOnDateInput.sendKeys(actionTakenOnDate);
  }

  async getActionTakenOnDateInput() {
    return await this.actionTakenOnDateInput.getAttribute('value');
  }

  async setRemarksInput(remarks) {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput() {
    return await this.remarksInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class UserLogsDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-userLogs-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-userLogs'));

  async getDialogTitle() {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}
