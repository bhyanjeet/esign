import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { UserLogsComponentsPage, UserLogsDeleteDialog, UserLogsUpdatePage } from './user-logs.page-object';

const expect = chai.expect;

describe('UserLogs e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let userLogsComponentsPage: UserLogsComponentsPage;
  let userLogsUpdatePage: UserLogsUpdatePage;
  let userLogsDeleteDialog: UserLogsDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load UserLogs', async () => {
    await navBarPage.goToEntity('user-logs');
    userLogsComponentsPage = new UserLogsComponentsPage();
    await browser.wait(ec.visibilityOf(userLogsComponentsPage.title), 5000);
    expect(await userLogsComponentsPage.getTitle()).to.eq('User Logs');
  });

  it('should load create UserLogs page', async () => {
    await userLogsComponentsPage.clickOnCreateButton();
    userLogsUpdatePage = new UserLogsUpdatePage();
    expect(await userLogsUpdatePage.getPageTitle()).to.eq('Create or edit a User Logs');
    await userLogsUpdatePage.cancel();
  });

  it('should create and save UserLogs', async () => {
    const nbButtonsBeforeCreate = await userLogsComponentsPage.countDeleteButtons();

    await userLogsComponentsPage.clickOnCreateButton();
    await promise.all([
      userLogsUpdatePage.setActionTakenInput('actionTaken'),
      userLogsUpdatePage.setActionTakenByInput('actionTakenBy'),
      userLogsUpdatePage.setActionTakenOnUserInput('5'),
      userLogsUpdatePage.setActionTakenOnDateInput('2000-12-31'),
      userLogsUpdatePage.setRemarksInput('remarks')
    ]);
    expect(await userLogsUpdatePage.getActionTakenInput()).to.eq('actionTaken', 'Expected ActionTaken value to be equals to actionTaken');
    expect(await userLogsUpdatePage.getActionTakenByInput()).to.eq(
      'actionTakenBy',
      'Expected ActionTakenBy value to be equals to actionTakenBy'
    );
    expect(await userLogsUpdatePage.getActionTakenOnUserInput()).to.eq('5', 'Expected actionTakenOnUser value to be equals to 5');
    expect(await userLogsUpdatePage.getActionTakenOnDateInput()).to.eq(
      '2000-12-31',
      'Expected actionTakenOnDate value to be equals to 2000-12-31'
    );
    expect(await userLogsUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
    await userLogsUpdatePage.save();
    expect(await userLogsUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await userLogsComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last UserLogs', async () => {
    const nbButtonsBeforeDelete = await userLogsComponentsPage.countDeleteButtons();
    await userLogsComponentsPage.clickOnLastDeleteButton();

    userLogsDeleteDialog = new UserLogsDeleteDialog();
    expect(await userLogsDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this User Logs?');
    await userLogsDeleteDialog.clickOnConfirmButton();

    expect(await userLogsComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
