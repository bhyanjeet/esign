import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  ApplicationMasterComponentsPage,
  ApplicationMasterDeleteDialog,
  ApplicationMasterUpdatePage
} from './application-master.page-object';

const expect = chai.expect;

describe('ApplicationMaster e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let applicationMasterComponentsPage: ApplicationMasterComponentsPage;
  let applicationMasterUpdatePage: ApplicationMasterUpdatePage;
  let applicationMasterDeleteDialog: ApplicationMasterDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load ApplicationMasters', async () => {
    await navBarPage.goToEntity('application-master');
    applicationMasterComponentsPage = new ApplicationMasterComponentsPage();
    await browser.wait(ec.visibilityOf(applicationMasterComponentsPage.title), 5000);
    expect(await applicationMasterComponentsPage.getTitle()).to.eq('Application Masters');
    await browser.wait(
      ec.or(ec.visibilityOf(applicationMasterComponentsPage.entities), ec.visibilityOf(applicationMasterComponentsPage.noResult)),
      1000
    );
  });

  it('should load create ApplicationMaster page', async () => {
    await applicationMasterComponentsPage.clickOnCreateButton();
    applicationMasterUpdatePage = new ApplicationMasterUpdatePage();
    expect(await applicationMasterUpdatePage.getPageTitle()).to.eq('Create or edit a Application Master');
    await applicationMasterUpdatePage.cancel();
  });

  it('should create and save ApplicationMasters', async () => {
    const nbButtonsBeforeCreate = await applicationMasterComponentsPage.countDeleteButtons();

    await applicationMasterComponentsPage.clickOnCreateButton();

    await promise.all([
      applicationMasterUpdatePage.setApplicationNameInput('applicationName'),
      applicationMasterUpdatePage.setCreatedByInput('createdBy'),
      applicationMasterUpdatePage.setCreatedOnInput('2000-12-31'),
      applicationMasterUpdatePage.setLastUpdatedByInput('lastUpdatedBy'),
      applicationMasterUpdatePage.setLastUpdatedOnInput('2000-12-31'),
      applicationMasterUpdatePage.setVerifiedByInput('verifiedBy'),
      applicationMasterUpdatePage.setVerifiedOnInput('2000-12-31'),
      applicationMasterUpdatePage.setRemarksInput('remarks'),
      applicationMasterUpdatePage.setApplicationURLInput('applicationURL'),
      applicationMasterUpdatePage.setApplicationStatusInput('applicationStatus'),
      applicationMasterUpdatePage.setApplicationIdCodeInput('applicationIdCode'),
      applicationMasterUpdatePage.setStatusInput('status'),
      applicationMasterUpdatePage.setCertPublicKeyInput('certPublicKey'),
      applicationMasterUpdatePage.organisationMasterSelectLastOption()
    ]);

    expect(await applicationMasterUpdatePage.getApplicationNameInput()).to.eq(
      'applicationName',
      'Expected ApplicationName value to be equals to applicationName'
    );
    expect(await applicationMasterUpdatePage.getCreatedByInput()).to.eq('createdBy', 'Expected CreatedBy value to be equals to createdBy');
    expect(await applicationMasterUpdatePage.getCreatedOnInput()).to.eq(
      '2000-12-31',
      'Expected createdOn value to be equals to 2000-12-31'
    );
    expect(await applicationMasterUpdatePage.getLastUpdatedByInput()).to.eq(
      'lastUpdatedBy',
      'Expected LastUpdatedBy value to be equals to lastUpdatedBy'
    );
    expect(await applicationMasterUpdatePage.getLastUpdatedOnInput()).to.eq(
      '2000-12-31',
      'Expected lastUpdatedOn value to be equals to 2000-12-31'
    );
    expect(await applicationMasterUpdatePage.getVerifiedByInput()).to.eq(
      'verifiedBy',
      'Expected VerifiedBy value to be equals to verifiedBy'
    );
    expect(await applicationMasterUpdatePage.getVerifiedOnInput()).to.eq(
      '2000-12-31',
      'Expected verifiedOn value to be equals to 2000-12-31'
    );
    expect(await applicationMasterUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
    expect(await applicationMasterUpdatePage.getApplicationURLInput()).to.eq(
      'applicationURL',
      'Expected ApplicationURL value to be equals to applicationURL'
    );
    expect(await applicationMasterUpdatePage.getApplicationStatusInput()).to.eq(
      'applicationStatus',
      'Expected ApplicationStatus value to be equals to applicationStatus'
    );
    expect(await applicationMasterUpdatePage.getApplicationIdCodeInput()).to.eq(
      'applicationIdCode',
      'Expected ApplicationIdCode value to be equals to applicationIdCode'
    );
    expect(await applicationMasterUpdatePage.getStatusInput()).to.eq('status', 'Expected Status value to be equals to status');
    expect(await applicationMasterUpdatePage.getCertPublicKeyInput()).to.eq(
      'certPublicKey',
      'Expected CertPublicKey value to be equals to certPublicKey'
    );

    await applicationMasterUpdatePage.save();
    expect(await applicationMasterUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await applicationMasterComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last ApplicationMaster', async () => {
    const nbButtonsBeforeDelete = await applicationMasterComponentsPage.countDeleteButtons();
    await applicationMasterComponentsPage.clickOnLastDeleteButton();

    applicationMasterDeleteDialog = new ApplicationMasterDeleteDialog();
    expect(await applicationMasterDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Application Master?');
    await applicationMasterDeleteDialog.clickOnConfirmButton();

    expect(await applicationMasterComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
