import { element, by, ElementFinder } from 'protractor';

export class ApplicationMasterComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-application-master div table .btn-danger'));
  title = element.all(by.css('jhi-application-master div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getText();
  }
}

export class ApplicationMasterUpdatePage {
  pageTitle = element(by.id('jhi-application-master-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  applicationNameInput = element(by.id('field_applicationName'));
  createdByInput = element(by.id('field_createdBy'));
  createdOnInput = element(by.id('field_createdOn'));
  lastUpdatedByInput = element(by.id('field_lastUpdatedBy'));
  lastUpdatedOnInput = element(by.id('field_lastUpdatedOn'));
  verifiedByInput = element(by.id('field_verifiedBy'));
  verifiedOnInput = element(by.id('field_verifiedOn'));
  remarksInput = element(by.id('field_remarks'));
  applicationURLInput = element(by.id('field_applicationURL'));
  applicationStatusInput = element(by.id('field_applicationStatus'));
  applicationIdCodeInput = element(by.id('field_applicationIdCode'));
  statusInput = element(by.id('field_status'));
  certPublicKeyInput = element(by.id('field_certPublicKey'));

  organisationMasterSelect = element(by.id('field_organisationMaster'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }

  async setApplicationNameInput(applicationName: string): Promise<void> {
    await this.applicationNameInput.sendKeys(applicationName);
  }

  async getApplicationNameInput(): Promise<string> {
    return await this.applicationNameInput.getAttribute('value');
  }

  async setCreatedByInput(createdBy: string): Promise<void> {
    await this.createdByInput.sendKeys(createdBy);
  }

  async getCreatedByInput(): Promise<string> {
    return await this.createdByInput.getAttribute('value');
  }

  async setCreatedOnInput(createdOn: string): Promise<void> {
    await this.createdOnInput.sendKeys(createdOn);
  }

  async getCreatedOnInput(): Promise<string> {
    return await this.createdOnInput.getAttribute('value');
  }

  async setLastUpdatedByInput(lastUpdatedBy: string): Promise<void> {
    await this.lastUpdatedByInput.sendKeys(lastUpdatedBy);
  }

  async getLastUpdatedByInput(): Promise<string> {
    return await this.lastUpdatedByInput.getAttribute('value');
  }

  async setLastUpdatedOnInput(lastUpdatedOn: string): Promise<void> {
    await this.lastUpdatedOnInput.sendKeys(lastUpdatedOn);
  }

  async getLastUpdatedOnInput(): Promise<string> {
    return await this.lastUpdatedOnInput.getAttribute('value');
  }

  async setVerifiedByInput(verifiedBy: string): Promise<void> {
    await this.verifiedByInput.sendKeys(verifiedBy);
  }

  async getVerifiedByInput(): Promise<string> {
    return await this.verifiedByInput.getAttribute('value');
  }

  async setVerifiedOnInput(verifiedOn: string): Promise<void> {
    await this.verifiedOnInput.sendKeys(verifiedOn);
  }

  async getVerifiedOnInput(): Promise<string> {
    return await this.verifiedOnInput.getAttribute('value');
  }

  async setRemarksInput(remarks: string): Promise<void> {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput(): Promise<string> {
    return await this.remarksInput.getAttribute('value');
  }

  async setApplicationURLInput(applicationURL: string): Promise<void> {
    await this.applicationURLInput.sendKeys(applicationURL);
  }

  async getApplicationURLInput(): Promise<string> {
    return await this.applicationURLInput.getAttribute('value');
  }

  async setApplicationStatusInput(applicationStatus: string): Promise<void> {
    await this.applicationStatusInput.sendKeys(applicationStatus);
  }

  async getApplicationStatusInput(): Promise<string> {
    return await this.applicationStatusInput.getAttribute('value');
  }

  async setApplicationIdCodeInput(applicationIdCode: string): Promise<void> {
    await this.applicationIdCodeInput.sendKeys(applicationIdCode);
  }

  async getApplicationIdCodeInput(): Promise<string> {
    return await this.applicationIdCodeInput.getAttribute('value');
  }

  async setStatusInput(status: string): Promise<void> {
    await this.statusInput.sendKeys(status);
  }

  async getStatusInput(): Promise<string> {
    return await this.statusInput.getAttribute('value');
  }

  async setCertPublicKeyInput(certPublicKey: string): Promise<void> {
    await this.certPublicKeyInput.sendKeys(certPublicKey);
  }

  async getCertPublicKeyInput(): Promise<string> {
    return await this.certPublicKeyInput.getAttribute('value');
  }

  async organisationMasterSelectLastOption(): Promise<void> {
    await this.organisationMasterSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async organisationMasterSelectOption(option: string): Promise<void> {
    await this.organisationMasterSelect.sendKeys(option);
  }

  getOrganisationMasterSelect(): ElementFinder {
    return this.organisationMasterSelect;
  }

  async getOrganisationMasterSelectedOption(): Promise<string> {
    return await this.organisationMasterSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ApplicationMasterDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-applicationMaster-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-applicationMaster'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
