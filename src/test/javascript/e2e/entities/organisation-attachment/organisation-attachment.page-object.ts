import { element, by, ElementFinder } from 'protractor';

export class OrganisationAttachmentComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-organisation-attachment div table .btn-danger'));
  title = element.all(by.css('jhi-organisation-attachment div h2#page-heading span')).first();

  async clickOnCreateButton() {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton() {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getText();
  }
}

export class OrganisationAttachmentUpdatePage {
  pageTitle = element(by.id('jhi-organisation-attachment-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  attachmentInput = element(by.id('field_attachment'));
  organisationMasterSelect = element(by.id('field_organisationMaster'));
  organisationDocumentSelect = element(by.id('field_organisationDocument'));

  async getPageTitle() {
    return this.pageTitle.getText();
  }

  async setAttachmentInput(attachment) {
    await this.attachmentInput.sendKeys(attachment);
  }

  async getAttachmentInput() {
    return await this.attachmentInput.getAttribute('value');
  }

  async organisationMasterSelectLastOption() {
    await this.organisationMasterSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async organisationMasterSelectOption(option) {
    await this.organisationMasterSelect.sendKeys(option);
  }

  getOrganisationMasterSelect(): ElementFinder {
    return this.organisationMasterSelect;
  }

  async getOrganisationMasterSelectedOption() {
    return await this.organisationMasterSelect.element(by.css('option:checked')).getText();
  }

  async organisationDocumentSelectLastOption() {
    await this.organisationDocumentSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async organisationDocumentSelectOption(option) {
    await this.organisationDocumentSelect.sendKeys(option);
  }

  getOrganisationDocumentSelect(): ElementFinder {
    return this.organisationDocumentSelect;
  }

  async getOrganisationDocumentSelectedOption() {
    return await this.organisationDocumentSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class OrganisationAttachmentDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-organisationAttachment-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-organisationAttachment'));

  async getDialogTitle() {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}
