import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  OrganisationAttachmentComponentsPage,
  OrganisationAttachmentDeleteDialog,
  OrganisationAttachmentUpdatePage
} from './organisation-attachment.page-object';

const expect = chai.expect;

describe('OrganisationAttachment e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let organisationAttachmentComponentsPage: OrganisationAttachmentComponentsPage;
  let organisationAttachmentUpdatePage: OrganisationAttachmentUpdatePage;
  let organisationAttachmentDeleteDialog: OrganisationAttachmentDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load OrganisationAttachments', async () => {
    await navBarPage.goToEntity('organisation-attachment');
    organisationAttachmentComponentsPage = new OrganisationAttachmentComponentsPage();
    await browser.wait(ec.visibilityOf(organisationAttachmentComponentsPage.title), 5000);
    expect(await organisationAttachmentComponentsPage.getTitle()).to.eq('Organisation Attachments');
  });

  it('should load create OrganisationAttachment page', async () => {
    await organisationAttachmentComponentsPage.clickOnCreateButton();
    organisationAttachmentUpdatePage = new OrganisationAttachmentUpdatePage();
    expect(await organisationAttachmentUpdatePage.getPageTitle()).to.eq('Create or edit a Organisation Attachment');
    await organisationAttachmentUpdatePage.cancel();
  });

  it('should create and save OrganisationAttachments', async () => {
    const nbButtonsBeforeCreate = await organisationAttachmentComponentsPage.countDeleteButtons();

    await organisationAttachmentComponentsPage.clickOnCreateButton();
    await promise.all([
      organisationAttachmentUpdatePage.setAttachmentInput('attachment'),
      organisationAttachmentUpdatePage.organisationMasterSelectLastOption(),
      organisationAttachmentUpdatePage.organisationDocumentSelectLastOption()
    ]);
    expect(await organisationAttachmentUpdatePage.getAttachmentInput()).to.eq(
      'attachment',
      'Expected Attachment value to be equals to attachment'
    );
    await organisationAttachmentUpdatePage.save();
    expect(await organisationAttachmentUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await organisationAttachmentComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last OrganisationAttachment', async () => {
    const nbButtonsBeforeDelete = await organisationAttachmentComponentsPage.countDeleteButtons();
    await organisationAttachmentComponentsPage.clickOnLastDeleteButton();

    organisationAttachmentDeleteDialog = new OrganisationAttachmentDeleteDialog();
    expect(await organisationAttachmentDeleteDialog.getDialogTitle()).to.eq(
      'Are you sure you want to delete this Organisation Attachment?'
    );
    await organisationAttachmentDeleteDialog.clickOnConfirmButton();

    expect(await organisationAttachmentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
