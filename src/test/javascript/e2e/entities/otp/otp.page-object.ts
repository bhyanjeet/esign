import { element, by, ElementFinder } from 'protractor';

export class OtpComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-otp div table .btn-danger'));
  title = element.all(by.css('jhi-otp div h2#page-heading span')).first();

  async clickOnCreateButton() {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton() {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getText();
  }
}

export class OtpUpdatePage {
  pageTitle = element(by.id('jhi-otp-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  otpInput = element(by.id('field_otp'));
  mobileNumberInput = element(by.id('field_mobileNumber'));
  requestTimeInput = element(by.id('field_requestTime'));

  async getPageTitle() {
    return this.pageTitle.getText();
  }

  async setOtpInput(otp) {
    await this.otpInput.sendKeys(otp);
  }

  async getOtpInput() {
    return await this.otpInput.getAttribute('value');
  }

  async setMobileNumberInput(mobileNumber) {
    await this.mobileNumberInput.sendKeys(mobileNumber);
  }

  async getMobileNumberInput() {
    return await this.mobileNumberInput.getAttribute('value');
  }

  async setRequestTimeInput(requestTime) {
    await this.requestTimeInput.sendKeys(requestTime);
  }

  async getRequestTimeInput() {
    return await this.requestTimeInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class OtpDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-otp-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-otp'));

  async getDialogTitle() {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}
