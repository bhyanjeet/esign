import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { OtpComponentsPage, OtpDeleteDialog, OtpUpdatePage } from './otp.page-object';

const expect = chai.expect;

describe('Otp e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let otpComponentsPage: OtpComponentsPage;
  let otpUpdatePage: OtpUpdatePage;
  let otpDeleteDialog: OtpDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Otps', async () => {
    await navBarPage.goToEntity('otp');
    otpComponentsPage = new OtpComponentsPage();
    await browser.wait(ec.visibilityOf(otpComponentsPage.title), 5000);
    expect(await otpComponentsPage.getTitle()).to.eq('Otps');
  });

  it('should load create Otp page', async () => {
    await otpComponentsPage.clickOnCreateButton();
    otpUpdatePage = new OtpUpdatePage();
    expect(await otpUpdatePage.getPageTitle()).to.eq('Create or edit a Otp');
    await otpUpdatePage.cancel();
  });

  it('should create and save Otps', async () => {
    const nbButtonsBeforeCreate = await otpComponentsPage.countDeleteButtons();

    await otpComponentsPage.clickOnCreateButton();
    await promise.all([
      otpUpdatePage.setOtpInput('5'),
      otpUpdatePage.setMobileNumberInput('5'),
      otpUpdatePage.setRequestTimeInput('01/01/2001' + protractor.Key.TAB + '02:30AM')
    ]);
    expect(await otpUpdatePage.getOtpInput()).to.eq('5', 'Expected otp value to be equals to 5');
    expect(await otpUpdatePage.getMobileNumberInput()).to.eq('5', 'Expected mobileNumber value to be equals to 5');
    expect(await otpUpdatePage.getRequestTimeInput()).to.contain(
      '2001-01-01T02:30',
      'Expected requestTime value to be equals to 2000-12-31'
    );
    await otpUpdatePage.save();
    expect(await otpUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await otpComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Otp', async () => {
    const nbButtonsBeforeDelete = await otpComponentsPage.countDeleteButtons();
    await otpComponentsPage.clickOnLastDeleteButton();

    otpDeleteDialog = new OtpDeleteDialog();
    expect(await otpDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Otp?');
    await otpDeleteDialog.clickOnConfirmButton();

    expect(await otpComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
