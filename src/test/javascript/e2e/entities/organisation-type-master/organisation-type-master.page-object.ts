import { element, by, ElementFinder } from 'protractor';

export class OrganisationTypeMasterComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-organisation-type-master div table .btn-danger'));
  title = element.all(by.css('jhi-organisation-type-master div h2#page-heading span')).first();

  async clickOnCreateButton() {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton() {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getText();
  }
}

export class OrganisationTypeMasterUpdatePage {
  pageTitle = element(by.id('jhi-organisation-type-master-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  organisationTypeDetailInput = element(by.id('field_organisationTypeDetail'));
  organisationTypeAbbreviationInput = element(by.id('field_organisationTypeAbbreviation'));
  createdByInput = element(by.id('field_createdBy'));
  createdOnInput = element(by.id('field_createdOn'));
  lastUpdatedByInput = element(by.id('field_lastUpdatedBy'));
  lastUpdatedOnInput = element(by.id('field_lastUpdatedOn'));
  verifiedByInput = element(by.id('field_verifiedBy'));
  verifiedOnInput = element(by.id('field_verifiedOn'));
  remarksInput = element(by.id('field_remarks'));

  async getPageTitle() {
    return this.pageTitle.getText();
  }

  async setOrganisationTypeDetailInput(organisationTypeDetail) {
    await this.organisationTypeDetailInput.sendKeys(organisationTypeDetail);
  }

  async getOrganisationTypeDetailInput() {
    return await this.organisationTypeDetailInput.getAttribute('value');
  }

  async setOrganisationTypeAbbreviationInput(organisationTypeAbbreviation) {
    await this.organisationTypeAbbreviationInput.sendKeys(organisationTypeAbbreviation);
  }

  async getOrganisationTypeAbbreviationInput() {
    return await this.organisationTypeAbbreviationInput.getAttribute('value');
  }

  async setCreatedByInput(createdBy) {
    await this.createdByInput.sendKeys(createdBy);
  }

  async getCreatedByInput() {
    return await this.createdByInput.getAttribute('value');
  }

  async setCreatedOnInput(createdOn) {
    await this.createdOnInput.sendKeys(createdOn);
  }

  async getCreatedOnInput() {
    return await this.createdOnInput.getAttribute('value');
  }

  async setLastUpdatedByInput(lastUpdatedBy) {
    await this.lastUpdatedByInput.sendKeys(lastUpdatedBy);
  }

  async getLastUpdatedByInput() {
    return await this.lastUpdatedByInput.getAttribute('value');
  }

  async setLastUpdatedOnInput(lastUpdatedOn) {
    await this.lastUpdatedOnInput.sendKeys(lastUpdatedOn);
  }

  async getLastUpdatedOnInput() {
    return await this.lastUpdatedOnInput.getAttribute('value');
  }

  async setVerifiedByInput(verifiedBy) {
    await this.verifiedByInput.sendKeys(verifiedBy);
  }

  async getVerifiedByInput() {
    return await this.verifiedByInput.getAttribute('value');
  }

  async setVerifiedOnInput(verifiedOn) {
    await this.verifiedOnInput.sendKeys(verifiedOn);
  }

  async getVerifiedOnInput() {
    return await this.verifiedOnInput.getAttribute('value');
  }

  async setRemarksInput(remarks) {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput() {
    return await this.remarksInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class OrganisationTypeMasterDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-organisationTypeMaster-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-organisationTypeMaster'));

  async getDialogTitle() {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}
