import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  OrganisationTypeMasterComponentsPage,
  OrganisationTypeMasterDeleteDialog,
  OrganisationTypeMasterUpdatePage
} from './organisation-type-master.page-object';

const expect = chai.expect;

describe('OrganisationTypeMaster e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let organisationTypeMasterComponentsPage: OrganisationTypeMasterComponentsPage;
  let organisationTypeMasterUpdatePage: OrganisationTypeMasterUpdatePage;
  let organisationTypeMasterDeleteDialog: OrganisationTypeMasterDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load OrganisationTypeMasters', async () => {
    await navBarPage.goToEntity('organisation-type-master');
    organisationTypeMasterComponentsPage = new OrganisationTypeMasterComponentsPage();
    await browser.wait(ec.visibilityOf(organisationTypeMasterComponentsPage.title), 5000);
    expect(await organisationTypeMasterComponentsPage.getTitle()).to.eq('Organisation Type Masters');
  });

  it('should load create OrganisationTypeMaster page', async () => {
    await organisationTypeMasterComponentsPage.clickOnCreateButton();
    organisationTypeMasterUpdatePage = new OrganisationTypeMasterUpdatePage();
    expect(await organisationTypeMasterUpdatePage.getPageTitle()).to.eq('Create or edit a Organisation Type Master');
    await organisationTypeMasterUpdatePage.cancel();
  });

  it('should create and save OrganisationTypeMasters', async () => {
    const nbButtonsBeforeCreate = await organisationTypeMasterComponentsPage.countDeleteButtons();

    await organisationTypeMasterComponentsPage.clickOnCreateButton();
    await promise.all([
      organisationTypeMasterUpdatePage.setOrganisationTypeDetailInput('organisationTypeDetail'),
      organisationTypeMasterUpdatePage.setOrganisationTypeAbbreviationInput('organisationTypeAbbreviation'),
      organisationTypeMasterUpdatePage.setCreatedByInput('createdBy'),
      organisationTypeMasterUpdatePage.setCreatedOnInput('2000-12-31'),
      organisationTypeMasterUpdatePage.setLastUpdatedByInput('lastUpdatedBy'),
      organisationTypeMasterUpdatePage.setLastUpdatedOnInput('2000-12-31'),
      organisationTypeMasterUpdatePage.setVerifiedByInput('verifiedBy'),
      organisationTypeMasterUpdatePage.setVerifiedOnInput('2000-12-31'),
      organisationTypeMasterUpdatePage.setRemarksInput('remarks')
    ]);
    expect(await organisationTypeMasterUpdatePage.getOrganisationTypeDetailInput()).to.eq(
      'organisationTypeDetail',
      'Expected OrganisationTypeDetail value to be equals to organisationTypeDetail'
    );
    expect(await organisationTypeMasterUpdatePage.getOrganisationTypeAbbreviationInput()).to.eq(
      'organisationTypeAbbreviation',
      'Expected OrganisationTypeAbbreviation value to be equals to organisationTypeAbbreviation'
    );
    expect(await organisationTypeMasterUpdatePage.getCreatedByInput()).to.eq(
      'createdBy',
      'Expected CreatedBy value to be equals to createdBy'
    );
    expect(await organisationTypeMasterUpdatePage.getCreatedOnInput()).to.eq(
      '2000-12-31',
      'Expected createdOn value to be equals to 2000-12-31'
    );
    expect(await organisationTypeMasterUpdatePage.getLastUpdatedByInput()).to.eq(
      'lastUpdatedBy',
      'Expected LastUpdatedBy value to be equals to lastUpdatedBy'
    );
    expect(await organisationTypeMasterUpdatePage.getLastUpdatedOnInput()).to.eq(
      '2000-12-31',
      'Expected lastUpdatedOn value to be equals to 2000-12-31'
    );
    expect(await organisationTypeMasterUpdatePage.getVerifiedByInput()).to.eq(
      'verifiedBy',
      'Expected VerifiedBy value to be equals to verifiedBy'
    );
    expect(await organisationTypeMasterUpdatePage.getVerifiedOnInput()).to.eq(
      '2000-12-31',
      'Expected verifiedOn value to be equals to 2000-12-31'
    );
    expect(await organisationTypeMasterUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
    await organisationTypeMasterUpdatePage.save();
    expect(await organisationTypeMasterUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await organisationTypeMasterComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last OrganisationTypeMaster', async () => {
    const nbButtonsBeforeDelete = await organisationTypeMasterComponentsPage.countDeleteButtons();
    await organisationTypeMasterComponentsPage.clickOnLastDeleteButton();

    organisationTypeMasterDeleteDialog = new OrganisationTypeMasterDeleteDialog();
    expect(await organisationTypeMasterDeleteDialog.getDialogTitle()).to.eq(
      'Are you sure you want to delete this Organisation Type Master?'
    );
    await organisationTypeMasterDeleteDialog.clickOnConfirmButton();

    expect(await organisationTypeMasterComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
