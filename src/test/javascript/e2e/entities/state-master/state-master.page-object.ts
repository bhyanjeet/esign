import { element, by, ElementFinder } from 'protractor';

export class StateMasterComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-state-master div table .btn-danger'));
  title = element.all(by.css('jhi-state-master div h2#page-heading span')).first();

  async clickOnCreateButton() {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton() {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getText();
  }
}

export class StateMasterUpdatePage {
  pageTitle = element(by.id('jhi-state-master-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  stateCodeInput = element(by.id('field_stateCode'));
  stateNameInput = element(by.id('field_stateName'));
  createdOnInput = element(by.id('field_createdOn'));
  lastUpdatedOnInput = element(by.id('field_lastUpdatedOn'));
  lastUpdatedByInput = element(by.id('field_lastUpdatedBy'));
  verifiedByInput = element(by.id('field_verifiedBy'));
  verifiedOnInput = element(by.id('field_verifiedOn'));
  remarksInput = element(by.id('field_remarks'));

  async getPageTitle() {
    return this.pageTitle.getText();
  }

  async setStateCodeInput(stateCode) {
    await this.stateCodeInput.sendKeys(stateCode);
  }

  async getStateCodeInput() {
    return await this.stateCodeInput.getAttribute('value');
  }

  async setStateNameInput(stateName) {
    await this.stateNameInput.sendKeys(stateName);
  }

  async getStateNameInput() {
    return await this.stateNameInput.getAttribute('value');
  }

  async setCreatedOnInput(createdOn) {
    await this.createdOnInput.sendKeys(createdOn);
  }

  async getCreatedOnInput() {
    return await this.createdOnInput.getAttribute('value');
  }

  async setLastUpdatedOnInput(lastUpdatedOn) {
    await this.lastUpdatedOnInput.sendKeys(lastUpdatedOn);
  }

  async getLastUpdatedOnInput() {
    return await this.lastUpdatedOnInput.getAttribute('value');
  }

  async setLastUpdatedByInput(lastUpdatedBy) {
    await this.lastUpdatedByInput.sendKeys(lastUpdatedBy);
  }

  async getLastUpdatedByInput() {
    return await this.lastUpdatedByInput.getAttribute('value');
  }

  async setVerifiedByInput(verifiedBy) {
    await this.verifiedByInput.sendKeys(verifiedBy);
  }

  async getVerifiedByInput() {
    return await this.verifiedByInput.getAttribute('value');
  }

  async setVerifiedOnInput(verifiedOn) {
    await this.verifiedOnInput.sendKeys(verifiedOn);
  }

  async getVerifiedOnInput() {
    return await this.verifiedOnInput.getAttribute('value');
  }

  async setRemarksInput(remarks) {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput() {
    return await this.remarksInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class StateMasterDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-stateMaster-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-stateMaster'));

  async getDialogTitle() {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}
