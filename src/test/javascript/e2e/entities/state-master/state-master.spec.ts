import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { StateMasterComponentsPage, StateMasterDeleteDialog, StateMasterUpdatePage } from './state-master.page-object';

const expect = chai.expect;

describe('StateMaster e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let stateMasterComponentsPage: StateMasterComponentsPage;
  let stateMasterUpdatePage: StateMasterUpdatePage;
  let stateMasterDeleteDialog: StateMasterDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load StateMasters', async () => {
    await navBarPage.goToEntity('state-master');
    stateMasterComponentsPage = new StateMasterComponentsPage();
    await browser.wait(ec.visibilityOf(stateMasterComponentsPage.title), 5000);
    expect(await stateMasterComponentsPage.getTitle()).to.eq('State Masters');
  });

  it('should load create StateMaster page', async () => {
    await stateMasterComponentsPage.clickOnCreateButton();
    stateMasterUpdatePage = new StateMasterUpdatePage();
    expect(await stateMasterUpdatePage.getPageTitle()).to.eq('Create or edit a State Master');
    await stateMasterUpdatePage.cancel();
  });

  it('should create and save StateMasters', async () => {
    const nbButtonsBeforeCreate = await stateMasterComponentsPage.countDeleteButtons();

    await stateMasterComponentsPage.clickOnCreateButton();
    await promise.all([
      stateMasterUpdatePage.setStateCodeInput('stateCode'),
      stateMasterUpdatePage.setStateNameInput('stateName'),
      stateMasterUpdatePage.setCreatedOnInput('2000-12-31'),
      stateMasterUpdatePage.setLastUpdatedOnInput('2000-12-31'),
      stateMasterUpdatePage.setLastUpdatedByInput('lastUpdatedBy'),
      stateMasterUpdatePage.setVerifiedByInput('verifiedBy'),
      stateMasterUpdatePage.setVerifiedOnInput('2000-12-31'),
      stateMasterUpdatePage.setRemarksInput('remarks')
    ]);
    expect(await stateMasterUpdatePage.getStateCodeInput()).to.eq('stateCode', 'Expected StateCode value to be equals to stateCode');
    expect(await stateMasterUpdatePage.getStateNameInput()).to.eq('stateName', 'Expected StateName value to be equals to stateName');
    expect(await stateMasterUpdatePage.getCreatedOnInput()).to.eq('2000-12-31', 'Expected createdOn value to be equals to 2000-12-31');
    expect(await stateMasterUpdatePage.getLastUpdatedOnInput()).to.eq(
      '2000-12-31',
      'Expected lastUpdatedOn value to be equals to 2000-12-31'
    );
    expect(await stateMasterUpdatePage.getLastUpdatedByInput()).to.eq(
      'lastUpdatedBy',
      'Expected LastUpdatedBy value to be equals to lastUpdatedBy'
    );
    expect(await stateMasterUpdatePage.getVerifiedByInput()).to.eq('verifiedBy', 'Expected VerifiedBy value to be equals to verifiedBy');
    expect(await stateMasterUpdatePage.getVerifiedOnInput()).to.eq('2000-12-31', 'Expected verifiedOn value to be equals to 2000-12-31');
    expect(await stateMasterUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
    await stateMasterUpdatePage.save();
    expect(await stateMasterUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await stateMasterComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last StateMaster', async () => {
    const nbButtonsBeforeDelete = await stateMasterComponentsPage.countDeleteButtons();
    await stateMasterComponentsPage.clickOnLastDeleteButton();

    stateMasterDeleteDialog = new StateMasterDeleteDialog();
    expect(await stateMasterDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this State Master?');
    await stateMasterDeleteDialog.clickOnConfirmButton();

    expect(await stateMasterComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
