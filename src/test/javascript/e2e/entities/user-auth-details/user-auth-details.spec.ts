import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { UserAuthDetailsComponentsPage, UserAuthDetailsDeleteDialog, UserAuthDetailsUpdatePage } from './user-auth-details.page-object';

const expect = chai.expect;

describe('UserAuthDetails e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let userAuthDetailsComponentsPage: UserAuthDetailsComponentsPage;
  let userAuthDetailsUpdatePage: UserAuthDetailsUpdatePage;
  let userAuthDetailsDeleteDialog: UserAuthDetailsDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load UserAuthDetails', async () => {
    await navBarPage.goToEntity('user-auth-details');
    userAuthDetailsComponentsPage = new UserAuthDetailsComponentsPage();
    await browser.wait(ec.visibilityOf(userAuthDetailsComponentsPage.title), 5000);
    expect(await userAuthDetailsComponentsPage.getTitle()).to.eq('User Auth Details');
    await browser.wait(
      ec.or(ec.visibilityOf(userAuthDetailsComponentsPage.entities), ec.visibilityOf(userAuthDetailsComponentsPage.noResult)),
      1000
    );
  });

  it('should load create UserAuthDetails page', async () => {
    await userAuthDetailsComponentsPage.clickOnCreateButton();
    userAuthDetailsUpdatePage = new UserAuthDetailsUpdatePage();
    expect(await userAuthDetailsUpdatePage.getPageTitle()).to.eq('Create or edit a User Auth Details');
    await userAuthDetailsUpdatePage.cancel();
  });

  it('should create and save UserAuthDetails', async () => {
    const nbButtonsBeforeCreate = await userAuthDetailsComponentsPage.countDeleteButtons();

    await userAuthDetailsComponentsPage.clickOnCreateButton();

    await promise.all([
      userAuthDetailsUpdatePage.setLoginInput('login'),
      userAuthDetailsUpdatePage.setDeviceInput('device'),
      userAuthDetailsUpdatePage.setAuthTokenInput('authToken')
    ]);

    expect(await userAuthDetailsUpdatePage.getLoginInput()).to.eq('login', 'Expected Login value to be equals to login');
    expect(await userAuthDetailsUpdatePage.getDeviceInput()).to.eq('device', 'Expected Device value to be equals to device');
    expect(await userAuthDetailsUpdatePage.getAuthTokenInput()).to.eq('authToken', 'Expected AuthToken value to be equals to authToken');

    await userAuthDetailsUpdatePage.save();
    expect(await userAuthDetailsUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await userAuthDetailsComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last UserAuthDetails', async () => {
    const nbButtonsBeforeDelete = await userAuthDetailsComponentsPage.countDeleteButtons();
    await userAuthDetailsComponentsPage.clickOnLastDeleteButton();

    userAuthDetailsDeleteDialog = new UserAuthDetailsDeleteDialog();
    expect(await userAuthDetailsDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this User Auth Details?');
    await userAuthDetailsDeleteDialog.clickOnConfirmButton();

    expect(await userAuthDetailsComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
