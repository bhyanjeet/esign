import { element, by, ElementFinder } from 'protractor';

export class UserAuthDetailsComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-user-auth-details div table .btn-danger'));
  title = element.all(by.css('jhi-user-auth-details div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getText();
  }
}

export class UserAuthDetailsUpdatePage {
  pageTitle = element(by.id('jhi-user-auth-details-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  loginInput = element(by.id('field_login'));
  deviceInput = element(by.id('field_device'));
  authTokenInput = element(by.id('field_authToken'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }

  async setLoginInput(login: string): Promise<void> {
    await this.loginInput.sendKeys(login);
  }

  async getLoginInput(): Promise<string> {
    return await this.loginInput.getAttribute('value');
  }

  async setDeviceInput(device: string): Promise<void> {
    await this.deviceInput.sendKeys(device);
  }

  async getDeviceInput(): Promise<string> {
    return await this.deviceInput.getAttribute('value');
  }

  async setAuthTokenInput(authToken: string): Promise<void> {
    await this.authTokenInput.sendKeys(authToken);
  }

  async getAuthTokenInput(): Promise<string> {
    return await this.authTokenInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class UserAuthDetailsDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-userAuthDetails-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-userAuthDetails'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
