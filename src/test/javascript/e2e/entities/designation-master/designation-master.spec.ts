import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  DesignationMasterComponentsPage,
  DesignationMasterDeleteDialog,
  DesignationMasterUpdatePage
} from './designation-master.page-object';

const expect = chai.expect;

describe('DesignationMaster e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let designationMasterComponentsPage: DesignationMasterComponentsPage;
  let designationMasterUpdatePage: DesignationMasterUpdatePage;
  let designationMasterDeleteDialog: DesignationMasterDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load DesignationMasters', async () => {
    await navBarPage.goToEntity('designation-master');
    designationMasterComponentsPage = new DesignationMasterComponentsPage();
    await browser.wait(ec.visibilityOf(designationMasterComponentsPage.title), 5000);
    expect(await designationMasterComponentsPage.getTitle()).to.eq('Designation Masters');
  });

  it('should load create DesignationMaster page', async () => {
    await designationMasterComponentsPage.clickOnCreateButton();
    designationMasterUpdatePage = new DesignationMasterUpdatePage();
    expect(await designationMasterUpdatePage.getPageTitle()).to.eq('Create or edit a Designation Master');
    await designationMasterUpdatePage.cancel();
  });

  it('should create and save DesignationMasters', async () => {
    const nbButtonsBeforeCreate = await designationMasterComponentsPage.countDeleteButtons();

    await designationMasterComponentsPage.clickOnCreateButton();
    await promise.all([
      designationMasterUpdatePage.setDesignationNameInput('designationName'),
      designationMasterUpdatePage.setDesignationAbbreviationInput('designationAbbreviation'),
      designationMasterUpdatePage.setCreatedByInput('createdBy'),
      designationMasterUpdatePage.setCreatedOnInput('2000-12-31'),
      designationMasterUpdatePage.setLastUpdatedByInput('lastUpdatedBy'),
      designationMasterUpdatePage.setLastUpdatedOnInput('2000-12-31'),
      designationMasterUpdatePage.setVerifiedByInput('verifiedBy'),
      designationMasterUpdatePage.setVerifiedOnInput('2000-12-31'),
      designationMasterUpdatePage.setRemarksInput('remarks')
    ]);
    expect(await designationMasterUpdatePage.getDesignationNameInput()).to.eq(
      'designationName',
      'Expected DesignationName value to be equals to designationName'
    );
    expect(await designationMasterUpdatePage.getDesignationAbbreviationInput()).to.eq(
      'designationAbbreviation',
      'Expected DesignationAbbreviation value to be equals to designationAbbreviation'
    );
    expect(await designationMasterUpdatePage.getCreatedByInput()).to.eq('createdBy', 'Expected CreatedBy value to be equals to createdBy');
    expect(await designationMasterUpdatePage.getCreatedOnInput()).to.eq(
      '2000-12-31',
      'Expected createdOn value to be equals to 2000-12-31'
    );
    expect(await designationMasterUpdatePage.getLastUpdatedByInput()).to.eq(
      'lastUpdatedBy',
      'Expected LastUpdatedBy value to be equals to lastUpdatedBy'
    );
    expect(await designationMasterUpdatePage.getLastUpdatedOnInput()).to.eq(
      '2000-12-31',
      'Expected lastUpdatedOn value to be equals to 2000-12-31'
    );
    expect(await designationMasterUpdatePage.getVerifiedByInput()).to.eq(
      'verifiedBy',
      'Expected VerifiedBy value to be equals to verifiedBy'
    );
    expect(await designationMasterUpdatePage.getVerifiedOnInput()).to.eq(
      '2000-12-31',
      'Expected verifiedOn value to be equals to 2000-12-31'
    );
    expect(await designationMasterUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
    await designationMasterUpdatePage.save();
    expect(await designationMasterUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await designationMasterComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last DesignationMaster', async () => {
    const nbButtonsBeforeDelete = await designationMasterComponentsPage.countDeleteButtons();
    await designationMasterComponentsPage.clickOnLastDeleteButton();

    designationMasterDeleteDialog = new DesignationMasterDeleteDialog();
    expect(await designationMasterDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Designation Master?');
    await designationMasterDeleteDialog.clickOnConfirmButton();

    expect(await designationMasterComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
