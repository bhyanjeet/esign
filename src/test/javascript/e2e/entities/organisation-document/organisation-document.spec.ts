import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  OrganisationDocumentComponentsPage,
  OrganisationDocumentDeleteDialog,
  OrganisationDocumentUpdatePage
} from './organisation-document.page-object';

const expect = chai.expect;

describe('OrganisationDocument e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let organisationDocumentComponentsPage: OrganisationDocumentComponentsPage;
  let organisationDocumentUpdatePage: OrganisationDocumentUpdatePage;
  let organisationDocumentDeleteDialog: OrganisationDocumentDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load OrganisationDocuments', async () => {
    await navBarPage.goToEntity('organisation-document');
    organisationDocumentComponentsPage = new OrganisationDocumentComponentsPage();
    await browser.wait(ec.visibilityOf(organisationDocumentComponentsPage.title), 5000);
    expect(await organisationDocumentComponentsPage.getTitle()).to.eq('Organisation Documents');
  });

  it('should load create OrganisationDocument page', async () => {
    await organisationDocumentComponentsPage.clickOnCreateButton();
    organisationDocumentUpdatePage = new OrganisationDocumentUpdatePage();
    expect(await organisationDocumentUpdatePage.getPageTitle()).to.eq('Create or edit a Organisation Document');
    await organisationDocumentUpdatePage.cancel();
  });

  it('should create and save OrganisationDocuments', async () => {
    const nbButtonsBeforeCreate = await organisationDocumentComponentsPage.countDeleteButtons();

    await organisationDocumentComponentsPage.clickOnCreateButton();
    await promise.all([
      organisationDocumentUpdatePage.setDocumentTitleInput('documentTitle'),
      organisationDocumentUpdatePage.setCreatedByInput('createdBy'),
      organisationDocumentUpdatePage.setCreatedOnInput('2000-12-31'),
      organisationDocumentUpdatePage.setVerifiedByInput('verifiedBy'),
      organisationDocumentUpdatePage.setVerifiedOnInput('2000-12-31'),
      organisationDocumentUpdatePage.setRemarksInput('remarks'),
      organisationDocumentUpdatePage.setDocumentRelatedToInput('documentRelatedTo'),
      organisationDocumentUpdatePage.organisationTypeMasterSelectLastOption()
    ]);
    expect(await organisationDocumentUpdatePage.getDocumentTitleInput()).to.eq(
      'documentTitle',
      'Expected DocumentTitle value to be equals to documentTitle'
    );
    expect(await organisationDocumentUpdatePage.getCreatedByInput()).to.eq(
      'createdBy',
      'Expected CreatedBy value to be equals to createdBy'
    );
    expect(await organisationDocumentUpdatePage.getCreatedOnInput()).to.eq(
      '2000-12-31',
      'Expected createdOn value to be equals to 2000-12-31'
    );
    expect(await organisationDocumentUpdatePage.getVerifiedByInput()).to.eq(
      'verifiedBy',
      'Expected VerifiedBy value to be equals to verifiedBy'
    );
    expect(await organisationDocumentUpdatePage.getVerifiedOnInput()).to.eq(
      '2000-12-31',
      'Expected verifiedOn value to be equals to 2000-12-31'
    );
    expect(await organisationDocumentUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
    expect(await organisationDocumentUpdatePage.getDocumentRelatedToInput()).to.eq(
      'documentRelatedTo',
      'Expected DocumentRelatedTo value to be equals to documentRelatedTo'
    );
    await organisationDocumentUpdatePage.save();
    expect(await organisationDocumentUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await organisationDocumentComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last OrganisationDocument', async () => {
    const nbButtonsBeforeDelete = await organisationDocumentComponentsPage.countDeleteButtons();
    await organisationDocumentComponentsPage.clickOnLastDeleteButton();

    organisationDocumentDeleteDialog = new OrganisationDocumentDeleteDialog();
    expect(await organisationDocumentDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Organisation Document?');
    await organisationDocumentDeleteDialog.clickOnConfirmButton();

    expect(await organisationDocumentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
