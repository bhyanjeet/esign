import { element, by, ElementFinder } from 'protractor';

export class OrganisationDocumentComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-organisation-document div table .btn-danger'));
  title = element.all(by.css('jhi-organisation-document div h2#page-heading span')).first();

  async clickOnCreateButton() {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton() {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getText();
  }
}

export class OrganisationDocumentUpdatePage {
  pageTitle = element(by.id('jhi-organisation-document-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  documentTitleInput = element(by.id('field_documentTitle'));
  createdByInput = element(by.id('field_createdBy'));
  createdOnInput = element(by.id('field_createdOn'));
  verifiedByInput = element(by.id('field_verifiedBy'));
  verifiedOnInput = element(by.id('field_verifiedOn'));
  remarksInput = element(by.id('field_remarks'));
  documentRelatedToInput = element(by.id('field_documentRelatedTo'));
  organisationTypeMasterSelect = element(by.id('field_organisationTypeMaster'));

  async getPageTitle() {
    return this.pageTitle.getText();
  }

  async setDocumentTitleInput(documentTitle) {
    await this.documentTitleInput.sendKeys(documentTitle);
  }

  async getDocumentTitleInput() {
    return await this.documentTitleInput.getAttribute('value');
  }

  async setCreatedByInput(createdBy) {
    await this.createdByInput.sendKeys(createdBy);
  }

  async getCreatedByInput() {
    return await this.createdByInput.getAttribute('value');
  }

  async setCreatedOnInput(createdOn) {
    await this.createdOnInput.sendKeys(createdOn);
  }

  async getCreatedOnInput() {
    return await this.createdOnInput.getAttribute('value');
  }

  async setVerifiedByInput(verifiedBy) {
    await this.verifiedByInput.sendKeys(verifiedBy);
  }

  async getVerifiedByInput() {
    return await this.verifiedByInput.getAttribute('value');
  }

  async setVerifiedOnInput(verifiedOn) {
    await this.verifiedOnInput.sendKeys(verifiedOn);
  }

  async getVerifiedOnInput() {
    return await this.verifiedOnInput.getAttribute('value');
  }

  async setRemarksInput(remarks) {
    await this.remarksInput.sendKeys(remarks);
  }

  async getRemarksInput() {
    return await this.remarksInput.getAttribute('value');
  }

  async setDocumentRelatedToInput(documentRelatedTo) {
    await this.documentRelatedToInput.sendKeys(documentRelatedTo);
  }

  async getDocumentRelatedToInput() {
    return await this.documentRelatedToInput.getAttribute('value');
  }

  async organisationTypeMasterSelectLastOption() {
    await this.organisationTypeMasterSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async organisationTypeMasterSelectOption(option) {
    await this.organisationTypeMasterSelect.sendKeys(option);
  }

  getOrganisationTypeMasterSelect(): ElementFinder {
    return this.organisationTypeMasterSelect;
  }

  async getOrganisationTypeMasterSelectedOption() {
    return await this.organisationTypeMasterSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class OrganisationDocumentDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-organisationDocument-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-organisationDocument'));

  async getDialogTitle() {
    return this.dialogTitle.getText();
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}
