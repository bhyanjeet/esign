import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { EsignRoleComponentsPage, EsignRoleDeleteDialog, EsignRoleUpdatePage } from './esign-role.page-object';

const expect = chai.expect;

describe('EsignRole e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let esignRoleComponentsPage: EsignRoleComponentsPage;
  let esignRoleUpdatePage: EsignRoleUpdatePage;
  let esignRoleDeleteDialog: EsignRoleDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load EsignRoles', async () => {
    await navBarPage.goToEntity('esign-role');
    esignRoleComponentsPage = new EsignRoleComponentsPage();
    await browser.wait(ec.visibilityOf(esignRoleComponentsPage.title), 5000);
    expect(await esignRoleComponentsPage.getTitle()).to.eq('Esign Roles');
  });

  it('should load create EsignRole page', async () => {
    await esignRoleComponentsPage.clickOnCreateButton();
    esignRoleUpdatePage = new EsignRoleUpdatePage();
    expect(await esignRoleUpdatePage.getPageTitle()).to.eq('Create or edit a Esign Role');
    await esignRoleUpdatePage.cancel();
  });

  it('should create and save EsignRoles', async () => {
    const nbButtonsBeforeCreate = await esignRoleComponentsPage.countDeleteButtons();

    await esignRoleComponentsPage.clickOnCreateButton();
    await promise.all([
      esignRoleUpdatePage.setESignRoleDetailInput('eSignRoleDetail'),
      esignRoleUpdatePage.setCreatedByInput('createdBy'),
      esignRoleUpdatePage.setCreatedOnInput('2000-12-31'),
      esignRoleUpdatePage.setLastUpdatedByInput('lastUpdatedBy'),
      esignRoleUpdatePage.setLastUpdatedOnInput('2000-12-31'),
      esignRoleUpdatePage.setVerifiedByInput('verifiedBy'),
      esignRoleUpdatePage.setVerifiedOnInput('2000-12-31'),
      esignRoleUpdatePage.setRemarksInput('remarks')
    ]);
    expect(await esignRoleUpdatePage.getESignRoleDetailInput()).to.eq(
      'eSignRoleDetail',
      'Expected ESignRoleDetail value to be equals to eSignRoleDetail'
    );
    expect(await esignRoleUpdatePage.getCreatedByInput()).to.eq('createdBy', 'Expected CreatedBy value to be equals to createdBy');
    expect(await esignRoleUpdatePage.getCreatedOnInput()).to.eq('2000-12-31', 'Expected createdOn value to be equals to 2000-12-31');
    expect(await esignRoleUpdatePage.getLastUpdatedByInput()).to.eq(
      'lastUpdatedBy',
      'Expected LastUpdatedBy value to be equals to lastUpdatedBy'
    );
    expect(await esignRoleUpdatePage.getLastUpdatedOnInput()).to.eq(
      '2000-12-31',
      'Expected lastUpdatedOn value to be equals to 2000-12-31'
    );
    expect(await esignRoleUpdatePage.getVerifiedByInput()).to.eq('verifiedBy', 'Expected VerifiedBy value to be equals to verifiedBy');
    expect(await esignRoleUpdatePage.getVerifiedOnInput()).to.eq('2000-12-31', 'Expected verifiedOn value to be equals to 2000-12-31');
    expect(await esignRoleUpdatePage.getRemarksInput()).to.eq('remarks', 'Expected Remarks value to be equals to remarks');
    await esignRoleUpdatePage.save();
    expect(await esignRoleUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await esignRoleComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last EsignRole', async () => {
    const nbButtonsBeforeDelete = await esignRoleComponentsPage.countDeleteButtons();
    await esignRoleComponentsPage.clickOnLastDeleteButton();

    esignRoleDeleteDialog = new EsignRoleDeleteDialog();
    expect(await esignRoleDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Esign Role?');
    await esignRoleDeleteDialog.clickOnConfirmButton();

    expect(await esignRoleComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
