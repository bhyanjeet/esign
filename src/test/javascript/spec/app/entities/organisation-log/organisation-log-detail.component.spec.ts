import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { OrganisationLogDetailComponent } from 'app/entities/organisation-log/organisation-log-detail.component';
import { OrganisationLog } from 'app/shared/model/organisation-log.model';

describe('Component Tests', () => {
  describe('OrganisationLog Management Detail Component', () => {
    let comp: OrganisationLogDetailComponent;
    let fixture: ComponentFixture<OrganisationLogDetailComponent>;
    const route = ({ data: of({ organisationLog: new OrganisationLog(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OrganisationLogDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(OrganisationLogDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OrganisationLogDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.organisationLog).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
