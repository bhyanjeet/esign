import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { OrganisationLogService } from 'app/entities/organisation-log/organisation-log.service';
import { IOrganisationLog, OrganisationLog } from 'app/shared/model/organisation-log.model';

describe('Service Tests', () => {
  describe('OrganisationLog Service', () => {
    let injector: TestBed;
    let service: OrganisationLogService;
    let httpMock: HttpTestingController;
    let elemDefault: IOrganisationLog;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(OrganisationLogService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new OrganisationLog(0, 0, 'AAAAAAA', currentDate, 'AAAAAAA', currentDate, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            verifyDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a OrganisationLog', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            verifyDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            createdDate: currentDate,
            verifyDate: currentDate
          },
          returnedFromService
        );
        service
          .create(new OrganisationLog(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a OrganisationLog', () => {
        const returnedFromService = Object.assign(
          {
            organisationId: 1,
            createdByLogin: 'BBBBBB',
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            verifyByName: 'BBBBBB',
            verifyDate: currentDate.format(DATE_TIME_FORMAT),
            remarks: 'BBBBBB',
            status: 'BBBBBB',
            verifyByLogin: 'BBBBBB',
            createdByName: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdDate: currentDate,
            verifyDate: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of OrganisationLog', () => {
        const returnedFromService = Object.assign(
          {
            organisationId: 1,
            createdByLogin: 'BBBBBB',
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            verifyByName: 'BBBBBB',
            verifyDate: currentDate.format(DATE_TIME_FORMAT),
            remarks: 'BBBBBB',
            status: 'BBBBBB',
            verifyByLogin: 'BBBBBB',
            createdByName: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            createdDate: currentDate,
            verifyDate: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a OrganisationLog', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
