import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { OrganisationLogUpdateComponent } from 'app/entities/organisation-log/organisation-log-update.component';
import { OrganisationLogService } from 'app/entities/organisation-log/organisation-log.service';
import { OrganisationLog } from 'app/shared/model/organisation-log.model';

describe('Component Tests', () => {
  describe('OrganisationLog Management Update Component', () => {
    let comp: OrganisationLogUpdateComponent;
    let fixture: ComponentFixture<OrganisationLogUpdateComponent>;
    let service: OrganisationLogService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OrganisationLogUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(OrganisationLogUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OrganisationLogUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OrganisationLogService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrganisationLog(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrganisationLog();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
