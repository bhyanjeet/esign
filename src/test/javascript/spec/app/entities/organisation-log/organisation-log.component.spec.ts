import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { EsignharyanaTestModule } from '../../../test.module';
import { OrganisationLogComponent } from 'app/entities/organisation-log/organisation-log.component';
import { OrganisationLogService } from 'app/entities/organisation-log/organisation-log.service';
import { OrganisationLog } from 'app/shared/model/organisation-log.model';

describe('Component Tests', () => {
  describe('OrganisationLog Management Component', () => {
    let comp: OrganisationLogComponent;
    let fixture: ComponentFixture<OrganisationLogComponent>;
    let service: OrganisationLogService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OrganisationLogComponent],
        providers: []
      })
        .overrideTemplate(OrganisationLogComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OrganisationLogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OrganisationLogService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new OrganisationLog(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.organisationLogs[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
