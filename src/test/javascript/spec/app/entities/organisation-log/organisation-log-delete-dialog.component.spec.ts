import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { EsignharyanaTestModule } from '../../../test.module';
import { OrganisationLogDeleteDialogComponent } from 'app/entities/organisation-log/organisation-log-delete-dialog.component';
import { OrganisationLogService } from 'app/entities/organisation-log/organisation-log.service';

describe('Component Tests', () => {
  describe('OrganisationLog Management Delete Component', () => {
    let comp: OrganisationLogDeleteDialogComponent;
    let fixture: ComponentFixture<OrganisationLogDeleteDialogComponent>;
    let service: OrganisationLogService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OrganisationLogDeleteDialogComponent]
      })
        .overrideTemplate(OrganisationLogDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OrganisationLogDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OrganisationLogService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
