import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { EsignharyanaTestModule } from '../../../test.module';
import { UserApplicationDeleteDialogComponent } from 'app/entities/user-application/user-application-delete-dialog.component';
import { UserApplicationService } from 'app/entities/user-application/user-application.service';

describe('Component Tests', () => {
  describe('UserApplication Management Delete Component', () => {
    let comp: UserApplicationDeleteDialogComponent;
    let fixture: ComponentFixture<UserApplicationDeleteDialogComponent>;
    let service: UserApplicationService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [UserApplicationDeleteDialogComponent]
      })
        .overrideTemplate(UserApplicationDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserApplicationDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserApplicationService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
