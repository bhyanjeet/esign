import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { UserApplicationService } from 'app/entities/user-application/user-application.service';
import { IUserApplication, UserApplication } from 'app/shared/model/user-application.model';

describe('Service Tests', () => {
  describe('UserApplication Service', () => {
    let injector: TestBed;
    let service: UserApplicationService;
    let httpMock: HttpTestingController;
    let elemDefault: IUserApplication;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(UserApplicationService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new UserApplication(
        0,
        0,
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            createdOn: currentDate.format(DATE_FORMAT),
            updatedOn: currentDate.format(DATE_FORMAT),
            verifiedOn: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a UserApplication', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            createdOn: currentDate.format(DATE_FORMAT),
            updatedOn: currentDate.format(DATE_FORMAT),
            verifiedOn: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            createdOn: currentDate,
            updatedOn: currentDate,
            verifiedOn: currentDate
          },
          returnedFromService
        );
        service
          .create(new UserApplication(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a UserApplication', () => {
        const returnedFromService = Object.assign(
          {
            userId: 1,
            userLogin: 'BBBBBB',
            createdBy: 'BBBBBB',
            createdOn: currentDate.format(DATE_FORMAT),
            updatedBy: 'BBBBBB',
            updatedOn: currentDate.format(DATE_FORMAT),
            verifiedBy: 'BBBBBB',
            verifiedOn: currentDate.format(DATE_FORMAT),
            userCodeId: 'BBBBBB',
            applicationCodeId: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdOn: currentDate,
            updatedOn: currentDate,
            verifiedOn: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of UserApplication', () => {
        const returnedFromService = Object.assign(
          {
            userId: 1,
            userLogin: 'BBBBBB',
            createdBy: 'BBBBBB',
            createdOn: currentDate.format(DATE_FORMAT),
            updatedBy: 'BBBBBB',
            updatedOn: currentDate.format(DATE_FORMAT),
            verifiedBy: 'BBBBBB',
            verifiedOn: currentDate.format(DATE_FORMAT),
            userCodeId: 'BBBBBB',
            applicationCodeId: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            createdOn: currentDate,
            updatedOn: currentDate,
            verifiedOn: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a UserApplication', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
