import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { UserAuthDetailsUpdateComponent } from 'app/entities/user-auth-details/user-auth-details-update.component';
import { UserAuthDetailsService } from 'app/entities/user-auth-details/user-auth-details.service';
import { UserAuthDetails } from 'app/shared/model/user-auth-details.model';

describe('Component Tests', () => {
  describe('UserAuthDetails Management Update Component', () => {
    let comp: UserAuthDetailsUpdateComponent;
    let fixture: ComponentFixture<UserAuthDetailsUpdateComponent>;
    let service: UserAuthDetailsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [UserAuthDetailsUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(UserAuthDetailsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UserAuthDetailsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserAuthDetailsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserAuthDetails(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserAuthDetails();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
