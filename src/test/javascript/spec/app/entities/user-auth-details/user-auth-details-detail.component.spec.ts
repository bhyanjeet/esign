import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { UserAuthDetailsDetailComponent } from 'app/entities/user-auth-details/user-auth-details-detail.component';
import { UserAuthDetails } from 'app/shared/model/user-auth-details.model';

describe('Component Tests', () => {
  describe('UserAuthDetails Management Detail Component', () => {
    let comp: UserAuthDetailsDetailComponent;
    let fixture: ComponentFixture<UserAuthDetailsDetailComponent>;
    const route = ({ data: of({ userAuthDetails: new UserAuthDetails(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [UserAuthDetailsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(UserAuthDetailsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserAuthDetailsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load userAuthDetails on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.userAuthDetails).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
