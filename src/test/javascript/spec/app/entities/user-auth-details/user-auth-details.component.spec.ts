import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { EsignharyanaTestModule } from '../../../test.module';
import { UserAuthDetailsComponent } from 'app/entities/user-auth-details/user-auth-details.component';
import { UserAuthDetailsService } from 'app/entities/user-auth-details/user-auth-details.service';
import { UserAuthDetails } from 'app/shared/model/user-auth-details.model';

describe('Component Tests', () => {
  describe('UserAuthDetails Management Component', () => {
    let comp: UserAuthDetailsComponent;
    let fixture: ComponentFixture<UserAuthDetailsComponent>;
    let service: UserAuthDetailsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [UserAuthDetailsComponent]
      })
        .overrideTemplate(UserAuthDetailsComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UserAuthDetailsComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserAuthDetailsService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new UserAuthDetails(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.userAuthDetails && comp.userAuthDetails[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
