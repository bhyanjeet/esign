import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { DistrictMasterUpdateComponent } from 'app/entities/district-master/district-master-update.component';
import { DistrictMasterService } from 'app/entities/district-master/district-master.service';
import { DistrictMaster } from 'app/shared/model/district-master.model';

describe('Component Tests', () => {
  describe('DistrictMaster Management Update Component', () => {
    let comp: DistrictMasterUpdateComponent;
    let fixture: ComponentFixture<DistrictMasterUpdateComponent>;
    let service: DistrictMasterService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [DistrictMasterUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(DistrictMasterUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DistrictMasterUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DistrictMasterService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new DistrictMaster(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new DistrictMaster();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
