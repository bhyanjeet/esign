import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { DistrictMasterDetailComponent } from 'app/entities/district-master/district-master-detail.component';
import { DistrictMaster } from 'app/shared/model/district-master.model';

describe('Component Tests', () => {
  describe('DistrictMaster Management Detail Component', () => {
    let comp: DistrictMasterDetailComponent;
    let fixture: ComponentFixture<DistrictMasterDetailComponent>;
    const route = ({ data: of({ districtMaster: new DistrictMaster(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [DistrictMasterDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(DistrictMasterDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DistrictMasterDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.districtMaster).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
