import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { EsignharyanaTestModule } from '../../../test.module';
import { DistrictMasterDeleteDialogComponent } from 'app/entities/district-master/district-master-delete-dialog.component';
import { DistrictMasterService } from 'app/entities/district-master/district-master.service';

describe('Component Tests', () => {
  describe('DistrictMaster Management Delete Component', () => {
    let comp: DistrictMasterDeleteDialogComponent;
    let fixture: ComponentFixture<DistrictMasterDeleteDialogComponent>;
    let service: DistrictMasterService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [DistrictMasterDeleteDialogComponent]
      })
        .overrideTemplate(DistrictMasterDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DistrictMasterDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DistrictMasterService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
