import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { ApplicationMasterService } from 'app/entities/application-master/application-master.service';
import { IApplicationMaster, ApplicationMaster } from 'app/shared/model/application-master.model';

describe('Service Tests', () => {
  describe('ApplicationMaster Service', () => {
    let injector: TestBed;
    let service: ApplicationMasterService;
    let httpMock: HttpTestingController;
    let elemDefault: IApplicationMaster;
    let expectedResult: IApplicationMaster | IApplicationMaster[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(ApplicationMasterService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new ApplicationMaster(
        0,
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            createdOn: currentDate.format(DATE_FORMAT),
            lastUpdatedOn: currentDate.format(DATE_FORMAT),
            verifiedOn: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ApplicationMaster', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            createdOn: currentDate.format(DATE_FORMAT),
            lastUpdatedOn: currentDate.format(DATE_FORMAT),
            verifiedOn: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdOn: currentDate,
            lastUpdatedOn: currentDate,
            verifiedOn: currentDate
          },
          returnedFromService
        );

        service.create(new ApplicationMaster()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ApplicationMaster', () => {
        const returnedFromService = Object.assign(
          {
            applicationName: 'BBBBBB',
            createdBy: 'BBBBBB',
            createdOn: currentDate.format(DATE_FORMAT),
            lastUpdatedBy: 'BBBBBB',
            lastUpdatedOn: currentDate.format(DATE_FORMAT),
            verifiedBy: 'BBBBBB',
            verifiedOn: currentDate.format(DATE_FORMAT),
            remarks: 'BBBBBB',
            applicationURL: 'BBBBBB',
            applicationStatus: 'BBBBBB',
            applicationIdCode: 'BBBBBB',
            status: 'BBBBBB',
            certPublicKey: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdOn: currentDate,
            lastUpdatedOn: currentDate,
            verifiedOn: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ApplicationMaster', () => {
        const returnedFromService = Object.assign(
          {
            applicationName: 'BBBBBB',
            createdBy: 'BBBBBB',
            createdOn: currentDate.format(DATE_FORMAT),
            lastUpdatedBy: 'BBBBBB',
            lastUpdatedOn: currentDate.format(DATE_FORMAT),
            verifiedBy: 'BBBBBB',
            verifiedOn: currentDate.format(DATE_FORMAT),
            remarks: 'BBBBBB',
            applicationURL: 'BBBBBB',
            applicationStatus: 'BBBBBB',
            applicationIdCode: 'BBBBBB',
            status: 'BBBBBB',
            certPublicKey: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdOn: currentDate,
            lastUpdatedOn: currentDate,
            verifiedOn: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ApplicationMaster', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
