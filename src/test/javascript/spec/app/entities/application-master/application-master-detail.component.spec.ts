import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { ApplicationMasterDetailComponent } from 'app/entities/application-master/application-master-detail.component';
import { ApplicationMaster } from 'app/shared/model/application-master.model';

describe('Component Tests', () => {
  describe('ApplicationMaster Management Detail Component', () => {
    let comp: ApplicationMasterDetailComponent;
    let fixture: ComponentFixture<ApplicationMasterDetailComponent>;
    const route = ({ data: of({ applicationMaster: new ApplicationMaster(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [ApplicationMasterDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ApplicationMasterDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ApplicationMasterDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load applicationMaster on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.applicationMaster).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
