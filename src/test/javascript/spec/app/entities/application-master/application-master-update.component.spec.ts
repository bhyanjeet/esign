import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { ApplicationMasterUpdateComponent } from 'app/entities/application-master/application-master-update.component';
import { ApplicationMasterService } from 'app/entities/application-master/application-master.service';
import { ApplicationMaster } from 'app/shared/model/application-master.model';

describe('Component Tests', () => {
  describe('ApplicationMaster Management Update Component', () => {
    let comp: ApplicationMasterUpdateComponent;
    let fixture: ComponentFixture<ApplicationMasterUpdateComponent>;
    let service: ApplicationMasterService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [ApplicationMasterUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ApplicationMasterUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ApplicationMasterUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ApplicationMasterService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ApplicationMaster(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ApplicationMaster();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
