import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { EsignharyanaTestModule } from '../../../test.module';
import { OrganisationDocumentDeleteDialogComponent } from 'app/entities/organisation-document/organisation-document-delete-dialog.component';
import { OrganisationDocumentService } from 'app/entities/organisation-document/organisation-document.service';

describe('Component Tests', () => {
  describe('OrganisationDocument Management Delete Component', () => {
    let comp: OrganisationDocumentDeleteDialogComponent;
    let fixture: ComponentFixture<OrganisationDocumentDeleteDialogComponent>;
    let service: OrganisationDocumentService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OrganisationDocumentDeleteDialogComponent]
      })
        .overrideTemplate(OrganisationDocumentDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OrganisationDocumentDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OrganisationDocumentService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
