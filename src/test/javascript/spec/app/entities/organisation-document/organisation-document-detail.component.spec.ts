import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { OrganisationDocumentDetailComponent } from 'app/entities/organisation-document/organisation-document-detail.component';
import { OrganisationDocument } from 'app/shared/model/organisation-document.model';

describe('Component Tests', () => {
  describe('OrganisationDocument Management Detail Component', () => {
    let comp: OrganisationDocumentDetailComponent;
    let fixture: ComponentFixture<OrganisationDocumentDetailComponent>;
    const route = ({ data: of({ organisationDocument: new OrganisationDocument(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OrganisationDocumentDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(OrganisationDocumentDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OrganisationDocumentDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.organisationDocument).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
