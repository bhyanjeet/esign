import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { OrganisationDocumentUpdateComponent } from 'app/entities/organisation-document/organisation-document-update.component';
import { OrganisationDocumentService } from 'app/entities/organisation-document/organisation-document.service';
import { OrganisationDocument } from 'app/shared/model/organisation-document.model';

describe('Component Tests', () => {
  describe('OrganisationDocument Management Update Component', () => {
    let comp: OrganisationDocumentUpdateComponent;
    let fixture: ComponentFixture<OrganisationDocumentUpdateComponent>;
    let service: OrganisationDocumentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OrganisationDocumentUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(OrganisationDocumentUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OrganisationDocumentUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OrganisationDocumentService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrganisationDocument(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrganisationDocument();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
