import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { EsignTransationDetailComponent } from 'app/entities/esign-transation/esign-transation-detail.component';
import { EsignTransation } from 'app/shared/model/esign-transation.model';

describe('Component Tests', () => {
  describe('EsignTransation Management Detail Component', () => {
    let comp: EsignTransationDetailComponent;
    let fixture: ComponentFixture<EsignTransationDetailComponent>;
    const route = ({ data: of({ esignTransation: new EsignTransation(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [EsignTransationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(EsignTransationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(EsignTransationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.esignTransation).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
