import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { EsignTransationUpdateComponent } from 'app/entities/esign-transation/esign-transation-update.component';
import { EsignTransationService } from 'app/entities/esign-transation/esign-transation.service';
import { EsignTransation } from 'app/shared/model/esign-transation.model';

describe('Component Tests', () => {
  describe('EsignTransation Management Update Component', () => {
    let comp: EsignTransationUpdateComponent;
    let fixture: ComponentFixture<EsignTransationUpdateComponent>;
    let service: EsignTransationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [EsignTransationUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(EsignTransationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EsignTransationUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EsignTransationService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new EsignTransation(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new EsignTransation();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
