import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { EsignTransationService } from 'app/entities/esign-transation/esign-transation.service';
import { IEsignTransation, EsignTransation } from 'app/shared/model/esign-transation.model';

describe('Service Tests', () => {
  describe('EsignTransation Service', () => {
    let injector: TestBed;
    let service: EsignTransationService;
    let httpMock: HttpTestingController;
    let elemDefault: IEsignTransation;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(EsignTransationService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new EsignTransation(
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            requestDate: currentDate.format(DATE_FORMAT),
            responseDate: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a EsignTransation', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            requestDate: currentDate.format(DATE_FORMAT),
            responseDate: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            requestDate: currentDate,
            responseDate: currentDate
          },
          returnedFromService
        );
        service
          .create(new EsignTransation(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a EsignTransation', () => {
        const returnedFromService = Object.assign(
          {
            txnId: 'BBBBBB',
            userCodeId: 'BBBBBB',
            applicationCodeId: 'BBBBBB',
            orgResponseSuccessURL: 'BBBBBB',
            orgResponseFailURL: 'BBBBBB',
            orgStatus: 'BBBBBB',
            requestDate: currentDate.format(DATE_FORMAT),
            organisationIdCode: 'BBBBBB',
            signerId: 'BBBBBB',
            requestType: 'BBBBBB',
            requestStatus: 'BBBBBB',
            responseType: 'BBBBBB',
            responseStatus: 'BBBBBB',
            responseCode: 'BBBBBB',
            requestXml: 'BBBBBB',
            responseXml: 'BBBBBB',
            responseDate: currentDate.format(DATE_FORMAT),
            ts: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            requestDate: currentDate,
            responseDate: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of EsignTransation', () => {
        const returnedFromService = Object.assign(
          {
            txnId: 'BBBBBB',
            userCodeId: 'BBBBBB',
            applicationCodeId: 'BBBBBB',
            orgResponseSuccessURL: 'BBBBBB',
            orgResponseFailURL: 'BBBBBB',
            orgStatus: 'BBBBBB',
            requestDate: currentDate.format(DATE_FORMAT),
            organisationIdCode: 'BBBBBB',
            signerId: 'BBBBBB',
            requestType: 'BBBBBB',
            requestStatus: 'BBBBBB',
            responseType: 'BBBBBB',
            responseStatus: 'BBBBBB',
            responseCode: 'BBBBBB',
            requestXml: 'BBBBBB',
            responseXml: 'BBBBBB',
            responseDate: currentDate.format(DATE_FORMAT),
            ts: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            requestDate: currentDate,
            responseDate: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a EsignTransation', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
