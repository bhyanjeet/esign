import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { EsignErrorUpdateComponent } from 'app/entities/esign-error/esign-error-update.component';
import { EsignErrorService } from 'app/entities/esign-error/esign-error.service';
import { EsignError } from 'app/shared/model/esign-error.model';

describe('Component Tests', () => {
  describe('EsignError Management Update Component', () => {
    let comp: EsignErrorUpdateComponent;
    let fixture: ComponentFixture<EsignErrorUpdateComponent>;
    let service: EsignErrorService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [EsignErrorUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(EsignErrorUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EsignErrorUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EsignErrorService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new EsignError(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new EsignError();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
