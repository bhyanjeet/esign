import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { EsignharyanaTestModule } from '../../../test.module';
import { EsignErrorComponent } from 'app/entities/esign-error/esign-error.component';
import { EsignErrorService } from 'app/entities/esign-error/esign-error.service';
import { EsignError } from 'app/shared/model/esign-error.model';

describe('Component Tests', () => {
  describe('EsignError Management Component', () => {
    let comp: EsignErrorComponent;
    let fixture: ComponentFixture<EsignErrorComponent>;
    let service: EsignErrorService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [EsignErrorComponent]
      })
        .overrideTemplate(EsignErrorComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EsignErrorComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EsignErrorService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new EsignError(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.esignErrors && comp.esignErrors[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
