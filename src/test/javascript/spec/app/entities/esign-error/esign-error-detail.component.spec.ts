import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { EsignErrorDetailComponent } from 'app/entities/esign-error/esign-error-detail.component';
import { EsignError } from 'app/shared/model/esign-error.model';

describe('Component Tests', () => {
  describe('EsignError Management Detail Component', () => {
    let comp: EsignErrorDetailComponent;
    let fixture: ComponentFixture<EsignErrorDetailComponent>;
    const route = ({ data: of({ esignError: new EsignError(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [EsignErrorDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(EsignErrorDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(EsignErrorDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load esignError on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.esignError).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
