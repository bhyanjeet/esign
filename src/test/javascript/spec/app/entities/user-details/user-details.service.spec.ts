import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { UserDetailsService } from 'app/entities/user-details/user-details.service';
import { IUserDetails, UserDetails } from 'app/shared/model/user-details.model';

describe('Service Tests', () => {
  describe('UserDetails Service', () => {
    let injector: TestBed;
    let service: UserDetailsService;
    let httpMock: HttpTestingController;
    let elemDefault: IUserDetails;
    let expectedResult: IUserDetails | IUserDetails[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(UserDetailsService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new UserDetails(
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            createdOn: currentDate.format(DATE_FORMAT),
            lastUpdatedOn: currentDate.format(DATE_FORMAT),
            verifiedOn: currentDate.format(DATE_FORMAT),
            dob: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a UserDetails', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            createdOn: currentDate.format(DATE_FORMAT),
            lastUpdatedOn: currentDate.format(DATE_FORMAT),
            verifiedOn: currentDate.format(DATE_FORMAT),
            dob: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdOn: currentDate,
            lastUpdatedOn: currentDate,
            verifiedOn: currentDate,
            dob: currentDate
          },
          returnedFromService
        );

        service.create(new UserDetails()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a UserDetails', () => {
        const returnedFromService = Object.assign(
          {
            userFullName: 'BBBBBB',
            userPhoneMobile: 'BBBBBB',
            userPhoneLandline: 'BBBBBB',
            userOfficialEmail: 'BBBBBB',
            userAlternativeEmail: 'BBBBBB',
            userLoginPassword: 'BBBBBB',
            userLoginPasswordSalt: 'BBBBBB',
            userTransactionPassword: 'BBBBBB',
            userTransactionPasswordSalt: 'BBBBBB',
            createdBy: 'BBBBBB',
            createdOn: currentDate.format(DATE_FORMAT),
            lastUpdatedBy: 'BBBBBB',
            lastUpdatedOn: currentDate.format(DATE_FORMAT),
            verifiedBy: 'BBBBBB',
            verifiedOn: currentDate.format(DATE_FORMAT),
            remarks: 'BBBBBB',
            userIdCode: 'BBBBBB',
            status: 'BBBBBB',
            username: 'BBBBBB',
            userPIN: 'BBBBBB',
            organisationUnit: 'BBBBBB',
            address: 'BBBBBB',
            counrty: 'BBBBBB',
            postalCode: 'BBBBBB',
            photograph: 'BBBBBB',
            dob: currentDate.format(DATE_FORMAT),
            gender: 'BBBBBB',
            pan: 'BBBBBB',
            aadhaar: 'BBBBBB',
            panAttachment: 'BBBBBB',
            employeeId: 'BBBBBB',
            employeeCardAttachment: 'BBBBBB',
            signerId: 'BBBBBB',
            lastAction: 'BBBBBB',
            accountStatus: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdOn: currentDate,
            lastUpdatedOn: currentDate,
            verifiedOn: currentDate,
            dob: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of UserDetails', () => {
        const returnedFromService = Object.assign(
          {
            userFullName: 'BBBBBB',
            userPhoneMobile: 'BBBBBB',
            userPhoneLandline: 'BBBBBB',
            userOfficialEmail: 'BBBBBB',
            userAlternativeEmail: 'BBBBBB',
            userLoginPassword: 'BBBBBB',
            userLoginPasswordSalt: 'BBBBBB',
            userTransactionPassword: 'BBBBBB',
            userTransactionPasswordSalt: 'BBBBBB',
            createdBy: 'BBBBBB',
            createdOn: currentDate.format(DATE_FORMAT),
            lastUpdatedBy: 'BBBBBB',
            lastUpdatedOn: currentDate.format(DATE_FORMAT),
            verifiedBy: 'BBBBBB',
            verifiedOn: currentDate.format(DATE_FORMAT),
            remarks: 'BBBBBB',
            userIdCode: 'BBBBBB',
            status: 'BBBBBB',
            username: 'BBBBBB',
            userPIN: 'BBBBBB',
            organisationUnit: 'BBBBBB',
            address: 'BBBBBB',
            counrty: 'BBBBBB',
            postalCode: 'BBBBBB',
            photograph: 'BBBBBB',
            dob: currentDate.format(DATE_FORMAT),
            gender: 'BBBBBB',
            pan: 'BBBBBB',
            aadhaar: 'BBBBBB',
            panAttachment: 'BBBBBB',
            employeeId: 'BBBBBB',
            employeeCardAttachment: 'BBBBBB',
            signerId: 'BBBBBB',
            lastAction: 'BBBBBB',
            accountStatus: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdOn: currentDate,
            lastUpdatedOn: currentDate,
            verifiedOn: currentDate,
            dob: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a UserDetails', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
