import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { UserLogsService } from 'app/entities/user-logs/user-logs.service';
import { IUserLogs, UserLogs } from 'app/shared/model/user-logs.model';

describe('Service Tests', () => {
  describe('UserLogs Service', () => {
    let injector: TestBed;
    let service: UserLogsService;
    let httpMock: HttpTestingController;
    let elemDefault: IUserLogs;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(UserLogsService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new UserLogs(0, 'AAAAAAA', 'AAAAAAA', 0, currentDate, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            actionTakenOnDate: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a UserLogs', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            actionTakenOnDate: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            actionTakenOnDate: currentDate
          },
          returnedFromService
        );
        service
          .create(new UserLogs(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a UserLogs', () => {
        const returnedFromService = Object.assign(
          {
            actionTaken: 'BBBBBB',
            actionTakenBy: 'BBBBBB',
            actionTakenOnUser: 1,
            actionTakenOnDate: currentDate.format(DATE_FORMAT),
            remarks: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            actionTakenOnDate: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of UserLogs', () => {
        const returnedFromService = Object.assign(
          {
            actionTaken: 'BBBBBB',
            actionTakenBy: 'BBBBBB',
            actionTakenOnUser: 1,
            actionTakenOnDate: currentDate.format(DATE_FORMAT),
            remarks: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            actionTakenOnDate: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a UserLogs', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
