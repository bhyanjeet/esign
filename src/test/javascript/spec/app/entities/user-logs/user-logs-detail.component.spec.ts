import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { UserLogsDetailComponent } from 'app/entities/user-logs/user-logs-detail.component';
import { UserLogs } from 'app/shared/model/user-logs.model';

describe('Component Tests', () => {
  describe('UserLogs Management Detail Component', () => {
    let comp: UserLogsDetailComponent;
    let fixture: ComponentFixture<UserLogsDetailComponent>;
    const route = ({ data: of({ userLogs: new UserLogs(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [UserLogsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(UserLogsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserLogsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.userLogs).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
