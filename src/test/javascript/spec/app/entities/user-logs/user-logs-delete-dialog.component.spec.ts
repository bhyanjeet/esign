import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { EsignharyanaTestModule } from '../../../test.module';
import { UserLogsDeleteDialogComponent } from 'app/entities/user-logs/user-logs-delete-dialog.component';
import { UserLogsService } from 'app/entities/user-logs/user-logs.service';

describe('Component Tests', () => {
  describe('UserLogs Management Delete Component', () => {
    let comp: UserLogsDeleteDialogComponent;
    let fixture: ComponentFixture<UserLogsDeleteDialogComponent>;
    let service: UserLogsService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [UserLogsDeleteDialogComponent]
      })
        .overrideTemplate(UserLogsDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserLogsDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserLogsService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
