import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { UserLogsUpdateComponent } from 'app/entities/user-logs/user-logs-update.component';
import { UserLogsService } from 'app/entities/user-logs/user-logs.service';
import { UserLogs } from 'app/shared/model/user-logs.model';

describe('Component Tests', () => {
  describe('UserLogs Management Update Component', () => {
    let comp: UserLogsUpdateComponent;
    let fixture: ComponentFixture<UserLogsUpdateComponent>;
    let service: UserLogsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [UserLogsUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(UserLogsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UserLogsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserLogsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserLogs(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserLogs();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
