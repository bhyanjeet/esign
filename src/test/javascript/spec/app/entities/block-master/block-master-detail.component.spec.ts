import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { BlockMasterDetailComponent } from 'app/entities/block-master/block-master-detail.component';
import { BlockMaster } from 'app/shared/model/block-master.model';

describe('Component Tests', () => {
  describe('BlockMaster Management Detail Component', () => {
    let comp: BlockMasterDetailComponent;
    let fixture: ComponentFixture<BlockMasterDetailComponent>;
    const route = ({ data: of({ blockMaster: new BlockMaster(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [BlockMasterDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BlockMasterDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BlockMasterDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.blockMaster).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
