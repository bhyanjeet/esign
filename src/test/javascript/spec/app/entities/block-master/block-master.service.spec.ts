import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { BlockMasterService } from 'app/entities/block-master/block-master.service';
import { IBlockMaster, BlockMaster } from 'app/shared/model/block-master.model';

describe('Service Tests', () => {
  describe('BlockMaster Service', () => {
    let injector: TestBed;
    let service: BlockMasterService;
    let httpMock: HttpTestingController;
    let elemDefault: IBlockMaster;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(BlockMasterService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new BlockMaster(0, 'AAAAAAA', 'AAAAAAA', currentDate, 'AAAAAAA', currentDate, 'AAAAAAA', currentDate, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            createdOn: currentDate.format(DATE_FORMAT),
            lastUpdatedOn: currentDate.format(DATE_FORMAT),
            verifiedOn: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a BlockMaster', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            createdOn: currentDate.format(DATE_FORMAT),
            lastUpdatedOn: currentDate.format(DATE_FORMAT),
            verifiedOn: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            createdOn: currentDate,
            lastUpdatedOn: currentDate,
            verifiedOn: currentDate
          },
          returnedFromService
        );
        service
          .create(new BlockMaster(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a BlockMaster', () => {
        const returnedFromService = Object.assign(
          {
            blockCode: 'BBBBBB',
            blockName: 'BBBBBB',
            createdOn: currentDate.format(DATE_FORMAT),
            lastUpdatedBy: 'BBBBBB',
            lastUpdatedOn: currentDate.format(DATE_FORMAT),
            verifiedBy: 'BBBBBB',
            verifiedOn: currentDate.format(DATE_FORMAT),
            remarks: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdOn: currentDate,
            lastUpdatedOn: currentDate,
            verifiedOn: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of BlockMaster', () => {
        const returnedFromService = Object.assign(
          {
            blockCode: 'BBBBBB',
            blockName: 'BBBBBB',
            createdOn: currentDate.format(DATE_FORMAT),
            lastUpdatedBy: 'BBBBBB',
            lastUpdatedOn: currentDate.format(DATE_FORMAT),
            verifiedBy: 'BBBBBB',
            verifiedOn: currentDate.format(DATE_FORMAT),
            remarks: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            createdOn: currentDate,
            lastUpdatedOn: currentDate,
            verifiedOn: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a BlockMaster', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
