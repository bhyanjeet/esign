import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { OrganisationTypeMasterDetailComponent } from 'app/entities/organisation-type-master/organisation-type-master-detail.component';
import { OrganisationTypeMaster } from 'app/shared/model/organisation-type-master.model';

describe('Component Tests', () => {
  describe('OrganisationTypeMaster Management Detail Component', () => {
    let comp: OrganisationTypeMasterDetailComponent;
    let fixture: ComponentFixture<OrganisationTypeMasterDetailComponent>;
    const route = ({ data: of({ organisationTypeMaster: new OrganisationTypeMaster(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OrganisationTypeMasterDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(OrganisationTypeMasterDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OrganisationTypeMasterDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.organisationTypeMaster).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
