import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { EsignharyanaTestModule } from '../../../test.module';
import { OrganisationTypeMasterDeleteDialogComponent } from 'app/entities/organisation-type-master/organisation-type-master-delete-dialog.component';
import { OrganisationTypeMasterService } from 'app/entities/organisation-type-master/organisation-type-master.service';

describe('Component Tests', () => {
  describe('OrganisationTypeMaster Management Delete Component', () => {
    let comp: OrganisationTypeMasterDeleteDialogComponent;
    let fixture: ComponentFixture<OrganisationTypeMasterDeleteDialogComponent>;
    let service: OrganisationTypeMasterService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OrganisationTypeMasterDeleteDialogComponent]
      })
        .overrideTemplate(OrganisationTypeMasterDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OrganisationTypeMasterDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OrganisationTypeMasterService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
