import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { OrganisationTypeMasterUpdateComponent } from 'app/entities/organisation-type-master/organisation-type-master-update.component';
import { OrganisationTypeMasterService } from 'app/entities/organisation-type-master/organisation-type-master.service';
import { OrganisationTypeMaster } from 'app/shared/model/organisation-type-master.model';

describe('Component Tests', () => {
  describe('OrganisationTypeMaster Management Update Component', () => {
    let comp: OrganisationTypeMasterUpdateComponent;
    let fixture: ComponentFixture<OrganisationTypeMasterUpdateComponent>;
    let service: OrganisationTypeMasterService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OrganisationTypeMasterUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(OrganisationTypeMasterUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OrganisationTypeMasterUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OrganisationTypeMasterService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrganisationTypeMaster(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrganisationTypeMaster();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
