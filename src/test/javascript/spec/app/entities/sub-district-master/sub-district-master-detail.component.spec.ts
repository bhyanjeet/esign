import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { SubDistrictMasterDetailComponent } from 'app/entities/sub-district-master/sub-district-master-detail.component';
import { SubDistrictMaster } from 'app/shared/model/sub-district-master.model';

describe('Component Tests', () => {
  describe('SubDistrictMaster Management Detail Component', () => {
    let comp: SubDistrictMasterDetailComponent;
    let fixture: ComponentFixture<SubDistrictMasterDetailComponent>;
    const route = ({ data: of({ subDistrictMaster: new SubDistrictMaster(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [SubDistrictMasterDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SubDistrictMasterDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SubDistrictMasterDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.subDistrictMaster).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
