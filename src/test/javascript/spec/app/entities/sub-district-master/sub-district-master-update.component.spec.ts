import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { SubDistrictMasterUpdateComponent } from 'app/entities/sub-district-master/sub-district-master-update.component';
import { SubDistrictMasterService } from 'app/entities/sub-district-master/sub-district-master.service';
import { SubDistrictMaster } from 'app/shared/model/sub-district-master.model';

describe('Component Tests', () => {
  describe('SubDistrictMaster Management Update Component', () => {
    let comp: SubDistrictMasterUpdateComponent;
    let fixture: ComponentFixture<SubDistrictMasterUpdateComponent>;
    let service: SubDistrictMasterService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [SubDistrictMasterUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SubDistrictMasterUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SubDistrictMasterUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SubDistrictMasterService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new SubDistrictMaster(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new SubDistrictMaster();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
