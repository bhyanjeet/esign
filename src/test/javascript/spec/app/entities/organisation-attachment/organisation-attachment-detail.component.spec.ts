import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { OrganisationAttachmentDetailComponent } from 'app/entities/organisation-attachment/organisation-attachment-detail.component';
import { OrganisationAttachment } from 'app/shared/model/organisation-attachment.model';

describe('Component Tests', () => {
  describe('OrganisationAttachment Management Detail Component', () => {
    let comp: OrganisationAttachmentDetailComponent;
    let fixture: ComponentFixture<OrganisationAttachmentDetailComponent>;
    const route = ({ data: of({ organisationAttachment: new OrganisationAttachment(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OrganisationAttachmentDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(OrganisationAttachmentDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OrganisationAttachmentDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.organisationAttachment).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
