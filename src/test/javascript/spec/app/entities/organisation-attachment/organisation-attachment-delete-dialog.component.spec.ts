import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { EsignharyanaTestModule } from '../../../test.module';
import { OrganisationAttachmentDeleteDialogComponent } from 'app/entities/organisation-attachment/organisation-attachment-delete-dialog.component';
import { OrganisationAttachmentService } from 'app/entities/organisation-attachment/organisation-attachment.service';

describe('Component Tests', () => {
  describe('OrganisationAttachment Management Delete Component', () => {
    let comp: OrganisationAttachmentDeleteDialogComponent;
    let fixture: ComponentFixture<OrganisationAttachmentDeleteDialogComponent>;
    let service: OrganisationAttachmentService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OrganisationAttachmentDeleteDialogComponent]
      })
        .overrideTemplate(OrganisationAttachmentDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OrganisationAttachmentDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OrganisationAttachmentService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
