import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { OrganisationAttachmentUpdateComponent } from 'app/entities/organisation-attachment/organisation-attachment-update.component';
import { OrganisationAttachmentService } from 'app/entities/organisation-attachment/organisation-attachment.service';
import { OrganisationAttachment } from 'app/shared/model/organisation-attachment.model';

describe('Component Tests', () => {
  describe('OrganisationAttachment Management Update Component', () => {
    let comp: OrganisationAttachmentUpdateComponent;
    let fixture: ComponentFixture<OrganisationAttachmentUpdateComponent>;
    let service: OrganisationAttachmentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OrganisationAttachmentUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(OrganisationAttachmentUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OrganisationAttachmentUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OrganisationAttachmentService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrganisationAttachment(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrganisationAttachment();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
