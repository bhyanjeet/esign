import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { OrganisationMasterUpdateComponent } from 'app/entities/organisation-master/organisation-master-update.component';
import { OrganisationMasterService } from 'app/entities/organisation-master/organisation-master.service';
import { OrganisationMaster } from 'app/shared/model/organisation-master.model';

describe('Component Tests', () => {
  describe('OrganisationMaster Management Update Component', () => {
    let comp: OrganisationMasterUpdateComponent;
    let fixture: ComponentFixture<OrganisationMasterUpdateComponent>;
    let service: OrganisationMasterService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OrganisationMasterUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(OrganisationMasterUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OrganisationMasterUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OrganisationMasterService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrganisationMaster(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrganisationMaster();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
