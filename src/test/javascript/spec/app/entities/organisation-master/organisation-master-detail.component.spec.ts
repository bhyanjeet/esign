import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { OrganisationMasterDetailComponent } from 'app/entities/organisation-master/organisation-master-detail.component';
import { OrganisationMaster } from 'app/shared/model/organisation-master.model';

describe('Component Tests', () => {
  describe('OrganisationMaster Management Detail Component', () => {
    let comp: OrganisationMasterDetailComponent;
    let fixture: ComponentFixture<OrganisationMasterDetailComponent>;
    const route = ({ data: of({ organisationMaster: new OrganisationMaster(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OrganisationMasterDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(OrganisationMasterDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OrganisationMasterDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load organisationMaster on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.organisationMaster).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
