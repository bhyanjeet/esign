import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { OrganisationMasterService } from 'app/entities/organisation-master/organisation-master.service';
import { IOrganisationMaster, OrganisationMaster } from 'app/shared/model/organisation-master.model';

describe('Service Tests', () => {
  describe('OrganisationMaster Service', () => {
    let injector: TestBed;
    let service: OrganisationMasterService;
    let httpMock: HttpTestingController;
    let elemDefault: IOrganisationMaster;
    let expectedResult: IOrganisationMaster | IOrganisationMaster[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(OrganisationMasterService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new OrganisationMaster(
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            createdOn: currentDate.format(DATE_FORMAT),
            lastUpdatedOn: currentDate.format(DATE_FORMAT),
            verifiedOn: currentDate.format(DATE_FORMAT),
            dateOfBirth: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a OrganisationMaster', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            createdOn: currentDate.format(DATE_FORMAT),
            lastUpdatedOn: currentDate.format(DATE_FORMAT),
            verifiedOn: currentDate.format(DATE_FORMAT),
            dateOfBirth: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdOn: currentDate,
            lastUpdatedOn: currentDate,
            verifiedOn: currentDate,
            dateOfBirth: currentDate
          },
          returnedFromService
        );

        service.create(new OrganisationMaster()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a OrganisationMaster', () => {
        const returnedFromService = Object.assign(
          {
            organisationName: 'BBBBBB',
            organisationAbbreviation: 'BBBBBB',
            organisationCorrespondenceEmail: 'BBBBBB',
            organisationCorrespondencePhone1: 'BBBBBB',
            organisationCorrespondencePhone2: 'BBBBBB',
            organisationCorrespondenceAddress: 'BBBBBB',
            organisationWebsite: 'BBBBBB',
            organisationNodalOfficerName: 'BBBBBB',
            organisationNodalOfficerPhoneMobile: 'BBBBBB',
            organisationNodalOfficerPhoneLandline: 'BBBBBB',
            nodalOfficerOfficialEmail: 'BBBBBB',
            organisationNodalOfficerAlternativeEmail: 'BBBBBB',
            createdBy: 'BBBBBB',
            createdOn: currentDate.format(DATE_FORMAT),
            lastUpdatedBy: 'BBBBBB',
            lastUpdatedOn: currentDate.format(DATE_FORMAT),
            verifiedBy: 'BBBBBB',
            verifiedOn: currentDate.format(DATE_FORMAT),
            remarks: 'BBBBBB',
            userId: 1,
            stage: 'BBBBBB',
            status: 'BBBBBB',
            organisationIdCode: 'BBBBBB',
            organisationEmployeeId: 'BBBBBB',
            dateOfBirth: currentDate.format(DATE_FORMAT),
            gender: 'BBBBBB',
            pan: 'BBBBBB',
            aadhaar: 'BBBBBB',
            orgnisationEmpolyeeCardAttchment: 'BBBBBB',
            panAttachment: 'BBBBBB',
            photographAttachment: 'BBBBBB',
            aadhaarAttachment: 'BBBBBB',
            pinCode: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdOn: currentDate,
            lastUpdatedOn: currentDate,
            verifiedOn: currentDate,
            dateOfBirth: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of OrganisationMaster', () => {
        const returnedFromService = Object.assign(
          {
            organisationName: 'BBBBBB',
            organisationAbbreviation: 'BBBBBB',
            organisationCorrespondenceEmail: 'BBBBBB',
            organisationCorrespondencePhone1: 'BBBBBB',
            organisationCorrespondencePhone2: 'BBBBBB',
            organisationCorrespondenceAddress: 'BBBBBB',
            organisationWebsite: 'BBBBBB',
            organisationNodalOfficerName: 'BBBBBB',
            organisationNodalOfficerPhoneMobile: 'BBBBBB',
            organisationNodalOfficerPhoneLandline: 'BBBBBB',
            nodalOfficerOfficialEmail: 'BBBBBB',
            organisationNodalOfficerAlternativeEmail: 'BBBBBB',
            createdBy: 'BBBBBB',
            createdOn: currentDate.format(DATE_FORMAT),
            lastUpdatedBy: 'BBBBBB',
            lastUpdatedOn: currentDate.format(DATE_FORMAT),
            verifiedBy: 'BBBBBB',
            verifiedOn: currentDate.format(DATE_FORMAT),
            remarks: 'BBBBBB',
            userId: 1,
            stage: 'BBBBBB',
            status: 'BBBBBB',
            organisationIdCode: 'BBBBBB',
            organisationEmployeeId: 'BBBBBB',
            dateOfBirth: currentDate.format(DATE_FORMAT),
            gender: 'BBBBBB',
            pan: 'BBBBBB',
            aadhaar: 'BBBBBB',
            orgnisationEmpolyeeCardAttchment: 'BBBBBB',
            panAttachment: 'BBBBBB',
            photographAttachment: 'BBBBBB',
            aadhaarAttachment: 'BBBBBB',
            pinCode: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdOn: currentDate,
            lastUpdatedOn: currentDate,
            verifiedOn: currentDate,
            dateOfBirth: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a OrganisationMaster', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
