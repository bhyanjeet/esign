import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { EsignharyanaTestModule } from '../../../test.module';
import { ApplicationLogsDeleteDialogComponent } from 'app/entities/application-logs/application-logs-delete-dialog.component';
import { ApplicationLogsService } from 'app/entities/application-logs/application-logs.service';

describe('Component Tests', () => {
  describe('ApplicationLogs Management Delete Component', () => {
    let comp: ApplicationLogsDeleteDialogComponent;
    let fixture: ComponentFixture<ApplicationLogsDeleteDialogComponent>;
    let service: ApplicationLogsService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [ApplicationLogsDeleteDialogComponent]
      })
        .overrideTemplate(ApplicationLogsDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ApplicationLogsDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ApplicationLogsService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
