import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { ApplicationLogsDetailComponent } from 'app/entities/application-logs/application-logs-detail.component';
import { ApplicationLogs } from 'app/shared/model/application-logs.model';

describe('Component Tests', () => {
  describe('ApplicationLogs Management Detail Component', () => {
    let comp: ApplicationLogsDetailComponent;
    let fixture: ComponentFixture<ApplicationLogsDetailComponent>;
    const route = ({ data: of({ applicationLogs: new ApplicationLogs(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [ApplicationLogsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ApplicationLogsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ApplicationLogsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.applicationLogs).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
