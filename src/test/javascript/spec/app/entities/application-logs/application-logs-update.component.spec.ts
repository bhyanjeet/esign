import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { ApplicationLogsUpdateComponent } from 'app/entities/application-logs/application-logs-update.component';
import { ApplicationLogsService } from 'app/entities/application-logs/application-logs.service';
import { ApplicationLogs } from 'app/shared/model/application-logs.model';

describe('Component Tests', () => {
  describe('ApplicationLogs Management Update Component', () => {
    let comp: ApplicationLogsUpdateComponent;
    let fixture: ComponentFixture<ApplicationLogsUpdateComponent>;
    let service: ApplicationLogsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [ApplicationLogsUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ApplicationLogsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ApplicationLogsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ApplicationLogsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ApplicationLogs(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ApplicationLogs();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
