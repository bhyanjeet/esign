import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { EsignharyanaTestModule } from '../../../test.module';
import { EsignRoleDeleteDialogComponent } from 'app/entities/esign-role/esign-role-delete-dialog.component';
import { EsignRoleService } from 'app/entities/esign-role/esign-role.service';

describe('Component Tests', () => {
  describe('EsignRole Management Delete Component', () => {
    let comp: EsignRoleDeleteDialogComponent;
    let fixture: ComponentFixture<EsignRoleDeleteDialogComponent>;
    let service: EsignRoleService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [EsignRoleDeleteDialogComponent]
      })
        .overrideTemplate(EsignRoleDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(EsignRoleDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EsignRoleService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
