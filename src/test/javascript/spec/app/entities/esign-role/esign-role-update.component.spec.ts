import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { EsignRoleUpdateComponent } from 'app/entities/esign-role/esign-role-update.component';
import { EsignRoleService } from 'app/entities/esign-role/esign-role.service';
import { EsignRole } from 'app/shared/model/esign-role.model';

describe('Component Tests', () => {
  describe('EsignRole Management Update Component', () => {
    let comp: EsignRoleUpdateComponent;
    let fixture: ComponentFixture<EsignRoleUpdateComponent>;
    let service: EsignRoleService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [EsignRoleUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(EsignRoleUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EsignRoleUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EsignRoleService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new EsignRole(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new EsignRole();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
