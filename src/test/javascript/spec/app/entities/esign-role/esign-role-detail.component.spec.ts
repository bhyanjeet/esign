import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { EsignRoleDetailComponent } from 'app/entities/esign-role/esign-role-detail.component';
import { EsignRole } from 'app/shared/model/esign-role.model';

describe('Component Tests', () => {
  describe('EsignRole Management Detail Component', () => {
    let comp: EsignRoleDetailComponent;
    let fixture: ComponentFixture<EsignRoleDetailComponent>;
    const route = ({ data: of({ esignRole: new EsignRole(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [EsignRoleDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(EsignRoleDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(EsignRoleDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.esignRole).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
