import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { OtpDetailComponent } from 'app/entities/otp/otp-detail.component';
import { Otp } from 'app/shared/model/otp.model';

describe('Component Tests', () => {
  describe('Otp Management Detail Component', () => {
    let comp: OtpDetailComponent;
    let fixture: ComponentFixture<OtpDetailComponent>;
    const route = ({ data: of({ otp: new Otp(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OtpDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(OtpDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OtpDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.otp).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
