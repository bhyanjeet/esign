import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { EsignharyanaTestModule } from '../../../test.module';
import { OtpComponent } from 'app/entities/otp/otp.component';
import { OtpService } from 'app/entities/otp/otp.service';
import { Otp } from 'app/shared/model/otp.model';

describe('Component Tests', () => {
  describe('Otp Management Component', () => {
    let comp: OtpComponent;
    let fixture: ComponentFixture<OtpComponent>;
    let service: OtpService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OtpComponent],
        providers: []
      })
        .overrideTemplate(OtpComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OtpComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OtpService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Otp(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.otps[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
