import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { OtpUpdateComponent } from 'app/entities/otp/otp-update.component';
import { OtpService } from 'app/entities/otp/otp.service';
import { Otp } from 'app/shared/model/otp.model';

describe('Component Tests', () => {
  describe('Otp Management Update Component', () => {
    let comp: OtpUpdateComponent;
    let fixture: ComponentFixture<OtpUpdateComponent>;
    let service: OtpService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [OtpUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(OtpUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OtpUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OtpService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Otp(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Otp();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
