import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { EsignharyanaTestModule } from '../../../test.module';
import { DesignationMasterDeleteDialogComponent } from 'app/entities/designation-master/designation-master-delete-dialog.component';
import { DesignationMasterService } from 'app/entities/designation-master/designation-master.service';

describe('Component Tests', () => {
  describe('DesignationMaster Management Delete Component', () => {
    let comp: DesignationMasterDeleteDialogComponent;
    let fixture: ComponentFixture<DesignationMasterDeleteDialogComponent>;
    let service: DesignationMasterService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [DesignationMasterDeleteDialogComponent]
      })
        .overrideTemplate(DesignationMasterDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DesignationMasterDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DesignationMasterService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
