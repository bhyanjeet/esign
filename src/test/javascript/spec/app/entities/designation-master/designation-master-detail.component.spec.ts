import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { DesignationMasterDetailComponent } from 'app/entities/designation-master/designation-master-detail.component';
import { DesignationMaster } from 'app/shared/model/designation-master.model';

describe('Component Tests', () => {
  describe('DesignationMaster Management Detail Component', () => {
    let comp: DesignationMasterDetailComponent;
    let fixture: ComponentFixture<DesignationMasterDetailComponent>;
    const route = ({ data: of({ designationMaster: new DesignationMaster(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [DesignationMasterDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(DesignationMasterDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DesignationMasterDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.designationMaster).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
