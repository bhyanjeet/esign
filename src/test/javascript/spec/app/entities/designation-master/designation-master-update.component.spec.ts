import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { DesignationMasterUpdateComponent } from 'app/entities/designation-master/designation-master-update.component';
import { DesignationMasterService } from 'app/entities/designation-master/designation-master.service';
import { DesignationMaster } from 'app/shared/model/designation-master.model';

describe('Component Tests', () => {
  describe('DesignationMaster Management Update Component', () => {
    let comp: DesignationMasterUpdateComponent;
    let fixture: ComponentFixture<DesignationMasterUpdateComponent>;
    let service: DesignationMasterService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [DesignationMasterUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(DesignationMasterUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DesignationMasterUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DesignationMasterService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new DesignationMaster(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new DesignationMaster();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
