import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ExternalUserEsignRequestService } from 'app/entities/external-user-esign-request/external-user-esign-request.service';
import { IExternalUserEsignRequest, ExternalUserEsignRequest } from 'app/shared/model/external-user-esign-request.model';

describe('Service Tests', () => {
  describe('ExternalUserEsignRequest Service', () => {
    let injector: TestBed;
    let service: ExternalUserEsignRequestService;
    let httpMock: HttpTestingController;
    let elemDefault: IExternalUserEsignRequest;
    let expectedResult: IExternalUserEsignRequest | IExternalUserEsignRequest[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(ExternalUserEsignRequestService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new ExternalUserEsignRequest(
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ExternalUserEsignRequest', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new ExternalUserEsignRequest()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ExternalUserEsignRequest', () => {
        const returnedFromService = Object.assign(
          {
            userCodeId: 'BBBBBB',
            applicationIdCode: 'BBBBBB',
            responseUrl: 'BBBBBB',
            redirectUrl: 'BBBBBB',
            ts: 'BBBBBB',
            signerId: 'BBBBBB',
            docInfo: 'BBBBBB',
            docUrl: 'BBBBBB',
            docHash: 'BBBBBB',
            requestStatus: 'BBBBBB',
            requestTime: 'BBBBBB',
            txn: 'BBBBBB',
            txnRef: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ExternalUserEsignRequest', () => {
        const returnedFromService = Object.assign(
          {
            userCodeId: 'BBBBBB',
            applicationIdCode: 'BBBBBB',
            responseUrl: 'BBBBBB',
            redirectUrl: 'BBBBBB',
            ts: 'BBBBBB',
            signerId: 'BBBBBB',
            docInfo: 'BBBBBB',
            docUrl: 'BBBBBB',
            docHash: 'BBBBBB',
            requestStatus: 'BBBBBB',
            requestTime: 'BBBBBB',
            txn: 'BBBBBB',
            txnRef: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ExternalUserEsignRequest', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
