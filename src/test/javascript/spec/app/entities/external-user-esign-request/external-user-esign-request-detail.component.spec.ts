import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { ExternalUserEsignRequestDetailComponent } from 'app/entities/external-user-esign-request/external-user-esign-request-detail.component';
import { ExternalUserEsignRequest } from 'app/shared/model/external-user-esign-request.model';

describe('Component Tests', () => {
  describe('ExternalUserEsignRequest Management Detail Component', () => {
    let comp: ExternalUserEsignRequestDetailComponent;
    let fixture: ComponentFixture<ExternalUserEsignRequestDetailComponent>;
    const route = ({ data: of({ externalUserEsignRequest: new ExternalUserEsignRequest(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [ExternalUserEsignRequestDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ExternalUserEsignRequestDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ExternalUserEsignRequestDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load externalUserEsignRequest on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.externalUserEsignRequest).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
