import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EsignharyanaTestModule } from '../../../test.module';
import { ExternalUserEsignRequestUpdateComponent } from 'app/entities/external-user-esign-request/external-user-esign-request-update.component';
import { ExternalUserEsignRequestService } from 'app/entities/external-user-esign-request/external-user-esign-request.service';
import { ExternalUserEsignRequest } from 'app/shared/model/external-user-esign-request.model';

describe('Component Tests', () => {
  describe('ExternalUserEsignRequest Management Update Component', () => {
    let comp: ExternalUserEsignRequestUpdateComponent;
    let fixture: ComponentFixture<ExternalUserEsignRequestUpdateComponent>;
    let service: ExternalUserEsignRequestService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [ExternalUserEsignRequestUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ExternalUserEsignRequestUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ExternalUserEsignRequestUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ExternalUserEsignRequestService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ExternalUserEsignRequest(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ExternalUserEsignRequest();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
