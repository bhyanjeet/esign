import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Data } from '@angular/router';

import { EsignharyanaTestModule } from '../../../test.module';
import { ExternalUserEsignRequestComponent } from 'app/entities/external-user-esign-request/external-user-esign-request.component';
import { ExternalUserEsignRequestService } from 'app/entities/external-user-esign-request/external-user-esign-request.service';
import { ExternalUserEsignRequest } from 'app/shared/model/external-user-esign-request.model';

describe('Component Tests', () => {
  describe('ExternalUserEsignRequest Management Component', () => {
    let comp: ExternalUserEsignRequestComponent;
    let fixture: ComponentFixture<ExternalUserEsignRequestComponent>;
    let service: ExternalUserEsignRequestService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EsignharyanaTestModule],
        declarations: [ExternalUserEsignRequestComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: {
                subscribe: (fn: (value: Data) => void) =>
                  fn({
                    pagingParams: {
                      predicate: 'id',
                      reverse: false,
                      page: 0
                    }
                  })
              }
            }
          }
        ]
      })
        .overrideTemplate(ExternalUserEsignRequestComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ExternalUserEsignRequestComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ExternalUserEsignRequestService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ExternalUserEsignRequest(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.externalUserEsignRequests && comp.externalUserEsignRequests[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ExternalUserEsignRequest(123)],
            headers
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.externalUserEsignRequests && comp.externalUserEsignRequests[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
