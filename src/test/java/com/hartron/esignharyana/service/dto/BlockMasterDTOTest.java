package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class BlockMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BlockMasterDTO.class);
        BlockMasterDTO blockMasterDTO1 = new BlockMasterDTO();
        blockMasterDTO1.setId(1L);
        BlockMasterDTO blockMasterDTO2 = new BlockMasterDTO();
        assertThat(blockMasterDTO1).isNotEqualTo(blockMasterDTO2);
        blockMasterDTO2.setId(blockMasterDTO1.getId());
        assertThat(blockMasterDTO1).isEqualTo(blockMasterDTO2);
        blockMasterDTO2.setId(2L);
        assertThat(blockMasterDTO1).isNotEqualTo(blockMasterDTO2);
        blockMasterDTO1.setId(null);
        assertThat(blockMasterDTO1).isNotEqualTo(blockMasterDTO2);
    }
}
