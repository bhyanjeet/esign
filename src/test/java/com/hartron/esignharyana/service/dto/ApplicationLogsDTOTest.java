package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class ApplicationLogsDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ApplicationLogsDTO.class);
        ApplicationLogsDTO applicationLogsDTO1 = new ApplicationLogsDTO();
        applicationLogsDTO1.setId(1L);
        ApplicationLogsDTO applicationLogsDTO2 = new ApplicationLogsDTO();
        assertThat(applicationLogsDTO1).isNotEqualTo(applicationLogsDTO2);
        applicationLogsDTO2.setId(applicationLogsDTO1.getId());
        assertThat(applicationLogsDTO1).isEqualTo(applicationLogsDTO2);
        applicationLogsDTO2.setId(2L);
        assertThat(applicationLogsDTO1).isNotEqualTo(applicationLogsDTO2);
        applicationLogsDTO1.setId(null);
        assertThat(applicationLogsDTO1).isNotEqualTo(applicationLogsDTO2);
    }
}
