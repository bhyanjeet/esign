package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class EsignErrorDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EsignErrorDTO.class);
        EsignErrorDTO esignErrorDTO1 = new EsignErrorDTO();
        esignErrorDTO1.setId(1L);
        EsignErrorDTO esignErrorDTO2 = new EsignErrorDTO();
        assertThat(esignErrorDTO1).isNotEqualTo(esignErrorDTO2);
        esignErrorDTO2.setId(esignErrorDTO1.getId());
        assertThat(esignErrorDTO1).isEqualTo(esignErrorDTO2);
        esignErrorDTO2.setId(2L);
        assertThat(esignErrorDTO1).isNotEqualTo(esignErrorDTO2);
        esignErrorDTO1.setId(null);
        assertThat(esignErrorDTO1).isNotEqualTo(esignErrorDTO2);
    }
}
