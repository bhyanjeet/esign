package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class DesignationMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DesignationMasterDTO.class);
        DesignationMasterDTO designationMasterDTO1 = new DesignationMasterDTO();
        designationMasterDTO1.setId(1L);
        DesignationMasterDTO designationMasterDTO2 = new DesignationMasterDTO();
        assertThat(designationMasterDTO1).isNotEqualTo(designationMasterDTO2);
        designationMasterDTO2.setId(designationMasterDTO1.getId());
        assertThat(designationMasterDTO1).isEqualTo(designationMasterDTO2);
        designationMasterDTO2.setId(2L);
        assertThat(designationMasterDTO1).isNotEqualTo(designationMasterDTO2);
        designationMasterDTO1.setId(null);
        assertThat(designationMasterDTO1).isNotEqualTo(designationMasterDTO2);
    }
}
