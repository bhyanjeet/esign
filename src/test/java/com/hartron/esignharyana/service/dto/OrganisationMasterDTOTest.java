package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class OrganisationMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrganisationMasterDTO.class);
        OrganisationMasterDTO organisationMasterDTO1 = new OrganisationMasterDTO();
        organisationMasterDTO1.setId(1L);
        OrganisationMasterDTO organisationMasterDTO2 = new OrganisationMasterDTO();
        assertThat(organisationMasterDTO1).isNotEqualTo(organisationMasterDTO2);
        organisationMasterDTO2.setId(organisationMasterDTO1.getId());
        assertThat(organisationMasterDTO1).isEqualTo(organisationMasterDTO2);
        organisationMasterDTO2.setId(2L);
        assertThat(organisationMasterDTO1).isNotEqualTo(organisationMasterDTO2);
        organisationMasterDTO1.setId(null);
        assertThat(organisationMasterDTO1).isNotEqualTo(organisationMasterDTO2);
    }
}
