package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class DistrictMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DistrictMasterDTO.class);
        DistrictMasterDTO districtMasterDTO1 = new DistrictMasterDTO();
        districtMasterDTO1.setId(1L);
        DistrictMasterDTO districtMasterDTO2 = new DistrictMasterDTO();
        assertThat(districtMasterDTO1).isNotEqualTo(districtMasterDTO2);
        districtMasterDTO2.setId(districtMasterDTO1.getId());
        assertThat(districtMasterDTO1).isEqualTo(districtMasterDTO2);
        districtMasterDTO2.setId(2L);
        assertThat(districtMasterDTO1).isNotEqualTo(districtMasterDTO2);
        districtMasterDTO1.setId(null);
        assertThat(districtMasterDTO1).isNotEqualTo(districtMasterDTO2);
    }
}
