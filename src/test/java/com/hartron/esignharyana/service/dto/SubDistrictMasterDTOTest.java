package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class SubDistrictMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubDistrictMasterDTO.class);
        SubDistrictMasterDTO subDistrictMasterDTO1 = new SubDistrictMasterDTO();
        subDistrictMasterDTO1.setId(1L);
        SubDistrictMasterDTO subDistrictMasterDTO2 = new SubDistrictMasterDTO();
        assertThat(subDistrictMasterDTO1).isNotEqualTo(subDistrictMasterDTO2);
        subDistrictMasterDTO2.setId(subDistrictMasterDTO1.getId());
        assertThat(subDistrictMasterDTO1).isEqualTo(subDistrictMasterDTO2);
        subDistrictMasterDTO2.setId(2L);
        assertThat(subDistrictMasterDTO1).isNotEqualTo(subDistrictMasterDTO2);
        subDistrictMasterDTO1.setId(null);
        assertThat(subDistrictMasterDTO1).isNotEqualTo(subDistrictMasterDTO2);
    }
}
