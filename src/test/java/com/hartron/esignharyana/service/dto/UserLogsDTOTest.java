package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class UserLogsDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserLogsDTO.class);
        UserLogsDTO userLogsDTO1 = new UserLogsDTO();
        userLogsDTO1.setId(1L);
        UserLogsDTO userLogsDTO2 = new UserLogsDTO();
        assertThat(userLogsDTO1).isNotEqualTo(userLogsDTO2);
        userLogsDTO2.setId(userLogsDTO1.getId());
        assertThat(userLogsDTO1).isEqualTo(userLogsDTO2);
        userLogsDTO2.setId(2L);
        assertThat(userLogsDTO1).isNotEqualTo(userLogsDTO2);
        userLogsDTO1.setId(null);
        assertThat(userLogsDTO1).isNotEqualTo(userLogsDTO2);
    }
}
