package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class ApplicationMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ApplicationMasterDTO.class);
        ApplicationMasterDTO applicationMasterDTO1 = new ApplicationMasterDTO();
        applicationMasterDTO1.setId(1L);
        ApplicationMasterDTO applicationMasterDTO2 = new ApplicationMasterDTO();
        assertThat(applicationMasterDTO1).isNotEqualTo(applicationMasterDTO2);
        applicationMasterDTO2.setId(applicationMasterDTO1.getId());
        assertThat(applicationMasterDTO1).isEqualTo(applicationMasterDTO2);
        applicationMasterDTO2.setId(2L);
        assertThat(applicationMasterDTO1).isNotEqualTo(applicationMasterDTO2);
        applicationMasterDTO1.setId(null);
        assertThat(applicationMasterDTO1).isNotEqualTo(applicationMasterDTO2);
    }
}
