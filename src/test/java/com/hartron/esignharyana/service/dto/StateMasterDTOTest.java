package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class StateMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(StateMasterDTO.class);
        StateMasterDTO stateMasterDTO1 = new StateMasterDTO();
        stateMasterDTO1.setId(1L);
        StateMasterDTO stateMasterDTO2 = new StateMasterDTO();
        assertThat(stateMasterDTO1).isNotEqualTo(stateMasterDTO2);
        stateMasterDTO2.setId(stateMasterDTO1.getId());
        assertThat(stateMasterDTO1).isEqualTo(stateMasterDTO2);
        stateMasterDTO2.setId(2L);
        assertThat(stateMasterDTO1).isNotEqualTo(stateMasterDTO2);
        stateMasterDTO1.setId(null);
        assertThat(stateMasterDTO1).isNotEqualTo(stateMasterDTO2);
    }
}
