package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class OrganisationTypeMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrganisationTypeMasterDTO.class);
        OrganisationTypeMasterDTO organisationTypeMasterDTO1 = new OrganisationTypeMasterDTO();
        organisationTypeMasterDTO1.setId(1L);
        OrganisationTypeMasterDTO organisationTypeMasterDTO2 = new OrganisationTypeMasterDTO();
        assertThat(organisationTypeMasterDTO1).isNotEqualTo(organisationTypeMasterDTO2);
        organisationTypeMasterDTO2.setId(organisationTypeMasterDTO1.getId());
        assertThat(organisationTypeMasterDTO1).isEqualTo(organisationTypeMasterDTO2);
        organisationTypeMasterDTO2.setId(2L);
        assertThat(organisationTypeMasterDTO1).isNotEqualTo(organisationTypeMasterDTO2);
        organisationTypeMasterDTO1.setId(null);
        assertThat(organisationTypeMasterDTO1).isNotEqualTo(organisationTypeMasterDTO2);
    }
}
