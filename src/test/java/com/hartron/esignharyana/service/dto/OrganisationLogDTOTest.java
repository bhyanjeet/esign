package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class OrganisationLogDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrganisationLogDTO.class);
        OrganisationLogDTO organisationLogDTO1 = new OrganisationLogDTO();
        organisationLogDTO1.setId(1L);
        OrganisationLogDTO organisationLogDTO2 = new OrganisationLogDTO();
        assertThat(organisationLogDTO1).isNotEqualTo(organisationLogDTO2);
        organisationLogDTO2.setId(organisationLogDTO1.getId());
        assertThat(organisationLogDTO1).isEqualTo(organisationLogDTO2);
        organisationLogDTO2.setId(2L);
        assertThat(organisationLogDTO1).isNotEqualTo(organisationLogDTO2);
        organisationLogDTO1.setId(null);
        assertThat(organisationLogDTO1).isNotEqualTo(organisationLogDTO2);
    }
}
