package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class OrganisationDocumentDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrganisationDocumentDTO.class);
        OrganisationDocumentDTO organisationDocumentDTO1 = new OrganisationDocumentDTO();
        organisationDocumentDTO1.setId(1L);
        OrganisationDocumentDTO organisationDocumentDTO2 = new OrganisationDocumentDTO();
        assertThat(organisationDocumentDTO1).isNotEqualTo(organisationDocumentDTO2);
        organisationDocumentDTO2.setId(organisationDocumentDTO1.getId());
        assertThat(organisationDocumentDTO1).isEqualTo(organisationDocumentDTO2);
        organisationDocumentDTO2.setId(2L);
        assertThat(organisationDocumentDTO1).isNotEqualTo(organisationDocumentDTO2);
        organisationDocumentDTO1.setId(null);
        assertThat(organisationDocumentDTO1).isNotEqualTo(organisationDocumentDTO2);
    }
}
