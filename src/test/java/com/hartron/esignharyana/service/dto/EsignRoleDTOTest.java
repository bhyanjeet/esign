package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class EsignRoleDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EsignRoleDTO.class);
        EsignRoleDTO esignRoleDTO1 = new EsignRoleDTO();
        esignRoleDTO1.setId(1L);
        EsignRoleDTO esignRoleDTO2 = new EsignRoleDTO();
        assertThat(esignRoleDTO1).isNotEqualTo(esignRoleDTO2);
        esignRoleDTO2.setId(esignRoleDTO1.getId());
        assertThat(esignRoleDTO1).isEqualTo(esignRoleDTO2);
        esignRoleDTO2.setId(2L);
        assertThat(esignRoleDTO1).isNotEqualTo(esignRoleDTO2);
        esignRoleDTO1.setId(null);
        assertThat(esignRoleDTO1).isNotEqualTo(esignRoleDTO2);
    }
}
