package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class ExternalUserEsignRequestDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExternalUserEsignRequestDTO.class);
        ExternalUserEsignRequestDTO externalUserEsignRequestDTO1 = new ExternalUserEsignRequestDTO();
        externalUserEsignRequestDTO1.setId(1L);
        ExternalUserEsignRequestDTO externalUserEsignRequestDTO2 = new ExternalUserEsignRequestDTO();
        assertThat(externalUserEsignRequestDTO1).isNotEqualTo(externalUserEsignRequestDTO2);
        externalUserEsignRequestDTO2.setId(externalUserEsignRequestDTO1.getId());
        assertThat(externalUserEsignRequestDTO1).isEqualTo(externalUserEsignRequestDTO2);
        externalUserEsignRequestDTO2.setId(2L);
        assertThat(externalUserEsignRequestDTO1).isNotEqualTo(externalUserEsignRequestDTO2);
        externalUserEsignRequestDTO1.setId(null);
        assertThat(externalUserEsignRequestDTO1).isNotEqualTo(externalUserEsignRequestDTO2);
    }
}
