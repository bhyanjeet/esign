package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class OrganisationAttachmentDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrganisationAttachmentDTO.class);
        OrganisationAttachmentDTO organisationAttachmentDTO1 = new OrganisationAttachmentDTO();
        organisationAttachmentDTO1.setId(1L);
        OrganisationAttachmentDTO organisationAttachmentDTO2 = new OrganisationAttachmentDTO();
        assertThat(organisationAttachmentDTO1).isNotEqualTo(organisationAttachmentDTO2);
        organisationAttachmentDTO2.setId(organisationAttachmentDTO1.getId());
        assertThat(organisationAttachmentDTO1).isEqualTo(organisationAttachmentDTO2);
        organisationAttachmentDTO2.setId(2L);
        assertThat(organisationAttachmentDTO1).isNotEqualTo(organisationAttachmentDTO2);
        organisationAttachmentDTO1.setId(null);
        assertThat(organisationAttachmentDTO1).isNotEqualTo(organisationAttachmentDTO2);
    }
}
