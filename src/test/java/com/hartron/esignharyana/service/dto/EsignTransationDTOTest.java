package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class EsignTransationDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EsignTransationDTO.class);
        EsignTransationDTO esignTransationDTO1 = new EsignTransationDTO();
        esignTransationDTO1.setId(1L);
        EsignTransationDTO esignTransationDTO2 = new EsignTransationDTO();
        assertThat(esignTransationDTO1).isNotEqualTo(esignTransationDTO2);
        esignTransationDTO2.setId(esignTransationDTO1.getId());
        assertThat(esignTransationDTO1).isEqualTo(esignTransationDTO2);
        esignTransationDTO2.setId(2L);
        assertThat(esignTransationDTO1).isNotEqualTo(esignTransationDTO2);
        esignTransationDTO1.setId(null);
        assertThat(esignTransationDTO1).isNotEqualTo(esignTransationDTO2);
    }
}
