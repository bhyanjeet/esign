package com.hartron.esignharyana.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class UserAuthDetailsDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserAuthDetailsDTO.class);
        UserAuthDetailsDTO userAuthDetailsDTO1 = new UserAuthDetailsDTO();
        userAuthDetailsDTO1.setId(1L);
        UserAuthDetailsDTO userAuthDetailsDTO2 = new UserAuthDetailsDTO();
        assertThat(userAuthDetailsDTO1).isNotEqualTo(userAuthDetailsDTO2);
        userAuthDetailsDTO2.setId(userAuthDetailsDTO1.getId());
        assertThat(userAuthDetailsDTO1).isEqualTo(userAuthDetailsDTO2);
        userAuthDetailsDTO2.setId(2L);
        assertThat(userAuthDetailsDTO1).isNotEqualTo(userAuthDetailsDTO2);
        userAuthDetailsDTO1.setId(null);
        assertThat(userAuthDetailsDTO1).isNotEqualTo(userAuthDetailsDTO2);
    }
}
