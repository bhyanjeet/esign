package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class OrganisationLogMapperTest {

    private OrganisationLogMapper organisationLogMapper;

    @BeforeEach
    public void setUp() {
        organisationLogMapper = new OrganisationLogMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(organisationLogMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(organisationLogMapper.fromId(null)).isNull();
    }
}
