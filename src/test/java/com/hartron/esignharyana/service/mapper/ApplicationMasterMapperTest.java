package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ApplicationMasterMapperTest {

    private ApplicationMasterMapper applicationMasterMapper;

    @BeforeEach
    public void setUp() {
        applicationMasterMapper = new ApplicationMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(applicationMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(applicationMasterMapper.fromId(null)).isNull();
    }
}
