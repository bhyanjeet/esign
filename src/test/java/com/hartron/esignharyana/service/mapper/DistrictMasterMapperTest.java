package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class DistrictMasterMapperTest {

    private DistrictMasterMapper districtMasterMapper;

    @BeforeEach
    public void setUp() {
        districtMasterMapper = new DistrictMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(districtMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(districtMasterMapper.fromId(null)).isNull();
    }
}
