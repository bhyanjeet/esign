package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ExternalUserEsignRequestMapperTest {

    private ExternalUserEsignRequestMapper externalUserEsignRequestMapper;

    @BeforeEach
    public void setUp() {
        externalUserEsignRequestMapper = new ExternalUserEsignRequestMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(externalUserEsignRequestMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(externalUserEsignRequestMapper.fromId(null)).isNull();
    }
}
