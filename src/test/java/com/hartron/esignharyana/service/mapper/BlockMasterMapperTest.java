package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class BlockMasterMapperTest {

    private BlockMasterMapper blockMasterMapper;

    @BeforeEach
    public void setUp() {
        blockMasterMapper = new BlockMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(blockMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(blockMasterMapper.fromId(null)).isNull();
    }
}
