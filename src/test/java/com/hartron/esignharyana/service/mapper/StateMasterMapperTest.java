package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class StateMasterMapperTest {

    private StateMasterMapper stateMasterMapper;

    @BeforeEach
    public void setUp() {
        stateMasterMapper = new StateMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(stateMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(stateMasterMapper.fromId(null)).isNull();
    }
}
