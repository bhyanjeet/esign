package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class SubDistrictMasterMapperTest {

    private SubDistrictMasterMapper subDistrictMasterMapper;

    @BeforeEach
    public void setUp() {
        subDistrictMasterMapper = new SubDistrictMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(subDistrictMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(subDistrictMasterMapper.fromId(null)).isNull();
    }
}
