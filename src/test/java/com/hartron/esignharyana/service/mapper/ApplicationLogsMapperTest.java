package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class ApplicationLogsMapperTest {

    private ApplicationLogsMapper applicationLogsMapper;

    @BeforeEach
    public void setUp() {
        applicationLogsMapper = new ApplicationLogsMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(applicationLogsMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(applicationLogsMapper.fromId(null)).isNull();
    }
}
