package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class DesignationMasterMapperTest {

    private DesignationMasterMapper designationMasterMapper;

    @BeforeEach
    public void setUp() {
        designationMasterMapper = new DesignationMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(designationMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(designationMasterMapper.fromId(null)).isNull();
    }
}
