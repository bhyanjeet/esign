package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class OrganisationTypeMasterMapperTest {

    private OrganisationTypeMasterMapper organisationTypeMasterMapper;

    @BeforeEach
    public void setUp() {
        organisationTypeMasterMapper = new OrganisationTypeMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(organisationTypeMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(organisationTypeMasterMapper.fromId(null)).isNull();
    }
}
