package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class OrganisationMasterMapperTest {

    private OrganisationMasterMapper organisationMasterMapper;

    @BeforeEach
    public void setUp() {
        organisationMasterMapper = new OrganisationMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(organisationMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(organisationMasterMapper.fromId(null)).isNull();
    }
}
