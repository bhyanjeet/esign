package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class OrganisationAttachmentMapperTest {

    private OrganisationAttachmentMapper organisationAttachmentMapper;

    @BeforeEach
    public void setUp() {
        organisationAttachmentMapper = new OrganisationAttachmentMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(organisationAttachmentMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(organisationAttachmentMapper.fromId(null)).isNull();
    }
}
