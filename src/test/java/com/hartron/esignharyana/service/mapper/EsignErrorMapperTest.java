package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class EsignErrorMapperTest {

    private EsignErrorMapper esignErrorMapper;

    @BeforeEach
    public void setUp() {
        esignErrorMapper = new EsignErrorMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(esignErrorMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(esignErrorMapper.fromId(null)).isNull();
    }
}
