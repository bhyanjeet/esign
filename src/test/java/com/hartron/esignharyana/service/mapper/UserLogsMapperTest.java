package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class UserLogsMapperTest {

    private UserLogsMapper userLogsMapper;

    @BeforeEach
    public void setUp() {
        userLogsMapper = new UserLogsMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(userLogsMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(userLogsMapper.fromId(null)).isNull();
    }
}
