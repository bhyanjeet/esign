package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class EsignTransationMapperTest {

    private EsignTransationMapper esignTransationMapper;

    @BeforeEach
    public void setUp() {
        esignTransationMapper = new EsignTransationMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(esignTransationMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(esignTransationMapper.fromId(null)).isNull();
    }
}
