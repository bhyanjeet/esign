package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class UserAuthDetailsMapperTest {

    private UserAuthDetailsMapper userAuthDetailsMapper;

    @BeforeEach
    public void setUp() {
        userAuthDetailsMapper = new UserAuthDetailsMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(userAuthDetailsMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(userAuthDetailsMapper.fromId(null)).isNull();
    }
}
