package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class OrganisationDocumentMapperTest {

    private OrganisationDocumentMapper organisationDocumentMapper;

    @BeforeEach
    public void setUp() {
        organisationDocumentMapper = new OrganisationDocumentMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(organisationDocumentMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(organisationDocumentMapper.fromId(null)).isNull();
    }
}
