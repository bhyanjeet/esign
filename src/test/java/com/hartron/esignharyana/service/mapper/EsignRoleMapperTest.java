package com.hartron.esignharyana.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;


public class EsignRoleMapperTest {

    private EsignRoleMapper esignRoleMapper;

    @BeforeEach
    public void setUp() {
        esignRoleMapper = new EsignRoleMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(esignRoleMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(esignRoleMapper.fromId(null)).isNull();
    }
}
