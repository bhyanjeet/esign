package com.hartron.esignharyana.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class BlockMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BlockMaster.class);
        BlockMaster blockMaster1 = new BlockMaster();
        blockMaster1.setId(1L);
        BlockMaster blockMaster2 = new BlockMaster();
        blockMaster2.setId(blockMaster1.getId());
        assertThat(blockMaster1).isEqualTo(blockMaster2);
        blockMaster2.setId(2L);
        assertThat(blockMaster1).isNotEqualTo(blockMaster2);
        blockMaster1.setId(null);
        assertThat(blockMaster1).isNotEqualTo(blockMaster2);
    }
}
