package com.hartron.esignharyana.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class OrganisationAttachmentTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrganisationAttachment.class);
        OrganisationAttachment organisationAttachment1 = new OrganisationAttachment();
        organisationAttachment1.setId(1L);
        OrganisationAttachment organisationAttachment2 = new OrganisationAttachment();
        organisationAttachment2.setId(organisationAttachment1.getId());
        assertThat(organisationAttachment1).isEqualTo(organisationAttachment2);
        organisationAttachment2.setId(2L);
        assertThat(organisationAttachment1).isNotEqualTo(organisationAttachment2);
        organisationAttachment1.setId(null);
        assertThat(organisationAttachment1).isNotEqualTo(organisationAttachment2);
    }
}
