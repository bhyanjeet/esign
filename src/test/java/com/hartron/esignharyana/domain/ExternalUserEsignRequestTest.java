package com.hartron.esignharyana.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class ExternalUserEsignRequestTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExternalUserEsignRequest.class);
        ExternalUserEsignRequest externalUserEsignRequest1 = new ExternalUserEsignRequest();
        externalUserEsignRequest1.setId(1L);
        ExternalUserEsignRequest externalUserEsignRequest2 = new ExternalUserEsignRequest();
        externalUserEsignRequest2.setId(externalUserEsignRequest1.getId());
        assertThat(externalUserEsignRequest1).isEqualTo(externalUserEsignRequest2);
        externalUserEsignRequest2.setId(2L);
        assertThat(externalUserEsignRequest1).isNotEqualTo(externalUserEsignRequest2);
        externalUserEsignRequest1.setId(null);
        assertThat(externalUserEsignRequest1).isNotEqualTo(externalUserEsignRequest2);
    }
}
