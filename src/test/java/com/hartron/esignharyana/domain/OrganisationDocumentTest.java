package com.hartron.esignharyana.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class OrganisationDocumentTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrganisationDocument.class);
        OrganisationDocument organisationDocument1 = new OrganisationDocument();
        organisationDocument1.setId(1L);
        OrganisationDocument organisationDocument2 = new OrganisationDocument();
        organisationDocument2.setId(organisationDocument1.getId());
        assertThat(organisationDocument1).isEqualTo(organisationDocument2);
        organisationDocument2.setId(2L);
        assertThat(organisationDocument1).isNotEqualTo(organisationDocument2);
        organisationDocument1.setId(null);
        assertThat(organisationDocument1).isNotEqualTo(organisationDocument2);
    }
}
