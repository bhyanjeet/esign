package com.hartron.esignharyana.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class EsignTransationTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EsignTransation.class);
        EsignTransation esignTransation1 = new EsignTransation();
        esignTransation1.setId(1L);
        EsignTransation esignTransation2 = new EsignTransation();
        esignTransation2.setId(esignTransation1.getId());
        assertThat(esignTransation1).isEqualTo(esignTransation2);
        esignTransation2.setId(2L);
        assertThat(esignTransation1).isNotEqualTo(esignTransation2);
        esignTransation1.setId(null);
        assertThat(esignTransation1).isNotEqualTo(esignTransation2);
    }
}
