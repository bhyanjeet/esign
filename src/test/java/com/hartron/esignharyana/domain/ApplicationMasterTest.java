package com.hartron.esignharyana.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class ApplicationMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ApplicationMaster.class);
        ApplicationMaster applicationMaster1 = new ApplicationMaster();
        applicationMaster1.setId(1L);
        ApplicationMaster applicationMaster2 = new ApplicationMaster();
        applicationMaster2.setId(applicationMaster1.getId());
        assertThat(applicationMaster1).isEqualTo(applicationMaster2);
        applicationMaster2.setId(2L);
        assertThat(applicationMaster1).isNotEqualTo(applicationMaster2);
        applicationMaster1.setId(null);
        assertThat(applicationMaster1).isNotEqualTo(applicationMaster2);
    }
}
