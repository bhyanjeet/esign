package com.hartron.esignharyana.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class EsignErrorTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EsignError.class);
        EsignError esignError1 = new EsignError();
        esignError1.setId(1L);
        EsignError esignError2 = new EsignError();
        esignError2.setId(esignError1.getId());
        assertThat(esignError1).isEqualTo(esignError2);
        esignError2.setId(2L);
        assertThat(esignError1).isNotEqualTo(esignError2);
        esignError1.setId(null);
        assertThat(esignError1).isNotEqualTo(esignError2);
    }
}
