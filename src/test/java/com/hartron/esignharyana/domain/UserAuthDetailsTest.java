package com.hartron.esignharyana.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class UserAuthDetailsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserAuthDetails.class);
        UserAuthDetails userAuthDetails1 = new UserAuthDetails();
        userAuthDetails1.setId(1L);
        UserAuthDetails userAuthDetails2 = new UserAuthDetails();
        userAuthDetails2.setId(userAuthDetails1.getId());
        assertThat(userAuthDetails1).isEqualTo(userAuthDetails2);
        userAuthDetails2.setId(2L);
        assertThat(userAuthDetails1).isNotEqualTo(userAuthDetails2);
        userAuthDetails1.setId(null);
        assertThat(userAuthDetails1).isNotEqualTo(userAuthDetails2);
    }
}
