package com.hartron.esignharyana.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class OrganisationMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrganisationMaster.class);
        OrganisationMaster organisationMaster1 = new OrganisationMaster();
        organisationMaster1.setId(1L);
        OrganisationMaster organisationMaster2 = new OrganisationMaster();
        organisationMaster2.setId(organisationMaster1.getId());
        assertThat(organisationMaster1).isEqualTo(organisationMaster2);
        organisationMaster2.setId(2L);
        assertThat(organisationMaster1).isNotEqualTo(organisationMaster2);
        organisationMaster1.setId(null);
        assertThat(organisationMaster1).isNotEqualTo(organisationMaster2);
    }
}
