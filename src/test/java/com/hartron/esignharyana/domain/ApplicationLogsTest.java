package com.hartron.esignharyana.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class ApplicationLogsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ApplicationLogs.class);
        ApplicationLogs applicationLogs1 = new ApplicationLogs();
        applicationLogs1.setId(1L);
        ApplicationLogs applicationLogs2 = new ApplicationLogs();
        applicationLogs2.setId(applicationLogs1.getId());
        assertThat(applicationLogs1).isEqualTo(applicationLogs2);
        applicationLogs2.setId(2L);
        assertThat(applicationLogs1).isNotEqualTo(applicationLogs2);
        applicationLogs1.setId(null);
        assertThat(applicationLogs1).isNotEqualTo(applicationLogs2);
    }
}
