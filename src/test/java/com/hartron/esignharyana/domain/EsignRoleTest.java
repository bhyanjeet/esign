package com.hartron.esignharyana.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class EsignRoleTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EsignRole.class);
        EsignRole esignRole1 = new EsignRole();
        esignRole1.setId(1L);
        EsignRole esignRole2 = new EsignRole();
        esignRole2.setId(esignRole1.getId());
        assertThat(esignRole1).isEqualTo(esignRole2);
        esignRole2.setId(2L);
        assertThat(esignRole1).isNotEqualTo(esignRole2);
        esignRole1.setId(null);
        assertThat(esignRole1).isNotEqualTo(esignRole2);
    }
}
