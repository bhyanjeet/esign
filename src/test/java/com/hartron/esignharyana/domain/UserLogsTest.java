package com.hartron.esignharyana.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class UserLogsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserLogs.class);
        UserLogs userLogs1 = new UserLogs();
        userLogs1.setId(1L);
        UserLogs userLogs2 = new UserLogs();
        userLogs2.setId(userLogs1.getId());
        assertThat(userLogs1).isEqualTo(userLogs2);
        userLogs2.setId(2L);
        assertThat(userLogs1).isNotEqualTo(userLogs2);
        userLogs1.setId(null);
        assertThat(userLogs1).isNotEqualTo(userLogs2);
    }
}
