package com.hartron.esignharyana.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class OrganisationTypeMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrganisationTypeMaster.class);
        OrganisationTypeMaster organisationTypeMaster1 = new OrganisationTypeMaster();
        organisationTypeMaster1.setId(1L);
        OrganisationTypeMaster organisationTypeMaster2 = new OrganisationTypeMaster();
        organisationTypeMaster2.setId(organisationTypeMaster1.getId());
        assertThat(organisationTypeMaster1).isEqualTo(organisationTypeMaster2);
        organisationTypeMaster2.setId(2L);
        assertThat(organisationTypeMaster1).isNotEqualTo(organisationTypeMaster2);
        organisationTypeMaster1.setId(null);
        assertThat(organisationTypeMaster1).isNotEqualTo(organisationTypeMaster2);
    }
}
