package com.hartron.esignharyana.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class OrganisationLogTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrganisationLog.class);
        OrganisationLog organisationLog1 = new OrganisationLog();
        organisationLog1.setId(1L);
        OrganisationLog organisationLog2 = new OrganisationLog();
        organisationLog2.setId(organisationLog1.getId());
        assertThat(organisationLog1).isEqualTo(organisationLog2);
        organisationLog2.setId(2L);
        assertThat(organisationLog1).isNotEqualTo(organisationLog2);
        organisationLog1.setId(null);
        assertThat(organisationLog1).isNotEqualTo(organisationLog2);
    }
}
