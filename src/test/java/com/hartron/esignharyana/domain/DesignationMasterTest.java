package com.hartron.esignharyana.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class DesignationMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DesignationMaster.class);
        DesignationMaster designationMaster1 = new DesignationMaster();
        designationMaster1.setId(1L);
        DesignationMaster designationMaster2 = new DesignationMaster();
        designationMaster2.setId(designationMaster1.getId());
        assertThat(designationMaster1).isEqualTo(designationMaster2);
        designationMaster2.setId(2L);
        assertThat(designationMaster1).isNotEqualTo(designationMaster2);
        designationMaster1.setId(null);
        assertThat(designationMaster1).isNotEqualTo(designationMaster2);
    }
}
