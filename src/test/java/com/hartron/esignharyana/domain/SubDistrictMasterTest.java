package com.hartron.esignharyana.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hartron.esignharyana.web.rest.TestUtil;

public class SubDistrictMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubDistrictMaster.class);
        SubDistrictMaster subDistrictMaster1 = new SubDistrictMaster();
        subDistrictMaster1.setId(1L);
        SubDistrictMaster subDistrictMaster2 = new SubDistrictMaster();
        subDistrictMaster2.setId(subDistrictMaster1.getId());
        assertThat(subDistrictMaster1).isEqualTo(subDistrictMaster2);
        subDistrictMaster2.setId(2L);
        assertThat(subDistrictMaster1).isNotEqualTo(subDistrictMaster2);
        subDistrictMaster1.setId(null);
        assertThat(subDistrictMaster1).isNotEqualTo(subDistrictMaster2);
    }
}
