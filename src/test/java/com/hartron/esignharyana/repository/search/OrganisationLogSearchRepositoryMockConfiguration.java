package com.hartron.esignharyana.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link OrganisationLogSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class OrganisationLogSearchRepositoryMockConfiguration {

    @MockBean
    private OrganisationLogSearchRepository mockOrganisationLogSearchRepository;

}
