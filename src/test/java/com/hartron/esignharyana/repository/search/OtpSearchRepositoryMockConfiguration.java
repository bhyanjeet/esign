package com.hartron.esignharyana.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link OtpSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class OtpSearchRepositoryMockConfiguration {

    @MockBean
    private OtpSearchRepository mockOtpSearchRepository;

}
