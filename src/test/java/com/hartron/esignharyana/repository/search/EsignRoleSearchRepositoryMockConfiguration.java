package com.hartron.esignharyana.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link EsignRoleSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class EsignRoleSearchRepositoryMockConfiguration {

    @MockBean
    private EsignRoleSearchRepository mockEsignRoleSearchRepository;

}
