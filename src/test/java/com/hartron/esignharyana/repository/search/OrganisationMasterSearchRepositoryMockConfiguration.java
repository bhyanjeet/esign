package com.hartron.esignharyana.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link OrganisationMasterSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class OrganisationMasterSearchRepositoryMockConfiguration {

    @MockBean
    private OrganisationMasterSearchRepository mockOrganisationMasterSearchRepository;

}
