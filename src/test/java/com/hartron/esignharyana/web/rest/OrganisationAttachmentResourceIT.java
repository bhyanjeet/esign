package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.OrganisationAttachment;
import com.hartron.esignharyana.repository.OrganisationAttachmentRepository;
import com.hartron.esignharyana.repository.search.OrganisationAttachmentSearchRepository;
import com.hartron.esignharyana.service.OrganisationAttachmentService;
import com.hartron.esignharyana.service.dto.OrganisationAttachmentDTO;
import com.hartron.esignharyana.service.mapper.OrganisationAttachmentMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OrganisationAttachmentResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class OrganisationAttachmentResourceIT {

    private static final String DEFAULT_ATTACHMENT = "AAAAAAAAAA";
    private static final String UPDATED_ATTACHMENT = "BBBBBBBBBB";

    @Autowired
    private OrganisationAttachmentRepository organisationAttachmentRepository;

    @Autowired
    private OrganisationAttachmentMapper organisationAttachmentMapper;

    @Autowired
    private OrganisationAttachmentService organisationAttachmentService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.OrganisationAttachmentSearchRepositoryMockConfiguration
     */
    @Autowired
    private OrganisationAttachmentSearchRepository mockOrganisationAttachmentSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOrganisationAttachmentMockMvc;

    private OrganisationAttachment organisationAttachment;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OrganisationAttachmentResource organisationAttachmentResource = new OrganisationAttachmentResource(organisationAttachmentService);
        this.restOrganisationAttachmentMockMvc = MockMvcBuilders.standaloneSetup(organisationAttachmentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrganisationAttachment createEntity(EntityManager em) {
        OrganisationAttachment organisationAttachment = new OrganisationAttachment()
            .attachment(DEFAULT_ATTACHMENT);
        return organisationAttachment;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrganisationAttachment createUpdatedEntity(EntityManager em) {
        OrganisationAttachment organisationAttachment = new OrganisationAttachment()
            .attachment(UPDATED_ATTACHMENT);
        return organisationAttachment;
    }

    @BeforeEach
    public void initTest() {
        organisationAttachment = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrganisationAttachment() throws Exception {
        int databaseSizeBeforeCreate = organisationAttachmentRepository.findAll().size();

        // Create the OrganisationAttachment
        OrganisationAttachmentDTO organisationAttachmentDTO = organisationAttachmentMapper.toDto(organisationAttachment);
        restOrganisationAttachmentMockMvc.perform(post("/api/organisation-attachments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationAttachmentDTO)))
            .andExpect(status().isCreated());

        // Validate the OrganisationAttachment in the database
        List<OrganisationAttachment> organisationAttachmentList = organisationAttachmentRepository.findAll();
        assertThat(organisationAttachmentList).hasSize(databaseSizeBeforeCreate + 1);
        OrganisationAttachment testOrganisationAttachment = organisationAttachmentList.get(organisationAttachmentList.size() - 1);
        assertThat(testOrganisationAttachment.getAttachment()).isEqualTo(DEFAULT_ATTACHMENT);

        // Validate the OrganisationAttachment in Elasticsearch
        verify(mockOrganisationAttachmentSearchRepository, times(1)).save(testOrganisationAttachment);
    }

    @Test
    @Transactional
    public void createOrganisationAttachmentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = organisationAttachmentRepository.findAll().size();

        // Create the OrganisationAttachment with an existing ID
        organisationAttachment.setId(1L);
        OrganisationAttachmentDTO organisationAttachmentDTO = organisationAttachmentMapper.toDto(organisationAttachment);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrganisationAttachmentMockMvc.perform(post("/api/organisation-attachments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationAttachmentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrganisationAttachment in the database
        List<OrganisationAttachment> organisationAttachmentList = organisationAttachmentRepository.findAll();
        assertThat(organisationAttachmentList).hasSize(databaseSizeBeforeCreate);

        // Validate the OrganisationAttachment in Elasticsearch
        verify(mockOrganisationAttachmentSearchRepository, times(0)).save(organisationAttachment);
    }


    @Test
    @Transactional
    public void getAllOrganisationAttachments() throws Exception {
        // Initialize the database
        organisationAttachmentRepository.saveAndFlush(organisationAttachment);

        // Get all the organisationAttachmentList
        restOrganisationAttachmentMockMvc.perform(get("/api/organisation-attachments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(organisationAttachment.getId().intValue())))
            .andExpect(jsonPath("$.[*].attachment").value(hasItem(DEFAULT_ATTACHMENT)));
    }
    
    @Test
    @Transactional
    public void getOrganisationAttachment() throws Exception {
        // Initialize the database
        organisationAttachmentRepository.saveAndFlush(organisationAttachment);

        // Get the organisationAttachment
        restOrganisationAttachmentMockMvc.perform(get("/api/organisation-attachments/{id}", organisationAttachment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(organisationAttachment.getId().intValue()))
            .andExpect(jsonPath("$.attachment").value(DEFAULT_ATTACHMENT));
    }

    @Test
    @Transactional
    public void getNonExistingOrganisationAttachment() throws Exception {
        // Get the organisationAttachment
        restOrganisationAttachmentMockMvc.perform(get("/api/organisation-attachments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrganisationAttachment() throws Exception {
        // Initialize the database
        organisationAttachmentRepository.saveAndFlush(organisationAttachment);

        int databaseSizeBeforeUpdate = organisationAttachmentRepository.findAll().size();

        // Update the organisationAttachment
        OrganisationAttachment updatedOrganisationAttachment = organisationAttachmentRepository.findById(organisationAttachment.getId()).get();
        // Disconnect from session so that the updates on updatedOrganisationAttachment are not directly saved in db
        em.detach(updatedOrganisationAttachment);
        updatedOrganisationAttachment
            .attachment(UPDATED_ATTACHMENT);
        OrganisationAttachmentDTO organisationAttachmentDTO = organisationAttachmentMapper.toDto(updatedOrganisationAttachment);

        restOrganisationAttachmentMockMvc.perform(put("/api/organisation-attachments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationAttachmentDTO)))
            .andExpect(status().isOk());

        // Validate the OrganisationAttachment in the database
        List<OrganisationAttachment> organisationAttachmentList = organisationAttachmentRepository.findAll();
        assertThat(organisationAttachmentList).hasSize(databaseSizeBeforeUpdate);
        OrganisationAttachment testOrganisationAttachment = organisationAttachmentList.get(organisationAttachmentList.size() - 1);
        assertThat(testOrganisationAttachment.getAttachment()).isEqualTo(UPDATED_ATTACHMENT);

        // Validate the OrganisationAttachment in Elasticsearch
        verify(mockOrganisationAttachmentSearchRepository, times(1)).save(testOrganisationAttachment);
    }

    @Test
    @Transactional
    public void updateNonExistingOrganisationAttachment() throws Exception {
        int databaseSizeBeforeUpdate = organisationAttachmentRepository.findAll().size();

        // Create the OrganisationAttachment
        OrganisationAttachmentDTO organisationAttachmentDTO = organisationAttachmentMapper.toDto(organisationAttachment);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrganisationAttachmentMockMvc.perform(put("/api/organisation-attachments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationAttachmentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrganisationAttachment in the database
        List<OrganisationAttachment> organisationAttachmentList = organisationAttachmentRepository.findAll();
        assertThat(organisationAttachmentList).hasSize(databaseSizeBeforeUpdate);

        // Validate the OrganisationAttachment in Elasticsearch
        verify(mockOrganisationAttachmentSearchRepository, times(0)).save(organisationAttachment);
    }

    @Test
    @Transactional
    public void deleteOrganisationAttachment() throws Exception {
        // Initialize the database
        organisationAttachmentRepository.saveAndFlush(organisationAttachment);

        int databaseSizeBeforeDelete = organisationAttachmentRepository.findAll().size();

        // Delete the organisationAttachment
        restOrganisationAttachmentMockMvc.perform(delete("/api/organisation-attachments/{id}", organisationAttachment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrganisationAttachment> organisationAttachmentList = organisationAttachmentRepository.findAll();
        assertThat(organisationAttachmentList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the OrganisationAttachment in Elasticsearch
        verify(mockOrganisationAttachmentSearchRepository, times(1)).deleteById(organisationAttachment.getId());
    }

    @Test
    @Transactional
    public void searchOrganisationAttachment() throws Exception {
        // Initialize the database
        organisationAttachmentRepository.saveAndFlush(organisationAttachment);
        when(mockOrganisationAttachmentSearchRepository.search(queryStringQuery("id:" + organisationAttachment.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(organisationAttachment), PageRequest.of(0, 1), 1));
        // Search the organisationAttachment
        restOrganisationAttachmentMockMvc.perform(get("/api/_search/organisation-attachments?query=id:" + organisationAttachment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(organisationAttachment.getId().intValue())))
            .andExpect(jsonPath("$.[*].attachment").value(hasItem(DEFAULT_ATTACHMENT)));
    }
}
