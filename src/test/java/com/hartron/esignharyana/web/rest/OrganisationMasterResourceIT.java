package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.OrganisationMaster;
import com.hartron.esignharyana.repository.OrganisationMasterRepository;
import com.hartron.esignharyana.repository.search.OrganisationMasterSearchRepository;
import com.hartron.esignharyana.service.OrganisationMasterService;
import com.hartron.esignharyana.service.dto.OrganisationMasterDTO;
import com.hartron.esignharyana.service.mapper.OrganisationMasterMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OrganisationMasterResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class OrganisationMasterResourceIT {

    private static final String DEFAULT_ORGANISATION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ORGANISATION_ABBREVIATION = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_ABBREVIATION = "BBBBBBBBBB";

    private static final String DEFAULT_ORGANISATION_CORRESPONDENCE_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_CORRESPONDENCE_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_ORGANISATION_CORRESPONDENCE_PHONE_1 = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_CORRESPONDENCE_PHONE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_ORGANISATION_CORRESPONDENCE_PHONE_2 = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_CORRESPONDENCE_PHONE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_ORGANISATION_CORRESPONDENCE_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_CORRESPONDENCE_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_ORGANISATION_WEBSITE = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_WEBSITE = "BBBBBBBBBB";

    private static final String DEFAULT_ORGANISATION_NODAL_OFFICER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_NODAL_OFFICER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ORGANISATION_NODAL_OFFICER_PHONE_MOBILE = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_NODAL_OFFICER_PHONE_MOBILE = "BBBBBBBBBB";

    private static final String DEFAULT_ORGANISATION_NODAL_OFFICER_PHONE_LANDLINE = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_NODAL_OFFICER_PHONE_LANDLINE = "BBBBBBBBBB";

    private static final String DEFAULT_NODAL_OFFICER_OFFICIAL_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_NODAL_OFFICER_OFFICIAL_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_ORGANISATION_NODAL_OFFICER_ALTERNATIVE_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_NODAL_OFFICER_ALTERNATIVE_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LAST_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_VERIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_VERIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_VERIFIED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VERIFIED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final Integer DEFAULT_USER_ID = 1;
    private static final Integer UPDATED_USER_ID = 2;

    private static final String DEFAULT_STAGE = "AAAAAAAAAA";
    private static final String UPDATED_STAGE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_ORGANISATION_ID_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_ID_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_ORGANISATION_EMPLOYEE_ID = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_EMPLOYEE_ID = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_OF_BIRTH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_OF_BIRTH = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_GENDER = "AAAAAAAAAA";
    private static final String UPDATED_GENDER = "BBBBBBBBBB";

    private static final String DEFAULT_PAN = "AAAAAAAAAA";
    private static final String UPDATED_PAN = "BBBBBBBBBB";

    private static final String DEFAULT_AADHAAR = "AAAAAAAAAA";
    private static final String UPDATED_AADHAAR = "BBBBBBBBBB";

    private static final String DEFAULT_ORGNISATION_EMPOLYEE_CARD_ATTCHMENT = "AAAAAAAAAA";
    private static final String UPDATED_ORGNISATION_EMPOLYEE_CARD_ATTCHMENT = "BBBBBBBBBB";

    private static final String DEFAULT_PAN_ATTACHMENT = "AAAAAAAAAA";
    private static final String UPDATED_PAN_ATTACHMENT = "BBBBBBBBBB";

    private static final String DEFAULT_PHOTOGRAPH_ATTACHMENT = "AAAAAAAAAA";
    private static final String UPDATED_PHOTOGRAPH_ATTACHMENT = "BBBBBBBBBB";

    private static final String DEFAULT_AADHAAR_ATTACHMENT = "AAAAAAAAAA";
    private static final String UPDATED_AADHAAR_ATTACHMENT = "BBBBBBBBBB";

    private static final String DEFAULT_PIN_CODE = "AAAAAAAAAA";
    private static final String UPDATED_PIN_CODE = "BBBBBBBBBB";

    @Autowired
    private OrganisationMasterRepository organisationMasterRepository;

    @Autowired
    private OrganisationMasterMapper organisationMasterMapper;

    @Autowired
    private OrganisationMasterService organisationMasterService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.OrganisationMasterSearchRepositoryMockConfiguration
     */
    @Autowired
    private OrganisationMasterSearchRepository mockOrganisationMasterSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOrganisationMasterMockMvc;

    private OrganisationMaster organisationMaster;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OrganisationMasterResource organisationMasterResource = new OrganisationMasterResource(organisationMasterService);
        this.restOrganisationMasterMockMvc = MockMvcBuilders.standaloneSetup(organisationMasterResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrganisationMaster createEntity(EntityManager em) {
        OrganisationMaster organisationMaster = new OrganisationMaster()
            .organisationName(DEFAULT_ORGANISATION_NAME)
            .organisationAbbreviation(DEFAULT_ORGANISATION_ABBREVIATION)
            .organisationCorrespondenceEmail(DEFAULT_ORGANISATION_CORRESPONDENCE_EMAIL)
            .organisationCorrespondencePhone1(DEFAULT_ORGANISATION_CORRESPONDENCE_PHONE_1)
            .organisationCorrespondencePhone2(DEFAULT_ORGANISATION_CORRESPONDENCE_PHONE_2)
            .organisationCorrespondenceAddress(DEFAULT_ORGANISATION_CORRESPONDENCE_ADDRESS)
            .organisationWebsite(DEFAULT_ORGANISATION_WEBSITE)
            .organisationNodalOfficerName(DEFAULT_ORGANISATION_NODAL_OFFICER_NAME)
            .organisationNodalOfficerPhoneMobile(DEFAULT_ORGANISATION_NODAL_OFFICER_PHONE_MOBILE)
            .organisationNodalOfficerPhoneLandline(DEFAULT_ORGANISATION_NODAL_OFFICER_PHONE_LANDLINE)
            .nodalOfficerOfficialEmail(DEFAULT_NODAL_OFFICER_OFFICIAL_EMAIL)
            .organisationNodalOfficerAlternativeEmail(DEFAULT_ORGANISATION_NODAL_OFFICER_ALTERNATIVE_EMAIL)
            .createdBy(DEFAULT_CREATED_BY)
            .createdOn(DEFAULT_CREATED_ON)
            .lastUpdatedBy(DEFAULT_LAST_UPDATED_BY)
            .lastUpdatedOn(DEFAULT_LAST_UPDATED_ON)
            .verifiedBy(DEFAULT_VERIFIED_BY)
            .verifiedOn(DEFAULT_VERIFIED_ON)
            .remarks(DEFAULT_REMARKS)
            .userId(DEFAULT_USER_ID)
            .stage(DEFAULT_STAGE)
            .status(DEFAULT_STATUS)
            .organisationIdCode(DEFAULT_ORGANISATION_ID_CODE)
            .organisationEmployeeId(DEFAULT_ORGANISATION_EMPLOYEE_ID)
            .dateOfBirth(DEFAULT_DATE_OF_BIRTH)
            .gender(DEFAULT_GENDER)
            .pan(DEFAULT_PAN)
            .aadhaar(DEFAULT_AADHAAR)
            .orgnisationEmpolyeeCardAttchment(DEFAULT_ORGNISATION_EMPOLYEE_CARD_ATTCHMENT)
            .panAttachment(DEFAULT_PAN_ATTACHMENT)
            .photographAttachment(DEFAULT_PHOTOGRAPH_ATTACHMENT)
            .aadhaarAttachment(DEFAULT_AADHAAR_ATTACHMENT);
        return organisationMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrganisationMaster createUpdatedEntity(EntityManager em) {
        OrganisationMaster organisationMaster = new OrganisationMaster()
            .organisationName(UPDATED_ORGANISATION_NAME)
            .organisationAbbreviation(UPDATED_ORGANISATION_ABBREVIATION)
            .organisationCorrespondenceEmail(UPDATED_ORGANISATION_CORRESPONDENCE_EMAIL)
            .organisationCorrespondencePhone1(UPDATED_ORGANISATION_CORRESPONDENCE_PHONE_1)
            .organisationCorrespondencePhone2(UPDATED_ORGANISATION_CORRESPONDENCE_PHONE_2)
            .organisationCorrespondenceAddress(UPDATED_ORGANISATION_CORRESPONDENCE_ADDRESS)
            .organisationWebsite(UPDATED_ORGANISATION_WEBSITE)
            .organisationNodalOfficerName(UPDATED_ORGANISATION_NODAL_OFFICER_NAME)
            .organisationNodalOfficerPhoneMobile(UPDATED_ORGANISATION_NODAL_OFFICER_PHONE_MOBILE)
            .organisationNodalOfficerPhoneLandline(UPDATED_ORGANISATION_NODAL_OFFICER_PHONE_LANDLINE)
            .nodalOfficerOfficialEmail(UPDATED_NODAL_OFFICER_OFFICIAL_EMAIL)
            .organisationNodalOfficerAlternativeEmail(UPDATED_ORGANISATION_NODAL_OFFICER_ALTERNATIVE_EMAIL)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS)
            .userId(UPDATED_USER_ID)
            .stage(UPDATED_STAGE)
            .status(UPDATED_STATUS)
            .organisationIdCode(UPDATED_ORGANISATION_ID_CODE)
            .organisationEmployeeId(UPDATED_ORGANISATION_EMPLOYEE_ID)
            .dateOfBirth(UPDATED_DATE_OF_BIRTH)
            .gender(UPDATED_GENDER)
            .pan(UPDATED_PAN)
            .aadhaar(UPDATED_AADHAAR)
            .orgnisationEmpolyeeCardAttchment(UPDATED_ORGNISATION_EMPOLYEE_CARD_ATTCHMENT)
            .panAttachment(UPDATED_PAN_ATTACHMENT)
            .photographAttachment(UPDATED_PHOTOGRAPH_ATTACHMENT)
            .aadhaarAttachment(UPDATED_AADHAAR_ATTACHMENT);
        return organisationMaster;
    }

    @BeforeEach
    public void initTest() {
        organisationMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrganisationMaster() throws Exception {
        int databaseSizeBeforeCreate = organisationMasterRepository.findAll().size();

        // Create the OrganisationMaster
        OrganisationMasterDTO organisationMasterDTO = organisationMasterMapper.toDto(organisationMaster);
        restOrganisationMasterMockMvc.perform(post("/api/organisation-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationMasterDTO)))
            .andExpect(status().isCreated());

        // Validate the OrganisationMaster in the database
        List<OrganisationMaster> organisationMasterList = organisationMasterRepository.findAll();
        assertThat(organisationMasterList).hasSize(databaseSizeBeforeCreate + 1);
        OrganisationMaster testOrganisationMaster = organisationMasterList.get(organisationMasterList.size() - 1);
        assertThat(testOrganisationMaster.getOrganisationName()).isEqualTo(DEFAULT_ORGANISATION_NAME);
        assertThat(testOrganisationMaster.getOrganisationAbbreviation()).isEqualTo(DEFAULT_ORGANISATION_ABBREVIATION);
        assertThat(testOrganisationMaster.getOrganisationCorrespondenceEmail()).isEqualTo(DEFAULT_ORGANISATION_CORRESPONDENCE_EMAIL);
        assertThat(testOrganisationMaster.getOrganisationCorrespondencePhone1()).isEqualTo(DEFAULT_ORGANISATION_CORRESPONDENCE_PHONE_1);
        assertThat(testOrganisationMaster.getOrganisationCorrespondencePhone2()).isEqualTo(DEFAULT_ORGANISATION_CORRESPONDENCE_PHONE_2);
        assertThat(testOrganisationMaster.getOrganisationCorrespondenceAddress()).isEqualTo(DEFAULT_ORGANISATION_CORRESPONDENCE_ADDRESS);
        assertThat(testOrganisationMaster.getOrganisationWebsite()).isEqualTo(DEFAULT_ORGANISATION_WEBSITE);
        assertThat(testOrganisationMaster.getOrganisationNodalOfficerName()).isEqualTo(DEFAULT_ORGANISATION_NODAL_OFFICER_NAME);
        assertThat(testOrganisationMaster.getOrganisationNodalOfficerPhoneMobile()).isEqualTo(DEFAULT_ORGANISATION_NODAL_OFFICER_PHONE_MOBILE);
        assertThat(testOrganisationMaster.getOrganisationNodalOfficerPhoneLandline()).isEqualTo(DEFAULT_ORGANISATION_NODAL_OFFICER_PHONE_LANDLINE);
        assertThat(testOrganisationMaster.getNodalOfficerOfficialEmail()).isEqualTo(DEFAULT_NODAL_OFFICER_OFFICIAL_EMAIL);
        assertThat(testOrganisationMaster.getOrganisationNodalOfficerAlternativeEmail()).isEqualTo(DEFAULT_ORGANISATION_NODAL_OFFICER_ALTERNATIVE_EMAIL);
        assertThat(testOrganisationMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testOrganisationMaster.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testOrganisationMaster.getLastUpdatedBy()).isEqualTo(DEFAULT_LAST_UPDATED_BY);
        assertThat(testOrganisationMaster.getLastUpdatedOn()).isEqualTo(DEFAULT_LAST_UPDATED_ON);
        assertThat(testOrganisationMaster.getVerifiedBy()).isEqualTo(DEFAULT_VERIFIED_BY);
        assertThat(testOrganisationMaster.getVerifiedOn()).isEqualTo(DEFAULT_VERIFIED_ON);
        assertThat(testOrganisationMaster.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testOrganisationMaster.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testOrganisationMaster.getStage()).isEqualTo(DEFAULT_STAGE);
        assertThat(testOrganisationMaster.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testOrganisationMaster.getOrganisationIdCode()).isEqualTo(DEFAULT_ORGANISATION_ID_CODE);
        assertThat(testOrganisationMaster.getOrganisationEmployeeId()).isEqualTo(DEFAULT_ORGANISATION_EMPLOYEE_ID);
        assertThat(testOrganisationMaster.getDateOfBirth()).isEqualTo(DEFAULT_DATE_OF_BIRTH);
        assertThat(testOrganisationMaster.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testOrganisationMaster.getPan()).isEqualTo(DEFAULT_PAN);
        assertThat(testOrganisationMaster.getAadhaar()).isEqualTo(DEFAULT_AADHAAR);
        assertThat(testOrganisationMaster.getOrgnisationEmpolyeeCardAttchment()).isEqualTo(DEFAULT_ORGNISATION_EMPOLYEE_CARD_ATTCHMENT);
        assertThat(testOrganisationMaster.getPanAttachment()).isEqualTo(DEFAULT_PAN_ATTACHMENT);
        assertThat(testOrganisationMaster.getPhotographAttachment()).isEqualTo(DEFAULT_PHOTOGRAPH_ATTACHMENT);
        assertThat(testOrganisationMaster.getAadhaarAttachment()).isEqualTo(DEFAULT_AADHAAR_ATTACHMENT);

        // Validate the OrganisationMaster in Elasticsearch
        verify(mockOrganisationMasterSearchRepository, times(1)).save(testOrganisationMaster);
    }

    @Test
    @Transactional
    public void createOrganisationMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = organisationMasterRepository.findAll().size();

        // Create the OrganisationMaster with an existing ID
        organisationMaster.setId(1L);
        OrganisationMasterDTO organisationMasterDTO = organisationMasterMapper.toDto(organisationMaster);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrganisationMasterMockMvc.perform(post("/api/organisation-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrganisationMaster in the database
        List<OrganisationMaster> organisationMasterList = organisationMasterRepository.findAll();
        assertThat(organisationMasterList).hasSize(databaseSizeBeforeCreate);

        // Validate the OrganisationMaster in Elasticsearch
        verify(mockOrganisationMasterSearchRepository, times(0)).save(organisationMaster);
    }


    @Test
    @Transactional
    public void checkOrganisationNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = organisationMasterRepository.findAll().size();
        // set the field null
        organisationMaster.setOrganisationName(null);

        // Create the OrganisationMaster, which fails.
        OrganisationMasterDTO organisationMasterDTO = organisationMasterMapper.toDto(organisationMaster);

        restOrganisationMasterMockMvc.perform(post("/api/organisation-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationMasterDTO)))
            .andExpect(status().isBadRequest());

        List<OrganisationMaster> organisationMasterList = organisationMasterRepository.findAll();
        assertThat(organisationMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOrganisationAbbreviationIsRequired() throws Exception {
        int databaseSizeBeforeTest = organisationMasterRepository.findAll().size();
        // set the field null
        organisationMaster.setOrganisationAbbreviation(null);

        // Create the OrganisationMaster, which fails.
        OrganisationMasterDTO organisationMasterDTO = organisationMasterMapper.toDto(organisationMaster);

        restOrganisationMasterMockMvc.perform(post("/api/organisation-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationMasterDTO)))
            .andExpect(status().isBadRequest());

        List<OrganisationMaster> organisationMasterList = organisationMasterRepository.findAll();
        assertThat(organisationMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOrganisationCorrespondenceEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = organisationMasterRepository.findAll().size();
        // set the field null
        organisationMaster.setOrganisationCorrespondenceEmail(null);

        // Create the OrganisationMaster, which fails.
        OrganisationMasterDTO organisationMasterDTO = organisationMasterMapper.toDto(organisationMaster);

        restOrganisationMasterMockMvc.perform(post("/api/organisation-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationMasterDTO)))
            .andExpect(status().isBadRequest());

        List<OrganisationMaster> organisationMasterList = organisationMasterRepository.findAll();
        assertThat(organisationMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOrganisationCorrespondencePhone1IsRequired() throws Exception {
        int databaseSizeBeforeTest = organisationMasterRepository.findAll().size();
        // set the field null
        organisationMaster.setOrganisationCorrespondencePhone1(null);

        // Create the OrganisationMaster, which fails.
        OrganisationMasterDTO organisationMasterDTO = organisationMasterMapper.toDto(organisationMaster);

        restOrganisationMasterMockMvc.perform(post("/api/organisation-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationMasterDTO)))
            .andExpect(status().isBadRequest());

        List<OrganisationMaster> organisationMasterList = organisationMasterRepository.findAll();
        assertThat(organisationMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOrganisationMasters() throws Exception {
        // Initialize the database
        organisationMasterRepository.saveAndFlush(organisationMaster);

        // Get all the organisationMasterList
        restOrganisationMasterMockMvc.perform(get("/api/organisation-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(organisationMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].organisationName").value(hasItem(DEFAULT_ORGANISATION_NAME)))
            .andExpect(jsonPath("$.[*].organisationAbbreviation").value(hasItem(DEFAULT_ORGANISATION_ABBREVIATION)))
            .andExpect(jsonPath("$.[*].organisationCorrespondenceEmail").value(hasItem(DEFAULT_ORGANISATION_CORRESPONDENCE_EMAIL)))
            .andExpect(jsonPath("$.[*].organisationCorrespondencePhone1").value(hasItem(DEFAULT_ORGANISATION_CORRESPONDENCE_PHONE_1)))
            .andExpect(jsonPath("$.[*].organisationCorrespondencePhone2").value(hasItem(DEFAULT_ORGANISATION_CORRESPONDENCE_PHONE_2)))
            .andExpect(jsonPath("$.[*].organisationCorrespondenceAddress").value(hasItem(DEFAULT_ORGANISATION_CORRESPONDENCE_ADDRESS)))
            .andExpect(jsonPath("$.[*].organisationWebsite").value(hasItem(DEFAULT_ORGANISATION_WEBSITE)))
            .andExpect(jsonPath("$.[*].organisationNodalOfficerName").value(hasItem(DEFAULT_ORGANISATION_NODAL_OFFICER_NAME)))
            .andExpect(jsonPath("$.[*].organisationNodalOfficerPhoneMobile").value(hasItem(DEFAULT_ORGANISATION_NODAL_OFFICER_PHONE_MOBILE)))
            .andExpect(jsonPath("$.[*].organisationNodalOfficerPhoneLandline").value(hasItem(DEFAULT_ORGANISATION_NODAL_OFFICER_PHONE_LANDLINE)))
            .andExpect(jsonPath("$.[*].nodalOfficerOfficialEmail").value(hasItem(DEFAULT_NODAL_OFFICER_OFFICIAL_EMAIL)))
            .andExpect(jsonPath("$.[*].organisationNodalOfficerAlternativeEmail").value(hasItem(DEFAULT_ORGANISATION_NODAL_OFFICER_ALTERNATIVE_EMAIL)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].stage").value(hasItem(DEFAULT_STAGE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].organisationIdCode").value(hasItem(DEFAULT_ORGANISATION_ID_CODE)))
            .andExpect(jsonPath("$.[*].organisationEmployeeId").value(hasItem(DEFAULT_ORGANISATION_EMPLOYEE_ID)))
            .andExpect(jsonPath("$.[*].dateOfBirth").value(hasItem(DEFAULT_DATE_OF_BIRTH.toString())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            .andExpect(jsonPath("$.[*].pan").value(hasItem(DEFAULT_PAN)))
            .andExpect(jsonPath("$.[*].aadhaar").value(hasItem(DEFAULT_AADHAAR)))
            .andExpect(jsonPath("$.[*].orgnisationEmpolyeeCardAttchment").value(hasItem(DEFAULT_ORGNISATION_EMPOLYEE_CARD_ATTCHMENT)))
            .andExpect(jsonPath("$.[*].panAttachment").value(hasItem(DEFAULT_PAN_ATTACHMENT)))
            .andExpect(jsonPath("$.[*].photographAttachment").value(hasItem(DEFAULT_PHOTOGRAPH_ATTACHMENT)))
            .andExpect(jsonPath("$.[*].aadhaarAttachment").value(hasItem(DEFAULT_AADHAAR_ATTACHMENT)));
    }
    
    @Test
    @Transactional
    public void getOrganisationMaster() throws Exception {
        // Initialize the database
        organisationMasterRepository.saveAndFlush(organisationMaster);

        // Get the organisationMaster
        restOrganisationMasterMockMvc.perform(get("/api/organisation-masters/{id}", organisationMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(organisationMaster.getId().intValue()))
            .andExpect(jsonPath("$.organisationName").value(DEFAULT_ORGANISATION_NAME))
            .andExpect(jsonPath("$.organisationAbbreviation").value(DEFAULT_ORGANISATION_ABBREVIATION))
            .andExpect(jsonPath("$.organisationCorrespondenceEmail").value(DEFAULT_ORGANISATION_CORRESPONDENCE_EMAIL))
            .andExpect(jsonPath("$.organisationCorrespondencePhone1").value(DEFAULT_ORGANISATION_CORRESPONDENCE_PHONE_1))
            .andExpect(jsonPath("$.organisationCorrespondencePhone2").value(DEFAULT_ORGANISATION_CORRESPONDENCE_PHONE_2))
            .andExpect(jsonPath("$.organisationCorrespondenceAddress").value(DEFAULT_ORGANISATION_CORRESPONDENCE_ADDRESS))
            .andExpect(jsonPath("$.organisationWebsite").value(DEFAULT_ORGANISATION_WEBSITE))
            .andExpect(jsonPath("$.organisationNodalOfficerName").value(DEFAULT_ORGANISATION_NODAL_OFFICER_NAME))
            .andExpect(jsonPath("$.organisationNodalOfficerPhoneMobile").value(DEFAULT_ORGANISATION_NODAL_OFFICER_PHONE_MOBILE))
            .andExpect(jsonPath("$.organisationNodalOfficerPhoneLandline").value(DEFAULT_ORGANISATION_NODAL_OFFICER_PHONE_LANDLINE))
            .andExpect(jsonPath("$.nodalOfficerOfficialEmail").value(DEFAULT_NODAL_OFFICER_OFFICIAL_EMAIL))
            .andExpect(jsonPath("$.organisationNodalOfficerAlternativeEmail").value(DEFAULT_ORGANISATION_NODAL_OFFICER_ALTERNATIVE_EMAIL))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.lastUpdatedBy").value(DEFAULT_LAST_UPDATED_BY))
            .andExpect(jsonPath("$.lastUpdatedOn").value(DEFAULT_LAST_UPDATED_ON.toString()))
            .andExpect(jsonPath("$.verifiedBy").value(DEFAULT_VERIFIED_BY))
            .andExpect(jsonPath("$.verifiedOn").value(DEFAULT_VERIFIED_ON.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.stage").value(DEFAULT_STAGE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.organisationIdCode").value(DEFAULT_ORGANISATION_ID_CODE))
            .andExpect(jsonPath("$.organisationEmployeeId").value(DEFAULT_ORGANISATION_EMPLOYEE_ID))
            .andExpect(jsonPath("$.dateOfBirth").value(DEFAULT_DATE_OF_BIRTH.toString()))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER))
            .andExpect(jsonPath("$.pan").value(DEFAULT_PAN))
            .andExpect(jsonPath("$.aadhaar").value(DEFAULT_AADHAAR))
            .andExpect(jsonPath("$.orgnisationEmpolyeeCardAttchment").value(DEFAULT_ORGNISATION_EMPOLYEE_CARD_ATTCHMENT))
            .andExpect(jsonPath("$.panAttachment").value(DEFAULT_PAN_ATTACHMENT))
            .andExpect(jsonPath("$.photographAttachment").value(DEFAULT_PHOTOGRAPH_ATTACHMENT))
            .andExpect(jsonPath("$.aadhaarAttachment").value(DEFAULT_AADHAAR_ATTACHMENT));
    }

    @Test
    @Transactional
    public void getNonExistingOrganisationMaster() throws Exception {
        // Get the organisationMaster
        restOrganisationMasterMockMvc.perform(get("/api/organisation-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrganisationMaster() throws Exception {
        // Initialize the database
        organisationMasterRepository.saveAndFlush(organisationMaster);

        int databaseSizeBeforeUpdate = organisationMasterRepository.findAll().size();

        // Update the organisationMaster
        OrganisationMaster updatedOrganisationMaster = organisationMasterRepository.findById(organisationMaster.getId()).get();
        // Disconnect from session so that the updates on updatedOrganisationMaster are not directly saved in db
        em.detach(updatedOrganisationMaster);
        updatedOrganisationMaster
            .organisationName(UPDATED_ORGANISATION_NAME)
            .organisationAbbreviation(UPDATED_ORGANISATION_ABBREVIATION)
            .organisationCorrespondenceEmail(UPDATED_ORGANISATION_CORRESPONDENCE_EMAIL)
            .organisationCorrespondencePhone1(UPDATED_ORGANISATION_CORRESPONDENCE_PHONE_1)
            .organisationCorrespondencePhone2(UPDATED_ORGANISATION_CORRESPONDENCE_PHONE_2)
            .organisationCorrespondenceAddress(UPDATED_ORGANISATION_CORRESPONDENCE_ADDRESS)
            .organisationWebsite(UPDATED_ORGANISATION_WEBSITE)
            .organisationNodalOfficerName(UPDATED_ORGANISATION_NODAL_OFFICER_NAME)
            .organisationNodalOfficerPhoneMobile(UPDATED_ORGANISATION_NODAL_OFFICER_PHONE_MOBILE)
            .organisationNodalOfficerPhoneLandline(UPDATED_ORGANISATION_NODAL_OFFICER_PHONE_LANDLINE)
            .nodalOfficerOfficialEmail(UPDATED_NODAL_OFFICER_OFFICIAL_EMAIL)
            .organisationNodalOfficerAlternativeEmail(UPDATED_ORGANISATION_NODAL_OFFICER_ALTERNATIVE_EMAIL)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS)
            .userId(UPDATED_USER_ID)
            .stage(UPDATED_STAGE)
            .status(UPDATED_STATUS)
            .organisationIdCode(UPDATED_ORGANISATION_ID_CODE)
            .organisationEmployeeId(UPDATED_ORGANISATION_EMPLOYEE_ID)
            .dateOfBirth(UPDATED_DATE_OF_BIRTH)
            .gender(UPDATED_GENDER)
            .pan(UPDATED_PAN)
            .aadhaar(UPDATED_AADHAAR)
            .orgnisationEmpolyeeCardAttchment(UPDATED_ORGNISATION_EMPOLYEE_CARD_ATTCHMENT)
            .panAttachment(UPDATED_PAN_ATTACHMENT)
            .photographAttachment(UPDATED_PHOTOGRAPH_ATTACHMENT)
            .aadhaarAttachment(UPDATED_AADHAAR_ATTACHMENT);
        OrganisationMasterDTO organisationMasterDTO = organisationMasterMapper.toDto(updatedOrganisationMaster);

        restOrganisationMasterMockMvc.perform(put("/api/organisation-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationMasterDTO)))
            .andExpect(status().isOk());

        // Validate the OrganisationMaster in the database
        List<OrganisationMaster> organisationMasterList = organisationMasterRepository.findAll();
        assertThat(organisationMasterList).hasSize(databaseSizeBeforeUpdate);
        OrganisationMaster testOrganisationMaster = organisationMasterList.get(organisationMasterList.size() - 1);
        assertThat(testOrganisationMaster.getOrganisationName()).isEqualTo(UPDATED_ORGANISATION_NAME);
        assertThat(testOrganisationMaster.getOrganisationAbbreviation()).isEqualTo(UPDATED_ORGANISATION_ABBREVIATION);
        assertThat(testOrganisationMaster.getOrganisationCorrespondenceEmail()).isEqualTo(UPDATED_ORGANISATION_CORRESPONDENCE_EMAIL);
        assertThat(testOrganisationMaster.getOrganisationCorrespondencePhone1()).isEqualTo(UPDATED_ORGANISATION_CORRESPONDENCE_PHONE_1);
        assertThat(testOrganisationMaster.getOrganisationCorrespondencePhone2()).isEqualTo(UPDATED_ORGANISATION_CORRESPONDENCE_PHONE_2);
        assertThat(testOrganisationMaster.getOrganisationCorrespondenceAddress()).isEqualTo(UPDATED_ORGANISATION_CORRESPONDENCE_ADDRESS);
        assertThat(testOrganisationMaster.getOrganisationWebsite()).isEqualTo(UPDATED_ORGANISATION_WEBSITE);
        assertThat(testOrganisationMaster.getOrganisationNodalOfficerName()).isEqualTo(UPDATED_ORGANISATION_NODAL_OFFICER_NAME);
        assertThat(testOrganisationMaster.getOrganisationNodalOfficerPhoneMobile()).isEqualTo(UPDATED_ORGANISATION_NODAL_OFFICER_PHONE_MOBILE);
        assertThat(testOrganisationMaster.getOrganisationNodalOfficerPhoneLandline()).isEqualTo(UPDATED_ORGANISATION_NODAL_OFFICER_PHONE_LANDLINE);
        assertThat(testOrganisationMaster.getNodalOfficerOfficialEmail()).isEqualTo(UPDATED_NODAL_OFFICER_OFFICIAL_EMAIL);
        assertThat(testOrganisationMaster.getOrganisationNodalOfficerAlternativeEmail()).isEqualTo(UPDATED_ORGANISATION_NODAL_OFFICER_ALTERNATIVE_EMAIL);
        assertThat(testOrganisationMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testOrganisationMaster.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testOrganisationMaster.getLastUpdatedBy()).isEqualTo(UPDATED_LAST_UPDATED_BY);
        assertThat(testOrganisationMaster.getLastUpdatedOn()).isEqualTo(UPDATED_LAST_UPDATED_ON);
        assertThat(testOrganisationMaster.getVerifiedBy()).isEqualTo(UPDATED_VERIFIED_BY);
        assertThat(testOrganisationMaster.getVerifiedOn()).isEqualTo(UPDATED_VERIFIED_ON);
        assertThat(testOrganisationMaster.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testOrganisationMaster.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testOrganisationMaster.getStage()).isEqualTo(UPDATED_STAGE);
        assertThat(testOrganisationMaster.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOrganisationMaster.getOrganisationIdCode()).isEqualTo(UPDATED_ORGANISATION_ID_CODE);
        assertThat(testOrganisationMaster.getOrganisationEmployeeId()).isEqualTo(UPDATED_ORGANISATION_EMPLOYEE_ID);
        assertThat(testOrganisationMaster.getDateOfBirth()).isEqualTo(UPDATED_DATE_OF_BIRTH);
        assertThat(testOrganisationMaster.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testOrganisationMaster.getPan()).isEqualTo(UPDATED_PAN);
        assertThat(testOrganisationMaster.getAadhaar()).isEqualTo(UPDATED_AADHAAR);
        assertThat(testOrganisationMaster.getOrgnisationEmpolyeeCardAttchment()).isEqualTo(UPDATED_ORGNISATION_EMPOLYEE_CARD_ATTCHMENT);
        assertThat(testOrganisationMaster.getPanAttachment()).isEqualTo(UPDATED_PAN_ATTACHMENT);
        assertThat(testOrganisationMaster.getPhotographAttachment()).isEqualTo(UPDATED_PHOTOGRAPH_ATTACHMENT);
        assertThat(testOrganisationMaster.getAadhaarAttachment()).isEqualTo(UPDATED_AADHAAR_ATTACHMENT);

        // Validate the OrganisationMaster in Elasticsearch
        verify(mockOrganisationMasterSearchRepository, times(1)).save(testOrganisationMaster);
    }

    @Test
    @Transactional
    public void updateNonExistingOrganisationMaster() throws Exception {
        int databaseSizeBeforeUpdate = organisationMasterRepository.findAll().size();

        // Create the OrganisationMaster
        OrganisationMasterDTO organisationMasterDTO = organisationMasterMapper.toDto(organisationMaster);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrganisationMasterMockMvc.perform(put("/api/organisation-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrganisationMaster in the database
        List<OrganisationMaster> organisationMasterList = organisationMasterRepository.findAll();
        assertThat(organisationMasterList).hasSize(databaseSizeBeforeUpdate);

        // Validate the OrganisationMaster in Elasticsearch
        verify(mockOrganisationMasterSearchRepository, times(0)).save(organisationMaster);
    }

    @Test
    @Transactional
    public void deleteOrganisationMaster() throws Exception {
        // Initialize the database
        organisationMasterRepository.saveAndFlush(organisationMaster);

        int databaseSizeBeforeDelete = organisationMasterRepository.findAll().size();

        // Delete the organisationMaster
        restOrganisationMasterMockMvc.perform(delete("/api/organisation-masters/{id}", organisationMaster.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrganisationMaster> organisationMasterList = organisationMasterRepository.findAll();
        assertThat(organisationMasterList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the OrganisationMaster in Elasticsearch
        verify(mockOrganisationMasterSearchRepository, times(1)).deleteById(organisationMaster.getId());
    }

    @Test
    @Transactional
    public void searchOrganisationMaster() throws Exception {
        // Initialize the database
        organisationMasterRepository.saveAndFlush(organisationMaster);
        when(mockOrganisationMasterSearchRepository.search(queryStringQuery("id:" + organisationMaster.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(organisationMaster), PageRequest.of(0, 1), 1));
        // Search the organisationMaster
        restOrganisationMasterMockMvc.perform(get("/api/_search/organisation-masters?query=id:" + organisationMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(organisationMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].organisationName").value(hasItem(DEFAULT_ORGANISATION_NAME)))
            .andExpect(jsonPath("$.[*].organisationAbbreviation").value(hasItem(DEFAULT_ORGANISATION_ABBREVIATION)))
            .andExpect(jsonPath("$.[*].organisationCorrespondenceEmail").value(hasItem(DEFAULT_ORGANISATION_CORRESPONDENCE_EMAIL)))
            .andExpect(jsonPath("$.[*].organisationCorrespondencePhone1").value(hasItem(DEFAULT_ORGANISATION_CORRESPONDENCE_PHONE_1)))
            .andExpect(jsonPath("$.[*].organisationCorrespondencePhone2").value(hasItem(DEFAULT_ORGANISATION_CORRESPONDENCE_PHONE_2)))
            .andExpect(jsonPath("$.[*].organisationCorrespondenceAddress").value(hasItem(DEFAULT_ORGANISATION_CORRESPONDENCE_ADDRESS)))
            .andExpect(jsonPath("$.[*].organisationWebsite").value(hasItem(DEFAULT_ORGANISATION_WEBSITE)))
            .andExpect(jsonPath("$.[*].organisationNodalOfficerName").value(hasItem(DEFAULT_ORGANISATION_NODAL_OFFICER_NAME)))
            .andExpect(jsonPath("$.[*].organisationNodalOfficerPhoneMobile").value(hasItem(DEFAULT_ORGANISATION_NODAL_OFFICER_PHONE_MOBILE)))
            .andExpect(jsonPath("$.[*].organisationNodalOfficerPhoneLandline").value(hasItem(DEFAULT_ORGANISATION_NODAL_OFFICER_PHONE_LANDLINE)))
            .andExpect(jsonPath("$.[*].nodalOfficerOfficialEmail").value(hasItem(DEFAULT_NODAL_OFFICER_OFFICIAL_EMAIL)))
            .andExpect(jsonPath("$.[*].organisationNodalOfficerAlternativeEmail").value(hasItem(DEFAULT_ORGANISATION_NODAL_OFFICER_ALTERNATIVE_EMAIL)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].stage").value(hasItem(DEFAULT_STAGE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].organisationIdCode").value(hasItem(DEFAULT_ORGANISATION_ID_CODE)))
            .andExpect(jsonPath("$.[*].organisationEmployeeId").value(hasItem(DEFAULT_ORGANISATION_EMPLOYEE_ID)))
            .andExpect(jsonPath("$.[*].dateOfBirth").value(hasItem(DEFAULT_DATE_OF_BIRTH.toString())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            .andExpect(jsonPath("$.[*].pan").value(hasItem(DEFAULT_PAN)))
            .andExpect(jsonPath("$.[*].aadhaar").value(hasItem(DEFAULT_AADHAAR)))
            .andExpect(jsonPath("$.[*].orgnisationEmpolyeeCardAttchment").value(hasItem(DEFAULT_ORGNISATION_EMPOLYEE_CARD_ATTCHMENT)))
            .andExpect(jsonPath("$.[*].panAttachment").value(hasItem(DEFAULT_PAN_ATTACHMENT)))
            .andExpect(jsonPath("$.[*].photographAttachment").value(hasItem(DEFAULT_PHOTOGRAPH_ATTACHMENT)))
            .andExpect(jsonPath("$.[*].aadhaarAttachment").value(hasItem(DEFAULT_AADHAAR_ATTACHMENT)));
    }
}
