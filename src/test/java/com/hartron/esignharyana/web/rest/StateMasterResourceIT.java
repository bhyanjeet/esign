package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.StateMaster;
import com.hartron.esignharyana.repository.StateMasterRepository;
import com.hartron.esignharyana.repository.search.StateMasterSearchRepository;
import com.hartron.esignharyana.service.StateMasterService;
import com.hartron.esignharyana.service.dto.StateMasterDTO;
import com.hartron.esignharyana.service.mapper.StateMasterMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link StateMasterResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class StateMasterResourceIT {

    private static final String DEFAULT_STATE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_STATE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_STATE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_STATE_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_LAST_UPDATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LAST_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_UPDATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_VERIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_VERIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_VERIFIED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VERIFIED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    @Autowired
    private StateMasterRepository stateMasterRepository;

    @Autowired
    private StateMasterMapper stateMasterMapper;

    @Autowired
    private StateMasterService stateMasterService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.StateMasterSearchRepositoryMockConfiguration
     */
    @Autowired
    private StateMasterSearchRepository mockStateMasterSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restStateMasterMockMvc;

    private StateMaster stateMaster;

    @BeforeEach
    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final StateMasterResource stateMasterResource = new StateMasterResource(stateMasterService);
//        this.restStateMasterMockMvc = MockMvcBuilders.standaloneSetup(stateMasterResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StateMaster createEntity(EntityManager em) {
        StateMaster stateMaster = new StateMaster()
            .stateCode(DEFAULT_STATE_CODE)
            .stateName(DEFAULT_STATE_NAME)
            .createdOn(DEFAULT_CREATED_ON)
            .lastUpdatedOn(DEFAULT_LAST_UPDATED_ON)
            .lastUpdatedBy(DEFAULT_LAST_UPDATED_BY)
            .verifiedBy(DEFAULT_VERIFIED_BY)
            .verifiedOn(DEFAULT_VERIFIED_ON)
            .remarks(DEFAULT_REMARKS);
        return stateMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StateMaster createUpdatedEntity(EntityManager em) {
        StateMaster stateMaster = new StateMaster()
            .stateCode(UPDATED_STATE_CODE)
            .stateName(UPDATED_STATE_NAME)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS);
        return stateMaster;
    }

    @BeforeEach
    public void initTest() {
        stateMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createStateMaster() throws Exception {
        int databaseSizeBeforeCreate = stateMasterRepository.findAll().size();

        // Create the StateMaster
        StateMasterDTO stateMasterDTO = stateMasterMapper.toDto(stateMaster);
        restStateMasterMockMvc.perform(post("/api/state-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stateMasterDTO)))
            .andExpect(status().isCreated());

        // Validate the StateMaster in the database
        List<StateMaster> stateMasterList = stateMasterRepository.findAll();
        assertThat(stateMasterList).hasSize(databaseSizeBeforeCreate + 1);
        StateMaster testStateMaster = stateMasterList.get(stateMasterList.size() - 1);
        assertThat(testStateMaster.getStateCode()).isEqualTo(DEFAULT_STATE_CODE);
        assertThat(testStateMaster.getStateName()).isEqualTo(DEFAULT_STATE_NAME);
        assertThat(testStateMaster.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testStateMaster.getLastUpdatedOn()).isEqualTo(DEFAULT_LAST_UPDATED_ON);
        assertThat(testStateMaster.getLastUpdatedBy()).isEqualTo(DEFAULT_LAST_UPDATED_BY);
        assertThat(testStateMaster.getVerifiedBy()).isEqualTo(DEFAULT_VERIFIED_BY);
        assertThat(testStateMaster.getVerifiedOn()).isEqualTo(DEFAULT_VERIFIED_ON);
        assertThat(testStateMaster.getRemarks()).isEqualTo(DEFAULT_REMARKS);

        // Validate the StateMaster in Elasticsearch
        verify(mockStateMasterSearchRepository, times(1)).save(testStateMaster);
    }

    @Test
    @Transactional
    public void createStateMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = stateMasterRepository.findAll().size();

        // Create the StateMaster with an existing ID
        stateMaster.setId(1L);
        StateMasterDTO stateMasterDTO = stateMasterMapper.toDto(stateMaster);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStateMasterMockMvc.perform(post("/api/state-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stateMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the StateMaster in the database
        List<StateMaster> stateMasterList = stateMasterRepository.findAll();
        assertThat(stateMasterList).hasSize(databaseSizeBeforeCreate);

        // Validate the StateMaster in Elasticsearch
        verify(mockStateMasterSearchRepository, times(0)).save(stateMaster);
    }


    @Test
    @Transactional
    public void checkStateNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = stateMasterRepository.findAll().size();
        // set the field null
        stateMaster.setStateName(null);

        // Create the StateMaster, which fails.
        StateMasterDTO stateMasterDTO = stateMasterMapper.toDto(stateMaster);

        restStateMasterMockMvc.perform(post("/api/state-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stateMasterDTO)))
            .andExpect(status().isBadRequest());

        List<StateMaster> stateMasterList = stateMasterRepository.findAll();
        assertThat(stateMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllStateMasters() throws Exception {
        // Initialize the database
        stateMasterRepository.saveAndFlush(stateMaster);

        // Get all the stateMasterList
        restStateMasterMockMvc.perform(get("/api/state-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(stateMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].stateCode").value(hasItem(DEFAULT_STATE_CODE)))
            .andExpect(jsonPath("$.[*].stateName").value(hasItem(DEFAULT_STATE_NAME)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
    
    @Test
    @Transactional
    public void getStateMaster() throws Exception {
        // Initialize the database
        stateMasterRepository.saveAndFlush(stateMaster);

        // Get the stateMaster
        restStateMasterMockMvc.perform(get("/api/state-masters/{id}", stateMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(stateMaster.getId().intValue()))
            .andExpect(jsonPath("$.stateCode").value(DEFAULT_STATE_CODE))
            .andExpect(jsonPath("$.stateName").value(DEFAULT_STATE_NAME))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.lastUpdatedOn").value(DEFAULT_LAST_UPDATED_ON.toString()))
            .andExpect(jsonPath("$.lastUpdatedBy").value(DEFAULT_LAST_UPDATED_BY))
            .andExpect(jsonPath("$.verifiedBy").value(DEFAULT_VERIFIED_BY))
            .andExpect(jsonPath("$.verifiedOn").value(DEFAULT_VERIFIED_ON.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS));
    }

    @Test
    @Transactional
    public void getNonExistingStateMaster() throws Exception {
        // Get the stateMaster
        restStateMasterMockMvc.perform(get("/api/state-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStateMaster() throws Exception {
        // Initialize the database
        stateMasterRepository.saveAndFlush(stateMaster);

        int databaseSizeBeforeUpdate = stateMasterRepository.findAll().size();

        // Update the stateMaster
        StateMaster updatedStateMaster = stateMasterRepository.findById(stateMaster.getId()).get();
        // Disconnect from session so that the updates on updatedStateMaster are not directly saved in db
        em.detach(updatedStateMaster);
        updatedStateMaster
            .stateCode(UPDATED_STATE_CODE)
            .stateName(UPDATED_STATE_NAME)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS);
        StateMasterDTO stateMasterDTO = stateMasterMapper.toDto(updatedStateMaster);

        restStateMasterMockMvc.perform(put("/api/state-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stateMasterDTO)))
            .andExpect(status().isOk());

        // Validate the StateMaster in the database
        List<StateMaster> stateMasterList = stateMasterRepository.findAll();
        assertThat(stateMasterList).hasSize(databaseSizeBeforeUpdate);
        StateMaster testStateMaster = stateMasterList.get(stateMasterList.size() - 1);
        assertThat(testStateMaster.getStateCode()).isEqualTo(UPDATED_STATE_CODE);
        assertThat(testStateMaster.getStateName()).isEqualTo(UPDATED_STATE_NAME);
        assertThat(testStateMaster.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testStateMaster.getLastUpdatedOn()).isEqualTo(UPDATED_LAST_UPDATED_ON);
        assertThat(testStateMaster.getLastUpdatedBy()).isEqualTo(UPDATED_LAST_UPDATED_BY);
        assertThat(testStateMaster.getVerifiedBy()).isEqualTo(UPDATED_VERIFIED_BY);
        assertThat(testStateMaster.getVerifiedOn()).isEqualTo(UPDATED_VERIFIED_ON);
        assertThat(testStateMaster.getRemarks()).isEqualTo(UPDATED_REMARKS);

        // Validate the StateMaster in Elasticsearch
        verify(mockStateMasterSearchRepository, times(1)).save(testStateMaster);
    }

    @Test
    @Transactional
    public void updateNonExistingStateMaster() throws Exception {
        int databaseSizeBeforeUpdate = stateMasterRepository.findAll().size();

        // Create the StateMaster
        StateMasterDTO stateMasterDTO = stateMasterMapper.toDto(stateMaster);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStateMasterMockMvc.perform(put("/api/state-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stateMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the StateMaster in the database
        List<StateMaster> stateMasterList = stateMasterRepository.findAll();
        assertThat(stateMasterList).hasSize(databaseSizeBeforeUpdate);

        // Validate the StateMaster in Elasticsearch
        verify(mockStateMasterSearchRepository, times(0)).save(stateMaster);
    }

    @Test
    @Transactional
    public void deleteStateMaster() throws Exception {
        // Initialize the database
        stateMasterRepository.saveAndFlush(stateMaster);

        int databaseSizeBeforeDelete = stateMasterRepository.findAll().size();

        // Delete the stateMaster
        restStateMasterMockMvc.perform(delete("/api/state-masters/{id}", stateMaster.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<StateMaster> stateMasterList = stateMasterRepository.findAll();
        assertThat(stateMasterList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the StateMaster in Elasticsearch
        verify(mockStateMasterSearchRepository, times(1)).deleteById(stateMaster.getId());
    }

    @Test
    @Transactional
    public void searchStateMaster() throws Exception {
        // Initialize the database
        stateMasterRepository.saveAndFlush(stateMaster);
        when(mockStateMasterSearchRepository.search(queryStringQuery("id:" + stateMaster.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(stateMaster), PageRequest.of(0, 1), 1));
        // Search the stateMaster
        restStateMasterMockMvc.perform(get("/api/_search/state-masters?query=id:" + stateMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(stateMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].stateCode").value(hasItem(DEFAULT_STATE_CODE)))
            .andExpect(jsonPath("$.[*].stateName").value(hasItem(DEFAULT_STATE_NAME)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
}
