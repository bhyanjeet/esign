package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.OrganisationDocument;
import com.hartron.esignharyana.repository.OrganisationDocumentRepository;
import com.hartron.esignharyana.repository.search.OrganisationDocumentSearchRepository;
import com.hartron.esignharyana.service.OrganisationDocumentService;
import com.hartron.esignharyana.service.dto.OrganisationDocumentDTO;
import com.hartron.esignharyana.service.mapper.OrganisationDocumentMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OrganisationDocumentResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class OrganisationDocumentResourceIT {

    private static final String DEFAULT_DOCUMENT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_DOCUMENT_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_VERIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_VERIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_VERIFIED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VERIFIED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final String DEFAULT_DOCUMENT_RELATED_TO = "AAAAAAAAAA";
    private static final String UPDATED_DOCUMENT_RELATED_TO = "BBBBBBBBBB";

    @Autowired
    private OrganisationDocumentRepository organisationDocumentRepository;

    @Autowired
    private OrganisationDocumentMapper organisationDocumentMapper;

    @Autowired
    private OrganisationDocumentService organisationDocumentService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.OrganisationDocumentSearchRepositoryMockConfiguration
     */
    @Autowired
    private OrganisationDocumentSearchRepository mockOrganisationDocumentSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOrganisationDocumentMockMvc;

    private OrganisationDocument organisationDocument;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OrganisationDocumentResource organisationDocumentResource = new OrganisationDocumentResource(organisationDocumentService);
        this.restOrganisationDocumentMockMvc = MockMvcBuilders.standaloneSetup(organisationDocumentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrganisationDocument createEntity(EntityManager em) {
        OrganisationDocument organisationDocument = new OrganisationDocument()
            .documentTitle(DEFAULT_DOCUMENT_TITLE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdOn(DEFAULT_CREATED_ON)
            .verifiedBy(DEFAULT_VERIFIED_BY)
            .verifiedOn(DEFAULT_VERIFIED_ON)
            .remarks(DEFAULT_REMARKS)
            .documentRelatedTo(DEFAULT_DOCUMENT_RELATED_TO);
        return organisationDocument;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrganisationDocument createUpdatedEntity(EntityManager em) {
        OrganisationDocument organisationDocument = new OrganisationDocument()
            .documentTitle(UPDATED_DOCUMENT_TITLE)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS)
            .documentRelatedTo(UPDATED_DOCUMENT_RELATED_TO);
        return organisationDocument;
    }

    @BeforeEach
    public void initTest() {
        organisationDocument = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrganisationDocument() throws Exception {
        int databaseSizeBeforeCreate = organisationDocumentRepository.findAll().size();

        // Create the OrganisationDocument
        OrganisationDocumentDTO organisationDocumentDTO = organisationDocumentMapper.toDto(organisationDocument);
        restOrganisationDocumentMockMvc.perform(post("/api/organisation-documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationDocumentDTO)))
            .andExpect(status().isCreated());

        // Validate the OrganisationDocument in the database
        List<OrganisationDocument> organisationDocumentList = organisationDocumentRepository.findAll();
        assertThat(organisationDocumentList).hasSize(databaseSizeBeforeCreate + 1);
        OrganisationDocument testOrganisationDocument = organisationDocumentList.get(organisationDocumentList.size() - 1);
        assertThat(testOrganisationDocument.getDocumentTitle()).isEqualTo(DEFAULT_DOCUMENT_TITLE);
        assertThat(testOrganisationDocument.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testOrganisationDocument.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testOrganisationDocument.getVerifiedBy()).isEqualTo(DEFAULT_VERIFIED_BY);
        assertThat(testOrganisationDocument.getVerifiedOn()).isEqualTo(DEFAULT_VERIFIED_ON);
        assertThat(testOrganisationDocument.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testOrganisationDocument.getDocumentRelatedTo()).isEqualTo(DEFAULT_DOCUMENT_RELATED_TO);

        // Validate the OrganisationDocument in Elasticsearch
        verify(mockOrganisationDocumentSearchRepository, times(1)).save(testOrganisationDocument);
    }

    @Test
    @Transactional
    public void createOrganisationDocumentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = organisationDocumentRepository.findAll().size();

        // Create the OrganisationDocument with an existing ID
        organisationDocument.setId(1L);
        OrganisationDocumentDTO organisationDocumentDTO = organisationDocumentMapper.toDto(organisationDocument);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrganisationDocumentMockMvc.perform(post("/api/organisation-documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationDocumentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrganisationDocument in the database
        List<OrganisationDocument> organisationDocumentList = organisationDocumentRepository.findAll();
        assertThat(organisationDocumentList).hasSize(databaseSizeBeforeCreate);

        // Validate the OrganisationDocument in Elasticsearch
        verify(mockOrganisationDocumentSearchRepository, times(0)).save(organisationDocument);
    }


    @Test
    @Transactional
    public void checkDocumentTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = organisationDocumentRepository.findAll().size();
        // set the field null
        organisationDocument.setDocumentTitle(null);

        // Create the OrganisationDocument, which fails.
        OrganisationDocumentDTO organisationDocumentDTO = organisationDocumentMapper.toDto(organisationDocument);

        restOrganisationDocumentMockMvc.perform(post("/api/organisation-documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationDocumentDTO)))
            .andExpect(status().isBadRequest());

        List<OrganisationDocument> organisationDocumentList = organisationDocumentRepository.findAll();
        assertThat(organisationDocumentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOrganisationDocuments() throws Exception {
        // Initialize the database
        organisationDocumentRepository.saveAndFlush(organisationDocument);

        // Get all the organisationDocumentList
        restOrganisationDocumentMockMvc.perform(get("/api/organisation-documents?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(organisationDocument.getId().intValue())))
            .andExpect(jsonPath("$.[*].documentTitle").value(hasItem(DEFAULT_DOCUMENT_TITLE)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].documentRelatedTo").value(hasItem(DEFAULT_DOCUMENT_RELATED_TO)));
    }
    
    @Test
    @Transactional
    public void getOrganisationDocument() throws Exception {
        // Initialize the database
        organisationDocumentRepository.saveAndFlush(organisationDocument);

        // Get the organisationDocument
        restOrganisationDocumentMockMvc.perform(get("/api/organisation-documents/{id}", organisationDocument.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(organisationDocument.getId().intValue()))
            .andExpect(jsonPath("$.documentTitle").value(DEFAULT_DOCUMENT_TITLE))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.verifiedBy").value(DEFAULT_VERIFIED_BY))
            .andExpect(jsonPath("$.verifiedOn").value(DEFAULT_VERIFIED_ON.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS))
            .andExpect(jsonPath("$.documentRelatedTo").value(DEFAULT_DOCUMENT_RELATED_TO));
    }

    @Test
    @Transactional
    public void getNonExistingOrganisationDocument() throws Exception {
        // Get the organisationDocument
        restOrganisationDocumentMockMvc.perform(get("/api/organisation-documents/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrganisationDocument() throws Exception {
        // Initialize the database
        organisationDocumentRepository.saveAndFlush(organisationDocument);

        int databaseSizeBeforeUpdate = organisationDocumentRepository.findAll().size();

        // Update the organisationDocument
        OrganisationDocument updatedOrganisationDocument = organisationDocumentRepository.findById(organisationDocument.getId()).get();
        // Disconnect from session so that the updates on updatedOrganisationDocument are not directly saved in db
        em.detach(updatedOrganisationDocument);
        updatedOrganisationDocument
            .documentTitle(UPDATED_DOCUMENT_TITLE)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS)
            .documentRelatedTo(UPDATED_DOCUMENT_RELATED_TO);
        OrganisationDocumentDTO organisationDocumentDTO = organisationDocumentMapper.toDto(updatedOrganisationDocument);

        restOrganisationDocumentMockMvc.perform(put("/api/organisation-documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationDocumentDTO)))
            .andExpect(status().isOk());

        // Validate the OrganisationDocument in the database
        List<OrganisationDocument> organisationDocumentList = organisationDocumentRepository.findAll();
        assertThat(organisationDocumentList).hasSize(databaseSizeBeforeUpdate);
        OrganisationDocument testOrganisationDocument = organisationDocumentList.get(organisationDocumentList.size() - 1);
        assertThat(testOrganisationDocument.getDocumentTitle()).isEqualTo(UPDATED_DOCUMENT_TITLE);
        assertThat(testOrganisationDocument.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testOrganisationDocument.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testOrganisationDocument.getVerifiedBy()).isEqualTo(UPDATED_VERIFIED_BY);
        assertThat(testOrganisationDocument.getVerifiedOn()).isEqualTo(UPDATED_VERIFIED_ON);
        assertThat(testOrganisationDocument.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testOrganisationDocument.getDocumentRelatedTo()).isEqualTo(UPDATED_DOCUMENT_RELATED_TO);

        // Validate the OrganisationDocument in Elasticsearch
        verify(mockOrganisationDocumentSearchRepository, times(1)).save(testOrganisationDocument);
    }

    @Test
    @Transactional
    public void updateNonExistingOrganisationDocument() throws Exception {
        int databaseSizeBeforeUpdate = organisationDocumentRepository.findAll().size();

        // Create the OrganisationDocument
        OrganisationDocumentDTO organisationDocumentDTO = organisationDocumentMapper.toDto(organisationDocument);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrganisationDocumentMockMvc.perform(put("/api/organisation-documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationDocumentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrganisationDocument in the database
        List<OrganisationDocument> organisationDocumentList = organisationDocumentRepository.findAll();
        assertThat(organisationDocumentList).hasSize(databaseSizeBeforeUpdate);

        // Validate the OrganisationDocument in Elasticsearch
        verify(mockOrganisationDocumentSearchRepository, times(0)).save(organisationDocument);
    }

    @Test
    @Transactional
    public void deleteOrganisationDocument() throws Exception {
        // Initialize the database
        organisationDocumentRepository.saveAndFlush(organisationDocument);

        int databaseSizeBeforeDelete = organisationDocumentRepository.findAll().size();

        // Delete the organisationDocument
        restOrganisationDocumentMockMvc.perform(delete("/api/organisation-documents/{id}", organisationDocument.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrganisationDocument> organisationDocumentList = organisationDocumentRepository.findAll();
        assertThat(organisationDocumentList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the OrganisationDocument in Elasticsearch
        verify(mockOrganisationDocumentSearchRepository, times(1)).deleteById(organisationDocument.getId());
    }

    @Test
    @Transactional
    public void searchOrganisationDocument() throws Exception {
        // Initialize the database
        organisationDocumentRepository.saveAndFlush(organisationDocument);
        when(mockOrganisationDocumentSearchRepository.search(queryStringQuery("id:" + organisationDocument.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(organisationDocument), PageRequest.of(0, 1), 1));
        // Search the organisationDocument
        restOrganisationDocumentMockMvc.perform(get("/api/_search/organisation-documents?query=id:" + organisationDocument.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(organisationDocument.getId().intValue())))
            .andExpect(jsonPath("$.[*].documentTitle").value(hasItem(DEFAULT_DOCUMENT_TITLE)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].documentRelatedTo").value(hasItem(DEFAULT_DOCUMENT_RELATED_TO)));
    }
}
