package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.SubDistrictMaster;
import com.hartron.esignharyana.repository.SubDistrictMasterRepository;
import com.hartron.esignharyana.repository.search.SubDistrictMasterSearchRepository;
import com.hartron.esignharyana.service.SubDistrictMasterService;
import com.hartron.esignharyana.service.dto.SubDistrictMasterDTO;
import com.hartron.esignharyana.service.mapper.SubDistrictMasterMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SubDistrictMasterResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class SubDistrictMasterResourceIT {

    private static final String DEFAULT_SUB_DISTRICT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_SUB_DISTRICT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_SUB_DISTRICT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SUB_DISTRICT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_VERIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_VERIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_VERIFIED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VERIFIED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    @Autowired
    private SubDistrictMasterRepository subDistrictMasterRepository;

    @Autowired
    private SubDistrictMasterMapper subDistrictMasterMapper;

    @Autowired
    private SubDistrictMasterService subDistrictMasterService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.SubDistrictMasterSearchRepositoryMockConfiguration
     */
    @Autowired
    private SubDistrictMasterSearchRepository mockSubDistrictMasterSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSubDistrictMasterMockMvc;

    private SubDistrictMaster subDistrictMaster;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SubDistrictMasterResource subDistrictMasterResource = new SubDistrictMasterResource(subDistrictMasterService);
        this.restSubDistrictMasterMockMvc = MockMvcBuilders.standaloneSetup(subDistrictMasterResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubDistrictMaster createEntity(EntityManager em) {
        SubDistrictMaster subDistrictMaster = new SubDistrictMaster()
            .subDistrictCode(DEFAULT_SUB_DISTRICT_CODE)
            .subDistrictName(DEFAULT_SUB_DISTRICT_NAME)
            .lastUpdatedBy(DEFAULT_LAST_UPDATED_BY)
            .lastUpdatedOn(DEFAULT_LAST_UPDATED_ON)
            .verifiedBy(DEFAULT_VERIFIED_BY)
            .verifiedOn(DEFAULT_VERIFIED_ON)
            .remarks(DEFAULT_REMARKS);
        return subDistrictMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubDistrictMaster createUpdatedEntity(EntityManager em) {
        SubDistrictMaster subDistrictMaster = new SubDistrictMaster()
            .subDistrictCode(UPDATED_SUB_DISTRICT_CODE)
            .subDistrictName(UPDATED_SUB_DISTRICT_NAME)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS);
        return subDistrictMaster;
    }

    @BeforeEach
    public void initTest() {
        subDistrictMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createSubDistrictMaster() throws Exception {
        int databaseSizeBeforeCreate = subDistrictMasterRepository.findAll().size();

        // Create the SubDistrictMaster
        SubDistrictMasterDTO subDistrictMasterDTO = subDistrictMasterMapper.toDto(subDistrictMaster);
        restSubDistrictMasterMockMvc.perform(post("/api/sub-district-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDistrictMasterDTO)))
            .andExpect(status().isCreated());

        // Validate the SubDistrictMaster in the database
        List<SubDistrictMaster> subDistrictMasterList = subDistrictMasterRepository.findAll();
        assertThat(subDistrictMasterList).hasSize(databaseSizeBeforeCreate + 1);
        SubDistrictMaster testSubDistrictMaster = subDistrictMasterList.get(subDistrictMasterList.size() - 1);
        assertThat(testSubDistrictMaster.getSubDistrictCode()).isEqualTo(DEFAULT_SUB_DISTRICT_CODE);
        assertThat(testSubDistrictMaster.getSubDistrictName()).isEqualTo(DEFAULT_SUB_DISTRICT_NAME);
        assertThat(testSubDistrictMaster.getLastUpdatedBy()).isEqualTo(DEFAULT_LAST_UPDATED_BY);
        assertThat(testSubDistrictMaster.getLastUpdatedOn()).isEqualTo(DEFAULT_LAST_UPDATED_ON);
        assertThat(testSubDistrictMaster.getVerifiedBy()).isEqualTo(DEFAULT_VERIFIED_BY);
        assertThat(testSubDistrictMaster.getVerifiedOn()).isEqualTo(DEFAULT_VERIFIED_ON);
        assertThat(testSubDistrictMaster.getRemarks()).isEqualTo(DEFAULT_REMARKS);

        // Validate the SubDistrictMaster in Elasticsearch
        verify(mockSubDistrictMasterSearchRepository, times(1)).save(testSubDistrictMaster);
    }

    @Test
    @Transactional
    public void createSubDistrictMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = subDistrictMasterRepository.findAll().size();

        // Create the SubDistrictMaster with an existing ID
        subDistrictMaster.setId(1L);
        SubDistrictMasterDTO subDistrictMasterDTO = subDistrictMasterMapper.toDto(subDistrictMaster);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubDistrictMasterMockMvc.perform(post("/api/sub-district-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDistrictMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubDistrictMaster in the database
        List<SubDistrictMaster> subDistrictMasterList = subDistrictMasterRepository.findAll();
        assertThat(subDistrictMasterList).hasSize(databaseSizeBeforeCreate);

        // Validate the SubDistrictMaster in Elasticsearch
        verify(mockSubDistrictMasterSearchRepository, times(0)).save(subDistrictMaster);
    }


    @Test
    @Transactional
    public void checkSubDistrictNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = subDistrictMasterRepository.findAll().size();
        // set the field null
        subDistrictMaster.setSubDistrictName(null);

        // Create the SubDistrictMaster, which fails.
        SubDistrictMasterDTO subDistrictMasterDTO = subDistrictMasterMapper.toDto(subDistrictMaster);

        restSubDistrictMasterMockMvc.perform(post("/api/sub-district-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDistrictMasterDTO)))
            .andExpect(status().isBadRequest());

        List<SubDistrictMaster> subDistrictMasterList = subDistrictMasterRepository.findAll();
        assertThat(subDistrictMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSubDistrictMasters() throws Exception {
        // Initialize the database
        subDistrictMasterRepository.saveAndFlush(subDistrictMaster);

        // Get all the subDistrictMasterList
        restSubDistrictMasterMockMvc.perform(get("/api/sub-district-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subDistrictMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].subDistrictCode").value(hasItem(DEFAULT_SUB_DISTRICT_CODE)))
            .andExpect(jsonPath("$.[*].subDistrictName").value(hasItem(DEFAULT_SUB_DISTRICT_NAME)))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
    
    @Test
    @Transactional
    public void getSubDistrictMaster() throws Exception {
        // Initialize the database
        subDistrictMasterRepository.saveAndFlush(subDistrictMaster);

        // Get the subDistrictMaster
        restSubDistrictMasterMockMvc.perform(get("/api/sub-district-masters/{id}", subDistrictMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(subDistrictMaster.getId().intValue()))
            .andExpect(jsonPath("$.subDistrictCode").value(DEFAULT_SUB_DISTRICT_CODE))
            .andExpect(jsonPath("$.subDistrictName").value(DEFAULT_SUB_DISTRICT_NAME))
            .andExpect(jsonPath("$.lastUpdatedBy").value(DEFAULT_LAST_UPDATED_BY))
            .andExpect(jsonPath("$.lastUpdatedOn").value(DEFAULT_LAST_UPDATED_ON.toString()))
            .andExpect(jsonPath("$.verifiedBy").value(DEFAULT_VERIFIED_BY))
            .andExpect(jsonPath("$.verifiedOn").value(DEFAULT_VERIFIED_ON.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS));
    }

    @Test
    @Transactional
    public void getNonExistingSubDistrictMaster() throws Exception {
        // Get the subDistrictMaster
        restSubDistrictMasterMockMvc.perform(get("/api/sub-district-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSubDistrictMaster() throws Exception {
        // Initialize the database
        subDistrictMasterRepository.saveAndFlush(subDistrictMaster);

        int databaseSizeBeforeUpdate = subDistrictMasterRepository.findAll().size();

        // Update the subDistrictMaster
        SubDistrictMaster updatedSubDistrictMaster = subDistrictMasterRepository.findById(subDistrictMaster.getId()).get();
        // Disconnect from session so that the updates on updatedSubDistrictMaster are not directly saved in db
        em.detach(updatedSubDistrictMaster);
        updatedSubDistrictMaster
            .subDistrictCode(UPDATED_SUB_DISTRICT_CODE)
            .subDistrictName(UPDATED_SUB_DISTRICT_NAME)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS);
        SubDistrictMasterDTO subDistrictMasterDTO = subDistrictMasterMapper.toDto(updatedSubDistrictMaster);

        restSubDistrictMasterMockMvc.perform(put("/api/sub-district-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDistrictMasterDTO)))
            .andExpect(status().isOk());

        // Validate the SubDistrictMaster in the database
        List<SubDistrictMaster> subDistrictMasterList = subDistrictMasterRepository.findAll();
        assertThat(subDistrictMasterList).hasSize(databaseSizeBeforeUpdate);
        SubDistrictMaster testSubDistrictMaster = subDistrictMasterList.get(subDistrictMasterList.size() - 1);
        assertThat(testSubDistrictMaster.getSubDistrictCode()).isEqualTo(UPDATED_SUB_DISTRICT_CODE);
        assertThat(testSubDistrictMaster.getSubDistrictName()).isEqualTo(UPDATED_SUB_DISTRICT_NAME);
        assertThat(testSubDistrictMaster.getLastUpdatedBy()).isEqualTo(UPDATED_LAST_UPDATED_BY);
        assertThat(testSubDistrictMaster.getLastUpdatedOn()).isEqualTo(UPDATED_LAST_UPDATED_ON);
        assertThat(testSubDistrictMaster.getVerifiedBy()).isEqualTo(UPDATED_VERIFIED_BY);
        assertThat(testSubDistrictMaster.getVerifiedOn()).isEqualTo(UPDATED_VERIFIED_ON);
        assertThat(testSubDistrictMaster.getRemarks()).isEqualTo(UPDATED_REMARKS);

        // Validate the SubDistrictMaster in Elasticsearch
        verify(mockSubDistrictMasterSearchRepository, times(1)).save(testSubDistrictMaster);
    }

    @Test
    @Transactional
    public void updateNonExistingSubDistrictMaster() throws Exception {
        int databaseSizeBeforeUpdate = subDistrictMasterRepository.findAll().size();

        // Create the SubDistrictMaster
        SubDistrictMasterDTO subDistrictMasterDTO = subDistrictMasterMapper.toDto(subDistrictMaster);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSubDistrictMasterMockMvc.perform(put("/api/sub-district-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDistrictMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubDistrictMaster in the database
        List<SubDistrictMaster> subDistrictMasterList = subDistrictMasterRepository.findAll();
        assertThat(subDistrictMasterList).hasSize(databaseSizeBeforeUpdate);

        // Validate the SubDistrictMaster in Elasticsearch
        verify(mockSubDistrictMasterSearchRepository, times(0)).save(subDistrictMaster);
    }

    @Test
    @Transactional
    public void deleteSubDistrictMaster() throws Exception {
        // Initialize the database
        subDistrictMasterRepository.saveAndFlush(subDistrictMaster);

        int databaseSizeBeforeDelete = subDistrictMasterRepository.findAll().size();

        // Delete the subDistrictMaster
        restSubDistrictMasterMockMvc.perform(delete("/api/sub-district-masters/{id}", subDistrictMaster.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SubDistrictMaster> subDistrictMasterList = subDistrictMasterRepository.findAll();
        assertThat(subDistrictMasterList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the SubDistrictMaster in Elasticsearch
        verify(mockSubDistrictMasterSearchRepository, times(1)).deleteById(subDistrictMaster.getId());
    }

    @Test
    @Transactional
    public void searchSubDistrictMaster() throws Exception {
        // Initialize the database
        subDistrictMasterRepository.saveAndFlush(subDistrictMaster);
        when(mockSubDistrictMasterSearchRepository.search(queryStringQuery("id:" + subDistrictMaster.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(subDistrictMaster), PageRequest.of(0, 1), 1));
        // Search the subDistrictMaster
        restSubDistrictMasterMockMvc.perform(get("/api/_search/sub-district-masters?query=id:" + subDistrictMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subDistrictMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].subDistrictCode").value(hasItem(DEFAULT_SUB_DISTRICT_CODE)))
            .andExpect(jsonPath("$.[*].subDistrictName").value(hasItem(DEFAULT_SUB_DISTRICT_NAME)))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
}
