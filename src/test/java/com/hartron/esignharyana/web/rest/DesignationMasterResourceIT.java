package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.DesignationMaster;
import com.hartron.esignharyana.repository.DesignationMasterRepository;
import com.hartron.esignharyana.repository.search.DesignationMasterSearchRepository;
import com.hartron.esignharyana.service.DesignationMasterService;
import com.hartron.esignharyana.service.dto.DesignationMasterDTO;
import com.hartron.esignharyana.service.mapper.DesignationMasterMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DesignationMasterResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class DesignationMasterResourceIT {

    private static final String DEFAULT_DESIGNATION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DESIGNATION_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESIGNATION_ABBREVIATION = "AAAAAAAAAA";
    private static final String UPDATED_DESIGNATION_ABBREVIATION = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LAST_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_VERIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_VERIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_VERIFIED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VERIFIED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    @Autowired
    private DesignationMasterRepository designationMasterRepository;

    @Autowired
    private DesignationMasterMapper designationMasterMapper;

    @Autowired
    private DesignationMasterService designationMasterService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.DesignationMasterSearchRepositoryMockConfiguration
     */
    @Autowired
    private DesignationMasterSearchRepository mockDesignationMasterSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDesignationMasterMockMvc;

    private DesignationMaster designationMaster;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DesignationMasterResource designationMasterResource = new DesignationMasterResource(designationMasterService);
        this.restDesignationMasterMockMvc = MockMvcBuilders.standaloneSetup(designationMasterResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DesignationMaster createEntity(EntityManager em) {
        DesignationMaster designationMaster = new DesignationMaster()
            .designationName(DEFAULT_DESIGNATION_NAME)
            .designationAbbreviation(DEFAULT_DESIGNATION_ABBREVIATION)
            .createdBy(DEFAULT_CREATED_BY)
            .createdOn(DEFAULT_CREATED_ON)
            .lastUpdatedBy(DEFAULT_LAST_UPDATED_BY)
            .lastUpdatedOn(DEFAULT_LAST_UPDATED_ON)
            .verifiedBy(DEFAULT_VERIFIED_BY)
            .verifiedOn(DEFAULT_VERIFIED_ON)
            .remarks(DEFAULT_REMARKS);
        return designationMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DesignationMaster createUpdatedEntity(EntityManager em) {
        DesignationMaster designationMaster = new DesignationMaster()
            .designationName(UPDATED_DESIGNATION_NAME)
            .designationAbbreviation(UPDATED_DESIGNATION_ABBREVIATION)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS);
        return designationMaster;
    }

    @BeforeEach
    public void initTest() {
        designationMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createDesignationMaster() throws Exception {
        int databaseSizeBeforeCreate = designationMasterRepository.findAll().size();

        // Create the DesignationMaster
        DesignationMasterDTO designationMasterDTO = designationMasterMapper.toDto(designationMaster);
        restDesignationMasterMockMvc.perform(post("/api/designation-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(designationMasterDTO)))
            .andExpect(status().isCreated());

        // Validate the DesignationMaster in the database
        List<DesignationMaster> designationMasterList = designationMasterRepository.findAll();
        assertThat(designationMasterList).hasSize(databaseSizeBeforeCreate + 1);
        DesignationMaster testDesignationMaster = designationMasterList.get(designationMasterList.size() - 1);
        assertThat(testDesignationMaster.getDesignationName()).isEqualTo(DEFAULT_DESIGNATION_NAME);
        assertThat(testDesignationMaster.getDesignationAbbreviation()).isEqualTo(DEFAULT_DESIGNATION_ABBREVIATION);
        assertThat(testDesignationMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testDesignationMaster.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testDesignationMaster.getLastUpdatedBy()).isEqualTo(DEFAULT_LAST_UPDATED_BY);
        assertThat(testDesignationMaster.getLastUpdatedOn()).isEqualTo(DEFAULT_LAST_UPDATED_ON);
        assertThat(testDesignationMaster.getVerifiedBy()).isEqualTo(DEFAULT_VERIFIED_BY);
        assertThat(testDesignationMaster.getVerifiedOn()).isEqualTo(DEFAULT_VERIFIED_ON);
        assertThat(testDesignationMaster.getRemarks()).isEqualTo(DEFAULT_REMARKS);

        // Validate the DesignationMaster in Elasticsearch
        verify(mockDesignationMasterSearchRepository, times(1)).save(testDesignationMaster);
    }

    @Test
    @Transactional
    public void createDesignationMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = designationMasterRepository.findAll().size();

        // Create the DesignationMaster with an existing ID
        designationMaster.setId(1L);
        DesignationMasterDTO designationMasterDTO = designationMasterMapper.toDto(designationMaster);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDesignationMasterMockMvc.perform(post("/api/designation-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(designationMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DesignationMaster in the database
        List<DesignationMaster> designationMasterList = designationMasterRepository.findAll();
        assertThat(designationMasterList).hasSize(databaseSizeBeforeCreate);

        // Validate the DesignationMaster in Elasticsearch
        verify(mockDesignationMasterSearchRepository, times(0)).save(designationMaster);
    }


    @Test
    @Transactional
    public void getAllDesignationMasters() throws Exception {
        // Initialize the database
        designationMasterRepository.saveAndFlush(designationMaster);

        // Get all the designationMasterList
        restDesignationMasterMockMvc.perform(get("/api/designation-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(designationMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].designationName").value(hasItem(DEFAULT_DESIGNATION_NAME)))
            .andExpect(jsonPath("$.[*].designationAbbreviation").value(hasItem(DEFAULT_DESIGNATION_ABBREVIATION)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
    
    @Test
    @Transactional
    public void getDesignationMaster() throws Exception {
        // Initialize the database
        designationMasterRepository.saveAndFlush(designationMaster);

        // Get the designationMaster
        restDesignationMasterMockMvc.perform(get("/api/designation-masters/{id}", designationMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(designationMaster.getId().intValue()))
            .andExpect(jsonPath("$.designationName").value(DEFAULT_DESIGNATION_NAME))
            .andExpect(jsonPath("$.designationAbbreviation").value(DEFAULT_DESIGNATION_ABBREVIATION))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.lastUpdatedBy").value(DEFAULT_LAST_UPDATED_BY))
            .andExpect(jsonPath("$.lastUpdatedOn").value(DEFAULT_LAST_UPDATED_ON.toString()))
            .andExpect(jsonPath("$.verifiedBy").value(DEFAULT_VERIFIED_BY))
            .andExpect(jsonPath("$.verifiedOn").value(DEFAULT_VERIFIED_ON.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS));
    }

    @Test
    @Transactional
    public void getNonExistingDesignationMaster() throws Exception {
        // Get the designationMaster
        restDesignationMasterMockMvc.perform(get("/api/designation-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDesignationMaster() throws Exception {
        // Initialize the database
        designationMasterRepository.saveAndFlush(designationMaster);

        int databaseSizeBeforeUpdate = designationMasterRepository.findAll().size();

        // Update the designationMaster
        DesignationMaster updatedDesignationMaster = designationMasterRepository.findById(designationMaster.getId()).get();
        // Disconnect from session so that the updates on updatedDesignationMaster are not directly saved in db
        em.detach(updatedDesignationMaster);
        updatedDesignationMaster
            .designationName(UPDATED_DESIGNATION_NAME)
            .designationAbbreviation(UPDATED_DESIGNATION_ABBREVIATION)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS);
        DesignationMasterDTO designationMasterDTO = designationMasterMapper.toDto(updatedDesignationMaster);

        restDesignationMasterMockMvc.perform(put("/api/designation-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(designationMasterDTO)))
            .andExpect(status().isOk());

        // Validate the DesignationMaster in the database
        List<DesignationMaster> designationMasterList = designationMasterRepository.findAll();
        assertThat(designationMasterList).hasSize(databaseSizeBeforeUpdate);
        DesignationMaster testDesignationMaster = designationMasterList.get(designationMasterList.size() - 1);
        assertThat(testDesignationMaster.getDesignationName()).isEqualTo(UPDATED_DESIGNATION_NAME);
        assertThat(testDesignationMaster.getDesignationAbbreviation()).isEqualTo(UPDATED_DESIGNATION_ABBREVIATION);
        assertThat(testDesignationMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testDesignationMaster.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testDesignationMaster.getLastUpdatedBy()).isEqualTo(UPDATED_LAST_UPDATED_BY);
        assertThat(testDesignationMaster.getLastUpdatedOn()).isEqualTo(UPDATED_LAST_UPDATED_ON);
        assertThat(testDesignationMaster.getVerifiedBy()).isEqualTo(UPDATED_VERIFIED_BY);
        assertThat(testDesignationMaster.getVerifiedOn()).isEqualTo(UPDATED_VERIFIED_ON);
        assertThat(testDesignationMaster.getRemarks()).isEqualTo(UPDATED_REMARKS);

        // Validate the DesignationMaster in Elasticsearch
        verify(mockDesignationMasterSearchRepository, times(1)).save(testDesignationMaster);
    }

    @Test
    @Transactional
    public void updateNonExistingDesignationMaster() throws Exception {
        int databaseSizeBeforeUpdate = designationMasterRepository.findAll().size();

        // Create the DesignationMaster
        DesignationMasterDTO designationMasterDTO = designationMasterMapper.toDto(designationMaster);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDesignationMasterMockMvc.perform(put("/api/designation-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(designationMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DesignationMaster in the database
        List<DesignationMaster> designationMasterList = designationMasterRepository.findAll();
        assertThat(designationMasterList).hasSize(databaseSizeBeforeUpdate);

        // Validate the DesignationMaster in Elasticsearch
        verify(mockDesignationMasterSearchRepository, times(0)).save(designationMaster);
    }

    @Test
    @Transactional
    public void deleteDesignationMaster() throws Exception {
        // Initialize the database
        designationMasterRepository.saveAndFlush(designationMaster);

        int databaseSizeBeforeDelete = designationMasterRepository.findAll().size();

        // Delete the designationMaster
        restDesignationMasterMockMvc.perform(delete("/api/designation-masters/{id}", designationMaster.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DesignationMaster> designationMasterList = designationMasterRepository.findAll();
        assertThat(designationMasterList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the DesignationMaster in Elasticsearch
        verify(mockDesignationMasterSearchRepository, times(1)).deleteById(designationMaster.getId());
    }

    @Test
    @Transactional
    public void searchDesignationMaster() throws Exception {
        // Initialize the database
        designationMasterRepository.saveAndFlush(designationMaster);
        when(mockDesignationMasterSearchRepository.search(queryStringQuery("id:" + designationMaster.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(designationMaster), PageRequest.of(0, 1), 1));
        // Search the designationMaster
        restDesignationMasterMockMvc.perform(get("/api/_search/designation-masters?query=id:" + designationMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(designationMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].designationName").value(hasItem(DEFAULT_DESIGNATION_NAME)))
            .andExpect(jsonPath("$.[*].designationAbbreviation").value(hasItem(DEFAULT_DESIGNATION_ABBREVIATION)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
}
