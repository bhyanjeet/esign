package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.EsignRole;
import com.hartron.esignharyana.repository.EsignRoleRepository;
import com.hartron.esignharyana.repository.search.EsignRoleSearchRepository;
import com.hartron.esignharyana.service.EsignRoleService;
import com.hartron.esignharyana.service.dto.EsignRoleDTO;
import com.hartron.esignharyana.service.mapper.EsignRoleMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EsignRoleResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class EsignRoleResourceIT {

    private static final String DEFAULT_E_SIGN_ROLE_DETAIL = "AAAAAAAAAA";
    private static final String UPDATED_E_SIGN_ROLE_DETAIL = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LAST_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_VERIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_VERIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_VERIFIED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VERIFIED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    @Autowired
    private EsignRoleRepository esignRoleRepository;

    @Autowired
    private EsignRoleMapper esignRoleMapper;

    @Autowired
    private EsignRoleService esignRoleService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.EsignRoleSearchRepositoryMockConfiguration
     */
    @Autowired
    private EsignRoleSearchRepository mockEsignRoleSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEsignRoleMockMvc;

    private EsignRole esignRole;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EsignRoleResource esignRoleResource = new EsignRoleResource(esignRoleService);
        this.restEsignRoleMockMvc = MockMvcBuilders.standaloneSetup(esignRoleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EsignRole createEntity(EntityManager em) {
        EsignRole esignRole = new EsignRole()
            .eSignRoleDetail(DEFAULT_E_SIGN_ROLE_DETAIL)
            .createdBy(DEFAULT_CREATED_BY)
            .createdOn(DEFAULT_CREATED_ON)
            .lastUpdatedBy(DEFAULT_LAST_UPDATED_BY)
            .lastUpdatedOn(DEFAULT_LAST_UPDATED_ON)
            .verifiedBy(DEFAULT_VERIFIED_BY)
            .verifiedOn(DEFAULT_VERIFIED_ON)
            .remarks(DEFAULT_REMARKS);
        return esignRole;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EsignRole createUpdatedEntity(EntityManager em) {
        EsignRole esignRole = new EsignRole()
            .eSignRoleDetail(UPDATED_E_SIGN_ROLE_DETAIL)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS);
        return esignRole;
    }

    @BeforeEach
    public void initTest() {
        esignRole = createEntity(em);
    }

    @Test
    @Transactional
    public void createEsignRole() throws Exception {
        int databaseSizeBeforeCreate = esignRoleRepository.findAll().size();

        // Create the EsignRole
        EsignRoleDTO esignRoleDTO = esignRoleMapper.toDto(esignRole);
        restEsignRoleMockMvc.perform(post("/api/esign-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(esignRoleDTO)))
            .andExpect(status().isCreated());

        // Validate the EsignRole in the database
        List<EsignRole> esignRoleList = esignRoleRepository.findAll();
        assertThat(esignRoleList).hasSize(databaseSizeBeforeCreate + 1);
        EsignRole testEsignRole = esignRoleList.get(esignRoleList.size() - 1);
        assertThat(testEsignRole.geteSignRoleDetail()).isEqualTo(DEFAULT_E_SIGN_ROLE_DETAIL);
        assertThat(testEsignRole.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testEsignRole.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testEsignRole.getLastUpdatedBy()).isEqualTo(DEFAULT_LAST_UPDATED_BY);
        assertThat(testEsignRole.getLastUpdatedOn()).isEqualTo(DEFAULT_LAST_UPDATED_ON);
        assertThat(testEsignRole.getVerifiedBy()).isEqualTo(DEFAULT_VERIFIED_BY);
        assertThat(testEsignRole.getVerifiedOn()).isEqualTo(DEFAULT_VERIFIED_ON);
        assertThat(testEsignRole.getRemarks()).isEqualTo(DEFAULT_REMARKS);

        // Validate the EsignRole in Elasticsearch
        verify(mockEsignRoleSearchRepository, times(1)).save(testEsignRole);
    }

    @Test
    @Transactional
    public void createEsignRoleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = esignRoleRepository.findAll().size();

        // Create the EsignRole with an existing ID
        esignRole.setId(1L);
        EsignRoleDTO esignRoleDTO = esignRoleMapper.toDto(esignRole);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEsignRoleMockMvc.perform(post("/api/esign-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(esignRoleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EsignRole in the database
        List<EsignRole> esignRoleList = esignRoleRepository.findAll();
        assertThat(esignRoleList).hasSize(databaseSizeBeforeCreate);

        // Validate the EsignRole in Elasticsearch
        verify(mockEsignRoleSearchRepository, times(0)).save(esignRole);
    }


    @Test
    @Transactional
    public void checkeSignRoleDetailIsRequired() throws Exception {
        int databaseSizeBeforeTest = esignRoleRepository.findAll().size();
        // set the field null
        esignRole.seteSignRoleDetail(null);

        // Create the EsignRole, which fails.
        EsignRoleDTO esignRoleDTO = esignRoleMapper.toDto(esignRole);

        restEsignRoleMockMvc.perform(post("/api/esign-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(esignRoleDTO)))
            .andExpect(status().isBadRequest());

        List<EsignRole> esignRoleList = esignRoleRepository.findAll();
        assertThat(esignRoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEsignRoles() throws Exception {
        // Initialize the database
        esignRoleRepository.saveAndFlush(esignRole);

        // Get all the esignRoleList
        restEsignRoleMockMvc.perform(get("/api/esign-roles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(esignRole.getId().intValue())))
            .andExpect(jsonPath("$.[*].eSignRoleDetail").value(hasItem(DEFAULT_E_SIGN_ROLE_DETAIL)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
    
    @Test
    @Transactional
    public void getEsignRole() throws Exception {
        // Initialize the database
        esignRoleRepository.saveAndFlush(esignRole);

        // Get the esignRole
        restEsignRoleMockMvc.perform(get("/api/esign-roles/{id}", esignRole.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(esignRole.getId().intValue()))
            .andExpect(jsonPath("$.eSignRoleDetail").value(DEFAULT_E_SIGN_ROLE_DETAIL))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.lastUpdatedBy").value(DEFAULT_LAST_UPDATED_BY))
            .andExpect(jsonPath("$.lastUpdatedOn").value(DEFAULT_LAST_UPDATED_ON.toString()))
            .andExpect(jsonPath("$.verifiedBy").value(DEFAULT_VERIFIED_BY))
            .andExpect(jsonPath("$.verifiedOn").value(DEFAULT_VERIFIED_ON.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS));
    }

    @Test
    @Transactional
    public void getNonExistingEsignRole() throws Exception {
        // Get the esignRole
        restEsignRoleMockMvc.perform(get("/api/esign-roles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEsignRole() throws Exception {
        // Initialize the database
        esignRoleRepository.saveAndFlush(esignRole);

        int databaseSizeBeforeUpdate = esignRoleRepository.findAll().size();

        // Update the esignRole
        EsignRole updatedEsignRole = esignRoleRepository.findById(esignRole.getId()).get();
        // Disconnect from session so that the updates on updatedEsignRole are not directly saved in db
        em.detach(updatedEsignRole);
        updatedEsignRole
            .eSignRoleDetail(UPDATED_E_SIGN_ROLE_DETAIL)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS);
        EsignRoleDTO esignRoleDTO = esignRoleMapper.toDto(updatedEsignRole);

        restEsignRoleMockMvc.perform(put("/api/esign-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(esignRoleDTO)))
            .andExpect(status().isOk());

        // Validate the EsignRole in the database
        List<EsignRole> esignRoleList = esignRoleRepository.findAll();
        assertThat(esignRoleList).hasSize(databaseSizeBeforeUpdate);
        EsignRole testEsignRole = esignRoleList.get(esignRoleList.size() - 1);
        assertThat(testEsignRole.geteSignRoleDetail()).isEqualTo(UPDATED_E_SIGN_ROLE_DETAIL);
        assertThat(testEsignRole.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testEsignRole.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testEsignRole.getLastUpdatedBy()).isEqualTo(UPDATED_LAST_UPDATED_BY);
        assertThat(testEsignRole.getLastUpdatedOn()).isEqualTo(UPDATED_LAST_UPDATED_ON);
        assertThat(testEsignRole.getVerifiedBy()).isEqualTo(UPDATED_VERIFIED_BY);
        assertThat(testEsignRole.getVerifiedOn()).isEqualTo(UPDATED_VERIFIED_ON);
        assertThat(testEsignRole.getRemarks()).isEqualTo(UPDATED_REMARKS);

        // Validate the EsignRole in Elasticsearch
        verify(mockEsignRoleSearchRepository, times(1)).save(testEsignRole);
    }

    @Test
    @Transactional
    public void updateNonExistingEsignRole() throws Exception {
        int databaseSizeBeforeUpdate = esignRoleRepository.findAll().size();

        // Create the EsignRole
        EsignRoleDTO esignRoleDTO = esignRoleMapper.toDto(esignRole);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEsignRoleMockMvc.perform(put("/api/esign-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(esignRoleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EsignRole in the database
        List<EsignRole> esignRoleList = esignRoleRepository.findAll();
        assertThat(esignRoleList).hasSize(databaseSizeBeforeUpdate);

        // Validate the EsignRole in Elasticsearch
        verify(mockEsignRoleSearchRepository, times(0)).save(esignRole);
    }

    @Test
    @Transactional
    public void deleteEsignRole() throws Exception {
        // Initialize the database
        esignRoleRepository.saveAndFlush(esignRole);

        int databaseSizeBeforeDelete = esignRoleRepository.findAll().size();

        // Delete the esignRole
        restEsignRoleMockMvc.perform(delete("/api/esign-roles/{id}", esignRole.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EsignRole> esignRoleList = esignRoleRepository.findAll();
        assertThat(esignRoleList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the EsignRole in Elasticsearch
        verify(mockEsignRoleSearchRepository, times(1)).deleteById(esignRole.getId());
    }

    @Test
    @Transactional
    public void searchEsignRole() throws Exception {
        // Initialize the database
        esignRoleRepository.saveAndFlush(esignRole);
        when(mockEsignRoleSearchRepository.search(queryStringQuery("id:" + esignRole.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(esignRole), PageRequest.of(0, 1), 1));
        // Search the esignRole
        restEsignRoleMockMvc.perform(get("/api/_search/esign-roles?query=id:" + esignRole.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(esignRole.getId().intValue())))
            .andExpect(jsonPath("$.[*].eSignRoleDetail").value(hasItem(DEFAULT_E_SIGN_ROLE_DETAIL)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
}
