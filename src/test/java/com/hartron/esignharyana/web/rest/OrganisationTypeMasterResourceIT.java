package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.OrganisationTypeMaster;
import com.hartron.esignharyana.repository.OrganisationTypeMasterRepository;
import com.hartron.esignharyana.repository.search.OrganisationTypeMasterSearchRepository;
import com.hartron.esignharyana.service.OrganisationTypeMasterService;
import com.hartron.esignharyana.service.dto.OrganisationTypeMasterDTO;
import com.hartron.esignharyana.service.mapper.OrganisationTypeMasterMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OrganisationTypeMasterResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class OrganisationTypeMasterResourceIT {

    private static final String DEFAULT_ORGANISATION_TYPE_DETAIL = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_TYPE_DETAIL = "BBBBBBBBBB";

    private static final String DEFAULT_ORGANISATION_TYPE_ABBREVIATION = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_TYPE_ABBREVIATION = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LAST_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_VERIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_VERIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_VERIFIED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VERIFIED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    @Autowired
    private OrganisationTypeMasterRepository organisationTypeMasterRepository;

    @Autowired
    private OrganisationTypeMasterMapper organisationTypeMasterMapper;

    @Autowired
    private OrganisationTypeMasterService organisationTypeMasterService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.OrganisationTypeMasterSearchRepositoryMockConfiguration
     */
    @Autowired
    private OrganisationTypeMasterSearchRepository mockOrganisationTypeMasterSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOrganisationTypeMasterMockMvc;

    private OrganisationTypeMaster organisationTypeMaster;

    @BeforeEach
    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final OrganisationTypeMasterResource organisationTypeMasterResource = new OrganisationTypeMasterResource(organisationTypeMasterService);
//        this.restOrganisationTypeMasterMockMvc = MockMvcBuilders.standaloneSetup(organisationTypeMasterResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrganisationTypeMaster createEntity(EntityManager em) {
        OrganisationTypeMaster organisationTypeMaster = new OrganisationTypeMaster()
            .organisationTypeDetail(DEFAULT_ORGANISATION_TYPE_DETAIL)
            .organisationTypeAbbreviation(DEFAULT_ORGANISATION_TYPE_ABBREVIATION)
            .createdBy(DEFAULT_CREATED_BY)
            .createdOn(DEFAULT_CREATED_ON)
            .lastUpdatedBy(DEFAULT_LAST_UPDATED_BY)
            .lastUpdatedOn(DEFAULT_LAST_UPDATED_ON)
            .verifiedBy(DEFAULT_VERIFIED_BY)
            .verifiedOn(DEFAULT_VERIFIED_ON)
            .remarks(DEFAULT_REMARKS);
        return organisationTypeMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrganisationTypeMaster createUpdatedEntity(EntityManager em) {
        OrganisationTypeMaster organisationTypeMaster = new OrganisationTypeMaster()
            .organisationTypeDetail(UPDATED_ORGANISATION_TYPE_DETAIL)
            .organisationTypeAbbreviation(UPDATED_ORGANISATION_TYPE_ABBREVIATION)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS);
        return organisationTypeMaster;
    }

    @BeforeEach
    public void initTest() {
        organisationTypeMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrganisationTypeMaster() throws Exception {
        int databaseSizeBeforeCreate = organisationTypeMasterRepository.findAll().size();

        // Create the OrganisationTypeMaster
        OrganisationTypeMasterDTO organisationTypeMasterDTO = organisationTypeMasterMapper.toDto(organisationTypeMaster);
        restOrganisationTypeMasterMockMvc.perform(post("/api/organisation-type-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationTypeMasterDTO)))
            .andExpect(status().isCreated());

        // Validate the OrganisationTypeMaster in the database
        List<OrganisationTypeMaster> organisationTypeMasterList = organisationTypeMasterRepository.findAll();
        assertThat(organisationTypeMasterList).hasSize(databaseSizeBeforeCreate + 1);
        OrganisationTypeMaster testOrganisationTypeMaster = organisationTypeMasterList.get(organisationTypeMasterList.size() - 1);
        assertThat(testOrganisationTypeMaster.getOrganisationTypeDetail()).isEqualTo(DEFAULT_ORGANISATION_TYPE_DETAIL);
        assertThat(testOrganisationTypeMaster.getOrganisationTypeAbbreviation()).isEqualTo(DEFAULT_ORGANISATION_TYPE_ABBREVIATION);
        assertThat(testOrganisationTypeMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testOrganisationTypeMaster.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testOrganisationTypeMaster.getLastUpdatedBy()).isEqualTo(DEFAULT_LAST_UPDATED_BY);
        assertThat(testOrganisationTypeMaster.getLastUpdatedOn()).isEqualTo(DEFAULT_LAST_UPDATED_ON);
        assertThat(testOrganisationTypeMaster.getVerifiedBy()).isEqualTo(DEFAULT_VERIFIED_BY);
        assertThat(testOrganisationTypeMaster.getVerifiedOn()).isEqualTo(DEFAULT_VERIFIED_ON);
        assertThat(testOrganisationTypeMaster.getRemarks()).isEqualTo(DEFAULT_REMARKS);

        // Validate the OrganisationTypeMaster in Elasticsearch
        verify(mockOrganisationTypeMasterSearchRepository, times(1)).save(testOrganisationTypeMaster);
    }

    @Test
    @Transactional
    public void createOrganisationTypeMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = organisationTypeMasterRepository.findAll().size();

        // Create the OrganisationTypeMaster with an existing ID
        organisationTypeMaster.setId(1L);
        OrganisationTypeMasterDTO organisationTypeMasterDTO = organisationTypeMasterMapper.toDto(organisationTypeMaster);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrganisationTypeMasterMockMvc.perform(post("/api/organisation-type-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationTypeMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrganisationTypeMaster in the database
        List<OrganisationTypeMaster> organisationTypeMasterList = organisationTypeMasterRepository.findAll();
        assertThat(organisationTypeMasterList).hasSize(databaseSizeBeforeCreate);

        // Validate the OrganisationTypeMaster in Elasticsearch
        verify(mockOrganisationTypeMasterSearchRepository, times(0)).save(organisationTypeMaster);
    }


    @Test
    @Transactional
    public void checkOrganisationTypeDetailIsRequired() throws Exception {
        int databaseSizeBeforeTest = organisationTypeMasterRepository.findAll().size();
        // set the field null
        organisationTypeMaster.setOrganisationTypeDetail(null);

        // Create the OrganisationTypeMaster, which fails.
        OrganisationTypeMasterDTO organisationTypeMasterDTO = organisationTypeMasterMapper.toDto(organisationTypeMaster);

        restOrganisationTypeMasterMockMvc.perform(post("/api/organisation-type-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationTypeMasterDTO)))
            .andExpect(status().isBadRequest());

        List<OrganisationTypeMaster> organisationTypeMasterList = organisationTypeMasterRepository.findAll();
        assertThat(organisationTypeMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOrganisationTypeAbbreviationIsRequired() throws Exception {
        int databaseSizeBeforeTest = organisationTypeMasterRepository.findAll().size();
        // set the field null
        organisationTypeMaster.setOrganisationTypeAbbreviation(null);

        // Create the OrganisationTypeMaster, which fails.
        OrganisationTypeMasterDTO organisationTypeMasterDTO = organisationTypeMasterMapper.toDto(organisationTypeMaster);

        restOrganisationTypeMasterMockMvc.perform(post("/api/organisation-type-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationTypeMasterDTO)))
            .andExpect(status().isBadRequest());

        List<OrganisationTypeMaster> organisationTypeMasterList = organisationTypeMasterRepository.findAll();
        assertThat(organisationTypeMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOrganisationTypeMasters() throws Exception {
        // Initialize the database
        organisationTypeMasterRepository.saveAndFlush(organisationTypeMaster);

        // Get all the organisationTypeMasterList
        restOrganisationTypeMasterMockMvc.perform(get("/api/organisation-type-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(organisationTypeMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].organisationTypeDetail").value(hasItem(DEFAULT_ORGANISATION_TYPE_DETAIL)))
            .andExpect(jsonPath("$.[*].organisationTypeAbbreviation").value(hasItem(DEFAULT_ORGANISATION_TYPE_ABBREVIATION)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
    
    @Test
    @Transactional
    public void getOrganisationTypeMaster() throws Exception {
        // Initialize the database
        organisationTypeMasterRepository.saveAndFlush(organisationTypeMaster);

        // Get the organisationTypeMaster
        restOrganisationTypeMasterMockMvc.perform(get("/api/organisation-type-masters/{id}", organisationTypeMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(organisationTypeMaster.getId().intValue()))
            .andExpect(jsonPath("$.organisationTypeDetail").value(DEFAULT_ORGANISATION_TYPE_DETAIL))
            .andExpect(jsonPath("$.organisationTypeAbbreviation").value(DEFAULT_ORGANISATION_TYPE_ABBREVIATION))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.lastUpdatedBy").value(DEFAULT_LAST_UPDATED_BY))
            .andExpect(jsonPath("$.lastUpdatedOn").value(DEFAULT_LAST_UPDATED_ON.toString()))
            .andExpect(jsonPath("$.verifiedBy").value(DEFAULT_VERIFIED_BY))
            .andExpect(jsonPath("$.verifiedOn").value(DEFAULT_VERIFIED_ON.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS));
    }

    @Test
    @Transactional
    public void getNonExistingOrganisationTypeMaster() throws Exception {
        // Get the organisationTypeMaster
        restOrganisationTypeMasterMockMvc.perform(get("/api/organisation-type-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrganisationTypeMaster() throws Exception {
        // Initialize the database
        organisationTypeMasterRepository.saveAndFlush(organisationTypeMaster);

        int databaseSizeBeforeUpdate = organisationTypeMasterRepository.findAll().size();

        // Update the organisationTypeMaster
        OrganisationTypeMaster updatedOrganisationTypeMaster = organisationTypeMasterRepository.findById(organisationTypeMaster.getId()).get();
        // Disconnect from session so that the updates on updatedOrganisationTypeMaster are not directly saved in db
        em.detach(updatedOrganisationTypeMaster);
        updatedOrganisationTypeMaster
            .organisationTypeDetail(UPDATED_ORGANISATION_TYPE_DETAIL)
            .organisationTypeAbbreviation(UPDATED_ORGANISATION_TYPE_ABBREVIATION)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS);
        OrganisationTypeMasterDTO organisationTypeMasterDTO = organisationTypeMasterMapper.toDto(updatedOrganisationTypeMaster);

        restOrganisationTypeMasterMockMvc.perform(put("/api/organisation-type-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationTypeMasterDTO)))
            .andExpect(status().isOk());

        // Validate the OrganisationTypeMaster in the database
        List<OrganisationTypeMaster> organisationTypeMasterList = organisationTypeMasterRepository.findAll();
        assertThat(organisationTypeMasterList).hasSize(databaseSizeBeforeUpdate);
        OrganisationTypeMaster testOrganisationTypeMaster = organisationTypeMasterList.get(organisationTypeMasterList.size() - 1);
        assertThat(testOrganisationTypeMaster.getOrganisationTypeDetail()).isEqualTo(UPDATED_ORGANISATION_TYPE_DETAIL);
        assertThat(testOrganisationTypeMaster.getOrganisationTypeAbbreviation()).isEqualTo(UPDATED_ORGANISATION_TYPE_ABBREVIATION);
        assertThat(testOrganisationTypeMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testOrganisationTypeMaster.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testOrganisationTypeMaster.getLastUpdatedBy()).isEqualTo(UPDATED_LAST_UPDATED_BY);
        assertThat(testOrganisationTypeMaster.getLastUpdatedOn()).isEqualTo(UPDATED_LAST_UPDATED_ON);
        assertThat(testOrganisationTypeMaster.getVerifiedBy()).isEqualTo(UPDATED_VERIFIED_BY);
        assertThat(testOrganisationTypeMaster.getVerifiedOn()).isEqualTo(UPDATED_VERIFIED_ON);
        assertThat(testOrganisationTypeMaster.getRemarks()).isEqualTo(UPDATED_REMARKS);

        // Validate the OrganisationTypeMaster in Elasticsearch
        verify(mockOrganisationTypeMasterSearchRepository, times(1)).save(testOrganisationTypeMaster);
    }

    @Test
    @Transactional
    public void updateNonExistingOrganisationTypeMaster() throws Exception {
        int databaseSizeBeforeUpdate = organisationTypeMasterRepository.findAll().size();

        // Create the OrganisationTypeMaster
        OrganisationTypeMasterDTO organisationTypeMasterDTO = organisationTypeMasterMapper.toDto(organisationTypeMaster);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrganisationTypeMasterMockMvc.perform(put("/api/organisation-type-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationTypeMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrganisationTypeMaster in the database
        List<OrganisationTypeMaster> organisationTypeMasterList = organisationTypeMasterRepository.findAll();
        assertThat(organisationTypeMasterList).hasSize(databaseSizeBeforeUpdate);

        // Validate the OrganisationTypeMaster in Elasticsearch
        verify(mockOrganisationTypeMasterSearchRepository, times(0)).save(organisationTypeMaster);
    }

    @Test
    @Transactional
    public void deleteOrganisationTypeMaster() throws Exception {
        // Initialize the database
        organisationTypeMasterRepository.saveAndFlush(organisationTypeMaster);

        int databaseSizeBeforeDelete = organisationTypeMasterRepository.findAll().size();

        // Delete the organisationTypeMaster
        restOrganisationTypeMasterMockMvc.perform(delete("/api/organisation-type-masters/{id}", organisationTypeMaster.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrganisationTypeMaster> organisationTypeMasterList = organisationTypeMasterRepository.findAll();
        assertThat(organisationTypeMasterList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the OrganisationTypeMaster in Elasticsearch
        verify(mockOrganisationTypeMasterSearchRepository, times(1)).deleteById(organisationTypeMaster.getId());
    }

    @Test
    @Transactional
    public void searchOrganisationTypeMaster() throws Exception {
        // Initialize the database
        organisationTypeMasterRepository.saveAndFlush(organisationTypeMaster);
        when(mockOrganisationTypeMasterSearchRepository.search(queryStringQuery("id:" + organisationTypeMaster.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(organisationTypeMaster), PageRequest.of(0, 1), 1));
        // Search the organisationTypeMaster
        restOrganisationTypeMasterMockMvc.perform(get("/api/_search/organisation-type-masters?query=id:" + organisationTypeMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(organisationTypeMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].organisationTypeDetail").value(hasItem(DEFAULT_ORGANISATION_TYPE_DETAIL)))
            .andExpect(jsonPath("$.[*].organisationTypeAbbreviation").value(hasItem(DEFAULT_ORGANISATION_TYPE_ABBREVIATION)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
}
