package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.Department;
import com.hartron.esignharyana.repository.DepartmentRepository;
import com.hartron.esignharyana.repository.search.DepartmentSearchRepository;
import com.hartron.esignharyana.service.DepartmentService;
import com.hartron.esignharyana.service.dto.DepartmentDTO;
import com.hartron.esignharyana.service.mapper.DepartmentMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;
import com.hartron.esignharyana.service.DepartmentQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DepartmentResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class DepartmentResourceIT {

    private static final String DEFAULT_DEPARTMENT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_ON = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CREATED_ON = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_UPDATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_ON = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_UPDATED_ON = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_REMARK = "AAAAAAAAAA";
    private static final String UPDATED_REMARK = "BBBBBBBBBB";

    private static final String DEFAULT_WHITE_LIST_IP_1 = "AAAAAAAAAA";
    private static final String UPDATED_WHITE_LIST_IP_1 = "BBBBBBBBBB";

    private static final String DEFAULT_WHITE_LIST_IP_2 = "AAAAAAAAAA";
    private static final String UPDATED_WHITE_LIST_IP_2 = "BBBBBBBBBB";

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private DepartmentMapper departmentMapper;

    @Autowired
    private DepartmentService departmentService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.DepartmentSearchRepositoryMockConfiguration
     */
    @Autowired
    private DepartmentSearchRepository mockDepartmentSearchRepository;

    @Autowired
    private DepartmentQueryService departmentQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDepartmentMockMvc;

    private Department department;

    @BeforeEach
    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final DepartmentResource departmentResource = new DepartmentResource(departmentService, departmentQueryService, userService);
//        this.restDepartmentMockMvc = MockMvcBuilders.standaloneSetup(departmentResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Department createEntity(EntityManager em) {
        Department department = new Department()
            .departmentName(DEFAULT_DEPARTMENT_NAME)
            .createdOn(DEFAULT_CREATED_ON)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedOn(DEFAULT_UPDATED_ON)
            .updatedBy(DEFAULT_UPDATED_BY)
            .status(DEFAULT_STATUS)
            .remark(DEFAULT_REMARK)
            .whiteListIp1(DEFAULT_WHITE_LIST_IP_1)
            .whiteListIp2(DEFAULT_WHITE_LIST_IP_2);
        return department;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Department createUpdatedEntity(EntityManager em) {
        Department department = new Department()
            .departmentName(UPDATED_DEPARTMENT_NAME)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .updatedOn(UPDATED_UPDATED_ON)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS)
            .remark(UPDATED_REMARK)
            .whiteListIp1(UPDATED_WHITE_LIST_IP_1)
            .whiteListIp2(UPDATED_WHITE_LIST_IP_2);
        return department;
    }

    @BeforeEach
    public void initTest() {
        department = createEntity(em);
    }

    @Test
    @Transactional
    public void createDepartment() throws Exception {
        int databaseSizeBeforeCreate = departmentRepository.findAll().size();

        // Create the Department
        DepartmentDTO departmentDTO = departmentMapper.toDto(department);
        restDepartmentMockMvc.perform(post("/api/departments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(departmentDTO)))
            .andExpect(status().isCreated());

        // Validate the Department in the database
        List<Department> departmentList = departmentRepository.findAll();
        assertThat(departmentList).hasSize(databaseSizeBeforeCreate + 1);
        Department testDepartment = departmentList.get(departmentList.size() - 1);
        assertThat(testDepartment.getDepartmentName()).isEqualTo(DEFAULT_DEPARTMENT_NAME);
        assertThat(testDepartment.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testDepartment.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testDepartment.getUpdatedOn()).isEqualTo(DEFAULT_UPDATED_ON);
        assertThat(testDepartment.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testDepartment.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testDepartment.getRemark()).isEqualTo(DEFAULT_REMARK);
        assertThat(testDepartment.getWhiteListIp1()).isEqualTo(DEFAULT_WHITE_LIST_IP_1);
        assertThat(testDepartment.getWhiteListIp2()).isEqualTo(DEFAULT_WHITE_LIST_IP_2);

        // Validate the Department in Elasticsearch
        verify(mockDepartmentSearchRepository, times(1)).save(testDepartment);
    }

    @Test
    @Transactional
    public void createDepartmentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = departmentRepository.findAll().size();

        // Create the Department with an existing ID
        department.setId(1L);
        DepartmentDTO departmentDTO = departmentMapper.toDto(department);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDepartmentMockMvc.perform(post("/api/departments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(departmentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Department in the database
        List<Department> departmentList = departmentRepository.findAll();
        assertThat(departmentList).hasSize(databaseSizeBeforeCreate);

        // Validate the Department in Elasticsearch
        verify(mockDepartmentSearchRepository, times(0)).save(department);
    }


    @Test
    @Transactional
    public void getAllDepartments() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList
        restDepartmentMockMvc.perform(get("/api/departments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(department.getId().intValue())))
            .andExpect(jsonPath("$.[*].departmentName").value(hasItem(DEFAULT_DEPARTMENT_NAME)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedOn").value(hasItem(DEFAULT_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].remark").value(hasItem(DEFAULT_REMARK)))
            .andExpect(jsonPath("$.[*].whiteListIp1").value(hasItem(DEFAULT_WHITE_LIST_IP_1)))
            .andExpect(jsonPath("$.[*].whiteListIp2").value(hasItem(DEFAULT_WHITE_LIST_IP_2)));
    }
    
    @Test
    @Transactional
    public void getDepartment() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get the department
        restDepartmentMockMvc.perform(get("/api/departments/{id}", department.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(department.getId().intValue()))
            .andExpect(jsonPath("$.departmentName").value(DEFAULT_DEPARTMENT_NAME))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedOn").value(DEFAULT_UPDATED_ON.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.remark").value(DEFAULT_REMARK))
            .andExpect(jsonPath("$.whiteListIp1").value(DEFAULT_WHITE_LIST_IP_1))
            .andExpect(jsonPath("$.whiteListIp2").value(DEFAULT_WHITE_LIST_IP_2));
    }


    @Test
    @Transactional
    public void getDepartmentsByIdFiltering() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        Long id = department.getId();

        defaultDepartmentShouldBeFound("id.equals=" + id);
        defaultDepartmentShouldNotBeFound("id.notEquals=" + id);

        defaultDepartmentShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDepartmentShouldNotBeFound("id.greaterThan=" + id);

        defaultDepartmentShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDepartmentShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllDepartmentsByDepartmentNameIsEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where departmentName equals to DEFAULT_DEPARTMENT_NAME
        defaultDepartmentShouldBeFound("departmentName.equals=" + DEFAULT_DEPARTMENT_NAME);

        // Get all the departmentList where departmentName equals to UPDATED_DEPARTMENT_NAME
        defaultDepartmentShouldNotBeFound("departmentName.equals=" + UPDATED_DEPARTMENT_NAME);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByDepartmentNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where departmentName not equals to DEFAULT_DEPARTMENT_NAME
        defaultDepartmentShouldNotBeFound("departmentName.notEquals=" + DEFAULT_DEPARTMENT_NAME);

        // Get all the departmentList where departmentName not equals to UPDATED_DEPARTMENT_NAME
        defaultDepartmentShouldBeFound("departmentName.notEquals=" + UPDATED_DEPARTMENT_NAME);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByDepartmentNameIsInShouldWork() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where departmentName in DEFAULT_DEPARTMENT_NAME or UPDATED_DEPARTMENT_NAME
        defaultDepartmentShouldBeFound("departmentName.in=" + DEFAULT_DEPARTMENT_NAME + "," + UPDATED_DEPARTMENT_NAME);

        // Get all the departmentList where departmentName equals to UPDATED_DEPARTMENT_NAME
        defaultDepartmentShouldNotBeFound("departmentName.in=" + UPDATED_DEPARTMENT_NAME);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByDepartmentNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where departmentName is not null
        defaultDepartmentShouldBeFound("departmentName.specified=true");

        // Get all the departmentList where departmentName is null
        defaultDepartmentShouldNotBeFound("departmentName.specified=false");
    }
                @Test
    @Transactional
    public void getAllDepartmentsByDepartmentNameContainsSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where departmentName contains DEFAULT_DEPARTMENT_NAME
        defaultDepartmentShouldBeFound("departmentName.contains=" + DEFAULT_DEPARTMENT_NAME);

        // Get all the departmentList where departmentName contains UPDATED_DEPARTMENT_NAME
        defaultDepartmentShouldNotBeFound("departmentName.contains=" + UPDATED_DEPARTMENT_NAME);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByDepartmentNameNotContainsSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where departmentName does not contain DEFAULT_DEPARTMENT_NAME
        defaultDepartmentShouldNotBeFound("departmentName.doesNotContain=" + DEFAULT_DEPARTMENT_NAME);

        // Get all the departmentList where departmentName does not contain UPDATED_DEPARTMENT_NAME
        defaultDepartmentShouldBeFound("departmentName.doesNotContain=" + UPDATED_DEPARTMENT_NAME);
    }


    @Test
    @Transactional
    public void getAllDepartmentsByCreatedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where createdOn equals to DEFAULT_CREATED_ON
        defaultDepartmentShouldBeFound("createdOn.equals=" + DEFAULT_CREATED_ON);

        // Get all the departmentList where createdOn equals to UPDATED_CREATED_ON
        defaultDepartmentShouldNotBeFound("createdOn.equals=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByCreatedOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where createdOn not equals to DEFAULT_CREATED_ON
        defaultDepartmentShouldNotBeFound("createdOn.notEquals=" + DEFAULT_CREATED_ON);

        // Get all the departmentList where createdOn not equals to UPDATED_CREATED_ON
        defaultDepartmentShouldBeFound("createdOn.notEquals=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByCreatedOnIsInShouldWork() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where createdOn in DEFAULT_CREATED_ON or UPDATED_CREATED_ON
        defaultDepartmentShouldBeFound("createdOn.in=" + DEFAULT_CREATED_ON + "," + UPDATED_CREATED_ON);

        // Get all the departmentList where createdOn equals to UPDATED_CREATED_ON
        defaultDepartmentShouldNotBeFound("createdOn.in=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByCreatedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where createdOn is not null
        defaultDepartmentShouldBeFound("createdOn.specified=true");

        // Get all the departmentList where createdOn is null
        defaultDepartmentShouldNotBeFound("createdOn.specified=false");
    }

    @Test
    @Transactional
    public void getAllDepartmentsByCreatedOnIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where createdOn is greater than or equal to DEFAULT_CREATED_ON
        defaultDepartmentShouldBeFound("createdOn.greaterThanOrEqual=" + DEFAULT_CREATED_ON);

        // Get all the departmentList where createdOn is greater than or equal to UPDATED_CREATED_ON
        defaultDepartmentShouldNotBeFound("createdOn.greaterThanOrEqual=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByCreatedOnIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where createdOn is less than or equal to DEFAULT_CREATED_ON
        defaultDepartmentShouldBeFound("createdOn.lessThanOrEqual=" + DEFAULT_CREATED_ON);

        // Get all the departmentList where createdOn is less than or equal to SMALLER_CREATED_ON
        defaultDepartmentShouldNotBeFound("createdOn.lessThanOrEqual=" + SMALLER_CREATED_ON);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByCreatedOnIsLessThanSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where createdOn is less than DEFAULT_CREATED_ON
        defaultDepartmentShouldNotBeFound("createdOn.lessThan=" + DEFAULT_CREATED_ON);

        // Get all the departmentList where createdOn is less than UPDATED_CREATED_ON
        defaultDepartmentShouldBeFound("createdOn.lessThan=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByCreatedOnIsGreaterThanSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where createdOn is greater than DEFAULT_CREATED_ON
        defaultDepartmentShouldNotBeFound("createdOn.greaterThan=" + DEFAULT_CREATED_ON);

        // Get all the departmentList where createdOn is greater than SMALLER_CREATED_ON
        defaultDepartmentShouldBeFound("createdOn.greaterThan=" + SMALLER_CREATED_ON);
    }


    @Test
    @Transactional
    public void getAllDepartmentsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where createdBy equals to DEFAULT_CREATED_BY
        defaultDepartmentShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the departmentList where createdBy equals to UPDATED_CREATED_BY
        defaultDepartmentShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where createdBy not equals to DEFAULT_CREATED_BY
        defaultDepartmentShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the departmentList where createdBy not equals to UPDATED_CREATED_BY
        defaultDepartmentShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultDepartmentShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the departmentList where createdBy equals to UPDATED_CREATED_BY
        defaultDepartmentShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where createdBy is not null
        defaultDepartmentShouldBeFound("createdBy.specified=true");

        // Get all the departmentList where createdBy is null
        defaultDepartmentShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllDepartmentsByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where createdBy contains DEFAULT_CREATED_BY
        defaultDepartmentShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the departmentList where createdBy contains UPDATED_CREATED_BY
        defaultDepartmentShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where createdBy does not contain DEFAULT_CREATED_BY
        defaultDepartmentShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the departmentList where createdBy does not contain UPDATED_CREATED_BY
        defaultDepartmentShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllDepartmentsByUpdatedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where updatedOn equals to DEFAULT_UPDATED_ON
        defaultDepartmentShouldBeFound("updatedOn.equals=" + DEFAULT_UPDATED_ON);

        // Get all the departmentList where updatedOn equals to UPDATED_UPDATED_ON
        defaultDepartmentShouldNotBeFound("updatedOn.equals=" + UPDATED_UPDATED_ON);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByUpdatedOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where updatedOn not equals to DEFAULT_UPDATED_ON
        defaultDepartmentShouldNotBeFound("updatedOn.notEquals=" + DEFAULT_UPDATED_ON);

        // Get all the departmentList where updatedOn not equals to UPDATED_UPDATED_ON
        defaultDepartmentShouldBeFound("updatedOn.notEquals=" + UPDATED_UPDATED_ON);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByUpdatedOnIsInShouldWork() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where updatedOn in DEFAULT_UPDATED_ON or UPDATED_UPDATED_ON
        defaultDepartmentShouldBeFound("updatedOn.in=" + DEFAULT_UPDATED_ON + "," + UPDATED_UPDATED_ON);

        // Get all the departmentList where updatedOn equals to UPDATED_UPDATED_ON
        defaultDepartmentShouldNotBeFound("updatedOn.in=" + UPDATED_UPDATED_ON);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByUpdatedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where updatedOn is not null
        defaultDepartmentShouldBeFound("updatedOn.specified=true");

        // Get all the departmentList where updatedOn is null
        defaultDepartmentShouldNotBeFound("updatedOn.specified=false");
    }

    @Test
    @Transactional
    public void getAllDepartmentsByUpdatedOnIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where updatedOn is greater than or equal to DEFAULT_UPDATED_ON
        defaultDepartmentShouldBeFound("updatedOn.greaterThanOrEqual=" + DEFAULT_UPDATED_ON);

        // Get all the departmentList where updatedOn is greater than or equal to UPDATED_UPDATED_ON
        defaultDepartmentShouldNotBeFound("updatedOn.greaterThanOrEqual=" + UPDATED_UPDATED_ON);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByUpdatedOnIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where updatedOn is less than or equal to DEFAULT_UPDATED_ON
        defaultDepartmentShouldBeFound("updatedOn.lessThanOrEqual=" + DEFAULT_UPDATED_ON);

        // Get all the departmentList where updatedOn is less than or equal to SMALLER_UPDATED_ON
        defaultDepartmentShouldNotBeFound("updatedOn.lessThanOrEqual=" + SMALLER_UPDATED_ON);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByUpdatedOnIsLessThanSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where updatedOn is less than DEFAULT_UPDATED_ON
        defaultDepartmentShouldNotBeFound("updatedOn.lessThan=" + DEFAULT_UPDATED_ON);

        // Get all the departmentList where updatedOn is less than UPDATED_UPDATED_ON
        defaultDepartmentShouldBeFound("updatedOn.lessThan=" + UPDATED_UPDATED_ON);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByUpdatedOnIsGreaterThanSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where updatedOn is greater than DEFAULT_UPDATED_ON
        defaultDepartmentShouldNotBeFound("updatedOn.greaterThan=" + DEFAULT_UPDATED_ON);

        // Get all the departmentList where updatedOn is greater than SMALLER_UPDATED_ON
        defaultDepartmentShouldBeFound("updatedOn.greaterThan=" + SMALLER_UPDATED_ON);
    }


    @Test
    @Transactional
    public void getAllDepartmentsByUpdatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where updatedBy equals to DEFAULT_UPDATED_BY
        defaultDepartmentShouldBeFound("updatedBy.equals=" + DEFAULT_UPDATED_BY);

        // Get all the departmentList where updatedBy equals to UPDATED_UPDATED_BY
        defaultDepartmentShouldNotBeFound("updatedBy.equals=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByUpdatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where updatedBy not equals to DEFAULT_UPDATED_BY
        defaultDepartmentShouldNotBeFound("updatedBy.notEquals=" + DEFAULT_UPDATED_BY);

        // Get all the departmentList where updatedBy not equals to UPDATED_UPDATED_BY
        defaultDepartmentShouldBeFound("updatedBy.notEquals=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByUpdatedByIsInShouldWork() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where updatedBy in DEFAULT_UPDATED_BY or UPDATED_UPDATED_BY
        defaultDepartmentShouldBeFound("updatedBy.in=" + DEFAULT_UPDATED_BY + "," + UPDATED_UPDATED_BY);

        // Get all the departmentList where updatedBy equals to UPDATED_UPDATED_BY
        defaultDepartmentShouldNotBeFound("updatedBy.in=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByUpdatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where updatedBy is not null
        defaultDepartmentShouldBeFound("updatedBy.specified=true");

        // Get all the departmentList where updatedBy is null
        defaultDepartmentShouldNotBeFound("updatedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllDepartmentsByUpdatedByContainsSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where updatedBy contains DEFAULT_UPDATED_BY
        defaultDepartmentShouldBeFound("updatedBy.contains=" + DEFAULT_UPDATED_BY);

        // Get all the departmentList where updatedBy contains UPDATED_UPDATED_BY
        defaultDepartmentShouldNotBeFound("updatedBy.contains=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByUpdatedByNotContainsSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where updatedBy does not contain DEFAULT_UPDATED_BY
        defaultDepartmentShouldNotBeFound("updatedBy.doesNotContain=" + DEFAULT_UPDATED_BY);

        // Get all the departmentList where updatedBy does not contain UPDATED_UPDATED_BY
        defaultDepartmentShouldBeFound("updatedBy.doesNotContain=" + UPDATED_UPDATED_BY);
    }


    @Test
    @Transactional
    public void getAllDepartmentsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where status equals to DEFAULT_STATUS
        defaultDepartmentShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the departmentList where status equals to UPDATED_STATUS
        defaultDepartmentShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where status not equals to DEFAULT_STATUS
        defaultDepartmentShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the departmentList where status not equals to UPDATED_STATUS
        defaultDepartmentShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultDepartmentShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the departmentList where status equals to UPDATED_STATUS
        defaultDepartmentShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where status is not null
        defaultDepartmentShouldBeFound("status.specified=true");

        // Get all the departmentList where status is null
        defaultDepartmentShouldNotBeFound("status.specified=false");
    }
                @Test
    @Transactional
    public void getAllDepartmentsByStatusContainsSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where status contains DEFAULT_STATUS
        defaultDepartmentShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the departmentList where status contains UPDATED_STATUS
        defaultDepartmentShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where status does not contain DEFAULT_STATUS
        defaultDepartmentShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the departmentList where status does not contain UPDATED_STATUS
        defaultDepartmentShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllDepartmentsByRemarkIsEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where remark equals to DEFAULT_REMARK
        defaultDepartmentShouldBeFound("remark.equals=" + DEFAULT_REMARK);

        // Get all the departmentList where remark equals to UPDATED_REMARK
        defaultDepartmentShouldNotBeFound("remark.equals=" + UPDATED_REMARK);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByRemarkIsNotEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where remark not equals to DEFAULT_REMARK
        defaultDepartmentShouldNotBeFound("remark.notEquals=" + DEFAULT_REMARK);

        // Get all the departmentList where remark not equals to UPDATED_REMARK
        defaultDepartmentShouldBeFound("remark.notEquals=" + UPDATED_REMARK);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByRemarkIsInShouldWork() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where remark in DEFAULT_REMARK or UPDATED_REMARK
        defaultDepartmentShouldBeFound("remark.in=" + DEFAULT_REMARK + "," + UPDATED_REMARK);

        // Get all the departmentList where remark equals to UPDATED_REMARK
        defaultDepartmentShouldNotBeFound("remark.in=" + UPDATED_REMARK);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByRemarkIsNullOrNotNull() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where remark is not null
        defaultDepartmentShouldBeFound("remark.specified=true");

        // Get all the departmentList where remark is null
        defaultDepartmentShouldNotBeFound("remark.specified=false");
    }
                @Test
    @Transactional
    public void getAllDepartmentsByRemarkContainsSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where remark contains DEFAULT_REMARK
        defaultDepartmentShouldBeFound("remark.contains=" + DEFAULT_REMARK);

        // Get all the departmentList where remark contains UPDATED_REMARK
        defaultDepartmentShouldNotBeFound("remark.contains=" + UPDATED_REMARK);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByRemarkNotContainsSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where remark does not contain DEFAULT_REMARK
        defaultDepartmentShouldNotBeFound("remark.doesNotContain=" + DEFAULT_REMARK);

        // Get all the departmentList where remark does not contain UPDATED_REMARK
        defaultDepartmentShouldBeFound("remark.doesNotContain=" + UPDATED_REMARK);
    }


    @Test
    @Transactional
    public void getAllDepartmentsByWhiteListIp1IsEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where whiteListIp1 equals to DEFAULT_WHITE_LIST_IP_1
        defaultDepartmentShouldBeFound("whiteListIp1.equals=" + DEFAULT_WHITE_LIST_IP_1);

        // Get all the departmentList where whiteListIp1 equals to UPDATED_WHITE_LIST_IP_1
        defaultDepartmentShouldNotBeFound("whiteListIp1.equals=" + UPDATED_WHITE_LIST_IP_1);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByWhiteListIp1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where whiteListIp1 not equals to DEFAULT_WHITE_LIST_IP_1
        defaultDepartmentShouldNotBeFound("whiteListIp1.notEquals=" + DEFAULT_WHITE_LIST_IP_1);

        // Get all the departmentList where whiteListIp1 not equals to UPDATED_WHITE_LIST_IP_1
        defaultDepartmentShouldBeFound("whiteListIp1.notEquals=" + UPDATED_WHITE_LIST_IP_1);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByWhiteListIp1IsInShouldWork() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where whiteListIp1 in DEFAULT_WHITE_LIST_IP_1 or UPDATED_WHITE_LIST_IP_1
        defaultDepartmentShouldBeFound("whiteListIp1.in=" + DEFAULT_WHITE_LIST_IP_1 + "," + UPDATED_WHITE_LIST_IP_1);

        // Get all the departmentList where whiteListIp1 equals to UPDATED_WHITE_LIST_IP_1
        defaultDepartmentShouldNotBeFound("whiteListIp1.in=" + UPDATED_WHITE_LIST_IP_1);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByWhiteListIp1IsNullOrNotNull() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where whiteListIp1 is not null
        defaultDepartmentShouldBeFound("whiteListIp1.specified=true");

        // Get all the departmentList where whiteListIp1 is null
        defaultDepartmentShouldNotBeFound("whiteListIp1.specified=false");
    }
                @Test
    @Transactional
    public void getAllDepartmentsByWhiteListIp1ContainsSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where whiteListIp1 contains DEFAULT_WHITE_LIST_IP_1
        defaultDepartmentShouldBeFound("whiteListIp1.contains=" + DEFAULT_WHITE_LIST_IP_1);

        // Get all the departmentList where whiteListIp1 contains UPDATED_WHITE_LIST_IP_1
        defaultDepartmentShouldNotBeFound("whiteListIp1.contains=" + UPDATED_WHITE_LIST_IP_1);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByWhiteListIp1NotContainsSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where whiteListIp1 does not contain DEFAULT_WHITE_LIST_IP_1
        defaultDepartmentShouldNotBeFound("whiteListIp1.doesNotContain=" + DEFAULT_WHITE_LIST_IP_1);

        // Get all the departmentList where whiteListIp1 does not contain UPDATED_WHITE_LIST_IP_1
        defaultDepartmentShouldBeFound("whiteListIp1.doesNotContain=" + UPDATED_WHITE_LIST_IP_1);
    }


    @Test
    @Transactional
    public void getAllDepartmentsByWhiteListIp2IsEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where whiteListIp2 equals to DEFAULT_WHITE_LIST_IP_2
        defaultDepartmentShouldBeFound("whiteListIp2.equals=" + DEFAULT_WHITE_LIST_IP_2);

        // Get all the departmentList where whiteListIp2 equals to UPDATED_WHITE_LIST_IP_2
        defaultDepartmentShouldNotBeFound("whiteListIp2.equals=" + UPDATED_WHITE_LIST_IP_2);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByWhiteListIp2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where whiteListIp2 not equals to DEFAULT_WHITE_LIST_IP_2
        defaultDepartmentShouldNotBeFound("whiteListIp2.notEquals=" + DEFAULT_WHITE_LIST_IP_2);

        // Get all the departmentList where whiteListIp2 not equals to UPDATED_WHITE_LIST_IP_2
        defaultDepartmentShouldBeFound("whiteListIp2.notEquals=" + UPDATED_WHITE_LIST_IP_2);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByWhiteListIp2IsInShouldWork() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where whiteListIp2 in DEFAULT_WHITE_LIST_IP_2 or UPDATED_WHITE_LIST_IP_2
        defaultDepartmentShouldBeFound("whiteListIp2.in=" + DEFAULT_WHITE_LIST_IP_2 + "," + UPDATED_WHITE_LIST_IP_2);

        // Get all the departmentList where whiteListIp2 equals to UPDATED_WHITE_LIST_IP_2
        defaultDepartmentShouldNotBeFound("whiteListIp2.in=" + UPDATED_WHITE_LIST_IP_2);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByWhiteListIp2IsNullOrNotNull() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where whiteListIp2 is not null
        defaultDepartmentShouldBeFound("whiteListIp2.specified=true");

        // Get all the departmentList where whiteListIp2 is null
        defaultDepartmentShouldNotBeFound("whiteListIp2.specified=false");
    }
                @Test
    @Transactional
    public void getAllDepartmentsByWhiteListIp2ContainsSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where whiteListIp2 contains DEFAULT_WHITE_LIST_IP_2
        defaultDepartmentShouldBeFound("whiteListIp2.contains=" + DEFAULT_WHITE_LIST_IP_2);

        // Get all the departmentList where whiteListIp2 contains UPDATED_WHITE_LIST_IP_2
        defaultDepartmentShouldNotBeFound("whiteListIp2.contains=" + UPDATED_WHITE_LIST_IP_2);
    }

    @Test
    @Transactional
    public void getAllDepartmentsByWhiteListIp2NotContainsSomething() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        // Get all the departmentList where whiteListIp2 does not contain DEFAULT_WHITE_LIST_IP_2
        defaultDepartmentShouldNotBeFound("whiteListIp2.doesNotContain=" + DEFAULT_WHITE_LIST_IP_2);

        // Get all the departmentList where whiteListIp2 does not contain UPDATED_WHITE_LIST_IP_2
        defaultDepartmentShouldBeFound("whiteListIp2.doesNotContain=" + UPDATED_WHITE_LIST_IP_2);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDepartmentShouldBeFound(String filter) throws Exception {
        restDepartmentMockMvc.perform(get("/api/departments?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(department.getId().intValue())))
            .andExpect(jsonPath("$.[*].departmentName").value(hasItem(DEFAULT_DEPARTMENT_NAME)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedOn").value(hasItem(DEFAULT_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].remark").value(hasItem(DEFAULT_REMARK)))
            .andExpect(jsonPath("$.[*].whiteListIp1").value(hasItem(DEFAULT_WHITE_LIST_IP_1)))
            .andExpect(jsonPath("$.[*].whiteListIp2").value(hasItem(DEFAULT_WHITE_LIST_IP_2)));

        // Check, that the count call also returns 1
        restDepartmentMockMvc.perform(get("/api/departments/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDepartmentShouldNotBeFound(String filter) throws Exception {
        restDepartmentMockMvc.perform(get("/api/departments?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDepartmentMockMvc.perform(get("/api/departments/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingDepartment() throws Exception {
        // Get the department
        restDepartmentMockMvc.perform(get("/api/departments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDepartment() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        int databaseSizeBeforeUpdate = departmentRepository.findAll().size();

        // Update the department
        Department updatedDepartment = departmentRepository.findById(department.getId()).get();
        // Disconnect from session so that the updates on updatedDepartment are not directly saved in db
        em.detach(updatedDepartment);
        updatedDepartment
            .departmentName(UPDATED_DEPARTMENT_NAME)
            .createdOn(UPDATED_CREATED_ON)
            .createdBy(UPDATED_CREATED_BY)
            .updatedOn(UPDATED_UPDATED_ON)
            .updatedBy(UPDATED_UPDATED_BY)
            .status(UPDATED_STATUS)
            .remark(UPDATED_REMARK)
            .whiteListIp1(UPDATED_WHITE_LIST_IP_1)
            .whiteListIp2(UPDATED_WHITE_LIST_IP_2);
        DepartmentDTO departmentDTO = departmentMapper.toDto(updatedDepartment);

        restDepartmentMockMvc.perform(put("/api/departments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(departmentDTO)))
            .andExpect(status().isOk());

        // Validate the Department in the database
        List<Department> departmentList = departmentRepository.findAll();
        assertThat(departmentList).hasSize(databaseSizeBeforeUpdate);
        Department testDepartment = departmentList.get(departmentList.size() - 1);
        assertThat(testDepartment.getDepartmentName()).isEqualTo(UPDATED_DEPARTMENT_NAME);
        assertThat(testDepartment.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testDepartment.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testDepartment.getUpdatedOn()).isEqualTo(UPDATED_UPDATED_ON);
        assertThat(testDepartment.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testDepartment.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testDepartment.getRemark()).isEqualTo(UPDATED_REMARK);
        assertThat(testDepartment.getWhiteListIp1()).isEqualTo(UPDATED_WHITE_LIST_IP_1);
        assertThat(testDepartment.getWhiteListIp2()).isEqualTo(UPDATED_WHITE_LIST_IP_2);

        // Validate the Department in Elasticsearch
        verify(mockDepartmentSearchRepository, times(1)).save(testDepartment);
    }

    @Test
    @Transactional
    public void updateNonExistingDepartment() throws Exception {
        int databaseSizeBeforeUpdate = departmentRepository.findAll().size();

        // Create the Department
        DepartmentDTO departmentDTO = departmentMapper.toDto(department);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDepartmentMockMvc.perform(put("/api/departments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(departmentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Department in the database
        List<Department> departmentList = departmentRepository.findAll();
        assertThat(departmentList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Department in Elasticsearch
        verify(mockDepartmentSearchRepository, times(0)).save(department);
    }

    @Test
    @Transactional
    public void deleteDepartment() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);

        int databaseSizeBeforeDelete = departmentRepository.findAll().size();

        // Delete the department
        restDepartmentMockMvc.perform(delete("/api/departments/{id}", department.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Department> departmentList = departmentRepository.findAll();
        assertThat(departmentList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Department in Elasticsearch
        verify(mockDepartmentSearchRepository, times(1)).deleteById(department.getId());
    }

    @Test
    @Transactional
    public void searchDepartment() throws Exception {
        // Initialize the database
        departmentRepository.saveAndFlush(department);
        when(mockDepartmentSearchRepository.search(queryStringQuery("id:" + department.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(department), PageRequest.of(0, 1), 1));
        // Search the department
        restDepartmentMockMvc.perform(get("/api/_search/departments?query=id:" + department.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(department.getId().intValue())))
            .andExpect(jsonPath("$.[*].departmentName").value(hasItem(DEFAULT_DEPARTMENT_NAME)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedOn").value(hasItem(DEFAULT_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].remark").value(hasItem(DEFAULT_REMARK)))
            .andExpect(jsonPath("$.[*].whiteListIp1").value(hasItem(DEFAULT_WHITE_LIST_IP_1)))
            .andExpect(jsonPath("$.[*].whiteListIp2").value(hasItem(DEFAULT_WHITE_LIST_IP_2)));
    }
}
