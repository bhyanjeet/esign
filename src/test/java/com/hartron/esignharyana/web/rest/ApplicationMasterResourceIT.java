package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.ApplicationMaster;
import com.hartron.esignharyana.repository.ApplicationMasterRepository;
import com.hartron.esignharyana.repository.search.ApplicationMasterSearchRepository;
import com.hartron.esignharyana.service.ApplicationMasterService;
import com.hartron.esignharyana.service.dto.ApplicationMasterDTO;
import com.hartron.esignharyana.service.mapper.ApplicationMasterMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ApplicationMasterResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class ApplicationMasterResourceIT {

    private static final String DEFAULT_APPLICATION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_APPLICATION_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LAST_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_VERIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_VERIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_VERIFIED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VERIFIED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final String DEFAULT_APPLICATION_URL = "AAAAAAAAAA";
    private static final String UPDATED_APPLICATION_URL = "BBBBBBBBBB";

    private static final String DEFAULT_APPLICATION_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_APPLICATION_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_APPLICATION_ID_CODE = "AAAAAAAAAA";
    private static final String UPDATED_APPLICATION_ID_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_CERT_PUBLIC_KEY = "AAAAAAAAAA";
    private static final String UPDATED_CERT_PUBLIC_KEY = "BBBBBBBBBB";

    @Autowired
    private ApplicationMasterRepository applicationMasterRepository;

    @Autowired
    private ApplicationMasterMapper applicationMasterMapper;

    @Autowired
    private ApplicationMasterService applicationMasterService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.ApplicationMasterSearchRepositoryMockConfiguration
     */
    @Autowired
    private ApplicationMasterSearchRepository mockApplicationMasterSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restApplicationMasterMockMvc;

    private ApplicationMaster applicationMaster;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ApplicationMasterResource applicationMasterResource = new ApplicationMasterResource(applicationMasterService);
        this.restApplicationMasterMockMvc = MockMvcBuilders.standaloneSetup(applicationMasterResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ApplicationMaster createEntity(EntityManager em) {
        ApplicationMaster applicationMaster = new ApplicationMaster()
            .applicationName(DEFAULT_APPLICATION_NAME)
            .createdBy(DEFAULT_CREATED_BY)
            .createdOn(DEFAULT_CREATED_ON)
            .lastUpdatedBy(DEFAULT_LAST_UPDATED_BY)
            .lastUpdatedOn(DEFAULT_LAST_UPDATED_ON)
            .verifiedBy(DEFAULT_VERIFIED_BY)
            .verifiedOn(DEFAULT_VERIFIED_ON)
            .remarks(DEFAULT_REMARKS)
            .applicationURL(DEFAULT_APPLICATION_URL)
            .applicationStatus(DEFAULT_APPLICATION_STATUS)
            .applicationIdCode(DEFAULT_APPLICATION_ID_CODE)
            .status(DEFAULT_STATUS)
            .certPublicKey(DEFAULT_CERT_PUBLIC_KEY);
        return applicationMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ApplicationMaster createUpdatedEntity(EntityManager em) {
        ApplicationMaster applicationMaster = new ApplicationMaster()
            .applicationName(UPDATED_APPLICATION_NAME)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS)
            .applicationURL(UPDATED_APPLICATION_URL)
            .applicationStatus(UPDATED_APPLICATION_STATUS)
            .applicationIdCode(UPDATED_APPLICATION_ID_CODE)
            .status(UPDATED_STATUS)
            .certPublicKey(UPDATED_CERT_PUBLIC_KEY);
        return applicationMaster;
    }

    @BeforeEach
    public void initTest() {
        applicationMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createApplicationMaster() throws Exception {
        int databaseSizeBeforeCreate = applicationMasterRepository.findAll().size();

        // Create the ApplicationMaster
        ApplicationMasterDTO applicationMasterDTO = applicationMasterMapper.toDto(applicationMaster);
        restApplicationMasterMockMvc.perform(post("/api/application-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(applicationMasterDTO)))
            .andExpect(status().isCreated());

        // Validate the ApplicationMaster in the database
        List<ApplicationMaster> applicationMasterList = applicationMasterRepository.findAll();
        assertThat(applicationMasterList).hasSize(databaseSizeBeforeCreate + 1);
        ApplicationMaster testApplicationMaster = applicationMasterList.get(applicationMasterList.size() - 1);
        assertThat(testApplicationMaster.getApplicationName()).isEqualTo(DEFAULT_APPLICATION_NAME);
        assertThat(testApplicationMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testApplicationMaster.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testApplicationMaster.getLastUpdatedBy()).isEqualTo(DEFAULT_LAST_UPDATED_BY);
        assertThat(testApplicationMaster.getLastUpdatedOn()).isEqualTo(DEFAULT_LAST_UPDATED_ON);
        assertThat(testApplicationMaster.getVerifiedBy()).isEqualTo(DEFAULT_VERIFIED_BY);
        assertThat(testApplicationMaster.getVerifiedOn()).isEqualTo(DEFAULT_VERIFIED_ON);
        assertThat(testApplicationMaster.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testApplicationMaster.getApplicationURL()).isEqualTo(DEFAULT_APPLICATION_URL);
        assertThat(testApplicationMaster.getApplicationStatus()).isEqualTo(DEFAULT_APPLICATION_STATUS);
        assertThat(testApplicationMaster.getApplicationIdCode()).isEqualTo(DEFAULT_APPLICATION_ID_CODE);
        assertThat(testApplicationMaster.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testApplicationMaster.getCertPublicKey()).isEqualTo(DEFAULT_CERT_PUBLIC_KEY);

        // Validate the ApplicationMaster in Elasticsearch
        verify(mockApplicationMasterSearchRepository, times(1)).save(testApplicationMaster);
    }

    @Test
    @Transactional
    public void createApplicationMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = applicationMasterRepository.findAll().size();

        // Create the ApplicationMaster with an existing ID
        applicationMaster.setId(1L);
        ApplicationMasterDTO applicationMasterDTO = applicationMasterMapper.toDto(applicationMaster);

        // An entity with an existing ID cannot be created, so this API call must fail
        restApplicationMasterMockMvc.perform(post("/api/application-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(applicationMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ApplicationMaster in the database
        List<ApplicationMaster> applicationMasterList = applicationMasterRepository.findAll();
        assertThat(applicationMasterList).hasSize(databaseSizeBeforeCreate);

        // Validate the ApplicationMaster in Elasticsearch
        verify(mockApplicationMasterSearchRepository, times(0)).save(applicationMaster);
    }


    @Test
    @Transactional
    public void checkApplicationNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = applicationMasterRepository.findAll().size();
        // set the field null
        applicationMaster.setApplicationName(null);

        // Create the ApplicationMaster, which fails.
        ApplicationMasterDTO applicationMasterDTO = applicationMasterMapper.toDto(applicationMaster);

        restApplicationMasterMockMvc.perform(post("/api/application-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(applicationMasterDTO)))
            .andExpect(status().isBadRequest());

        List<ApplicationMaster> applicationMasterList = applicationMasterRepository.findAll();
        assertThat(applicationMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllApplicationMasters() throws Exception {
        // Initialize the database
        applicationMasterRepository.saveAndFlush(applicationMaster);

        // Get all the applicationMasterList
        restApplicationMasterMockMvc.perform(get("/api/application-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(applicationMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].applicationName").value(hasItem(DEFAULT_APPLICATION_NAME)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].applicationURL").value(hasItem(DEFAULT_APPLICATION_URL)))
            .andExpect(jsonPath("$.[*].applicationStatus").value(hasItem(DEFAULT_APPLICATION_STATUS)))
            .andExpect(jsonPath("$.[*].applicationIdCode").value(hasItem(DEFAULT_APPLICATION_ID_CODE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].certPublicKey").value(hasItem(DEFAULT_CERT_PUBLIC_KEY)));
    }
    
    @Test
    @Transactional
    public void getApplicationMaster() throws Exception {
        // Initialize the database
        applicationMasterRepository.saveAndFlush(applicationMaster);

        // Get the applicationMaster
        restApplicationMasterMockMvc.perform(get("/api/application-masters/{id}", applicationMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(applicationMaster.getId().intValue()))
            .andExpect(jsonPath("$.applicationName").value(DEFAULT_APPLICATION_NAME))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.lastUpdatedBy").value(DEFAULT_LAST_UPDATED_BY))
            .andExpect(jsonPath("$.lastUpdatedOn").value(DEFAULT_LAST_UPDATED_ON.toString()))
            .andExpect(jsonPath("$.verifiedBy").value(DEFAULT_VERIFIED_BY))
            .andExpect(jsonPath("$.verifiedOn").value(DEFAULT_VERIFIED_ON.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS))
            .andExpect(jsonPath("$.applicationURL").value(DEFAULT_APPLICATION_URL))
            .andExpect(jsonPath("$.applicationStatus").value(DEFAULT_APPLICATION_STATUS))
            .andExpect(jsonPath("$.applicationIdCode").value(DEFAULT_APPLICATION_ID_CODE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.certPublicKey").value(DEFAULT_CERT_PUBLIC_KEY));
    }

    @Test
    @Transactional
    public void getNonExistingApplicationMaster() throws Exception {
        // Get the applicationMaster
        restApplicationMasterMockMvc.perform(get("/api/application-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateApplicationMaster() throws Exception {
        // Initialize the database
        applicationMasterRepository.saveAndFlush(applicationMaster);

        int databaseSizeBeforeUpdate = applicationMasterRepository.findAll().size();

        // Update the applicationMaster
        ApplicationMaster updatedApplicationMaster = applicationMasterRepository.findById(applicationMaster.getId()).get();
        // Disconnect from session so that the updates on updatedApplicationMaster are not directly saved in db
        em.detach(updatedApplicationMaster);
        updatedApplicationMaster
            .applicationName(UPDATED_APPLICATION_NAME)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS)
            .applicationURL(UPDATED_APPLICATION_URL)
            .applicationStatus(UPDATED_APPLICATION_STATUS)
            .applicationIdCode(UPDATED_APPLICATION_ID_CODE)
            .status(UPDATED_STATUS)
            .certPublicKey(UPDATED_CERT_PUBLIC_KEY);
        ApplicationMasterDTO applicationMasterDTO = applicationMasterMapper.toDto(updatedApplicationMaster);

        restApplicationMasterMockMvc.perform(put("/api/application-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(applicationMasterDTO)))
            .andExpect(status().isOk());

        // Validate the ApplicationMaster in the database
        List<ApplicationMaster> applicationMasterList = applicationMasterRepository.findAll();
        assertThat(applicationMasterList).hasSize(databaseSizeBeforeUpdate);
        ApplicationMaster testApplicationMaster = applicationMasterList.get(applicationMasterList.size() - 1);
        assertThat(testApplicationMaster.getApplicationName()).isEqualTo(UPDATED_APPLICATION_NAME);
        assertThat(testApplicationMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testApplicationMaster.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testApplicationMaster.getLastUpdatedBy()).isEqualTo(UPDATED_LAST_UPDATED_BY);
        assertThat(testApplicationMaster.getLastUpdatedOn()).isEqualTo(UPDATED_LAST_UPDATED_ON);
        assertThat(testApplicationMaster.getVerifiedBy()).isEqualTo(UPDATED_VERIFIED_BY);
        assertThat(testApplicationMaster.getVerifiedOn()).isEqualTo(UPDATED_VERIFIED_ON);
        assertThat(testApplicationMaster.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testApplicationMaster.getApplicationURL()).isEqualTo(UPDATED_APPLICATION_URL);
        assertThat(testApplicationMaster.getApplicationStatus()).isEqualTo(UPDATED_APPLICATION_STATUS);
        assertThat(testApplicationMaster.getApplicationIdCode()).isEqualTo(UPDATED_APPLICATION_ID_CODE);
        assertThat(testApplicationMaster.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testApplicationMaster.getCertPublicKey()).isEqualTo(UPDATED_CERT_PUBLIC_KEY);

        // Validate the ApplicationMaster in Elasticsearch
        verify(mockApplicationMasterSearchRepository, times(1)).save(testApplicationMaster);
    }

    @Test
    @Transactional
    public void updateNonExistingApplicationMaster() throws Exception {
        int databaseSizeBeforeUpdate = applicationMasterRepository.findAll().size();

        // Create the ApplicationMaster
        ApplicationMasterDTO applicationMasterDTO = applicationMasterMapper.toDto(applicationMaster);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restApplicationMasterMockMvc.perform(put("/api/application-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(applicationMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ApplicationMaster in the database
        List<ApplicationMaster> applicationMasterList = applicationMasterRepository.findAll();
        assertThat(applicationMasterList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ApplicationMaster in Elasticsearch
        verify(mockApplicationMasterSearchRepository, times(0)).save(applicationMaster);
    }

    @Test
    @Transactional
    public void deleteApplicationMaster() throws Exception {
        // Initialize the database
        applicationMasterRepository.saveAndFlush(applicationMaster);

        int databaseSizeBeforeDelete = applicationMasterRepository.findAll().size();

        // Delete the applicationMaster
        restApplicationMasterMockMvc.perform(delete("/api/application-masters/{id}", applicationMaster.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ApplicationMaster> applicationMasterList = applicationMasterRepository.findAll();
        assertThat(applicationMasterList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ApplicationMaster in Elasticsearch
        verify(mockApplicationMasterSearchRepository, times(1)).deleteById(applicationMaster.getId());
    }

    @Test
    @Transactional
    public void searchApplicationMaster() throws Exception {
        // Initialize the database
        applicationMasterRepository.saveAndFlush(applicationMaster);
        when(mockApplicationMasterSearchRepository.search(queryStringQuery("id:" + applicationMaster.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(applicationMaster), PageRequest.of(0, 1), 1));
        // Search the applicationMaster
        restApplicationMasterMockMvc.perform(get("/api/_search/application-masters?query=id:" + applicationMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(applicationMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].applicationName").value(hasItem(DEFAULT_APPLICATION_NAME)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].applicationURL").value(hasItem(DEFAULT_APPLICATION_URL)))
            .andExpect(jsonPath("$.[*].applicationStatus").value(hasItem(DEFAULT_APPLICATION_STATUS)))
            .andExpect(jsonPath("$.[*].applicationIdCode").value(hasItem(DEFAULT_APPLICATION_ID_CODE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].certPublicKey").value(hasItem(DEFAULT_CERT_PUBLIC_KEY)));
    }
}
