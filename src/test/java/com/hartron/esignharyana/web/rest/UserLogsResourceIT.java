package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.UserLogs;
import com.hartron.esignharyana.repository.UserLogsRepository;
import com.hartron.esignharyana.repository.search.UserLogsSearchRepository;
import com.hartron.esignharyana.service.UserLogsService;
import com.hartron.esignharyana.service.dto.UserLogsDTO;
import com.hartron.esignharyana.service.mapper.UserLogsMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;
import com.hartron.esignharyana.service.dto.UserLogsCriteria;
import com.hartron.esignharyana.service.UserLogsQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UserLogsResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class UserLogsResourceIT {

    private static final String DEFAULT_ACTION_TAKEN = "AAAAAAAAAA";
    private static final String UPDATED_ACTION_TAKEN = "BBBBBBBBBB";

    private static final String DEFAULT_ACTION_TAKEN_BY = "AAAAAAAAAA";
    private static final String UPDATED_ACTION_TAKEN_BY = "BBBBBBBBBB";

    private static final Long DEFAULT_ACTION_TAKEN_ON_USER = 1L;
    private static final Long UPDATED_ACTION_TAKEN_ON_USER = 2L;
    private static final Long SMALLER_ACTION_TAKEN_ON_USER = 1L - 1L;

    private static final LocalDate DEFAULT_ACTION_TAKEN_ON_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ACTION_TAKEN_ON_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_ACTION_TAKEN_ON_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    @Autowired
    private UserLogsRepository userLogsRepository;

    @Autowired
    private UserLogsMapper userLogsMapper;

    @Autowired
    private UserLogsService userLogsService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.UserLogsSearchRepositoryMockConfiguration
     */
    @Autowired
    private UserLogsSearchRepository mockUserLogsSearchRepository;

    @Autowired
    private UserLogsQueryService userLogsQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserLogsMockMvc;

    private UserLogs userLogs;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserLogsResource userLogsResource = new UserLogsResource(userLogsService, userLogsQueryService);
        this.restUserLogsMockMvc = MockMvcBuilders.standaloneSetup(userLogsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserLogs createEntity(EntityManager em) {
        UserLogs userLogs = new UserLogs()
            .actionTaken(DEFAULT_ACTION_TAKEN)
            .actionTakenBy(DEFAULT_ACTION_TAKEN_BY)
            .actionTakenOnUser(DEFAULT_ACTION_TAKEN_ON_USER)
            .actionTakenOnDate(DEFAULT_ACTION_TAKEN_ON_DATE)
            .remarks(DEFAULT_REMARKS);
        return userLogs;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserLogs createUpdatedEntity(EntityManager em) {
        UserLogs userLogs = new UserLogs()
            .actionTaken(UPDATED_ACTION_TAKEN)
            .actionTakenBy(UPDATED_ACTION_TAKEN_BY)
            .actionTakenOnUser(UPDATED_ACTION_TAKEN_ON_USER)
            .actionTakenOnDate(UPDATED_ACTION_TAKEN_ON_DATE)
            .remarks(UPDATED_REMARKS);
        return userLogs;
    }

    @BeforeEach
    public void initTest() {
        userLogs = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserLogs() throws Exception {
        int databaseSizeBeforeCreate = userLogsRepository.findAll().size();

        // Create the UserLogs
        UserLogsDTO userLogsDTO = userLogsMapper.toDto(userLogs);
        restUserLogsMockMvc.perform(post("/api/user-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userLogsDTO)))
            .andExpect(status().isCreated());

        // Validate the UserLogs in the database
        List<UserLogs> userLogsList = userLogsRepository.findAll();
        assertThat(userLogsList).hasSize(databaseSizeBeforeCreate + 1);
        UserLogs testUserLogs = userLogsList.get(userLogsList.size() - 1);
        assertThat(testUserLogs.getActionTaken()).isEqualTo(DEFAULT_ACTION_TAKEN);
        assertThat(testUserLogs.getActionTakenBy()).isEqualTo(DEFAULT_ACTION_TAKEN_BY);
        assertThat(testUserLogs.getActionTakenOnUser()).isEqualTo(DEFAULT_ACTION_TAKEN_ON_USER);
        assertThat(testUserLogs.getActionTakenOnDate()).isEqualTo(DEFAULT_ACTION_TAKEN_ON_DATE);
        assertThat(testUserLogs.getRemarks()).isEqualTo(DEFAULT_REMARKS);

        // Validate the UserLogs in Elasticsearch
        verify(mockUserLogsSearchRepository, times(1)).save(testUserLogs);
    }

    @Test
    @Transactional
    public void createUserLogsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userLogsRepository.findAll().size();

        // Create the UserLogs with an existing ID
        userLogs.setId(1L);
        UserLogsDTO userLogsDTO = userLogsMapper.toDto(userLogs);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserLogsMockMvc.perform(post("/api/user-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userLogsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserLogs in the database
        List<UserLogs> userLogsList = userLogsRepository.findAll();
        assertThat(userLogsList).hasSize(databaseSizeBeforeCreate);

        // Validate the UserLogs in Elasticsearch
        verify(mockUserLogsSearchRepository, times(0)).save(userLogs);
    }


    @Test
    @Transactional
    public void getAllUserLogs() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList
        restUserLogsMockMvc.perform(get("/api/user-logs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userLogs.getId().intValue())))
            .andExpect(jsonPath("$.[*].actionTaken").value(hasItem(DEFAULT_ACTION_TAKEN)))
            .andExpect(jsonPath("$.[*].actionTakenBy").value(hasItem(DEFAULT_ACTION_TAKEN_BY)))
            .andExpect(jsonPath("$.[*].actionTakenOnUser").value(hasItem(DEFAULT_ACTION_TAKEN_ON_USER.intValue())))
            .andExpect(jsonPath("$.[*].actionTakenOnDate").value(hasItem(DEFAULT_ACTION_TAKEN_ON_DATE.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
    
    @Test
    @Transactional
    public void getUserLogs() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get the userLogs
        restUserLogsMockMvc.perform(get("/api/user-logs/{id}", userLogs.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userLogs.getId().intValue()))
            .andExpect(jsonPath("$.actionTaken").value(DEFAULT_ACTION_TAKEN))
            .andExpect(jsonPath("$.actionTakenBy").value(DEFAULT_ACTION_TAKEN_BY))
            .andExpect(jsonPath("$.actionTakenOnUser").value(DEFAULT_ACTION_TAKEN_ON_USER.intValue()))
            .andExpect(jsonPath("$.actionTakenOnDate").value(DEFAULT_ACTION_TAKEN_ON_DATE.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS));
    }


    @Test
    @Transactional
    public void getUserLogsByIdFiltering() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        Long id = userLogs.getId();

        defaultUserLogsShouldBeFound("id.equals=" + id);
        defaultUserLogsShouldNotBeFound("id.notEquals=" + id);

        defaultUserLogsShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultUserLogsShouldNotBeFound("id.greaterThan=" + id);

        defaultUserLogsShouldBeFound("id.lessThanOrEqual=" + id);
        defaultUserLogsShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllUserLogsByActionTakenIsEqualToSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTaken equals to DEFAULT_ACTION_TAKEN
        defaultUserLogsShouldBeFound("actionTaken.equals=" + DEFAULT_ACTION_TAKEN);

        // Get all the userLogsList where actionTaken equals to UPDATED_ACTION_TAKEN
        defaultUserLogsShouldNotBeFound("actionTaken.equals=" + UPDATED_ACTION_TAKEN);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTaken not equals to DEFAULT_ACTION_TAKEN
        defaultUserLogsShouldNotBeFound("actionTaken.notEquals=" + DEFAULT_ACTION_TAKEN);

        // Get all the userLogsList where actionTaken not equals to UPDATED_ACTION_TAKEN
        defaultUserLogsShouldBeFound("actionTaken.notEquals=" + UPDATED_ACTION_TAKEN);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenIsInShouldWork() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTaken in DEFAULT_ACTION_TAKEN or UPDATED_ACTION_TAKEN
        defaultUserLogsShouldBeFound("actionTaken.in=" + DEFAULT_ACTION_TAKEN + "," + UPDATED_ACTION_TAKEN);

        // Get all the userLogsList where actionTaken equals to UPDATED_ACTION_TAKEN
        defaultUserLogsShouldNotBeFound("actionTaken.in=" + UPDATED_ACTION_TAKEN);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenIsNullOrNotNull() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTaken is not null
        defaultUserLogsShouldBeFound("actionTaken.specified=true");

        // Get all the userLogsList where actionTaken is null
        defaultUserLogsShouldNotBeFound("actionTaken.specified=false");
    }
                @Test
    @Transactional
    public void getAllUserLogsByActionTakenContainsSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTaken contains DEFAULT_ACTION_TAKEN
        defaultUserLogsShouldBeFound("actionTaken.contains=" + DEFAULT_ACTION_TAKEN);

        // Get all the userLogsList where actionTaken contains UPDATED_ACTION_TAKEN
        defaultUserLogsShouldNotBeFound("actionTaken.contains=" + UPDATED_ACTION_TAKEN);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenNotContainsSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTaken does not contain DEFAULT_ACTION_TAKEN
        defaultUserLogsShouldNotBeFound("actionTaken.doesNotContain=" + DEFAULT_ACTION_TAKEN);

        // Get all the userLogsList where actionTaken does not contain UPDATED_ACTION_TAKEN
        defaultUserLogsShouldBeFound("actionTaken.doesNotContain=" + UPDATED_ACTION_TAKEN);
    }


    @Test
    @Transactional
    public void getAllUserLogsByActionTakenByIsEqualToSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenBy equals to DEFAULT_ACTION_TAKEN_BY
        defaultUserLogsShouldBeFound("actionTakenBy.equals=" + DEFAULT_ACTION_TAKEN_BY);

        // Get all the userLogsList where actionTakenBy equals to UPDATED_ACTION_TAKEN_BY
        defaultUserLogsShouldNotBeFound("actionTakenBy.equals=" + UPDATED_ACTION_TAKEN_BY);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenBy not equals to DEFAULT_ACTION_TAKEN_BY
        defaultUserLogsShouldNotBeFound("actionTakenBy.notEquals=" + DEFAULT_ACTION_TAKEN_BY);

        // Get all the userLogsList where actionTakenBy not equals to UPDATED_ACTION_TAKEN_BY
        defaultUserLogsShouldBeFound("actionTakenBy.notEquals=" + UPDATED_ACTION_TAKEN_BY);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenByIsInShouldWork() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenBy in DEFAULT_ACTION_TAKEN_BY or UPDATED_ACTION_TAKEN_BY
        defaultUserLogsShouldBeFound("actionTakenBy.in=" + DEFAULT_ACTION_TAKEN_BY + "," + UPDATED_ACTION_TAKEN_BY);

        // Get all the userLogsList where actionTakenBy equals to UPDATED_ACTION_TAKEN_BY
        defaultUserLogsShouldNotBeFound("actionTakenBy.in=" + UPDATED_ACTION_TAKEN_BY);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenByIsNullOrNotNull() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenBy is not null
        defaultUserLogsShouldBeFound("actionTakenBy.specified=true");

        // Get all the userLogsList where actionTakenBy is null
        defaultUserLogsShouldNotBeFound("actionTakenBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllUserLogsByActionTakenByContainsSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenBy contains DEFAULT_ACTION_TAKEN_BY
        defaultUserLogsShouldBeFound("actionTakenBy.contains=" + DEFAULT_ACTION_TAKEN_BY);

        // Get all the userLogsList where actionTakenBy contains UPDATED_ACTION_TAKEN_BY
        defaultUserLogsShouldNotBeFound("actionTakenBy.contains=" + UPDATED_ACTION_TAKEN_BY);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenByNotContainsSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenBy does not contain DEFAULT_ACTION_TAKEN_BY
        defaultUserLogsShouldNotBeFound("actionTakenBy.doesNotContain=" + DEFAULT_ACTION_TAKEN_BY);

        // Get all the userLogsList where actionTakenBy does not contain UPDATED_ACTION_TAKEN_BY
        defaultUserLogsShouldBeFound("actionTakenBy.doesNotContain=" + UPDATED_ACTION_TAKEN_BY);
    }


    @Test
    @Transactional
    public void getAllUserLogsByActionTakenOnUserIsEqualToSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenOnUser equals to DEFAULT_ACTION_TAKEN_ON_USER
        defaultUserLogsShouldBeFound("actionTakenOnUser.equals=" + DEFAULT_ACTION_TAKEN_ON_USER);

        // Get all the userLogsList where actionTakenOnUser equals to UPDATED_ACTION_TAKEN_ON_USER
        defaultUserLogsShouldNotBeFound("actionTakenOnUser.equals=" + UPDATED_ACTION_TAKEN_ON_USER);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenOnUserIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenOnUser not equals to DEFAULT_ACTION_TAKEN_ON_USER
        defaultUserLogsShouldNotBeFound("actionTakenOnUser.notEquals=" + DEFAULT_ACTION_TAKEN_ON_USER);

        // Get all the userLogsList where actionTakenOnUser not equals to UPDATED_ACTION_TAKEN_ON_USER
        defaultUserLogsShouldBeFound("actionTakenOnUser.notEquals=" + UPDATED_ACTION_TAKEN_ON_USER);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenOnUserIsInShouldWork() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenOnUser in DEFAULT_ACTION_TAKEN_ON_USER or UPDATED_ACTION_TAKEN_ON_USER
        defaultUserLogsShouldBeFound("actionTakenOnUser.in=" + DEFAULT_ACTION_TAKEN_ON_USER + "," + UPDATED_ACTION_TAKEN_ON_USER);

        // Get all the userLogsList where actionTakenOnUser equals to UPDATED_ACTION_TAKEN_ON_USER
        defaultUserLogsShouldNotBeFound("actionTakenOnUser.in=" + UPDATED_ACTION_TAKEN_ON_USER);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenOnUserIsNullOrNotNull() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenOnUser is not null
        defaultUserLogsShouldBeFound("actionTakenOnUser.specified=true");

        // Get all the userLogsList where actionTakenOnUser is null
        defaultUserLogsShouldNotBeFound("actionTakenOnUser.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenOnUserIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenOnUser is greater than or equal to DEFAULT_ACTION_TAKEN_ON_USER
        defaultUserLogsShouldBeFound("actionTakenOnUser.greaterThanOrEqual=" + DEFAULT_ACTION_TAKEN_ON_USER);

        // Get all the userLogsList where actionTakenOnUser is greater than or equal to UPDATED_ACTION_TAKEN_ON_USER
        defaultUserLogsShouldNotBeFound("actionTakenOnUser.greaterThanOrEqual=" + UPDATED_ACTION_TAKEN_ON_USER);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenOnUserIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenOnUser is less than or equal to DEFAULT_ACTION_TAKEN_ON_USER
        defaultUserLogsShouldBeFound("actionTakenOnUser.lessThanOrEqual=" + DEFAULT_ACTION_TAKEN_ON_USER);

        // Get all the userLogsList where actionTakenOnUser is less than or equal to SMALLER_ACTION_TAKEN_ON_USER
        defaultUserLogsShouldNotBeFound("actionTakenOnUser.lessThanOrEqual=" + SMALLER_ACTION_TAKEN_ON_USER);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenOnUserIsLessThanSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenOnUser is less than DEFAULT_ACTION_TAKEN_ON_USER
        defaultUserLogsShouldNotBeFound("actionTakenOnUser.lessThan=" + DEFAULT_ACTION_TAKEN_ON_USER);

        // Get all the userLogsList where actionTakenOnUser is less than UPDATED_ACTION_TAKEN_ON_USER
        defaultUserLogsShouldBeFound("actionTakenOnUser.lessThan=" + UPDATED_ACTION_TAKEN_ON_USER);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenOnUserIsGreaterThanSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenOnUser is greater than DEFAULT_ACTION_TAKEN_ON_USER
        defaultUserLogsShouldNotBeFound("actionTakenOnUser.greaterThan=" + DEFAULT_ACTION_TAKEN_ON_USER);

        // Get all the userLogsList where actionTakenOnUser is greater than SMALLER_ACTION_TAKEN_ON_USER
        defaultUserLogsShouldBeFound("actionTakenOnUser.greaterThan=" + SMALLER_ACTION_TAKEN_ON_USER);
    }


    @Test
    @Transactional
    public void getAllUserLogsByActionTakenOnDateIsEqualToSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenOnDate equals to DEFAULT_ACTION_TAKEN_ON_DATE
        defaultUserLogsShouldBeFound("actionTakenOnDate.equals=" + DEFAULT_ACTION_TAKEN_ON_DATE);

        // Get all the userLogsList where actionTakenOnDate equals to UPDATED_ACTION_TAKEN_ON_DATE
        defaultUserLogsShouldNotBeFound("actionTakenOnDate.equals=" + UPDATED_ACTION_TAKEN_ON_DATE);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenOnDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenOnDate not equals to DEFAULT_ACTION_TAKEN_ON_DATE
        defaultUserLogsShouldNotBeFound("actionTakenOnDate.notEquals=" + DEFAULT_ACTION_TAKEN_ON_DATE);

        // Get all the userLogsList where actionTakenOnDate not equals to UPDATED_ACTION_TAKEN_ON_DATE
        defaultUserLogsShouldBeFound("actionTakenOnDate.notEquals=" + UPDATED_ACTION_TAKEN_ON_DATE);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenOnDateIsInShouldWork() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenOnDate in DEFAULT_ACTION_TAKEN_ON_DATE or UPDATED_ACTION_TAKEN_ON_DATE
        defaultUserLogsShouldBeFound("actionTakenOnDate.in=" + DEFAULT_ACTION_TAKEN_ON_DATE + "," + UPDATED_ACTION_TAKEN_ON_DATE);

        // Get all the userLogsList where actionTakenOnDate equals to UPDATED_ACTION_TAKEN_ON_DATE
        defaultUserLogsShouldNotBeFound("actionTakenOnDate.in=" + UPDATED_ACTION_TAKEN_ON_DATE);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenOnDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenOnDate is not null
        defaultUserLogsShouldBeFound("actionTakenOnDate.specified=true");

        // Get all the userLogsList where actionTakenOnDate is null
        defaultUserLogsShouldNotBeFound("actionTakenOnDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenOnDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenOnDate is greater than or equal to DEFAULT_ACTION_TAKEN_ON_DATE
        defaultUserLogsShouldBeFound("actionTakenOnDate.greaterThanOrEqual=" + DEFAULT_ACTION_TAKEN_ON_DATE);

        // Get all the userLogsList where actionTakenOnDate is greater than or equal to UPDATED_ACTION_TAKEN_ON_DATE
        defaultUserLogsShouldNotBeFound("actionTakenOnDate.greaterThanOrEqual=" + UPDATED_ACTION_TAKEN_ON_DATE);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenOnDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenOnDate is less than or equal to DEFAULT_ACTION_TAKEN_ON_DATE
        defaultUserLogsShouldBeFound("actionTakenOnDate.lessThanOrEqual=" + DEFAULT_ACTION_TAKEN_ON_DATE);

        // Get all the userLogsList where actionTakenOnDate is less than or equal to SMALLER_ACTION_TAKEN_ON_DATE
        defaultUserLogsShouldNotBeFound("actionTakenOnDate.lessThanOrEqual=" + SMALLER_ACTION_TAKEN_ON_DATE);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenOnDateIsLessThanSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenOnDate is less than DEFAULT_ACTION_TAKEN_ON_DATE
        defaultUserLogsShouldNotBeFound("actionTakenOnDate.lessThan=" + DEFAULT_ACTION_TAKEN_ON_DATE);

        // Get all the userLogsList where actionTakenOnDate is less than UPDATED_ACTION_TAKEN_ON_DATE
        defaultUserLogsShouldBeFound("actionTakenOnDate.lessThan=" + UPDATED_ACTION_TAKEN_ON_DATE);
    }

    @Test
    @Transactional
    public void getAllUserLogsByActionTakenOnDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where actionTakenOnDate is greater than DEFAULT_ACTION_TAKEN_ON_DATE
        defaultUserLogsShouldNotBeFound("actionTakenOnDate.greaterThan=" + DEFAULT_ACTION_TAKEN_ON_DATE);

        // Get all the userLogsList where actionTakenOnDate is greater than SMALLER_ACTION_TAKEN_ON_DATE
        defaultUserLogsShouldBeFound("actionTakenOnDate.greaterThan=" + SMALLER_ACTION_TAKEN_ON_DATE);
    }


    @Test
    @Transactional
    public void getAllUserLogsByRemarksIsEqualToSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where remarks equals to DEFAULT_REMARKS
        defaultUserLogsShouldBeFound("remarks.equals=" + DEFAULT_REMARKS);

        // Get all the userLogsList where remarks equals to UPDATED_REMARKS
        defaultUserLogsShouldNotBeFound("remarks.equals=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllUserLogsByRemarksIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where remarks not equals to DEFAULT_REMARKS
        defaultUserLogsShouldNotBeFound("remarks.notEquals=" + DEFAULT_REMARKS);

        // Get all the userLogsList where remarks not equals to UPDATED_REMARKS
        defaultUserLogsShouldBeFound("remarks.notEquals=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllUserLogsByRemarksIsInShouldWork() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where remarks in DEFAULT_REMARKS or UPDATED_REMARKS
        defaultUserLogsShouldBeFound("remarks.in=" + DEFAULT_REMARKS + "," + UPDATED_REMARKS);

        // Get all the userLogsList where remarks equals to UPDATED_REMARKS
        defaultUserLogsShouldNotBeFound("remarks.in=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllUserLogsByRemarksIsNullOrNotNull() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where remarks is not null
        defaultUserLogsShouldBeFound("remarks.specified=true");

        // Get all the userLogsList where remarks is null
        defaultUserLogsShouldNotBeFound("remarks.specified=false");
    }
                @Test
    @Transactional
    public void getAllUserLogsByRemarksContainsSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where remarks contains DEFAULT_REMARKS
        defaultUserLogsShouldBeFound("remarks.contains=" + DEFAULT_REMARKS);

        // Get all the userLogsList where remarks contains UPDATED_REMARKS
        defaultUserLogsShouldNotBeFound("remarks.contains=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllUserLogsByRemarksNotContainsSomething() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        // Get all the userLogsList where remarks does not contain DEFAULT_REMARKS
        defaultUserLogsShouldNotBeFound("remarks.doesNotContain=" + DEFAULT_REMARKS);

        // Get all the userLogsList where remarks does not contain UPDATED_REMARKS
        defaultUserLogsShouldBeFound("remarks.doesNotContain=" + UPDATED_REMARKS);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUserLogsShouldBeFound(String filter) throws Exception {
        restUserLogsMockMvc.perform(get("/api/user-logs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userLogs.getId().intValue())))
            .andExpect(jsonPath("$.[*].actionTaken").value(hasItem(DEFAULT_ACTION_TAKEN)))
            .andExpect(jsonPath("$.[*].actionTakenBy").value(hasItem(DEFAULT_ACTION_TAKEN_BY)))
            .andExpect(jsonPath("$.[*].actionTakenOnUser").value(hasItem(DEFAULT_ACTION_TAKEN_ON_USER.intValue())))
            .andExpect(jsonPath("$.[*].actionTakenOnDate").value(hasItem(DEFAULT_ACTION_TAKEN_ON_DATE.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));

        // Check, that the count call also returns 1
        restUserLogsMockMvc.perform(get("/api/user-logs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUserLogsShouldNotBeFound(String filter) throws Exception {
        restUserLogsMockMvc.perform(get("/api/user-logs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUserLogsMockMvc.perform(get("/api/user-logs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingUserLogs() throws Exception {
        // Get the userLogs
        restUserLogsMockMvc.perform(get("/api/user-logs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserLogs() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        int databaseSizeBeforeUpdate = userLogsRepository.findAll().size();

        // Update the userLogs
        UserLogs updatedUserLogs = userLogsRepository.findById(userLogs.getId()).get();
        // Disconnect from session so that the updates on updatedUserLogs are not directly saved in db
        em.detach(updatedUserLogs);
        updatedUserLogs
            .actionTaken(UPDATED_ACTION_TAKEN)
            .actionTakenBy(UPDATED_ACTION_TAKEN_BY)
            .actionTakenOnUser(UPDATED_ACTION_TAKEN_ON_USER)
            .actionTakenOnDate(UPDATED_ACTION_TAKEN_ON_DATE)
            .remarks(UPDATED_REMARKS);
        UserLogsDTO userLogsDTO = userLogsMapper.toDto(updatedUserLogs);

        restUserLogsMockMvc.perform(put("/api/user-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userLogsDTO)))
            .andExpect(status().isOk());

        // Validate the UserLogs in the database
        List<UserLogs> userLogsList = userLogsRepository.findAll();
        assertThat(userLogsList).hasSize(databaseSizeBeforeUpdate);
        UserLogs testUserLogs = userLogsList.get(userLogsList.size() - 1);
        assertThat(testUserLogs.getActionTaken()).isEqualTo(UPDATED_ACTION_TAKEN);
        assertThat(testUserLogs.getActionTakenBy()).isEqualTo(UPDATED_ACTION_TAKEN_BY);
        assertThat(testUserLogs.getActionTakenOnUser()).isEqualTo(UPDATED_ACTION_TAKEN_ON_USER);
        assertThat(testUserLogs.getActionTakenOnDate()).isEqualTo(UPDATED_ACTION_TAKEN_ON_DATE);
        assertThat(testUserLogs.getRemarks()).isEqualTo(UPDATED_REMARKS);

        // Validate the UserLogs in Elasticsearch
        verify(mockUserLogsSearchRepository, times(1)).save(testUserLogs);
    }

    @Test
    @Transactional
    public void updateNonExistingUserLogs() throws Exception {
        int databaseSizeBeforeUpdate = userLogsRepository.findAll().size();

        // Create the UserLogs
        UserLogsDTO userLogsDTO = userLogsMapper.toDto(userLogs);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserLogsMockMvc.perform(put("/api/user-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userLogsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserLogs in the database
        List<UserLogs> userLogsList = userLogsRepository.findAll();
        assertThat(userLogsList).hasSize(databaseSizeBeforeUpdate);

        // Validate the UserLogs in Elasticsearch
        verify(mockUserLogsSearchRepository, times(0)).save(userLogs);
    }

    @Test
    @Transactional
    public void deleteUserLogs() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);

        int databaseSizeBeforeDelete = userLogsRepository.findAll().size();

        // Delete the userLogs
        restUserLogsMockMvc.perform(delete("/api/user-logs/{id}", userLogs.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserLogs> userLogsList = userLogsRepository.findAll();
        assertThat(userLogsList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the UserLogs in Elasticsearch
        verify(mockUserLogsSearchRepository, times(1)).deleteById(userLogs.getId());
    }

    @Test
    @Transactional
    public void searchUserLogs() throws Exception {
        // Initialize the database
        userLogsRepository.saveAndFlush(userLogs);
        when(mockUserLogsSearchRepository.search(queryStringQuery("id:" + userLogs.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(userLogs), PageRequest.of(0, 1), 1));
        // Search the userLogs
        restUserLogsMockMvc.perform(get("/api/_search/user-logs?query=id:" + userLogs.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userLogs.getId().intValue())))
            .andExpect(jsonPath("$.[*].actionTaken").value(hasItem(DEFAULT_ACTION_TAKEN)))
            .andExpect(jsonPath("$.[*].actionTakenBy").value(hasItem(DEFAULT_ACTION_TAKEN_BY)))
            .andExpect(jsonPath("$.[*].actionTakenOnUser").value(hasItem(DEFAULT_ACTION_TAKEN_ON_USER.intValue())))
            .andExpect(jsonPath("$.[*].actionTakenOnDate").value(hasItem(DEFAULT_ACTION_TAKEN_ON_DATE.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
}
