package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.UserApplication;
import com.hartron.esignharyana.domain.ApplicationMaster;
import com.hartron.esignharyana.repository.UserApplicationRepository;
import com.hartron.esignharyana.repository.search.UserApplicationSearchRepository;
import com.hartron.esignharyana.service.UserApplicationService;
import com.hartron.esignharyana.service.dto.UserApplicationDTO;
import com.hartron.esignharyana.service.mapper.UserApplicationMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;
import com.hartron.esignharyana.service.dto.UserApplicationCriteria;
import com.hartron.esignharyana.service.UserApplicationQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UserApplicationResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class UserApplicationResourceIT {

    private static final Long DEFAULT_USER_ID = 1L;
    private static final Long UPDATED_USER_ID = 2L;
    private static final Long SMALLER_USER_ID = 1L - 1L;

    private static final String DEFAULT_USER_LOGIN = "AAAAAAAAAA";
    private static final String UPDATED_USER_LOGIN = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_ON = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CREATED_ON = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_UPDATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_ON = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_UPDATED_ON = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_VERIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_VERIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_VERIFIED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VERIFIED_ON = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_VERIFIED_ON = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_USER_CODE_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_CODE_ID = "BBBBBBBBBB";

    private static final String DEFAULT_APPLICATION_CODE_ID = "AAAAAAAAAA";
    private static final String UPDATED_APPLICATION_CODE_ID = "BBBBBBBBBB";

    @Autowired
    private UserApplicationRepository userApplicationRepository;

    @Autowired
    private UserApplicationMapper userApplicationMapper;

    @Autowired
    private UserApplicationService userApplicationService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.UserApplicationSearchRepositoryMockConfiguration
     */
    @Autowired
    private UserApplicationSearchRepository mockUserApplicationSearchRepository;

    @Autowired
    private UserApplicationQueryService userApplicationQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserApplicationMockMvc;

    private UserApplication userApplication;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserApplicationResource userApplicationResource = new UserApplicationResource(userApplicationService, userApplicationQueryService);
        this.restUserApplicationMockMvc = MockMvcBuilders.standaloneSetup(userApplicationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserApplication createEntity(EntityManager em) {
        UserApplication userApplication = new UserApplication()
            .userId(DEFAULT_USER_ID)
            .userLogin(DEFAULT_USER_LOGIN)
            .createdBy(DEFAULT_CREATED_BY)
            .createdOn(DEFAULT_CREATED_ON)
            .updatedBy(DEFAULT_UPDATED_BY)
            .updatedOn(DEFAULT_UPDATED_ON)
            .verifiedBy(DEFAULT_VERIFIED_BY)
            .verifiedOn(DEFAULT_VERIFIED_ON)
            .userCodeId(DEFAULT_USER_CODE_ID)
            .applicationCodeId(DEFAULT_APPLICATION_CODE_ID);
        return userApplication;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserApplication createUpdatedEntity(EntityManager em) {
        UserApplication userApplication = new UserApplication()
            .userId(UPDATED_USER_ID)
            .userLogin(UPDATED_USER_LOGIN)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .updatedBy(UPDATED_UPDATED_BY)
            .updatedOn(UPDATED_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .userCodeId(UPDATED_USER_CODE_ID)
            .applicationCodeId(UPDATED_APPLICATION_CODE_ID);
        return userApplication;
    }

    @BeforeEach
    public void initTest() {
        userApplication = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserApplication() throws Exception {
        int databaseSizeBeforeCreate = userApplicationRepository.findAll().size();

        // Create the UserApplication
        UserApplicationDTO userApplicationDTO = userApplicationMapper.toDto(userApplication);
        restUserApplicationMockMvc.perform(post("/api/user-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userApplicationDTO)))
            .andExpect(status().isCreated());

        // Validate the UserApplication in the database
        List<UserApplication> userApplicationList = userApplicationRepository.findAll();
        assertThat(userApplicationList).hasSize(databaseSizeBeforeCreate + 1);
        UserApplication testUserApplication = userApplicationList.get(userApplicationList.size() - 1);
        assertThat(testUserApplication.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testUserApplication.getUserLogin()).isEqualTo(DEFAULT_USER_LOGIN);
        assertThat(testUserApplication.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testUserApplication.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testUserApplication.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testUserApplication.getUpdatedOn()).isEqualTo(DEFAULT_UPDATED_ON);
        assertThat(testUserApplication.getVerifiedBy()).isEqualTo(DEFAULT_VERIFIED_BY);
        assertThat(testUserApplication.getVerifiedOn()).isEqualTo(DEFAULT_VERIFIED_ON);
        assertThat(testUserApplication.getUserCodeId()).isEqualTo(DEFAULT_USER_CODE_ID);
        assertThat(testUserApplication.getApplicationCodeId()).isEqualTo(DEFAULT_APPLICATION_CODE_ID);

        // Validate the UserApplication in Elasticsearch
        verify(mockUserApplicationSearchRepository, times(1)).save(testUserApplication);
    }

    @Test
    @Transactional
    public void createUserApplicationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userApplicationRepository.findAll().size();

        // Create the UserApplication with an existing ID
        userApplication.setId(1L);
        UserApplicationDTO userApplicationDTO = userApplicationMapper.toDto(userApplication);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserApplicationMockMvc.perform(post("/api/user-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userApplicationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserApplication in the database
        List<UserApplication> userApplicationList = userApplicationRepository.findAll();
        assertThat(userApplicationList).hasSize(databaseSizeBeforeCreate);

        // Validate the UserApplication in Elasticsearch
        verify(mockUserApplicationSearchRepository, times(0)).save(userApplication);
    }


    @Test
    @Transactional
    public void getAllUserApplications() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList
        restUserApplicationMockMvc.perform(get("/api/user-applications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userApplication.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].userLogin").value(hasItem(DEFAULT_USER_LOGIN)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].updatedOn").value(hasItem(DEFAULT_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].userCodeId").value(hasItem(DEFAULT_USER_CODE_ID)))
            .andExpect(jsonPath("$.[*].applicationCodeId").value(hasItem(DEFAULT_APPLICATION_CODE_ID)));
    }
    
    @Test
    @Transactional
    public void getUserApplication() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get the userApplication
        restUserApplicationMockMvc.perform(get("/api/user-applications/{id}", userApplication.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userApplication.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID.intValue()))
            .andExpect(jsonPath("$.userLogin").value(DEFAULT_USER_LOGIN))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY))
            .andExpect(jsonPath("$.updatedOn").value(DEFAULT_UPDATED_ON.toString()))
            .andExpect(jsonPath("$.verifiedBy").value(DEFAULT_VERIFIED_BY))
            .andExpect(jsonPath("$.verifiedOn").value(DEFAULT_VERIFIED_ON.toString()))
            .andExpect(jsonPath("$.userCodeId").value(DEFAULT_USER_CODE_ID))
            .andExpect(jsonPath("$.applicationCodeId").value(DEFAULT_APPLICATION_CODE_ID));
    }


    @Test
    @Transactional
    public void getUserApplicationsByIdFiltering() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        Long id = userApplication.getId();

        defaultUserApplicationShouldBeFound("id.equals=" + id);
        defaultUserApplicationShouldNotBeFound("id.notEquals=" + id);

        defaultUserApplicationShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultUserApplicationShouldNotBeFound("id.greaterThan=" + id);

        defaultUserApplicationShouldBeFound("id.lessThanOrEqual=" + id);
        defaultUserApplicationShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllUserApplicationsByUserIdIsEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userId equals to DEFAULT_USER_ID
        defaultUserApplicationShouldBeFound("userId.equals=" + DEFAULT_USER_ID);

        // Get all the userApplicationList where userId equals to UPDATED_USER_ID
        defaultUserApplicationShouldNotBeFound("userId.equals=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUserIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userId not equals to DEFAULT_USER_ID
        defaultUserApplicationShouldNotBeFound("userId.notEquals=" + DEFAULT_USER_ID);

        // Get all the userApplicationList where userId not equals to UPDATED_USER_ID
        defaultUserApplicationShouldBeFound("userId.notEquals=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUserIdIsInShouldWork() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userId in DEFAULT_USER_ID or UPDATED_USER_ID
        defaultUserApplicationShouldBeFound("userId.in=" + DEFAULT_USER_ID + "," + UPDATED_USER_ID);

        // Get all the userApplicationList where userId equals to UPDATED_USER_ID
        defaultUserApplicationShouldNotBeFound("userId.in=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUserIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userId is not null
        defaultUserApplicationShouldBeFound("userId.specified=true");

        // Get all the userApplicationList where userId is null
        defaultUserApplicationShouldNotBeFound("userId.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUserIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userId is greater than or equal to DEFAULT_USER_ID
        defaultUserApplicationShouldBeFound("userId.greaterThanOrEqual=" + DEFAULT_USER_ID);

        // Get all the userApplicationList where userId is greater than or equal to UPDATED_USER_ID
        defaultUserApplicationShouldNotBeFound("userId.greaterThanOrEqual=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUserIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userId is less than or equal to DEFAULT_USER_ID
        defaultUserApplicationShouldBeFound("userId.lessThanOrEqual=" + DEFAULT_USER_ID);

        // Get all the userApplicationList where userId is less than or equal to SMALLER_USER_ID
        defaultUserApplicationShouldNotBeFound("userId.lessThanOrEqual=" + SMALLER_USER_ID);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUserIdIsLessThanSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userId is less than DEFAULT_USER_ID
        defaultUserApplicationShouldNotBeFound("userId.lessThan=" + DEFAULT_USER_ID);

        // Get all the userApplicationList where userId is less than UPDATED_USER_ID
        defaultUserApplicationShouldBeFound("userId.lessThan=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUserIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userId is greater than DEFAULT_USER_ID
        defaultUserApplicationShouldNotBeFound("userId.greaterThan=" + DEFAULT_USER_ID);

        // Get all the userApplicationList where userId is greater than SMALLER_USER_ID
        defaultUserApplicationShouldBeFound("userId.greaterThan=" + SMALLER_USER_ID);
    }


    @Test
    @Transactional
    public void getAllUserApplicationsByUserLoginIsEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userLogin equals to DEFAULT_USER_LOGIN
        defaultUserApplicationShouldBeFound("userLogin.equals=" + DEFAULT_USER_LOGIN);

        // Get all the userApplicationList where userLogin equals to UPDATED_USER_LOGIN
        defaultUserApplicationShouldNotBeFound("userLogin.equals=" + UPDATED_USER_LOGIN);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUserLoginIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userLogin not equals to DEFAULT_USER_LOGIN
        defaultUserApplicationShouldNotBeFound("userLogin.notEquals=" + DEFAULT_USER_LOGIN);

        // Get all the userApplicationList where userLogin not equals to UPDATED_USER_LOGIN
        defaultUserApplicationShouldBeFound("userLogin.notEquals=" + UPDATED_USER_LOGIN);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUserLoginIsInShouldWork() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userLogin in DEFAULT_USER_LOGIN or UPDATED_USER_LOGIN
        defaultUserApplicationShouldBeFound("userLogin.in=" + DEFAULT_USER_LOGIN + "," + UPDATED_USER_LOGIN);

        // Get all the userApplicationList where userLogin equals to UPDATED_USER_LOGIN
        defaultUserApplicationShouldNotBeFound("userLogin.in=" + UPDATED_USER_LOGIN);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUserLoginIsNullOrNotNull() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userLogin is not null
        defaultUserApplicationShouldBeFound("userLogin.specified=true");

        // Get all the userApplicationList where userLogin is null
        defaultUserApplicationShouldNotBeFound("userLogin.specified=false");
    }
                @Test
    @Transactional
    public void getAllUserApplicationsByUserLoginContainsSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userLogin contains DEFAULT_USER_LOGIN
        defaultUserApplicationShouldBeFound("userLogin.contains=" + DEFAULT_USER_LOGIN);

        // Get all the userApplicationList where userLogin contains UPDATED_USER_LOGIN
        defaultUserApplicationShouldNotBeFound("userLogin.contains=" + UPDATED_USER_LOGIN);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUserLoginNotContainsSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userLogin does not contain DEFAULT_USER_LOGIN
        defaultUserApplicationShouldNotBeFound("userLogin.doesNotContain=" + DEFAULT_USER_LOGIN);

        // Get all the userApplicationList where userLogin does not contain UPDATED_USER_LOGIN
        defaultUserApplicationShouldBeFound("userLogin.doesNotContain=" + UPDATED_USER_LOGIN);
    }


    @Test
    @Transactional
    public void getAllUserApplicationsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where createdBy equals to DEFAULT_CREATED_BY
        defaultUserApplicationShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the userApplicationList where createdBy equals to UPDATED_CREATED_BY
        defaultUserApplicationShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where createdBy not equals to DEFAULT_CREATED_BY
        defaultUserApplicationShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the userApplicationList where createdBy not equals to UPDATED_CREATED_BY
        defaultUserApplicationShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultUserApplicationShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the userApplicationList where createdBy equals to UPDATED_CREATED_BY
        defaultUserApplicationShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where createdBy is not null
        defaultUserApplicationShouldBeFound("createdBy.specified=true");

        // Get all the userApplicationList where createdBy is null
        defaultUserApplicationShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllUserApplicationsByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where createdBy contains DEFAULT_CREATED_BY
        defaultUserApplicationShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the userApplicationList where createdBy contains UPDATED_CREATED_BY
        defaultUserApplicationShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where createdBy does not contain DEFAULT_CREATED_BY
        defaultUserApplicationShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the userApplicationList where createdBy does not contain UPDATED_CREATED_BY
        defaultUserApplicationShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllUserApplicationsByCreatedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where createdOn equals to DEFAULT_CREATED_ON
        defaultUserApplicationShouldBeFound("createdOn.equals=" + DEFAULT_CREATED_ON);

        // Get all the userApplicationList where createdOn equals to UPDATED_CREATED_ON
        defaultUserApplicationShouldNotBeFound("createdOn.equals=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByCreatedOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where createdOn not equals to DEFAULT_CREATED_ON
        defaultUserApplicationShouldNotBeFound("createdOn.notEquals=" + DEFAULT_CREATED_ON);

        // Get all the userApplicationList where createdOn not equals to UPDATED_CREATED_ON
        defaultUserApplicationShouldBeFound("createdOn.notEquals=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByCreatedOnIsInShouldWork() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where createdOn in DEFAULT_CREATED_ON or UPDATED_CREATED_ON
        defaultUserApplicationShouldBeFound("createdOn.in=" + DEFAULT_CREATED_ON + "," + UPDATED_CREATED_ON);

        // Get all the userApplicationList where createdOn equals to UPDATED_CREATED_ON
        defaultUserApplicationShouldNotBeFound("createdOn.in=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByCreatedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where createdOn is not null
        defaultUserApplicationShouldBeFound("createdOn.specified=true");

        // Get all the userApplicationList where createdOn is null
        defaultUserApplicationShouldNotBeFound("createdOn.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByCreatedOnIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where createdOn is greater than or equal to DEFAULT_CREATED_ON
        defaultUserApplicationShouldBeFound("createdOn.greaterThanOrEqual=" + DEFAULT_CREATED_ON);

        // Get all the userApplicationList where createdOn is greater than or equal to UPDATED_CREATED_ON
        defaultUserApplicationShouldNotBeFound("createdOn.greaterThanOrEqual=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByCreatedOnIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where createdOn is less than or equal to DEFAULT_CREATED_ON
        defaultUserApplicationShouldBeFound("createdOn.lessThanOrEqual=" + DEFAULT_CREATED_ON);

        // Get all the userApplicationList where createdOn is less than or equal to SMALLER_CREATED_ON
        defaultUserApplicationShouldNotBeFound("createdOn.lessThanOrEqual=" + SMALLER_CREATED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByCreatedOnIsLessThanSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where createdOn is less than DEFAULT_CREATED_ON
        defaultUserApplicationShouldNotBeFound("createdOn.lessThan=" + DEFAULT_CREATED_ON);

        // Get all the userApplicationList where createdOn is less than UPDATED_CREATED_ON
        defaultUserApplicationShouldBeFound("createdOn.lessThan=" + UPDATED_CREATED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByCreatedOnIsGreaterThanSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where createdOn is greater than DEFAULT_CREATED_ON
        defaultUserApplicationShouldNotBeFound("createdOn.greaterThan=" + DEFAULT_CREATED_ON);

        // Get all the userApplicationList where createdOn is greater than SMALLER_CREATED_ON
        defaultUserApplicationShouldBeFound("createdOn.greaterThan=" + SMALLER_CREATED_ON);
    }


    @Test
    @Transactional
    public void getAllUserApplicationsByUpdatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where updatedBy equals to DEFAULT_UPDATED_BY
        defaultUserApplicationShouldBeFound("updatedBy.equals=" + DEFAULT_UPDATED_BY);

        // Get all the userApplicationList where updatedBy equals to UPDATED_UPDATED_BY
        defaultUserApplicationShouldNotBeFound("updatedBy.equals=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUpdatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where updatedBy not equals to DEFAULT_UPDATED_BY
        defaultUserApplicationShouldNotBeFound("updatedBy.notEquals=" + DEFAULT_UPDATED_BY);

        // Get all the userApplicationList where updatedBy not equals to UPDATED_UPDATED_BY
        defaultUserApplicationShouldBeFound("updatedBy.notEquals=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUpdatedByIsInShouldWork() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where updatedBy in DEFAULT_UPDATED_BY or UPDATED_UPDATED_BY
        defaultUserApplicationShouldBeFound("updatedBy.in=" + DEFAULT_UPDATED_BY + "," + UPDATED_UPDATED_BY);

        // Get all the userApplicationList where updatedBy equals to UPDATED_UPDATED_BY
        defaultUserApplicationShouldNotBeFound("updatedBy.in=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUpdatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where updatedBy is not null
        defaultUserApplicationShouldBeFound("updatedBy.specified=true");

        // Get all the userApplicationList where updatedBy is null
        defaultUserApplicationShouldNotBeFound("updatedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllUserApplicationsByUpdatedByContainsSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where updatedBy contains DEFAULT_UPDATED_BY
        defaultUserApplicationShouldBeFound("updatedBy.contains=" + DEFAULT_UPDATED_BY);

        // Get all the userApplicationList where updatedBy contains UPDATED_UPDATED_BY
        defaultUserApplicationShouldNotBeFound("updatedBy.contains=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUpdatedByNotContainsSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where updatedBy does not contain DEFAULT_UPDATED_BY
        defaultUserApplicationShouldNotBeFound("updatedBy.doesNotContain=" + DEFAULT_UPDATED_BY);

        // Get all the userApplicationList where updatedBy does not contain UPDATED_UPDATED_BY
        defaultUserApplicationShouldBeFound("updatedBy.doesNotContain=" + UPDATED_UPDATED_BY);
    }


    @Test
    @Transactional
    public void getAllUserApplicationsByUpdatedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where updatedOn equals to DEFAULT_UPDATED_ON
        defaultUserApplicationShouldBeFound("updatedOn.equals=" + DEFAULT_UPDATED_ON);

        // Get all the userApplicationList where updatedOn equals to UPDATED_UPDATED_ON
        defaultUserApplicationShouldNotBeFound("updatedOn.equals=" + UPDATED_UPDATED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUpdatedOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where updatedOn not equals to DEFAULT_UPDATED_ON
        defaultUserApplicationShouldNotBeFound("updatedOn.notEquals=" + DEFAULT_UPDATED_ON);

        // Get all the userApplicationList where updatedOn not equals to UPDATED_UPDATED_ON
        defaultUserApplicationShouldBeFound("updatedOn.notEquals=" + UPDATED_UPDATED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUpdatedOnIsInShouldWork() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where updatedOn in DEFAULT_UPDATED_ON or UPDATED_UPDATED_ON
        defaultUserApplicationShouldBeFound("updatedOn.in=" + DEFAULT_UPDATED_ON + "," + UPDATED_UPDATED_ON);

        // Get all the userApplicationList where updatedOn equals to UPDATED_UPDATED_ON
        defaultUserApplicationShouldNotBeFound("updatedOn.in=" + UPDATED_UPDATED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUpdatedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where updatedOn is not null
        defaultUserApplicationShouldBeFound("updatedOn.specified=true");

        // Get all the userApplicationList where updatedOn is null
        defaultUserApplicationShouldNotBeFound("updatedOn.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUpdatedOnIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where updatedOn is greater than or equal to DEFAULT_UPDATED_ON
        defaultUserApplicationShouldBeFound("updatedOn.greaterThanOrEqual=" + DEFAULT_UPDATED_ON);

        // Get all the userApplicationList where updatedOn is greater than or equal to UPDATED_UPDATED_ON
        defaultUserApplicationShouldNotBeFound("updatedOn.greaterThanOrEqual=" + UPDATED_UPDATED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUpdatedOnIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where updatedOn is less than or equal to DEFAULT_UPDATED_ON
        defaultUserApplicationShouldBeFound("updatedOn.lessThanOrEqual=" + DEFAULT_UPDATED_ON);

        // Get all the userApplicationList where updatedOn is less than or equal to SMALLER_UPDATED_ON
        defaultUserApplicationShouldNotBeFound("updatedOn.lessThanOrEqual=" + SMALLER_UPDATED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUpdatedOnIsLessThanSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where updatedOn is less than DEFAULT_UPDATED_ON
        defaultUserApplicationShouldNotBeFound("updatedOn.lessThan=" + DEFAULT_UPDATED_ON);

        // Get all the userApplicationList where updatedOn is less than UPDATED_UPDATED_ON
        defaultUserApplicationShouldBeFound("updatedOn.lessThan=" + UPDATED_UPDATED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUpdatedOnIsGreaterThanSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where updatedOn is greater than DEFAULT_UPDATED_ON
        defaultUserApplicationShouldNotBeFound("updatedOn.greaterThan=" + DEFAULT_UPDATED_ON);

        // Get all the userApplicationList where updatedOn is greater than SMALLER_UPDATED_ON
        defaultUserApplicationShouldBeFound("updatedOn.greaterThan=" + SMALLER_UPDATED_ON);
    }


    @Test
    @Transactional
    public void getAllUserApplicationsByVerifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where verifiedBy equals to DEFAULT_VERIFIED_BY
        defaultUserApplicationShouldBeFound("verifiedBy.equals=" + DEFAULT_VERIFIED_BY);

        // Get all the userApplicationList where verifiedBy equals to UPDATED_VERIFIED_BY
        defaultUserApplicationShouldNotBeFound("verifiedBy.equals=" + UPDATED_VERIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByVerifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where verifiedBy not equals to DEFAULT_VERIFIED_BY
        defaultUserApplicationShouldNotBeFound("verifiedBy.notEquals=" + DEFAULT_VERIFIED_BY);

        // Get all the userApplicationList where verifiedBy not equals to UPDATED_VERIFIED_BY
        defaultUserApplicationShouldBeFound("verifiedBy.notEquals=" + UPDATED_VERIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByVerifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where verifiedBy in DEFAULT_VERIFIED_BY or UPDATED_VERIFIED_BY
        defaultUserApplicationShouldBeFound("verifiedBy.in=" + DEFAULT_VERIFIED_BY + "," + UPDATED_VERIFIED_BY);

        // Get all the userApplicationList where verifiedBy equals to UPDATED_VERIFIED_BY
        defaultUserApplicationShouldNotBeFound("verifiedBy.in=" + UPDATED_VERIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByVerifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where verifiedBy is not null
        defaultUserApplicationShouldBeFound("verifiedBy.specified=true");

        // Get all the userApplicationList where verifiedBy is null
        defaultUserApplicationShouldNotBeFound("verifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllUserApplicationsByVerifiedByContainsSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where verifiedBy contains DEFAULT_VERIFIED_BY
        defaultUserApplicationShouldBeFound("verifiedBy.contains=" + DEFAULT_VERIFIED_BY);

        // Get all the userApplicationList where verifiedBy contains UPDATED_VERIFIED_BY
        defaultUserApplicationShouldNotBeFound("verifiedBy.contains=" + UPDATED_VERIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByVerifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where verifiedBy does not contain DEFAULT_VERIFIED_BY
        defaultUserApplicationShouldNotBeFound("verifiedBy.doesNotContain=" + DEFAULT_VERIFIED_BY);

        // Get all the userApplicationList where verifiedBy does not contain UPDATED_VERIFIED_BY
        defaultUserApplicationShouldBeFound("verifiedBy.doesNotContain=" + UPDATED_VERIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllUserApplicationsByVerifiedOnIsEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where verifiedOn equals to DEFAULT_VERIFIED_ON
        defaultUserApplicationShouldBeFound("verifiedOn.equals=" + DEFAULT_VERIFIED_ON);

        // Get all the userApplicationList where verifiedOn equals to UPDATED_VERIFIED_ON
        defaultUserApplicationShouldNotBeFound("verifiedOn.equals=" + UPDATED_VERIFIED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByVerifiedOnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where verifiedOn not equals to DEFAULT_VERIFIED_ON
        defaultUserApplicationShouldNotBeFound("verifiedOn.notEquals=" + DEFAULT_VERIFIED_ON);

        // Get all the userApplicationList where verifiedOn not equals to UPDATED_VERIFIED_ON
        defaultUserApplicationShouldBeFound("verifiedOn.notEquals=" + UPDATED_VERIFIED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByVerifiedOnIsInShouldWork() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where verifiedOn in DEFAULT_VERIFIED_ON or UPDATED_VERIFIED_ON
        defaultUserApplicationShouldBeFound("verifiedOn.in=" + DEFAULT_VERIFIED_ON + "," + UPDATED_VERIFIED_ON);

        // Get all the userApplicationList where verifiedOn equals to UPDATED_VERIFIED_ON
        defaultUserApplicationShouldNotBeFound("verifiedOn.in=" + UPDATED_VERIFIED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByVerifiedOnIsNullOrNotNull() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where verifiedOn is not null
        defaultUserApplicationShouldBeFound("verifiedOn.specified=true");

        // Get all the userApplicationList where verifiedOn is null
        defaultUserApplicationShouldNotBeFound("verifiedOn.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByVerifiedOnIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where verifiedOn is greater than or equal to DEFAULT_VERIFIED_ON
        defaultUserApplicationShouldBeFound("verifiedOn.greaterThanOrEqual=" + DEFAULT_VERIFIED_ON);

        // Get all the userApplicationList where verifiedOn is greater than or equal to UPDATED_VERIFIED_ON
        defaultUserApplicationShouldNotBeFound("verifiedOn.greaterThanOrEqual=" + UPDATED_VERIFIED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByVerifiedOnIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where verifiedOn is less than or equal to DEFAULT_VERIFIED_ON
        defaultUserApplicationShouldBeFound("verifiedOn.lessThanOrEqual=" + DEFAULT_VERIFIED_ON);

        // Get all the userApplicationList where verifiedOn is less than or equal to SMALLER_VERIFIED_ON
        defaultUserApplicationShouldNotBeFound("verifiedOn.lessThanOrEqual=" + SMALLER_VERIFIED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByVerifiedOnIsLessThanSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where verifiedOn is less than DEFAULT_VERIFIED_ON
        defaultUserApplicationShouldNotBeFound("verifiedOn.lessThan=" + DEFAULT_VERIFIED_ON);

        // Get all the userApplicationList where verifiedOn is less than UPDATED_VERIFIED_ON
        defaultUserApplicationShouldBeFound("verifiedOn.lessThan=" + UPDATED_VERIFIED_ON);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByVerifiedOnIsGreaterThanSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where verifiedOn is greater than DEFAULT_VERIFIED_ON
        defaultUserApplicationShouldNotBeFound("verifiedOn.greaterThan=" + DEFAULT_VERIFIED_ON);

        // Get all the userApplicationList where verifiedOn is greater than SMALLER_VERIFIED_ON
        defaultUserApplicationShouldBeFound("verifiedOn.greaterThan=" + SMALLER_VERIFIED_ON);
    }


    @Test
    @Transactional
    public void getAllUserApplicationsByUserCodeIdIsEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userCodeId equals to DEFAULT_USER_CODE_ID
        defaultUserApplicationShouldBeFound("userCodeId.equals=" + DEFAULT_USER_CODE_ID);

        // Get all the userApplicationList where userCodeId equals to UPDATED_USER_CODE_ID
        defaultUserApplicationShouldNotBeFound("userCodeId.equals=" + UPDATED_USER_CODE_ID);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUserCodeIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userCodeId not equals to DEFAULT_USER_CODE_ID
        defaultUserApplicationShouldNotBeFound("userCodeId.notEquals=" + DEFAULT_USER_CODE_ID);

        // Get all the userApplicationList where userCodeId not equals to UPDATED_USER_CODE_ID
        defaultUserApplicationShouldBeFound("userCodeId.notEquals=" + UPDATED_USER_CODE_ID);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUserCodeIdIsInShouldWork() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userCodeId in DEFAULT_USER_CODE_ID or UPDATED_USER_CODE_ID
        defaultUserApplicationShouldBeFound("userCodeId.in=" + DEFAULT_USER_CODE_ID + "," + UPDATED_USER_CODE_ID);

        // Get all the userApplicationList where userCodeId equals to UPDATED_USER_CODE_ID
        defaultUserApplicationShouldNotBeFound("userCodeId.in=" + UPDATED_USER_CODE_ID);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUserCodeIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userCodeId is not null
        defaultUserApplicationShouldBeFound("userCodeId.specified=true");

        // Get all the userApplicationList where userCodeId is null
        defaultUserApplicationShouldNotBeFound("userCodeId.specified=false");
    }
                @Test
    @Transactional
    public void getAllUserApplicationsByUserCodeIdContainsSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userCodeId contains DEFAULT_USER_CODE_ID
        defaultUserApplicationShouldBeFound("userCodeId.contains=" + DEFAULT_USER_CODE_ID);

        // Get all the userApplicationList where userCodeId contains UPDATED_USER_CODE_ID
        defaultUserApplicationShouldNotBeFound("userCodeId.contains=" + UPDATED_USER_CODE_ID);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByUserCodeIdNotContainsSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where userCodeId does not contain DEFAULT_USER_CODE_ID
        defaultUserApplicationShouldNotBeFound("userCodeId.doesNotContain=" + DEFAULT_USER_CODE_ID);

        // Get all the userApplicationList where userCodeId does not contain UPDATED_USER_CODE_ID
        defaultUserApplicationShouldBeFound("userCodeId.doesNotContain=" + UPDATED_USER_CODE_ID);
    }


    @Test
    @Transactional
    public void getAllUserApplicationsByApplicationCodeIdIsEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where applicationCodeId equals to DEFAULT_APPLICATION_CODE_ID
        defaultUserApplicationShouldBeFound("applicationCodeId.equals=" + DEFAULT_APPLICATION_CODE_ID);

        // Get all the userApplicationList where applicationCodeId equals to UPDATED_APPLICATION_CODE_ID
        defaultUserApplicationShouldNotBeFound("applicationCodeId.equals=" + UPDATED_APPLICATION_CODE_ID);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByApplicationCodeIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where applicationCodeId not equals to DEFAULT_APPLICATION_CODE_ID
        defaultUserApplicationShouldNotBeFound("applicationCodeId.notEquals=" + DEFAULT_APPLICATION_CODE_ID);

        // Get all the userApplicationList where applicationCodeId not equals to UPDATED_APPLICATION_CODE_ID
        defaultUserApplicationShouldBeFound("applicationCodeId.notEquals=" + UPDATED_APPLICATION_CODE_ID);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByApplicationCodeIdIsInShouldWork() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where applicationCodeId in DEFAULT_APPLICATION_CODE_ID or UPDATED_APPLICATION_CODE_ID
        defaultUserApplicationShouldBeFound("applicationCodeId.in=" + DEFAULT_APPLICATION_CODE_ID + "," + UPDATED_APPLICATION_CODE_ID);

        // Get all the userApplicationList where applicationCodeId equals to UPDATED_APPLICATION_CODE_ID
        defaultUserApplicationShouldNotBeFound("applicationCodeId.in=" + UPDATED_APPLICATION_CODE_ID);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByApplicationCodeIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where applicationCodeId is not null
        defaultUserApplicationShouldBeFound("applicationCodeId.specified=true");

        // Get all the userApplicationList where applicationCodeId is null
        defaultUserApplicationShouldNotBeFound("applicationCodeId.specified=false");
    }
                @Test
    @Transactional
    public void getAllUserApplicationsByApplicationCodeIdContainsSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where applicationCodeId contains DEFAULT_APPLICATION_CODE_ID
        defaultUserApplicationShouldBeFound("applicationCodeId.contains=" + DEFAULT_APPLICATION_CODE_ID);

        // Get all the userApplicationList where applicationCodeId contains UPDATED_APPLICATION_CODE_ID
        defaultUserApplicationShouldNotBeFound("applicationCodeId.contains=" + UPDATED_APPLICATION_CODE_ID);
    }

    @Test
    @Transactional
    public void getAllUserApplicationsByApplicationCodeIdNotContainsSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        // Get all the userApplicationList where applicationCodeId does not contain DEFAULT_APPLICATION_CODE_ID
        defaultUserApplicationShouldNotBeFound("applicationCodeId.doesNotContain=" + DEFAULT_APPLICATION_CODE_ID);

        // Get all the userApplicationList where applicationCodeId does not contain UPDATED_APPLICATION_CODE_ID
        defaultUserApplicationShouldBeFound("applicationCodeId.doesNotContain=" + UPDATED_APPLICATION_CODE_ID);
    }


    @Test
    @Transactional
    public void getAllUserApplicationsByApplicationMasterIsEqualToSomething() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);
        ApplicationMaster applicationMaster = ApplicationMasterResourceIT.createEntity(em);
        em.persist(applicationMaster);
        em.flush();
        userApplication.setApplicationMaster(applicationMaster);
        userApplicationRepository.saveAndFlush(userApplication);
        Long applicationMasterId = applicationMaster.getId();

        // Get all the userApplicationList where applicationMaster equals to applicationMasterId
        defaultUserApplicationShouldBeFound("applicationMasterId.equals=" + applicationMasterId);

        // Get all the userApplicationList where applicationMaster equals to applicationMasterId + 1
        defaultUserApplicationShouldNotBeFound("applicationMasterId.equals=" + (applicationMasterId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUserApplicationShouldBeFound(String filter) throws Exception {
        restUserApplicationMockMvc.perform(get("/api/user-applications?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userApplication.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].userLogin").value(hasItem(DEFAULT_USER_LOGIN)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].updatedOn").value(hasItem(DEFAULT_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].userCodeId").value(hasItem(DEFAULT_USER_CODE_ID)))
            .andExpect(jsonPath("$.[*].applicationCodeId").value(hasItem(DEFAULT_APPLICATION_CODE_ID)));

        // Check, that the count call also returns 1
        restUserApplicationMockMvc.perform(get("/api/user-applications/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUserApplicationShouldNotBeFound(String filter) throws Exception {
        restUserApplicationMockMvc.perform(get("/api/user-applications?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUserApplicationMockMvc.perform(get("/api/user-applications/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingUserApplication() throws Exception {
        // Get the userApplication
        restUserApplicationMockMvc.perform(get("/api/user-applications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserApplication() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        int databaseSizeBeforeUpdate = userApplicationRepository.findAll().size();

        // Update the userApplication
        UserApplication updatedUserApplication = userApplicationRepository.findById(userApplication.getId()).get();
        // Disconnect from session so that the updates on updatedUserApplication are not directly saved in db
        em.detach(updatedUserApplication);
        updatedUserApplication
            .userId(UPDATED_USER_ID)
            .userLogin(UPDATED_USER_LOGIN)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .updatedBy(UPDATED_UPDATED_BY)
            .updatedOn(UPDATED_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .userCodeId(UPDATED_USER_CODE_ID)
            .applicationCodeId(UPDATED_APPLICATION_CODE_ID);
        UserApplicationDTO userApplicationDTO = userApplicationMapper.toDto(updatedUserApplication);

        restUserApplicationMockMvc.perform(put("/api/user-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userApplicationDTO)))
            .andExpect(status().isOk());

        // Validate the UserApplication in the database
        List<UserApplication> userApplicationList = userApplicationRepository.findAll();
        assertThat(userApplicationList).hasSize(databaseSizeBeforeUpdate);
        UserApplication testUserApplication = userApplicationList.get(userApplicationList.size() - 1);
        assertThat(testUserApplication.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testUserApplication.getUserLogin()).isEqualTo(UPDATED_USER_LOGIN);
        assertThat(testUserApplication.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testUserApplication.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testUserApplication.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testUserApplication.getUpdatedOn()).isEqualTo(UPDATED_UPDATED_ON);
        assertThat(testUserApplication.getVerifiedBy()).isEqualTo(UPDATED_VERIFIED_BY);
        assertThat(testUserApplication.getVerifiedOn()).isEqualTo(UPDATED_VERIFIED_ON);
        assertThat(testUserApplication.getUserCodeId()).isEqualTo(UPDATED_USER_CODE_ID);
        assertThat(testUserApplication.getApplicationCodeId()).isEqualTo(UPDATED_APPLICATION_CODE_ID);

        // Validate the UserApplication in Elasticsearch
        verify(mockUserApplicationSearchRepository, times(1)).save(testUserApplication);
    }

    @Test
    @Transactional
    public void updateNonExistingUserApplication() throws Exception {
        int databaseSizeBeforeUpdate = userApplicationRepository.findAll().size();

        // Create the UserApplication
        UserApplicationDTO userApplicationDTO = userApplicationMapper.toDto(userApplication);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserApplicationMockMvc.perform(put("/api/user-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userApplicationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserApplication in the database
        List<UserApplication> userApplicationList = userApplicationRepository.findAll();
        assertThat(userApplicationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the UserApplication in Elasticsearch
        verify(mockUserApplicationSearchRepository, times(0)).save(userApplication);
    }

    @Test
    @Transactional
    public void deleteUserApplication() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);

        int databaseSizeBeforeDelete = userApplicationRepository.findAll().size();

        // Delete the userApplication
        restUserApplicationMockMvc.perform(delete("/api/user-applications/{id}", userApplication.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserApplication> userApplicationList = userApplicationRepository.findAll();
        assertThat(userApplicationList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the UserApplication in Elasticsearch
        verify(mockUserApplicationSearchRepository, times(1)).deleteById(userApplication.getId());
    }

    @Test
    @Transactional
    public void searchUserApplication() throws Exception {
        // Initialize the database
        userApplicationRepository.saveAndFlush(userApplication);
        when(mockUserApplicationSearchRepository.search(queryStringQuery("id:" + userApplication.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(userApplication), PageRequest.of(0, 1), 1));
        // Search the userApplication
        restUserApplicationMockMvc.perform(get("/api/_search/user-applications?query=id:" + userApplication.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userApplication.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].userLogin").value(hasItem(DEFAULT_USER_LOGIN)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].updatedOn").value(hasItem(DEFAULT_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].userCodeId").value(hasItem(DEFAULT_USER_CODE_ID)))
            .andExpect(jsonPath("$.[*].applicationCodeId").value(hasItem(DEFAULT_APPLICATION_CODE_ID)));
    }
}
