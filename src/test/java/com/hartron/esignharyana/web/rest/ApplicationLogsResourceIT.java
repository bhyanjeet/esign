package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.ApplicationLogs;
import com.hartron.esignharyana.repository.ApplicationLogsRepository;
import com.hartron.esignharyana.repository.search.ApplicationLogsSearchRepository;
import com.hartron.esignharyana.service.ApplicationLogsService;
import com.hartron.esignharyana.service.dto.ApplicationLogsDTO;
import com.hartron.esignharyana.service.mapper.ApplicationLogsMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;
import com.hartron.esignharyana.service.dto.ApplicationLogsCriteria;
import com.hartron.esignharyana.service.ApplicationLogsQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ApplicationLogsResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class ApplicationLogsResourceIT {

    private static final String DEFAULT_ACTION_TAKEN = "AAAAAAAAAA";
    private static final String UPDATED_ACTION_TAKEN = "BBBBBBBBBB";

    private static final String DEFAULT_ACTION_TAKEN_BY = "AAAAAAAAAA";
    private static final String UPDATED_ACTION_TAKEN_BY = "BBBBBBBBBB";

    private static final Long DEFAULT_ACTION_TAKEN_ON_APPLICATION = 1L;
    private static final Long UPDATED_ACTION_TAKEN_ON_APPLICATION = 2L;
    private static final Long SMALLER_ACTION_TAKEN_ON_APPLICATION = 1L - 1L;

    private static final LocalDate DEFAULT_ACTION_TAKEN_ON_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ACTION_TAKEN_ON_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_ACTION_TAKEN_ON_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    @Autowired
    private ApplicationLogsRepository applicationLogsRepository;

    @Autowired
    private ApplicationLogsMapper applicationLogsMapper;

    @Autowired
    private ApplicationLogsService applicationLogsService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.ApplicationLogsSearchRepositoryMockConfiguration
     */
    @Autowired
    private ApplicationLogsSearchRepository mockApplicationLogsSearchRepository;

    @Autowired
    private ApplicationLogsQueryService applicationLogsQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restApplicationLogsMockMvc;

    private ApplicationLogs applicationLogs;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ApplicationLogsResource applicationLogsResource = new ApplicationLogsResource(applicationLogsService, applicationLogsQueryService);
        this.restApplicationLogsMockMvc = MockMvcBuilders.standaloneSetup(applicationLogsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ApplicationLogs createEntity(EntityManager em) {
        ApplicationLogs applicationLogs = new ApplicationLogs()
            .actionTaken(DEFAULT_ACTION_TAKEN)
            .actionTakenBy(DEFAULT_ACTION_TAKEN_BY)
            .actionTakenOnApplication(DEFAULT_ACTION_TAKEN_ON_APPLICATION)
            .actionTakenOnDate(DEFAULT_ACTION_TAKEN_ON_DATE)
            .remarks(DEFAULT_REMARKS);
        return applicationLogs;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ApplicationLogs createUpdatedEntity(EntityManager em) {
        ApplicationLogs applicationLogs = new ApplicationLogs()
            .actionTaken(UPDATED_ACTION_TAKEN)
            .actionTakenBy(UPDATED_ACTION_TAKEN_BY)
            .actionTakenOnApplication(UPDATED_ACTION_TAKEN_ON_APPLICATION)
            .actionTakenOnDate(UPDATED_ACTION_TAKEN_ON_DATE)
            .remarks(UPDATED_REMARKS);
        return applicationLogs;
    }

    @BeforeEach
    public void initTest() {
        applicationLogs = createEntity(em);
    }

    @Test
    @Transactional
    public void createApplicationLogs() throws Exception {
        int databaseSizeBeforeCreate = applicationLogsRepository.findAll().size();

        // Create the ApplicationLogs
        ApplicationLogsDTO applicationLogsDTO = applicationLogsMapper.toDto(applicationLogs);
        restApplicationLogsMockMvc.perform(post("/api/application-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(applicationLogsDTO)))
            .andExpect(status().isCreated());

        // Validate the ApplicationLogs in the database
        List<ApplicationLogs> applicationLogsList = applicationLogsRepository.findAll();
        assertThat(applicationLogsList).hasSize(databaseSizeBeforeCreate + 1);
        ApplicationLogs testApplicationLogs = applicationLogsList.get(applicationLogsList.size() - 1);
        assertThat(testApplicationLogs.getActionTaken()).isEqualTo(DEFAULT_ACTION_TAKEN);
        assertThat(testApplicationLogs.getActionTakenBy()).isEqualTo(DEFAULT_ACTION_TAKEN_BY);
        assertThat(testApplicationLogs.getActionTakenOnApplication()).isEqualTo(DEFAULT_ACTION_TAKEN_ON_APPLICATION);
        assertThat(testApplicationLogs.getActionTakenOnDate()).isEqualTo(DEFAULT_ACTION_TAKEN_ON_DATE);
        assertThat(testApplicationLogs.getRemarks()).isEqualTo(DEFAULT_REMARKS);

        // Validate the ApplicationLogs in Elasticsearch
        verify(mockApplicationLogsSearchRepository, times(1)).save(testApplicationLogs);
    }

    @Test
    @Transactional
    public void createApplicationLogsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = applicationLogsRepository.findAll().size();

        // Create the ApplicationLogs with an existing ID
        applicationLogs.setId(1L);
        ApplicationLogsDTO applicationLogsDTO = applicationLogsMapper.toDto(applicationLogs);

        // An entity with an existing ID cannot be created, so this API call must fail
        restApplicationLogsMockMvc.perform(post("/api/application-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(applicationLogsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ApplicationLogs in the database
        List<ApplicationLogs> applicationLogsList = applicationLogsRepository.findAll();
        assertThat(applicationLogsList).hasSize(databaseSizeBeforeCreate);

        // Validate the ApplicationLogs in Elasticsearch
        verify(mockApplicationLogsSearchRepository, times(0)).save(applicationLogs);
    }


    @Test
    @Transactional
    public void getAllApplicationLogs() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList
        restApplicationLogsMockMvc.perform(get("/api/application-logs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(applicationLogs.getId().intValue())))
            .andExpect(jsonPath("$.[*].actionTaken").value(hasItem(DEFAULT_ACTION_TAKEN)))
            .andExpect(jsonPath("$.[*].actionTakenBy").value(hasItem(DEFAULT_ACTION_TAKEN_BY)))
            .andExpect(jsonPath("$.[*].actionTakenOnApplication").value(hasItem(DEFAULT_ACTION_TAKEN_ON_APPLICATION.intValue())))
            .andExpect(jsonPath("$.[*].actionTakenOnDate").value(hasItem(DEFAULT_ACTION_TAKEN_ON_DATE.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
    
    @Test
    @Transactional
    public void getApplicationLogs() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get the applicationLogs
        restApplicationLogsMockMvc.perform(get("/api/application-logs/{id}", applicationLogs.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(applicationLogs.getId().intValue()))
            .andExpect(jsonPath("$.actionTaken").value(DEFAULT_ACTION_TAKEN))
            .andExpect(jsonPath("$.actionTakenBy").value(DEFAULT_ACTION_TAKEN_BY))
            .andExpect(jsonPath("$.actionTakenOnApplication").value(DEFAULT_ACTION_TAKEN_ON_APPLICATION.intValue()))
            .andExpect(jsonPath("$.actionTakenOnDate").value(DEFAULT_ACTION_TAKEN_ON_DATE.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS));
    }


    @Test
    @Transactional
    public void getApplicationLogsByIdFiltering() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        Long id = applicationLogs.getId();

        defaultApplicationLogsShouldBeFound("id.equals=" + id);
        defaultApplicationLogsShouldNotBeFound("id.notEquals=" + id);

        defaultApplicationLogsShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultApplicationLogsShouldNotBeFound("id.greaterThan=" + id);

        defaultApplicationLogsShouldBeFound("id.lessThanOrEqual=" + id);
        defaultApplicationLogsShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenIsEqualToSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTaken equals to DEFAULT_ACTION_TAKEN
        defaultApplicationLogsShouldBeFound("actionTaken.equals=" + DEFAULT_ACTION_TAKEN);

        // Get all the applicationLogsList where actionTaken equals to UPDATED_ACTION_TAKEN
        defaultApplicationLogsShouldNotBeFound("actionTaken.equals=" + UPDATED_ACTION_TAKEN);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTaken not equals to DEFAULT_ACTION_TAKEN
        defaultApplicationLogsShouldNotBeFound("actionTaken.notEquals=" + DEFAULT_ACTION_TAKEN);

        // Get all the applicationLogsList where actionTaken not equals to UPDATED_ACTION_TAKEN
        defaultApplicationLogsShouldBeFound("actionTaken.notEquals=" + UPDATED_ACTION_TAKEN);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenIsInShouldWork() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTaken in DEFAULT_ACTION_TAKEN or UPDATED_ACTION_TAKEN
        defaultApplicationLogsShouldBeFound("actionTaken.in=" + DEFAULT_ACTION_TAKEN + "," + UPDATED_ACTION_TAKEN);

        // Get all the applicationLogsList where actionTaken equals to UPDATED_ACTION_TAKEN
        defaultApplicationLogsShouldNotBeFound("actionTaken.in=" + UPDATED_ACTION_TAKEN);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenIsNullOrNotNull() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTaken is not null
        defaultApplicationLogsShouldBeFound("actionTaken.specified=true");

        // Get all the applicationLogsList where actionTaken is null
        defaultApplicationLogsShouldNotBeFound("actionTaken.specified=false");
    }
                @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenContainsSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTaken contains DEFAULT_ACTION_TAKEN
        defaultApplicationLogsShouldBeFound("actionTaken.contains=" + DEFAULT_ACTION_TAKEN);

        // Get all the applicationLogsList where actionTaken contains UPDATED_ACTION_TAKEN
        defaultApplicationLogsShouldNotBeFound("actionTaken.contains=" + UPDATED_ACTION_TAKEN);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenNotContainsSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTaken does not contain DEFAULT_ACTION_TAKEN
        defaultApplicationLogsShouldNotBeFound("actionTaken.doesNotContain=" + DEFAULT_ACTION_TAKEN);

        // Get all the applicationLogsList where actionTaken does not contain UPDATED_ACTION_TAKEN
        defaultApplicationLogsShouldBeFound("actionTaken.doesNotContain=" + UPDATED_ACTION_TAKEN);
    }


    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenByIsEqualToSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenBy equals to DEFAULT_ACTION_TAKEN_BY
        defaultApplicationLogsShouldBeFound("actionTakenBy.equals=" + DEFAULT_ACTION_TAKEN_BY);

        // Get all the applicationLogsList where actionTakenBy equals to UPDATED_ACTION_TAKEN_BY
        defaultApplicationLogsShouldNotBeFound("actionTakenBy.equals=" + UPDATED_ACTION_TAKEN_BY);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenBy not equals to DEFAULT_ACTION_TAKEN_BY
        defaultApplicationLogsShouldNotBeFound("actionTakenBy.notEquals=" + DEFAULT_ACTION_TAKEN_BY);

        // Get all the applicationLogsList where actionTakenBy not equals to UPDATED_ACTION_TAKEN_BY
        defaultApplicationLogsShouldBeFound("actionTakenBy.notEquals=" + UPDATED_ACTION_TAKEN_BY);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenByIsInShouldWork() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenBy in DEFAULT_ACTION_TAKEN_BY or UPDATED_ACTION_TAKEN_BY
        defaultApplicationLogsShouldBeFound("actionTakenBy.in=" + DEFAULT_ACTION_TAKEN_BY + "," + UPDATED_ACTION_TAKEN_BY);

        // Get all the applicationLogsList where actionTakenBy equals to UPDATED_ACTION_TAKEN_BY
        defaultApplicationLogsShouldNotBeFound("actionTakenBy.in=" + UPDATED_ACTION_TAKEN_BY);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenByIsNullOrNotNull() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenBy is not null
        defaultApplicationLogsShouldBeFound("actionTakenBy.specified=true");

        // Get all the applicationLogsList where actionTakenBy is null
        defaultApplicationLogsShouldNotBeFound("actionTakenBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenByContainsSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenBy contains DEFAULT_ACTION_TAKEN_BY
        defaultApplicationLogsShouldBeFound("actionTakenBy.contains=" + DEFAULT_ACTION_TAKEN_BY);

        // Get all the applicationLogsList where actionTakenBy contains UPDATED_ACTION_TAKEN_BY
        defaultApplicationLogsShouldNotBeFound("actionTakenBy.contains=" + UPDATED_ACTION_TAKEN_BY);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenByNotContainsSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenBy does not contain DEFAULT_ACTION_TAKEN_BY
        defaultApplicationLogsShouldNotBeFound("actionTakenBy.doesNotContain=" + DEFAULT_ACTION_TAKEN_BY);

        // Get all the applicationLogsList where actionTakenBy does not contain UPDATED_ACTION_TAKEN_BY
        defaultApplicationLogsShouldBeFound("actionTakenBy.doesNotContain=" + UPDATED_ACTION_TAKEN_BY);
    }


    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenOnApplicationIsEqualToSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenOnApplication equals to DEFAULT_ACTION_TAKEN_ON_APPLICATION
        defaultApplicationLogsShouldBeFound("actionTakenOnApplication.equals=" + DEFAULT_ACTION_TAKEN_ON_APPLICATION);

        // Get all the applicationLogsList where actionTakenOnApplication equals to UPDATED_ACTION_TAKEN_ON_APPLICATION
        defaultApplicationLogsShouldNotBeFound("actionTakenOnApplication.equals=" + UPDATED_ACTION_TAKEN_ON_APPLICATION);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenOnApplicationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenOnApplication not equals to DEFAULT_ACTION_TAKEN_ON_APPLICATION
        defaultApplicationLogsShouldNotBeFound("actionTakenOnApplication.notEquals=" + DEFAULT_ACTION_TAKEN_ON_APPLICATION);

        // Get all the applicationLogsList where actionTakenOnApplication not equals to UPDATED_ACTION_TAKEN_ON_APPLICATION
        defaultApplicationLogsShouldBeFound("actionTakenOnApplication.notEquals=" + UPDATED_ACTION_TAKEN_ON_APPLICATION);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenOnApplicationIsInShouldWork() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenOnApplication in DEFAULT_ACTION_TAKEN_ON_APPLICATION or UPDATED_ACTION_TAKEN_ON_APPLICATION
        defaultApplicationLogsShouldBeFound("actionTakenOnApplication.in=" + DEFAULT_ACTION_TAKEN_ON_APPLICATION + "," + UPDATED_ACTION_TAKEN_ON_APPLICATION);

        // Get all the applicationLogsList where actionTakenOnApplication equals to UPDATED_ACTION_TAKEN_ON_APPLICATION
        defaultApplicationLogsShouldNotBeFound("actionTakenOnApplication.in=" + UPDATED_ACTION_TAKEN_ON_APPLICATION);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenOnApplicationIsNullOrNotNull() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenOnApplication is not null
        defaultApplicationLogsShouldBeFound("actionTakenOnApplication.specified=true");

        // Get all the applicationLogsList where actionTakenOnApplication is null
        defaultApplicationLogsShouldNotBeFound("actionTakenOnApplication.specified=false");
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenOnApplicationIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenOnApplication is greater than or equal to DEFAULT_ACTION_TAKEN_ON_APPLICATION
        defaultApplicationLogsShouldBeFound("actionTakenOnApplication.greaterThanOrEqual=" + DEFAULT_ACTION_TAKEN_ON_APPLICATION);

        // Get all the applicationLogsList where actionTakenOnApplication is greater than or equal to UPDATED_ACTION_TAKEN_ON_APPLICATION
        defaultApplicationLogsShouldNotBeFound("actionTakenOnApplication.greaterThanOrEqual=" + UPDATED_ACTION_TAKEN_ON_APPLICATION);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenOnApplicationIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenOnApplication is less than or equal to DEFAULT_ACTION_TAKEN_ON_APPLICATION
        defaultApplicationLogsShouldBeFound("actionTakenOnApplication.lessThanOrEqual=" + DEFAULT_ACTION_TAKEN_ON_APPLICATION);

        // Get all the applicationLogsList where actionTakenOnApplication is less than or equal to SMALLER_ACTION_TAKEN_ON_APPLICATION
        defaultApplicationLogsShouldNotBeFound("actionTakenOnApplication.lessThanOrEqual=" + SMALLER_ACTION_TAKEN_ON_APPLICATION);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenOnApplicationIsLessThanSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenOnApplication is less than DEFAULT_ACTION_TAKEN_ON_APPLICATION
        defaultApplicationLogsShouldNotBeFound("actionTakenOnApplication.lessThan=" + DEFAULT_ACTION_TAKEN_ON_APPLICATION);

        // Get all the applicationLogsList where actionTakenOnApplication is less than UPDATED_ACTION_TAKEN_ON_APPLICATION
        defaultApplicationLogsShouldBeFound("actionTakenOnApplication.lessThan=" + UPDATED_ACTION_TAKEN_ON_APPLICATION);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenOnApplicationIsGreaterThanSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenOnApplication is greater than DEFAULT_ACTION_TAKEN_ON_APPLICATION
        defaultApplicationLogsShouldNotBeFound("actionTakenOnApplication.greaterThan=" + DEFAULT_ACTION_TAKEN_ON_APPLICATION);

        // Get all the applicationLogsList where actionTakenOnApplication is greater than SMALLER_ACTION_TAKEN_ON_APPLICATION
        defaultApplicationLogsShouldBeFound("actionTakenOnApplication.greaterThan=" + SMALLER_ACTION_TAKEN_ON_APPLICATION);
    }


    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenOnDateIsEqualToSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenOnDate equals to DEFAULT_ACTION_TAKEN_ON_DATE
        defaultApplicationLogsShouldBeFound("actionTakenOnDate.equals=" + DEFAULT_ACTION_TAKEN_ON_DATE);

        // Get all the applicationLogsList where actionTakenOnDate equals to UPDATED_ACTION_TAKEN_ON_DATE
        defaultApplicationLogsShouldNotBeFound("actionTakenOnDate.equals=" + UPDATED_ACTION_TAKEN_ON_DATE);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenOnDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenOnDate not equals to DEFAULT_ACTION_TAKEN_ON_DATE
        defaultApplicationLogsShouldNotBeFound("actionTakenOnDate.notEquals=" + DEFAULT_ACTION_TAKEN_ON_DATE);

        // Get all the applicationLogsList where actionTakenOnDate not equals to UPDATED_ACTION_TAKEN_ON_DATE
        defaultApplicationLogsShouldBeFound("actionTakenOnDate.notEquals=" + UPDATED_ACTION_TAKEN_ON_DATE);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenOnDateIsInShouldWork() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenOnDate in DEFAULT_ACTION_TAKEN_ON_DATE or UPDATED_ACTION_TAKEN_ON_DATE
        defaultApplicationLogsShouldBeFound("actionTakenOnDate.in=" + DEFAULT_ACTION_TAKEN_ON_DATE + "," + UPDATED_ACTION_TAKEN_ON_DATE);

        // Get all the applicationLogsList where actionTakenOnDate equals to UPDATED_ACTION_TAKEN_ON_DATE
        defaultApplicationLogsShouldNotBeFound("actionTakenOnDate.in=" + UPDATED_ACTION_TAKEN_ON_DATE);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenOnDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenOnDate is not null
        defaultApplicationLogsShouldBeFound("actionTakenOnDate.specified=true");

        // Get all the applicationLogsList where actionTakenOnDate is null
        defaultApplicationLogsShouldNotBeFound("actionTakenOnDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenOnDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenOnDate is greater than or equal to DEFAULT_ACTION_TAKEN_ON_DATE
        defaultApplicationLogsShouldBeFound("actionTakenOnDate.greaterThanOrEqual=" + DEFAULT_ACTION_TAKEN_ON_DATE);

        // Get all the applicationLogsList where actionTakenOnDate is greater than or equal to UPDATED_ACTION_TAKEN_ON_DATE
        defaultApplicationLogsShouldNotBeFound("actionTakenOnDate.greaterThanOrEqual=" + UPDATED_ACTION_TAKEN_ON_DATE);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenOnDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenOnDate is less than or equal to DEFAULT_ACTION_TAKEN_ON_DATE
        defaultApplicationLogsShouldBeFound("actionTakenOnDate.lessThanOrEqual=" + DEFAULT_ACTION_TAKEN_ON_DATE);

        // Get all the applicationLogsList where actionTakenOnDate is less than or equal to SMALLER_ACTION_TAKEN_ON_DATE
        defaultApplicationLogsShouldNotBeFound("actionTakenOnDate.lessThanOrEqual=" + SMALLER_ACTION_TAKEN_ON_DATE);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenOnDateIsLessThanSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenOnDate is less than DEFAULT_ACTION_TAKEN_ON_DATE
        defaultApplicationLogsShouldNotBeFound("actionTakenOnDate.lessThan=" + DEFAULT_ACTION_TAKEN_ON_DATE);

        // Get all the applicationLogsList where actionTakenOnDate is less than UPDATED_ACTION_TAKEN_ON_DATE
        defaultApplicationLogsShouldBeFound("actionTakenOnDate.lessThan=" + UPDATED_ACTION_TAKEN_ON_DATE);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByActionTakenOnDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where actionTakenOnDate is greater than DEFAULT_ACTION_TAKEN_ON_DATE
        defaultApplicationLogsShouldNotBeFound("actionTakenOnDate.greaterThan=" + DEFAULT_ACTION_TAKEN_ON_DATE);

        // Get all the applicationLogsList where actionTakenOnDate is greater than SMALLER_ACTION_TAKEN_ON_DATE
        defaultApplicationLogsShouldBeFound("actionTakenOnDate.greaterThan=" + SMALLER_ACTION_TAKEN_ON_DATE);
    }


    @Test
    @Transactional
    public void getAllApplicationLogsByRemarksIsEqualToSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where remarks equals to DEFAULT_REMARKS
        defaultApplicationLogsShouldBeFound("remarks.equals=" + DEFAULT_REMARKS);

        // Get all the applicationLogsList where remarks equals to UPDATED_REMARKS
        defaultApplicationLogsShouldNotBeFound("remarks.equals=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByRemarksIsNotEqualToSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where remarks not equals to DEFAULT_REMARKS
        defaultApplicationLogsShouldNotBeFound("remarks.notEquals=" + DEFAULT_REMARKS);

        // Get all the applicationLogsList where remarks not equals to UPDATED_REMARKS
        defaultApplicationLogsShouldBeFound("remarks.notEquals=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByRemarksIsInShouldWork() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where remarks in DEFAULT_REMARKS or UPDATED_REMARKS
        defaultApplicationLogsShouldBeFound("remarks.in=" + DEFAULT_REMARKS + "," + UPDATED_REMARKS);

        // Get all the applicationLogsList where remarks equals to UPDATED_REMARKS
        defaultApplicationLogsShouldNotBeFound("remarks.in=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByRemarksIsNullOrNotNull() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where remarks is not null
        defaultApplicationLogsShouldBeFound("remarks.specified=true");

        // Get all the applicationLogsList where remarks is null
        defaultApplicationLogsShouldNotBeFound("remarks.specified=false");
    }
                @Test
    @Transactional
    public void getAllApplicationLogsByRemarksContainsSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where remarks contains DEFAULT_REMARKS
        defaultApplicationLogsShouldBeFound("remarks.contains=" + DEFAULT_REMARKS);

        // Get all the applicationLogsList where remarks contains UPDATED_REMARKS
        defaultApplicationLogsShouldNotBeFound("remarks.contains=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllApplicationLogsByRemarksNotContainsSomething() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        // Get all the applicationLogsList where remarks does not contain DEFAULT_REMARKS
        defaultApplicationLogsShouldNotBeFound("remarks.doesNotContain=" + DEFAULT_REMARKS);

        // Get all the applicationLogsList where remarks does not contain UPDATED_REMARKS
        defaultApplicationLogsShouldBeFound("remarks.doesNotContain=" + UPDATED_REMARKS);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultApplicationLogsShouldBeFound(String filter) throws Exception {
        restApplicationLogsMockMvc.perform(get("/api/application-logs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(applicationLogs.getId().intValue())))
            .andExpect(jsonPath("$.[*].actionTaken").value(hasItem(DEFAULT_ACTION_TAKEN)))
            .andExpect(jsonPath("$.[*].actionTakenBy").value(hasItem(DEFAULT_ACTION_TAKEN_BY)))
            .andExpect(jsonPath("$.[*].actionTakenOnApplication").value(hasItem(DEFAULT_ACTION_TAKEN_ON_APPLICATION.intValue())))
            .andExpect(jsonPath("$.[*].actionTakenOnDate").value(hasItem(DEFAULT_ACTION_TAKEN_ON_DATE.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));

        // Check, that the count call also returns 1
        restApplicationLogsMockMvc.perform(get("/api/application-logs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultApplicationLogsShouldNotBeFound(String filter) throws Exception {
        restApplicationLogsMockMvc.perform(get("/api/application-logs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restApplicationLogsMockMvc.perform(get("/api/application-logs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingApplicationLogs() throws Exception {
        // Get the applicationLogs
        restApplicationLogsMockMvc.perform(get("/api/application-logs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateApplicationLogs() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        int databaseSizeBeforeUpdate = applicationLogsRepository.findAll().size();

        // Update the applicationLogs
        ApplicationLogs updatedApplicationLogs = applicationLogsRepository.findById(applicationLogs.getId()).get();
        // Disconnect from session so that the updates on updatedApplicationLogs are not directly saved in db
        em.detach(updatedApplicationLogs);
        updatedApplicationLogs
            .actionTaken(UPDATED_ACTION_TAKEN)
            .actionTakenBy(UPDATED_ACTION_TAKEN_BY)
            .actionTakenOnApplication(UPDATED_ACTION_TAKEN_ON_APPLICATION)
            .actionTakenOnDate(UPDATED_ACTION_TAKEN_ON_DATE)
            .remarks(UPDATED_REMARKS);
        ApplicationLogsDTO applicationLogsDTO = applicationLogsMapper.toDto(updatedApplicationLogs);

        restApplicationLogsMockMvc.perform(put("/api/application-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(applicationLogsDTO)))
            .andExpect(status().isOk());

        // Validate the ApplicationLogs in the database
        List<ApplicationLogs> applicationLogsList = applicationLogsRepository.findAll();
        assertThat(applicationLogsList).hasSize(databaseSizeBeforeUpdate);
        ApplicationLogs testApplicationLogs = applicationLogsList.get(applicationLogsList.size() - 1);
        assertThat(testApplicationLogs.getActionTaken()).isEqualTo(UPDATED_ACTION_TAKEN);
        assertThat(testApplicationLogs.getActionTakenBy()).isEqualTo(UPDATED_ACTION_TAKEN_BY);
        assertThat(testApplicationLogs.getActionTakenOnApplication()).isEqualTo(UPDATED_ACTION_TAKEN_ON_APPLICATION);
        assertThat(testApplicationLogs.getActionTakenOnDate()).isEqualTo(UPDATED_ACTION_TAKEN_ON_DATE);
        assertThat(testApplicationLogs.getRemarks()).isEqualTo(UPDATED_REMARKS);

        // Validate the ApplicationLogs in Elasticsearch
        verify(mockApplicationLogsSearchRepository, times(1)).save(testApplicationLogs);
    }

    @Test
    @Transactional
    public void updateNonExistingApplicationLogs() throws Exception {
        int databaseSizeBeforeUpdate = applicationLogsRepository.findAll().size();

        // Create the ApplicationLogs
        ApplicationLogsDTO applicationLogsDTO = applicationLogsMapper.toDto(applicationLogs);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restApplicationLogsMockMvc.perform(put("/api/application-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(applicationLogsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ApplicationLogs in the database
        List<ApplicationLogs> applicationLogsList = applicationLogsRepository.findAll();
        assertThat(applicationLogsList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ApplicationLogs in Elasticsearch
        verify(mockApplicationLogsSearchRepository, times(0)).save(applicationLogs);
    }

    @Test
    @Transactional
    public void deleteApplicationLogs() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);

        int databaseSizeBeforeDelete = applicationLogsRepository.findAll().size();

        // Delete the applicationLogs
        restApplicationLogsMockMvc.perform(delete("/api/application-logs/{id}", applicationLogs.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ApplicationLogs> applicationLogsList = applicationLogsRepository.findAll();
        assertThat(applicationLogsList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ApplicationLogs in Elasticsearch
        verify(mockApplicationLogsSearchRepository, times(1)).deleteById(applicationLogs.getId());
    }

    @Test
    @Transactional
    public void searchApplicationLogs() throws Exception {
        // Initialize the database
        applicationLogsRepository.saveAndFlush(applicationLogs);
        when(mockApplicationLogsSearchRepository.search(queryStringQuery("id:" + applicationLogs.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(applicationLogs), PageRequest.of(0, 1), 1));
        // Search the applicationLogs
        restApplicationLogsMockMvc.perform(get("/api/_search/application-logs?query=id:" + applicationLogs.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(applicationLogs.getId().intValue())))
            .andExpect(jsonPath("$.[*].actionTaken").value(hasItem(DEFAULT_ACTION_TAKEN)))
            .andExpect(jsonPath("$.[*].actionTakenBy").value(hasItem(DEFAULT_ACTION_TAKEN_BY)))
            .andExpect(jsonPath("$.[*].actionTakenOnApplication").value(hasItem(DEFAULT_ACTION_TAKEN_ON_APPLICATION.intValue())))
            .andExpect(jsonPath("$.[*].actionTakenOnDate").value(hasItem(DEFAULT_ACTION_TAKEN_ON_DATE.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
}
