package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.BlockMaster;
import com.hartron.esignharyana.repository.BlockMasterRepository;
import com.hartron.esignharyana.repository.search.BlockMasterSearchRepository;
import com.hartron.esignharyana.service.BlockMasterService;
import com.hartron.esignharyana.service.dto.BlockMasterDTO;
import com.hartron.esignharyana.service.mapper.BlockMasterMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BlockMasterResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class BlockMasterResourceIT {

    private static final String DEFAULT_BLOCK_CODE = "AAAAAAAAAA";
    private static final String UPDATED_BLOCK_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_BLOCK_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BLOCK_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LAST_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_VERIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_VERIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_VERIFIED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VERIFIED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    @Autowired
    private BlockMasterRepository blockMasterRepository;

    @Autowired
    private BlockMasterMapper blockMasterMapper;

    @Autowired
    private BlockMasterService blockMasterService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.BlockMasterSearchRepositoryMockConfiguration
     */
    @Autowired
    private BlockMasterSearchRepository mockBlockMasterSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBlockMasterMockMvc;

    private BlockMaster blockMaster;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BlockMasterResource blockMasterResource = new BlockMasterResource(blockMasterService);
        this.restBlockMasterMockMvc = MockMvcBuilders.standaloneSetup(blockMasterResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BlockMaster createEntity(EntityManager em) {
        BlockMaster blockMaster = new BlockMaster()
            .blockCode(DEFAULT_BLOCK_CODE)
            .blockName(DEFAULT_BLOCK_NAME)
            .createdOn(DEFAULT_CREATED_ON)
            .lastUpdatedBy(DEFAULT_LAST_UPDATED_BY)
            .lastUpdatedOn(DEFAULT_LAST_UPDATED_ON)
            .verifiedBy(DEFAULT_VERIFIED_BY)
            .verifiedOn(DEFAULT_VERIFIED_ON)
            .remarks(DEFAULT_REMARKS);
        return blockMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BlockMaster createUpdatedEntity(EntityManager em) {
        BlockMaster blockMaster = new BlockMaster()
            .blockCode(UPDATED_BLOCK_CODE)
            .blockName(UPDATED_BLOCK_NAME)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS);
        return blockMaster;
    }

    @BeforeEach
    public void initTest() {
        blockMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createBlockMaster() throws Exception {
        int databaseSizeBeforeCreate = blockMasterRepository.findAll().size();

        // Create the BlockMaster
        BlockMasterDTO blockMasterDTO = blockMasterMapper.toDto(blockMaster);
        restBlockMasterMockMvc.perform(post("/api/block-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(blockMasterDTO)))
            .andExpect(status().isCreated());

        // Validate the BlockMaster in the database
        List<BlockMaster> blockMasterList = blockMasterRepository.findAll();
        assertThat(blockMasterList).hasSize(databaseSizeBeforeCreate + 1);
        BlockMaster testBlockMaster = blockMasterList.get(blockMasterList.size() - 1);
        assertThat(testBlockMaster.getBlockCode()).isEqualTo(DEFAULT_BLOCK_CODE);
        assertThat(testBlockMaster.getBlockName()).isEqualTo(DEFAULT_BLOCK_NAME);
        assertThat(testBlockMaster.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testBlockMaster.getLastUpdatedBy()).isEqualTo(DEFAULT_LAST_UPDATED_BY);
        assertThat(testBlockMaster.getLastUpdatedOn()).isEqualTo(DEFAULT_LAST_UPDATED_ON);
        assertThat(testBlockMaster.getVerifiedBy()).isEqualTo(DEFAULT_VERIFIED_BY);
        assertThat(testBlockMaster.getVerifiedOn()).isEqualTo(DEFAULT_VERIFIED_ON);
        assertThat(testBlockMaster.getRemarks()).isEqualTo(DEFAULT_REMARKS);

        // Validate the BlockMaster in Elasticsearch
        verify(mockBlockMasterSearchRepository, times(1)).save(testBlockMaster);
    }

    @Test
    @Transactional
    public void createBlockMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = blockMasterRepository.findAll().size();

        // Create the BlockMaster with an existing ID
        blockMaster.setId(1L);
        BlockMasterDTO blockMasterDTO = blockMasterMapper.toDto(blockMaster);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBlockMasterMockMvc.perform(post("/api/block-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(blockMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BlockMaster in the database
        List<BlockMaster> blockMasterList = blockMasterRepository.findAll();
        assertThat(blockMasterList).hasSize(databaseSizeBeforeCreate);

        // Validate the BlockMaster in Elasticsearch
        verify(mockBlockMasterSearchRepository, times(0)).save(blockMaster);
    }


    @Test
    @Transactional
    public void checkBlockNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = blockMasterRepository.findAll().size();
        // set the field null
        blockMaster.setBlockName(null);

        // Create the BlockMaster, which fails.
        BlockMasterDTO blockMasterDTO = blockMasterMapper.toDto(blockMaster);

        restBlockMasterMockMvc.perform(post("/api/block-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(blockMasterDTO)))
            .andExpect(status().isBadRequest());

        List<BlockMaster> blockMasterList = blockMasterRepository.findAll();
        assertThat(blockMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBlockMasters() throws Exception {
        // Initialize the database
        blockMasterRepository.saveAndFlush(blockMaster);

        // Get all the blockMasterList
        restBlockMasterMockMvc.perform(get("/api/block-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(blockMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].blockCode").value(hasItem(DEFAULT_BLOCK_CODE)))
            .andExpect(jsonPath("$.[*].blockName").value(hasItem(DEFAULT_BLOCK_NAME)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
    
    @Test
    @Transactional
    public void getBlockMaster() throws Exception {
        // Initialize the database
        blockMasterRepository.saveAndFlush(blockMaster);

        // Get the blockMaster
        restBlockMasterMockMvc.perform(get("/api/block-masters/{id}", blockMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(blockMaster.getId().intValue()))
            .andExpect(jsonPath("$.blockCode").value(DEFAULT_BLOCK_CODE))
            .andExpect(jsonPath("$.blockName").value(DEFAULT_BLOCK_NAME))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.lastUpdatedBy").value(DEFAULT_LAST_UPDATED_BY))
            .andExpect(jsonPath("$.lastUpdatedOn").value(DEFAULT_LAST_UPDATED_ON.toString()))
            .andExpect(jsonPath("$.verifiedBy").value(DEFAULT_VERIFIED_BY))
            .andExpect(jsonPath("$.verifiedOn").value(DEFAULT_VERIFIED_ON.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS));
    }

    @Test
    @Transactional
    public void getNonExistingBlockMaster() throws Exception {
        // Get the blockMaster
        restBlockMasterMockMvc.perform(get("/api/block-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBlockMaster() throws Exception {
        // Initialize the database
        blockMasterRepository.saveAndFlush(blockMaster);

        int databaseSizeBeforeUpdate = blockMasterRepository.findAll().size();

        // Update the blockMaster
        BlockMaster updatedBlockMaster = blockMasterRepository.findById(blockMaster.getId()).get();
        // Disconnect from session so that the updates on updatedBlockMaster are not directly saved in db
        em.detach(updatedBlockMaster);
        updatedBlockMaster
            .blockCode(UPDATED_BLOCK_CODE)
            .blockName(UPDATED_BLOCK_NAME)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS);
        BlockMasterDTO blockMasterDTO = blockMasterMapper.toDto(updatedBlockMaster);

        restBlockMasterMockMvc.perform(put("/api/block-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(blockMasterDTO)))
            .andExpect(status().isOk());

        // Validate the BlockMaster in the database
        List<BlockMaster> blockMasterList = blockMasterRepository.findAll();
        assertThat(blockMasterList).hasSize(databaseSizeBeforeUpdate);
        BlockMaster testBlockMaster = blockMasterList.get(blockMasterList.size() - 1);
        assertThat(testBlockMaster.getBlockCode()).isEqualTo(UPDATED_BLOCK_CODE);
        assertThat(testBlockMaster.getBlockName()).isEqualTo(UPDATED_BLOCK_NAME);
        assertThat(testBlockMaster.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testBlockMaster.getLastUpdatedBy()).isEqualTo(UPDATED_LAST_UPDATED_BY);
        assertThat(testBlockMaster.getLastUpdatedOn()).isEqualTo(UPDATED_LAST_UPDATED_ON);
        assertThat(testBlockMaster.getVerifiedBy()).isEqualTo(UPDATED_VERIFIED_BY);
        assertThat(testBlockMaster.getVerifiedOn()).isEqualTo(UPDATED_VERIFIED_ON);
        assertThat(testBlockMaster.getRemarks()).isEqualTo(UPDATED_REMARKS);

        // Validate the BlockMaster in Elasticsearch
        verify(mockBlockMasterSearchRepository, times(1)).save(testBlockMaster);
    }

    @Test
    @Transactional
    public void updateNonExistingBlockMaster() throws Exception {
        int databaseSizeBeforeUpdate = blockMasterRepository.findAll().size();

        // Create the BlockMaster
        BlockMasterDTO blockMasterDTO = blockMasterMapper.toDto(blockMaster);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBlockMasterMockMvc.perform(put("/api/block-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(blockMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BlockMaster in the database
        List<BlockMaster> blockMasterList = blockMasterRepository.findAll();
        assertThat(blockMasterList).hasSize(databaseSizeBeforeUpdate);

        // Validate the BlockMaster in Elasticsearch
        verify(mockBlockMasterSearchRepository, times(0)).save(blockMaster);
    }

    @Test
    @Transactional
    public void deleteBlockMaster() throws Exception {
        // Initialize the database
        blockMasterRepository.saveAndFlush(blockMaster);

        int databaseSizeBeforeDelete = blockMasterRepository.findAll().size();

        // Delete the blockMaster
        restBlockMasterMockMvc.perform(delete("/api/block-masters/{id}", blockMaster.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BlockMaster> blockMasterList = blockMasterRepository.findAll();
        assertThat(blockMasterList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the BlockMaster in Elasticsearch
        verify(mockBlockMasterSearchRepository, times(1)).deleteById(blockMaster.getId());
    }

    @Test
    @Transactional
    public void searchBlockMaster() throws Exception {
        // Initialize the database
        blockMasterRepository.saveAndFlush(blockMaster);
        when(mockBlockMasterSearchRepository.search(queryStringQuery("id:" + blockMaster.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(blockMaster), PageRequest.of(0, 1), 1));
        // Search the blockMaster
        restBlockMasterMockMvc.perform(get("/api/_search/block-masters?query=id:" + blockMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(blockMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].blockCode").value(hasItem(DEFAULT_BLOCK_CODE)))
            .andExpect(jsonPath("$.[*].blockName").value(hasItem(DEFAULT_BLOCK_NAME)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
}
