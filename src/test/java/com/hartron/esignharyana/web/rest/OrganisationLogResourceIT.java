package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.OrganisationLog;
import com.hartron.esignharyana.repository.OrganisationLogRepository;
import com.hartron.esignharyana.repository.search.OrganisationLogSearchRepository;
import com.hartron.esignharyana.service.OrganisationLogService;
import com.hartron.esignharyana.service.dto.OrganisationLogDTO;
import com.hartron.esignharyana.service.mapper.OrganisationLogMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.sameInstant;
import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OrganisationLogResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class OrganisationLogResourceIT {

    private static final Long DEFAULT_ORGANISATION_ID = 1L;
    private static final Long UPDATED_ORGANISATION_ID = 2L;

    private static final String DEFAULT_CREATED_BY_LOGIN = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY_LOGIN = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_VERIFY_BY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_VERIFY_BY_NAME = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_VERIFY_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_VERIFY_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_VERIFY_BY_LOGIN = "AAAAAAAAAA";
    private static final String UPDATED_VERIFY_BY_LOGIN = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY_NAME = "BBBBBBBBBB";

    @Autowired
    private OrganisationLogRepository organisationLogRepository;

    @Autowired
    private OrganisationLogMapper organisationLogMapper;

    @Autowired
    private OrganisationLogService organisationLogService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.OrganisationLogSearchRepositoryMockConfiguration
     */
    @Autowired
    private OrganisationLogSearchRepository mockOrganisationLogSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOrganisationLogMockMvc;

    private OrganisationLog organisationLog;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OrganisationLogResource organisationLogResource = new OrganisationLogResource(organisationLogService);
        this.restOrganisationLogMockMvc = MockMvcBuilders.standaloneSetup(organisationLogResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrganisationLog createEntity(EntityManager em) {
        OrganisationLog organisationLog = new OrganisationLog()
            .organisationId(DEFAULT_ORGANISATION_ID)
            .createdByLogin(DEFAULT_CREATED_BY_LOGIN)
            .createdDate(DEFAULT_CREATED_DATE)
            .verifyByName(DEFAULT_VERIFY_BY_NAME)
            .verifyDate(DEFAULT_VERIFY_DATE)
            .remarks(DEFAULT_REMARKS)
            .status(DEFAULT_STATUS)
            .verifyByLogin(DEFAULT_VERIFY_BY_LOGIN)
            .createdByName(DEFAULT_CREATED_BY_NAME);
        return organisationLog;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrganisationLog createUpdatedEntity(EntityManager em) {
        OrganisationLog organisationLog = new OrganisationLog()
            .organisationId(UPDATED_ORGANISATION_ID)
            .createdByLogin(UPDATED_CREATED_BY_LOGIN)
            .createdDate(UPDATED_CREATED_DATE)
            .verifyByName(UPDATED_VERIFY_BY_NAME)
            .verifyDate(UPDATED_VERIFY_DATE)
            .remarks(UPDATED_REMARKS)
            .status(UPDATED_STATUS)
            .verifyByLogin(UPDATED_VERIFY_BY_LOGIN)
            .createdByName(UPDATED_CREATED_BY_NAME);
        return organisationLog;
    }

    @BeforeEach
    public void initTest() {
        organisationLog = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrganisationLog() throws Exception {
        int databaseSizeBeforeCreate = organisationLogRepository.findAll().size();

        // Create the OrganisationLog
        OrganisationLogDTO organisationLogDTO = organisationLogMapper.toDto(organisationLog);
        restOrganisationLogMockMvc.perform(post("/api/organisation-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationLogDTO)))
            .andExpect(status().isCreated());

        // Validate the OrganisationLog in the database
        List<OrganisationLog> organisationLogList = organisationLogRepository.findAll();
        assertThat(organisationLogList).hasSize(databaseSizeBeforeCreate + 1);
        OrganisationLog testOrganisationLog = organisationLogList.get(organisationLogList.size() - 1);
        assertThat(testOrganisationLog.getOrganisationId()).isEqualTo(DEFAULT_ORGANISATION_ID);
        assertThat(testOrganisationLog.getCreatedByLogin()).isEqualTo(DEFAULT_CREATED_BY_LOGIN);
        assertThat(testOrganisationLog.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testOrganisationLog.getVerifyByName()).isEqualTo(DEFAULT_VERIFY_BY_NAME);
        assertThat(testOrganisationLog.getVerifyDate()).isEqualTo(DEFAULT_VERIFY_DATE);
        assertThat(testOrganisationLog.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testOrganisationLog.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testOrganisationLog.getVerifyByLogin()).isEqualTo(DEFAULT_VERIFY_BY_LOGIN);
        assertThat(testOrganisationLog.getCreatedByName()).isEqualTo(DEFAULT_CREATED_BY_NAME);

        // Validate the OrganisationLog in Elasticsearch
        verify(mockOrganisationLogSearchRepository, times(1)).save(testOrganisationLog);
    }

    @Test
    @Transactional
    public void createOrganisationLogWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = organisationLogRepository.findAll().size();

        // Create the OrganisationLog with an existing ID
        organisationLog.setId(1L);
        OrganisationLogDTO organisationLogDTO = organisationLogMapper.toDto(organisationLog);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrganisationLogMockMvc.perform(post("/api/organisation-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationLogDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrganisationLog in the database
        List<OrganisationLog> organisationLogList = organisationLogRepository.findAll();
        assertThat(organisationLogList).hasSize(databaseSizeBeforeCreate);

        // Validate the OrganisationLog in Elasticsearch
        verify(mockOrganisationLogSearchRepository, times(0)).save(organisationLog);
    }


    @Test
    @Transactional
    public void getAllOrganisationLogs() throws Exception {
        // Initialize the database
        organisationLogRepository.saveAndFlush(organisationLog);

        // Get all the organisationLogList
        restOrganisationLogMockMvc.perform(get("/api/organisation-logs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(organisationLog.getId().intValue())))
            .andExpect(jsonPath("$.[*].organisationId").value(hasItem(DEFAULT_ORGANISATION_ID.intValue())))
            .andExpect(jsonPath("$.[*].createdByLogin").value(hasItem(DEFAULT_CREATED_BY_LOGIN)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].verifyByName").value(hasItem(DEFAULT_VERIFY_BY_NAME)))
            .andExpect(jsonPath("$.[*].verifyDate").value(hasItem(sameInstant(DEFAULT_VERIFY_DATE))))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].verifyByLogin").value(hasItem(DEFAULT_VERIFY_BY_LOGIN)))
            .andExpect(jsonPath("$.[*].createdByName").value(hasItem(DEFAULT_CREATED_BY_NAME)));
    }
    
    @Test
    @Transactional
    public void getOrganisationLog() throws Exception {
        // Initialize the database
        organisationLogRepository.saveAndFlush(organisationLog);

        // Get the organisationLog
        restOrganisationLogMockMvc.perform(get("/api/organisation-logs/{id}", organisationLog.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(organisationLog.getId().intValue()))
            .andExpect(jsonPath("$.organisationId").value(DEFAULT_ORGANISATION_ID.intValue()))
            .andExpect(jsonPath("$.createdByLogin").value(DEFAULT_CREATED_BY_LOGIN))
            .andExpect(jsonPath("$.createdDate").value(sameInstant(DEFAULT_CREATED_DATE)))
            .andExpect(jsonPath("$.verifyByName").value(DEFAULT_VERIFY_BY_NAME))
            .andExpect(jsonPath("$.verifyDate").value(sameInstant(DEFAULT_VERIFY_DATE)))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.verifyByLogin").value(DEFAULT_VERIFY_BY_LOGIN))
            .andExpect(jsonPath("$.createdByName").value(DEFAULT_CREATED_BY_NAME));
    }

    @Test
    @Transactional
    public void getNonExistingOrganisationLog() throws Exception {
        // Get the organisationLog
        restOrganisationLogMockMvc.perform(get("/api/organisation-logs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrganisationLog() throws Exception {
        // Initialize the database
        organisationLogRepository.saveAndFlush(organisationLog);

        int databaseSizeBeforeUpdate = organisationLogRepository.findAll().size();

        // Update the organisationLog
        OrganisationLog updatedOrganisationLog = organisationLogRepository.findById(organisationLog.getId()).get();
        // Disconnect from session so that the updates on updatedOrganisationLog are not directly saved in db
        em.detach(updatedOrganisationLog);
        updatedOrganisationLog
            .organisationId(UPDATED_ORGANISATION_ID)
            .createdByLogin(UPDATED_CREATED_BY_LOGIN)
            .createdDate(UPDATED_CREATED_DATE)
            .verifyByName(UPDATED_VERIFY_BY_NAME)
            .verifyDate(UPDATED_VERIFY_DATE)
            .remarks(UPDATED_REMARKS)
            .status(UPDATED_STATUS)
            .verifyByLogin(UPDATED_VERIFY_BY_LOGIN)
            .createdByName(UPDATED_CREATED_BY_NAME);
        OrganisationLogDTO organisationLogDTO = organisationLogMapper.toDto(updatedOrganisationLog);

        restOrganisationLogMockMvc.perform(put("/api/organisation-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationLogDTO)))
            .andExpect(status().isOk());

        // Validate the OrganisationLog in the database
        List<OrganisationLog> organisationLogList = organisationLogRepository.findAll();
        assertThat(organisationLogList).hasSize(databaseSizeBeforeUpdate);
        OrganisationLog testOrganisationLog = organisationLogList.get(organisationLogList.size() - 1);
        assertThat(testOrganisationLog.getOrganisationId()).isEqualTo(UPDATED_ORGANISATION_ID);
        assertThat(testOrganisationLog.getCreatedByLogin()).isEqualTo(UPDATED_CREATED_BY_LOGIN);
        assertThat(testOrganisationLog.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testOrganisationLog.getVerifyByName()).isEqualTo(UPDATED_VERIFY_BY_NAME);
        assertThat(testOrganisationLog.getVerifyDate()).isEqualTo(UPDATED_VERIFY_DATE);
        assertThat(testOrganisationLog.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testOrganisationLog.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOrganisationLog.getVerifyByLogin()).isEqualTo(UPDATED_VERIFY_BY_LOGIN);
        assertThat(testOrganisationLog.getCreatedByName()).isEqualTo(UPDATED_CREATED_BY_NAME);

        // Validate the OrganisationLog in Elasticsearch
        verify(mockOrganisationLogSearchRepository, times(1)).save(testOrganisationLog);
    }

    @Test
    @Transactional
    public void updateNonExistingOrganisationLog() throws Exception {
        int databaseSizeBeforeUpdate = organisationLogRepository.findAll().size();

        // Create the OrganisationLog
        OrganisationLogDTO organisationLogDTO = organisationLogMapper.toDto(organisationLog);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrganisationLogMockMvc.perform(put("/api/organisation-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(organisationLogDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrganisationLog in the database
        List<OrganisationLog> organisationLogList = organisationLogRepository.findAll();
        assertThat(organisationLogList).hasSize(databaseSizeBeforeUpdate);

        // Validate the OrganisationLog in Elasticsearch
        verify(mockOrganisationLogSearchRepository, times(0)).save(organisationLog);
    }

    @Test
    @Transactional
    public void deleteOrganisationLog() throws Exception {
        // Initialize the database
        organisationLogRepository.saveAndFlush(organisationLog);

        int databaseSizeBeforeDelete = organisationLogRepository.findAll().size();

        // Delete the organisationLog
        restOrganisationLogMockMvc.perform(delete("/api/organisation-logs/{id}", organisationLog.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrganisationLog> organisationLogList = organisationLogRepository.findAll();
        assertThat(organisationLogList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the OrganisationLog in Elasticsearch
        verify(mockOrganisationLogSearchRepository, times(1)).deleteById(organisationLog.getId());
    }

    @Test
    @Transactional
    public void searchOrganisationLog() throws Exception {
        // Initialize the database
        organisationLogRepository.saveAndFlush(organisationLog);
        when(mockOrganisationLogSearchRepository.search(queryStringQuery("id:" + organisationLog.getId())))
            .thenReturn(Collections.singletonList(organisationLog));
        // Search the organisationLog
        restOrganisationLogMockMvc.perform(get("/api/_search/organisation-logs?query=id:" + organisationLog.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(organisationLog.getId().intValue())))
            .andExpect(jsonPath("$.[*].organisationId").value(hasItem(DEFAULT_ORGANISATION_ID.intValue())))
            .andExpect(jsonPath("$.[*].createdByLogin").value(hasItem(DEFAULT_CREATED_BY_LOGIN)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].verifyByName").value(hasItem(DEFAULT_VERIFY_BY_NAME)))
            .andExpect(jsonPath("$.[*].verifyDate").value(hasItem(sameInstant(DEFAULT_VERIFY_DATE))))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].verifyByLogin").value(hasItem(DEFAULT_VERIFY_BY_LOGIN)))
            .andExpect(jsonPath("$.[*].createdByName").value(hasItem(DEFAULT_CREATED_BY_NAME)));
    }
}
