package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.EsignError;
import com.hartron.esignharyana.repository.EsignErrorRepository;
import com.hartron.esignharyana.repository.search.EsignErrorSearchRepository;
import com.hartron.esignharyana.service.EsignErrorService;
import com.hartron.esignharyana.service.dto.EsignErrorDTO;
import com.hartron.esignharyana.service.mapper.EsignErrorMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EsignErrorResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class EsignErrorResourceIT {

    private static final String DEFAULT_ERROR_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ERROR_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_ERROR_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_ERROR_MESSAGE = "BBBBBBBBBB";

    private static final String DEFAULT_ERROR_STAGE = "AAAAAAAAAA";
    private static final String UPDATED_ERROR_STAGE = "BBBBBBBBBB";

    @Autowired
    private EsignErrorRepository esignErrorRepository;

    @Autowired
    private EsignErrorMapper esignErrorMapper;

    @Autowired
    private EsignErrorService esignErrorService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.EsignErrorSearchRepositoryMockConfiguration
     */
    @Autowired
    private EsignErrorSearchRepository mockEsignErrorSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEsignErrorMockMvc;

    private EsignError esignError;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EsignErrorResource esignErrorResource = new EsignErrorResource(esignErrorService);
        this.restEsignErrorMockMvc = MockMvcBuilders.standaloneSetup(esignErrorResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EsignError createEntity(EntityManager em) {
        EsignError esignError = new EsignError()
            .errorCode(DEFAULT_ERROR_CODE)
            .errorMessage(DEFAULT_ERROR_MESSAGE)
            .errorStage(DEFAULT_ERROR_STAGE);
        return esignError;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EsignError createUpdatedEntity(EntityManager em) {
        EsignError esignError = new EsignError()
            .errorCode(UPDATED_ERROR_CODE)
            .errorMessage(UPDATED_ERROR_MESSAGE)
            .errorStage(UPDATED_ERROR_STAGE);
        return esignError;
    }

    @BeforeEach
    public void initTest() {
        esignError = createEntity(em);
    }

    @Test
    @Transactional
    public void createEsignError() throws Exception {
        int databaseSizeBeforeCreate = esignErrorRepository.findAll().size();

        // Create the EsignError
        EsignErrorDTO esignErrorDTO = esignErrorMapper.toDto(esignError);
        restEsignErrorMockMvc.perform(post("/api/esign-errors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(esignErrorDTO)))
            .andExpect(status().isCreated());

        // Validate the EsignError in the database
        List<EsignError> esignErrorList = esignErrorRepository.findAll();
        assertThat(esignErrorList).hasSize(databaseSizeBeforeCreate + 1);
        EsignError testEsignError = esignErrorList.get(esignErrorList.size() - 1);
        assertThat(testEsignError.getErrorCode()).isEqualTo(DEFAULT_ERROR_CODE);
        assertThat(testEsignError.getErrorMessage()).isEqualTo(DEFAULT_ERROR_MESSAGE);
        assertThat(testEsignError.getErrorStage()).isEqualTo(DEFAULT_ERROR_STAGE);

        // Validate the EsignError in Elasticsearch
        verify(mockEsignErrorSearchRepository, times(1)).save(testEsignError);
    }

    @Test
    @Transactional
    public void createEsignErrorWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = esignErrorRepository.findAll().size();

        // Create the EsignError with an existing ID
        esignError.setId(1L);
        EsignErrorDTO esignErrorDTO = esignErrorMapper.toDto(esignError);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEsignErrorMockMvc.perform(post("/api/esign-errors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(esignErrorDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EsignError in the database
        List<EsignError> esignErrorList = esignErrorRepository.findAll();
        assertThat(esignErrorList).hasSize(databaseSizeBeforeCreate);

        // Validate the EsignError in Elasticsearch
        verify(mockEsignErrorSearchRepository, times(0)).save(esignError);
    }


    @Test
    @Transactional
    public void getAllEsignErrors() throws Exception {
        // Initialize the database
        esignErrorRepository.saveAndFlush(esignError);

        // Get all the esignErrorList
        restEsignErrorMockMvc.perform(get("/api/esign-errors?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(esignError.getId().intValue())))
            .andExpect(jsonPath("$.[*].errorCode").value(hasItem(DEFAULT_ERROR_CODE)))
            .andExpect(jsonPath("$.[*].errorMessage").value(hasItem(DEFAULT_ERROR_MESSAGE)))
            .andExpect(jsonPath("$.[*].errorStage").value(hasItem(DEFAULT_ERROR_STAGE)));
    }
    
    @Test
    @Transactional
    public void getEsignError() throws Exception {
        // Initialize the database
        esignErrorRepository.saveAndFlush(esignError);

        // Get the esignError
        restEsignErrorMockMvc.perform(get("/api/esign-errors/{id}", esignError.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(esignError.getId().intValue()))
            .andExpect(jsonPath("$.errorCode").value(DEFAULT_ERROR_CODE))
            .andExpect(jsonPath("$.errorMessage").value(DEFAULT_ERROR_MESSAGE))
            .andExpect(jsonPath("$.errorStage").value(DEFAULT_ERROR_STAGE));
    }

    @Test
    @Transactional
    public void getNonExistingEsignError() throws Exception {
        // Get the esignError
        restEsignErrorMockMvc.perform(get("/api/esign-errors/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEsignError() throws Exception {
        // Initialize the database
        esignErrorRepository.saveAndFlush(esignError);

        int databaseSizeBeforeUpdate = esignErrorRepository.findAll().size();

        // Update the esignError
        EsignError updatedEsignError = esignErrorRepository.findById(esignError.getId()).get();
        // Disconnect from session so that the updates on updatedEsignError are not directly saved in db
        em.detach(updatedEsignError);
        updatedEsignError
            .errorCode(UPDATED_ERROR_CODE)
            .errorMessage(UPDATED_ERROR_MESSAGE)
            .errorStage(UPDATED_ERROR_STAGE);
        EsignErrorDTO esignErrorDTO = esignErrorMapper.toDto(updatedEsignError);

        restEsignErrorMockMvc.perform(put("/api/esign-errors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(esignErrorDTO)))
            .andExpect(status().isOk());

        // Validate the EsignError in the database
        List<EsignError> esignErrorList = esignErrorRepository.findAll();
        assertThat(esignErrorList).hasSize(databaseSizeBeforeUpdate);
        EsignError testEsignError = esignErrorList.get(esignErrorList.size() - 1);
        assertThat(testEsignError.getErrorCode()).isEqualTo(UPDATED_ERROR_CODE);
        assertThat(testEsignError.getErrorMessage()).isEqualTo(UPDATED_ERROR_MESSAGE);
        assertThat(testEsignError.getErrorStage()).isEqualTo(UPDATED_ERROR_STAGE);

        // Validate the EsignError in Elasticsearch
        verify(mockEsignErrorSearchRepository, times(1)).save(testEsignError);
    }

    @Test
    @Transactional
    public void updateNonExistingEsignError() throws Exception {
        int databaseSizeBeforeUpdate = esignErrorRepository.findAll().size();

        // Create the EsignError
        EsignErrorDTO esignErrorDTO = esignErrorMapper.toDto(esignError);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEsignErrorMockMvc.perform(put("/api/esign-errors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(esignErrorDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EsignError in the database
        List<EsignError> esignErrorList = esignErrorRepository.findAll();
        assertThat(esignErrorList).hasSize(databaseSizeBeforeUpdate);

        // Validate the EsignError in Elasticsearch
        verify(mockEsignErrorSearchRepository, times(0)).save(esignError);
    }

    @Test
    @Transactional
    public void deleteEsignError() throws Exception {
        // Initialize the database
        esignErrorRepository.saveAndFlush(esignError);

        int databaseSizeBeforeDelete = esignErrorRepository.findAll().size();

        // Delete the esignError
        restEsignErrorMockMvc.perform(delete("/api/esign-errors/{id}", esignError.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EsignError> esignErrorList = esignErrorRepository.findAll();
        assertThat(esignErrorList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the EsignError in Elasticsearch
        verify(mockEsignErrorSearchRepository, times(1)).deleteById(esignError.getId());
    }

    @Test
    @Transactional
    public void searchEsignError() throws Exception {
        // Initialize the database
        esignErrorRepository.saveAndFlush(esignError);
        when(mockEsignErrorSearchRepository.search(queryStringQuery("id:" + esignError.getId())))
            .thenReturn(Collections.singletonList(esignError));
        // Search the esignError
        restEsignErrorMockMvc.perform(get("/api/_search/esign-errors?query=id:" + esignError.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(esignError.getId().intValue())))
            .andExpect(jsonPath("$.[*].errorCode").value(hasItem(DEFAULT_ERROR_CODE)))
            .andExpect(jsonPath("$.[*].errorMessage").value(hasItem(DEFAULT_ERROR_MESSAGE)))
            .andExpect(jsonPath("$.[*].errorStage").value(hasItem(DEFAULT_ERROR_STAGE)));
    }
}
