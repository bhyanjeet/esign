package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.UserDetails;
import com.hartron.esignharyana.repository.UserDetailsRepository;
import com.hartron.esignharyana.repository.search.UserDetailsSearchRepository;
import com.hartron.esignharyana.service.UserDetailsService;
import com.hartron.esignharyana.service.dto.UserDetailsDTO;
import com.hartron.esignharyana.service.mapper.UserDetailsMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UserDetailsResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class UserDetailsResourceIT {

    private static final String DEFAULT_USER_FULL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_USER_FULL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_USER_PHONE_MOBILE = "AAAAAAAAAA";
    private static final String UPDATED_USER_PHONE_MOBILE = "BBBBBBBBBB";

    private static final String DEFAULT_USER_PHONE_LANDLINE = "AAAAAAAAAA";
    private static final String UPDATED_USER_PHONE_LANDLINE = "BBBBBBBBBB";

    private static final String DEFAULT_USER_OFFICIAL_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_USER_OFFICIAL_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_USER_ALTERNATIVE_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_USER_ALTERNATIVE_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_USER_LOGIN_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_USER_LOGIN_PASSWORD = "BBBBBBBBBB";

    private static final String DEFAULT_USER_LOGIN_PASSWORD_SALT = "AAAAAAAAAA";
    private static final String UPDATED_USER_LOGIN_PASSWORD_SALT = "BBBBBBBBBB";

    private static final String DEFAULT_USER_TRANSACTION_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_USER_TRANSACTION_PASSWORD = "BBBBBBBBBB";

    private static final String DEFAULT_USER_TRANSACTION_PASSWORD_SALT = "AAAAAAAAAA";
    private static final String UPDATED_USER_TRANSACTION_PASSWORD_SALT = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LAST_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_VERIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_VERIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_VERIFIED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VERIFIED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final String DEFAULT_USER_ID_CODE = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_USER_PIN = "AAAAAAAAAA";
    private static final String UPDATED_USER_PIN = "BBBBBBBBBB";

    private static final String DEFAULT_ORGANISATION_UNIT = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_UNIT = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_COUNRTY = "AAAAAAAAAA";
    private static final String UPDATED_COUNRTY = "BBBBBBBBBB";

    private static final String DEFAULT_POSTAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POSTAL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_PHOTOGRAPH = "AAAAAAAAAA";
    private static final String UPDATED_PHOTOGRAPH = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DOB = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DOB = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_GENDER = "AAAAAAAAAA";
    private static final String UPDATED_GENDER = "BBBBBBBBBB";

    private static final String DEFAULT_PAN = "AAAAAAAAAA";
    private static final String UPDATED_PAN = "BBBBBBBBBB";

    private static final String DEFAULT_AADHAAR = "AAAAAAAAAA";
    private static final String UPDATED_AADHAAR = "BBBBBBBBBB";

    private static final String DEFAULT_PAN_ATTACHMENT = "AAAAAAAAAA";
    private static final String UPDATED_PAN_ATTACHMENT = "BBBBBBBBBB";

    private static final String DEFAULT_EMPLOYEE_ID = "AAAAAAAAAA";
    private static final String UPDATED_EMPLOYEE_ID = "BBBBBBBBBB";

    private static final String DEFAULT_EMPLOYEE_CARD_ATTACHMENT = "AAAAAAAAAA";
    private static final String UPDATED_EMPLOYEE_CARD_ATTACHMENT = "BBBBBBBBBB";

    private static final String DEFAULT_SIGNER_ID = "AAAAAAAAAA";
    private static final String UPDATED_SIGNER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_ACTION = "AAAAAAAAAA";
    private static final String UPDATED_LAST_ACTION = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_STATUS = "BBBBBBBBBB";

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    @Autowired
    private UserDetailsMapper userDetailsMapper;

    @Autowired
    private UserDetailsService userDetailsService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.UserDetailsSearchRepositoryMockConfiguration
     */
    @Autowired
    private UserDetailsSearchRepository mockUserDetailsSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserDetailsMockMvc;

    private UserDetails userDetails;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserDetailsResource userDetailsResource = new UserDetailsResource(userDetailsService);
        this.restUserDetailsMockMvc = MockMvcBuilders.standaloneSetup(userDetailsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserDetails createEntity(EntityManager em) {
        UserDetails userDetails = new UserDetails()
            .userFullName(DEFAULT_USER_FULL_NAME)
            .userPhoneMobile(DEFAULT_USER_PHONE_MOBILE)
            .userPhoneLandline(DEFAULT_USER_PHONE_LANDLINE)
            .userOfficialEmail(DEFAULT_USER_OFFICIAL_EMAIL)
            .userAlternativeEmail(DEFAULT_USER_ALTERNATIVE_EMAIL)
            .userLoginPassword(DEFAULT_USER_LOGIN_PASSWORD)
            .userLoginPasswordSalt(DEFAULT_USER_LOGIN_PASSWORD_SALT)
            .userTransactionPassword(DEFAULT_USER_TRANSACTION_PASSWORD)
            .userTransactionPasswordSalt(DEFAULT_USER_TRANSACTION_PASSWORD_SALT)
            .createdBy(DEFAULT_CREATED_BY)
            .createdOn(DEFAULT_CREATED_ON)
            .lastUpdatedBy(DEFAULT_LAST_UPDATED_BY)
            .lastUpdatedOn(DEFAULT_LAST_UPDATED_ON)
            .verifiedBy(DEFAULT_VERIFIED_BY)
            .verifiedOn(DEFAULT_VERIFIED_ON)
            .remarks(DEFAULT_REMARKS)
            .userIdCode(DEFAULT_USER_ID_CODE)
            .status(DEFAULT_STATUS)
            .username(DEFAULT_USERNAME)
            .userPIN(DEFAULT_USER_PIN)
            .organisationUnit(DEFAULT_ORGANISATION_UNIT)
            .address(DEFAULT_ADDRESS)
            .counrty(DEFAULT_COUNRTY)
            .postalCode(DEFAULT_POSTAL_CODE)
            .photograph(DEFAULT_PHOTOGRAPH)
            .dob(DEFAULT_DOB)
            .gender(DEFAULT_GENDER)
            .pan(DEFAULT_PAN)
            .aadhaar(DEFAULT_AADHAAR)
            .panAttachment(DEFAULT_PAN_ATTACHMENT)
            .employeeId(DEFAULT_EMPLOYEE_ID)
            .employeeCardAttachment(DEFAULT_EMPLOYEE_CARD_ATTACHMENT)
            .signerId(DEFAULT_SIGNER_ID)
            .lastAction(DEFAULT_LAST_ACTION)
            .accountStatus(DEFAULT_ACCOUNT_STATUS);
        return userDetails;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserDetails createUpdatedEntity(EntityManager em) {
        UserDetails userDetails = new UserDetails()
            .userFullName(UPDATED_USER_FULL_NAME)
            .userPhoneMobile(UPDATED_USER_PHONE_MOBILE)
            .userPhoneLandline(UPDATED_USER_PHONE_LANDLINE)
            .userOfficialEmail(UPDATED_USER_OFFICIAL_EMAIL)
            .userAlternativeEmail(UPDATED_USER_ALTERNATIVE_EMAIL)
            .userLoginPassword(UPDATED_USER_LOGIN_PASSWORD)
            .userLoginPasswordSalt(UPDATED_USER_LOGIN_PASSWORD_SALT)
            .userTransactionPassword(UPDATED_USER_TRANSACTION_PASSWORD)
            .userTransactionPasswordSalt(UPDATED_USER_TRANSACTION_PASSWORD_SALT)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS)
            .userIdCode(UPDATED_USER_ID_CODE)
            .status(UPDATED_STATUS)
            .username(UPDATED_USERNAME)
            .userPIN(UPDATED_USER_PIN)
            .organisationUnit(UPDATED_ORGANISATION_UNIT)
            .address(UPDATED_ADDRESS)
            .counrty(UPDATED_COUNRTY)
            .postalCode(UPDATED_POSTAL_CODE)
            .photograph(UPDATED_PHOTOGRAPH)
            .dob(UPDATED_DOB)
            .gender(UPDATED_GENDER)
            .pan(UPDATED_PAN)
            .aadhaar(UPDATED_AADHAAR)
            .panAttachment(UPDATED_PAN_ATTACHMENT)
            .employeeId(UPDATED_EMPLOYEE_ID)
            .employeeCardAttachment(UPDATED_EMPLOYEE_CARD_ATTACHMENT)
            .signerId(UPDATED_SIGNER_ID)
            .lastAction(UPDATED_LAST_ACTION)
            .accountStatus(UPDATED_ACCOUNT_STATUS);
        return userDetails;
    }

    @BeforeEach
    public void initTest() {
        userDetails = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserDetails() throws Exception {
        int databaseSizeBeforeCreate = userDetailsRepository.findAll().size();

        // Create the UserDetails
        UserDetailsDTO userDetailsDTO = userDetailsMapper.toDto(userDetails);
        restUserDetailsMockMvc.perform(post("/api/user-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDetailsDTO)))
            .andExpect(status().isCreated());

        // Validate the UserDetails in the database
        List<UserDetails> userDetailsList = userDetailsRepository.findAll();
        assertThat(userDetailsList).hasSize(databaseSizeBeforeCreate + 1);
        UserDetails testUserDetails = userDetailsList.get(userDetailsList.size() - 1);
        assertThat(testUserDetails.getUserFullName()).isEqualTo(DEFAULT_USER_FULL_NAME);
        assertThat(testUserDetails.getUserPhoneMobile()).isEqualTo(DEFAULT_USER_PHONE_MOBILE);
        assertThat(testUserDetails.getUserPhoneLandline()).isEqualTo(DEFAULT_USER_PHONE_LANDLINE);
        assertThat(testUserDetails.getUserOfficialEmail()).isEqualTo(DEFAULT_USER_OFFICIAL_EMAIL);
        assertThat(testUserDetails.getUserAlternativeEmail()).isEqualTo(DEFAULT_USER_ALTERNATIVE_EMAIL);
        assertThat(testUserDetails.getUserLoginPassword()).isEqualTo(DEFAULT_USER_LOGIN_PASSWORD);
        assertThat(testUserDetails.getUserLoginPasswordSalt()).isEqualTo(DEFAULT_USER_LOGIN_PASSWORD_SALT);
        assertThat(testUserDetails.getUserTransactionPassword()).isEqualTo(DEFAULT_USER_TRANSACTION_PASSWORD);
        assertThat(testUserDetails.getUserTransactionPasswordSalt()).isEqualTo(DEFAULT_USER_TRANSACTION_PASSWORD_SALT);
        assertThat(testUserDetails.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testUserDetails.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testUserDetails.getLastUpdatedBy()).isEqualTo(DEFAULT_LAST_UPDATED_BY);
        assertThat(testUserDetails.getLastUpdatedOn()).isEqualTo(DEFAULT_LAST_UPDATED_ON);
        assertThat(testUserDetails.getVerifiedBy()).isEqualTo(DEFAULT_VERIFIED_BY);
        assertThat(testUserDetails.getVerifiedOn()).isEqualTo(DEFAULT_VERIFIED_ON);
        assertThat(testUserDetails.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testUserDetails.getUserIdCode()).isEqualTo(DEFAULT_USER_ID_CODE);
        assertThat(testUserDetails.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testUserDetails.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testUserDetails.getUserPIN()).isEqualTo(DEFAULT_USER_PIN);
        assertThat(testUserDetails.getOrganisationUnit()).isEqualTo(DEFAULT_ORGANISATION_UNIT);
        assertThat(testUserDetails.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testUserDetails.getCounrty()).isEqualTo(DEFAULT_COUNRTY);
        assertThat(testUserDetails.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
        assertThat(testUserDetails.getPhotograph()).isEqualTo(DEFAULT_PHOTOGRAPH);
        assertThat(testUserDetails.getDob()).isEqualTo(DEFAULT_DOB);
        assertThat(testUserDetails.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testUserDetails.getPan()).isEqualTo(DEFAULT_PAN);
        assertThat(testUserDetails.getAadhaar()).isEqualTo(DEFAULT_AADHAAR);
        assertThat(testUserDetails.getPanAttachment()).isEqualTo(DEFAULT_PAN_ATTACHMENT);
        assertThat(testUserDetails.getEmployeeId()).isEqualTo(DEFAULT_EMPLOYEE_ID);
        assertThat(testUserDetails.getEmployeeCardAttachment()).isEqualTo(DEFAULT_EMPLOYEE_CARD_ATTACHMENT);
        assertThat(testUserDetails.getSignerId()).isEqualTo(DEFAULT_SIGNER_ID);
        assertThat(testUserDetails.getLastAction()).isEqualTo(DEFAULT_LAST_ACTION);
        assertThat(testUserDetails.getAccountStatus()).isEqualTo(DEFAULT_ACCOUNT_STATUS);

        // Validate the UserDetails in Elasticsearch
        verify(mockUserDetailsSearchRepository, times(1)).save(testUserDetails);
    }

    @Test
    @Transactional
    public void createUserDetailsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userDetailsRepository.findAll().size();

        // Create the UserDetails with an existing ID
        userDetails.setId(1L);
        UserDetailsDTO userDetailsDTO = userDetailsMapper.toDto(userDetails);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserDetailsMockMvc.perform(post("/api/user-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDetailsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserDetails in the database
        List<UserDetails> userDetailsList = userDetailsRepository.findAll();
        assertThat(userDetailsList).hasSize(databaseSizeBeforeCreate);

        // Validate the UserDetails in Elasticsearch
        verify(mockUserDetailsSearchRepository, times(0)).save(userDetails);
    }


    @Test
    @Transactional
    public void checkUserPhoneMobileIsRequired() throws Exception {
        int databaseSizeBeforeTest = userDetailsRepository.findAll().size();
        // set the field null
        userDetails.setUserPhoneMobile(null);

        // Create the UserDetails, which fails.
        UserDetailsDTO userDetailsDTO = userDetailsMapper.toDto(userDetails);

        restUserDetailsMockMvc.perform(post("/api/user-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDetailsDTO)))
            .andExpect(status().isBadRequest());

        List<UserDetails> userDetailsList = userDetailsRepository.findAll();
        assertThat(userDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUserOfficialEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = userDetailsRepository.findAll().size();
        // set the field null
        userDetails.setUserOfficialEmail(null);

        // Create the UserDetails, which fails.
        UserDetailsDTO userDetailsDTO = userDetailsMapper.toDto(userDetails);

        restUserDetailsMockMvc.perform(post("/api/user-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDetailsDTO)))
            .andExpect(status().isBadRequest());

        List<UserDetails> userDetailsList = userDetailsRepository.findAll();
        assertThat(userDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUserDetails() throws Exception {
        // Initialize the database
        userDetailsRepository.saveAndFlush(userDetails);

        // Get all the userDetailsList
        restUserDetailsMockMvc.perform(get("/api/user-details?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].userFullName").value(hasItem(DEFAULT_USER_FULL_NAME)))
            .andExpect(jsonPath("$.[*].userPhoneMobile").value(hasItem(DEFAULT_USER_PHONE_MOBILE)))
            .andExpect(jsonPath("$.[*].userPhoneLandline").value(hasItem(DEFAULT_USER_PHONE_LANDLINE)))
            .andExpect(jsonPath("$.[*].userOfficialEmail").value(hasItem(DEFAULT_USER_OFFICIAL_EMAIL)))
            .andExpect(jsonPath("$.[*].userAlternativeEmail").value(hasItem(DEFAULT_USER_ALTERNATIVE_EMAIL)))
            .andExpect(jsonPath("$.[*].userLoginPassword").value(hasItem(DEFAULT_USER_LOGIN_PASSWORD)))
            .andExpect(jsonPath("$.[*].userLoginPasswordSalt").value(hasItem(DEFAULT_USER_LOGIN_PASSWORD_SALT)))
            .andExpect(jsonPath("$.[*].userTransactionPassword").value(hasItem(DEFAULT_USER_TRANSACTION_PASSWORD)))
            .andExpect(jsonPath("$.[*].userTransactionPasswordSalt").value(hasItem(DEFAULT_USER_TRANSACTION_PASSWORD_SALT)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].userIdCode").value(hasItem(DEFAULT_USER_ID_CODE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME)))
            .andExpect(jsonPath("$.[*].userPIN").value(hasItem(DEFAULT_USER_PIN)))
            .andExpect(jsonPath("$.[*].organisationUnit").value(hasItem(DEFAULT_ORGANISATION_UNIT)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].counrty").value(hasItem(DEFAULT_COUNRTY)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].photograph").value(hasItem(DEFAULT_PHOTOGRAPH)))
            .andExpect(jsonPath("$.[*].dob").value(hasItem(DEFAULT_DOB.toString())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            .andExpect(jsonPath("$.[*].pan").value(hasItem(DEFAULT_PAN)))
            .andExpect(jsonPath("$.[*].aadhaar").value(hasItem(DEFAULT_AADHAAR)))
            .andExpect(jsonPath("$.[*].panAttachment").value(hasItem(DEFAULT_PAN_ATTACHMENT)))
            .andExpect(jsonPath("$.[*].employeeId").value(hasItem(DEFAULT_EMPLOYEE_ID)))
            .andExpect(jsonPath("$.[*].employeeCardAttachment").value(hasItem(DEFAULT_EMPLOYEE_CARD_ATTACHMENT)))
            .andExpect(jsonPath("$.[*].signerId").value(hasItem(DEFAULT_SIGNER_ID)))
            .andExpect(jsonPath("$.[*].lastAction").value(hasItem(DEFAULT_LAST_ACTION)))
            .andExpect(jsonPath("$.[*].accountStatus").value(hasItem(DEFAULT_ACCOUNT_STATUS)));
    }
    
    @Test
    @Transactional
    public void getUserDetails() throws Exception {
        // Initialize the database
        userDetailsRepository.saveAndFlush(userDetails);

        // Get the userDetails
        restUserDetailsMockMvc.perform(get("/api/user-details/{id}", userDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userDetails.getId().intValue()))
            .andExpect(jsonPath("$.userFullName").value(DEFAULT_USER_FULL_NAME))
            .andExpect(jsonPath("$.userPhoneMobile").value(DEFAULT_USER_PHONE_MOBILE))
            .andExpect(jsonPath("$.userPhoneLandline").value(DEFAULT_USER_PHONE_LANDLINE))
            .andExpect(jsonPath("$.userOfficialEmail").value(DEFAULT_USER_OFFICIAL_EMAIL))
            .andExpect(jsonPath("$.userAlternativeEmail").value(DEFAULT_USER_ALTERNATIVE_EMAIL))
            .andExpect(jsonPath("$.userLoginPassword").value(DEFAULT_USER_LOGIN_PASSWORD))
            .andExpect(jsonPath("$.userLoginPasswordSalt").value(DEFAULT_USER_LOGIN_PASSWORD_SALT))
            .andExpect(jsonPath("$.userTransactionPassword").value(DEFAULT_USER_TRANSACTION_PASSWORD))
            .andExpect(jsonPath("$.userTransactionPasswordSalt").value(DEFAULT_USER_TRANSACTION_PASSWORD_SALT))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.lastUpdatedBy").value(DEFAULT_LAST_UPDATED_BY))
            .andExpect(jsonPath("$.lastUpdatedOn").value(DEFAULT_LAST_UPDATED_ON.toString()))
            .andExpect(jsonPath("$.verifiedBy").value(DEFAULT_VERIFIED_BY))
            .andExpect(jsonPath("$.verifiedOn").value(DEFAULT_VERIFIED_ON.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS))
            .andExpect(jsonPath("$.userIdCode").value(DEFAULT_USER_ID_CODE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME))
            .andExpect(jsonPath("$.userPIN").value(DEFAULT_USER_PIN))
            .andExpect(jsonPath("$.organisationUnit").value(DEFAULT_ORGANISATION_UNIT))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.counrty").value(DEFAULT_COUNRTY))
            .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE))
            .andExpect(jsonPath("$.photograph").value(DEFAULT_PHOTOGRAPH))
            .andExpect(jsonPath("$.dob").value(DEFAULT_DOB.toString()))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER))
            .andExpect(jsonPath("$.pan").value(DEFAULT_PAN))
            .andExpect(jsonPath("$.aadhaar").value(DEFAULT_AADHAAR))
            .andExpect(jsonPath("$.panAttachment").value(DEFAULT_PAN_ATTACHMENT))
            .andExpect(jsonPath("$.employeeId").value(DEFAULT_EMPLOYEE_ID))
            .andExpect(jsonPath("$.employeeCardAttachment").value(DEFAULT_EMPLOYEE_CARD_ATTACHMENT))
            .andExpect(jsonPath("$.signerId").value(DEFAULT_SIGNER_ID))
            .andExpect(jsonPath("$.lastAction").value(DEFAULT_LAST_ACTION))
            .andExpect(jsonPath("$.accountStatus").value(DEFAULT_ACCOUNT_STATUS));
    }

    @Test
    @Transactional
    public void getNonExistingUserDetails() throws Exception {
        // Get the userDetails
        restUserDetailsMockMvc.perform(get("/api/user-details/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserDetails() throws Exception {
        // Initialize the database
        userDetailsRepository.saveAndFlush(userDetails);

        int databaseSizeBeforeUpdate = userDetailsRepository.findAll().size();

        // Update the userDetails
        UserDetails updatedUserDetails = userDetailsRepository.findById(userDetails.getId()).get();
        // Disconnect from session so that the updates on updatedUserDetails are not directly saved in db
        em.detach(updatedUserDetails);
        updatedUserDetails
            .userFullName(UPDATED_USER_FULL_NAME)
            .userPhoneMobile(UPDATED_USER_PHONE_MOBILE)
            .userPhoneLandline(UPDATED_USER_PHONE_LANDLINE)
            .userOfficialEmail(UPDATED_USER_OFFICIAL_EMAIL)
            .userAlternativeEmail(UPDATED_USER_ALTERNATIVE_EMAIL)
            .userLoginPassword(UPDATED_USER_LOGIN_PASSWORD)
            .userLoginPasswordSalt(UPDATED_USER_LOGIN_PASSWORD_SALT)
            .userTransactionPassword(UPDATED_USER_TRANSACTION_PASSWORD)
            .userTransactionPasswordSalt(UPDATED_USER_TRANSACTION_PASSWORD_SALT)
            .createdBy(UPDATED_CREATED_BY)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS)
            .userIdCode(UPDATED_USER_ID_CODE)
            .status(UPDATED_STATUS)
            .username(UPDATED_USERNAME)
            .userPIN(UPDATED_USER_PIN)
            .organisationUnit(UPDATED_ORGANISATION_UNIT)
            .address(UPDATED_ADDRESS)
            .counrty(UPDATED_COUNRTY)
            .postalCode(UPDATED_POSTAL_CODE)
            .photograph(UPDATED_PHOTOGRAPH)
            .dob(UPDATED_DOB)
            .gender(UPDATED_GENDER)
            .pan(UPDATED_PAN)
            .aadhaar(UPDATED_AADHAAR)
            .panAttachment(UPDATED_PAN_ATTACHMENT)
            .employeeId(UPDATED_EMPLOYEE_ID)
            .employeeCardAttachment(UPDATED_EMPLOYEE_CARD_ATTACHMENT)
            .signerId(UPDATED_SIGNER_ID)
            .lastAction(UPDATED_LAST_ACTION)
            .accountStatus(UPDATED_ACCOUNT_STATUS);
        UserDetailsDTO userDetailsDTO = userDetailsMapper.toDto(updatedUserDetails);

        restUserDetailsMockMvc.perform(put("/api/user-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDetailsDTO)))
            .andExpect(status().isOk());

        // Validate the UserDetails in the database
        List<UserDetails> userDetailsList = userDetailsRepository.findAll();
        assertThat(userDetailsList).hasSize(databaseSizeBeforeUpdate);
        UserDetails testUserDetails = userDetailsList.get(userDetailsList.size() - 1);
        assertThat(testUserDetails.getUserFullName()).isEqualTo(UPDATED_USER_FULL_NAME);
        assertThat(testUserDetails.getUserPhoneMobile()).isEqualTo(UPDATED_USER_PHONE_MOBILE);
        assertThat(testUserDetails.getUserPhoneLandline()).isEqualTo(UPDATED_USER_PHONE_LANDLINE);
        assertThat(testUserDetails.getUserOfficialEmail()).isEqualTo(UPDATED_USER_OFFICIAL_EMAIL);
        assertThat(testUserDetails.getUserAlternativeEmail()).isEqualTo(UPDATED_USER_ALTERNATIVE_EMAIL);
        assertThat(testUserDetails.getUserLoginPassword()).isEqualTo(UPDATED_USER_LOGIN_PASSWORD);
        assertThat(testUserDetails.getUserLoginPasswordSalt()).isEqualTo(UPDATED_USER_LOGIN_PASSWORD_SALT);
        assertThat(testUserDetails.getUserTransactionPassword()).isEqualTo(UPDATED_USER_TRANSACTION_PASSWORD);
        assertThat(testUserDetails.getUserTransactionPasswordSalt()).isEqualTo(UPDATED_USER_TRANSACTION_PASSWORD_SALT);
        assertThat(testUserDetails.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testUserDetails.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testUserDetails.getLastUpdatedBy()).isEqualTo(UPDATED_LAST_UPDATED_BY);
        assertThat(testUserDetails.getLastUpdatedOn()).isEqualTo(UPDATED_LAST_UPDATED_ON);
        assertThat(testUserDetails.getVerifiedBy()).isEqualTo(UPDATED_VERIFIED_BY);
        assertThat(testUserDetails.getVerifiedOn()).isEqualTo(UPDATED_VERIFIED_ON);
        assertThat(testUserDetails.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testUserDetails.getUserIdCode()).isEqualTo(UPDATED_USER_ID_CODE);
        assertThat(testUserDetails.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testUserDetails.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testUserDetails.getUserPIN()).isEqualTo(UPDATED_USER_PIN);
        assertThat(testUserDetails.getOrganisationUnit()).isEqualTo(UPDATED_ORGANISATION_UNIT);
        assertThat(testUserDetails.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testUserDetails.getCounrty()).isEqualTo(UPDATED_COUNRTY);
        assertThat(testUserDetails.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testUserDetails.getPhotograph()).isEqualTo(UPDATED_PHOTOGRAPH);
        assertThat(testUserDetails.getDob()).isEqualTo(UPDATED_DOB);
        assertThat(testUserDetails.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testUserDetails.getPan()).isEqualTo(UPDATED_PAN);
        assertThat(testUserDetails.getAadhaar()).isEqualTo(UPDATED_AADHAAR);
        assertThat(testUserDetails.getPanAttachment()).isEqualTo(UPDATED_PAN_ATTACHMENT);
        assertThat(testUserDetails.getEmployeeId()).isEqualTo(UPDATED_EMPLOYEE_ID);
        assertThat(testUserDetails.getEmployeeCardAttachment()).isEqualTo(UPDATED_EMPLOYEE_CARD_ATTACHMENT);
        assertThat(testUserDetails.getSignerId()).isEqualTo(UPDATED_SIGNER_ID);
        assertThat(testUserDetails.getLastAction()).isEqualTo(UPDATED_LAST_ACTION);
        assertThat(testUserDetails.getAccountStatus()).isEqualTo(UPDATED_ACCOUNT_STATUS);

        // Validate the UserDetails in Elasticsearch
        verify(mockUserDetailsSearchRepository, times(1)).save(testUserDetails);
    }

    @Test
    @Transactional
    public void updateNonExistingUserDetails() throws Exception {
        int databaseSizeBeforeUpdate = userDetailsRepository.findAll().size();

        // Create the UserDetails
        UserDetailsDTO userDetailsDTO = userDetailsMapper.toDto(userDetails);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserDetailsMockMvc.perform(put("/api/user-details")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userDetailsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserDetails in the database
        List<UserDetails> userDetailsList = userDetailsRepository.findAll();
        assertThat(userDetailsList).hasSize(databaseSizeBeforeUpdate);

        // Validate the UserDetails in Elasticsearch
        verify(mockUserDetailsSearchRepository, times(0)).save(userDetails);
    }

    @Test
    @Transactional
    public void deleteUserDetails() throws Exception {
        // Initialize the database
        userDetailsRepository.saveAndFlush(userDetails);

        int databaseSizeBeforeDelete = userDetailsRepository.findAll().size();

        // Delete the userDetails
        restUserDetailsMockMvc.perform(delete("/api/user-details/{id}", userDetails.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserDetails> userDetailsList = userDetailsRepository.findAll();
        assertThat(userDetailsList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the UserDetails in Elasticsearch
        verify(mockUserDetailsSearchRepository, times(1)).deleteById(userDetails.getId());
    }

    @Test
    @Transactional
    public void searchUserDetails() throws Exception {
        // Initialize the database
        userDetailsRepository.saveAndFlush(userDetails);
        when(mockUserDetailsSearchRepository.search(queryStringQuery("id:" + userDetails.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(userDetails), PageRequest.of(0, 1), 1));
        // Search the userDetails
        restUserDetailsMockMvc.perform(get("/api/_search/user-details?query=id:" + userDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].userFullName").value(hasItem(DEFAULT_USER_FULL_NAME)))
            .andExpect(jsonPath("$.[*].userPhoneMobile").value(hasItem(DEFAULT_USER_PHONE_MOBILE)))
            .andExpect(jsonPath("$.[*].userPhoneLandline").value(hasItem(DEFAULT_USER_PHONE_LANDLINE)))
            .andExpect(jsonPath("$.[*].userOfficialEmail").value(hasItem(DEFAULT_USER_OFFICIAL_EMAIL)))
            .andExpect(jsonPath("$.[*].userAlternativeEmail").value(hasItem(DEFAULT_USER_ALTERNATIVE_EMAIL)))
            .andExpect(jsonPath("$.[*].userLoginPassword").value(hasItem(DEFAULT_USER_LOGIN_PASSWORD)))
            .andExpect(jsonPath("$.[*].userLoginPasswordSalt").value(hasItem(DEFAULT_USER_LOGIN_PASSWORD_SALT)))
            .andExpect(jsonPath("$.[*].userTransactionPassword").value(hasItem(DEFAULT_USER_TRANSACTION_PASSWORD)))
            .andExpect(jsonPath("$.[*].userTransactionPasswordSalt").value(hasItem(DEFAULT_USER_TRANSACTION_PASSWORD_SALT)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].userIdCode").value(hasItem(DEFAULT_USER_ID_CODE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME)))
            .andExpect(jsonPath("$.[*].userPIN").value(hasItem(DEFAULT_USER_PIN)))
            .andExpect(jsonPath("$.[*].organisationUnit").value(hasItem(DEFAULT_ORGANISATION_UNIT)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].counrty").value(hasItem(DEFAULT_COUNRTY)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].photograph").value(hasItem(DEFAULT_PHOTOGRAPH)))
            .andExpect(jsonPath("$.[*].dob").value(hasItem(DEFAULT_DOB.toString())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            .andExpect(jsonPath("$.[*].pan").value(hasItem(DEFAULT_PAN)))
            .andExpect(jsonPath("$.[*].aadhaar").value(hasItem(DEFAULT_AADHAAR)))
            .andExpect(jsonPath("$.[*].panAttachment").value(hasItem(DEFAULT_PAN_ATTACHMENT)))
            .andExpect(jsonPath("$.[*].employeeId").value(hasItem(DEFAULT_EMPLOYEE_ID)))
            .andExpect(jsonPath("$.[*].employeeCardAttachment").value(hasItem(DEFAULT_EMPLOYEE_CARD_ATTACHMENT)))
            .andExpect(jsonPath("$.[*].signerId").value(hasItem(DEFAULT_SIGNER_ID)))
            .andExpect(jsonPath("$.[*].lastAction").value(hasItem(DEFAULT_LAST_ACTION)))
            .andExpect(jsonPath("$.[*].accountStatus").value(hasItem(DEFAULT_ACCOUNT_STATUS)));
    }
}
