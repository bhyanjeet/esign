package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.DistrictMaster;
import com.hartron.esignharyana.repository.DistrictMasterRepository;
import com.hartron.esignharyana.repository.search.DistrictMasterSearchRepository;
import com.hartron.esignharyana.service.DistrictMasterService;
import com.hartron.esignharyana.service.dto.DistrictMasterDTO;
import com.hartron.esignharyana.service.mapper.DistrictMasterMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DistrictMasterResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class DistrictMasterResourceIT {

    private static final String DEFAULT_DISTRICT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DISTRICT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DISTRICT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_DISTRICT_CODE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LAST_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_UPDATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_UPDATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_VERIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_VERIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_VERIFIED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VERIFIED_ON = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    @Autowired
    private DistrictMasterRepository districtMasterRepository;

    @Autowired
    private DistrictMasterMapper districtMasterMapper;

    @Autowired
    private DistrictMasterService districtMasterService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.DistrictMasterSearchRepositoryMockConfiguration
     */
    @Autowired
    private DistrictMasterSearchRepository mockDistrictMasterSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDistrictMasterMockMvc;

    private DistrictMaster districtMaster;

    @BeforeEach
    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final DistrictMasterResource districtMasterResource = new DistrictMasterResource(districtMasterService, userService);
//        this.restDistrictMasterMockMvc = MockMvcBuilders.standaloneSetup(districtMasterResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DistrictMaster createEntity(EntityManager em) {
        DistrictMaster districtMaster = new DistrictMaster()
            .districtName(DEFAULT_DISTRICT_NAME)
            .districtCode(DEFAULT_DISTRICT_CODE)
            .createdOn(DEFAULT_CREATED_ON)
            .lastUpdatedBy(DEFAULT_LAST_UPDATED_BY)
            .lastUpdatedOn(DEFAULT_LAST_UPDATED_ON)
            .verifiedBy(DEFAULT_VERIFIED_BY)
            .verifiedOn(DEFAULT_VERIFIED_ON)
            .remarks(DEFAULT_REMARKS);
        return districtMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DistrictMaster createUpdatedEntity(EntityManager em) {
        DistrictMaster districtMaster = new DistrictMaster()
            .districtName(UPDATED_DISTRICT_NAME)
            .districtCode(UPDATED_DISTRICT_CODE)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS);
        return districtMaster;
    }

    @BeforeEach
    public void initTest() {
        districtMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createDistrictMaster() throws Exception {
        int databaseSizeBeforeCreate = districtMasterRepository.findAll().size();

        // Create the DistrictMaster
        DistrictMasterDTO districtMasterDTO = districtMasterMapper.toDto(districtMaster);
        restDistrictMasterMockMvc.perform(post("/api/district-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(districtMasterDTO)))
            .andExpect(status().isCreated());

        // Validate the DistrictMaster in the database
        List<DistrictMaster> districtMasterList = districtMasterRepository.findAll();
        assertThat(districtMasterList).hasSize(databaseSizeBeforeCreate + 1);
        DistrictMaster testDistrictMaster = districtMasterList.get(districtMasterList.size() - 1);
        assertThat(testDistrictMaster.getDistrictName()).isEqualTo(DEFAULT_DISTRICT_NAME);
        assertThat(testDistrictMaster.getDistrictCode()).isEqualTo(DEFAULT_DISTRICT_CODE);
        assertThat(testDistrictMaster.getCreatedOn()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testDistrictMaster.getLastUpdatedBy()).isEqualTo(DEFAULT_LAST_UPDATED_BY);
        assertThat(testDistrictMaster.getLastUpdatedOn()).isEqualTo(DEFAULT_LAST_UPDATED_ON);
        assertThat(testDistrictMaster.getVerifiedBy()).isEqualTo(DEFAULT_VERIFIED_BY);
        assertThat(testDistrictMaster.getVerifiedOn()).isEqualTo(DEFAULT_VERIFIED_ON);
        assertThat(testDistrictMaster.getRemarks()).isEqualTo(DEFAULT_REMARKS);

        // Validate the DistrictMaster in Elasticsearch
        verify(mockDistrictMasterSearchRepository, times(1)).save(testDistrictMaster);
    }

    @Test
    @Transactional
    public void createDistrictMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = districtMasterRepository.findAll().size();

        // Create the DistrictMaster with an existing ID
        districtMaster.setId(1L);
        DistrictMasterDTO districtMasterDTO = districtMasterMapper.toDto(districtMaster);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDistrictMasterMockMvc.perform(post("/api/district-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(districtMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DistrictMaster in the database
        List<DistrictMaster> districtMasterList = districtMasterRepository.findAll();
        assertThat(districtMasterList).hasSize(databaseSizeBeforeCreate);

        // Validate the DistrictMaster in Elasticsearch
        verify(mockDistrictMasterSearchRepository, times(0)).save(districtMaster);
    }


    @Test
    @Transactional
    public void getAllDistrictMasters() throws Exception {
        // Initialize the database
        districtMasterRepository.saveAndFlush(districtMaster);

        // Get all the districtMasterList
        restDistrictMasterMockMvc.perform(get("/api/district-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(districtMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].districtName").value(hasItem(DEFAULT_DISTRICT_NAME)))
            .andExpect(jsonPath("$.[*].districtCode").value(hasItem(DEFAULT_DISTRICT_CODE)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
    
    @Test
    @Transactional
    public void getDistrictMaster() throws Exception {
        // Initialize the database
        districtMasterRepository.saveAndFlush(districtMaster);

        // Get the districtMaster
        restDistrictMasterMockMvc.perform(get("/api/district-masters/{id}", districtMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(districtMaster.getId().intValue()))
            .andExpect(jsonPath("$.districtName").value(DEFAULT_DISTRICT_NAME))
            .andExpect(jsonPath("$.districtCode").value(DEFAULT_DISTRICT_CODE))
            .andExpect(jsonPath("$.createdOn").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.lastUpdatedBy").value(DEFAULT_LAST_UPDATED_BY))
            .andExpect(jsonPath("$.lastUpdatedOn").value(DEFAULT_LAST_UPDATED_ON.toString()))
            .andExpect(jsonPath("$.verifiedBy").value(DEFAULT_VERIFIED_BY))
            .andExpect(jsonPath("$.verifiedOn").value(DEFAULT_VERIFIED_ON.toString()))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS));
    }

    @Test
    @Transactional
    public void getNonExistingDistrictMaster() throws Exception {
        // Get the districtMaster
        restDistrictMasterMockMvc.perform(get("/api/district-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDistrictMaster() throws Exception {
        // Initialize the database
        districtMasterRepository.saveAndFlush(districtMaster);

        int databaseSizeBeforeUpdate = districtMasterRepository.findAll().size();

        // Update the districtMaster
        DistrictMaster updatedDistrictMaster = districtMasterRepository.findById(districtMaster.getId()).get();
        // Disconnect from session so that the updates on updatedDistrictMaster are not directly saved in db
        em.detach(updatedDistrictMaster);
        updatedDistrictMaster
            .districtName(UPDATED_DISTRICT_NAME)
            .districtCode(UPDATED_DISTRICT_CODE)
            .createdOn(UPDATED_CREATED_ON)
            .lastUpdatedBy(UPDATED_LAST_UPDATED_BY)
            .lastUpdatedOn(UPDATED_LAST_UPDATED_ON)
            .verifiedBy(UPDATED_VERIFIED_BY)
            .verifiedOn(UPDATED_VERIFIED_ON)
            .remarks(UPDATED_REMARKS);
        DistrictMasterDTO districtMasterDTO = districtMasterMapper.toDto(updatedDistrictMaster);

        restDistrictMasterMockMvc.perform(put("/api/district-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(districtMasterDTO)))
            .andExpect(status().isOk());

        // Validate the DistrictMaster in the database
        List<DistrictMaster> districtMasterList = districtMasterRepository.findAll();
        assertThat(districtMasterList).hasSize(databaseSizeBeforeUpdate);
        DistrictMaster testDistrictMaster = districtMasterList.get(districtMasterList.size() - 1);
        assertThat(testDistrictMaster.getDistrictName()).isEqualTo(UPDATED_DISTRICT_NAME);
        assertThat(testDistrictMaster.getDistrictCode()).isEqualTo(UPDATED_DISTRICT_CODE);
        assertThat(testDistrictMaster.getCreatedOn()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testDistrictMaster.getLastUpdatedBy()).isEqualTo(UPDATED_LAST_UPDATED_BY);
        assertThat(testDistrictMaster.getLastUpdatedOn()).isEqualTo(UPDATED_LAST_UPDATED_ON);
        assertThat(testDistrictMaster.getVerifiedBy()).isEqualTo(UPDATED_VERIFIED_BY);
        assertThat(testDistrictMaster.getVerifiedOn()).isEqualTo(UPDATED_VERIFIED_ON);
        assertThat(testDistrictMaster.getRemarks()).isEqualTo(UPDATED_REMARKS);

        // Validate the DistrictMaster in Elasticsearch
        verify(mockDistrictMasterSearchRepository, times(1)).save(testDistrictMaster);
    }

    @Test
    @Transactional
    public void updateNonExistingDistrictMaster() throws Exception {
        int databaseSizeBeforeUpdate = districtMasterRepository.findAll().size();

        // Create the DistrictMaster
        DistrictMasterDTO districtMasterDTO = districtMasterMapper.toDto(districtMaster);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDistrictMasterMockMvc.perform(put("/api/district-masters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(districtMasterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DistrictMaster in the database
        List<DistrictMaster> districtMasterList = districtMasterRepository.findAll();
        assertThat(districtMasterList).hasSize(databaseSizeBeforeUpdate);

        // Validate the DistrictMaster in Elasticsearch
        verify(mockDistrictMasterSearchRepository, times(0)).save(districtMaster);
    }

    @Test
    @Transactional
    public void deleteDistrictMaster() throws Exception {
        // Initialize the database
        districtMasterRepository.saveAndFlush(districtMaster);

        int databaseSizeBeforeDelete = districtMasterRepository.findAll().size();

        // Delete the districtMaster
        restDistrictMasterMockMvc.perform(delete("/api/district-masters/{id}", districtMaster.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DistrictMaster> districtMasterList = districtMasterRepository.findAll();
        assertThat(districtMasterList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the DistrictMaster in Elasticsearch
        verify(mockDistrictMasterSearchRepository, times(1)).deleteById(districtMaster.getId());
    }

    @Test
    @Transactional
    public void searchDistrictMaster() throws Exception {
        // Initialize the database
        districtMasterRepository.saveAndFlush(districtMaster);
        when(mockDistrictMasterSearchRepository.search(queryStringQuery("id:" + districtMaster.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(districtMaster), PageRequest.of(0, 1), 1));
        // Search the districtMaster
        restDistrictMasterMockMvc.perform(get("/api/_search/district-masters?query=id:" + districtMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(districtMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].districtName").value(hasItem(DEFAULT_DISTRICT_NAME)))
            .andExpect(jsonPath("$.[*].districtCode").value(hasItem(DEFAULT_DISTRICT_CODE)))
            .andExpect(jsonPath("$.[*].createdOn").value(hasItem(DEFAULT_CREATED_ON.toString())))
            .andExpect(jsonPath("$.[*].lastUpdatedBy").value(hasItem(DEFAULT_LAST_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].lastUpdatedOn").value(hasItem(DEFAULT_LAST_UPDATED_ON.toString())))
            .andExpect(jsonPath("$.[*].verifiedBy").value(hasItem(DEFAULT_VERIFIED_BY)))
            .andExpect(jsonPath("$.[*].verifiedOn").value(hasItem(DEFAULT_VERIFIED_ON.toString())))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)));
    }
}
