package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.ExternalUserEsignRequest;
import com.hartron.esignharyana.repository.ExternalUserEsignRequestRepository;
import com.hartron.esignharyana.repository.search.ExternalUserEsignRequestSearchRepository;
import com.hartron.esignharyana.service.ExternalUserEsignRequestService;
import com.hartron.esignharyana.service.dto.ExternalUserEsignRequestDTO;
import com.hartron.esignharyana.service.mapper.ExternalUserEsignRequestMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ExternalUserEsignRequestResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class ExternalUserEsignRequestResourceIT {

    private static final String DEFAULT_USER_CODE_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_CODE_ID = "BBBBBBBBBB";

    private static final String DEFAULT_APPLICATION_ID_CODE = "AAAAAAAAAA";
    private static final String UPDATED_APPLICATION_ID_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_RESPONSE_URL = "AAAAAAAAAA";
    private static final String UPDATED_RESPONSE_URL = "BBBBBBBBBB";

    private static final String DEFAULT_REDIRECT_URL = "AAAAAAAAAA";
    private static final String UPDATED_REDIRECT_URL = "BBBBBBBBBB";

    private static final String DEFAULT_TS = "AAAAAAAAAA";
    private static final String UPDATED_TS = "BBBBBBBBBB";

    private static final String DEFAULT_SIGNER_ID = "AAAAAAAAAA";
    private static final String UPDATED_SIGNER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_DOC_INFO = "AAAAAAAAAA";
    private static final String UPDATED_DOC_INFO = "BBBBBBBBBB";

    private static final String DEFAULT_DOC_URL = "AAAAAAAAAA";
    private static final String UPDATED_DOC_URL = "BBBBBBBBBB";

    private static final String DEFAULT_DOC_HASH = "AAAAAAAAAA";
    private static final String UPDATED_DOC_HASH = "BBBBBBBBBB";

    private static final String DEFAULT_REQUEST_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_REQUEST_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_REQUEST_TIME = "AAAAAAAAAA";
    private static final String UPDATED_REQUEST_TIME = "BBBBBBBBBB";

    @Autowired
    private ExternalUserEsignRequestRepository externalUserEsignRequestRepository;

    @Autowired
    private ExternalUserEsignRequestMapper externalUserEsignRequestMapper;

    @Autowired
    private ExternalUserEsignRequestService externalUserEsignRequestService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.ExternalUserEsignRequestSearchRepositoryMockConfiguration
     */
    @Autowired
    private ExternalUserEsignRequestSearchRepository mockExternalUserEsignRequestSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restExternalUserEsignRequestMockMvc;

    private ExternalUserEsignRequest externalUserEsignRequest;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ExternalUserEsignRequestResource externalUserEsignRequestResource = new ExternalUserEsignRequestResource(externalUserEsignRequestService);
        this.restExternalUserEsignRequestMockMvc = MockMvcBuilders.standaloneSetup(externalUserEsignRequestResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExternalUserEsignRequest createEntity(EntityManager em) {
        ExternalUserEsignRequest externalUserEsignRequest = new ExternalUserEsignRequest()
            .userCodeId(DEFAULT_USER_CODE_ID)
            .applicationIdCode(DEFAULT_APPLICATION_ID_CODE)
            .responseUrl(DEFAULT_RESPONSE_URL)
            .redirectUrl(DEFAULT_REDIRECT_URL)
            .ts(DEFAULT_TS)
            .signerId(DEFAULT_SIGNER_ID)
            .docInfo(DEFAULT_DOC_INFO)
            .docUrl(DEFAULT_DOC_URL)
            .docHash(DEFAULT_DOC_HASH)
            .requestStatus(DEFAULT_REQUEST_STATUS)
            .requestTime(DEFAULT_REQUEST_TIME);
        return externalUserEsignRequest;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExternalUserEsignRequest createUpdatedEntity(EntityManager em) {
        ExternalUserEsignRequest externalUserEsignRequest = new ExternalUserEsignRequest()
            .userCodeId(UPDATED_USER_CODE_ID)
            .applicationIdCode(UPDATED_APPLICATION_ID_CODE)
            .responseUrl(UPDATED_RESPONSE_URL)
            .redirectUrl(UPDATED_REDIRECT_URL)
            .ts(UPDATED_TS)
            .signerId(UPDATED_SIGNER_ID)
            .docInfo(UPDATED_DOC_INFO)
            .docUrl(UPDATED_DOC_URL)
            .docHash(UPDATED_DOC_HASH)
            .requestStatus(UPDATED_REQUEST_STATUS)
            .requestTime(UPDATED_REQUEST_TIME);
        return externalUserEsignRequest;
    }

    @BeforeEach
    public void initTest() {
        externalUserEsignRequest = createEntity(em);
    }

    @Test
    @Transactional
    public void createExternalUserEsignRequest() throws Exception {
        int databaseSizeBeforeCreate = externalUserEsignRequestRepository.findAll().size();

        // Create the ExternalUserEsignRequest
        ExternalUserEsignRequestDTO externalUserEsignRequestDTO = externalUserEsignRequestMapper.toDto(externalUserEsignRequest);
        restExternalUserEsignRequestMockMvc.perform(post("/api/external-user-esign-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(externalUserEsignRequestDTO)))
            .andExpect(status().isCreated());

        // Validate the ExternalUserEsignRequest in the database
        List<ExternalUserEsignRequest> externalUserEsignRequestList = externalUserEsignRequestRepository.findAll();
        assertThat(externalUserEsignRequestList).hasSize(databaseSizeBeforeCreate + 1);
        ExternalUserEsignRequest testExternalUserEsignRequest = externalUserEsignRequestList.get(externalUserEsignRequestList.size() - 1);
        assertThat(testExternalUserEsignRequest.getUserCodeId()).isEqualTo(DEFAULT_USER_CODE_ID);
        assertThat(testExternalUserEsignRequest.getApplicationIdCode()).isEqualTo(DEFAULT_APPLICATION_ID_CODE);
        assertThat(testExternalUserEsignRequest.getResponseUrl()).isEqualTo(DEFAULT_RESPONSE_URL);
        assertThat(testExternalUserEsignRequest.getRedirectUrl()).isEqualTo(DEFAULT_REDIRECT_URL);
        assertThat(testExternalUserEsignRequest.getTs()).isEqualTo(DEFAULT_TS);
        assertThat(testExternalUserEsignRequest.getSignerId()).isEqualTo(DEFAULT_SIGNER_ID);
        assertThat(testExternalUserEsignRequest.getDocInfo()).isEqualTo(DEFAULT_DOC_INFO);
        assertThat(testExternalUserEsignRequest.getDocUrl()).isEqualTo(DEFAULT_DOC_URL);
        assertThat(testExternalUserEsignRequest.getDocHash()).isEqualTo(DEFAULT_DOC_HASH);
        assertThat(testExternalUserEsignRequest.getRequestStatus()).isEqualTo(DEFAULT_REQUEST_STATUS);
        assertThat(testExternalUserEsignRequest.getRequestTime()).isEqualTo(DEFAULT_REQUEST_TIME);

        // Validate the ExternalUserEsignRequest in Elasticsearch
        verify(mockExternalUserEsignRequestSearchRepository, times(1)).save(testExternalUserEsignRequest);
    }

    @Test
    @Transactional
    public void createExternalUserEsignRequestWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = externalUserEsignRequestRepository.findAll().size();

        // Create the ExternalUserEsignRequest with an existing ID
        externalUserEsignRequest.setId(1L);
        ExternalUserEsignRequestDTO externalUserEsignRequestDTO = externalUserEsignRequestMapper.toDto(externalUserEsignRequest);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExternalUserEsignRequestMockMvc.perform(post("/api/external-user-esign-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(externalUserEsignRequestDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ExternalUserEsignRequest in the database
        List<ExternalUserEsignRequest> externalUserEsignRequestList = externalUserEsignRequestRepository.findAll();
        assertThat(externalUserEsignRequestList).hasSize(databaseSizeBeforeCreate);

        // Validate the ExternalUserEsignRequest in Elasticsearch
        verify(mockExternalUserEsignRequestSearchRepository, times(0)).save(externalUserEsignRequest);
    }


    @Test
    @Transactional
    public void getAllExternalUserEsignRequests() throws Exception {
        // Initialize the database
        externalUserEsignRequestRepository.saveAndFlush(externalUserEsignRequest);

        // Get all the externalUserEsignRequestList
        restExternalUserEsignRequestMockMvc.perform(get("/api/external-user-esign-requests?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(externalUserEsignRequest.getId().intValue())))
            .andExpect(jsonPath("$.[*].userCodeId").value(hasItem(DEFAULT_USER_CODE_ID)))
            .andExpect(jsonPath("$.[*].applicationIdCode").value(hasItem(DEFAULT_APPLICATION_ID_CODE)))
            .andExpect(jsonPath("$.[*].responseUrl").value(hasItem(DEFAULT_RESPONSE_URL)))
            .andExpect(jsonPath("$.[*].redirectUrl").value(hasItem(DEFAULT_REDIRECT_URL)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS)))
            .andExpect(jsonPath("$.[*].signerId").value(hasItem(DEFAULT_SIGNER_ID)))
            .andExpect(jsonPath("$.[*].docInfo").value(hasItem(DEFAULT_DOC_INFO)))
            .andExpect(jsonPath("$.[*].docUrl").value(hasItem(DEFAULT_DOC_URL)))
            .andExpect(jsonPath("$.[*].docHash").value(hasItem(DEFAULT_DOC_HASH)))
            .andExpect(jsonPath("$.[*].requestStatus").value(hasItem(DEFAULT_REQUEST_STATUS)))
            .andExpect(jsonPath("$.[*].requestTime").value(hasItem(DEFAULT_REQUEST_TIME)));
    }
    
    @Test
    @Transactional
    public void getExternalUserEsignRequest() throws Exception {
        // Initialize the database
        externalUserEsignRequestRepository.saveAndFlush(externalUserEsignRequest);

        // Get the externalUserEsignRequest
        restExternalUserEsignRequestMockMvc.perform(get("/api/external-user-esign-requests/{id}", externalUserEsignRequest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(externalUserEsignRequest.getId().intValue()))
            .andExpect(jsonPath("$.userCodeId").value(DEFAULT_USER_CODE_ID))
            .andExpect(jsonPath("$.applicationIdCode").value(DEFAULT_APPLICATION_ID_CODE))
            .andExpect(jsonPath("$.responseUrl").value(DEFAULT_RESPONSE_URL))
            .andExpect(jsonPath("$.redirectUrl").value(DEFAULT_REDIRECT_URL))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS))
            .andExpect(jsonPath("$.signerId").value(DEFAULT_SIGNER_ID))
            .andExpect(jsonPath("$.docInfo").value(DEFAULT_DOC_INFO))
            .andExpect(jsonPath("$.docUrl").value(DEFAULT_DOC_URL))
            .andExpect(jsonPath("$.docHash").value(DEFAULT_DOC_HASH))
            .andExpect(jsonPath("$.requestStatus").value(DEFAULT_REQUEST_STATUS))
            .andExpect(jsonPath("$.requestTime").value(DEFAULT_REQUEST_TIME));
    }

    @Test
    @Transactional
    public void getNonExistingExternalUserEsignRequest() throws Exception {
        // Get the externalUserEsignRequest
        restExternalUserEsignRequestMockMvc.perform(get("/api/external-user-esign-requests/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExternalUserEsignRequest() throws Exception {
        // Initialize the database
        externalUserEsignRequestRepository.saveAndFlush(externalUserEsignRequest);

        int databaseSizeBeforeUpdate = externalUserEsignRequestRepository.findAll().size();

        // Update the externalUserEsignRequest
        ExternalUserEsignRequest updatedExternalUserEsignRequest = externalUserEsignRequestRepository.findById(externalUserEsignRequest.getId()).get();
        // Disconnect from session so that the updates on updatedExternalUserEsignRequest are not directly saved in db
        em.detach(updatedExternalUserEsignRequest);
        updatedExternalUserEsignRequest
            .userCodeId(UPDATED_USER_CODE_ID)
            .applicationIdCode(UPDATED_APPLICATION_ID_CODE)
            .responseUrl(UPDATED_RESPONSE_URL)
            .redirectUrl(UPDATED_REDIRECT_URL)
            .ts(UPDATED_TS)
            .signerId(UPDATED_SIGNER_ID)
            .docInfo(UPDATED_DOC_INFO)
            .docUrl(UPDATED_DOC_URL)
            .docHash(UPDATED_DOC_HASH)
            .requestStatus(UPDATED_REQUEST_STATUS)
            .requestTime(UPDATED_REQUEST_TIME);
        ExternalUserEsignRequestDTO externalUserEsignRequestDTO = externalUserEsignRequestMapper.toDto(updatedExternalUserEsignRequest);

        restExternalUserEsignRequestMockMvc.perform(put("/api/external-user-esign-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(externalUserEsignRequestDTO)))
            .andExpect(status().isOk());

        // Validate the ExternalUserEsignRequest in the database
        List<ExternalUserEsignRequest> externalUserEsignRequestList = externalUserEsignRequestRepository.findAll();
        assertThat(externalUserEsignRequestList).hasSize(databaseSizeBeforeUpdate);
        ExternalUserEsignRequest testExternalUserEsignRequest = externalUserEsignRequestList.get(externalUserEsignRequestList.size() - 1);
        assertThat(testExternalUserEsignRequest.getUserCodeId()).isEqualTo(UPDATED_USER_CODE_ID);
        assertThat(testExternalUserEsignRequest.getApplicationIdCode()).isEqualTo(UPDATED_APPLICATION_ID_CODE);
        assertThat(testExternalUserEsignRequest.getResponseUrl()).isEqualTo(UPDATED_RESPONSE_URL);
        assertThat(testExternalUserEsignRequest.getRedirectUrl()).isEqualTo(UPDATED_REDIRECT_URL);
        assertThat(testExternalUserEsignRequest.getTs()).isEqualTo(UPDATED_TS);
        assertThat(testExternalUserEsignRequest.getSignerId()).isEqualTo(UPDATED_SIGNER_ID);
        assertThat(testExternalUserEsignRequest.getDocInfo()).isEqualTo(UPDATED_DOC_INFO);
        assertThat(testExternalUserEsignRequest.getDocUrl()).isEqualTo(UPDATED_DOC_URL);
        assertThat(testExternalUserEsignRequest.getDocHash()).isEqualTo(UPDATED_DOC_HASH);
        assertThat(testExternalUserEsignRequest.getRequestStatus()).isEqualTo(UPDATED_REQUEST_STATUS);
        assertThat(testExternalUserEsignRequest.getRequestTime()).isEqualTo(UPDATED_REQUEST_TIME);

        // Validate the ExternalUserEsignRequest in Elasticsearch
        verify(mockExternalUserEsignRequestSearchRepository, times(1)).save(testExternalUserEsignRequest);
    }

    @Test
    @Transactional
    public void updateNonExistingExternalUserEsignRequest() throws Exception {
        int databaseSizeBeforeUpdate = externalUserEsignRequestRepository.findAll().size();

        // Create the ExternalUserEsignRequest
        ExternalUserEsignRequestDTO externalUserEsignRequestDTO = externalUserEsignRequestMapper.toDto(externalUserEsignRequest);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExternalUserEsignRequestMockMvc.perform(put("/api/external-user-esign-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(externalUserEsignRequestDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ExternalUserEsignRequest in the database
        List<ExternalUserEsignRequest> externalUserEsignRequestList = externalUserEsignRequestRepository.findAll();
        assertThat(externalUserEsignRequestList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ExternalUserEsignRequest in Elasticsearch
        verify(mockExternalUserEsignRequestSearchRepository, times(0)).save(externalUserEsignRequest);
    }

    @Test
    @Transactional
    public void deleteExternalUserEsignRequest() throws Exception {
        // Initialize the database
        externalUserEsignRequestRepository.saveAndFlush(externalUserEsignRequest);

        int databaseSizeBeforeDelete = externalUserEsignRequestRepository.findAll().size();

        // Delete the externalUserEsignRequest
        restExternalUserEsignRequestMockMvc.perform(delete("/api/external-user-esign-requests/{id}", externalUserEsignRequest.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExternalUserEsignRequest> externalUserEsignRequestList = externalUserEsignRequestRepository.findAll();
        assertThat(externalUserEsignRequestList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ExternalUserEsignRequest in Elasticsearch
        verify(mockExternalUserEsignRequestSearchRepository, times(1)).deleteById(externalUserEsignRequest.getId());
    }

    @Test
    @Transactional
    public void searchExternalUserEsignRequest() throws Exception {
        // Initialize the database
        externalUserEsignRequestRepository.saveAndFlush(externalUserEsignRequest);
        when(mockExternalUserEsignRequestSearchRepository.search(queryStringQuery("id:" + externalUserEsignRequest.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(externalUserEsignRequest), PageRequest.of(0, 1), 1));
        // Search the externalUserEsignRequest
        restExternalUserEsignRequestMockMvc.perform(get("/api/_search/external-user-esign-requests?query=id:" + externalUserEsignRequest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(externalUserEsignRequest.getId().intValue())))
            .andExpect(jsonPath("$.[*].userCodeId").value(hasItem(DEFAULT_USER_CODE_ID)))
            .andExpect(jsonPath("$.[*].applicationIdCode").value(hasItem(DEFAULT_APPLICATION_ID_CODE)))
            .andExpect(jsonPath("$.[*].responseUrl").value(hasItem(DEFAULT_RESPONSE_URL)))
            .andExpect(jsonPath("$.[*].redirectUrl").value(hasItem(DEFAULT_REDIRECT_URL)))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS)))
            .andExpect(jsonPath("$.[*].signerId").value(hasItem(DEFAULT_SIGNER_ID)))
            .andExpect(jsonPath("$.[*].docInfo").value(hasItem(DEFAULT_DOC_INFO)))
            .andExpect(jsonPath("$.[*].docUrl").value(hasItem(DEFAULT_DOC_URL)))
            .andExpect(jsonPath("$.[*].docHash").value(hasItem(DEFAULT_DOC_HASH)))
            .andExpect(jsonPath("$.[*].requestStatus").value(hasItem(DEFAULT_REQUEST_STATUS)))
            .andExpect(jsonPath("$.[*].requestTime").value(hasItem(DEFAULT_REQUEST_TIME)));
    }
}
