package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.Otp;
import com.hartron.esignharyana.repository.OtpRepository;
import com.hartron.esignharyana.repository.search.OtpSearchRepository;
import com.hartron.esignharyana.service.OtpService;
import com.hartron.esignharyana.service.dto.OtpDTO;
import com.hartron.esignharyana.service.mapper.OtpMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.sameInstant;
import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OtpResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class OtpResourceIT {

    private static final Integer DEFAULT_OTP = 1;
    private static final Integer UPDATED_OTP = 2;

    private static final Long DEFAULT_MOBILE_NUMBER = 1L;
    private static final Long UPDATED_MOBILE_NUMBER = 2L;

    private static final ZonedDateTime DEFAULT_REQUEST_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_REQUEST_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private OtpRepository otpRepository;

    @Autowired
    private OtpMapper otpMapper;

    @Autowired
    private OtpService otpService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.OtpSearchRepositoryMockConfiguration
     */
    @Autowired
    private OtpSearchRepository mockOtpSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOtpMockMvc;

    private Otp otp;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OtpResource otpResource = new OtpResource(otpService);
        this.restOtpMockMvc = MockMvcBuilders.standaloneSetup(otpResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Otp createEntity(EntityManager em) {
        Otp otp = new Otp()
            .otp(DEFAULT_OTP)
            .mobileNumber(DEFAULT_MOBILE_NUMBER)
            .requestTime(DEFAULT_REQUEST_TIME);
        return otp;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Otp createUpdatedEntity(EntityManager em) {
        Otp otp = new Otp()
            .otp(UPDATED_OTP)
            .mobileNumber(UPDATED_MOBILE_NUMBER)
            .requestTime(UPDATED_REQUEST_TIME);
        return otp;
    }

    @BeforeEach
    public void initTest() {
        otp = createEntity(em);
    }

    @Test
    @Transactional
    public void createOtp() throws Exception {
        int databaseSizeBeforeCreate = otpRepository.findAll().size();

        // Create the Otp
        OtpDTO otpDTO = otpMapper.toDto(otp);
        restOtpMockMvc.perform(post("/api/otps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(otpDTO)))
            .andExpect(status().isCreated());

        // Validate the Otp in the database
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeCreate + 1);
        Otp testOtp = otpList.get(otpList.size() - 1);
        assertThat(testOtp.getOtp()).isEqualTo(DEFAULT_OTP);
        assertThat(testOtp.getMobileNumber()).isEqualTo(DEFAULT_MOBILE_NUMBER);
        assertThat(testOtp.getRequestTime()).isEqualTo(DEFAULT_REQUEST_TIME);

        // Validate the Otp in Elasticsearch
        verify(mockOtpSearchRepository, times(1)).save(testOtp);
    }

    @Test
    @Transactional
    public void createOtpWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = otpRepository.findAll().size();

        // Create the Otp with an existing ID
        otp.setId(1L);
        OtpDTO otpDTO = otpMapper.toDto(otp);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOtpMockMvc.perform(post("/api/otps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(otpDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Otp in the database
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeCreate);

        // Validate the Otp in Elasticsearch
        verify(mockOtpSearchRepository, times(0)).save(otp);
    }


    @Test
    @Transactional
    public void getAllOtps() throws Exception {
        // Initialize the database
        otpRepository.saveAndFlush(otp);

        // Get all the otpList
        restOtpMockMvc.perform(get("/api/otps?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(otp.getId().intValue())))
            .andExpect(jsonPath("$.[*].otp").value(hasItem(DEFAULT_OTP)))
            .andExpect(jsonPath("$.[*].mobileNumber").value(hasItem(DEFAULT_MOBILE_NUMBER.intValue())))
            .andExpect(jsonPath("$.[*].requestTime").value(hasItem(sameInstant(DEFAULT_REQUEST_TIME))));
    }
    
    @Test
    @Transactional
    public void getOtp() throws Exception {
        // Initialize the database
        otpRepository.saveAndFlush(otp);

        // Get the otp
        restOtpMockMvc.perform(get("/api/otps/{id}", otp.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(otp.getId().intValue()))
            .andExpect(jsonPath("$.otp").value(DEFAULT_OTP))
            .andExpect(jsonPath("$.mobileNumber").value(DEFAULT_MOBILE_NUMBER.intValue()))
            .andExpect(jsonPath("$.requestTime").value(sameInstant(DEFAULT_REQUEST_TIME)));
    }

    @Test
    @Transactional
    public void getNonExistingOtp() throws Exception {
        // Get the otp
        restOtpMockMvc.perform(get("/api/otps/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOtp() throws Exception {
        // Initialize the database
        otpRepository.saveAndFlush(otp);

        int databaseSizeBeforeUpdate = otpRepository.findAll().size();

        // Update the otp
        Otp updatedOtp = otpRepository.findById(otp.getId()).get();
        // Disconnect from session so that the updates on updatedOtp are not directly saved in db
        em.detach(updatedOtp);
        updatedOtp
            .otp(UPDATED_OTP)
            .mobileNumber(UPDATED_MOBILE_NUMBER)
            .requestTime(UPDATED_REQUEST_TIME);
        OtpDTO otpDTO = otpMapper.toDto(updatedOtp);

        restOtpMockMvc.perform(put("/api/otps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(otpDTO)))
            .andExpect(status().isOk());

        // Validate the Otp in the database
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeUpdate);
        Otp testOtp = otpList.get(otpList.size() - 1);
        assertThat(testOtp.getOtp()).isEqualTo(UPDATED_OTP);
        assertThat(testOtp.getMobileNumber()).isEqualTo(UPDATED_MOBILE_NUMBER);
        assertThat(testOtp.getRequestTime()).isEqualTo(UPDATED_REQUEST_TIME);

        // Validate the Otp in Elasticsearch
        verify(mockOtpSearchRepository, times(1)).save(testOtp);
    }

    @Test
    @Transactional
    public void updateNonExistingOtp() throws Exception {
        int databaseSizeBeforeUpdate = otpRepository.findAll().size();

        // Create the Otp
        OtpDTO otpDTO = otpMapper.toDto(otp);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOtpMockMvc.perform(put("/api/otps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(otpDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Otp in the database
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Otp in Elasticsearch
        verify(mockOtpSearchRepository, times(0)).save(otp);
    }

    @Test
    @Transactional
    public void deleteOtp() throws Exception {
        // Initialize the database
        otpRepository.saveAndFlush(otp);

        int databaseSizeBeforeDelete = otpRepository.findAll().size();

        // Delete the otp
        restOtpMockMvc.perform(delete("/api/otps/{id}", otp.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Otp> otpList = otpRepository.findAll();
        assertThat(otpList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Otp in Elasticsearch
        verify(mockOtpSearchRepository, times(1)).deleteById(otp.getId());
    }

    @Test
    @Transactional
    public void searchOtp() throws Exception {
        // Initialize the database
        otpRepository.saveAndFlush(otp);
        when(mockOtpSearchRepository.search(queryStringQuery("id:" + otp.getId())))
            .thenReturn(Collections.singletonList(otp));
        // Search the otp
        restOtpMockMvc.perform(get("/api/_search/otps?query=id:" + otp.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(otp.getId().intValue())))
            .andExpect(jsonPath("$.[*].otp").value(hasItem(DEFAULT_OTP)))
            .andExpect(jsonPath("$.[*].mobileNumber").value(hasItem(DEFAULT_MOBILE_NUMBER.intValue())))
            .andExpect(jsonPath("$.[*].requestTime").value(hasItem(sameInstant(DEFAULT_REQUEST_TIME))));
    }
}
