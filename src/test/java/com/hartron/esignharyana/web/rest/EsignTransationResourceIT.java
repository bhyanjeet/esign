package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.EsignharyanaApp;
import com.hartron.esignharyana.domain.EsignTransation;
import com.hartron.esignharyana.repository.EsignTransationRepository;
import com.hartron.esignharyana.repository.search.EsignTransationSearchRepository;
import com.hartron.esignharyana.service.EsignTransationService;
import com.hartron.esignharyana.service.dto.EsignTransationDTO;
import com.hartron.esignharyana.service.mapper.EsignTransationMapper;
import com.hartron.esignharyana.web.rest.errors.ExceptionTranslator;
import com.hartron.esignharyana.service.dto.EsignTransationCriteria;
import com.hartron.esignharyana.service.EsignTransationQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import static com.hartron.esignharyana.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EsignTransationResource} REST controller.
 */
@SpringBootTest(classes = EsignharyanaApp.class)
public class EsignTransationResourceIT {

    private static final String DEFAULT_TXN_ID = "AAAAAAAAAA";
    private static final String UPDATED_TXN_ID = "BBBBBBBBBB";

    private static final String DEFAULT_USER_CODE_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_CODE_ID = "BBBBBBBBBB";

    private static final String DEFAULT_APPLICATION_CODE_ID = "AAAAAAAAAA";
    private static final String UPDATED_APPLICATION_CODE_ID = "BBBBBBBBBB";

    private static final String DEFAULT_ORG_RESPONSE_SUCCESS_URL = "AAAAAAAAAA";
    private static final String UPDATED_ORG_RESPONSE_SUCCESS_URL = "BBBBBBBBBB";

    private static final String DEFAULT_ORG_RESPONSE_FAIL_URL = "AAAAAAAAAA";
    private static final String UPDATED_ORG_RESPONSE_FAIL_URL = "BBBBBBBBBB";

    private static final String DEFAULT_ORG_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_ORG_STATUS = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_REQUEST_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_REQUEST_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_REQUEST_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_ORGANISATION_ID_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISATION_ID_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_SIGNER_ID = "AAAAAAAAAA";
    private static final String UPDATED_SIGNER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_REQUEST_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_REQUEST_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_REQUEST_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_REQUEST_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_RESPONSE_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_RESPONSE_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_RESPONSE_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_RESPONSE_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_RESPONSE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_RESPONSE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_REQUEST_XML = "AAAAAAAAAA";
    private static final String UPDATED_REQUEST_XML = "BBBBBBBBBB";

    private static final String DEFAULT_RESPONSE_XML = "AAAAAAAAAA";
    private static final String UPDATED_RESPONSE_XML = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_RESPONSE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_RESPONSE_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_RESPONSE_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_TS = "AAAAAAAAAA";
    private static final String UPDATED_TS = "BBBBBBBBBB";

    @Autowired
    private EsignTransationRepository esignTransationRepository;

    @Autowired
    private EsignTransationMapper esignTransationMapper;

    @Autowired
    private EsignTransationService esignTransationService;

    /**
     * This repository is mocked in the com.hartron.esignharyana.repository.search test package.
     *
     * @see com.hartron.esignharyana.repository.search.EsignTransationSearchRepositoryMockConfiguration
     */
    @Autowired
    private EsignTransationSearchRepository mockEsignTransationSearchRepository;

    @Autowired
    private EsignTransationQueryService esignTransationQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEsignTransationMockMvc;

    private EsignTransation esignTransation;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EsignTransationResource esignTransationResource = new EsignTransationResource(esignTransationService, esignTransationQueryService);
        this.restEsignTransationMockMvc = MockMvcBuilders.standaloneSetup(esignTransationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EsignTransation createEntity(EntityManager em) {
        EsignTransation esignTransation = new EsignTransation()
            .txnId(DEFAULT_TXN_ID)
            .userCodeId(DEFAULT_USER_CODE_ID)
            .applicationCodeId(DEFAULT_APPLICATION_CODE_ID)
            .orgResponseSuccessURL(DEFAULT_ORG_RESPONSE_SUCCESS_URL)
            .orgResponseFailURL(DEFAULT_ORG_RESPONSE_FAIL_URL)
            .orgStatus(DEFAULT_ORG_STATUS)
            .requestDate(DEFAULT_REQUEST_DATE)
            .organisationIdCode(DEFAULT_ORGANISATION_ID_CODE)
            .signerId(DEFAULT_SIGNER_ID)
            .requestType(DEFAULT_REQUEST_TYPE)
            .requestStatus(DEFAULT_REQUEST_STATUS)
            .responseType(DEFAULT_RESPONSE_TYPE)
            .responseStatus(DEFAULT_RESPONSE_STATUS)
            .responseCode(DEFAULT_RESPONSE_CODE)
            .requestXml(DEFAULT_REQUEST_XML)
            .responseXml(DEFAULT_RESPONSE_XML)
            .responseDate(DEFAULT_RESPONSE_DATE)
            .ts(DEFAULT_TS);
        return esignTransation;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EsignTransation createUpdatedEntity(EntityManager em) {
        EsignTransation esignTransation = new EsignTransation()
            .txnId(UPDATED_TXN_ID)
            .userCodeId(UPDATED_USER_CODE_ID)
            .applicationCodeId(UPDATED_APPLICATION_CODE_ID)
            .orgResponseSuccessURL(UPDATED_ORG_RESPONSE_SUCCESS_URL)
            .orgResponseFailURL(UPDATED_ORG_RESPONSE_FAIL_URL)
            .orgStatus(UPDATED_ORG_STATUS)
            .requestDate(UPDATED_REQUEST_DATE)
            .organisationIdCode(UPDATED_ORGANISATION_ID_CODE)
            .signerId(UPDATED_SIGNER_ID)
            .requestType(UPDATED_REQUEST_TYPE)
            .requestStatus(UPDATED_REQUEST_STATUS)
            .responseType(UPDATED_RESPONSE_TYPE)
            .responseStatus(UPDATED_RESPONSE_STATUS)
            .responseCode(UPDATED_RESPONSE_CODE)
            .requestXml(UPDATED_REQUEST_XML)
            .responseXml(UPDATED_RESPONSE_XML)
            .responseDate(UPDATED_RESPONSE_DATE)
            .ts(UPDATED_TS);
        return esignTransation;
    }

    @BeforeEach
    public void initTest() {
        esignTransation = createEntity(em);
    }

    @Test
    @Transactional
    public void createEsignTransation() throws Exception {
        int databaseSizeBeforeCreate = esignTransationRepository.findAll().size();

        // Create the EsignTransation
        EsignTransationDTO esignTransationDTO = esignTransationMapper.toDto(esignTransation);
        restEsignTransationMockMvc.perform(post("/api/esign-transations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(esignTransationDTO)))
            .andExpect(status().isCreated());

        // Validate the EsignTransation in the database
        List<EsignTransation> esignTransationList = esignTransationRepository.findAll();
        assertThat(esignTransationList).hasSize(databaseSizeBeforeCreate + 1);
        EsignTransation testEsignTransation = esignTransationList.get(esignTransationList.size() - 1);
        assertThat(testEsignTransation.getTxnId()).isEqualTo(DEFAULT_TXN_ID);
        assertThat(testEsignTransation.getUserCodeId()).isEqualTo(DEFAULT_USER_CODE_ID);
        assertThat(testEsignTransation.getApplicationCodeId()).isEqualTo(DEFAULT_APPLICATION_CODE_ID);
        assertThat(testEsignTransation.getOrgResponseSuccessURL()).isEqualTo(DEFAULT_ORG_RESPONSE_SUCCESS_URL);
        assertThat(testEsignTransation.getOrgResponseFailURL()).isEqualTo(DEFAULT_ORG_RESPONSE_FAIL_URL);
        assertThat(testEsignTransation.getOrgStatus()).isEqualTo(DEFAULT_ORG_STATUS);
        assertThat(testEsignTransation.getRequestDate()).isEqualTo(DEFAULT_REQUEST_DATE);
        assertThat(testEsignTransation.getOrganisationIdCode()).isEqualTo(DEFAULT_ORGANISATION_ID_CODE);
        assertThat(testEsignTransation.getSignerId()).isEqualTo(DEFAULT_SIGNER_ID);
        assertThat(testEsignTransation.getRequestType()).isEqualTo(DEFAULT_REQUEST_TYPE);
        assertThat(testEsignTransation.getRequestStatus()).isEqualTo(DEFAULT_REQUEST_STATUS);
        assertThat(testEsignTransation.getResponseType()).isEqualTo(DEFAULT_RESPONSE_TYPE);
        assertThat(testEsignTransation.getResponseStatus()).isEqualTo(DEFAULT_RESPONSE_STATUS);
        assertThat(testEsignTransation.getResponseCode()).isEqualTo(DEFAULT_RESPONSE_CODE);
        assertThat(testEsignTransation.getRequestXml()).isEqualTo(DEFAULT_REQUEST_XML);
        assertThat(testEsignTransation.getResponseXml()).isEqualTo(DEFAULT_RESPONSE_XML);
        assertThat(testEsignTransation.getResponseDate()).isEqualTo(DEFAULT_RESPONSE_DATE);
        assertThat(testEsignTransation.getTs()).isEqualTo(DEFAULT_TS);

        // Validate the EsignTransation in Elasticsearch
        verify(mockEsignTransationSearchRepository, times(1)).save(testEsignTransation);
    }

    @Test
    @Transactional
    public void createEsignTransationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = esignTransationRepository.findAll().size();

        // Create the EsignTransation with an existing ID
        esignTransation.setId(1L);
        EsignTransationDTO esignTransationDTO = esignTransationMapper.toDto(esignTransation);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEsignTransationMockMvc.perform(post("/api/esign-transations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(esignTransationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EsignTransation in the database
        List<EsignTransation> esignTransationList = esignTransationRepository.findAll();
        assertThat(esignTransationList).hasSize(databaseSizeBeforeCreate);

        // Validate the EsignTransation in Elasticsearch
        verify(mockEsignTransationSearchRepository, times(0)).save(esignTransation);
    }


    @Test
    @Transactional
    public void getAllEsignTransations() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList
        restEsignTransationMockMvc.perform(get("/api/esign-transations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(esignTransation.getId().intValue())))
            .andExpect(jsonPath("$.[*].txnId").value(hasItem(DEFAULT_TXN_ID)))
            .andExpect(jsonPath("$.[*].userCodeId").value(hasItem(DEFAULT_USER_CODE_ID)))
            .andExpect(jsonPath("$.[*].applicationCodeId").value(hasItem(DEFAULT_APPLICATION_CODE_ID)))
            .andExpect(jsonPath("$.[*].orgResponseSuccessURL").value(hasItem(DEFAULT_ORG_RESPONSE_SUCCESS_URL)))
            .andExpect(jsonPath("$.[*].orgResponseFailURL").value(hasItem(DEFAULT_ORG_RESPONSE_FAIL_URL)))
            .andExpect(jsonPath("$.[*].orgStatus").value(hasItem(DEFAULT_ORG_STATUS)))
            .andExpect(jsonPath("$.[*].requestDate").value(hasItem(DEFAULT_REQUEST_DATE.toString())))
            .andExpect(jsonPath("$.[*].organisationIdCode").value(hasItem(DEFAULT_ORGANISATION_ID_CODE)))
            .andExpect(jsonPath("$.[*].signerId").value(hasItem(DEFAULT_SIGNER_ID)))
            .andExpect(jsonPath("$.[*].requestType").value(hasItem(DEFAULT_REQUEST_TYPE)))
            .andExpect(jsonPath("$.[*].requestStatus").value(hasItem(DEFAULT_REQUEST_STATUS)))
            .andExpect(jsonPath("$.[*].responseType").value(hasItem(DEFAULT_RESPONSE_TYPE)))
            .andExpect(jsonPath("$.[*].responseStatus").value(hasItem(DEFAULT_RESPONSE_STATUS)))
            .andExpect(jsonPath("$.[*].responseCode").value(hasItem(DEFAULT_RESPONSE_CODE)))
            .andExpect(jsonPath("$.[*].requestXml").value(hasItem(DEFAULT_REQUEST_XML)))
            .andExpect(jsonPath("$.[*].responseXml").value(hasItem(DEFAULT_RESPONSE_XML)))
            .andExpect(jsonPath("$.[*].responseDate").value(hasItem(DEFAULT_RESPONSE_DATE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS)));
    }
    
    @Test
    @Transactional
    public void getEsignTransation() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get the esignTransation
        restEsignTransationMockMvc.perform(get("/api/esign-transations/{id}", esignTransation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(esignTransation.getId().intValue()))
            .andExpect(jsonPath("$.txnId").value(DEFAULT_TXN_ID))
            .andExpect(jsonPath("$.userCodeId").value(DEFAULT_USER_CODE_ID))
            .andExpect(jsonPath("$.applicationCodeId").value(DEFAULT_APPLICATION_CODE_ID))
            .andExpect(jsonPath("$.orgResponseSuccessURL").value(DEFAULT_ORG_RESPONSE_SUCCESS_URL))
            .andExpect(jsonPath("$.orgResponseFailURL").value(DEFAULT_ORG_RESPONSE_FAIL_URL))
            .andExpect(jsonPath("$.orgStatus").value(DEFAULT_ORG_STATUS))
            .andExpect(jsonPath("$.requestDate").value(DEFAULT_REQUEST_DATE.toString()))
            .andExpect(jsonPath("$.organisationIdCode").value(DEFAULT_ORGANISATION_ID_CODE))
            .andExpect(jsonPath("$.signerId").value(DEFAULT_SIGNER_ID))
            .andExpect(jsonPath("$.requestType").value(DEFAULT_REQUEST_TYPE))
            .andExpect(jsonPath("$.requestStatus").value(DEFAULT_REQUEST_STATUS))
            .andExpect(jsonPath("$.responseType").value(DEFAULT_RESPONSE_TYPE))
            .andExpect(jsonPath("$.responseStatus").value(DEFAULT_RESPONSE_STATUS))
            .andExpect(jsonPath("$.responseCode").value(DEFAULT_RESPONSE_CODE))
            .andExpect(jsonPath("$.requestXml").value(DEFAULT_REQUEST_XML))
            .andExpect(jsonPath("$.responseXml").value(DEFAULT_RESPONSE_XML))
            .andExpect(jsonPath("$.responseDate").value(DEFAULT_RESPONSE_DATE.toString()))
            .andExpect(jsonPath("$.ts").value(DEFAULT_TS));
    }


    @Test
    @Transactional
    public void getEsignTransationsByIdFiltering() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        Long id = esignTransation.getId();

        defaultEsignTransationShouldBeFound("id.equals=" + id);
        defaultEsignTransationShouldNotBeFound("id.notEquals=" + id);

        defaultEsignTransationShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultEsignTransationShouldNotBeFound("id.greaterThan=" + id);

        defaultEsignTransationShouldBeFound("id.lessThanOrEqual=" + id);
        defaultEsignTransationShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByTxnIdIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where txnId equals to DEFAULT_TXN_ID
        defaultEsignTransationShouldBeFound("txnId.equals=" + DEFAULT_TXN_ID);

        // Get all the esignTransationList where txnId equals to UPDATED_TXN_ID
        defaultEsignTransationShouldNotBeFound("txnId.equals=" + UPDATED_TXN_ID);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByTxnIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where txnId not equals to DEFAULT_TXN_ID
        defaultEsignTransationShouldNotBeFound("txnId.notEquals=" + DEFAULT_TXN_ID);

        // Get all the esignTransationList where txnId not equals to UPDATED_TXN_ID
        defaultEsignTransationShouldBeFound("txnId.notEquals=" + UPDATED_TXN_ID);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByTxnIdIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where txnId in DEFAULT_TXN_ID or UPDATED_TXN_ID
        defaultEsignTransationShouldBeFound("txnId.in=" + DEFAULT_TXN_ID + "," + UPDATED_TXN_ID);

        // Get all the esignTransationList where txnId equals to UPDATED_TXN_ID
        defaultEsignTransationShouldNotBeFound("txnId.in=" + UPDATED_TXN_ID);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByTxnIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where txnId is not null
        defaultEsignTransationShouldBeFound("txnId.specified=true");

        // Get all the esignTransationList where txnId is null
        defaultEsignTransationShouldNotBeFound("txnId.specified=false");
    }
                @Test
    @Transactional
    public void getAllEsignTransationsByTxnIdContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where txnId contains DEFAULT_TXN_ID
        defaultEsignTransationShouldBeFound("txnId.contains=" + DEFAULT_TXN_ID);

        // Get all the esignTransationList where txnId contains UPDATED_TXN_ID
        defaultEsignTransationShouldNotBeFound("txnId.contains=" + UPDATED_TXN_ID);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByTxnIdNotContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where txnId does not contain DEFAULT_TXN_ID
        defaultEsignTransationShouldNotBeFound("txnId.doesNotContain=" + DEFAULT_TXN_ID);

        // Get all the esignTransationList where txnId does not contain UPDATED_TXN_ID
        defaultEsignTransationShouldBeFound("txnId.doesNotContain=" + UPDATED_TXN_ID);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByUserCodeIdIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where userCodeId equals to DEFAULT_USER_CODE_ID
        defaultEsignTransationShouldBeFound("userCodeId.equals=" + DEFAULT_USER_CODE_ID);

        // Get all the esignTransationList where userCodeId equals to UPDATED_USER_CODE_ID
        defaultEsignTransationShouldNotBeFound("userCodeId.equals=" + UPDATED_USER_CODE_ID);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByUserCodeIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where userCodeId not equals to DEFAULT_USER_CODE_ID
        defaultEsignTransationShouldNotBeFound("userCodeId.notEquals=" + DEFAULT_USER_CODE_ID);

        // Get all the esignTransationList where userCodeId not equals to UPDATED_USER_CODE_ID
        defaultEsignTransationShouldBeFound("userCodeId.notEquals=" + UPDATED_USER_CODE_ID);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByUserCodeIdIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where userCodeId in DEFAULT_USER_CODE_ID or UPDATED_USER_CODE_ID
        defaultEsignTransationShouldBeFound("userCodeId.in=" + DEFAULT_USER_CODE_ID + "," + UPDATED_USER_CODE_ID);

        // Get all the esignTransationList where userCodeId equals to UPDATED_USER_CODE_ID
        defaultEsignTransationShouldNotBeFound("userCodeId.in=" + UPDATED_USER_CODE_ID);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByUserCodeIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where userCodeId is not null
        defaultEsignTransationShouldBeFound("userCodeId.specified=true");

        // Get all the esignTransationList where userCodeId is null
        defaultEsignTransationShouldNotBeFound("userCodeId.specified=false");
    }
                @Test
    @Transactional
    public void getAllEsignTransationsByUserCodeIdContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where userCodeId contains DEFAULT_USER_CODE_ID
        defaultEsignTransationShouldBeFound("userCodeId.contains=" + DEFAULT_USER_CODE_ID);

        // Get all the esignTransationList where userCodeId contains UPDATED_USER_CODE_ID
        defaultEsignTransationShouldNotBeFound("userCodeId.contains=" + UPDATED_USER_CODE_ID);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByUserCodeIdNotContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where userCodeId does not contain DEFAULT_USER_CODE_ID
        defaultEsignTransationShouldNotBeFound("userCodeId.doesNotContain=" + DEFAULT_USER_CODE_ID);

        // Get all the esignTransationList where userCodeId does not contain UPDATED_USER_CODE_ID
        defaultEsignTransationShouldBeFound("userCodeId.doesNotContain=" + UPDATED_USER_CODE_ID);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByApplicationCodeIdIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where applicationCodeId equals to DEFAULT_APPLICATION_CODE_ID
        defaultEsignTransationShouldBeFound("applicationCodeId.equals=" + DEFAULT_APPLICATION_CODE_ID);

        // Get all the esignTransationList where applicationCodeId equals to UPDATED_APPLICATION_CODE_ID
        defaultEsignTransationShouldNotBeFound("applicationCodeId.equals=" + UPDATED_APPLICATION_CODE_ID);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByApplicationCodeIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where applicationCodeId not equals to DEFAULT_APPLICATION_CODE_ID
        defaultEsignTransationShouldNotBeFound("applicationCodeId.notEquals=" + DEFAULT_APPLICATION_CODE_ID);

        // Get all the esignTransationList where applicationCodeId not equals to UPDATED_APPLICATION_CODE_ID
        defaultEsignTransationShouldBeFound("applicationCodeId.notEquals=" + UPDATED_APPLICATION_CODE_ID);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByApplicationCodeIdIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where applicationCodeId in DEFAULT_APPLICATION_CODE_ID or UPDATED_APPLICATION_CODE_ID
        defaultEsignTransationShouldBeFound("applicationCodeId.in=" + DEFAULT_APPLICATION_CODE_ID + "," + UPDATED_APPLICATION_CODE_ID);

        // Get all the esignTransationList where applicationCodeId equals to UPDATED_APPLICATION_CODE_ID
        defaultEsignTransationShouldNotBeFound("applicationCodeId.in=" + UPDATED_APPLICATION_CODE_ID);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByApplicationCodeIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where applicationCodeId is not null
        defaultEsignTransationShouldBeFound("applicationCodeId.specified=true");

        // Get all the esignTransationList where applicationCodeId is null
        defaultEsignTransationShouldNotBeFound("applicationCodeId.specified=false");
    }
                @Test
    @Transactional
    public void getAllEsignTransationsByApplicationCodeIdContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where applicationCodeId contains DEFAULT_APPLICATION_CODE_ID
        defaultEsignTransationShouldBeFound("applicationCodeId.contains=" + DEFAULT_APPLICATION_CODE_ID);

        // Get all the esignTransationList where applicationCodeId contains UPDATED_APPLICATION_CODE_ID
        defaultEsignTransationShouldNotBeFound("applicationCodeId.contains=" + UPDATED_APPLICATION_CODE_ID);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByApplicationCodeIdNotContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where applicationCodeId does not contain DEFAULT_APPLICATION_CODE_ID
        defaultEsignTransationShouldNotBeFound("applicationCodeId.doesNotContain=" + DEFAULT_APPLICATION_CODE_ID);

        // Get all the esignTransationList where applicationCodeId does not contain UPDATED_APPLICATION_CODE_ID
        defaultEsignTransationShouldBeFound("applicationCodeId.doesNotContain=" + UPDATED_APPLICATION_CODE_ID);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByOrgResponseSuccessURLIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgResponseSuccessURL equals to DEFAULT_ORG_RESPONSE_SUCCESS_URL
        defaultEsignTransationShouldBeFound("orgResponseSuccessURL.equals=" + DEFAULT_ORG_RESPONSE_SUCCESS_URL);

        // Get all the esignTransationList where orgResponseSuccessURL equals to UPDATED_ORG_RESPONSE_SUCCESS_URL
        defaultEsignTransationShouldNotBeFound("orgResponseSuccessURL.equals=" + UPDATED_ORG_RESPONSE_SUCCESS_URL);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByOrgResponseSuccessURLIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgResponseSuccessURL not equals to DEFAULT_ORG_RESPONSE_SUCCESS_URL
        defaultEsignTransationShouldNotBeFound("orgResponseSuccessURL.notEquals=" + DEFAULT_ORG_RESPONSE_SUCCESS_URL);

        // Get all the esignTransationList where orgResponseSuccessURL not equals to UPDATED_ORG_RESPONSE_SUCCESS_URL
        defaultEsignTransationShouldBeFound("orgResponseSuccessURL.notEquals=" + UPDATED_ORG_RESPONSE_SUCCESS_URL);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByOrgResponseSuccessURLIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgResponseSuccessURL in DEFAULT_ORG_RESPONSE_SUCCESS_URL or UPDATED_ORG_RESPONSE_SUCCESS_URL
        defaultEsignTransationShouldBeFound("orgResponseSuccessURL.in=" + DEFAULT_ORG_RESPONSE_SUCCESS_URL + "," + UPDATED_ORG_RESPONSE_SUCCESS_URL);

        // Get all the esignTransationList where orgResponseSuccessURL equals to UPDATED_ORG_RESPONSE_SUCCESS_URL
        defaultEsignTransationShouldNotBeFound("orgResponseSuccessURL.in=" + UPDATED_ORG_RESPONSE_SUCCESS_URL);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByOrgResponseSuccessURLIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgResponseSuccessURL is not null
        defaultEsignTransationShouldBeFound("orgResponseSuccessURL.specified=true");

        // Get all the esignTransationList where orgResponseSuccessURL is null
        defaultEsignTransationShouldNotBeFound("orgResponseSuccessURL.specified=false");
    }
                @Test
    @Transactional
    public void getAllEsignTransationsByOrgResponseSuccessURLContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgResponseSuccessURL contains DEFAULT_ORG_RESPONSE_SUCCESS_URL
        defaultEsignTransationShouldBeFound("orgResponseSuccessURL.contains=" + DEFAULT_ORG_RESPONSE_SUCCESS_URL);

        // Get all the esignTransationList where orgResponseSuccessURL contains UPDATED_ORG_RESPONSE_SUCCESS_URL
        defaultEsignTransationShouldNotBeFound("orgResponseSuccessURL.contains=" + UPDATED_ORG_RESPONSE_SUCCESS_URL);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByOrgResponseSuccessURLNotContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgResponseSuccessURL does not contain DEFAULT_ORG_RESPONSE_SUCCESS_URL
        defaultEsignTransationShouldNotBeFound("orgResponseSuccessURL.doesNotContain=" + DEFAULT_ORG_RESPONSE_SUCCESS_URL);

        // Get all the esignTransationList where orgResponseSuccessURL does not contain UPDATED_ORG_RESPONSE_SUCCESS_URL
        defaultEsignTransationShouldBeFound("orgResponseSuccessURL.doesNotContain=" + UPDATED_ORG_RESPONSE_SUCCESS_URL);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByOrgResponseFailURLIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgResponseFailURL equals to DEFAULT_ORG_RESPONSE_FAIL_URL
        defaultEsignTransationShouldBeFound("orgResponseFailURL.equals=" + DEFAULT_ORG_RESPONSE_FAIL_URL);

        // Get all the esignTransationList where orgResponseFailURL equals to UPDATED_ORG_RESPONSE_FAIL_URL
        defaultEsignTransationShouldNotBeFound("orgResponseFailURL.equals=" + UPDATED_ORG_RESPONSE_FAIL_URL);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByOrgResponseFailURLIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgResponseFailURL not equals to DEFAULT_ORG_RESPONSE_FAIL_URL
        defaultEsignTransationShouldNotBeFound("orgResponseFailURL.notEquals=" + DEFAULT_ORG_RESPONSE_FAIL_URL);

        // Get all the esignTransationList where orgResponseFailURL not equals to UPDATED_ORG_RESPONSE_FAIL_URL
        defaultEsignTransationShouldBeFound("orgResponseFailURL.notEquals=" + UPDATED_ORG_RESPONSE_FAIL_URL);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByOrgResponseFailURLIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgResponseFailURL in DEFAULT_ORG_RESPONSE_FAIL_URL or UPDATED_ORG_RESPONSE_FAIL_URL
        defaultEsignTransationShouldBeFound("orgResponseFailURL.in=" + DEFAULT_ORG_RESPONSE_FAIL_URL + "," + UPDATED_ORG_RESPONSE_FAIL_URL);

        // Get all the esignTransationList where orgResponseFailURL equals to UPDATED_ORG_RESPONSE_FAIL_URL
        defaultEsignTransationShouldNotBeFound("orgResponseFailURL.in=" + UPDATED_ORG_RESPONSE_FAIL_URL);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByOrgResponseFailURLIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgResponseFailURL is not null
        defaultEsignTransationShouldBeFound("orgResponseFailURL.specified=true");

        // Get all the esignTransationList where orgResponseFailURL is null
        defaultEsignTransationShouldNotBeFound("orgResponseFailURL.specified=false");
    }
                @Test
    @Transactional
    public void getAllEsignTransationsByOrgResponseFailURLContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgResponseFailURL contains DEFAULT_ORG_RESPONSE_FAIL_URL
        defaultEsignTransationShouldBeFound("orgResponseFailURL.contains=" + DEFAULT_ORG_RESPONSE_FAIL_URL);

        // Get all the esignTransationList where orgResponseFailURL contains UPDATED_ORG_RESPONSE_FAIL_URL
        defaultEsignTransationShouldNotBeFound("orgResponseFailURL.contains=" + UPDATED_ORG_RESPONSE_FAIL_URL);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByOrgResponseFailURLNotContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgResponseFailURL does not contain DEFAULT_ORG_RESPONSE_FAIL_URL
        defaultEsignTransationShouldNotBeFound("orgResponseFailURL.doesNotContain=" + DEFAULT_ORG_RESPONSE_FAIL_URL);

        // Get all the esignTransationList where orgResponseFailURL does not contain UPDATED_ORG_RESPONSE_FAIL_URL
        defaultEsignTransationShouldBeFound("orgResponseFailURL.doesNotContain=" + UPDATED_ORG_RESPONSE_FAIL_URL);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByOrgStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgStatus equals to DEFAULT_ORG_STATUS
        defaultEsignTransationShouldBeFound("orgStatus.equals=" + DEFAULT_ORG_STATUS);

        // Get all the esignTransationList where orgStatus equals to UPDATED_ORG_STATUS
        defaultEsignTransationShouldNotBeFound("orgStatus.equals=" + UPDATED_ORG_STATUS);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByOrgStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgStatus not equals to DEFAULT_ORG_STATUS
        defaultEsignTransationShouldNotBeFound("orgStatus.notEquals=" + DEFAULT_ORG_STATUS);

        // Get all the esignTransationList where orgStatus not equals to UPDATED_ORG_STATUS
        defaultEsignTransationShouldBeFound("orgStatus.notEquals=" + UPDATED_ORG_STATUS);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByOrgStatusIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgStatus in DEFAULT_ORG_STATUS or UPDATED_ORG_STATUS
        defaultEsignTransationShouldBeFound("orgStatus.in=" + DEFAULT_ORG_STATUS + "," + UPDATED_ORG_STATUS);

        // Get all the esignTransationList where orgStatus equals to UPDATED_ORG_STATUS
        defaultEsignTransationShouldNotBeFound("orgStatus.in=" + UPDATED_ORG_STATUS);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByOrgStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgStatus is not null
        defaultEsignTransationShouldBeFound("orgStatus.specified=true");

        // Get all the esignTransationList where orgStatus is null
        defaultEsignTransationShouldNotBeFound("orgStatus.specified=false");
    }
                @Test
    @Transactional
    public void getAllEsignTransationsByOrgStatusContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgStatus contains DEFAULT_ORG_STATUS
        defaultEsignTransationShouldBeFound("orgStatus.contains=" + DEFAULT_ORG_STATUS);

        // Get all the esignTransationList where orgStatus contains UPDATED_ORG_STATUS
        defaultEsignTransationShouldNotBeFound("orgStatus.contains=" + UPDATED_ORG_STATUS);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByOrgStatusNotContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where orgStatus does not contain DEFAULT_ORG_STATUS
        defaultEsignTransationShouldNotBeFound("orgStatus.doesNotContain=" + DEFAULT_ORG_STATUS);

        // Get all the esignTransationList where orgStatus does not contain UPDATED_ORG_STATUS
        defaultEsignTransationShouldBeFound("orgStatus.doesNotContain=" + UPDATED_ORG_STATUS);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByRequestDateIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestDate equals to DEFAULT_REQUEST_DATE
        defaultEsignTransationShouldBeFound("requestDate.equals=" + DEFAULT_REQUEST_DATE);

        // Get all the esignTransationList where requestDate equals to UPDATED_REQUEST_DATE
        defaultEsignTransationShouldNotBeFound("requestDate.equals=" + UPDATED_REQUEST_DATE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestDate not equals to DEFAULT_REQUEST_DATE
        defaultEsignTransationShouldNotBeFound("requestDate.notEquals=" + DEFAULT_REQUEST_DATE);

        // Get all the esignTransationList where requestDate not equals to UPDATED_REQUEST_DATE
        defaultEsignTransationShouldBeFound("requestDate.notEquals=" + UPDATED_REQUEST_DATE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestDateIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestDate in DEFAULT_REQUEST_DATE or UPDATED_REQUEST_DATE
        defaultEsignTransationShouldBeFound("requestDate.in=" + DEFAULT_REQUEST_DATE + "," + UPDATED_REQUEST_DATE);

        // Get all the esignTransationList where requestDate equals to UPDATED_REQUEST_DATE
        defaultEsignTransationShouldNotBeFound("requestDate.in=" + UPDATED_REQUEST_DATE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestDate is not null
        defaultEsignTransationShouldBeFound("requestDate.specified=true");

        // Get all the esignTransationList where requestDate is null
        defaultEsignTransationShouldNotBeFound("requestDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestDate is greater than or equal to DEFAULT_REQUEST_DATE
        defaultEsignTransationShouldBeFound("requestDate.greaterThanOrEqual=" + DEFAULT_REQUEST_DATE);

        // Get all the esignTransationList where requestDate is greater than or equal to UPDATED_REQUEST_DATE
        defaultEsignTransationShouldNotBeFound("requestDate.greaterThanOrEqual=" + UPDATED_REQUEST_DATE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestDate is less than or equal to DEFAULT_REQUEST_DATE
        defaultEsignTransationShouldBeFound("requestDate.lessThanOrEqual=" + DEFAULT_REQUEST_DATE);

        // Get all the esignTransationList where requestDate is less than or equal to SMALLER_REQUEST_DATE
        defaultEsignTransationShouldNotBeFound("requestDate.lessThanOrEqual=" + SMALLER_REQUEST_DATE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestDateIsLessThanSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestDate is less than DEFAULT_REQUEST_DATE
        defaultEsignTransationShouldNotBeFound("requestDate.lessThan=" + DEFAULT_REQUEST_DATE);

        // Get all the esignTransationList where requestDate is less than UPDATED_REQUEST_DATE
        defaultEsignTransationShouldBeFound("requestDate.lessThan=" + UPDATED_REQUEST_DATE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestDate is greater than DEFAULT_REQUEST_DATE
        defaultEsignTransationShouldNotBeFound("requestDate.greaterThan=" + DEFAULT_REQUEST_DATE);

        // Get all the esignTransationList where requestDate is greater than SMALLER_REQUEST_DATE
        defaultEsignTransationShouldBeFound("requestDate.greaterThan=" + SMALLER_REQUEST_DATE);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByOrganisationIdCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where organisationIdCode equals to DEFAULT_ORGANISATION_ID_CODE
        defaultEsignTransationShouldBeFound("organisationIdCode.equals=" + DEFAULT_ORGANISATION_ID_CODE);

        // Get all the esignTransationList where organisationIdCode equals to UPDATED_ORGANISATION_ID_CODE
        defaultEsignTransationShouldNotBeFound("organisationIdCode.equals=" + UPDATED_ORGANISATION_ID_CODE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByOrganisationIdCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where organisationIdCode not equals to DEFAULT_ORGANISATION_ID_CODE
        defaultEsignTransationShouldNotBeFound("organisationIdCode.notEquals=" + DEFAULT_ORGANISATION_ID_CODE);

        // Get all the esignTransationList where organisationIdCode not equals to UPDATED_ORGANISATION_ID_CODE
        defaultEsignTransationShouldBeFound("organisationIdCode.notEquals=" + UPDATED_ORGANISATION_ID_CODE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByOrganisationIdCodeIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where organisationIdCode in DEFAULT_ORGANISATION_ID_CODE or UPDATED_ORGANISATION_ID_CODE
        defaultEsignTransationShouldBeFound("organisationIdCode.in=" + DEFAULT_ORGANISATION_ID_CODE + "," + UPDATED_ORGANISATION_ID_CODE);

        // Get all the esignTransationList where organisationIdCode equals to UPDATED_ORGANISATION_ID_CODE
        defaultEsignTransationShouldNotBeFound("organisationIdCode.in=" + UPDATED_ORGANISATION_ID_CODE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByOrganisationIdCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where organisationIdCode is not null
        defaultEsignTransationShouldBeFound("organisationIdCode.specified=true");

        // Get all the esignTransationList where organisationIdCode is null
        defaultEsignTransationShouldNotBeFound("organisationIdCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllEsignTransationsByOrganisationIdCodeContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where organisationIdCode contains DEFAULT_ORGANISATION_ID_CODE
        defaultEsignTransationShouldBeFound("organisationIdCode.contains=" + DEFAULT_ORGANISATION_ID_CODE);

        // Get all the esignTransationList where organisationIdCode contains UPDATED_ORGANISATION_ID_CODE
        defaultEsignTransationShouldNotBeFound("organisationIdCode.contains=" + UPDATED_ORGANISATION_ID_CODE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByOrganisationIdCodeNotContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where organisationIdCode does not contain DEFAULT_ORGANISATION_ID_CODE
        defaultEsignTransationShouldNotBeFound("organisationIdCode.doesNotContain=" + DEFAULT_ORGANISATION_ID_CODE);

        // Get all the esignTransationList where organisationIdCode does not contain UPDATED_ORGANISATION_ID_CODE
        defaultEsignTransationShouldBeFound("organisationIdCode.doesNotContain=" + UPDATED_ORGANISATION_ID_CODE);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsBySignerIdIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where signerId equals to DEFAULT_SIGNER_ID
        defaultEsignTransationShouldBeFound("signerId.equals=" + DEFAULT_SIGNER_ID);

        // Get all the esignTransationList where signerId equals to UPDATED_SIGNER_ID
        defaultEsignTransationShouldNotBeFound("signerId.equals=" + UPDATED_SIGNER_ID);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsBySignerIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where signerId not equals to DEFAULT_SIGNER_ID
        defaultEsignTransationShouldNotBeFound("signerId.notEquals=" + DEFAULT_SIGNER_ID);

        // Get all the esignTransationList where signerId not equals to UPDATED_SIGNER_ID
        defaultEsignTransationShouldBeFound("signerId.notEquals=" + UPDATED_SIGNER_ID);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsBySignerIdIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where signerId in DEFAULT_SIGNER_ID or UPDATED_SIGNER_ID
        defaultEsignTransationShouldBeFound("signerId.in=" + DEFAULT_SIGNER_ID + "," + UPDATED_SIGNER_ID);

        // Get all the esignTransationList where signerId equals to UPDATED_SIGNER_ID
        defaultEsignTransationShouldNotBeFound("signerId.in=" + UPDATED_SIGNER_ID);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsBySignerIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where signerId is not null
        defaultEsignTransationShouldBeFound("signerId.specified=true");

        // Get all the esignTransationList where signerId is null
        defaultEsignTransationShouldNotBeFound("signerId.specified=false");
    }
                @Test
    @Transactional
    public void getAllEsignTransationsBySignerIdContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where signerId contains DEFAULT_SIGNER_ID
        defaultEsignTransationShouldBeFound("signerId.contains=" + DEFAULT_SIGNER_ID);

        // Get all the esignTransationList where signerId contains UPDATED_SIGNER_ID
        defaultEsignTransationShouldNotBeFound("signerId.contains=" + UPDATED_SIGNER_ID);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsBySignerIdNotContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where signerId does not contain DEFAULT_SIGNER_ID
        defaultEsignTransationShouldNotBeFound("signerId.doesNotContain=" + DEFAULT_SIGNER_ID);

        // Get all the esignTransationList where signerId does not contain UPDATED_SIGNER_ID
        defaultEsignTransationShouldBeFound("signerId.doesNotContain=" + UPDATED_SIGNER_ID);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByRequestTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestType equals to DEFAULT_REQUEST_TYPE
        defaultEsignTransationShouldBeFound("requestType.equals=" + DEFAULT_REQUEST_TYPE);

        // Get all the esignTransationList where requestType equals to UPDATED_REQUEST_TYPE
        defaultEsignTransationShouldNotBeFound("requestType.equals=" + UPDATED_REQUEST_TYPE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestType not equals to DEFAULT_REQUEST_TYPE
        defaultEsignTransationShouldNotBeFound("requestType.notEquals=" + DEFAULT_REQUEST_TYPE);

        // Get all the esignTransationList where requestType not equals to UPDATED_REQUEST_TYPE
        defaultEsignTransationShouldBeFound("requestType.notEquals=" + UPDATED_REQUEST_TYPE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestTypeIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestType in DEFAULT_REQUEST_TYPE or UPDATED_REQUEST_TYPE
        defaultEsignTransationShouldBeFound("requestType.in=" + DEFAULT_REQUEST_TYPE + "," + UPDATED_REQUEST_TYPE);

        // Get all the esignTransationList where requestType equals to UPDATED_REQUEST_TYPE
        defaultEsignTransationShouldNotBeFound("requestType.in=" + UPDATED_REQUEST_TYPE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestType is not null
        defaultEsignTransationShouldBeFound("requestType.specified=true");

        // Get all the esignTransationList where requestType is null
        defaultEsignTransationShouldNotBeFound("requestType.specified=false");
    }
                @Test
    @Transactional
    public void getAllEsignTransationsByRequestTypeContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestType contains DEFAULT_REQUEST_TYPE
        defaultEsignTransationShouldBeFound("requestType.contains=" + DEFAULT_REQUEST_TYPE);

        // Get all the esignTransationList where requestType contains UPDATED_REQUEST_TYPE
        defaultEsignTransationShouldNotBeFound("requestType.contains=" + UPDATED_REQUEST_TYPE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestTypeNotContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestType does not contain DEFAULT_REQUEST_TYPE
        defaultEsignTransationShouldNotBeFound("requestType.doesNotContain=" + DEFAULT_REQUEST_TYPE);

        // Get all the esignTransationList where requestType does not contain UPDATED_REQUEST_TYPE
        defaultEsignTransationShouldBeFound("requestType.doesNotContain=" + UPDATED_REQUEST_TYPE);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByRequestStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestStatus equals to DEFAULT_REQUEST_STATUS
        defaultEsignTransationShouldBeFound("requestStatus.equals=" + DEFAULT_REQUEST_STATUS);

        // Get all the esignTransationList where requestStatus equals to UPDATED_REQUEST_STATUS
        defaultEsignTransationShouldNotBeFound("requestStatus.equals=" + UPDATED_REQUEST_STATUS);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestStatus not equals to DEFAULT_REQUEST_STATUS
        defaultEsignTransationShouldNotBeFound("requestStatus.notEquals=" + DEFAULT_REQUEST_STATUS);

        // Get all the esignTransationList where requestStatus not equals to UPDATED_REQUEST_STATUS
        defaultEsignTransationShouldBeFound("requestStatus.notEquals=" + UPDATED_REQUEST_STATUS);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestStatusIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestStatus in DEFAULT_REQUEST_STATUS or UPDATED_REQUEST_STATUS
        defaultEsignTransationShouldBeFound("requestStatus.in=" + DEFAULT_REQUEST_STATUS + "," + UPDATED_REQUEST_STATUS);

        // Get all the esignTransationList where requestStatus equals to UPDATED_REQUEST_STATUS
        defaultEsignTransationShouldNotBeFound("requestStatus.in=" + UPDATED_REQUEST_STATUS);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestStatus is not null
        defaultEsignTransationShouldBeFound("requestStatus.specified=true");

        // Get all the esignTransationList where requestStatus is null
        defaultEsignTransationShouldNotBeFound("requestStatus.specified=false");
    }
                @Test
    @Transactional
    public void getAllEsignTransationsByRequestStatusContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestStatus contains DEFAULT_REQUEST_STATUS
        defaultEsignTransationShouldBeFound("requestStatus.contains=" + DEFAULT_REQUEST_STATUS);

        // Get all the esignTransationList where requestStatus contains UPDATED_REQUEST_STATUS
        defaultEsignTransationShouldNotBeFound("requestStatus.contains=" + UPDATED_REQUEST_STATUS);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestStatusNotContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestStatus does not contain DEFAULT_REQUEST_STATUS
        defaultEsignTransationShouldNotBeFound("requestStatus.doesNotContain=" + DEFAULT_REQUEST_STATUS);

        // Get all the esignTransationList where requestStatus does not contain UPDATED_REQUEST_STATUS
        defaultEsignTransationShouldBeFound("requestStatus.doesNotContain=" + UPDATED_REQUEST_STATUS);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByResponseTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseType equals to DEFAULT_RESPONSE_TYPE
        defaultEsignTransationShouldBeFound("responseType.equals=" + DEFAULT_RESPONSE_TYPE);

        // Get all the esignTransationList where responseType equals to UPDATED_RESPONSE_TYPE
        defaultEsignTransationShouldNotBeFound("responseType.equals=" + UPDATED_RESPONSE_TYPE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseType not equals to DEFAULT_RESPONSE_TYPE
        defaultEsignTransationShouldNotBeFound("responseType.notEquals=" + DEFAULT_RESPONSE_TYPE);

        // Get all the esignTransationList where responseType not equals to UPDATED_RESPONSE_TYPE
        defaultEsignTransationShouldBeFound("responseType.notEquals=" + UPDATED_RESPONSE_TYPE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseTypeIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseType in DEFAULT_RESPONSE_TYPE or UPDATED_RESPONSE_TYPE
        defaultEsignTransationShouldBeFound("responseType.in=" + DEFAULT_RESPONSE_TYPE + "," + UPDATED_RESPONSE_TYPE);

        // Get all the esignTransationList where responseType equals to UPDATED_RESPONSE_TYPE
        defaultEsignTransationShouldNotBeFound("responseType.in=" + UPDATED_RESPONSE_TYPE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseType is not null
        defaultEsignTransationShouldBeFound("responseType.specified=true");

        // Get all the esignTransationList where responseType is null
        defaultEsignTransationShouldNotBeFound("responseType.specified=false");
    }
                @Test
    @Transactional
    public void getAllEsignTransationsByResponseTypeContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseType contains DEFAULT_RESPONSE_TYPE
        defaultEsignTransationShouldBeFound("responseType.contains=" + DEFAULT_RESPONSE_TYPE);

        // Get all the esignTransationList where responseType contains UPDATED_RESPONSE_TYPE
        defaultEsignTransationShouldNotBeFound("responseType.contains=" + UPDATED_RESPONSE_TYPE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseTypeNotContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseType does not contain DEFAULT_RESPONSE_TYPE
        defaultEsignTransationShouldNotBeFound("responseType.doesNotContain=" + DEFAULT_RESPONSE_TYPE);

        // Get all the esignTransationList where responseType does not contain UPDATED_RESPONSE_TYPE
        defaultEsignTransationShouldBeFound("responseType.doesNotContain=" + UPDATED_RESPONSE_TYPE);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByResponseStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseStatus equals to DEFAULT_RESPONSE_STATUS
        defaultEsignTransationShouldBeFound("responseStatus.equals=" + DEFAULT_RESPONSE_STATUS);

        // Get all the esignTransationList where responseStatus equals to UPDATED_RESPONSE_STATUS
        defaultEsignTransationShouldNotBeFound("responseStatus.equals=" + UPDATED_RESPONSE_STATUS);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseStatus not equals to DEFAULT_RESPONSE_STATUS
        defaultEsignTransationShouldNotBeFound("responseStatus.notEquals=" + DEFAULT_RESPONSE_STATUS);

        // Get all the esignTransationList where responseStatus not equals to UPDATED_RESPONSE_STATUS
        defaultEsignTransationShouldBeFound("responseStatus.notEquals=" + UPDATED_RESPONSE_STATUS);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseStatusIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseStatus in DEFAULT_RESPONSE_STATUS or UPDATED_RESPONSE_STATUS
        defaultEsignTransationShouldBeFound("responseStatus.in=" + DEFAULT_RESPONSE_STATUS + "," + UPDATED_RESPONSE_STATUS);

        // Get all the esignTransationList where responseStatus equals to UPDATED_RESPONSE_STATUS
        defaultEsignTransationShouldNotBeFound("responseStatus.in=" + UPDATED_RESPONSE_STATUS);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseStatus is not null
        defaultEsignTransationShouldBeFound("responseStatus.specified=true");

        // Get all the esignTransationList where responseStatus is null
        defaultEsignTransationShouldNotBeFound("responseStatus.specified=false");
    }
                @Test
    @Transactional
    public void getAllEsignTransationsByResponseStatusContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseStatus contains DEFAULT_RESPONSE_STATUS
        defaultEsignTransationShouldBeFound("responseStatus.contains=" + DEFAULT_RESPONSE_STATUS);

        // Get all the esignTransationList where responseStatus contains UPDATED_RESPONSE_STATUS
        defaultEsignTransationShouldNotBeFound("responseStatus.contains=" + UPDATED_RESPONSE_STATUS);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseStatusNotContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseStatus does not contain DEFAULT_RESPONSE_STATUS
        defaultEsignTransationShouldNotBeFound("responseStatus.doesNotContain=" + DEFAULT_RESPONSE_STATUS);

        // Get all the esignTransationList where responseStatus does not contain UPDATED_RESPONSE_STATUS
        defaultEsignTransationShouldBeFound("responseStatus.doesNotContain=" + UPDATED_RESPONSE_STATUS);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByResponseCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseCode equals to DEFAULT_RESPONSE_CODE
        defaultEsignTransationShouldBeFound("responseCode.equals=" + DEFAULT_RESPONSE_CODE);

        // Get all the esignTransationList where responseCode equals to UPDATED_RESPONSE_CODE
        defaultEsignTransationShouldNotBeFound("responseCode.equals=" + UPDATED_RESPONSE_CODE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseCode not equals to DEFAULT_RESPONSE_CODE
        defaultEsignTransationShouldNotBeFound("responseCode.notEquals=" + DEFAULT_RESPONSE_CODE);

        // Get all the esignTransationList where responseCode not equals to UPDATED_RESPONSE_CODE
        defaultEsignTransationShouldBeFound("responseCode.notEquals=" + UPDATED_RESPONSE_CODE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseCodeIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseCode in DEFAULT_RESPONSE_CODE or UPDATED_RESPONSE_CODE
        defaultEsignTransationShouldBeFound("responseCode.in=" + DEFAULT_RESPONSE_CODE + "," + UPDATED_RESPONSE_CODE);

        // Get all the esignTransationList where responseCode equals to UPDATED_RESPONSE_CODE
        defaultEsignTransationShouldNotBeFound("responseCode.in=" + UPDATED_RESPONSE_CODE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseCode is not null
        defaultEsignTransationShouldBeFound("responseCode.specified=true");

        // Get all the esignTransationList where responseCode is null
        defaultEsignTransationShouldNotBeFound("responseCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllEsignTransationsByResponseCodeContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseCode contains DEFAULT_RESPONSE_CODE
        defaultEsignTransationShouldBeFound("responseCode.contains=" + DEFAULT_RESPONSE_CODE);

        // Get all the esignTransationList where responseCode contains UPDATED_RESPONSE_CODE
        defaultEsignTransationShouldNotBeFound("responseCode.contains=" + UPDATED_RESPONSE_CODE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseCodeNotContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseCode does not contain DEFAULT_RESPONSE_CODE
        defaultEsignTransationShouldNotBeFound("responseCode.doesNotContain=" + DEFAULT_RESPONSE_CODE);

        // Get all the esignTransationList where responseCode does not contain UPDATED_RESPONSE_CODE
        defaultEsignTransationShouldBeFound("responseCode.doesNotContain=" + UPDATED_RESPONSE_CODE);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByRequestXmlIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestXml equals to DEFAULT_REQUEST_XML
        defaultEsignTransationShouldBeFound("requestXml.equals=" + DEFAULT_REQUEST_XML);

        // Get all the esignTransationList where requestXml equals to UPDATED_REQUEST_XML
        defaultEsignTransationShouldNotBeFound("requestXml.equals=" + UPDATED_REQUEST_XML);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestXmlIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestXml not equals to DEFAULT_REQUEST_XML
        defaultEsignTransationShouldNotBeFound("requestXml.notEquals=" + DEFAULT_REQUEST_XML);

        // Get all the esignTransationList where requestXml not equals to UPDATED_REQUEST_XML
        defaultEsignTransationShouldBeFound("requestXml.notEquals=" + UPDATED_REQUEST_XML);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestXmlIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestXml in DEFAULT_REQUEST_XML or UPDATED_REQUEST_XML
        defaultEsignTransationShouldBeFound("requestXml.in=" + DEFAULT_REQUEST_XML + "," + UPDATED_REQUEST_XML);

        // Get all the esignTransationList where requestXml equals to UPDATED_REQUEST_XML
        defaultEsignTransationShouldNotBeFound("requestXml.in=" + UPDATED_REQUEST_XML);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestXmlIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestXml is not null
        defaultEsignTransationShouldBeFound("requestXml.specified=true");

        // Get all the esignTransationList where requestXml is null
        defaultEsignTransationShouldNotBeFound("requestXml.specified=false");
    }
                @Test
    @Transactional
    public void getAllEsignTransationsByRequestXmlContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestXml contains DEFAULT_REQUEST_XML
        defaultEsignTransationShouldBeFound("requestXml.contains=" + DEFAULT_REQUEST_XML);

        // Get all the esignTransationList where requestXml contains UPDATED_REQUEST_XML
        defaultEsignTransationShouldNotBeFound("requestXml.contains=" + UPDATED_REQUEST_XML);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByRequestXmlNotContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where requestXml does not contain DEFAULT_REQUEST_XML
        defaultEsignTransationShouldNotBeFound("requestXml.doesNotContain=" + DEFAULT_REQUEST_XML);

        // Get all the esignTransationList where requestXml does not contain UPDATED_REQUEST_XML
        defaultEsignTransationShouldBeFound("requestXml.doesNotContain=" + UPDATED_REQUEST_XML);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByResponseXmlIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseXml equals to DEFAULT_RESPONSE_XML
        defaultEsignTransationShouldBeFound("responseXml.equals=" + DEFAULT_RESPONSE_XML);

        // Get all the esignTransationList where responseXml equals to UPDATED_RESPONSE_XML
        defaultEsignTransationShouldNotBeFound("responseXml.equals=" + UPDATED_RESPONSE_XML);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseXmlIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseXml not equals to DEFAULT_RESPONSE_XML
        defaultEsignTransationShouldNotBeFound("responseXml.notEquals=" + DEFAULT_RESPONSE_XML);

        // Get all the esignTransationList where responseXml not equals to UPDATED_RESPONSE_XML
        defaultEsignTransationShouldBeFound("responseXml.notEquals=" + UPDATED_RESPONSE_XML);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseXmlIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseXml in DEFAULT_RESPONSE_XML or UPDATED_RESPONSE_XML
        defaultEsignTransationShouldBeFound("responseXml.in=" + DEFAULT_RESPONSE_XML + "," + UPDATED_RESPONSE_XML);

        // Get all the esignTransationList where responseXml equals to UPDATED_RESPONSE_XML
        defaultEsignTransationShouldNotBeFound("responseXml.in=" + UPDATED_RESPONSE_XML);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseXmlIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseXml is not null
        defaultEsignTransationShouldBeFound("responseXml.specified=true");

        // Get all the esignTransationList where responseXml is null
        defaultEsignTransationShouldNotBeFound("responseXml.specified=false");
    }
                @Test
    @Transactional
    public void getAllEsignTransationsByResponseXmlContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseXml contains DEFAULT_RESPONSE_XML
        defaultEsignTransationShouldBeFound("responseXml.contains=" + DEFAULT_RESPONSE_XML);

        // Get all the esignTransationList where responseXml contains UPDATED_RESPONSE_XML
        defaultEsignTransationShouldNotBeFound("responseXml.contains=" + UPDATED_RESPONSE_XML);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseXmlNotContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseXml does not contain DEFAULT_RESPONSE_XML
        defaultEsignTransationShouldNotBeFound("responseXml.doesNotContain=" + DEFAULT_RESPONSE_XML);

        // Get all the esignTransationList where responseXml does not contain UPDATED_RESPONSE_XML
        defaultEsignTransationShouldBeFound("responseXml.doesNotContain=" + UPDATED_RESPONSE_XML);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByResponseDateIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseDate equals to DEFAULT_RESPONSE_DATE
        defaultEsignTransationShouldBeFound("responseDate.equals=" + DEFAULT_RESPONSE_DATE);

        // Get all the esignTransationList where responseDate equals to UPDATED_RESPONSE_DATE
        defaultEsignTransationShouldNotBeFound("responseDate.equals=" + UPDATED_RESPONSE_DATE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseDate not equals to DEFAULT_RESPONSE_DATE
        defaultEsignTransationShouldNotBeFound("responseDate.notEquals=" + DEFAULT_RESPONSE_DATE);

        // Get all the esignTransationList where responseDate not equals to UPDATED_RESPONSE_DATE
        defaultEsignTransationShouldBeFound("responseDate.notEquals=" + UPDATED_RESPONSE_DATE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseDateIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseDate in DEFAULT_RESPONSE_DATE or UPDATED_RESPONSE_DATE
        defaultEsignTransationShouldBeFound("responseDate.in=" + DEFAULT_RESPONSE_DATE + "," + UPDATED_RESPONSE_DATE);

        // Get all the esignTransationList where responseDate equals to UPDATED_RESPONSE_DATE
        defaultEsignTransationShouldNotBeFound("responseDate.in=" + UPDATED_RESPONSE_DATE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseDate is not null
        defaultEsignTransationShouldBeFound("responseDate.specified=true");

        // Get all the esignTransationList where responseDate is null
        defaultEsignTransationShouldNotBeFound("responseDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseDate is greater than or equal to DEFAULT_RESPONSE_DATE
        defaultEsignTransationShouldBeFound("responseDate.greaterThanOrEqual=" + DEFAULT_RESPONSE_DATE);

        // Get all the esignTransationList where responseDate is greater than or equal to UPDATED_RESPONSE_DATE
        defaultEsignTransationShouldNotBeFound("responseDate.greaterThanOrEqual=" + UPDATED_RESPONSE_DATE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseDate is less than or equal to DEFAULT_RESPONSE_DATE
        defaultEsignTransationShouldBeFound("responseDate.lessThanOrEqual=" + DEFAULT_RESPONSE_DATE);

        // Get all the esignTransationList where responseDate is less than or equal to SMALLER_RESPONSE_DATE
        defaultEsignTransationShouldNotBeFound("responseDate.lessThanOrEqual=" + SMALLER_RESPONSE_DATE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseDateIsLessThanSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseDate is less than DEFAULT_RESPONSE_DATE
        defaultEsignTransationShouldNotBeFound("responseDate.lessThan=" + DEFAULT_RESPONSE_DATE);

        // Get all the esignTransationList where responseDate is less than UPDATED_RESPONSE_DATE
        defaultEsignTransationShouldBeFound("responseDate.lessThan=" + UPDATED_RESPONSE_DATE);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByResponseDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where responseDate is greater than DEFAULT_RESPONSE_DATE
        defaultEsignTransationShouldNotBeFound("responseDate.greaterThan=" + DEFAULT_RESPONSE_DATE);

        // Get all the esignTransationList where responseDate is greater than SMALLER_RESPONSE_DATE
        defaultEsignTransationShouldBeFound("responseDate.greaterThan=" + SMALLER_RESPONSE_DATE);
    }


    @Test
    @Transactional
    public void getAllEsignTransationsByTsIsEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where ts equals to DEFAULT_TS
        defaultEsignTransationShouldBeFound("ts.equals=" + DEFAULT_TS);

        // Get all the esignTransationList where ts equals to UPDATED_TS
        defaultEsignTransationShouldNotBeFound("ts.equals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByTsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where ts not equals to DEFAULT_TS
        defaultEsignTransationShouldNotBeFound("ts.notEquals=" + DEFAULT_TS);

        // Get all the esignTransationList where ts not equals to UPDATED_TS
        defaultEsignTransationShouldBeFound("ts.notEquals=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByTsIsInShouldWork() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where ts in DEFAULT_TS or UPDATED_TS
        defaultEsignTransationShouldBeFound("ts.in=" + DEFAULT_TS + "," + UPDATED_TS);

        // Get all the esignTransationList where ts equals to UPDATED_TS
        defaultEsignTransationShouldNotBeFound("ts.in=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByTsIsNullOrNotNull() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where ts is not null
        defaultEsignTransationShouldBeFound("ts.specified=true");

        // Get all the esignTransationList where ts is null
        defaultEsignTransationShouldNotBeFound("ts.specified=false");
    }
                @Test
    @Transactional
    public void getAllEsignTransationsByTsContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where ts contains DEFAULT_TS
        defaultEsignTransationShouldBeFound("ts.contains=" + DEFAULT_TS);

        // Get all the esignTransationList where ts contains UPDATED_TS
        defaultEsignTransationShouldNotBeFound("ts.contains=" + UPDATED_TS);
    }

    @Test
    @Transactional
    public void getAllEsignTransationsByTsNotContainsSomething() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        // Get all the esignTransationList where ts does not contain DEFAULT_TS
        defaultEsignTransationShouldNotBeFound("ts.doesNotContain=" + DEFAULT_TS);

        // Get all the esignTransationList where ts does not contain UPDATED_TS
        defaultEsignTransationShouldBeFound("ts.doesNotContain=" + UPDATED_TS);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultEsignTransationShouldBeFound(String filter) throws Exception {
        restEsignTransationMockMvc.perform(get("/api/esign-transations?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(esignTransation.getId().intValue())))
            .andExpect(jsonPath("$.[*].txnId").value(hasItem(DEFAULT_TXN_ID)))
            .andExpect(jsonPath("$.[*].userCodeId").value(hasItem(DEFAULT_USER_CODE_ID)))
            .andExpect(jsonPath("$.[*].applicationCodeId").value(hasItem(DEFAULT_APPLICATION_CODE_ID)))
            .andExpect(jsonPath("$.[*].orgResponseSuccessURL").value(hasItem(DEFAULT_ORG_RESPONSE_SUCCESS_URL)))
            .andExpect(jsonPath("$.[*].orgResponseFailURL").value(hasItem(DEFAULT_ORG_RESPONSE_FAIL_URL)))
            .andExpect(jsonPath("$.[*].orgStatus").value(hasItem(DEFAULT_ORG_STATUS)))
            .andExpect(jsonPath("$.[*].requestDate").value(hasItem(DEFAULT_REQUEST_DATE.toString())))
            .andExpect(jsonPath("$.[*].organisationIdCode").value(hasItem(DEFAULT_ORGANISATION_ID_CODE)))
            .andExpect(jsonPath("$.[*].signerId").value(hasItem(DEFAULT_SIGNER_ID)))
            .andExpect(jsonPath("$.[*].requestType").value(hasItem(DEFAULT_REQUEST_TYPE)))
            .andExpect(jsonPath("$.[*].requestStatus").value(hasItem(DEFAULT_REQUEST_STATUS)))
            .andExpect(jsonPath("$.[*].responseType").value(hasItem(DEFAULT_RESPONSE_TYPE)))
            .andExpect(jsonPath("$.[*].responseStatus").value(hasItem(DEFAULT_RESPONSE_STATUS)))
            .andExpect(jsonPath("$.[*].responseCode").value(hasItem(DEFAULT_RESPONSE_CODE)))
            .andExpect(jsonPath("$.[*].requestXml").value(hasItem(DEFAULT_REQUEST_XML)))
            .andExpect(jsonPath("$.[*].responseXml").value(hasItem(DEFAULT_RESPONSE_XML)))
            .andExpect(jsonPath("$.[*].responseDate").value(hasItem(DEFAULT_RESPONSE_DATE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS)));

        // Check, that the count call also returns 1
        restEsignTransationMockMvc.perform(get("/api/esign-transations/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultEsignTransationShouldNotBeFound(String filter) throws Exception {
        restEsignTransationMockMvc.perform(get("/api/esign-transations?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restEsignTransationMockMvc.perform(get("/api/esign-transations/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingEsignTransation() throws Exception {
        // Get the esignTransation
        restEsignTransationMockMvc.perform(get("/api/esign-transations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEsignTransation() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        int databaseSizeBeforeUpdate = esignTransationRepository.findAll().size();

        // Update the esignTransation
        EsignTransation updatedEsignTransation = esignTransationRepository.findById(esignTransation.getId()).get();
        // Disconnect from session so that the updates on updatedEsignTransation are not directly saved in db
        em.detach(updatedEsignTransation);
        updatedEsignTransation
            .txnId(UPDATED_TXN_ID)
            .userCodeId(UPDATED_USER_CODE_ID)
            .applicationCodeId(UPDATED_APPLICATION_CODE_ID)
            .orgResponseSuccessURL(UPDATED_ORG_RESPONSE_SUCCESS_URL)
            .orgResponseFailURL(UPDATED_ORG_RESPONSE_FAIL_URL)
            .orgStatus(UPDATED_ORG_STATUS)
            .requestDate(UPDATED_REQUEST_DATE)
            .organisationIdCode(UPDATED_ORGANISATION_ID_CODE)
            .signerId(UPDATED_SIGNER_ID)
            .requestType(UPDATED_REQUEST_TYPE)
            .requestStatus(UPDATED_REQUEST_STATUS)
            .responseType(UPDATED_RESPONSE_TYPE)
            .responseStatus(UPDATED_RESPONSE_STATUS)
            .responseCode(UPDATED_RESPONSE_CODE)
            .requestXml(UPDATED_REQUEST_XML)
            .responseXml(UPDATED_RESPONSE_XML)
            .responseDate(UPDATED_RESPONSE_DATE)
            .ts(UPDATED_TS);
        EsignTransationDTO esignTransationDTO = esignTransationMapper.toDto(updatedEsignTransation);

        restEsignTransationMockMvc.perform(put("/api/esign-transations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(esignTransationDTO)))
            .andExpect(status().isOk());

        // Validate the EsignTransation in the database
        List<EsignTransation> esignTransationList = esignTransationRepository.findAll();
        assertThat(esignTransationList).hasSize(databaseSizeBeforeUpdate);
        EsignTransation testEsignTransation = esignTransationList.get(esignTransationList.size() - 1);
        assertThat(testEsignTransation.getTxnId()).isEqualTo(UPDATED_TXN_ID);
        assertThat(testEsignTransation.getUserCodeId()).isEqualTo(UPDATED_USER_CODE_ID);
        assertThat(testEsignTransation.getApplicationCodeId()).isEqualTo(UPDATED_APPLICATION_CODE_ID);
        assertThat(testEsignTransation.getOrgResponseSuccessURL()).isEqualTo(UPDATED_ORG_RESPONSE_SUCCESS_URL);
        assertThat(testEsignTransation.getOrgResponseFailURL()).isEqualTo(UPDATED_ORG_RESPONSE_FAIL_URL);
        assertThat(testEsignTransation.getOrgStatus()).isEqualTo(UPDATED_ORG_STATUS);
        assertThat(testEsignTransation.getRequestDate()).isEqualTo(UPDATED_REQUEST_DATE);
        assertThat(testEsignTransation.getOrganisationIdCode()).isEqualTo(UPDATED_ORGANISATION_ID_CODE);
        assertThat(testEsignTransation.getSignerId()).isEqualTo(UPDATED_SIGNER_ID);
        assertThat(testEsignTransation.getRequestType()).isEqualTo(UPDATED_REQUEST_TYPE);
        assertThat(testEsignTransation.getRequestStatus()).isEqualTo(UPDATED_REQUEST_STATUS);
        assertThat(testEsignTransation.getResponseType()).isEqualTo(UPDATED_RESPONSE_TYPE);
        assertThat(testEsignTransation.getResponseStatus()).isEqualTo(UPDATED_RESPONSE_STATUS);
        assertThat(testEsignTransation.getResponseCode()).isEqualTo(UPDATED_RESPONSE_CODE);
        assertThat(testEsignTransation.getRequestXml()).isEqualTo(UPDATED_REQUEST_XML);
        assertThat(testEsignTransation.getResponseXml()).isEqualTo(UPDATED_RESPONSE_XML);
        assertThat(testEsignTransation.getResponseDate()).isEqualTo(UPDATED_RESPONSE_DATE);
        assertThat(testEsignTransation.getTs()).isEqualTo(UPDATED_TS);

        // Validate the EsignTransation in Elasticsearch
        verify(mockEsignTransationSearchRepository, times(1)).save(testEsignTransation);
    }

    @Test
    @Transactional
    public void updateNonExistingEsignTransation() throws Exception {
        int databaseSizeBeforeUpdate = esignTransationRepository.findAll().size();

        // Create the EsignTransation
        EsignTransationDTO esignTransationDTO = esignTransationMapper.toDto(esignTransation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEsignTransationMockMvc.perform(put("/api/esign-transations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(esignTransationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EsignTransation in the database
        List<EsignTransation> esignTransationList = esignTransationRepository.findAll();
        assertThat(esignTransationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the EsignTransation in Elasticsearch
        verify(mockEsignTransationSearchRepository, times(0)).save(esignTransation);
    }

    @Test
    @Transactional
    public void deleteEsignTransation() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);

        int databaseSizeBeforeDelete = esignTransationRepository.findAll().size();

        // Delete the esignTransation
        restEsignTransationMockMvc.perform(delete("/api/esign-transations/{id}", esignTransation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EsignTransation> esignTransationList = esignTransationRepository.findAll();
        assertThat(esignTransationList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the EsignTransation in Elasticsearch
        verify(mockEsignTransationSearchRepository, times(1)).deleteById(esignTransation.getId());
    }

    @Test
    @Transactional
    public void searchEsignTransation() throws Exception {
        // Initialize the database
        esignTransationRepository.saveAndFlush(esignTransation);
        when(mockEsignTransationSearchRepository.search(queryStringQuery("id:" + esignTransation.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(esignTransation), PageRequest.of(0, 1), 1));
        // Search the esignTransation
        restEsignTransationMockMvc.perform(get("/api/_search/esign-transations?query=id:" + esignTransation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(esignTransation.getId().intValue())))
            .andExpect(jsonPath("$.[*].txnId").value(hasItem(DEFAULT_TXN_ID)))
            .andExpect(jsonPath("$.[*].userCodeId").value(hasItem(DEFAULT_USER_CODE_ID)))
            .andExpect(jsonPath("$.[*].applicationCodeId").value(hasItem(DEFAULT_APPLICATION_CODE_ID)))
            .andExpect(jsonPath("$.[*].orgResponseSuccessURL").value(hasItem(DEFAULT_ORG_RESPONSE_SUCCESS_URL)))
            .andExpect(jsonPath("$.[*].orgResponseFailURL").value(hasItem(DEFAULT_ORG_RESPONSE_FAIL_URL)))
            .andExpect(jsonPath("$.[*].orgStatus").value(hasItem(DEFAULT_ORG_STATUS)))
            .andExpect(jsonPath("$.[*].requestDate").value(hasItem(DEFAULT_REQUEST_DATE.toString())))
            .andExpect(jsonPath("$.[*].organisationIdCode").value(hasItem(DEFAULT_ORGANISATION_ID_CODE)))
            .andExpect(jsonPath("$.[*].signerId").value(hasItem(DEFAULT_SIGNER_ID)))
            .andExpect(jsonPath("$.[*].requestType").value(hasItem(DEFAULT_REQUEST_TYPE)))
            .andExpect(jsonPath("$.[*].requestStatus").value(hasItem(DEFAULT_REQUEST_STATUS)))
            .andExpect(jsonPath("$.[*].responseType").value(hasItem(DEFAULT_RESPONSE_TYPE)))
            .andExpect(jsonPath("$.[*].responseStatus").value(hasItem(DEFAULT_RESPONSE_STATUS)))
            .andExpect(jsonPath("$.[*].responseCode").value(hasItem(DEFAULT_RESPONSE_CODE)))
            .andExpect(jsonPath("$.[*].requestXml").value(hasItem(DEFAULT_REQUEST_XML)))
            .andExpect(jsonPath("$.[*].responseXml").value(hasItem(DEFAULT_RESPONSE_XML)))
            .andExpect(jsonPath("$.[*].responseDate").value(hasItem(DEFAULT_RESPONSE_DATE.toString())))
            .andExpect(jsonPath("$.[*].ts").value(hasItem(DEFAULT_TS)));
    }
}
