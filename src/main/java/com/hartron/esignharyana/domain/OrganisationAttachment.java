package com.hartron.esignharyana.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

/**
 * A OrganisationAttachment.
 */
@Entity
@Table(name = "organisation_attachment")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "esign_organisationattachment")
public class OrganisationAttachment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "attachment")
    private String attachment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("organisationAttachments")
    private OrganisationMaster organisationMaster;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("organisationAttachments")
    private OrganisationDocument organisationDocument;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAttachment() {
        return attachment;
    }

    public OrganisationAttachment attachment(String attachment) {
        this.attachment = attachment;
        return this;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public OrganisationMaster getOrganisationMaster() {
        return organisationMaster;
    }

    public OrganisationAttachment organisationMaster(OrganisationMaster organisationMaster) {
        this.organisationMaster = organisationMaster;
        return this;
    }

    public void setOrganisationMaster(OrganisationMaster organisationMaster) {
        this.organisationMaster = organisationMaster;
    }

    public OrganisationDocument getOrganisationDocument() {
        return organisationDocument;
    }

    public OrganisationAttachment organisationDocument(OrganisationDocument organisationDocument) {
        this.organisationDocument = organisationDocument;
        return this;
    }

    public void setOrganisationDocument(OrganisationDocument organisationDocument) {
        this.organisationDocument = organisationDocument;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrganisationAttachment)) {
            return false;
        }
        return id != null && id.equals(((OrganisationAttachment) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "OrganisationAttachment{" +
            "id=" + getId() +
            ", attachment='" + getAttachment() + "'" +
            "}";
    }
}
