package com.hartron.esignharyana.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A OrganisationMaster.
 */
@Entity
@Table(name = "organisation_master")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "esign_organisationmaster")
public class OrganisationMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @NotNull
    @Column(name = "organisation_name", nullable = false)
    private String organisationName;

    @NotNull
    @Column(name = "organisation_abbreviation", nullable = false)
    private String organisationAbbreviation;

    @NotNull
    @Column(name = "organisation_correspondence_email", nullable = false)
    private String organisationCorrespondenceEmail;

    @NotNull
    @Column(name = "organisation_correspondence_phone_1", nullable = false)
    private String organisationCorrespondencePhone1;

    @Column(name = "organisation_correspondence_phone_2")
    private String organisationCorrespondencePhone2;

    @Column(name = "organisation_correspondence_address")
    private String organisationCorrespondenceAddress;

    @Column(name = "organisation_website")
    private String organisationWebsite;

    @Column(name = "organisation_nodal_officer_name")
    private String organisationNodalOfficerName;

    @Column(name = "organisation_nodal_officer_phone_mobile")
    private String organisationNodalOfficerPhoneMobile;

    @Column(name = "organisation_nodal_officer_phone_landline")
    private String organisationNodalOfficerPhoneLandline;

    @Column(name = "nodal_officer_official_email")
    private String nodalOfficerOfficialEmail;

    @Column(name = "organisation_nodal_officer_alternative_email")
    private String organisationNodalOfficerAlternativeEmail;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    private LocalDate createdOn;

    @Column(name = "last_updated_by")
    private String lastUpdatedBy;

    @Column(name = "last_updated_on")
    private LocalDate lastUpdatedOn;

    @Column(name = "verified_by")
    private String verifiedBy;

    @Column(name = "verified_on")
    private LocalDate verifiedOn;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "stage")
    private String stage;

    @Column(name = "status")
    private String status;

    @Column(name = "organisation_id_code")
    private String organisationIdCode;

    @Column(name = "organisation_employee_id")
    private String organisationEmployeeId;

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @Column(name = "gender")
    private String gender;

    @Column(name = "pan")
    private String pan;

    @Column(name = "aadhaar")
    private String aadhaar;

    @Column(name = "orgnisation_empolyee_card_attchment")
    private String orgnisationEmpolyeeCardAttchment;

    @Column(name = "pan_attachment")
    private String panAttachment;

    @Column(name = "photograph_attachment")
    private String photographAttachment;

    @Column(name = "aadhaar_attachment")
    private String aadhaarAttachment;

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    @Column(name = "pinCode")
    private String pinCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("organisationMasters")
    private OrganisationTypeMaster organisationTypeMaster;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("organisationMasters")
    private StateMaster stateMaster;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("organisationMasters")
    private DistrictMaster districtMaster;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("organisationMasters")
    private SubDistrictMaster subDistrictMaster;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("organisationMasters")
    private Department department;


    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrganisationName() {
        return organisationName;
    }

    public OrganisationMaster organisationName(String organisationName) {
        this.organisationName = organisationName;
        return this;
    }

    public void setOrganisationName(String organisationName) {
        this.organisationName = organisationName;
    }

    public String getOrganisationAbbreviation() {
        return organisationAbbreviation;
    }

    public OrganisationMaster organisationAbbreviation(String organisationAbbreviation) {
        this.organisationAbbreviation = organisationAbbreviation;
        return this;
    }

    public void setOrganisationAbbreviation(String organisationAbbreviation) {
        this.organisationAbbreviation = organisationAbbreviation;
    }

    public String getOrganisationCorrespondenceEmail() {
        return organisationCorrespondenceEmail;
    }

    public OrganisationMaster organisationCorrespondenceEmail(String organisationCorrespondenceEmail) {
        this.organisationCorrespondenceEmail = organisationCorrespondenceEmail;
        return this;
    }

    public void setOrganisationCorrespondenceEmail(String organisationCorrespondenceEmail) {
        this.organisationCorrespondenceEmail = organisationCorrespondenceEmail;
    }

    public String getOrganisationCorrespondencePhone1() {
        return organisationCorrespondencePhone1;
    }

    public OrganisationMaster organisationCorrespondencePhone1(String organisationCorrespondencePhone1) {
        this.organisationCorrespondencePhone1 = organisationCorrespondencePhone1;
        return this;
    }

    public void setOrganisationCorrespondencePhone1(String organisationCorrespondencePhone1) {
        this.organisationCorrespondencePhone1 = organisationCorrespondencePhone1;
    }

    public String getOrganisationCorrespondencePhone2() {
        return organisationCorrespondencePhone2;
    }

    public OrganisationMaster organisationCorrespondencePhone2(String organisationCorrespondencePhone2) {
        this.organisationCorrespondencePhone2 = organisationCorrespondencePhone2;
        return this;
    }

    public void setOrganisationCorrespondencePhone2(String organisationCorrespondencePhone2) {
        this.organisationCorrespondencePhone2 = organisationCorrespondencePhone2;
    }

    public String getOrganisationCorrespondenceAddress() {
        return organisationCorrespondenceAddress;
    }

    public OrganisationMaster organisationCorrespondenceAddress(String organisationCorrespondenceAddress) {
        this.organisationCorrespondenceAddress = organisationCorrespondenceAddress;
        return this;
    }

    public void setOrganisationCorrespondenceAddress(String organisationCorrespondenceAddress) {
        this.organisationCorrespondenceAddress = organisationCorrespondenceAddress;
    }

    public String getOrganisationWebsite() {
        return organisationWebsite;
    }

    public OrganisationMaster organisationWebsite(String organisationWebsite) {
        this.organisationWebsite = organisationWebsite;
        return this;
    }

    public void setOrganisationWebsite(String organisationWebsite) {
        this.organisationWebsite = organisationWebsite;
    }

    public String getOrganisationNodalOfficerName() {
        return organisationNodalOfficerName;
    }

    public OrganisationMaster organisationNodalOfficerName(String organisationNodalOfficerName) {
        this.organisationNodalOfficerName = organisationNodalOfficerName;
        return this;
    }

    public void setOrganisationNodalOfficerName(String organisationNodalOfficerName) {
        this.organisationNodalOfficerName = organisationNodalOfficerName;
    }

    public String getOrganisationNodalOfficerPhoneMobile() {
        return organisationNodalOfficerPhoneMobile;
    }

    public OrganisationMaster organisationNodalOfficerPhoneMobile(String organisationNodalOfficerPhoneMobile) {
        this.organisationNodalOfficerPhoneMobile = organisationNodalOfficerPhoneMobile;
        return this;
    }

    public void setOrganisationNodalOfficerPhoneMobile(String organisationNodalOfficerPhoneMobile) {
        this.organisationNodalOfficerPhoneMobile = organisationNodalOfficerPhoneMobile;
    }

    public String getOrganisationNodalOfficerPhoneLandline() {
        return organisationNodalOfficerPhoneLandline;
    }

    public OrganisationMaster organisationNodalOfficerPhoneLandline(String organisationNodalOfficerPhoneLandline) {
        this.organisationNodalOfficerPhoneLandline = organisationNodalOfficerPhoneLandline;
        return this;
    }

    public void setOrganisationNodalOfficerPhoneLandline(String organisationNodalOfficerPhoneLandline) {
        this.organisationNodalOfficerPhoneLandline = organisationNodalOfficerPhoneLandline;
    }

    public String getNodalOfficerOfficialEmail() {
        return nodalOfficerOfficialEmail;
    }

    public OrganisationMaster nodalOfficerOfficialEmail(String nodalOfficerOfficialEmail) {
        this.nodalOfficerOfficialEmail = nodalOfficerOfficialEmail;
        return this;
    }

    public void setNodalOfficerOfficialEmail(String nodalOfficerOfficialEmail) {
        this.nodalOfficerOfficialEmail = nodalOfficerOfficialEmail;
    }

    public String getOrganisationNodalOfficerAlternativeEmail() {
        return organisationNodalOfficerAlternativeEmail;
    }

    public OrganisationMaster organisationNodalOfficerAlternativeEmail(String organisationNodalOfficerAlternativeEmail) {
        this.organisationNodalOfficerAlternativeEmail = organisationNodalOfficerAlternativeEmail;
        return this;
    }

    public void setOrganisationNodalOfficerAlternativeEmail(String organisationNodalOfficerAlternativeEmail) {
        this.organisationNodalOfficerAlternativeEmail = organisationNodalOfficerAlternativeEmail;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public OrganisationMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public OrganisationMaster createdOn(LocalDate createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public OrganisationMaster lastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
        return this;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public LocalDate getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public OrganisationMaster lastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
        return this;
    }

    public void setLastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public OrganisationMaster verifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
        return this;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public LocalDate getVerifiedOn() {
        return verifiedOn;
    }

    public OrganisationMaster verifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
        return this;
    }

    public void setVerifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getRemarks() {
        return remarks;
    }

    public OrganisationMaster remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getUserId() {
        return userId;
    }

    public OrganisationMaster userId(Integer userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getStage() {
        return stage;
    }

    public OrganisationMaster stage(String stage) {
        this.stage = stage;
        return this;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getStatus() {
        return status;
    }

    public OrganisationMaster status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrganisationIdCode() {
        return organisationIdCode;
    }

    public OrganisationMaster organisationIdCode(String organisationIdCode) {
        this.organisationIdCode = organisationIdCode;
        return this;
    }

    public void setOrganisationIdCode(String organisationIdCode) {
        this.organisationIdCode = organisationIdCode;
    }

    public String getOrganisationEmployeeId() {
        return organisationEmployeeId;
    }

    public OrganisationMaster organisationEmployeeId(String organisationEmployeeId) {
        this.organisationEmployeeId = organisationEmployeeId;
        return this;
    }

    public void setOrganisationEmployeeId(String organisationEmployeeId) {
        this.organisationEmployeeId = organisationEmployeeId;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public OrganisationMaster dateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public OrganisationMaster gender(String gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPan() {
        return pan;
    }

    public OrganisationMaster pan(String pan) {
        this.pan = pan;
        return this;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getAadhaar() {
        return aadhaar;
    }

    public OrganisationMaster aadhaar(String aadhaar) {
        this.aadhaar = aadhaar;
        return this;
    }

    public void setAadhaar(String aadhaar) {
        this.aadhaar = aadhaar;
    }

    public String getOrgnisationEmpolyeeCardAttchment() {
        return orgnisationEmpolyeeCardAttchment;
    }

    public OrganisationMaster orgnisationEmpolyeeCardAttchment(String orgnisationEmpolyeeCardAttchment) {
        this.orgnisationEmpolyeeCardAttchment = orgnisationEmpolyeeCardAttchment;
        return this;
    }

    public void setOrgnisationEmpolyeeCardAttchment(String orgnisationEmpolyeeCardAttchment) {
        this.orgnisationEmpolyeeCardAttchment = orgnisationEmpolyeeCardAttchment;
    }

    public String getPanAttachment() {
        return panAttachment;
    }

    public OrganisationMaster panAttachment(String panAttachment) {
        this.panAttachment = panAttachment;
        return this;
    }

    public void setPanAttachment(String panAttachment) {
        this.panAttachment = panAttachment;
    }

    public String getPhotographAttachment() {
        return photographAttachment;
    }

    public OrganisationMaster photographAttachment(String photographAttachment) {
        this.photographAttachment = photographAttachment;
        return this;
    }

    public void setPhotographAttachment(String photographAttachment) {
        this.photographAttachment = photographAttachment;
    }

    public String getAadhaarAttachment() {
        return aadhaarAttachment;
    }

    public OrganisationMaster aadhaarAttachment(String aadhaarAttachment) {
        this.aadhaarAttachment = aadhaarAttachment;
        return this;
    }

    public void setAadhaarAttachment(String aadhaarAttachment) {
        this.aadhaarAttachment = aadhaarAttachment;
    }

    public OrganisationTypeMaster getOrganisationTypeMaster() {
        return organisationTypeMaster;
    }

    public OrganisationMaster organisationTypeMaster(OrganisationTypeMaster organisationTypeMaster) {
        this.organisationTypeMaster = organisationTypeMaster;
        return this;
    }

    public void setOrganisationTypeMaster(OrganisationTypeMaster organisationTypeMaster) {
        this.organisationTypeMaster = organisationTypeMaster;
    }

    public StateMaster getStateMaster() {
        return stateMaster;
    }

    public OrganisationMaster stateMaster(StateMaster stateMaster) {
        this.stateMaster = stateMaster;
        return this;
    }

    public void setStateMaster(StateMaster stateMaster) {
        this.stateMaster = stateMaster;
    }

    public DistrictMaster getDistrictMaster() {
        return districtMaster;
    }

    public OrganisationMaster districtMaster(DistrictMaster districtMaster) {
        this.districtMaster = districtMaster;
        return this;
    }

    public void setDistrictMaster(DistrictMaster districtMaster) {
        this.districtMaster = districtMaster;
    }

    public SubDistrictMaster getSubDistrictMaster() {
        return subDistrictMaster;
    }

    public OrganisationMaster subDistrictMaster(SubDistrictMaster subDistrictMaster) {
        this.subDistrictMaster = subDistrictMaster;
        return this;
    }

    public void setSubDistrictMaster(SubDistrictMaster subDistrictMaster) {
        this.subDistrictMaster = subDistrictMaster;
    }

    public Department getDepartment() {
        return department;
    }

    public OrganisationMaster department(Department department) {
        this.department = department;
        return this;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrganisationMaster)) {
            return false;
        }
        return id != null && id.equals(((OrganisationMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "OrganisationMaster{" +
            "id=" + getId() +
            ", organisationName='" + getOrganisationName() + "'" +
            ", organisationAbbreviation='" + getOrganisationAbbreviation() + "'" +
            ", organisationCorrespondenceEmail='" + getOrganisationCorrespondenceEmail() + "'" +
            ", organisationCorrespondencePhone1='" + getOrganisationCorrespondencePhone1() + "'" +
            ", organisationCorrespondencePhone2='" + getOrganisationCorrespondencePhone2() + "'" +
            ", organisationCorrespondenceAddress='" + getOrganisationCorrespondenceAddress() + "'" +
            ", organisationWebsite='" + getOrganisationWebsite() + "'" +
            ", organisationNodalOfficerName='" + getOrganisationNodalOfficerName() + "'" +
            ", organisationNodalOfficerPhoneMobile='" + getOrganisationNodalOfficerPhoneMobile() + "'" +
            ", organisationNodalOfficerPhoneLandline='" + getOrganisationNodalOfficerPhoneLandline() + "'" +
            ", nodalOfficerOfficialEmail='" + getNodalOfficerOfficialEmail() + "'" +
            ", organisationNodalOfficerAlternativeEmail='" + getOrganisationNodalOfficerAlternativeEmail() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastUpdatedBy='" + getLastUpdatedBy() + "'" +
            ", lastUpdatedOn='" + getLastUpdatedOn() + "'" +
            ", verifiedBy='" + getVerifiedBy() + "'" +
            ", verifiedOn='" + getVerifiedOn() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", userId=" + getUserId() +
            ", stage='" + getStage() + "'" +
            ", status='" + getStatus() + "'" +
            ", organisationIdCode='" + getOrganisationIdCode() + "'" +
            ", organisationEmployeeId='" + getOrganisationEmployeeId() + "'" +
            ", dateOfBirth='" + getDateOfBirth() + "'" +
            ", gender='" + getGender() + "'" +
            ", pan='" + getPan() + "'" +
            ", aadhaar='" + getAadhaar() + "'" +
            ", orgnisationEmpolyeeCardAttchment='" + getOrgnisationEmpolyeeCardAttchment() + "'" +
            ", panAttachment='" + getPanAttachment() + "'" +
            ", photographAttachment='" + getPhotographAttachment() + "'" +
            ", aadhaarAttachment='" + getAadhaarAttachment() + "'" +
            ", pinCode='" + getPinCode() + "'" +
            "}";
    }
}
