package com.hartron.esignharyana.domain;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A OrganisationLog.
 */
@Entity
@Table(name = "organisation_log")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "esign_organisationlog")
public class OrganisationLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "organisation_id")
    private Long organisationId;

    @Column(name = "created_by_login")
    private String createdByLogin;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "verify_by_name")
    private String verifyByName;

    @Column(name = "verify_date")
    private ZonedDateTime verifyDate;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "status")
    private String status;

    @Column(name = "verify_by_login")
    private String verifyByLogin;

    @Column(name = "created_by_name")
    private String createdByName;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrganisationId() {
        return organisationId;
    }

    public OrganisationLog organisationId(Long organisationId) {
        this.organisationId = organisationId;
        return this;
    }

    public void setOrganisationId(Long organisationId) {
        this.organisationId = organisationId;
    }

    public String getCreatedByLogin() {
        return createdByLogin;
    }

    public OrganisationLog createdByLogin(String createdByLogin) {
        this.createdByLogin = createdByLogin;
        return this;
    }

    public void setCreatedByLogin(String createdByLogin) {
        this.createdByLogin = createdByLogin;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public OrganisationLog createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getVerifyByName() {
        return verifyByName;
    }

    public OrganisationLog verifyByName(String verifyByName) {
        this.verifyByName = verifyByName;
        return this;
    }

    public void setVerifyByName(String verifyByName) {
        this.verifyByName = verifyByName;
    }

    public ZonedDateTime getVerifyDate() {
        return verifyDate;
    }

    public OrganisationLog verifyDate(ZonedDateTime verifyDate) {
        this.verifyDate = verifyDate;
        return this;
    }

    public void setVerifyDate(ZonedDateTime verifyDate) {
        this.verifyDate = verifyDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public OrganisationLog remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return status;
    }

    public OrganisationLog status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVerifyByLogin() {
        return verifyByLogin;
    }

    public OrganisationLog verifyByLogin(String verifyByLogin) {
        this.verifyByLogin = verifyByLogin;
        return this;
    }

    public void setVerifyByLogin(String verifyByLogin) {
        this.verifyByLogin = verifyByLogin;
    }

    public String getCreatedByName() {
        return createdByName;
    }

    public OrganisationLog createdByName(String createdByName) {
        this.createdByName = createdByName;
        return this;
    }

    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrganisationLog)) {
            return false;
        }
        return id != null && id.equals(((OrganisationLog) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "OrganisationLog{" +
            "id=" + getId() +
            ", organisationId=" + getOrganisationId() +
            ", createdByLogin='" + getCreatedByLogin() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", verifyByName='" + getVerifyByName() + "'" +
            ", verifyDate='" + getVerifyDate() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", status='" + getStatus() + "'" +
            ", verifyByLogin='" + getVerifyByLogin() + "'" +
            ", createdByName='" + getCreatedByName() + "'" +
            "}";
    }
}
