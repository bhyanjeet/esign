package com.hartron.esignharyana.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A BlockMaster.
 */
@Entity
@Table(name = "block_master")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "esign_blockmaster")
public class BlockMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "block_code")
    private String blockCode;

    @NotNull
    @Column(name = "block_name", nullable = false)
    private String blockName;

    @Column(name = "created_on")
    private LocalDate createdOn;

    @Column(name = "last_updated_by")
    private String lastUpdatedBy;

    @Column(name = "last_updated_on")
    private LocalDate lastUpdatedOn;

    @Column(name = "verified_by")
    private String verifiedBy;

    @Column(name = "verified_on")
    private LocalDate verifiedOn;

    @Column(name = "remarks")
    private String remarks;

    @ManyToOne(fetch = FetchType.LAZY)
    private StateMaster stateMaster;

    @ManyToOne(fetch = FetchType.LAZY)
    private DistrictMaster districtMaster;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBlockCode() {
        return blockCode;
    }

    public BlockMaster blockCode(String blockCode) {
        this.blockCode = blockCode;
        return this;
    }

    public void setBlockCode(String blockCode) {
        this.blockCode = blockCode;
    }

    public String getBlockName() {
        return blockName;
    }

    public BlockMaster blockName(String blockName) {
        this.blockName = blockName;
        return this;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public BlockMaster createdOn(LocalDate createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public BlockMaster lastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
        return this;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public LocalDate getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public BlockMaster lastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
        return this;
    }

    public void setLastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public BlockMaster verifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
        return this;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public LocalDate getVerifiedOn() {
        return verifiedOn;
    }

    public BlockMaster verifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
        return this;
    }

    public void setVerifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getRemarks() {
        return remarks;
    }

    public BlockMaster remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public StateMaster getStateMaster() {
        return stateMaster;
    }

    public BlockMaster stateMaster(StateMaster stateMaster) {
        this.stateMaster = stateMaster;
        return this;
    }

    public void setStateMaster(StateMaster stateMaster) {
        this.stateMaster = stateMaster;
    }

    public DistrictMaster getDistrictMaster() {
        return districtMaster;
    }

    public BlockMaster districtMaster(DistrictMaster districtMaster) {
        this.districtMaster = districtMaster;
        return this;
    }

    public void setDistrictMaster(DistrictMaster districtMaster) {
        this.districtMaster = districtMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BlockMaster)) {
            return false;
        }
        return id != null && id.equals(((BlockMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "BlockMaster{" +
            "id=" + getId() +
            ", blockCode='" + getBlockCode() + "'" +
            ", blockName='" + getBlockName() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastUpdatedBy='" + getLastUpdatedBy() + "'" +
            ", lastUpdatedOn='" + getLastUpdatedOn() + "'" +
            ", verifiedBy='" + getVerifiedBy() + "'" +
            ", verifiedOn='" + getVerifiedOn() + "'" +
            ", remarks='" + getRemarks() + "'" +
            "}";
    }
}
