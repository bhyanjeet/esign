package com.hartron.esignharyana.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A ApplicationMaster.
 */
@Entity
@Table(name = "application_master")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "applicationmaster")
public class ApplicationMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "application_name", nullable = false, unique = true)
    private String applicationName;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    private LocalDate createdOn;

    @Column(name = "last_updated_by")
    private String lastUpdatedBy;

    @Column(name = "last_updated_on")
    private LocalDate lastUpdatedOn;

    @Column(name = "verified_by")
    private String verifiedBy;

    @Column(name = "verified_on")
    private LocalDate verifiedOn;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "application_url")
    private String applicationURL;

    @Column(name = "application_status")
    private String applicationStatus;

    
    @Column(name = "application_id_code", unique = true)
    private String applicationIdCode;

    @Column(name = "status")
    private String status;

    @Column(name = "cert_public_key")
    private String certPublicKey;

    @ManyToOne
    @JsonIgnoreProperties("applicationMasters")
    private OrganisationMaster organisationMaster;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public ApplicationMaster applicationName(String applicationName) {
        this.applicationName = applicationName;
        return this;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public ApplicationMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public ApplicationMaster createdOn(LocalDate createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public ApplicationMaster lastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
        return this;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public LocalDate getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public ApplicationMaster lastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
        return this;
    }

    public void setLastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public ApplicationMaster verifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
        return this;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public LocalDate getVerifiedOn() {
        return verifiedOn;
    }

    public ApplicationMaster verifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
        return this;
    }

    public void setVerifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getRemarks() {
        return remarks;
    }

    public ApplicationMaster remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getApplicationURL() {
        return applicationURL;
    }

    public ApplicationMaster applicationURL(String applicationURL) {
        this.applicationURL = applicationURL;
        return this;
    }

    public void setApplicationURL(String applicationURL) {
        this.applicationURL = applicationURL;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public ApplicationMaster applicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
        return this;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public String getApplicationIdCode() {
        return applicationIdCode;
    }

    public ApplicationMaster applicationIdCode(String applicationIdCode) {
        this.applicationIdCode = applicationIdCode;
        return this;
    }

    public void setApplicationIdCode(String applicationIdCode) {
        this.applicationIdCode = applicationIdCode;
    }

    public String getStatus() {
        return status;
    }

    public ApplicationMaster status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCertPublicKey() {
        return certPublicKey;
    }

    public ApplicationMaster certPublicKey(String certPublicKey) {
        this.certPublicKey = certPublicKey;
        return this;
    }

    public void setCertPublicKey(String certPublicKey) {
        this.certPublicKey = certPublicKey;
    }

    public OrganisationMaster getOrganisationMaster() {
        return organisationMaster;
    }

    public ApplicationMaster organisationMaster(OrganisationMaster organisationMaster) {
        this.organisationMaster = organisationMaster;
        return this;
    }

    public void setOrganisationMaster(OrganisationMaster organisationMaster) {
        this.organisationMaster = organisationMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ApplicationMaster)) {
            return false;
        }
        return id != null && id.equals(((ApplicationMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ApplicationMaster{" +
            "id=" + getId() +
            ", applicationName='" + getApplicationName() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastUpdatedBy='" + getLastUpdatedBy() + "'" +
            ", lastUpdatedOn='" + getLastUpdatedOn() + "'" +
            ", verifiedBy='" + getVerifiedBy() + "'" +
            ", verifiedOn='" + getVerifiedOn() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", applicationURL='" + getApplicationURL() + "'" +
            ", applicationStatus='" + getApplicationStatus() + "'" +
            ", applicationIdCode='" + getApplicationIdCode() + "'" +
            ", status='" + getStatus() + "'" +
            ", certPublicKey='" + getCertPublicKey() + "'" +
            "}";
    }
}
