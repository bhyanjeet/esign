package com.hartron.esignharyana.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

/**
 * A UserAuthDetails.
 */
@Entity
@Table(name = "user_auth_details")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "userauthdetails")
public class UserAuthDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "login")
    private String login;

    @Column(name = "device")
    private String device;

    @Column(name = "auth_token")
    private String authToken;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public UserAuthDetails login(String login) {
        this.login = login;
        return this;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getDevice() {
        return device;
    }

    public UserAuthDetails device(String device) {
        this.device = device;
        return this;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getAuthToken() {
        return authToken;
    }

    public UserAuthDetails authToken(String authToken) {
        this.authToken = authToken;
        return this;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserAuthDetails)) {
            return false;
        }
        return id != null && id.equals(((UserAuthDetails) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserAuthDetails{" +
            "id=" + getId() +
            ", login='" + getLogin() + "'" +
            ", device='" + getDevice() + "'" +
            ", authToken='" + getAuthToken() + "'" +
            "}";
    }
}
