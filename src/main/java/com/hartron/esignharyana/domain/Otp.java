package com.hartron.esignharyana.domain;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A Otp.
 */
@Entity
@Table(name = "otp")
@Document(indexName = "esign_otp")
public class Otp implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "otp")
    private Integer otp;

    @Column(name = "mobile_number")
    private Long mobileNumber;

    @Column(name = "request_time")
    private ZonedDateTime requestTime;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getOtp() {
        return otp;
    }

    public Otp otp(Integer otp) {
        this.otp = otp;
        return this;
    }

    public void setOtp(Integer otp) {
        this.otp = otp;
    }

    public Long getMobileNumber() {
        return mobileNumber;
    }

    public Otp mobileNumber(Long mobileNumber) {
        this.mobileNumber = mobileNumber;
        return this;
    }

    public void setMobileNumber(Long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public ZonedDateTime getRequestTime() {
        return requestTime;
    }

    public Otp requestTime(ZonedDateTime requestTime) {
        this.requestTime = requestTime;
        return this;
    }

    public void setRequestTime(ZonedDateTime requestTime) {
        this.requestTime = requestTime;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Otp)) {
            return false;
        }
        return id != null && id.equals(((Otp) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Otp{" +
            "id=" + getId() +
            ", otp=" + getOtp() +
            ", mobileNumber=" + getMobileNumber() +
            ", requestTime='" + getRequestTime() + "'" +
            "}";
    }
}
