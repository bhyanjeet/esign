package com.hartron.esignharyana.domain;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A ApplicationLogs.
 */
@Entity
@Table(name = "application_logs")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "esign_applicationlogs")
public class ApplicationLogs implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "action_taken")
    private String actionTaken;

    @Column(name = "action_taken_by")
    private String actionTakenBy;

    @Column(name = "action_taken_on_application")
    private Long actionTakenOnApplication;

    @Column(name = "action_taken_on_date")
    private LocalDate actionTakenOnDate;

    @Column(name = "remarks")
    private String remarks;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActionTaken() {
        return actionTaken;
    }

    public ApplicationLogs actionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
        return this;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    public String getActionTakenBy() {
        return actionTakenBy;
    }

    public ApplicationLogs actionTakenBy(String actionTakenBy) {
        this.actionTakenBy = actionTakenBy;
        return this;
    }

    public void setActionTakenBy(String actionTakenBy) {
        this.actionTakenBy = actionTakenBy;
    }

    public Long getActionTakenOnApplication() {
        return actionTakenOnApplication;
    }

    public ApplicationLogs actionTakenOnApplication(Long actionTakenOnApplication) {
        this.actionTakenOnApplication = actionTakenOnApplication;
        return this;
    }

    public void setActionTakenOnApplication(Long actionTakenOnApplication) {
        this.actionTakenOnApplication = actionTakenOnApplication;
    }

    public LocalDate getActionTakenOnDate() {
        return actionTakenOnDate;
    }

    public ApplicationLogs actionTakenOnDate(LocalDate actionTakenOnDate) {
        this.actionTakenOnDate = actionTakenOnDate;
        return this;
    }

    public void setActionTakenOnDate(LocalDate actionTakenOnDate) {
        this.actionTakenOnDate = actionTakenOnDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public ApplicationLogs remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ApplicationLogs)) {
            return false;
        }
        return id != null && id.equals(((ApplicationLogs) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ApplicationLogs{" +
            "id=" + getId() +
            ", actionTaken='" + getActionTaken() + "'" +
            ", actionTakenBy='" + getActionTakenBy() + "'" +
            ", actionTakenOnApplication=" + getActionTakenOnApplication() +
            ", actionTakenOnDate='" + getActionTakenOnDate() + "'" +
            ", remarks='" + getRemarks() + "'" +
            "}";
    }
}
