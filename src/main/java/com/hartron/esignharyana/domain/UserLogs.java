package com.hartron.esignharyana.domain;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A UserLogs.
 */
@Entity
@Table(name = "user_logs")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "esign_userlogs")
public class UserLogs implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "action_taken")
    private String actionTaken;

    @Column(name = "action_taken_by")
    private String actionTakenBy;

    @Column(name = "action_taken_on_user")
    private Long actionTakenOnUser;

    @Column(name = "action_taken_on_date")
    private LocalDate actionTakenOnDate;

    @Column(name = "remarks")
    private String remarks;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActionTaken() {
        return actionTaken;
    }

    public UserLogs actionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
        return this;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    public String getActionTakenBy() {
        return actionTakenBy;
    }

    public UserLogs actionTakenBy(String actionTakenBy) {
        this.actionTakenBy = actionTakenBy;
        return this;
    }

    public void setActionTakenBy(String actionTakenBy) {
        this.actionTakenBy = actionTakenBy;
    }

    public Long getActionTakenOnUser() {
        return actionTakenOnUser;
    }

    public UserLogs actionTakenOnUser(Long actionTakenOnUser) {
        this.actionTakenOnUser = actionTakenOnUser;
        return this;
    }

    public void setActionTakenOnUser(Long actionTakenOnUser) {
        this.actionTakenOnUser = actionTakenOnUser;
    }

    public LocalDate getActionTakenOnDate() {
        return actionTakenOnDate;
    }

    public UserLogs actionTakenOnDate(LocalDate actionTakenOnDate) {
        this.actionTakenOnDate = actionTakenOnDate;
        return this;
    }

    public void setActionTakenOnDate(LocalDate actionTakenOnDate) {
        this.actionTakenOnDate = actionTakenOnDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public UserLogs remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserLogs)) {
            return false;
        }
        return id != null && id.equals(((UserLogs) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserLogs{" +
            "id=" + getId() +
            ", actionTaken='" + getActionTaken() + "'" +
            ", actionTakenBy='" + getActionTakenBy() + "'" +
            ", actionTakenOnUser=" + getActionTakenOnUser() +
            ", actionTakenOnDate='" + getActionTakenOnDate() + "'" +
            ", remarks='" + getRemarks() + "'" +
            "}";
    }
}
