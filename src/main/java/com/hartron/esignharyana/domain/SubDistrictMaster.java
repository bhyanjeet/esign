package com.hartron.esignharyana.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A SubDistrictMaster.
 */
@Entity
@Table(name = "sub_district_master")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "esign_subdistrictmaster")
public class SubDistrictMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "sub_district_code")
    private String subDistrictCode;

    @NotNull
    @Column(name = "sub_district_name", nullable = false)
    private String subDistrictName;

    @Column(name = "last_updated_by")
    private String lastUpdatedBy;

    @Column(name = "last_updated_on")
    private LocalDate lastUpdatedOn;

    @Column(name = "verified_by")
    private String verifiedBy;

    @Column(name = "verified_on")
    private LocalDate verifiedOn;

    @Column(name = "remarks")
    private String remarks;

    @ManyToOne(fetch = FetchType.LAZY)
    private StateMaster stateMaster;

    @ManyToOne(fetch = FetchType.LAZY)
    private DistrictMaster districtMaster;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubDistrictCode() {
        return subDistrictCode;
    }

    public SubDistrictMaster subDistrictCode(String subDistrictCode) {
        this.subDistrictCode = subDistrictCode;
        return this;
    }

    public void setSubDistrictCode(String subDistrictCode) {
        this.subDistrictCode = subDistrictCode;
    }

    public String getSubDistrictName() {
        return subDistrictName;
    }

    public SubDistrictMaster subDistrictName(String subDistrictName) {
        this.subDistrictName = subDistrictName;
        return this;
    }

    public void setSubDistrictName(String subDistrictName) {
        this.subDistrictName = subDistrictName;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public SubDistrictMaster lastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
        return this;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public LocalDate getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public SubDistrictMaster lastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
        return this;
    }

    public void setLastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public SubDistrictMaster verifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
        return this;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public LocalDate getVerifiedOn() {
        return verifiedOn;
    }

    public SubDistrictMaster verifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
        return this;
    }

    public void setVerifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getRemarks() {
        return remarks;
    }

    public SubDistrictMaster remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public StateMaster getStateMaster() {
        return stateMaster;
    }

    public SubDistrictMaster stateMaster(StateMaster stateMaster) {
        this.stateMaster = stateMaster;
        return this;
    }

    public void setStateMaster(StateMaster stateMaster) {
        this.stateMaster = stateMaster;
    }

    public DistrictMaster getDistrictMaster() {
        return districtMaster;
    }

    public SubDistrictMaster districtMaster(DistrictMaster districtMaster) {
        this.districtMaster = districtMaster;
        return this;
    }

    public void setDistrictMaster(DistrictMaster districtMaster) {
        this.districtMaster = districtMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SubDistrictMaster)) {
            return false;
        }
        return id != null && id.equals(((SubDistrictMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "SubDistrictMaster{" +
            "id=" + getId() +
            ", subDistrictCode='" + getSubDistrictCode() + "'" +
            ", subDistrictName='" + getSubDistrictName() + "'" +
            ", lastUpdatedBy='" + getLastUpdatedBy() + "'" +
            ", lastUpdatedOn='" + getLastUpdatedOn() + "'" +
            ", verifiedBy='" + getVerifiedBy() + "'" +
            ", verifiedOn='" + getVerifiedOn() + "'" +
            ", remarks='" + getRemarks() + "'" +
            "}";
    }
}
