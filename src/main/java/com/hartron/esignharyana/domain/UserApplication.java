package com.hartron.esignharyana.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A UserApplication.
 */
@Entity
@Table(name = "user_application")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "esign_userapplication")
public class UserApplication implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "user_login")
    private String userLogin;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    private LocalDate createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_on")
    private LocalDate updatedOn;

    @Column(name = "verified_by")
    private String verifiedBy;

    @Column(name = "verified_on")
    private LocalDate verifiedOn;

    @Column(name = "user_code_id")
    private String userCodeId;

    @Column(name = "application_code_id")
    private String applicationCodeId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("userApplications")
    private ApplicationMaster applicationMaster;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public UserApplication userId(Long userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public UserApplication userLogin(String userLogin) {
        this.userLogin = userLogin;
        return this;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public UserApplication createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public UserApplication createdOn(LocalDate createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public UserApplication updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDate getUpdatedOn() {
        return updatedOn;
    }

    public UserApplication updatedOn(LocalDate updatedOn) {
        this.updatedOn = updatedOn;
        return this;
    }

    public void setUpdatedOn(LocalDate updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public UserApplication verifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
        return this;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public LocalDate getVerifiedOn() {
        return verifiedOn;
    }

    public UserApplication verifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
        return this;
    }

    public void setVerifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getUserCodeId() {
        return userCodeId;
    }

    public UserApplication userCodeId(String userCodeId) {
        this.userCodeId = userCodeId;
        return this;
    }

    public void setUserCodeId(String userCodeId) {
        this.userCodeId = userCodeId;
    }

    public String getApplicationCodeId() {
        return applicationCodeId;
    }

    public UserApplication applicationCodeId(String applicationCodeId) {
        this.applicationCodeId = applicationCodeId;
        return this;
    }

    public void setApplicationCodeId(String applicationCodeId) {
        this.applicationCodeId = applicationCodeId;
    }

    public ApplicationMaster getApplicationMaster() {
        return applicationMaster;
    }

    public UserApplication applicationMaster(ApplicationMaster applicationMaster) {
        this.applicationMaster = applicationMaster;
        return this;
    }

    public void setApplicationMaster(ApplicationMaster applicationMaster) {
        this.applicationMaster = applicationMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserApplication)) {
            return false;
        }
        return id != null && id.equals(((UserApplication) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserApplication{" +
            "id=" + getId() +
            ", userId=" + getUserId() +
            ", userLogin='" + getUserLogin() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", updatedOn='" + getUpdatedOn() + "'" +
            ", verifiedBy='" + getVerifiedBy() + "'" +
            ", verifiedOn='" + getVerifiedOn() + "'" +
            ", userCodeId='" + getUserCodeId() + "'" +
            ", applicationCodeId='" + getApplicationCodeId() + "'" +
            "}";
    }
}
