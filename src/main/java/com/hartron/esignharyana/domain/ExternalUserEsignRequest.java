package com.hartron.esignharyana.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

/**
 * A ExternalUserEsignRequest.
 */
@Entity
@Table(name = "external_user_esign_request")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "externaluseresignrequest")
public class ExternalUserEsignRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_code_id")
    private String userCodeId;

    @Column(name = "application_id_code")
    private String applicationIdCode;

    @Column(name = "response_url")
    private String responseUrl;

    @Column(name = "redirect_url")
    private String redirectUrl;

    @Column(name = "ts")
    private String ts;

    @Column(name = "signer_id")
    private String signerId;

    @Column(name = "doc_info")
    private String docInfo;

    @Column(name = "doc_url")
    private String docUrl;

    @Column(name = "doc_hash")
    private String docHash;

    @Column(name = "request_status")
    private String requestStatus;

    @Column(name = "request_time")
    private String requestTime;

    @Column(name = "txn")
    private String txn;

    @Column(name = "txnRef")
    private String txnRef;

    public String getTxn() {
        return txn;
    }

    public void setTxn(String txn) {
        this.txn = txn;
    }

    public String getTxnRef() {
        return txnRef;
    }

    public void setTxnRef(String txnRef) {
        this.txnRef = txnRef;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserCodeId() {
        return userCodeId;
    }

    public ExternalUserEsignRequest userCodeId(String userCodeId) {
        this.userCodeId = userCodeId;
        return this;
    }

    public void setUserCodeId(String userCodeId) {
        this.userCodeId = userCodeId;
    }

    public String getApplicationIdCode() {
        return applicationIdCode;
    }

    public ExternalUserEsignRequest applicationIdCode(String applicationIdCode) {
        this.applicationIdCode = applicationIdCode;
        return this;
    }

    public void setApplicationIdCode(String applicationIdCode) {
        this.applicationIdCode = applicationIdCode;
    }

    public String getResponseUrl() {
        return responseUrl;
    }

    public ExternalUserEsignRequest responseUrl(String responseUrl) {
        this.responseUrl = responseUrl;
        return this;
    }

    public void setResponseUrl(String responseUrl) {
        this.responseUrl = responseUrl;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public ExternalUserEsignRequest redirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
        return this;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getTs() {
        return ts;
    }

    public ExternalUserEsignRequest ts(String ts) {
        this.ts = ts;
        return this;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getSignerId() {
        return signerId;
    }

    public ExternalUserEsignRequest signerId(String signerId) {
        this.signerId = signerId;
        return this;
    }

    public void setSignerId(String signerId) {
        this.signerId = signerId;
    }

    public String getDocInfo() {
        return docInfo;
    }

    public ExternalUserEsignRequest docInfo(String docInfo) {
        this.docInfo = docInfo;
        return this;
    }

    public void setDocInfo(String docInfo) {
        this.docInfo = docInfo;
    }

    public String getDocUrl() {
        return docUrl;
    }

    public ExternalUserEsignRequest docUrl(String docUrl) {
        this.docUrl = docUrl;
        return this;
    }

    public void setDocUrl(String docUrl) {
        this.docUrl = docUrl;
    }

    public String getDocHash() {
        return docHash;
    }

    public ExternalUserEsignRequest docHash(String docHash) {
        this.docHash = docHash;
        return this;
    }

    public void setDocHash(String docHash) {
        this.docHash = docHash;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public ExternalUserEsignRequest requestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
        return this;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public ExternalUserEsignRequest requestTime(String requestTime) {
        this.requestTime = requestTime;
        return this;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExternalUserEsignRequest)) {
            return false;
        }
        return id != null && id.equals(((ExternalUserEsignRequest) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ExternalUserEsignRequest{" +
            "id=" + getId() +
            ", userCodeId='" + getUserCodeId() + "'" +
            ", applicationIdCode='" + getApplicationIdCode() + "'" +
            ", responseUrl='" + getResponseUrl() + "'" +
            ", redirectUrl='" + getRedirectUrl() + "'" +
            ", ts='" + getTs() + "'" +
            ", signerId='" + getSignerId() + "'" +
            ", docInfo='" + getDocInfo() + "'" +
            ", docUrl='" + getDocUrl() + "'" +
            ", docHash='" + getDocHash() + "'" +
            ", requestStatus='" + getRequestStatus() + "'" +
            ", requestTime='" + getRequestTime() + "'" +
            ", txn='" + getTxn() + "'" +
            ", txnRef='" + getTxnRef() + "'" +
            "}";
    }
}
