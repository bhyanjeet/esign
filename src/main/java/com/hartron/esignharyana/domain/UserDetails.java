package com.hartron.esignharyana.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A UserDetails.
 */
@Entity
@Table(name = "user_details")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "userdetails")
public class UserDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_full_name")
    private String userFullName;

    @NotNull
    @Column(name = "user_phone_mobile", nullable = false)
    private String userPhoneMobile;

    @Column(name = "user_phone_landline")
    private String userPhoneLandline;

    @NotNull
    @Column(name = "user_official_email", nullable = false)
    private String userOfficialEmail;

    @Column(name = "user_alternative_email")
    private String userAlternativeEmail;

    @Column(name = "user_login_password")
    private String userLoginPassword;

    @Column(name = "user_login_password_salt")
    private String userLoginPasswordSalt;

    @Column(name = "user_transaction_password")
    private String userTransactionPassword;

    @Column(name = "user_transaction_password_salt")
    private String userTransactionPasswordSalt;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    private LocalDate createdOn;

    @Column(name = "last_updated_by")
    private String lastUpdatedBy;

    @Column(name = "last_updated_on")
    private LocalDate lastUpdatedOn;

    @Column(name = "verified_by")
    private String verifiedBy;

    @Column(name = "verified_on")
    private LocalDate verifiedOn;

    @Column(name = "remarks")
    private String remarks;

    
    @Column(name = "user_id_code", unique = true)
    private String userIdCode;

    @Column(name = "status")
    private String status;

    @Column(name = "username")
    private String username;

    @Column(name = "user_pin")
    private String userPIN;

    @Column(name = "organisation_unit")
    private String organisationUnit;

    @Column(name = "address")
    private String address;

    @Column(name = "counrty")
    private String counrty;

    @Column(name = "postal_code")
    private String postalCode;

    @Column(name = "photograph")
    private String photograph;

    @Column(name = "dob")
    private LocalDate dob;

    @Column(name = "gender")
    private String gender;

    @Column(name = "pan")
    private String pan;

    @Column(name = "aadhaar")
    private String aadhaar;

    @Column(name = "pan_attachment")
    private String panAttachment;

    @Column(name = "employee_id")
    private String employeeId;

    @Column(name = "employee_card_attachment")
    private String employeeCardAttachment;

    @Column(name = "signer_id")
    private String signerId;

    @Column(name = "last_action")
    private String lastAction;

    @Column(name = "account_status")
    private String accountStatus;

    @ManyToOne
    @JsonIgnoreProperties("userDetails")
    private DesignationMaster designationMaster;

    @ManyToOne
    @JsonIgnoreProperties("userDetails")
    private OrganisationMaster organisationMaster;

    @ManyToOne
    @JsonIgnoreProperties("userDetails")
    private EsignRole esignRole;

    @ManyToOne
    @JsonIgnoreProperties("userDetails")
    private ApplicationMaster applicationMaster;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public UserDetails userFullName(String userFullName) {
        this.userFullName = userFullName;
        return this;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getUserPhoneMobile() {
        return userPhoneMobile;
    }

    public UserDetails userPhoneMobile(String userPhoneMobile) {
        this.userPhoneMobile = userPhoneMobile;
        return this;
    }

    public void setUserPhoneMobile(String userPhoneMobile) {
        this.userPhoneMobile = userPhoneMobile;
    }

    public String getUserPhoneLandline() {
        return userPhoneLandline;
    }

    public UserDetails userPhoneLandline(String userPhoneLandline) {
        this.userPhoneLandline = userPhoneLandline;
        return this;
    }

    public void setUserPhoneLandline(String userPhoneLandline) {
        this.userPhoneLandline = userPhoneLandline;
    }

    public String getUserOfficialEmail() {
        return userOfficialEmail;
    }

    public UserDetails userOfficialEmail(String userOfficialEmail) {
        this.userOfficialEmail = userOfficialEmail;
        return this;
    }

    public void setUserOfficialEmail(String userOfficialEmail) {
        this.userOfficialEmail = userOfficialEmail;
    }

    public String getUserAlternativeEmail() {
        return userAlternativeEmail;
    }

    public UserDetails userAlternativeEmail(String userAlternativeEmail) {
        this.userAlternativeEmail = userAlternativeEmail;
        return this;
    }

    public void setUserAlternativeEmail(String userAlternativeEmail) {
        this.userAlternativeEmail = userAlternativeEmail;
    }

    public String getUserLoginPassword() {
        return userLoginPassword;
    }

    public UserDetails userLoginPassword(String userLoginPassword) {
        this.userLoginPassword = userLoginPassword;
        return this;
    }

    public void setUserLoginPassword(String userLoginPassword) {
        this.userLoginPassword = userLoginPassword;
    }

    public String getUserLoginPasswordSalt() {
        return userLoginPasswordSalt;
    }

    public UserDetails userLoginPasswordSalt(String userLoginPasswordSalt) {
        this.userLoginPasswordSalt = userLoginPasswordSalt;
        return this;
    }

    public void setUserLoginPasswordSalt(String userLoginPasswordSalt) {
        this.userLoginPasswordSalt = userLoginPasswordSalt;
    }

    public String getUserTransactionPassword() {
        return userTransactionPassword;
    }

    public UserDetails userTransactionPassword(String userTransactionPassword) {
        this.userTransactionPassword = userTransactionPassword;
        return this;
    }

    public void setUserTransactionPassword(String userTransactionPassword) {
        this.userTransactionPassword = userTransactionPassword;
    }

    public String getUserTransactionPasswordSalt() {
        return userTransactionPasswordSalt;
    }

    public UserDetails userTransactionPasswordSalt(String userTransactionPasswordSalt) {
        this.userTransactionPasswordSalt = userTransactionPasswordSalt;
        return this;
    }

    public void setUserTransactionPasswordSalt(String userTransactionPasswordSalt) {
        this.userTransactionPasswordSalt = userTransactionPasswordSalt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public UserDetails createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public UserDetails createdOn(LocalDate createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public UserDetails lastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
        return this;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public LocalDate getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public UserDetails lastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
        return this;
    }

    public void setLastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public UserDetails verifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
        return this;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public LocalDate getVerifiedOn() {
        return verifiedOn;
    }

    public UserDetails verifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
        return this;
    }

    public void setVerifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getRemarks() {
        return remarks;
    }

    public UserDetails remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUserIdCode() {
        return userIdCode;
    }

    public UserDetails userIdCode(String userIdCode) {
        this.userIdCode = userIdCode;
        return this;
    }

    public void setUserIdCode(String userIdCode) {
        this.userIdCode = userIdCode;
    }

    public String getStatus() {
        return status;
    }

    public UserDetails status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public UserDetails username(String username) {
        this.username = username;
        return this;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserPIN() {
        return userPIN;
    }

    public UserDetails userPIN(String userPIN) {
        this.userPIN = userPIN;
        return this;
    }

    public void setUserPIN(String userPIN) {
        this.userPIN = userPIN;
    }

    public String getOrganisationUnit() {
        return organisationUnit;
    }

    public UserDetails organisationUnit(String organisationUnit) {
        this.organisationUnit = organisationUnit;
        return this;
    }

    public void setOrganisationUnit(String organisationUnit) {
        this.organisationUnit = organisationUnit;
    }

    public String getAddress() {
        return address;
    }

    public UserDetails address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCounrty() {
        return counrty;
    }

    public UserDetails counrty(String counrty) {
        this.counrty = counrty;
        return this;
    }

    public void setCounrty(String counrty) {
        this.counrty = counrty;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public UserDetails postalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhotograph() {
        return photograph;
    }

    public UserDetails photograph(String photograph) {
        this.photograph = photograph;
        return this;
    }

    public void setPhotograph(String photograph) {
        this.photograph = photograph;
    }

    public LocalDate getDob() {
        return dob;
    }

    public UserDetails dob(LocalDate dob) {
        this.dob = dob;
        return this;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public UserDetails gender(String gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPan() {
        return pan;
    }

    public UserDetails pan(String pan) {
        this.pan = pan;
        return this;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getAadhaar() {
        return aadhaar;
    }

    public UserDetails aadhaar(String aadhaar) {
        this.aadhaar = aadhaar;
        return this;
    }

    public void setAadhaar(String aadhaar) {
        this.aadhaar = aadhaar;
    }

    public String getPanAttachment() {
        return panAttachment;
    }

    public UserDetails panAttachment(String panAttachment) {
        this.panAttachment = panAttachment;
        return this;
    }

    public void setPanAttachment(String panAttachment) {
        this.panAttachment = panAttachment;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public UserDetails employeeId(String employeeId) {
        this.employeeId = employeeId;
        return this;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeCardAttachment() {
        return employeeCardAttachment;
    }

    public UserDetails employeeCardAttachment(String employeeCardAttachment) {
        this.employeeCardAttachment = employeeCardAttachment;
        return this;
    }

    public void setEmployeeCardAttachment(String employeeCardAttachment) {
        this.employeeCardAttachment = employeeCardAttachment;
    }

    public String getSignerId() {
        return signerId;
    }

    public UserDetails signerId(String signerId) {
        this.signerId = signerId;
        return this;
    }

    public void setSignerId(String signerId) {
        this.signerId = signerId;
    }

    public String getLastAction() {
        return lastAction;
    }

    public UserDetails lastAction(String lastAction) {
        this.lastAction = lastAction;
        return this;
    }

    public void setLastAction(String lastAction) {
        this.lastAction = lastAction;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public UserDetails accountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
        return this;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public DesignationMaster getDesignationMaster() {
        return designationMaster;
    }

    public UserDetails designationMaster(DesignationMaster designationMaster) {
        this.designationMaster = designationMaster;
        return this;
    }

    public void setDesignationMaster(DesignationMaster designationMaster) {
        this.designationMaster = designationMaster;
    }

    public OrganisationMaster getOrganisationMaster() {
        return organisationMaster;
    }

    public UserDetails organisationMaster(OrganisationMaster organisationMaster) {
        this.organisationMaster = organisationMaster;
        return this;
    }

    public void setOrganisationMaster(OrganisationMaster organisationMaster) {
        this.organisationMaster = organisationMaster;
    }

    public EsignRole getEsignRole() {
        return esignRole;
    }

    public UserDetails esignRole(EsignRole esignRole) {
        this.esignRole = esignRole;
        return this;
    }

    public void setEsignRole(EsignRole esignRole) {
        this.esignRole = esignRole;
    }

    public ApplicationMaster getApplicationMaster() {
        return applicationMaster;
    }

    public UserDetails applicationMaster(ApplicationMaster applicationMaster) {
        this.applicationMaster = applicationMaster;
        return this;
    }

    public void setApplicationMaster(ApplicationMaster applicationMaster) {
        this.applicationMaster = applicationMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserDetails)) {
            return false;
        }
        return id != null && id.equals(((UserDetails) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserDetails{" +
            "id=" + getId() +
            ", userFullName='" + getUserFullName() + "'" +
            ", userPhoneMobile='" + getUserPhoneMobile() + "'" +
            ", userPhoneLandline='" + getUserPhoneLandline() + "'" +
            ", userOfficialEmail='" + getUserOfficialEmail() + "'" +
            ", userAlternativeEmail='" + getUserAlternativeEmail() + "'" +
            ", userLoginPassword='" + getUserLoginPassword() + "'" +
            ", userLoginPasswordSalt='" + getUserLoginPasswordSalt() + "'" +
            ", userTransactionPassword='" + getUserTransactionPassword() + "'" +
            ", userTransactionPasswordSalt='" + getUserTransactionPasswordSalt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastUpdatedBy='" + getLastUpdatedBy() + "'" +
            ", lastUpdatedOn='" + getLastUpdatedOn() + "'" +
            ", verifiedBy='" + getVerifiedBy() + "'" +
            ", verifiedOn='" + getVerifiedOn() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", userIdCode='" + getUserIdCode() + "'" +
            ", status='" + getStatus() + "'" +
            ", username='" + getUsername() + "'" +
            ", userPIN='" + getUserPIN() + "'" +
            ", organisationUnit='" + getOrganisationUnit() + "'" +
            ", address='" + getAddress() + "'" +
            ", counrty='" + getCounrty() + "'" +
            ", postalCode='" + getPostalCode() + "'" +
            ", photograph='" + getPhotograph() + "'" +
            ", dob='" + getDob() + "'" +
            ", gender='" + getGender() + "'" +
            ", pan='" + getPan() + "'" +
            ", aadhaar='" + getAadhaar() + "'" +
            ", panAttachment='" + getPanAttachment() + "'" +
            ", employeeId='" + getEmployeeId() + "'" +
            ", employeeCardAttachment='" + getEmployeeCardAttachment() + "'" +
            ", signerId='" + getSignerId() + "'" +
            ", lastAction='" + getLastAction() + "'" +
            ", accountStatus='" + getAccountStatus() + "'" +
            "}";
    }
}
