package com.hartron.esignharyana.domain;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A EsignTransation.
 */
@Entity
@Table(name = "esign_transation")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "esigntransation")
public class EsignTransation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "txn_id")
    private String txnId;

    @Column(name = "user_code_id")
    private String userCodeId;

    @Column(name = "application_code_id")
    private String applicationCodeId;

    @Column(name = "org_response_success_url")
    private String orgResponseSuccessURL;

    @Column(name = "org_response_fail_url")
    private String orgResponseFailURL;

    @Column(name = "org_status")
    private String orgStatus;

    @Column(name = "request_date")
    private LocalDate requestDate;

    @Column(name = "organisation_id_code")
    private String organisationIdCode;

    @Column(name = "signer_id")
    private String signerId;

    @Column(name = "request_type")
    private String requestType;

    @Column(name = "request_status")
    private String requestStatus;

    @Column(name = "response_type")
    private String responseType;

    @Column(name = "response_status")
    private String responseStatus;

    @Column(name = "response_code")
    private String responseCode;

    @Column(name = "request_xml")
    private String requestXml;

    @Column(name = "response_xml")
    private String responseXml;

    @Column(name = "response_date")
    private LocalDate responseDate;

    @Column(name = "ts")
    private String ts;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTxnId() {
        return txnId;
    }

    public EsignTransation txnId(String txnId) {
        this.txnId = txnId;
        return this;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getUserCodeId() {
        return userCodeId;
    }

    public EsignTransation userCodeId(String userCodeId) {
        this.userCodeId = userCodeId;
        return this;
    }

    public void setUserCodeId(String userCodeId) {
        this.userCodeId = userCodeId;
    }

    public String getApplicationCodeId() {
        return applicationCodeId;
    }

    public EsignTransation applicationCodeId(String applicationCodeId) {
        this.applicationCodeId = applicationCodeId;
        return this;
    }

    public void setApplicationCodeId(String applicationCodeId) {
        this.applicationCodeId = applicationCodeId;
    }

    public String getOrgResponseSuccessURL() {
        return orgResponseSuccessURL;
    }

    public EsignTransation orgResponseSuccessURL(String orgResponseSuccessURL) {
        this.orgResponseSuccessURL = orgResponseSuccessURL;
        return this;
    }

    public void setOrgResponseSuccessURL(String orgResponseSuccessURL) {
        this.orgResponseSuccessURL = orgResponseSuccessURL;
    }

    public String getOrgResponseFailURL() {
        return orgResponseFailURL;
    }

    public EsignTransation orgResponseFailURL(String orgResponseFailURL) {
        this.orgResponseFailURL = orgResponseFailURL;
        return this;
    }

    public void setOrgResponseFailURL(String orgResponseFailURL) {
        this.orgResponseFailURL = orgResponseFailURL;
    }

    public String getOrgStatus() {
        return orgStatus;
    }

    public EsignTransation orgStatus(String orgStatus) {
        this.orgStatus = orgStatus;
        return this;
    }

    public void setOrgStatus(String orgStatus) {
        this.orgStatus = orgStatus;
    }

    public LocalDate getRequestDate() {
        return requestDate;
    }

    public EsignTransation requestDate(LocalDate requestDate) {
        this.requestDate = requestDate;
        return this;
    }

    public void setRequestDate(LocalDate requestDate) {
        this.requestDate = requestDate;
    }

    public String getOrganisationIdCode() {
        return organisationIdCode;
    }

    public EsignTransation organisationIdCode(String organisationIdCode) {
        this.organisationIdCode = organisationIdCode;
        return this;
    }

    public void setOrganisationIdCode(String organisationIdCode) {
        this.organisationIdCode = organisationIdCode;
    }

    public String getSignerId() {
        return signerId;
    }

    public EsignTransation signerId(String signerId) {
        this.signerId = signerId;
        return this;
    }

    public void setSignerId(String signerId) {
        this.signerId = signerId;
    }

    public String getRequestType() {
        return requestType;
    }

    public EsignTransation requestType(String requestType) {
        this.requestType = requestType;
        return this;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public EsignTransation requestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
        return this;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getResponseType() {
        return responseType;
    }

    public EsignTransation responseType(String responseType) {
        this.responseType = responseType;
        return this;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public EsignTransation responseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
        return this;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public EsignTransation responseCode(String responseCode) {
        this.responseCode = responseCode;
        return this;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getRequestXml() {
        return requestXml;
    }

    public EsignTransation requestXml(String requestXml) {
        this.requestXml = requestXml;
        return this;
    }

    public void setRequestXml(String requestXml) {
        this.requestXml = requestXml;
    }

    public String getResponseXml() {
        return responseXml;
    }

    public EsignTransation responseXml(String responseXml) {
        this.responseXml = responseXml;
        return this;
    }

    public void setResponseXml(String responseXml) {
        this.responseXml = responseXml;
    }

    public LocalDate getResponseDate() {
        return responseDate;
    }

    public EsignTransation responseDate(LocalDate responseDate) {
        this.responseDate = responseDate;
        return this;
    }

    public void setResponseDate(LocalDate responseDate) {
        this.responseDate = responseDate;
    }

    public String getTs() {
        return ts;
    }

    public EsignTransation ts(String ts) {
        this.ts = ts;
        return this;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EsignTransation)) {
            return false;
        }
        return id != null && id.equals(((EsignTransation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "EsignTransation{" +
            "id=" + getId() +
            ", txnId='" + getTxnId() + "'" +
            ", userCodeId='" + getUserCodeId() + "'" +
            ", applicationCodeId='" + getApplicationCodeId() + "'" +
            ", orgResponseSuccessURL='" + getOrgResponseSuccessURL() + "'" +
            ", orgResponseFailURL='" + getOrgResponseFailURL() + "'" +
            ", orgStatus='" + getOrgStatus() + "'" +
            ", requestDate='" + getRequestDate() + "'" +
            ", organisationIdCode='" + getOrganisationIdCode() + "'" +
            ", signerId='" + getSignerId() + "'" +
            ", requestType='" + getRequestType() + "'" +
            ", requestStatus='" + getRequestStatus() + "'" +
            ", responseType='" + getResponseType() + "'" +
            ", responseStatus='" + getResponseStatus() + "'" +
            ", responseCode='" + getResponseCode() + "'" +
            ", requestXml='" + getRequestXml() + "'" +
            ", responseXml='" + getResponseXml() + "'" +
            ", responseDate='" + getResponseDate() + "'" +
            ", ts='" + getTs() + "'" +
            "}";
    }
}
