package com.hartron.esignharyana.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A OrganisationTypeMaster.
 */
@Entity
@Table(name = "organisation_type_master")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "esign_organisationtypemaster")
public class OrganisationTypeMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @NotNull
    @Column(name = "organisation_type_detail", nullable = false)
    private String organisationTypeDetail;

    @NotNull
    @Column(name = "organisation_type_abbreviation", nullable = false)
    private String organisationTypeAbbreviation;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    private LocalDate createdOn;

    @Column(name = "last_updated_by")
    private String lastUpdatedBy;

    @Column(name = "last_updated_on")
    private LocalDate lastUpdatedOn;

    @Column(name = "verified_by")
    private String verifiedBy;

    @Column(name = "verified_on")
    private LocalDate verifiedOn;

    @Column(name = "remarks")
    private String remarks;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrganisationTypeDetail() {
        return organisationTypeDetail;
    }

    public OrganisationTypeMaster organisationTypeDetail(String organisationTypeDetail) {
        this.organisationTypeDetail = organisationTypeDetail;
        return this;
    }

    public void setOrganisationTypeDetail(String organisationTypeDetail) {
        this.organisationTypeDetail = organisationTypeDetail;
    }

    public String getOrganisationTypeAbbreviation() {
        return organisationTypeAbbreviation;
    }

    public OrganisationTypeMaster organisationTypeAbbreviation(String organisationTypeAbbreviation) {
        this.organisationTypeAbbreviation = organisationTypeAbbreviation;
        return this;
    }

    public void setOrganisationTypeAbbreviation(String organisationTypeAbbreviation) {
        this.organisationTypeAbbreviation = organisationTypeAbbreviation;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public OrganisationTypeMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public OrganisationTypeMaster createdOn(LocalDate createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public OrganisationTypeMaster lastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
        return this;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public LocalDate getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public OrganisationTypeMaster lastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
        return this;
    }

    public void setLastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public OrganisationTypeMaster verifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
        return this;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public LocalDate getVerifiedOn() {
        return verifiedOn;
    }

    public OrganisationTypeMaster verifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
        return this;
    }

    public void setVerifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getRemarks() {
        return remarks;
    }

    public OrganisationTypeMaster remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrganisationTypeMaster)) {
            return false;
        }
        return id != null && id.equals(((OrganisationTypeMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "OrganisationTypeMaster{" +
            "id=" + getId() +
            ", organisationTypeDetail='" + getOrganisationTypeDetail() + "'" +
            ", organisationTypeAbbreviation='" + getOrganisationTypeAbbreviation() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastUpdatedBy='" + getLastUpdatedBy() + "'" +
            ", lastUpdatedOn='" + getLastUpdatedOn() + "'" +
            ", verifiedBy='" + getVerifiedBy() + "'" +
            ", verifiedOn='" + getVerifiedOn() + "'" +
            ", remarks='" + getRemarks() + "'" +
            "}";
    }
}
