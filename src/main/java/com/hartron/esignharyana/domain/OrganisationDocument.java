package com.hartron.esignharyana.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * A OrganisationDocument.
 */
@Entity
@Table(name = "organisation_document")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "esign_organisationdocument")
public class OrganisationDocument implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @NotNull
    @Column(name = "document_title", nullable = false)
    private String documentTitle;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    private LocalDate createdOn;

    @Column(name = "verified_by")
    private String verifiedBy;

    @Column(name = "verified_on")
    private LocalDate verifiedOn;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "document_related_to")
    private String documentRelatedTo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("organisationDocuments")
    private OrganisationTypeMaster organisationTypeMaster;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocumentTitle() {
        return documentTitle;
    }

    public OrganisationDocument documentTitle(String documentTitle) {
        this.documentTitle = documentTitle;
        return this;
    }

    public void setDocumentTitle(String documentTitle) {
        this.documentTitle = documentTitle;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public OrganisationDocument createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public OrganisationDocument createdOn(LocalDate createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public OrganisationDocument verifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
        return this;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public LocalDate getVerifiedOn() {
        return verifiedOn;
    }

    public OrganisationDocument verifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
        return this;
    }

    public void setVerifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getRemarks() {
        return remarks;
    }

    public OrganisationDocument remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDocumentRelatedTo() {
        return documentRelatedTo;
    }

    public OrganisationDocument documentRelatedTo(String documentRelatedTo) {
        this.documentRelatedTo = documentRelatedTo;
        return this;
    }

    public void setDocumentRelatedTo(String documentRelatedTo) {
        this.documentRelatedTo = documentRelatedTo;
    }

    public OrganisationTypeMaster getOrganisationTypeMaster() {
        return organisationTypeMaster;
    }

    public OrganisationDocument organisationTypeMaster(OrganisationTypeMaster organisationTypeMaster) {
        this.organisationTypeMaster = organisationTypeMaster;
        return this;
    }

    public void setOrganisationTypeMaster(OrganisationTypeMaster organisationTypeMaster) {
        this.organisationTypeMaster = organisationTypeMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrganisationDocument)) {
            return false;
        }
        return id != null && id.equals(((OrganisationDocument) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "OrganisationDocument{" +
            "id=" + getId() +
            ", documentTitle='" + getDocumentTitle() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", verifiedBy='" + getVerifiedBy() + "'" +
            ", verifiedOn='" + getVerifiedOn() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", documentRelatedTo='" + getDocumentRelatedTo() + "'" +
            "}";
    }
}
