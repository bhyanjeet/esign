package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.*;
import com.hartron.esignharyana.service.dto.EsignRequestDTO;
import com.itextpdf.text.DocumentException;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OperatorCreationException;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.*;
import java.security.cert.CertificateException;

//import static jdk.nashorn.internal.parser.TokenType.XML;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.ApplicationMaster}.
 */
@RestController
@RequestMapping("/api")
public class eSignResource {

    private final Logger log = LoggerFactory.getLogger(eSignResource.class);

    private static final String ENTITY_NAME = "applicationMaster";

    private final UserApplicationService userApplicationService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ApplicationMasterService applicationMasterService;

    private final UserDetailsService userDetailsService;

    private final eSignService eSignService;

    private final EsignResponseService esignResponseService;

    public eSignResource(UserApplicationService userApplicationService, ApplicationMasterService applicationMasterService, UserDetailsService userDetailsService, eSignService createeSignXMLgnService, com.hartron.esignharyana.service.eSignService eSignService, EsignResponseService esignResponseService) {
        this.userApplicationService = userApplicationService;
        this.applicationMasterService = applicationMasterService;
        this.userDetailsService = userDetailsService;
        this.eSignService = eSignService;

        this.esignResponseService = esignResponseService;
    }

    @PostMapping(value = "eSign")
    public EsignRequestDTO createeSignXML(@Valid @RequestBody EsignRequestDTO esignRequestDTO) throws URISyntaxException, IOException, CertificateException, NoSuchAlgorithmException, ParserConfigurationException, XMLSignatureException, InvalidAlgorithmParameterException, SAXException, UnrecoverableEntryException, MarshalException, KeyStoreException, TransformerException, DocumentException, SignatureException, InvalidKeyException, CMSException, OperatorCreationException, NoSuchProviderException {
   // public Boolean createeSignXML() throws TransformerException, ParserConfigurationException, IOException, CertificateException, NoSuchAlgorithmException, XMLSignatureException, UnrecoverableEntryException, InvalidAlgorithmParameterException, SAXException, MarshalException, KeyStoreException {
//        EsignRequestDTO esignRequestDTO = new EsignRequestDTO();
        System.out.println("dtooooooooooooooooooo" + esignRequestDTO);
        esignRequestDTO  =  eSignService.createeSignXML(esignRequestDTO);
//        response.sendRedirect("http://117.255.216.162:8097/esign-request-form/CSORG1000002/117.255.216.162/"+ esignRequestDTO.getTxnref() +"/"+esignRequestDTO.getReq_data() +"/request");
        return esignRequestDTO;
    }

    @PostMapping(value = "sign/response")
    public String signResponse(@Valid @RequestBody String esignResponse, HttpServletResponse response) throws URISyntaxException, IOException, CertificateException, NoSuchAlgorithmException, ParserConfigurationException, XMLSignatureException, InvalidAlgorithmParameterException, SAXException, UnrecoverableEntryException, MarshalException, KeyStoreException, TransformerException, OperatorCreationException, NoSuchProviderException, SignatureException, DocumentException, InvalidKeyException, CMSException, JSONException {
        // public Boolean createeSignXML() throws TransformerException, ParserConfigurationException, IOException, CertificateException, NoSuchAlgorithmException, XMLSignatureException, UnrecoverableEntryException, InvalidAlgorithmParameterException, SAXException, MarshalException, KeyStoreException {
//        EsignRequestDTO esignRequestDTO = new EsignRequestDTO();

        String esignResonse = esignResponseService.saveEsignResponse(esignResponse, response);

        System.out.println("signResponsssssssssssssss" + esignResponse.toString());

        return esignResponse.toString();
    }

    @PostMapping(value = "sign/redirect")
    public String signRedirect(@Valid @RequestBody String esignRedirect, HttpServletResponse response) throws URISyntaxException, IOException, CertificateException, NoSuchAlgorithmException, ParserConfigurationException, XMLSignatureException, InvalidAlgorithmParameterException, SAXException, UnrecoverableEntryException, MarshalException, KeyStoreException, TransformerException, JSONException, DocumentException, InterruptedException {
        // public Boolean createeSignXML() throws TransformerException, ParserConfigurationException, IOException, CertificateException, NoSuchAlgorithmException, XMLSignatureException, UnrecoverableEntryException, InvalidAlgorithmParameterException, SAXException, MarshalException, KeyStoreException {
//        EsignRequestDTO esignRequestDTO = new EsignRequestDTO();
        System.out.println("signRedireccccct" + esignRedirect.toString());

        String esignResonse = esignResponseService.saveEsignRedirect(esignRedirect, response);

       // response.sendRedirect("http://localhost:9001/esign-response-data");

        return esignRedirect.toString();
    }

    @PostMapping(value = "checkEsignStatus")
    public String checkStatus(String EsignXml) throws URISyntaxException, IOException, CertificateException, NoSuchAlgorithmException, ParserConfigurationException, XMLSignatureException, InvalidAlgorithmParameterException, SAXException, UnrecoverableEntryException, MarshalException, KeyStoreException, TransformerException, JSONException {
        // public Boolean createeSignXML() throws TransformerException, ParserConfigurationException, IOException, CertificateException, NoSuchAlgorithmException, XMLSignatureException, UnrecoverableEntryException, InvalidAlgorithmParameterException, SAXException, MarshalException, KeyStoreException {
//        EsignRequestDTO esignRequestDTO = new EsignRequestDTO();


        String checkEsignStatus = esignResponseService.sendEsignStatus(EsignXml);

       // httpHeaders.setLocation(new URI("http://localhost:9001/admin/docs").getHost());

        return checkEsignStatus;
    }

    @PostMapping(value = "sign/status")
    public String signStatus(String userCodeId) throws URISyntaxException, IOException, CertificateException, NoSuchAlgorithmException, ParserConfigurationException, XMLSignatureException, InvalidAlgorithmParameterException, SAXException, UnrecoverableEntryException, MarshalException, KeyStoreException, TransformerException {
        // public Boolean createeSignXML() throws TransformerException, ParserConfigurationException, IOException, CertificateException, NoSuchAlgorithmException, XMLSignatureException, UnrecoverableEntryException, InvalidAlgorithmParameterException, SAXException, MarshalException, KeyStoreException {
//        EsignRequestDTO esignRequestDTO = new EsignRequestDTO();
      //  System.out.println("signRedireccccct" + esignRedirect.toString());

        String signStatus  =  eSignService.signStatus(userCodeId);

        return signStatus.toString();
    }

    @GetMapping(value = "eSignStatus")
    public void checkEsignStatus(HttpServletResponse response) throws URISyntaxException, IOException, CertificateException, NoSuchAlgorithmException, ParserConfigurationException, XMLSignatureException, InvalidAlgorithmParameterException, SAXException, UnrecoverableEntryException, MarshalException, KeyStoreException, TransformerException, DocumentException, SignatureException, InvalidKeyException, CMSException, OperatorCreationException, NoSuchProviderException {
        // public Boolean createeSignXML() throws TransformerException, ParserConfigurationException, IOException, CertificateException, NoSuchAlgorithmException, XMLSignatureException, UnrecoverableEntryException, InvalidAlgorithmParameterException, SAXException, MarshalException, KeyStoreException {
//        EsignRequestDTO esignRequestDTO = new EsignRequestDTO();
       // String abc  =  eSignService.signStatus(userCodeId);

        response.sendRedirect("http://stagingesign.hartron.org.in/esign-final-form-request/dfgdf/dfgdf/dfgdf/eert");
    }

    @RequestMapping(value = "esign/response-data", params = "resData", method = RequestMethod.GET)
    public String esignResponseData(@RequestParam String resData) throws URISyntaxException, IOException, CertificateException, NoSuchAlgorithmException, ParserConfigurationException, XMLSignatureException, InvalidAlgorithmParameterException, SAXException, UnrecoverableEntryException, MarshalException, KeyStoreException, TransformerException {
        System.out.println("signResponseeeDataaaaaaaaaaaaaaaaaaaaaaa" + resData.toString());

        return resData;
    }



//    @RequestMapping(value = "eSignStatus", params = {"userCodeId", "applicationIdCode", "responseUrl", "txn", "req_data"})
//    public void checkEsignStatus(@RequestParam String userCodeId, @RequestParam String applicationIdCode, @RequestParam String responseUrl, @RequestParam String txn, @RequestParam String req_data, HttpServletRequest request, HttpServletResponse response) throws URISyntaxException, IOException, CertificateException, NoSuchAlgorithmException, ParserConfigurationException, XMLSignatureException, InvalidAlgorithmParameterException, SAXException, UnrecoverableEntryException, MarshalException, KeyStoreException, TransformerException, DocumentException, SignatureException, InvalidKeyException, CMSException, OperatorCreationException, NoSuchProviderException {
//        // public Boolean createeSignXML() throws TransformerException, ParserConfigurationException, IOException, CertificateException, NoSuchAlgorithmException, XMLSignatureException, UnrecoverableEntryException, InvalidAlgorithmParameterException, SAXException, MarshalException, KeyStoreException {
////        EsignRequestDTO esignRequestDTO = new EsignRequestDTO();
//        // String abc  =  eSignService.signStatus(userCodeId);
//
//        String txn_ref = "";
//
//        //save user request
//
//        esignUserRequestService.sendEsignRequestToCSC(userCodeId, applicationIdCode, responseUrl, txn, req_data, request, response);
//
//        //
//        String url = "http://localhost:9001/esign-final-form-request";
//        String reqParameter = "txn_ref" + txn_ref;
//        response.sendRedirect(url +"/"+ reqParameter);
//    }


    @RequestMapping(value = "base64Decode", params = {"resData"}, method = RequestMethod.GET)
    public String base64Decode(@RequestParam String resData) throws URISyntaxException, IOException, CertificateException, NoSuchAlgorithmException, ParserConfigurationException, XMLSignatureException, InvalidAlgorithmParameterException, SAXException, UnrecoverableEntryException, MarshalException, KeyStoreException, TransformerException {

        String base64Base = this.eSignService.base64Base(resData);

        return base64Base;
    }



}







