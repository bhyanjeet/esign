package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.security.SecurityUtils;
import com.hartron.esignharyana.service.ApplicationMasterService;
import com.hartron.esignharyana.service.dto.OrganisationDocumentDTO;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.ApplicationMasterDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.micrometer.core.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.ApplicationMaster}.
 */
@RestController
@RequestMapping("/api")
public class ApplicationMasterResource {

    private final Logger log = LoggerFactory.getLogger(ApplicationMasterResource.class);

    private static final String ENTITY_NAME = "applicationMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ApplicationMasterService applicationMasterService;

    public ApplicationMasterResource(ApplicationMasterService applicationMasterService) {
        this.applicationMasterService = applicationMasterService;
    }

    /**
     * {@code POST  /application-masters} : Create a new applicationMaster.
     *
     * @param applicationMasterDTO the applicationMasterDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new applicationMasterDTO, or with status {@code 400 (Bad Request)} if the applicationMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/application-masters")
    public ResponseEntity<ApplicationMasterDTO> createApplicationMaster(@Valid @RequestBody ApplicationMasterDTO applicationMasterDTO) throws URISyntaxException {
        log.debug("REST request to save ApplicationMaster : {}", applicationMasterDTO);
        if (applicationMasterDTO.getId() != null) {
            throw new BadRequestAlertException("A new applicationMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ApplicationMasterDTO result = applicationMasterService.save(applicationMasterDTO);
        return ResponseEntity.created(new URI("/api/application-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /application-masters} : Updates an existing applicationMaster.
     *
     * @param applicationMasterDTO the applicationMasterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated applicationMasterDTO,
     * or with status {@code 400 (Bad Request)} if the applicationMasterDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the applicationMasterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/application-masters")
    public ResponseEntity<ApplicationMasterDTO> updateApplicationMaster(@Valid @RequestBody ApplicationMasterDTO applicationMasterDTO) throws URISyntaxException {
        log.debug("REST request to update ApplicationMaster : {}", applicationMasterDTO);
        if (applicationMasterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ApplicationMasterDTO result = applicationMasterService.save(applicationMasterDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, applicationMasterDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /application-masters} : get all the applicationMasters.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of applicationMasters in body.
     */
    @GetMapping("/application-masters")
    public ResponseEntity<List<ApplicationMasterDTO>> getAllApplicationMasters(Pageable pageable) {
        log.debug("REST request to get a page of ApplicationMasters");
        Page<ApplicationMasterDTO> page = applicationMasterService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /application-masters/:id} : get the "id" applicationMaster.
     *
     * @param id the id of the applicationMasterDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the applicationMasterDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/application-masters/{id}")
    public ResponseEntity<ApplicationMasterDTO> getApplicationMaster(@PathVariable Long id) {
        log.debug("REST request to get ApplicationMaster : {}", id);
        Optional<ApplicationMasterDTO> applicationMasterDTO = applicationMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(applicationMasterDTO);
    }

    /**
     * {@code DELETE  /application-masters/:id} : delete the "id" applicationMaster.
     *
     * @param id the id of the applicationMasterDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/application-masters/{id}")
    public ResponseEntity<Void> deleteApplicationMaster(@PathVariable Long id) {
        log.debug("REST request to delete ApplicationMaster : {}", id);
        applicationMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/application-masters?query=:query} : search for the applicationMaster corresponding
     * to the query.
     *
     * @param query the query of the applicationMaster search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/application-masters")
    public ResponseEntity<List<ApplicationMasterDTO>> searchApplicationMasters(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ApplicationMasters for query {}", query);
        Page<ApplicationMasterDTO> page = applicationMasterService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @RequestMapping(value = "userLogin")
    @Timed
    public List<ApplicationMasterDTO> getApplicationByLogin() {
        return applicationMasterService.findApplicationByUserLogin(SecurityUtils.getCurrentUserLogin().get());
    }

    @RequestMapping(value = "applicationByStatus", params = "status", method=RequestMethod.GET)
    @Timed
    public List<ApplicationMasterDTO> getApplicationRecordByStatus(@RequestParam String status) {
        return applicationMasterService.findApplicationRecordByStatus(status);
    }

    @RequestMapping(value = "applicationByLoginAndStatus", method=RequestMethod.GET)
    @Timed
    public List<ApplicationMasterDTO> getAllRecordByVerifyByAndStatus() {
        return applicationMasterService.findAllRecordByCreatedByAndStatus(SecurityUtils.getCurrentUserLogin().get(), "Approved");
    }


}
