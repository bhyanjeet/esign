package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.OtpService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.OtpDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import io.micrometer.core.annotation.Timed;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.Otp}.
 */
@RestController
@RequestMapping("/api")
public class OtpResource {

    private final Logger log = LoggerFactory.getLogger(OtpResource.class);

    private static final String ENTITY_NAME = "otp";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OtpService otpService;

    public OtpResource(OtpService otpService) {
        this.otpService = otpService;
    }

    /**
     * {@code POST  /otps} : Create a new otp.
     *
     * @param otpDTO the otpDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new otpDTO, or with status {@code 400 (Bad Request)} if the otp has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/otps")
    public ResponseEntity<OtpDTO> createOtp(@RequestBody OtpDTO otpDTO) throws URISyntaxException {
        log.debug("REST request to save Otp : {}", otpDTO);
        if (otpDTO.getId() != null) {
            throw new BadRequestAlertException("A new otp cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OtpDTO result = otpService.save(otpDTO);
        return ResponseEntity.created(new URI("/api/otps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /otps} : Updates an existing otp.
     *
     * @param otpDTO the otpDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated otpDTO,
     * or with status {@code 400 (Bad Request)} if the otpDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the otpDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/otps")
    public ResponseEntity<OtpDTO> updateOtp(@RequestBody OtpDTO otpDTO) throws URISyntaxException {
        log.debug("REST request to update Otp : {}", otpDTO);
        if (otpDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OtpDTO result = otpService.save(otpDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, otpDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /otps} : get all the otps.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of otps in body.
     */
    @GetMapping("/otps")
    public List<OtpDTO> getAllOtps() {
        log.debug("REST request to get all Otps");
        return otpService.findAll();
    }

    /**
     * {@code GET  /otps/:id} : get the "id" otp.
     *
     * @param id the id of the otpDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the otpDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/otps/{id}")
    public ResponseEntity<OtpDTO> getOtp(@PathVariable Long id) {
        log.debug("REST request to get Otp : {}", id);
        Optional<OtpDTO> otpDTO = otpService.findOne(id);
        return ResponseUtil.wrapOrNotFound(otpDTO);
    }

    /**
     * {@code DELETE  /otps/:id} : delete the "id" otp.
     *
     * @param id the id of the otpDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/otps/{id}")
    public ResponseEntity<Void> deleteOtp(@PathVariable Long id) {
        log.debug("REST request to delete Otp : {}", id);
        otpService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/otps?query=:query} : search for the otp corresponding
     * to the query.
     *
     * @param query the query of the otp search.
     * @return the result of the search.
     */
    @GetMapping("/_search/otps")
    public List<OtpDTO> searchOtps(@RequestParam String query) {
        log.debug("REST request to search Otps for query {}", query);
        return otpService.search(query);
    }

    @RequestMapping(value = "sendOtp", params = {"mobile"}, method = RequestMethod.GET)
    @Timed
    public Boolean sendOtp(@RequestParam Long mobile)  {
        return otpService.sendOtpByMobile(mobile);
    }

    @RequestMapping(value = "verifyOtp", params = {"mobile","otp"}, method = RequestMethod.GET)
    @Timed
    public Boolean verifyOtp(@RequestParam Long mobile, @RequestParam Integer otp) {
        return otpService.verifyOTPByMobile(mobile,otp);
    }
}
