package com.hartron.esignharyana.web.rest;
/*import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;*/
import com.hartron.esignharyana.config.MinioConfig;
import com.hartron.esignharyana.service.MinioFileService;
import com.hartron.esignharyana.service.OrganisationAttachmentService;
import com.hartron.esignharyana.service.dto.FileSaveResponseDTO;
import com.hartron.esignharyana.service.dto.MinioFileSaveDTO;
import com.hartron.esignharyana.service.dto.OrganisationAttachmentDTO;
import com.hartron.esignharyana.service.dto.UserDetailsDTO;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import io.minio.MinioClient;
import io.minio.errors.*;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.xmlpull.v1.XmlPullParserException;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MinioFileManagementResource {

    private final MinioFileService minioFileService;
    private final MinioConfig minioConfig;

    private final OrganisationAttachmentService organisationAttachmentService;

    @Autowired
    public MinioFileManagementResource(MinioFileService minioFileService, MinioConfig minioConfig, OrganisationAttachmentService organisationAttachmentService) {
        this.minioFileService = minioFileService;
        this.minioConfig = minioConfig;
        this.organisationAttachmentService = organisationAttachmentService;
    }

    @RequestMapping(value = "/minioFileSave", method = RequestMethod.POST)
    @ResponseBody
    public MinioFileSaveDTO saveFile(@RequestParam(value = "file") MultipartFile file, @RequestParam(value = "filename") String filename) {

//        String[] extension =  filename.split("\\.");
//        if (extension.length > 1) {
//            throw new BadRequestAlertException("Error while saving Attachment", "organisationMaster", "InvalidFileExtension");
//        }
//
//
//        if (!extension.equals("JPG") || !extension.equals("jpg") || !extension.equals("PNG") || !extension.equals("png") || !extension.equals("PDF") || !extension.equals("jpg")) {
//            throw new BadRequestAlertException("Error while saving Attachment", "organisationMaster", "InvalidFileExtension");
//        }

        String id = UUID.randomUUID().toString();
        String newFileName = id + "_" + filename;

        MinioFileSaveDTO minioFileSaveDTO = new MinioFileSaveDTO();
        minioFileService.minioSave(file, newFileName);
        minioFileSaveDTO.setFileName(newFileName);
       return minioFileSaveDTO;
    }
    @RequestMapping(value = "/minioFileSave/bulk", method = RequestMethod.POST)
    @ResponseBody
    public FileSaveResponseDTO saveMultipleAttachmentBulk(MultipartHttpServletRequest request) throws IOException {
        validateFile(request);
        FileSaveResponseDTO fileSaveResponseDTO = new FileSaveResponseDTO();
        Iterator<String> iterator = request.getFileNames();
        while (iterator.hasNext()) {
            String fileName = UUID.randomUUID().toString();
            String key = iterator.next();
            MultipartFile multipartFile = request.getFile(key);
            String[] extension = new String[0];


            // scales the input image to the output image
//            Graphics2D g2d = outputImage.createGraphics();

            if (multipartFile != null) {
                extension = multipartFile.getOriginalFilename().split("\\.");
                String documentId = multipartFile.getName();
                String savedFileName = minioFileService.multipleFileMinioSave(multipartFile, fileName + "." + extension[extension.length - 1]);
                if (savedFileName == null) {
                    throw new BadRequestAlertException("Error while saving Attachment", "", "Null FileName Value");
                } else {
                    if(key.equalsIgnoreCase("pan")) {
                        fileSaveResponseDTO.setPanAttachmentName(savedFileName);
                    }
                    if(key.equalsIgnoreCase("photograph")) {
                        fileSaveResponseDTO.setPhotoGraphAttachmentName(savedFileName);
                    }
                    if(key.equalsIgnoreCase("empidCard")) {
                        fileSaveResponseDTO.setEmpIdCardAttachmentName(savedFileName);
                    }
                }
            }
        }
        return fileSaveResponseDTO;
    }

    private void validateFile(MultipartHttpServletRequest request) {

        Iterator<String> iterator = request.getFileNames();
        while (iterator.hasNext()) {
            String fileName = UUID.randomUUID().toString();
            String key = iterator.next();
            MultipartFile multipartFile = request.getFile(key);
            if (multipartFile != null) {
                String[] extension = multipartFile.getOriginalFilename().split("\\.");
                if(extension.length == 2) {
                    String documentId = multipartFile.getName();
//                    String savedFileName = minioFileService.multipleFileMinioSave(multipartFile, fileName + "." + extension[extension.length - 1]);
                    if (documentId == null) {
                        throw new BadRequestAlertException("Please select Required Document", "", "Document Required!");
                    } else if (!extension[extension.length - 1].equalsIgnoreCase("jpg")) {

                        throw new BadRequestAlertException("Only JPG images attachment are allowed", "", "Please Select " + key + " as JPG image only!");

                    } else {
                        if (key.equalsIgnoreCase("pan")) {
                            if(multipartFile.getSize() > 100000)
                            {
                                throw new BadRequestAlertException("Pancard Card Attachment size should not be more than 100KB!", "", "Please Select " + key + " Image <= 100KB only!");
                            }
                            try{
//                                Image newImage = multipartFile.getOriginalFilename()getScaledInstance(newWidth, newHeight, Image.SCALE_DEFAULT);

                                File inputFile = new File(request.getFile(key).getOriginalFilename());
                                BufferedImage inputImage = ImageIO.read(multipartFile.getInputStream());
                                BufferedImage outputImage = new BufferedImage(100,100, inputImage.getType());

                            }catch(Exception e){
                                throw new BadRequestAlertException("Error while saving Attachment!", "", "Don't be over samart!");
                            }

                            System.out.println("Pan File Size =" + multipartFile.getSize());

                        }
                        if (key.equalsIgnoreCase("photograph")) {

                            if(multipartFile.getSize() > 25000)
                            {
                                throw new BadRequestAlertException("Photograph Attachment size should not be more than 25KB!", "", "Please Select " + key + " Image <= 25KB only!");
                            }
                            try{
                                File inputFile = new File(request.getFile(key).getOriginalFilename());
                                BufferedImage inputImage = ImageIO.read(multipartFile.getInputStream());
                                BufferedImage outputImage = new BufferedImage(100,100, inputImage.getType());

                            }catch(Exception e){
                                throw new BadRequestAlertException("Error while saving Attachment", "", "Don't be over samart!");
                            }

                            System.out.println("photograph File Size ="  + multipartFile.getSize());

                        }
                        if (key.equalsIgnoreCase("empidCard")) {
                            if(multipartFile.getSize() > 100000)
                            {
                                throw new BadRequestAlertException("Employee ID Card Attachment size should not be more than 100KB!", "", "Please Select " + key + " Image <= 100KB only!");
                            }
                            try{
                                File inputFile = new File(request.getFile(key).getOriginalFilename());
                                BufferedImage inputImage = ImageIO.read(multipartFile.getInputStream());
                                BufferedImage outputImage = new BufferedImage(100,100, inputImage.getType());

                            }catch(Exception e){
                                throw new BadRequestAlertException("Error while saving Attachment", "", "Don't be over samart!");
                            }

                            System.out.println("empidCard File Size =" + multipartFile.getSize());

                        }
                    }

                }else
                {
                    throw new BadRequestAlertException("Attachment File should have only one Dot!", "", "Please Select " + key + " as JPG image with single (.) only!");
                }
            }else
            {
                throw new BadRequestAlertException("Error while saving Attachment", "", "Please Select Document Attachments!");
            }

        }
    }


    @RequestMapping(value = "/getAttachment", params = "filename", method = RequestMethod.GET)
    @ResponseBody
    public String getSignature(@RequestParam String filename) {
        String url = null;
        try {
            MinioClient minioClient = minioConfig.getMinioClient();
            url = DatatypeConverter.printBase64Binary(IOUtils.toByteArray(minioClient.getObject("esignharyana",filename)));
        } catch (InvalidBucketNameException | NoSuchAlgorithmException | IOException | XmlPullParserException | InvalidKeyException | NoResponseException | io.minio.errors.InternalException | ErrorResponseException | InsufficientDataException | InvalidArgumentException e) {
        }
        return url;
    }
    @RequestMapping(value = "/saveMultipleAttachment", method = RequestMethod.POST)
    @ResponseBody
    public List<MinioFileSaveDTO> saveMultipleAttachment(MultipartHttpServletRequest request, HttpServletResponse response, @RequestParam Long organisationId) {
        List<MinioFileSaveDTO> minioFileSaveDTOList = new ArrayList<>();
        Iterator<String> iterator = request.getFileNames();
        while (iterator.hasNext()) {
            MinioFileSaveDTO minioFileSaveDTO = new MinioFileSaveDTO();
            String fileName = UUID.randomUUID().toString();
            MultipartFile multipartFile = request.getFile(iterator.next());
            String[] extension = new String[0];
            if (multipartFile != null) {
                extension = multipartFile.getOriginalFilename().split("\\.");
                String documentId = multipartFile.getName();
                String savedFileName = minioFileService.multipleFileMinioSave(multipartFile, fileName + "." + extension[extension.length - 1]);
                if (savedFileName == null) {
                    throw new BadRequestAlertException("Error while saving Attachment", "", "Null FileName Value");
                } else {
                    OrganisationAttachmentDTO organisationAttachmentDTO = new OrganisationAttachmentDTO();
                    organisationAttachmentDTO.setOrganisationMasterId(organisationId);
                    organisationAttachmentDTO.setOrganisationDocumentId(Long.valueOf(documentId));
                    organisationAttachmentDTO.setAttachment(savedFileName);
                    organisationAttachmentService.save(organisationAttachmentDTO);

                    minioFileSaveDTO.setFileName(savedFileName);
                    minioFileSaveDTOList.add(minioFileSaveDTO);
                }
            }
        }
        return minioFileSaveDTOList;
    }

}
