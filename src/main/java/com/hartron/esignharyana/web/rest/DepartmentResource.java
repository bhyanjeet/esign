package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.domain.User;
import com.hartron.esignharyana.service.DepartmentService;
import com.hartron.esignharyana.service.UserService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.DepartmentDTO;
import com.hartron.esignharyana.service.dto.DepartmentCriteria;
import com.hartron.esignharyana.service.DepartmentQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.Department}.
 */
@RestController
@RequestMapping("/api")
public class DepartmentResource {

    private final Logger log = LoggerFactory.getLogger(DepartmentResource.class);

    private static final String ENTITY_NAME = "department";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DepartmentService departmentService;

    private final DepartmentQueryService departmentQueryService;

    private final UserService userService;


    public DepartmentResource(DepartmentService departmentService, DepartmentQueryService departmentQueryService, UserService userService) {
        this.departmentService = departmentService;
        this.departmentQueryService = departmentQueryService;
        this.userService = userService;
    }

    /**
     * {@code POST  /departments} : Create a new department.
     *
     * @param departmentDTO the departmentDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new departmentDTO, or with status {@code 400 (Bad Request)} if the department has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/departments")
    public ResponseEntity<DepartmentDTO> createDepartment(@RequestBody DepartmentDTO departmentDTO) throws URISyntaxException {
        log.debug("REST request to save Department : {}", departmentDTO);
        if (departmentDTO.getId() != null) {
            throw new BadRequestAlertException("A new department cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Optional<User> user = userService.getUserWithAuthorities();

        departmentDTO.setCreatedBy(user.get().getLogin());
        departmentDTO.setUpdatedBy(user.get().getLogin());
        departmentDTO.setCreatedOn(LocalDate.now());
        departmentDTO.setUpdatedOn(LocalDate.now());
        departmentDTO.setStatus("Active");
        validateInput(departmentDTO);

        DepartmentDTO result = departmentService.save(departmentDTO);
        return ResponseEntity.created(new URI("/api/departments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    private void validateInput(DepartmentDTO departmentDTO) {
        String name = "[a-zA-Z ]*$";
        String createdBy = "[a-zA-Z0-9 ]*$";
        String deptId = "[0-9]*$";

        if(departmentDTO.getDepartmentName()!=null) {
            if (!departmentDTO.getDepartmentName().trim().matches(name) || departmentDTO.getDepartmentName() == null || departmentDTO.getDepartmentName().trim() == "") {
                System.out.println("Invalid Department Name");
                throw new BadRequestAlertException("Department Name should be alphabetic!", "organisationMaster", "invalidNodalOfficerName");
            }
        }else
        {
            System.out.println("Invalid Department Name");
            throw new BadRequestAlertException("Department Name can't be Empty!", "organisationMaster", "invalidNodalOfficerName");
        }
        if (!departmentDTO.getCreatedBy().trim().matches(createdBy)) {
            System.out.println("Invalid created by input");
            throw new BadRequestAlertException("Created By should be alphabetic!", "organisationMaster", "invalidNodalOfficerName");
        }
//        if(departmentDTO.getId().toString().equals("")) {
//            if (!departmentDTO.getId().toString().matches(deptId)) {
//                System.out.println("Invalid ID");
//                throw new BadRequestAlertException("Id should be Numeric!", "organisationMaster", "invalidNodalOfficerName");
//            }
//        }
        if (!departmentDTO.getUpdatedBy().matches(createdBy)) {
            System.out.println("Invalid Updated By");
            throw new BadRequestAlertException("Updated by should be alphabetic!", "organisationMaster", "invalidNodalOfficerName");
        }
        if(departmentDTO.getStatus()!=null) {
            if (!departmentDTO.getStatus().matches(createdBy)) {
                System.out.println("Invalid Status");
                throw new BadRequestAlertException("Status should be alphabetic!", "organisationMaster", "invalidNodalOfficerName");
            }
        }
        if(departmentDTO.getRemark()!=null) {
            if (!departmentDTO.getRemark().matches(createdBy)) {
                System.out.println("Invalid Status");
                throw new BadRequestAlertException("Remarks accepts only alphanumeric value!", "organisationMaster", "invalidNodalOfficerName");
            }
        }

    }

    /**
     * {@code PUT  /departments} : Updates an existing department.
     *
     * @param departmentDTO the departmentDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated departmentDTO,
     * or with status {@code 400 (Bad Request)} if the departmentDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the departmentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/departments")
    public ResponseEntity<DepartmentDTO> updateDepartment(@RequestBody DepartmentDTO departmentDTO) throws URISyntaxException {
        log.debug("REST request to update Department : {}", departmentDTO);
        if (departmentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Optional<User> user = userService.getUserWithAuthorities();
        departmentDTO.setCreatedBy(user.get().getLogin());
        departmentDTO.setUpdatedBy(user.get().getLogin());
        departmentDTO.setCreatedOn(LocalDate.now());
        departmentDTO.setUpdatedOn(LocalDate.now());
        departmentDTO.setStatus("Active");
        validateInput(departmentDTO);
        DepartmentDTO result = departmentService.save(departmentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, departmentDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /departments} : get all the departments.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of departments in body.
     */
    @GetMapping("/departments")
    public ResponseEntity<List<DepartmentDTO>> getAllDepartments(DepartmentCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Departments by criteria: {}", criteria);
        Page<DepartmentDTO> page = departmentQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /departments/count} : count all the departments.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/departments/count")
    public ResponseEntity<Long> countDepartments(DepartmentCriteria criteria) {
        log.debug("REST request to count Departments by criteria: {}", criteria);
        return ResponseEntity.ok().body(departmentQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /departments/:id} : get the "id" department.
     *
     * @param id the id of the departmentDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the departmentDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/departments/{id}")
    public ResponseEntity<DepartmentDTO> getDepartment(@PathVariable Long id) {
        log.debug("REST request to get Department : {}", id);
        Optional<DepartmentDTO> departmentDTO = departmentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(departmentDTO);
    }

    /**
     * {@code DELETE  /departments/:id} : delete the "id" department.
     *
     * @param id the id of the departmentDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/departments/{id}")
    public ResponseEntity<Void> deleteDepartment(@PathVariable Long id) {
        log.debug("REST request to delete Department : {}", id);
        departmentService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/departments?query=:query} : search for the department corresponding
     * to the query.
     *
     * @param query the query of the department search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/departments")
    public ResponseEntity<List<DepartmentDTO>> searchDepartments(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Departments for query {}", query);
        Page<DepartmentDTO> page = departmentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
