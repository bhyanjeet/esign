package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.EsignErrorService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.EsignErrorDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.EsignError}.
 */
@RestController
@RequestMapping("/api")
public class EsignErrorResource {

    private final Logger log = LoggerFactory.getLogger(EsignErrorResource.class);

    private static final String ENTITY_NAME = "esignError";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EsignErrorService esignErrorService;

    public EsignErrorResource(EsignErrorService esignErrorService) {
        this.esignErrorService = esignErrorService;
    }

    /**
     * {@code POST  /esign-errors} : Create a new esignError.
     *
     * @param esignErrorDTO the esignErrorDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new esignErrorDTO, or with status {@code 400 (Bad Request)} if the esignError has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/esign-errors")
    public ResponseEntity<EsignErrorDTO> createEsignError(@RequestBody EsignErrorDTO esignErrorDTO) throws URISyntaxException {
        log.debug("REST request to save EsignError : {}", esignErrorDTO);
        if (esignErrorDTO.getId() != null) {
            throw new BadRequestAlertException("A new esignError cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EsignErrorDTO result = esignErrorService.save(esignErrorDTO);
        return ResponseEntity.created(new URI("/api/esign-errors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /esign-errors} : Updates an existing esignError.
     *
     * @param esignErrorDTO the esignErrorDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated esignErrorDTO,
     * or with status {@code 400 (Bad Request)} if the esignErrorDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the esignErrorDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/esign-errors")
    public ResponseEntity<EsignErrorDTO> updateEsignError(@RequestBody EsignErrorDTO esignErrorDTO) throws URISyntaxException {
        log.debug("REST request to update EsignError : {}", esignErrorDTO);
        if (esignErrorDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EsignErrorDTO result = esignErrorService.save(esignErrorDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, esignErrorDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /esign-errors} : get all the esignErrors.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of esignErrors in body.
     */
    @GetMapping("/esign-errors")
    public List<EsignErrorDTO> getAllEsignErrors() {
        log.debug("REST request to get all EsignErrors");
        return esignErrorService.findAll();
    }

    /**
     * {@code GET  /esign-errors/:id} : get the "id" esignError.
     *
     * @param id the id of the esignErrorDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the esignErrorDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/esign-errors/{id}")
    public ResponseEntity<EsignErrorDTO> getEsignError(@PathVariable Long id) {
        log.debug("REST request to get EsignError : {}", id);
        Optional<EsignErrorDTO> esignErrorDTO = esignErrorService.findOne(id);
        return ResponseUtil.wrapOrNotFound(esignErrorDTO);
    }

    /**
     * {@code DELETE  /esign-errors/:id} : delete the "id" esignError.
     *
     * @param id the id of the esignErrorDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/esign-errors/{id}")
    public ResponseEntity<Void> deleteEsignError(@PathVariable Long id) {
        log.debug("REST request to delete EsignError : {}", id);
        esignErrorService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/esign-errors?query=:query} : search for the esignError corresponding
     * to the query.
     *
     * @param query the query of the esignError search.
     * @return the result of the search.
     */
    @GetMapping("/_search/esign-errors")
    public List<EsignErrorDTO> searchEsignErrors(@RequestParam String query) {
        log.debug("REST request to search EsignErrors for query {}", query);
        return esignErrorService.search(query);
    }
}
