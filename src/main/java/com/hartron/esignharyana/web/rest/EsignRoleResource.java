package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.EsignRoleService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.EsignRoleDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.EsignRole}.
 */
@RestController
@RequestMapping("/api")
public class EsignRoleResource {

    private final Logger log = LoggerFactory.getLogger(EsignRoleResource.class);

    private static final String ENTITY_NAME = "esignRole";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EsignRoleService esignRoleService;

    public EsignRoleResource(EsignRoleService esignRoleService) {
        this.esignRoleService = esignRoleService;
    }

    /**
     * {@code POST  /esign-roles} : Create a new esignRole.
     *
     * @param esignRoleDTO the esignRoleDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new esignRoleDTO, or with status {@code 400 (Bad Request)} if the esignRole has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/esign-roles")
    public ResponseEntity<EsignRoleDTO> createEsignRole(@Valid @RequestBody EsignRoleDTO esignRoleDTO) throws URISyntaxException {
        log.debug("REST request to save EsignRole : {}", esignRoleDTO);
        if (esignRoleDTO.getId() != null) {
            throw new BadRequestAlertException("A new esignRole cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EsignRoleDTO result = esignRoleService.save(esignRoleDTO);
        return ResponseEntity.created(new URI("/api/esign-roles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /esign-roles} : Updates an existing esignRole.
     *
     * @param esignRoleDTO the esignRoleDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated esignRoleDTO,
     * or with status {@code 400 (Bad Request)} if the esignRoleDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the esignRoleDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/esign-roles")
    public ResponseEntity<EsignRoleDTO> updateEsignRole(@Valid @RequestBody EsignRoleDTO esignRoleDTO) throws URISyntaxException {
        log.debug("REST request to update EsignRole : {}", esignRoleDTO);
        if (esignRoleDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EsignRoleDTO result = esignRoleService.save(esignRoleDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, esignRoleDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /esign-roles} : get all the esignRoles.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of esignRoles in body.
     */
    @GetMapping("/esign-roles")
    public ResponseEntity<List<EsignRoleDTO>> getAllEsignRoles(Pageable pageable) {
        log.debug("REST request to get a page of EsignRoles");
        Page<EsignRoleDTO> page = esignRoleService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /esign-roles/:id} : get the "id" esignRole.
     *
     * @param id the id of the esignRoleDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the esignRoleDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/esign-roles/{id}")
    public ResponseEntity<EsignRoleDTO> getEsignRole(@PathVariable Long id) {
        log.debug("REST request to get EsignRole : {}", id);
        Optional<EsignRoleDTO> esignRoleDTO = esignRoleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(esignRoleDTO);
    }

    /**
     * {@code DELETE  /esign-roles/:id} : delete the "id" esignRole.
     *
     * @param id the id of the esignRoleDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/esign-roles/{id}")
    public ResponseEntity<Void> deleteEsignRole(@PathVariable Long id) {
        log.debug("REST request to delete EsignRole : {}", id);
        esignRoleService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/esign-roles?query=:query} : search for the esignRole corresponding
     * to the query.
     *
     * @param query the query of the esignRole search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/esign-roles")
    public ResponseEntity<List<EsignRoleDTO>> searchEsignRoles(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of EsignRoles for query {}", query);
        Page<EsignRoleDTO> page = esignRoleService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
