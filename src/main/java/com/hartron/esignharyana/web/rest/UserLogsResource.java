package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.UserLogsService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.UserLogsDTO;
import com.hartron.esignharyana.service.dto.UserLogsCriteria;
import com.hartron.esignharyana.service.UserLogsQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.UserLogs}.
 */
@RestController
@RequestMapping("/api")
public class UserLogsResource {

    private final Logger log = LoggerFactory.getLogger(UserLogsResource.class);

    private static final String ENTITY_NAME = "userLogs";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserLogsService userLogsService;

    private final UserLogsQueryService userLogsQueryService;

    public UserLogsResource(UserLogsService userLogsService, UserLogsQueryService userLogsQueryService) {
        this.userLogsService = userLogsService;
        this.userLogsQueryService = userLogsQueryService;
    }

    /**
     * {@code POST  /user-logs} : Create a new userLogs.
     *
     * @param userLogsDTO the userLogsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userLogsDTO, or with status {@code 400 (Bad Request)} if the userLogs has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-logs")
    public ResponseEntity<UserLogsDTO> createUserLogs(@RequestBody UserLogsDTO userLogsDTO) throws URISyntaxException {
        log.debug("REST request to save UserLogs : {}", userLogsDTO);
        if (userLogsDTO.getId() != null) {
            throw new BadRequestAlertException("A new userLogs cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserLogsDTO result = userLogsService.save(userLogsDTO);
        return ResponseEntity.created(new URI("/api/user-logs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-logs} : Updates an existing userLogs.
     *
     * @param userLogsDTO the userLogsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userLogsDTO,
     * or with status {@code 400 (Bad Request)} if the userLogsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userLogsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-logs")
    public ResponseEntity<UserLogsDTO> updateUserLogs(@RequestBody UserLogsDTO userLogsDTO) throws URISyntaxException {
        log.debug("REST request to update UserLogs : {}", userLogsDTO);
        if (userLogsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserLogsDTO result = userLogsService.save(userLogsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, userLogsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-logs} : get all the userLogs.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userLogs in body.
     */
    @GetMapping("/user-logs")
    public ResponseEntity<List<UserLogsDTO>> getAllUserLogs(UserLogsCriteria criteria, Pageable pageable) {
        log.debug("REST request to get UserLogs by criteria: {}", criteria);
        Page<UserLogsDTO> page = userLogsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /user-logs/count} : count all the userLogs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/user-logs/count")
    public ResponseEntity<Long> countUserLogs(UserLogsCriteria criteria) {
        log.debug("REST request to count UserLogs by criteria: {}", criteria);
        return ResponseEntity.ok().body(userLogsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /user-logs/:id} : get the "id" userLogs.
     *
     * @param id the id of the userLogsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userLogsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-logs/{id}")
    public ResponseEntity<UserLogsDTO> getUserLogs(@PathVariable Long id) {
        log.debug("REST request to get UserLogs : {}", id);
        Optional<UserLogsDTO> userLogsDTO = userLogsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userLogsDTO);
    }

    /**
     * {@code DELETE  /user-logs/:id} : delete the "id" userLogs.
     *
     * @param id the id of the userLogsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-logs/{id}")
    public ResponseEntity<Void> deleteUserLogs(@PathVariable Long id) {
        log.debug("REST request to delete UserLogs : {}", id);
        userLogsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/user-logs?query=:query} : search for the userLogs corresponding
     * to the query.
     *
     * @param query the query of the userLogs search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/user-logs")
    public ResponseEntity<List<UserLogsDTO>> searchUserLogs(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of UserLogs for query {}", query);
        Page<UserLogsDTO> page = userLogsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
