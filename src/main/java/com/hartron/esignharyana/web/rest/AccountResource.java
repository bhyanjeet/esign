package com.hartron.esignharyana.web.rest;


import com.hartron.esignharyana.config.AesUtilHelper;
import com.hartron.esignharyana.domain.User;
import com.hartron.esignharyana.repository.UserRepository;
import com.hartron.esignharyana.security.SecurityUtils;
import com.hartron.esignharyana.service.MailService;
import com.hartron.esignharyana.service.UserService;
import com.hartron.esignharyana.service.dto.PasswordChangeDTO;
import com.hartron.esignharyana.service.dto.UserDTO;
import com.hartron.esignharyana.web.rest.errors.*;
import com.hartron.esignharyana.web.rest.vm.KeyAndPasswordVM;
import com.hartron.esignharyana.web.rest.vm.ManagedUserVM;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

    private static class AccountResourceException extends RuntimeException {
        private AccountResourceException(String message) {
            super(message);
        }
    }

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private final UserRepository userRepository;

    private final UserService userService;

    private final MailService mailService;

    public AccountResource(UserRepository userRepository, UserService userService, MailService mailService) {

        this.userRepository = userRepository;
        this.userService = userService;
        this.mailService = mailService;
    }

    /**
     * {@code POST  /register} : register the user.
     *
     * @param managedUserVM the managed user View Model.
     * @throws InvalidPasswordException {@code 400 (Bad Request)} if the password is incorrect.
     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already used.
     * @throws LoginAlreadyUsedException {@code 400 (Bad Request)} if the login is already used.
     */
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public void registerAccount(@Valid @RequestBody ManagedUserVM managedUserVM) {
        if (!checkPasswordLength(managedUserVM.getPassword())) {
            throw new InvalidPasswordException();
        }
        passwordValidation(managedUserVM.getLogin(), managedUserVM.getPassword());
        User user = userService.registerUser(managedUserVM, managedUserVM.getPassword());
        mailService.sendActivationEmail(user);
    }

    /**
     * {@code GET  /activate} : activate the registered user.
     *
     * @param key the activation key.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be activated.
     */
    @GetMapping("/activate")
    public void activateAccount(@RequestParam(value = "key") String key) {
        Optional<User> user = userService.activateRegistration(key);
        if (!user.isPresent()) {
            throw new AccountResourceException("No user was found for this activation key");
        }
    }

    /**
     * {@code GET  /authenticate} : check if the user is authenticated, and return its login.
     *
     * @param request the HTTP request.
     * @return the login if the user is authenticated.
     */
    @GetMapping("/authenticate")
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    /**
     * {@code GET  /account} : get the current user.
     *
     * @return the current user.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be returned.
     */
    @GetMapping("/account")
    public UserDTO getAccount() {
        return userService.getUserWithAuthorities()
            .map(UserDTO::new)
            .orElseThrow(() -> new AccountResourceException("User could not be found"));
    }

    /**
     * {@code POST  /account} : update the current user information.
     *
     * @param userDTO the current user information.
     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already used.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user login wasn't found.
     */
    @PostMapping("/account")
    public void saveAccount(@Valid @RequestBody UserDTO userDTO) {
        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new AccountResourceException("Current user login not found"));
        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userLogin))) {
            throw new EmailAlreadyUsedException();
        }
        Optional<User> user = userRepository.findOneByLogin(userLogin);
        if (!user.isPresent()) {
            throw new AccountResourceException("User could not be found");
        }
        userService.updateUser(userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(),
            userDTO.getLangKey(), userDTO.getImageUrl());
    }

    /**
     * {@code POST  /account/change-password} : changes the current user's password.
     *
     * @param passwordChangeDto current and new password.
     * @throws InvalidPasswordException {@code 400 (Bad Request)} if the new password is incorrect.
     */
    @PostMapping(path = "/account/change-password")
    public void changePassword(@RequestBody PasswordChangeDTO passwordChangeDto) {
        final String secretKey = "6D24456D6640616362676259646E4578";
        String originalCurrentPassword = passwordChangeDto.getCurrentPassword();
        String originalNewPassword = passwordChangeDto.getNewPassword();
        String decryptedCurrentPassword = AesUtilHelper.decrypt(originalCurrentPassword, secretKey);
        String decryptedNewPassword = AesUtilHelper.decrypt(originalNewPassword, secretKey);
        if (!checkPasswordLength(decryptedNewPassword)) {
            throw new InvalidPasswordException();
        }
        resetPasswordValidation(decryptedNewPassword);
        userService.changePassword(decryptedCurrentPassword, decryptedNewPassword);
    }

    /**
     * {@code POST   /account/reset-password/init} : Send an email to reset the password of the user.
     *
     * @param mail the mail of the user.
     * @throws EmailNotFoundException {@code 400 (Bad Request)} if the email address is not registered.
     */
    @PostMapping(path = "/account/reset-password/init")
    public void requestPasswordReset(@RequestBody String mail) {
       mailService.sendPasswordResetMail(
           userService.requestPasswordReset(mail)
               .orElseThrow(EmailNotFoundException::new)
       );
    }

    /**
     * {@code POST   /account/reset-password/finish} : Finish to reset the password of the user.
     *
     * @param keyAndPassword the generated key and the new password.
     * @throws InvalidPasswordException {@code 400 (Bad Request)} if the password is incorrect.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the password could not be reset.
     */
    @PostMapping(path = "/account/reset-password/finish")
    public void finishPasswordReset(@RequestBody KeyAndPasswordVM keyAndPassword) {
        final String secretKey = "6D24456D6640616362676259646E4578";
        String originalNewPassword = keyAndPassword.getNewPassword();
        String decryptedNewPassword = AesUtilHelper.decrypt(originalNewPassword, secretKey);
        System.out.println(decryptedNewPassword);
        System.out.println(originalNewPassword);
        if (!checkPasswordLength(decryptedNewPassword)) {
            throw new InvalidPasswordException();
        }
        resetPasswordValidation(decryptedNewPassword);
        Optional<User> user =
            userService.completePasswordReset(decryptedNewPassword, keyAndPassword.getKey());

        if (!user.isPresent()) {
            throw new AccountResourceException("No user was found for this reset key");
        }
    }

    private static boolean checkPasswordLength(String password) {
        return !StringUtils.isEmpty(password) &&
            password.length() >= ManagedUserVM.PASSWORD_MIN_LENGTH &&
            password.length() <= ManagedUserVM.PASSWORD_MAX_LENGTH;
    }

    public void passwordValidation(String userName, String password)
    {
        if (password.length() > 15 || password.length() < 8)
        {
            System.out.println("Password should be less than 15 and more than 8 characters in length.");
            throw new BadRequestAlertException("Password should be less than 15 and more than 8 characters in length!", "users", "invalidPassword");

        }
        if (password.indexOf(userName) > -1)
        {
            System.out.println("Password Should not be same as user name");
            throw new BadRequestAlertException("Password Should not be same as user name!", "users", "invalidPassword");
        }
        String upperCaseChars = "(.*[A-Z].*)";
        if (!password.matches(upperCaseChars ))
        {
            System.out.println("Password should contain atleast one upper case alphabet");
            throw new BadRequestAlertException("Password should contain atleast one upper case alphabet!", "users", "invalidPassword");

        }
        String lowerCaseChars = "(.*[a-z].*)";
        if (!password.matches(lowerCaseChars ))
        {
            System.out.println("Password should contain atleast one lower case alphabet");
            throw new BadRequestAlertException("Password should contain atleast one lower case alphabet!", "users", "invalidPassword");

        }
        String numbers = "(.*[0-9].*)";
        if (!password.matches(numbers ))
        {
            System.out.println("Password should contain atleast one number.");
            throw new BadRequestAlertException("Password should contain atleast one number!", "users", "invalidPassword");

        }
        String specialChars = "(.*[,~,!,@,#,$,%,^,&,*,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?].*$)";
        if (!password.matches(specialChars ))
        {
            System.out.println("Password should contain atleast one special character");
            throw new BadRequestAlertException("Password should contain atleast one special character!", "users", "invalidPassword");

        }

        String user_specialChars = "(.*[,~,!,@,#,$,%,^,&,*,(,),_,=,+,[,{,],},|,;,:,<,>,/,?].*$)";
        if (userName.matches(user_specialChars))
        {
            System.out.println("Username should not contain any special character");
            throw new BadRequestAlertException("Username should not contain any special character!", "users", "invalidUsername");

        }

        String user_numbers = "(.*[0-9].*)";
        if (userName.matches(user_numbers))
        {
            System.out.println("Username should not contain any number.");
            throw new BadRequestAlertException("Username should not contain any number!", "users", "invalidUsername");

        }

    }

    public void resetPasswordValidation(String password)
    {
        if (password.length() > 15 || password.length() < 8)
        {
            System.out.println("Password should be less than 15 and more than 8 characters in length.");
            throw new BadRequestAlertException("Password should be less than 15 and more than 8 characters in length!", "users", "invalidPassword");

        }
        String upperCaseChars = "(.*[A-Z].*)";
        if (!password.matches(upperCaseChars ))
        {
            System.out.println("Password should contain atleast one upper case alphabet");
            throw new BadRequestAlertException("Password should contain atleast one upper case alphabet!", "users", "invalidPassword");

        }
        String lowerCaseChars = "(.*[a-z].*)";
        if (!password.matches(lowerCaseChars ))
        {
            System.out.println("Password should contain atleast one lower case alphabet");
            throw new BadRequestAlertException("Password should contain atleast one lower case alphabet!", "users", "invalidPassword");

        }
        String numbers = "(.*[0-9].*)";
        if (!password.matches(numbers ))
        {
            System.out.println("Password should contain atleast one number.");
            throw new BadRequestAlertException("Password should contain atleast one number!", "users", "invalidPassword");

        }
        String specialChars = "(.*[,~,!,@,#,$,%,^,&,*,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?].*$)";
        if (!password.matches(specialChars ))
        {
            System.out.println("Password should contain atleast one special character");
            throw new BadRequestAlertException("Password should contain atleast one special character!", "users", "invalidPassword");

        }
    }

}
