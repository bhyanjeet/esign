package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.security.SecurityUtils;
import com.hartron.esignharyana.service.UserDetailsService;
import com.hartron.esignharyana.service.dto.UserDetailsDTO;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.UserDetails}.
 */
@RestController
@RequestMapping("/api")
public class UserDetailsResource {

    private final Logger log = LoggerFactory.getLogger(UserDetailsResource.class);

    private static final String ENTITY_NAME = "userDetails";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserDetailsService userDetailsService;

    public UserDetailsResource(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    /**
     * {@code POST  /user-details} : Create a new userDetails.
     *
     * @param userDetailsDTO the userDetailsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userDetailsDTO, or with status {@code 400 (Bad Request)} if the userDetails has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-details")
    public ResponseEntity<UserDetailsDTO> createUserDetails(@Valid @RequestBody UserDetailsDTO userDetailsDTO) throws URISyntaxException {
        log.debug("REST request to save UserDetails : {}", userDetailsDTO);
        if (userDetailsDTO.getId() != null) {
            throw new BadRequestAlertException("A new userDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        isEmailValid(userDetailsDTO.getUserOfficialEmail());
        isPanCardValid(userDetailsDTO.getPan());
        isValidName(userDetailsDTO.getUserFullName());
        validAadhar(userDetailsDTO.getAadhaar());
        validMobileNumber(userDetailsDTO.getUserPhoneMobile());
        UserDetailsDTO result = userDetailsService.save(userDetailsDTO);
        return ResponseEntity.created(new URI("/api/user-details/" + result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /user-details} : Updates an existing userDetails.
     *
     * @param userDetailsDTO the userDetailsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userDetailsDTO,
     * or with status {@code 400 (Bad Request)} if the userDetailsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userDetailsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-details")
    public ResponseEntity<UserDetailsDTO> updateUserDetails(@Valid @RequestBody UserDetailsDTO userDetailsDTO) throws URISyntaxException {
        log.debug("REST request to update UserDetails : {}", userDetailsDTO);
        if (userDetailsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserDetailsDTO result = userDetailsService.save(userDetailsDTO);
        return ResponseEntity.ok()
            .body(result);
    }

    /**
     * {@code GET  /user-details} : get all the userDetails.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userDetails in body.
     */
    @GetMapping("/user-details")
    public ResponseEntity<List<UserDetailsDTO>> getAllUserDetails(Pageable pageable) {
        log.debug("REST request to get a page of UserDetails");
        Page<UserDetailsDTO> page = userDetailsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /user-details/:id} : get the "id" userDetails.
     *
     * @param id the id of the userDetailsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userDetailsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-details/{id}")
    public ResponseEntity<UserDetailsDTO> getUserDetails(@PathVariable Long id) {
        log.debug("REST request to get UserDetails : {}", id);
        Optional<UserDetailsDTO> userDetailsDTO = userDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userDetailsDTO);
    }

    @GetMapping("/user-detailsById/{id}")
    public ResponseEntity<UserDetailsDTO> getUserDetailsById(@PathVariable Long id) {
        log.debug("REST request to get UserDetails : {}", id);
        Optional<UserDetailsDTO> userDetailsDTO = userDetailsService.findOneById(id);
        return ResponseUtil.wrapOrNotFound(userDetailsDTO);
    }

    /**
     * {@code DELETE  /user-details/:id} : delete the "id" userDetails.
     *
     * @param id the id of the userDetailsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-details/{id}")
    public ResponseEntity<Void> deleteUserDetails(@PathVariable Long id) {
        log.debug("REST request to delete UserDetails : {}", id);
        userDetailsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/user-details?query=:query} : search for the userDetails corresponding
     * to the query.
     *
     * @param query the query of the userDetails search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/user-details")
    public ResponseEntity<List<UserDetailsDTO>> searchUserDetails(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of UserDetails for query {}", query);
        Page<UserDetailsDTO> page = userDetailsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @RequestMapping(value = "userByUserLogin")
    public List<UserDetailsDTO> getUserByLogin() {
        return userDetailsService.findUserByUserLogin(SecurityUtils.getCurrentUserLogin().get());
    }

    @RequestMapping(value = "userByUserStatus")
    public List<UserDetailsDTO> getVerifiedUser() {
        return userDetailsService.findVerifiedUser("Verified");
    }

    @RequestMapping(value = "/userDetailsByUserIdCode", params = "userIdCode", method = RequestMethod.GET)
    public List<UserDetailsDTO> getUserByUserIdCode(@RequestParam String userIdCode) {
        return userDetailsService.findUserByUserCodeId(userIdCode);
    }


    public static void isPanCardValid(String pan_number) {

        Pattern pattern = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}");

        Matcher matcher = pattern.matcher(pan_number);
        // Check if pattern matches
        if (!matcher.matches()) {

            System.out.println("Invalid Pan Number");
            throw new BadRequestAlertException("PLease Enter Valid Pan Number!", "organisationMaster", "invalidPanNumber");

        }
    }

    public void isEmailValid(String email) {

        String emailRegex = "^[A-Za-z0-9+_.-]+@(.+)$";

        Pattern pat = Pattern.compile(emailRegex);
        if(!email.matches(emailRegex)) {
            System.out.println("Invalid Email Id");
            throw new BadRequestAlertException("PLease enter valid email Id!", "organisationMaster", "invalidEmailId");
        }
    }

    public void isValidName(String name) {
        String re = "[a-zA-Z ]*$";
        if (!name.matches(re)) {
            System.out.println("Invalid Nodal Officer Name");
            throw new BadRequestAlertException("Nodal Officer Name should be alphabetic!", "organisationMaster", "invalidNodalOfficerName");
        }
    }

    public void validAadhar(String aadhar) {
        if (aadhar.length() > 4 || aadhar.length() < 4)
        {
            System.out.println("Aadhar Number should be 4 digit.");
            throw new BadRequestAlertException("Aadhar Number should be 4 digit!", "organisationMaster", "invalidAadhar");

        }
    }

    public void validMobileNumber(String mobileNumber) {
        if (mobileNumber.length() > 10 || mobileNumber.length() < 10)
        {
            System.out.println("Enter valid mobile number.");
            throw new BadRequestAlertException("Please enter 10 digit mobile number!", "organisationMaster", "invalidMobileNumber");

        }
    }

    @GetMapping("/findByUserCodeId/{userCodeId}")
    public ResponseEntity<UserDetailsDTO> getUserDetailsById(@PathVariable String userCodeId) {
        log.debug("REST request to get UserDetails : {}", userCodeId);
        Optional<UserDetailsDTO> userDetailsDTO = userDetailsService.findUserDetailsByUserCodeId(userCodeId);
        return ResponseUtil.wrapOrNotFound(userDetailsDTO);
    }

}
