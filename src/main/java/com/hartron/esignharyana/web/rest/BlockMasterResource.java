package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.BlockMasterService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.BlockMasterDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.BlockMaster}.
 */
@RestController
@RequestMapping("/api")
public class BlockMasterResource {

    private final Logger log = LoggerFactory.getLogger(BlockMasterResource.class);

    private static final String ENTITY_NAME = "blockMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BlockMasterService blockMasterService;

    public BlockMasterResource(BlockMasterService blockMasterService) {
        this.blockMasterService = blockMasterService;
    }

    /**
     * {@code POST  /block-masters} : Create a new blockMaster.
     *
     * @param blockMasterDTO the blockMasterDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new blockMasterDTO, or with status {@code 400 (Bad Request)} if the blockMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/block-masters")
    public ResponseEntity<BlockMasterDTO> createBlockMaster(@Valid @RequestBody BlockMasterDTO blockMasterDTO) throws URISyntaxException {
        log.debug("REST request to save BlockMaster : {}", blockMasterDTO);
        if (blockMasterDTO.getId() != null) {
            throw new BadRequestAlertException("A new blockMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BlockMasterDTO result = blockMasterService.save(blockMasterDTO);
        return ResponseEntity.created(new URI("/api/block-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /block-masters} : Updates an existing blockMaster.
     *
     * @param blockMasterDTO the blockMasterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated blockMasterDTO,
     * or with status {@code 400 (Bad Request)} if the blockMasterDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the blockMasterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/block-masters")
    public ResponseEntity<BlockMasterDTO> updateBlockMaster(@Valid @RequestBody BlockMasterDTO blockMasterDTO) throws URISyntaxException {
        log.debug("REST request to update BlockMaster : {}", blockMasterDTO);
        if (blockMasterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BlockMasterDTO result = blockMasterService.save(blockMasterDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, blockMasterDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /block-masters} : get all the blockMasters.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of blockMasters in body.
     */
    @GetMapping("/block-masters")
    public ResponseEntity<List<BlockMasterDTO>> getAllBlockMasters(Pageable pageable) {
        log.debug("REST request to get a page of BlockMasters");
        Page<BlockMasterDTO> page = blockMasterService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /block-masters/:id} : get the "id" blockMaster.
     *
     * @param id the id of the blockMasterDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the blockMasterDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/block-masters/{id}")
    public ResponseEntity<BlockMasterDTO> getBlockMaster(@PathVariable Long id) {
        log.debug("REST request to get BlockMaster : {}", id);
        Optional<BlockMasterDTO> blockMasterDTO = blockMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(blockMasterDTO);
    }

    /**
     * {@code DELETE  /block-masters/:id} : delete the "id" blockMaster.
     *
     * @param id the id of the blockMasterDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/block-masters/{id}")
    public ResponseEntity<Void> deleteBlockMaster(@PathVariable Long id) {
        log.debug("REST request to delete BlockMaster : {}", id);
        blockMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/block-masters?query=:query} : search for the blockMaster corresponding
     * to the query.
     *
     * @param query the query of the blockMaster search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/block-masters")
    public ResponseEntity<List<BlockMasterDTO>> searchBlockMasters(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of BlockMasters for query {}", query);
        Page<BlockMasterDTO> page = blockMasterService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
