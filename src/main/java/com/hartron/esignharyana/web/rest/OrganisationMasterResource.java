package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.domain.OrganisationMaster;
import com.hartron.esignharyana.security.SecurityUtils;
import com.hartron.esignharyana.service.OrganisationMasterService;
import com.hartron.esignharyana.service.UserService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.OrganisationMasterDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.micrometer.core.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.OrganisationMaster}.
 */
@RestController
@RequestMapping("/api")
public class OrganisationMasterResource {

    private final Logger log = LoggerFactory.getLogger(OrganisationMasterResource.class);

    private static final String ENTITY_NAME = "organisationMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrganisationMasterService organisationMasterService;

    public OrganisationMasterResource(OrganisationMasterService organisationMasterService) {
        this.organisationMasterService = organisationMasterService;
    }


    /**
     * {@code POST  /organisation-masters} : Create a new organisationMaster.
     *
     * @param organisationMasterDTO the organisationMasterDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new organisationMasterDTO, or with status {@code 400 (Bad Request)} if the organisationMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/organisation-masters")
    public ResponseEntity<OrganisationMasterDTO> createOrganisationMaster(@Valid @RequestBody OrganisationMasterDTO organisationMasterDTO) throws URISyntaxException {
        log.debug("REST request to save OrganisationMaster : {}", organisationMasterDTO);
        if (organisationMasterDTO.getId() != null) {
            throw new BadRequestAlertException("A new organisationMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        isPanCardValid(organisationMasterDTO.getPan());
        isEmailValid(organisationMasterDTO.getOrganisationCorrespondenceEmail());
        isEmailValid(organisationMasterDTO.getNodalOfficerOfficialEmail());
        isValidName(organisationMasterDTO.getOrganisationNodalOfficerName());
        validAadhar(organisationMasterDTO.getAadhaar());
        validMobileNumber(organisationMasterDTO.getOrganisationNodalOfficerPhoneMobile());
        validMobileNumber(organisationMasterDTO.getOrganisationCorrespondencePhone1());
        validWebsite(organisationMasterDTO.getOrganisationWebsite());
        OrganisationMasterDTO result = organisationMasterService.save(organisationMasterDTO);
        return ResponseEntity.created(new URI("/api/organisation-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /organisation-masters} : Updates an existing organisationMaster.
     *
     * @param organisationMasterDTO the organisationMasterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated organisationMasterDTO,
     * or with status {@code 400 (Bad Request)} if the organisationMasterDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the organisationMasterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/organisation-masters")
    public ResponseEntity<OrganisationMasterDTO> updateOrganisationMaster(@Valid @RequestBody OrganisationMasterDTO organisationMasterDTO) throws URISyntaxException {
        log.debug("REST request to update OrganisationMaster : {}", organisationMasterDTO);
        if (organisationMasterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrganisationMasterDTO result = organisationMasterService.save(organisationMasterDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, organisationMasterDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /organisation-masters} : get all the organisationMasters.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of organisationMasters in body.
     */
    @GetMapping("/organisation-masters")
    public ResponseEntity<List<OrganisationMasterDTO>> getAllOrganisationMasters(Pageable pageable) {
        log.debug("REST request to get a page of OrganisationMasters");
        Page<OrganisationMasterDTO> page = organisationMasterService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /organisation-masters/:id} : get the "id" organisationMaster.
     *
     * @param id the id of the organisationMasterDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the organisationMasterDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/organisation-masters/{id}")
    public ResponseEntity<OrganisationMasterDTO> getOrganisationMaster(@PathVariable Long id) {
        log.debug("REST request to get OrganisationMaster : {}", id);
        Optional<OrganisationMasterDTO> organisationMasterDTO = organisationMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(organisationMasterDTO);
    }

    @GetMapping("/organisation-masters-By-id/{id}")
    public ResponseEntity<OrganisationMasterDTO> getOrganisationMasterById(@PathVariable Long id) {
        log.debug("REST request to get OrganisationMaster : {}", id);
        Optional<OrganisationMasterDTO> organisationMasterDTO = organisationMasterService.findOneById(id);
        return ResponseUtil.wrapOrNotFound(organisationMasterDTO);
    }

    /**
     * {@code DELETE  /organisation-masters/:id} : delete the "id" organisationMaster.
     *
     * @param id the id of the organisationMasterDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/organisation-masters/{id}")
    public ResponseEntity<Void> deleteOrganisationMaster(@PathVariable Long id) {
        log.debug("REST request to delete OrganisationMaster : {}", id);
        organisationMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/organisation-masters?query=:query} : search for the organisationMaster corresponding
     * to the query.
     *
     * @param query    the query of the organisationMaster search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/organisation-masters")
    public ResponseEntity<List<OrganisationMasterDTO>> searchOrganisationMasters(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of OrganisationMasters for query {}", query);
        Page<OrganisationMasterDTO> page = organisationMasterService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @RequestMapping(value = "organisationRecordByStatusAndVerifyBy", params = {"status", "verifyBy"}, method = RequestMethod.GET)
    @Timed
    public List<OrganisationMasterDTO> getAllRecordByStatusAndVerifyBy(@RequestParam String status, @RequestParam String verifyBy) {
        return organisationMasterService.findAllRecordByStatusAndVerifyBy(status, verifyBy);
    }

    @RequestMapping(value = "organisationRecordByStatus", params = "status", method = RequestMethod.GET)
    @Timed
    public List<OrganisationMasterDTO> getAllRecordByStatus(@RequestParam String status) {
        return organisationMasterService.findAllRecordByStatus(status);
    }


    @GetMapping("/organisationRecordByUserId")
    public ResponseEntity<OrganisationMasterDTO> getOrganisationMasterByUserId() {
        Optional<OrganisationMasterDTO> organisationMasterDTO = organisationMasterService.findOneByUserLogin(SecurityUtils.getCurrentUserLogin().get());
        return ResponseUtil.wrapOrNotFound(organisationMasterDTO);
    }

    @RequestMapping(value = "/organisationRecordByOrgIdCode", params = "oranisationIdCode", method = RequestMethod.GET)
    public List<OrganisationMasterDTO> getOrganisationMasterByOrgIdCode(@RequestParam String oranisationIdCode) {
        return organisationMasterService.findByOranisationIdCode(oranisationIdCode);
    }

    @RequestMapping(value = "/organisationsRecordByOrgIdCode", params = "oranisationIdCode", method = RequestMethod.GET)
    public Optional<OrganisationMasterDTO> getOrganisationByOrgIdCode(@RequestParam String oranisationIdCode) {
        return organisationMasterService.findOrganisationByOranisationIdCode(oranisationIdCode);
    }

    public static void isPanCardValid(String pan_number) {

        Pattern pattern = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}");

        Matcher matcher = pattern.matcher(pan_number);
        // Check if pattern matches
        if (!matcher.matches()) {

            System.out.println("Invalid Pan Number");
            throw new BadRequestAlertException("PLease Enter Valid Pan Number!", "organisationMaster", "invalidPanNumber");

        }
    }

    public void isEmailValid(String email) {

        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if(!email.matches(emailRegex)) {
            System.out.println("Invalid Email Id");
            throw new BadRequestAlertException("PLease enter valid email Id!", "organisationMaster", "invalidEmailId");
        }
    }

    public void isValidName(String name) {
        String re = "[a-zA-Z ]*$";
        if (!name.matches(re)) {
            System.out.println("Invalid Nodal Officer Name");
            throw new BadRequestAlertException("Nodal Officer Name should be alphabetic!", "organisationMaster", "invalidNodalOfficerName");
        }
    }

    public void validAadhar(String aadhar) {
        if (aadhar.length() > 4 || aadhar.length() < 4)
        {
            System.out.println("Aadhar Number should be 4 digit.");
            throw new BadRequestAlertException("Aadhar Number should be 4 digit!", "organisationMaster", "invalidAadhar");

        }
    }

    public void validMobileNumber(String mobileNumber) {
        if (mobileNumber.length() > 10 || mobileNumber.length() < 10)
        {
            System.out.println("Enter valid mobile number.");
            throw new BadRequestAlertException("Please enter 10 digit mobile number!", "organisationMaster", "invalidMobileNumber");

        }
    }

    public void validWebsite(String website) {
        Pattern p = Pattern.compile("(http://|https://)(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?");
        Matcher m;
        m=p.matcher(website);
        if (!m.matches()) {
            System.out.println("Enter valid website url");
            throw new BadRequestAlertException("Please enter valid website url", "organisationMaster", "invalidWebsiteUrl");

        }
    }

}
