package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.SubDistrictMasterService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.SubDistrictMasterDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.SubDistrictMaster}.
 */
@RestController
@RequestMapping("/api")
public class SubDistrictMasterResource {

    private final Logger log = LoggerFactory.getLogger(SubDistrictMasterResource.class);

    private static final String ENTITY_NAME = "subDistrictMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SubDistrictMasterService subDistrictMasterService;

    public SubDistrictMasterResource(SubDistrictMasterService subDistrictMasterService) {
        this.subDistrictMasterService = subDistrictMasterService;
    }

    /**
     * {@code POST  /sub-district-masters} : Create a new subDistrictMaster.
     *
     * @param subDistrictMasterDTO the subDistrictMasterDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new subDistrictMasterDTO, or with status {@code 400 (Bad Request)} if the subDistrictMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sub-district-masters")
    public ResponseEntity<SubDistrictMasterDTO> createSubDistrictMaster(@Valid @RequestBody SubDistrictMasterDTO subDistrictMasterDTO) throws URISyntaxException {
        log.debug("REST request to save SubDistrictMaster : {}", subDistrictMasterDTO);
        if (subDistrictMasterDTO.getId() != null) {
            throw new BadRequestAlertException("A new subDistrictMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubDistrictMasterDTO result = subDistrictMasterService.save(subDistrictMasterDTO);
        return ResponseEntity.created(new URI("/api/sub-district-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /sub-district-masters} : Updates an existing subDistrictMaster.
     *
     * @param subDistrictMasterDTO the subDistrictMasterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated subDistrictMasterDTO,
     * or with status {@code 400 (Bad Request)} if the subDistrictMasterDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the subDistrictMasterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/sub-district-masters")
    public ResponseEntity<SubDistrictMasterDTO> updateSubDistrictMaster(@Valid @RequestBody SubDistrictMasterDTO subDistrictMasterDTO) throws URISyntaxException {
        log.debug("REST request to update SubDistrictMaster : {}", subDistrictMasterDTO);
        if (subDistrictMasterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SubDistrictMasterDTO result = subDistrictMasterService.save(subDistrictMasterDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, subDistrictMasterDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /sub-district-masters} : get all the subDistrictMasters.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of subDistrictMasters in body.
     */
    @GetMapping("/sub-district-masters")
    public ResponseEntity<List<SubDistrictMasterDTO>> getAllSubDistrictMasters(Pageable pageable) {
        log.debug("REST request to get a page of SubDistrictMasters");
        Page<SubDistrictMasterDTO> page = subDistrictMasterService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /sub-district-masters/:id} : get the "id" subDistrictMaster.
     *
     * @param id the id of the subDistrictMasterDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the subDistrictMasterDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sub-district-masters/{id}")
    public ResponseEntity<SubDistrictMasterDTO> getSubDistrictMaster(@PathVariable Long id) {
        log.debug("REST request to get SubDistrictMaster : {}", id);
        Optional<SubDistrictMasterDTO> subDistrictMasterDTO = subDistrictMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(subDistrictMasterDTO);
    }

    /**
     * {@code DELETE  /sub-district-masters/:id} : delete the "id" subDistrictMaster.
     *
     * @param id the id of the subDistrictMasterDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sub-district-masters/{id}")
    public ResponseEntity<Void> deleteSubDistrictMaster(@PathVariable Long id) {
        log.debug("REST request to delete SubDistrictMaster : {}", id);
        subDistrictMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/sub-district-masters?query=:query} : search for the subDistrictMaster corresponding
     * to the query.
     *
     * @param query the query of the subDistrictMaster search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/sub-district-masters")
    public ResponseEntity<List<SubDistrictMasterDTO>> searchSubDistrictMasters(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of SubDistrictMasters for query {}", query);
        Page<SubDistrictMasterDTO> page = subDistrictMasterService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
