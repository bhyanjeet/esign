package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.ApplicationLogsService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.ApplicationLogsDTO;
import com.hartron.esignharyana.service.dto.ApplicationLogsCriteria;
import com.hartron.esignharyana.service.ApplicationLogsQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.ApplicationLogs}.
 */
@RestController
@RequestMapping("/api")
public class ApplicationLogsResource {

    private final Logger log = LoggerFactory.getLogger(ApplicationLogsResource.class);

    private static final String ENTITY_NAME = "applicationLogs";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ApplicationLogsService applicationLogsService;

    private final ApplicationLogsQueryService applicationLogsQueryService;

    public ApplicationLogsResource(ApplicationLogsService applicationLogsService, ApplicationLogsQueryService applicationLogsQueryService) {
        this.applicationLogsService = applicationLogsService;
        this.applicationLogsQueryService = applicationLogsQueryService;
    }

    /**
     * {@code POST  /application-logs} : Create a new applicationLogs.
     *
     * @param applicationLogsDTO the applicationLogsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new applicationLogsDTO, or with status {@code 400 (Bad Request)} if the applicationLogs has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/application-logs")
    public ResponseEntity<ApplicationLogsDTO> createApplicationLogs(@RequestBody ApplicationLogsDTO applicationLogsDTO) throws URISyntaxException {
        log.debug("REST request to save ApplicationLogs : {}", applicationLogsDTO);
        if (applicationLogsDTO.getId() != null) {
            throw new BadRequestAlertException("A new applicationLogs cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ApplicationLogsDTO result = applicationLogsService.save(applicationLogsDTO);
        return ResponseEntity.created(new URI("/api/application-logs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /application-logs} : Updates an existing applicationLogs.
     *
     * @param applicationLogsDTO the applicationLogsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated applicationLogsDTO,
     * or with status {@code 400 (Bad Request)} if the applicationLogsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the applicationLogsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/application-logs")
    public ResponseEntity<ApplicationLogsDTO> updateApplicationLogs(@RequestBody ApplicationLogsDTO applicationLogsDTO) throws URISyntaxException {
        log.debug("REST request to update ApplicationLogs : {}", applicationLogsDTO);
        if (applicationLogsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ApplicationLogsDTO result = applicationLogsService.save(applicationLogsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, applicationLogsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /application-logs} : get all the applicationLogs.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of applicationLogs in body.
     */
    @GetMapping("/application-logs")
    public ResponseEntity<List<ApplicationLogsDTO>> getAllApplicationLogs(ApplicationLogsCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ApplicationLogs by criteria: {}", criteria);
        Page<ApplicationLogsDTO> page = applicationLogsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /application-logs/count} : count all the applicationLogs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/application-logs/count")
    public ResponseEntity<Long> countApplicationLogs(ApplicationLogsCriteria criteria) {
        log.debug("REST request to count ApplicationLogs by criteria: {}", criteria);
        return ResponseEntity.ok().body(applicationLogsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /application-logs/:id} : get the "id" applicationLogs.
     *
     * @param id the id of the applicationLogsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the applicationLogsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/application-logs/{id}")
    public ResponseEntity<ApplicationLogsDTO> getApplicationLogs(@PathVariable Long id) {
        log.debug("REST request to get ApplicationLogs : {}", id);
        Optional<ApplicationLogsDTO> applicationLogsDTO = applicationLogsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(applicationLogsDTO);
    }

    /**
     * {@code DELETE  /application-logs/:id} : delete the "id" applicationLogs.
     *
     * @param id the id of the applicationLogsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/application-logs/{id}")
    public ResponseEntity<Void> deleteApplicationLogs(@PathVariable Long id) {
        log.debug("REST request to delete ApplicationLogs : {}", id);
        applicationLogsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/application-logs?query=:query} : search for the applicationLogs corresponding
     * to the query.
     *
     * @param query the query of the applicationLogs search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/application-logs")
    public ResponseEntity<List<ApplicationLogsDTO>> searchApplicationLogs(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ApplicationLogs for query {}", query);
        Page<ApplicationLogsDTO> page = applicationLogsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
