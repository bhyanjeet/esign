package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.EsignTransationQueryService;
import com.hartron.esignharyana.service.EsignTransationService;
import com.hartron.esignharyana.service.dto.EsignTransationCriteria;
import com.hartron.esignharyana.service.dto.EsignTransationDTO;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.EsignTransation}.
 */
@RestController
@RequestMapping("/api")
public class EsignTransationResource {

    private final Logger log = LoggerFactory.getLogger(EsignTransationResource.class);

    private static final String ENTITY_NAME = "esignTransation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EsignTransationService esignTransationService;

    private final EsignTransationQueryService esignTransationQueryService;

    public EsignTransationResource(EsignTransationService esignTransationService, EsignTransationQueryService esignTransationQueryService) {
        this.esignTransationService = esignTransationService;
        this.esignTransationQueryService = esignTransationQueryService;
    }

    /**
     * {@code POST  /esign-transations} : Create a new esignTransation.
     *
     * @param esignTransationDTO the esignTransationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new esignTransationDTO, or with status {@code 400 (Bad Request)} if the esignTransation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/esign-transations")
    public ResponseEntity<EsignTransationDTO> createEsignTransation(@RequestBody EsignTransationDTO esignTransationDTO) throws URISyntaxException {
        log.debug("REST request to save EsignTransation : {}", esignTransationDTO);
        if (esignTransationDTO.getId() != null) {
            throw new BadRequestAlertException("A new esignTransation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EsignTransationDTO result = esignTransationService.save(esignTransationDTO);
        return ResponseEntity.created(new URI("/api/esign-transations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /esign-transations} : Updates an existing esignTransation.
     *
     * @param esignTransationDTO the esignTransationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated esignTransationDTO,
     * or with status {@code 400 (Bad Request)} if the esignTransationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the esignTransationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/esign-transations")
    public ResponseEntity<EsignTransationDTO> updateEsignTransation(@RequestBody EsignTransationDTO esignTransationDTO) throws URISyntaxException {
        log.debug("REST request to update EsignTransation : {}", esignTransationDTO);
        if (esignTransationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EsignTransationDTO result = esignTransationService.save(esignTransationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, esignTransationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /esign-transations} : get all the esignTransations.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of esignTransations in body.
     */
    @GetMapping("/esign-transations")
    public ResponseEntity<List<EsignTransationDTO>> getAllEsignTransations(EsignTransationCriteria criteria, Pageable pageable) {
        log.debug("REST request to get EsignTransations by criteria: {}", criteria);
        Page<EsignTransationDTO> page = esignTransationQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /esign-transations/count} : count all the esignTransations.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/esign-transations/count")
    public ResponseEntity<Long> countEsignTransations(EsignTransationCriteria criteria) {
        log.debug("REST request to count EsignTransations by criteria: {}", criteria);
        return ResponseEntity.ok().body(esignTransationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /esign-transations/:id} : get the "id" esignTransation.
     *
     * @param id the id of the esignTransationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the esignTransationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/esign-transations/{id}")
    public ResponseEntity<EsignTransationDTO> getEsignTransation(@PathVariable Long id) {
        log.debug("REST request to get EsignTransation : {}", id);
        Optional<EsignTransationDTO> esignTransationDTO = esignTransationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(esignTransationDTO);
    }

    /**
     * {@code DELETE  /esign-transations/:id} : delete the "id" esignTransation.
     *
     * @param id the id of the esignTransationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/esign-transations/{id}")
    public ResponseEntity<Void> deleteEsignTransation(@PathVariable Long id) {
        log.debug("REST request to delete EsignTransation : {}", id);
        esignTransationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/esign-transations?query=:query} : search for the esignTransation corresponding
     * to the query.
     *
     * @param query the query of the esignTransation search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/esign-transations")
    public ResponseEntity<List<EsignTransationDTO>> searchEsignTransations(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of EsignTransations for query {}", query);
        Page<EsignTransationDTO> page = esignTransationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @PostMapping("/saveEsignTransaction")
    public EsignTransationDTO saveEsignTransation(@RequestBody EsignTransationDTO esignTransationDTO) throws URISyntaxException, ParserConfigurationException, SAXException, IOException {

        EsignTransationDTO result = esignTransationService.saveTransaction(esignTransationDTO);

        return result;
    }
}
