package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.UserAuthDetailsService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.UserAuthDetailsDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.UserAuthDetails}.
 */
@RestController
@RequestMapping("/api")
public class UserAuthDetailsResource {

    private final Logger log = LoggerFactory.getLogger(UserAuthDetailsResource.class);

    private static final String ENTITY_NAME = "userAuthDetails";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserAuthDetailsService userAuthDetailsService;

    public UserAuthDetailsResource(UserAuthDetailsService userAuthDetailsService) {
        this.userAuthDetailsService = userAuthDetailsService;
    }

    /**
     * {@code POST  /user-auth-details} : Create a new userAuthDetails.
     *
     * @param userAuthDetailsDTO the userAuthDetailsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userAuthDetailsDTO, or with status {@code 400 (Bad Request)} if the userAuthDetails has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-auth-details")
    public ResponseEntity<UserAuthDetailsDTO> createUserAuthDetails(@RequestBody UserAuthDetailsDTO userAuthDetailsDTO) throws URISyntaxException {
        log.debug("REST request to save UserAuthDetails : {}", userAuthDetailsDTO);
        if (userAuthDetailsDTO.getId() != null) {
            throw new BadRequestAlertException("A new userAuthDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserAuthDetailsDTO result = userAuthDetailsService.save(userAuthDetailsDTO);
        return ResponseEntity.created(new URI("/api/user-auth-details/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-auth-details} : Updates an existing userAuthDetails.
     *
     * @param userAuthDetailsDTO the userAuthDetailsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userAuthDetailsDTO,
     * or with status {@code 400 (Bad Request)} if the userAuthDetailsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userAuthDetailsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-auth-details")
    public ResponseEntity<UserAuthDetailsDTO> updateUserAuthDetails(@RequestBody UserAuthDetailsDTO userAuthDetailsDTO) throws URISyntaxException {
        log.debug("REST request to update UserAuthDetails : {}", userAuthDetailsDTO);
        if (userAuthDetailsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserAuthDetailsDTO result = userAuthDetailsService.save(userAuthDetailsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, userAuthDetailsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-auth-details} : get all the userAuthDetails.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userAuthDetails in body.
     */
    @GetMapping("/user-auth-details")
    public List<UserAuthDetailsDTO> getAllUserAuthDetails() {
        log.debug("REST request to get all UserAuthDetails");
        return userAuthDetailsService.findAll();
    }

    /**
     * {@code GET  /user-auth-details/:id} : get the "id" userAuthDetails.
     *
     * @param id the id of the userAuthDetailsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userAuthDetailsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-auth-details/{id}")
    public ResponseEntity<UserAuthDetailsDTO> getUserAuthDetails(@PathVariable Long id) {
        log.debug("REST request to get UserAuthDetails : {}", id);
        Optional<UserAuthDetailsDTO> userAuthDetailsDTO = userAuthDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userAuthDetailsDTO);
    }

    /**
     * {@code DELETE  /user-auth-details/:id} : delete the "id" userAuthDetails.
     *
     * @param id the id of the userAuthDetailsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-auth-details/{id}")
    public ResponseEntity<Void> deleteUserAuthDetails(@PathVariable Long id) {
        log.debug("REST request to delete UserAuthDetails : {}", id);
        userAuthDetailsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/user-auth-details?query=:query} : search for the userAuthDetails corresponding
     * to the query.
     *
     * @param query the query of the userAuthDetails search.
     * @return the result of the search.
     */
    @GetMapping("/_search/user-auth-details")
    public List<UserAuthDetailsDTO> searchUserAuthDetails(@RequestParam String query) {
        log.debug("REST request to search UserAuthDetails for query {}", query);
        return userAuthDetailsService.search(query);
    }
}
