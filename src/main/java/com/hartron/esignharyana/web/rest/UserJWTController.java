package com.hartron.esignharyana.web.rest;
import com.hartron.esignharyana.domain.UserAuthDetails;
import com.hartron.esignharyana.config.AesUtilHelper;
import com.hartron.esignharyana.security.jwt.JWTFilter;
import com.hartron.esignharyana.security.jwt.TokenProvider;
import com.hartron.esignharyana.service.UserAuthDetailsService;
import com.hartron.esignharyana.service.mapper.UserAuthDetailsMapper;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.web.rest.vm.LoginVM;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

    private final TokenProvider tokenProvider;

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    private final UserAuthDetailsService userAuthDetailsService;

    private final UserAuthDetailsMapper userAuthDetailsMapper;

    public UserJWTController(TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder, UserAuthDetailsService userAuthDetailsService, UserAuthDetailsMapper userAuthDetailsMapper) {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.userAuthDetailsService = userAuthDetailsService;
        this.userAuthDetailsMapper = userAuthDetailsMapper;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM, HttpServletRequest request) {
        isValidName(loginVM.getUsername());
        final String secretKey = "6D24456D6640616362676259646E4578";
        String originalString = loginVM.getPassword();
        String decryptedString = AesUtilHelper.decrypt(originalString, secretKey);

        loginVM.setPassword(decryptedString);

        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
        String jwt = tokenProvider.createToken(authentication, rememberMe);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        UserAuthDetails userAuthDetails = new UserAuthDetails();
        userAuthDetails.setAuthToken(jwt);
        userAuthDetails.setDevice(request.getHeader("User-Agent"));
        userAuthDetails.setLogin(loginVM.getUsername());
        userAuthDetailsService.save(userAuthDetailsMapper.toDto(userAuthDetails));
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }
    @PostMapping("/logout")
    @Timed
    public void logout(HttpServletRequest request) {
        String header_authorization = request.getHeader("Authorization");
        String token = (StringUtils.isEmpty(header_authorization) ? null : header_authorization.split(" ")[1]);
        if(token != null) {
            List<UserAuthDetails> userAuthDetails = userAuthDetailsService.getAllByToken(token);
            if(userAuthDetails.size() > 0) {
                userAuthDetailsService.delete(userAuthDetails.get(0).getId());
            }
        }
    }
    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }

    public void isValidName(String name) {
        String re = "[a-zA-Z0-9 ]*$";
        if (!name.matches(re)) {
            System.out.println("Invalid Nodal Officer Name");
            throw new BadRequestAlertException("Login should be alphabetic or numeric!", "users", "invalidNodalOfficerName");
        }
    }
}
