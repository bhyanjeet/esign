package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.*;
import com.hartron.esignharyana.service.dto.ExternalUserEsignRequestDTO;
import com.itextpdf.text.DocumentException;
import io.micrometer.core.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

//import static jdk.nashorn.internal.parser.TokenType.XML;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.ApplicationMaster}.
 */
@RestController
@RequestMapping("/api")
public class eSignUserRequestResource {

    private final Logger log = LoggerFactory.getLogger(eSignUserRequestResource.class);

    private static final String ENTITY_NAME = "applicationMaster";

    private final UserApplicationService userApplicationService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ApplicationMasterService applicationMasterService;

    private final UserDetailsService userDetailsService;

    private final eSignService eSignService;

    private final EsignDemoService esignDemoService;

    private final EsignUserRequestService esignUserRequestService;

    public eSignUserRequestResource(UserApplicationService userApplicationService, ApplicationMasterService applicationMasterService, UserDetailsService userDetailsService, eSignService createeSignXMLgnService, com.hartron.esignharyana.service.eSignService eSignService, EsignDemoService esignDemoService, EsignUserRequestService esignUserRequestService) {
        this.userApplicationService = userApplicationService;
        this.applicationMasterService = applicationMasterService;
        this.userDetailsService = userDetailsService;
        this.eSignService = eSignService;
        this.esignDemoService = esignDemoService;

        this.esignUserRequestService = esignUserRequestService;
    }

    @PostMapping("/dept/esignrequest")
    @Timed
    public void verifyUser(@RequestBody String useresignrequest, HttpServletRequest request, HttpServletResponse response) throws Exception {


        System.out.println("requestttttttttttttttt" + useresignrequest);


         String[] splitparams =  useresignrequest.split("&");

         String req_data1 = splitparams[0];
         String[] split_req_data1 = req_data1.split("=");
         String req_data2 = split_req_data1[1];
         String req_data3 =  req_data2.replaceAll("%2B", "+");
         String req_data =  req_data3.replaceAll("%3D", "=");

         System.out.println("rrrrrrrrrr" + req_data);

      //   String req_data = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48RXNpZ24gYXNwSWQ9IkNTT1JHMTAwMDAwMiIgbWF4V2FpdFBlcmlvZD0iMTQ0MCIgcmVkaXJlY3RVcmw9Imh0dHA6Ly8xMTcuMjU1LjIxNi4xNjIvcmVkaXJlY3RVcmwiIHJlc3BvbnNlVXJsPSJodHRwOi8vMTE3LjI1NS4yMTYuMTYyL3Jlc3BvbnNlVXJsIiBzaWduZXJpZD0iOTg3NzcwNDUyOUBtLmNzYyIgc2lnbmluZ0FsZ29yaXRobT0iUlNBIiB0cz0iMjAyMC0wMi0wM1QxNToyODoxNi43ODYiIHR4bj0iMjAyMDIyMDI1NDIzMjc3MDgiIHZlcj0iMy4yIj48RG9jcz48SW5wdXRIYXNoIGRvY0luZm89InRoaXMgaXMgdXNpbmcgdG8gZ2l2ZSBkb2N1bWVudCBpbmZvcm1hdGlvbiIgZG9jVXJsPSJodHRwOi8vaHR3Yi5oYXJ0cm9uLmlvL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE4LzA2L0JvYXJkLU5vdGlmaWNhdGlvbi5wZGYiIGhhc2hBbGdvcml0aG09IlNIQTI1NiIgaWQ9IjEiIHJlc3BvbnNlU2lnVHlwZT0icGtjczciPmE2NzI4ZTFhYzcxMjFjYzQwM2NmOWQwZWJlMDk4N2Q5ZDZkNDAyMmMwZDhkNzg4NTFkNDQ4NjE3NzJhNDI1OWY8L0lucHV0SGFzaD48L0RvY3M+PFNpZ25hdHVyZSB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+PFNpZ25lZEluZm8+PENhbm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxkc2lnLW1vcmUjcnNhLXNoYTI1NiIvPjxSZWZlcmVuY2UgVVJJPSIiPjxUcmFuc2Zvcm1zPjxUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPjwvVHJhbnNmb3Jtcz48RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxlbmMjc2hhMjU2Ii8+PERpZ2VzdFZhbHVlPmNjVXBFVEhaTFpMdGZJajM3QUpFeWdrbkZmN1o0dlU5WlVid21NTHJuZWM9PC9EaWdlc3RWYWx1ZT48L1JlZmVyZW5jZT48L1NpZ25lZEluZm8+PFNpZ25hdHVyZVZhbHVlPnhaeGlLRmJlQzlyaXRhamErcytQR2VuMzNOSmg0RWJobEdUbGJQQUV0d3RCNzY1YmlXbXZRQUdJT05xM254RHBTUGtLcG5Wa2tMSjgNCk52am5Vczh0YzdwNFhVcHVxMys3enBBbzkyZVd3TitTdllmVXBtdEI1cTJmQmhJeHJKVVlNdFBxNXpLMFF6NytTamdKSTlyczB6RjUNClhnNW55M3JkL1pFanF3RkM3TU8wZ0xnVUtiWm11L09VZXlsMG9pWnUveFB6Y1d3M3NiaEhZUXF2UUVzaWdrdDFwN1dFWk85aS9Pc1ANCjBETnRBbXorVlB5YVhOVCtZNXlpSXhIekZHQmk3a09FczV4TDZHWUNTcUR1bDZPb3VOdlYyZkhHdHF4bC9ubG1SdGNtdEpncHl4cXINCklBTnlWWEg3WmV0S0VPY1p6R0UvS2RDZEhZRURKbkdzczI5d1NnPT08L1NpZ25hdHVyZVZhbHVlPjwvU2lnbmF0dXJlPjwvRXNpZ24+";

         String responseUrl1 = splitparams[1];
        String[] split_responseUrl1 = responseUrl1.split("=");
        String responseUrl = "http://stagingesign.hartron.org.in/api/sign/response";


         String txn1 = splitparams[2];
        String[] split_txn1 = txn1.split("=");
        String txn = split_txn1[1];

         String applicationIdCode1 = splitparams[3];
        String[] split_applicationIdCode1 = applicationIdCode1.split("=");
        String applicationIdCode = split_applicationIdCode1[1];

         String userCodeId1 = splitparams[4];
        String[] split_userCodeId1 = userCodeId1.split("=");
        String userCodeId = split_userCodeId1[1];

        String txn_ref = esignUserRequestService.sendEsignRequestToCSC(userCodeId, applicationIdCode, responseUrl, txn, req_data, request, response);



    }

    @PostMapping(value ="/userEsignRequest")
    public ExternalUserEsignRequestDTO userEsignRequest(@Valid @RequestBody ExternalUserEsignRequestDTO externalUserEsignRequestDTO) throws Exception {


        externalUserEsignRequestDTO  =  esignDemoService.saveEsignRequest(externalUserEsignRequestDTO);

        return  externalUserEsignRequestDTO;
    }

//    @PostMapping(value ="/userEsignRequest", method = RequestMethod.GET)
//    public void userEsignRequest(@RequestParam("userCodeId")String userCodeId,@RequestParam("applicationIdCode")String applicationIdCode,@RequestParam("responseUrl")String responseUrl,@RequestParam("docHash")String docHash, HttpServletRequest request, HttpServletResponse response) throws Exception {
//
//        String txn_ref = "";
//
//        //save user request
//
//       // esignUserRequestService.sendEsignRequestToCSC(userCodeId, applicationIdCode, docHash, request, response);
//        esignUserRequestService.saveEsignRequest(userCodeId, applicationIdCode, responseUrl, docHash, request, response);
//
//        //
//        String url = "http://localhost:9001/esign-final-form-request";
//        String reqParameter = "txn_ref" + txn_ref;
//        response.sendRedirect(url +"/"+ reqParameter);
//
////        String requestStatus = esignUserRequestService.checkEsignRequest(userCodeId, applicationCodeId, response, request);
////
////        if (requestStatus.equals("dfgdsfg")){
////            esignUserRequestService.sendEsignRequestToCSC(userCodeId, applicationCodeId, responseUrl, txn, req_data, request, response);
////        }
//    }






    @GetMapping("/findByTxn")
    public ExternalUserEsignRequestDTO getExternalUserByTxn(@RequestParam String Txn) throws ParserConfigurationException, TransformerException, NoSuchAlgorithmException, SAXException, IOException, DocumentException, XMLSignatureException, MarshalException, KeyStoreException, InvalidAlgorithmParameterException, CertificateException, UnrecoverableEntryException {
     ExternalUserEsignRequestDTO  externalUserEsignRequestDTO =  esignUserRequestService.findByTxn(Txn);
    return externalUserEsignRequestDTO;
    }

    @PostMapping(value ="sendRedirectCheck")
    public String  RedirectCheck(HttpServletResponse response) throws Exception {

        String url = "http://stagingesign.hartron.org.in/asp-dashboard";

        response.sendRedirect(url);
        return  url;
    }

    @GetMapping("/verifySignaure")
    public boolean verifySignature(@RequestParam String xml) throws ParserConfigurationException, TransformerException, NoSuchAlgorithmException, SAXException, IOException, DocumentException, XMLSignatureException, MarshalException, KeyStoreException, InvalidAlgorithmParameterException, CertificateException, UnrecoverableEntryException {
        boolean   verifySignature =  esignUserRequestService.verifySign(xml);
        return verifySignature;
    }



}







