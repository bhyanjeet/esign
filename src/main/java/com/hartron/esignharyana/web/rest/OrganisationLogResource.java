package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.OrganisationLogService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.OrganisationLogDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.OrganisationLog}.
 */
@RestController
@RequestMapping("/api")
public class OrganisationLogResource {

    private final Logger log = LoggerFactory.getLogger(OrganisationLogResource.class);

    private static final String ENTITY_NAME = "organisationLog";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrganisationLogService organisationLogService;

    public OrganisationLogResource(OrganisationLogService organisationLogService) {
        this.organisationLogService = organisationLogService;
    }

    /**
     * {@code POST  /organisation-logs} : Create a new organisationLog.
     *
     * @param organisationLogDTO the organisationLogDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new organisationLogDTO, or with status {@code 400 (Bad Request)} if the organisationLog has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/organisation-logs")
    public ResponseEntity<OrganisationLogDTO> createOrganisationLog(@RequestBody OrganisationLogDTO organisationLogDTO) throws URISyntaxException {
        log.debug("REST request to save OrganisationLog : {}", organisationLogDTO);
        if (organisationLogDTO.getId() != null) {
            throw new BadRequestAlertException("A new organisationLog cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrganisationLogDTO result = organisationLogService.save(organisationLogDTO);
        return ResponseEntity.created(new URI("/api/organisation-logs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /organisation-logs} : Updates an existing organisationLog.
     *
     * @param organisationLogDTO the organisationLogDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated organisationLogDTO,
     * or with status {@code 400 (Bad Request)} if the organisationLogDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the organisationLogDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/organisation-logs")
    public ResponseEntity<OrganisationLogDTO> updateOrganisationLog(@RequestBody OrganisationLogDTO organisationLogDTO) throws URISyntaxException {
        log.debug("REST request to update OrganisationLog : {}", organisationLogDTO);
        if (organisationLogDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrganisationLogDTO result = organisationLogService.save(organisationLogDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, organisationLogDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /organisation-logs} : get all the organisationLogs.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of organisationLogs in body.
     */
    @GetMapping("/organisation-logs")
    public List<OrganisationLogDTO> getAllOrganisationLogs() {
        log.debug("REST request to get all OrganisationLogs");
        return organisationLogService.findAll();
    }

    /**
     * {@code GET  /organisation-logs/:id} : get the "id" organisationLog.
     *
     * @param id the id of the organisationLogDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the organisationLogDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/organisation-logs/{id}")
    public ResponseEntity<OrganisationLogDTO> getOrganisationLog(@PathVariable Long id) {
        log.debug("REST request to get OrganisationLog : {}", id);
        Optional<OrganisationLogDTO> organisationLogDTO = organisationLogService.findOne(id);
        return ResponseUtil.wrapOrNotFound(organisationLogDTO);
    }

    /**
     * {@code DELETE  /organisation-logs/:id} : delete the "id" organisationLog.
     *
     * @param id the id of the organisationLogDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/organisation-logs/{id}")
    public ResponseEntity<Void> deleteOrganisationLog(@PathVariable Long id) {
        log.debug("REST request to delete OrganisationLog : {}", id);
        organisationLogService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/organisation-logs?query=:query} : search for the organisationLog corresponding
     * to the query.
     *
     * @param query the query of the organisationLog search.
     * @return the result of the search.
     */
    @GetMapping("/_search/organisation-logs")
    public List<OrganisationLogDTO> searchOrganisationLogs(@RequestParam String query) {
        log.debug("REST request to search OrganisationLogs for query {}", query);
        return organisationLogService.search(query);
    }
}
