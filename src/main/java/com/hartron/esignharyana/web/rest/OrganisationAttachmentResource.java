package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.OrganisationAttachmentService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.OrganisationAttachmentDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.micrometer.core.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.OrganisationAttachment}.
 */
@RestController
@RequestMapping("/api")
public class OrganisationAttachmentResource {

    private final Logger log = LoggerFactory.getLogger(OrganisationAttachmentResource.class);

    private static final String ENTITY_NAME = "organisationAttachment";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrganisationAttachmentService organisationAttachmentService;

    public OrganisationAttachmentResource(OrganisationAttachmentService organisationAttachmentService) {
        this.organisationAttachmentService = organisationAttachmentService;
    }

    /**
     * {@code POST  /organisation-attachments} : Create a new organisationAttachment.
     *
     * @param organisationAttachmentDTO the organisationAttachmentDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new organisationAttachmentDTO, or with status {@code 400 (Bad Request)} if the organisationAttachment has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/organisation-attachments")
    public ResponseEntity<OrganisationAttachmentDTO> createOrganisationAttachment(@RequestBody OrganisationAttachmentDTO organisationAttachmentDTO) throws URISyntaxException {
        log.debug("REST request to save OrganisationAttachment : {}", organisationAttachmentDTO);
        if (organisationAttachmentDTO.getId() != null) {
            throw new BadRequestAlertException("A new organisationAttachment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrganisationAttachmentDTO result = organisationAttachmentService.save(organisationAttachmentDTO);
        return ResponseEntity.created(new URI("/api/organisation-attachments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /organisation-attachments} : Updates an existing organisationAttachment.
     *
     * @param organisationAttachmentDTO the organisationAttachmentDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated organisationAttachmentDTO,
     * or with status {@code 400 (Bad Request)} if the organisationAttachmentDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the organisationAttachmentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/organisation-attachments")
    public ResponseEntity<OrganisationAttachmentDTO> updateOrganisationAttachment(@RequestBody OrganisationAttachmentDTO organisationAttachmentDTO) throws URISyntaxException {
        log.debug("REST request to update OrganisationAttachment : {}", organisationAttachmentDTO);
        if (organisationAttachmentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrganisationAttachmentDTO result = organisationAttachmentService.save(organisationAttachmentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, organisationAttachmentDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /organisation-attachments} : get all the organisationAttachments.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of organisationAttachments in body.
     */
    @GetMapping("/organisation-attachments")
    public ResponseEntity<List<OrganisationAttachmentDTO>> getAllOrganisationAttachments(Pageable pageable) {
        log.debug("REST request to get a page of OrganisationAttachments");
        Page<OrganisationAttachmentDTO> page = organisationAttachmentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /organisation-attachments/:id} : get the "id" organisationAttachment.
     *
     * @param id the id of the organisationAttachmentDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the organisationAttachmentDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/organisation-attachments/{id}")
    public ResponseEntity<OrganisationAttachmentDTO> getOrganisationAttachment(@PathVariable Long id) {
        log.debug("REST request to get OrganisationAttachment : {}", id);
        Optional<OrganisationAttachmentDTO> organisationAttachmentDTO = organisationAttachmentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(organisationAttachmentDTO);
    }

    /**
     * {@code DELETE  /organisation-attachments/:id} : delete the "id" organisationAttachment.
     *
     * @param id the id of the organisationAttachmentDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/organisation-attachments/{id}")
    public ResponseEntity<Void> deleteOrganisationAttachment(@PathVariable Long id) {
        log.debug("REST request to delete OrganisationAttachment : {}", id);
        organisationAttachmentService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/organisation-attachments?query=:query} : search for the organisationAttachment corresponding
     * to the query.
     *
     * @param query the query of the organisationAttachment search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/organisation-attachments")
    public ResponseEntity<List<OrganisationAttachmentDTO>> searchOrganisationAttachments(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of OrganisationAttachments for query {}", query);
        Page<OrganisationAttachmentDTO> page = organisationAttachmentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @RequestMapping(value = "organisationAttachmentByOrganisationMaster", params = "id", method = RequestMethod.GET)
    @Timed
    public List<OrganisationAttachmentDTO> getAttachmentsByOrgisastionTypeId(@RequestParam Long id) {
        log.debug("REST request to get all Organisation Documents");
        return organisationAttachmentService.findAllByOrganisation(id);
    }
}

