package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.repository.OrganisationMasterRepository;
import com.hartron.esignharyana.repository.search.OrganisationMasterSearchRepository;
import com.hartron.esignharyana.security.SecurityUtils;
import com.hartron.esignharyana.service.OrganisationTypeMasterService;
import com.hartron.esignharyana.service.dto.OrganisationMasterDTO;
import com.hartron.esignharyana.service.mapper.OrganisationMasterMapper;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.OrganisationTypeMasterDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.OrganisationTypeMaster}.
 */
@RestController
@RequestMapping("/api")
public class OrganisationTypeMasterResource {

    private final Logger log = LoggerFactory.getLogger(OrganisationTypeMasterResource.class);

    private static final String ENTITY_NAME = "organisationTypeMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrganisationTypeMasterService organisationTypeMasterService;

    private final OrganisationMasterSearchRepository organisationMasterSearchRepository;

    private final OrganisationMasterMapper organisationMasterMapper;

    public OrganisationTypeMasterResource(OrganisationTypeMasterService organisationTypeMasterService, OrganisationMasterRepository organisationMasterRepository, OrganisationMasterSearchRepository organisationMasterSearchRepository, OrganisationMasterMapper organisationMasterMapper) {
        this.organisationTypeMasterService = organisationTypeMasterService;
        this.organisationMasterSearchRepository = organisationMasterSearchRepository;
        this.organisationMasterMapper = organisationMasterMapper;
    }

    /**
     * {@code POST  /organisation-type-masters} : Create a new organisationTypeMaster.
     *
     * @param organisationTypeMasterDTO the organisationTypeMasterDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new organisationTypeMasterDTO, or with status {@code 400 (Bad Request)} if the organisationTypeMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/organisation-type-masters")
    public ResponseEntity<OrganisationTypeMasterDTO> createOrganisationTypeMaster(@Valid @RequestBody OrganisationTypeMasterDTO organisationTypeMasterDTO) throws URISyntaxException {
        log.debug("REST request to save OrganisationTypeMaster : {}", organisationTypeMasterDTO);
        if (organisationTypeMasterDTO.getId() != null) {
            throw new BadRequestAlertException("A new organisationTypeMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrganisationTypeMasterDTO result = organisationTypeMasterService.save(organisationTypeMasterDTO);
        return ResponseEntity.created(new URI("/api/organisation-type-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /organisation-type-masters} : Updates an existing organisationTypeMaster.
     *
     * @param organisationTypeMasterDTO the organisationTypeMasterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated organisationTypeMasterDTO,
     * or with status {@code 400 (Bad Request)} if the organisationTypeMasterDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the organisationTypeMasterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/organisation-type-masters")
    public ResponseEntity<OrganisationTypeMasterDTO> updateOrganisationTypeMaster(@Valid @RequestBody OrganisationTypeMasterDTO organisationTypeMasterDTO) throws URISyntaxException {
        log.debug("REST request to update OrganisationTypeMaster : {}", organisationTypeMasterDTO);
        if (organisationTypeMasterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrganisationTypeMasterDTO result = organisationTypeMasterService.save(organisationTypeMasterDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, organisationTypeMasterDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /organisation-type-masters} : get all the organisationTypeMasters.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of organisationTypeMasters in body.
     */
    @GetMapping("/organisation-type-masters")
    public ResponseEntity<List<OrganisationTypeMasterDTO>> getAllOrganisationTypeMasters(Pageable pageable) {
        log.debug("REST request to get a page of OrganisationTypeMasters");
        Page<OrganisationTypeMasterDTO> page = organisationTypeMasterService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /organisation-type-masters/:id} : get the "id" organisationTypeMaster.
     *
     * @param id the id of the organisationTypeMasterDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the organisationTypeMasterDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/organisation-type-masters/{id}")
    public ResponseEntity<OrganisationTypeMasterDTO> getOrganisationTypeMaster(@PathVariable Long id) {
        log.debug("REST request to get OrganisationTypeMaster : {}", id);
        Optional<OrganisationTypeMasterDTO> organisationTypeMasterDTO = organisationTypeMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(organisationTypeMasterDTO);
    }

    /**
     * {@code DELETE  /organisation-type-masters/:id} : delete the "id" organisationTypeMaster.
     *
     * @param id the id of the organisationTypeMasterDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/organisation-type-masters/{id}")
    public ResponseEntity<Void> deleteOrganisationTypeMaster(@PathVariable Long id) {
        log.debug("REST request to delete OrganisationTypeMaster : {}", id);
        organisationTypeMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/organisation-type-masters?query=:query} : search for the organisationTypeMaster corresponding
     * to the query.
     *
     * @param query the query of the organisationTypeMaster search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/organisation-type-masters")
    public ResponseEntity<List<OrganisationTypeMasterDTO>> searchOrganisationTypeMasters(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of OrganisationTypeMasters for query {}", query);
        Page<OrganisationTypeMasterDTO> page = organisationTypeMasterService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
