package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.DesignationMasterService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.DesignationMasterDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.DesignationMaster}.
 */
@RestController
@RequestMapping("/api")
public class DesignationMasterResource {

    private final Logger log = LoggerFactory.getLogger(DesignationMasterResource.class);

    private static final String ENTITY_NAME = "designationMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DesignationMasterService designationMasterService;

    public DesignationMasterResource(DesignationMasterService designationMasterService) {
        this.designationMasterService = designationMasterService;
    }

    /**
     * {@code POST  /designation-masters} : Create a new designationMaster.
     *
     * @param designationMasterDTO the designationMasterDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new designationMasterDTO, or with status {@code 400 (Bad Request)} if the designationMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/designation-masters")
    public ResponseEntity<DesignationMasterDTO> createDesignationMaster(@RequestBody DesignationMasterDTO designationMasterDTO) throws URISyntaxException {
        log.debug("REST request to save DesignationMaster : {}", designationMasterDTO);
        if (designationMasterDTO.getId() != null) {
            throw new BadRequestAlertException("A new designationMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DesignationMasterDTO result = designationMasterService.save(designationMasterDTO);
        return ResponseEntity.created(new URI("/api/designation-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /designation-masters} : Updates an existing designationMaster.
     *
     * @param designationMasterDTO the designationMasterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated designationMasterDTO,
     * or with status {@code 400 (Bad Request)} if the designationMasterDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the designationMasterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/designation-masters")
    public ResponseEntity<DesignationMasterDTO> updateDesignationMaster(@RequestBody DesignationMasterDTO designationMasterDTO) throws URISyntaxException {
        log.debug("REST request to update DesignationMaster : {}", designationMasterDTO);
        if (designationMasterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DesignationMasterDTO result = designationMasterService.save(designationMasterDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, designationMasterDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /designation-masters} : get all the designationMasters.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of designationMasters in body.
     */
    @GetMapping("/designation-masters")
    public ResponseEntity<List<DesignationMasterDTO>> getAllDesignationMasters(Pageable pageable) {
        log.debug("REST request to get a page of DesignationMasters");
        Page<DesignationMasterDTO> page = designationMasterService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /designation-masters/:id} : get the "id" designationMaster.
     *
     * @param id the id of the designationMasterDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the designationMasterDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/designation-masters/{id}")
    public ResponseEntity<DesignationMasterDTO> getDesignationMaster(@PathVariable Long id) {
        log.debug("REST request to get DesignationMaster : {}", id);
        Optional<DesignationMasterDTO> designationMasterDTO = designationMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(designationMasterDTO);
    }

    /**
     * {@code DELETE  /designation-masters/:id} : delete the "id" designationMaster.
     *
     * @param id the id of the designationMasterDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/designation-masters/{id}")
    public ResponseEntity<Void> deleteDesignationMaster(@PathVariable Long id) {
        log.debug("REST request to delete DesignationMaster : {}", id);
        designationMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/designation-masters?query=:query} : search for the designationMaster corresponding
     * to the query.
     *
     * @param query the query of the designationMaster search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/designation-masters")
    public ResponseEntity<List<DesignationMasterDTO>> searchDesignationMasters(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of DesignationMasters for query {}", query);
        Page<DesignationMasterDTO> page = designationMasterService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
