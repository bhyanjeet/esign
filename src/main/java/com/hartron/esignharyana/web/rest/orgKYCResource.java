package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.*;
import com.hartron.esignharyana.service.dto.OrganisationMasterDTO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.crypto.*;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.*;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.*;


//import static jdk.nashorn.internal.parser.TokenType.XML;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.ApplicationMaster}.
 */
@RestController
@RequestMapping("/api")
public class orgKYCResource {

    private final Logger log = LoggerFactory.getLogger(orgKYCResource.class);

    private static final String ENTITY_NAME = "applicationMaster";

    private final UserApplicationService userApplicationService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ApplicationMasterService applicationMasterService;

    private final UserDetailsService userDetailsService;

    private  final OrganisationMasterService organisationMasterService;

    private final EsignTransationService esignTransationService;

    public orgKYCResource(UserApplicationService userApplicationService, ApplicationMasterService applicationMasterService, UserDetailsService userDetailsService, OrganisationMasterService organisationMasterService, EsignTransationService esignTransationService) {
        this.userApplicationService = userApplicationService;
        this.applicationMasterService = applicationMasterService;
        this.userDetailsService = userDetailsService;
        this.organisationMasterService = organisationMasterService;
        this.esignTransationService = esignTransationService;
    }



    @RequestMapping(value ="orgKYC", params = "oranisationIdCode",  method = RequestMethod.GET)
    public String geteSign(@RequestParam String oranisationIdCode) throws Exception {

        log.debug("REST request to save ApplicationMaster : {}");
        //todo
        String resMsg = "";

        boolean isrequestValid = checkRequest(oranisationIdCode);
        if(isrequestValid) {

            resMsg = "Successsssssssssssssssssssssssssssssssssssss!";

            Optional<OrganisationMasterDTO> organisationMaster =  organisationMasterService.findOrganisationByOranisationIdCode(oranisationIdCode);

            // String xmlFilePath = "/var/www/xmlfile.xml";

            byte[] fileContent = FileUtils.readFileToByteArray(new File("/var/www/jeet.jpg"));

            String encodedString = Base64.getEncoder().encodeToString(fileContent);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder;

            dBuilder = dbFactory.newDocumentBuilder();

            Document doc = dBuilder.newDocument();

            StringWriter writer = new StringWriter();

            Element rootElement = doc.createElement("orgKYC");

            doc.appendChild(rootElement);

            Attr ver = doc.createAttribute("ver");
            ver.setValue("3.2");
            rootElement.setAttributeNode(ver);

            Attr ts = doc.createAttribute("ts");
            ts.setValue("06-01-2020");
            rootElement.setAttributeNode(ts);

            Attr txn = doc.createAttribute("txn");
            txn.setValue("txn0000001");
            rootElement.setAttributeNode(txn);

            Attr kycOrgId = doc.createAttribute("kycOrgId");
            kycOrgId.setValue("CSCO0002");
            rootElement.setAttributeNode(kycOrgId);

            Attr orgName = doc.createAttribute("orgName");
            orgName.setValue(organisationMaster.get().getOrganisationName());
            rootElement.setAttributeNode(orgName);

            Attr orgSigName = doc.createAttribute("orgSigName");
            orgSigName.setValue(organisationMaster.get().getOrganisationNodalOfficerName());
            rootElement.setAttributeNode(orgSigName);

            Attr orgSigCertId = doc.createAttribute("orgSigCertId");
            orgSigCertId.setValue("hartron001010");
            rootElement.setAttributeNode(ts);


            Element KYCinfoElement = doc.createElement("KYCinfo");
            rootElement.appendChild(KYCinfoElement);

            Attr employeeId = doc.createAttribute("employeeId");
            employeeId.setValue(organisationMaster.get().getOrganisationEmployeeId());
            KYCinfoElement.setAttributeNode(employeeId);

            Attr Name = doc.createAttribute("Name");
            Name.setValue(organisationMaster.get().getOrganisationName());
            KYCinfoElement.setAttributeNode(Name);

            Attr UserMobile = doc.createAttribute("UserMobile");
            UserMobile.setValue(organisationMaster.get().getOrganisationNodalOfficerPhoneMobile());
            KYCinfoElement.setAttributeNode(UserMobile);

            Attr email = doc.createAttribute("email");
            email.setValue(organisationMaster.get().getNodalOfficerOfficialEmail());
            KYCinfoElement.setAttributeNode(email);

            Attr dateOfBirth = doc.createAttribute("dateOfBirth");
            dateOfBirth.setValue(organisationMaster.get().getDateOfBirth().toString());
            KYCinfoElement.setAttributeNode(dateOfBirth);


            Attr gender = doc.createAttribute("gender");
            gender.setValue(organisationMaster.get().getGender());
            KYCinfoElement.setAttributeNode(gender);


            Attr Aadhaar = doc.createAttribute("employeeId");
            Aadhaar.setValue(organisationMaster.get().getAadhaar());
            KYCinfoElement.setAttributeNode(Aadhaar);


            Element photo = doc.createElement("Photo");
            photo.appendChild(doc.createTextNode(encodedString));

            rootElement.appendChild(photo);


            Element physicalVerification = doc.createElement("physicalVerification");
            physicalVerification.appendChild(doc.createTextNode("I hereby certify the physical verfication of " +  organisationMaster.get().getOrganisationNodalOfficerName()));

            rootElement.appendChild(physicalVerification);

            String xmlString = doc.toString().replaceAll("\\<\\?xml(.+?)\\?\\>","").trim();

            // create the xml file
            //transform the DOM Object to an XML File
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            //StreamResult streamResult = new StreamResult(new File(xmlFilePath));

            // If you use
            StreamResult result = new StreamResult(writer);
            // the output will be pushed to the standard output ...
            // You can use that for debugging

            transformer.transform(domSource, result);

//           TransformerFactory transformerFactory = TransformerFactory.newInstance();
//

//
//            String xmlSignOutput =  eSignXml(writer.toString());
            createSignedSignerXML(writer.toString());


//            sendOrgKYCRequest(xmlSignOutput);


        }
        else
        {
            resMsg ="Faillllllllllllllllllllllllllllllllllll";


        }
        return  resMsg;

    }




    //    @RequestMapping(value ="/create-signer", method = RequestMethod.POST)
    private void createSignedSignerXML(String kycXML) throws Exception {

        String signedKycXml = eSignXml(kycXML);

//        log.debug("REST request to save ApplicationMaster : {}");

        String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        int month = LocalDate.now().getMonthValue();
        String randomNo = RandomStringUtils.randomNumeric(3);
        String randomNo1 = RandomStringUtils.randomNumeric(8);
        String txnId = "txn" + randomNo + year + month + randomNo1;


        String resMsg = "";

        resMsg = "Successsssssssssssssssssssssssssssssssssssss!";

        // String xmlFilePath = "/var/www/xmlfile.xml";

//            byte[] fileContent = FileUtils.readFileToByteArray(new File("/var/www/jeet.jpg"));

        String encodedString = Base64.getEncoder().encodeToString(signedKycXml.getBytes());

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;

        dBuilder = dbFactory.newDocumentBuilder();

        Document doc = dBuilder.newDocument();

        StringWriter writer = new StringWriter();

        Element rootElement = doc.createElement("Signer");

        doc.appendChild(rootElement);

        Attr api = doc.createAttribute("api");
        api.setValue("create");
        rootElement.setAttributeNode(api);

        Attr orgType = doc.createAttribute("orgType");
        orgType.setValue("ORG");
        rootElement.setAttributeNode(orgType);

        Attr orgId = doc.createAttribute("orgId");
        orgId.setValue("CSORG1000002");
        rootElement.setAttributeNode(orgId);

        Attr eKycType = doc.createAttribute("eKycType");
        eKycType.setValue("ORG1");
        rootElement.setAttributeNode(eKycType);

        Attr ver = doc.createAttribute("ver");
        ver.setValue("3.2");
        rootElement.setAttributeNode(ver);

        Attr ts = doc.createAttribute("ts");
        ts.setValue(ZonedDateTime.now().toString());
        rootElement.setAttributeNode(ts);

        Attr txn = doc.createAttribute("txn");
        txn.setValue(txnId);
        rootElement.setAttributeNode(txn);


        Attr aspId = doc.createAttribute("aspId");
        aspId.setValue("CSORG1000002");
        rootElement.setAttributeNode(aspId);

        Attr kycData = doc.createAttribute("kycData");
        kycData.setValue(encodedString);
        rootElement.setAttributeNode(kycData);


//            Element photo = doc.createElement("Photo");
//            photo.appendChild(doc.createTextNode(encodedString));
//
//            rootElement.appendChild(photo);
//
//
//            Element physicalVerification = doc.createElement("physicalVerification");
//            physicalVerification.appendChild(doc.createTextNode("I hereby certify the physical verfication of " +  organisationMaster.get().getOrganisationNodalOfficerName()));
//
//            rootElement.appendChild(physicalVerification);

        String xmlString = doc.toString().replaceAll("\\<\\?xml(.+?)\\?\\>","").trim();

        // create the xml file
        //transform the DOM Object to an XML File
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(doc);
        //StreamResult streamResult = new StreamResult(new File(xmlFilePath));

        // If you use
        StreamResult result = new StreamResult(writer);
        // the output will be pushed to the standard output ...
        // You can use that for debugging

        transformer.transform(domSource, result);

//           TransformerFactory transformerFactory = TransformerFactory.newInstance();
//
//           Transformer transformer;
//
//           transformer = transformerFactory.newTransformer();
//

//
//            transformer.transform(new DOMSource(doc), new StreamResult(writer));
//
//            StreamResult result = new StreamResult(System.out);
//
//            String xmlString = writer.getBuffer().toString();

//            System.out.println("xmlSignOutputttttttttttttttttttt " + xmlString);
//
        String xmlSignOutput =  eSignXml(writer.toString());

//            validateXmlSign(xmlSignOutput);

//            boolean verificationStatus = verify(xmlSignOutput);

        sendOrgKYCRequest(xmlSignOutput);
//
//               String POST_URL = "http://118.185.194.103/";
//
//                URL url = new URL(POST_URL + "?" + xmlSignOutput);
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setRequestMethod("POST");
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setUseCaches(false);
//                conn.connect();


//        return  resMsg;

    }


    private void sendOrgKYCRequest(String xmlSignOutput) throws IOException {


        String request = xmlSignOutput;

        URL url = new URL("http://117.255.216.162:8097/api/createsigner");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        // Set timeout as per needs
        connection.setConnectTimeout(40000);
        connection.setReadTimeout(40000);

        // Set DoOutput to true if you want to use URLConnection for output.
        // Default is false
        connection.setDoOutput(true);

        connection.setUseCaches(true);
        connection.setRequestMethod("POST");

        // Set Headers
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty("Cache-Control", "no-cache");


        // Write XML
        OutputStream outputStream = connection.getOutputStream();
        byte[] b = request.getBytes("UTF-8");
        outputStream.write(b);
        outputStream.flush();
        outputStream.close();

        // Read XML
        InputStream inputStream = connection.getInputStream();
        byte[] res = new byte[2048];
        int i = 0;
        StringBuilder response = new StringBuilder();
        while ((i = inputStream.read(res)) != -1) {
            response.append(new String(res, 0, i));
        }
        inputStream.close();

        System.out.println("Response= " + response.toString());
    }


    private String createEsignXML(String applicationCodeId, String userCodeId) {
        String eSignXML = "";
        return eSignXML;
    }

    private boolean checkRequest(String oranisationIdCode) {
        // String eSignXML = "";
        boolean checkDetail = true;

        Optional<OrganisationMasterDTO> organisationMaster =  organisationMasterService.findOrganisationByOranisationIdCode(oranisationIdCode);

        return checkDetail;

    }

    private String eSignXml(String outputter) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, KeyException, ParserConfigurationException, TransformerException, MarshalException, XMLSignatureException, IOException, SAXException, KeyStoreException, CertificateException, UnrecoverableEntryException {
        // Create a DOM XMLSignatureFactory that will be used to generate the
        // enveloped signature
        XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

        // Create a Reference to the enveloped document (in this case we are
        // signing the whole document, so a URI of "" signifies that) and
        // also specify the SHA256 digest algorithm and the ENVELOPED Transform.
        Reference ref = fac.newReference
            ("", fac.newDigestMethod(DigestMethod.SHA256, null),
                Collections.singletonList
                    (fac.newTransform
                        (Transform.ENVELOPED, (TransformParameterSpec) null)),
                null, null);

        // Create the SignedInfo
        SignedInfo si = fac.newSignedInfo
            (fac.newCanonicalizationMethod
                    (CanonicalizationMethod.EXCLUSIVE,
                        (C14NMethodParameterSpec) null),
                fac.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", null),
                Collections.singletonList(ref));


        KeyStore p12 = KeyStore.getInstance("pkcs12");
        p12.load(new FileInputStream("/var/DocSigner.pfx"), "12345678".toCharArray());


        Enumeration e = p12.aliases();
        String alias = (String) e.nextElement();
        System.out.println("Alias certifikata:" + alias);
        Key privateKey = p12.getKey(alias, "12345678".toCharArray());



        KeyStore.PrivateKeyEntry keyEntry
            = (KeyStore.PrivateKeyEntry) p12.getEntry(alias, new KeyStore.PasswordProtection("12345678".toCharArray()));

        X509Certificate cert = (X509Certificate) keyEntry.getCertificate();

        KeyInfoFactory kif = fac.getKeyInfoFactory();

        System.out.println(cert.getSerialNumber());

        X509IssuerSerial x509IssuerSerial = kif.newX509IssuerSerial(cert.getSubjectX500Principal().getName(), cert.getSerialNumber());

        List x509Content = new ArrayList();
        System.out.println("ime: " + cert.getSubjectX500Principal().getName());
        x509Content.add(cert.getSubjectX500Principal().getName());
        x509Content.add(x509IssuerSerial);

        X509Data xd = kif.newX509Data(x509Content);
        KeyInfo ki = kif.newKeyInfo(Collections.singletonList(xd));
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document doc =
            dbf.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(outputter), "UTF-8"));
        DOMSignContext dsc = new DOMSignContext(privateKey, doc.getDocumentElement());
        XMLSignature signature = fac.newXMLSignature(si, ki);
        signature.sign(dsc);


        String xmlSignatureOutput = signature.toString();

        String signedXMLObject =  writeXmlDocumentToXmlFile(doc);

        return signedXMLObject;
    }

    private String  writeXmlDocumentToXmlFile(Document xmlDocument)
    {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        String signedXMLObject ="";
        try {
            transformer = tf.newTransformer();

            // Uncomment if you do not require XML declaration
            // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            //A character stream that collects its output in a string buffer,
            //which can then be used to construct a string.
            StringWriter writer = new StringWriter();

            //transform document to string
            transformer.transform(new DOMSource(xmlDocument), new StreamResult(writer));

            String xmlString = writer.getBuffer().toString();
            System.out.println(xmlString);
            signedXMLObject = xmlString;
            //Print to console or logs
        }
        catch (TransformerException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return signedXMLObject;
    }


    private  void validateXmlSign(String signedXMLObject) throws Exception {


        // Instantiate the document to be validated
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document doc =
            dbf.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(signedXMLObject), "UTF-8"));

        // Find Signature element
        NodeList nl =
            doc.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
        if (nl.getLength() == 0) {
            throw new Exception("Cannot find Signature element");
        }

        NodeList keyinfo =
            doc.getElementsByTagNameNS(XMLSignature.XMLNS, "KeyInfo");
        if (keyinfo.getLength() == 0) {
            throw new Exception("Cannot find Signature element");
        }



        // Create a DOM XMLSignatureFactory that will be used to unmarshal the
        // document containing the XMLSignature
        XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

        // Create a DOMValidateContext and specify a KeyValue KeySelector
        // and document context
        DOMValidateContext valContext = new DOMValidateContext
            (new KeyValueKeySelector(), nl.item(0));

        // unmarshal the XMLSignature
        XMLSignature signature = fac.unmarshalXMLSignature(valContext);

        // Validate the XMLSignature (generated above)
        boolean coreValidity = signature.validate(valContext);



        // Check core validation statususerIdCodeer
        if (coreValidity == false) {
            System.err.println("Signature failed core validation");
            boolean sv = signature.getSignatureValue().validate(valContext);
            System.out.println("signature validation statussssssssssssssssss: " + sv);
            // check the validation status of each Reference
            Iterator i = signature.getSignedInfo().getReferences().iterator();
            for (int j=0; i.hasNext(); j++) {
                boolean refValid =
                    ((Reference) i.next()).validate(valContext);
                System.out.println("ref["+j+"] validity status: " + refValid);
            }
        } else {
            System.out.println("Signature passed core validationnnnnnnnnnnnnnnnnn");
        }
    }


    /**
     * KeySelector which retrieves the public key out of the
     * KeyValue element and returns it.
     * NOTE: If the key algorithm doesn't match signature algorithm,
     * then the public key will be ignored.
     */
    private static class KeyValueKeySelector extends KeySelector {
        public KeySelectorResult select(KeyInfo keyInfo,
                                        KeySelector.Purpose purpose,
                                        AlgorithmMethod method,
                                        XMLCryptoContext context)
            throws KeySelectorException {
            if (keyInfo == null) {
                throw new KeySelectorException("Null KeyInfo object!");
            }
            SignatureMethod sm = (SignatureMethod) method;
            List list = keyInfo.getContent();
            PublicKey pk = null;
            for (int i = 0; i < list.size(); i++) {
                XMLStructure xmlStructure = (XMLStructure) list.get(i);
                if (xmlStructure instanceof KeyValue) {

                    try {
                        pk = ((KeyValue)xmlStructure).getPublicKey();
                    } catch (KeyException ke) {
                        throw new KeySelectorException(ke);
                    }
                    // make sure algorithm is compatible with method
                    if (algEquals(sm.getAlgorithm(), pk.getAlgorithm())) {
                        return new SimpleKeySelectorResult(pk);
                    }

                }
                else if (xmlStructure instanceof X509Data) {
                    for (Object data : ((X509Data) xmlStructure).getContent()) {
                        if (data instanceof X509Certificate) {
                            pk = ((X509Certificate) data).getPublicKey();
                        }
                    }
                }
            }
            throw new KeySelectorException("No KeyValue element found!");
        }

        //@@@FIXME: this should also work for key types other than DSA/RSA
        static boolean algEquals(String algURI, String algName) {
            if (algName.equalsIgnoreCase("DSA") &&
                algURI.equalsIgnoreCase(SignatureMethod.DSA_SHA1)) {
                return true;
            } else if (algName.equalsIgnoreCase("RSA") &&
                algURI.equalsIgnoreCase(SignatureMethod.RSA_SHA1)) {
                return true;
            } else {
                return false;
            }
        }
    }

    private static class SimpleKeySelectorResult implements KeySelectorResult {
        private PublicKey pk;
        SimpleKeySelectorResult(PublicKey pk) {
            this.pk = pk;
        }

        public Key getKey() { return pk; }
    }

    public static boolean verify(String xmlSignOutput)
    {

        boolean verificationResult = false;

        try
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            Document signedDocument = dbf.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(xmlSignOutput), "UTF-8"));

            NodeList nl = signedDocument.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");

            if (nl.getLength() == 0)
            {
                throw new IllegalArgumentException("Cannot find Signature element");
            }

            XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

            DOMValidateContext valContext = new DOMValidateContext(GetPublicKey(), nl.item(0));
            XMLSignature signature = fac.unmarshalXMLSignature(valContext);

            verificationResult = signature.validate(valContext);

        }
        catch (Exception e)
        {
            System.out.println("Error while verifying digital siganature" + e.getMessage());
            e.printStackTrace();
        }

        return verificationResult;
    }

    public static PublicKey GetPublicKey() throws FileNotFoundException, KeyStoreException, CertificateException {



        PublicKey publicKey = null;
        FileInputStream is = null;

        try
        {
            is = new FileInputStream("/var/DSCPublicKey.cer");
//            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
//            keystore.load(is, "12345678".toCharArray());
//            String alias = "Alias";
//            Key key = keystore.getKey(alias, "password".toCharArray());
//            if (key instanceof PrivateKey)
//            {

//                 Get certificate of public key
//                java.security.Certificate cert = (java.security.Certificate) keystore.getCertificate(alias);
            CertificateFactory fact = CertificateFactory.getInstance("X.509");
            X509Certificate cert =  (X509Certificate) fact.generateCertificate(is);

            // Get public key
            publicKey = cert.getPublicKey();

//            }
        }
        catch (FileNotFoundException ex)
        {
        }
        catch ( IOException ex)
        {
        }
        finally
        {
            try
            {
                is.close();
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }


        return publicKey;

    }

}


