package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.domain.User;
import com.hartron.esignharyana.service.DistrictMasterService;
import com.hartron.esignharyana.service.UserService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.DistrictMasterDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.DistrictMaster}.
 */
@RestController
@RequestMapping("/api")
public class DistrictMasterResource {

    private final Logger log = LoggerFactory.getLogger(DistrictMasterResource.class);

    private static final String ENTITY_NAME = "districtMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DistrictMasterService districtMasterService;

    private final UserService userService;

    public DistrictMasterResource(DistrictMasterService districtMasterService, UserService userService) {
        this.districtMasterService = districtMasterService;
        this.userService = userService;
    }

    /**
     * {@code POST  /district-masters} : Create a new districtMaster.
     *
     * @param districtMasterDTO the districtMasterDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new districtMasterDTO, or with status {@code 400 (Bad Request)} if the districtMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/district-masters")
    public ResponseEntity<DistrictMasterDTO> createDistrictMaster(@RequestBody DistrictMasterDTO districtMasterDTO) throws URISyntaxException {
        log.debug("REST request to save DistrictMaster : {}", districtMasterDTO);
        if (districtMasterDTO.getId() != null) {
            throw new BadRequestAlertException("A new districtMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Optional<User> user = userService.getUserWithAuthorities();
        districtMasterDTO.setCreatedOn(LocalDate.now());
        districtMasterDTO.setLastUpdatedOn(LocalDate.now());
        districtMasterDTO.setVerifiedOn(LocalDate.now());
        districtMasterDTO.setVerifiedBy(user.get().getLogin());

        validateInput(districtMasterDTO);
        DistrictMasterDTO result = districtMasterService.save(districtMasterDTO);
        return ResponseEntity.created(new URI("/api/district-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /district-masters} : Updates an existing districtMaster.
     *
     * @param districtMasterDTO the districtMasterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated districtMasterDTO,
     * or with status {@code 400 (Bad Request)} if the districtMasterDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the districtMasterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/district-masters")
    public ResponseEntity<DistrictMasterDTO> updateDistrictMaster(@RequestBody DistrictMasterDTO districtMasterDTO) throws URISyntaxException {
        log.debug("REST request to update DistrictMaster : {}", districtMasterDTO);
        if (districtMasterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        Optional<User> user = userService.getUserWithAuthorities();
        districtMasterDTO.setCreatedOn(LocalDate.now());
        districtMasterDTO.setLastUpdatedOn(LocalDate.now());
        districtMasterDTO.setVerifiedOn(LocalDate.now());
        districtMasterDTO.setVerifiedBy(user.get().getLogin());

        validateInput(districtMasterDTO);
        DistrictMasterDTO result = districtMasterService.save(districtMasterDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, districtMasterDTO.getId().toString()))
            .body(result);
    }


    private void validateInput(DistrictMasterDTO districtMasterDTO) {
        String name = "[a-zA-Z ]*$";
        String createdBy = "[a-zA-Z0-9 ]*$";
        String deptId = "[0-9]*$";

        if(districtMasterDTO.getDistrictName()!=null) {
            if (!districtMasterDTO.getDistrictName().trim().matches(name)) {
                System.out.println("Invalid District Name");
                throw new BadRequestAlertException("District Name should be alphabetic!", "organisationMaster", "invalidNodalOfficerName");
            }
        }else
        {
            System.out.println("Invalid District Name");
            throw new BadRequestAlertException("District Name can't be Empty!", "organisationMaster", "invalidNodalOfficerName");
        }

        if(districtMasterDTO.getDistrictCode()!=null) {
            if (!districtMasterDTO.getRemarks().matches(createdBy)) {
                System.out.println("Invalid District Code");
                throw new BadRequestAlertException("District Code accepts only alphanumeric value!", "organisationMaster", "invalidNodalOfficerName");
            }
        }

        if(districtMasterDTO.getRemarks()!=null) {
            if (!districtMasterDTO.getRemarks().matches(createdBy)) {
                System.out.println("Invalid Status");
                throw new BadRequestAlertException("Remarks accepts only alphanumeric value!", "organisationMaster", "invalidNodalOfficerName");
            }
        }

    }

    /**
     * {@code GET  /district-masters} : get all the districtMasters.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of districtMasters in body.
     */
    @GetMapping("/district-masters")
    public ResponseEntity<List<DistrictMasterDTO>> getAllDistrictMasters(Pageable pageable) {
        log.debug("REST request to get a page of DistrictMasters");
        Page<DistrictMasterDTO> page = districtMasterService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /district-masters/:id} : get the "id" districtMaster.
     *
     * @param id the id of the districtMasterDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the districtMasterDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/district-masters/{id}")
    public ResponseEntity<DistrictMasterDTO> getDistrictMaster(@PathVariable Long id) {
        log.debug("REST request to get DistrictMaster : {}", id);
        Optional<DistrictMasterDTO> districtMasterDTO = districtMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(districtMasterDTO);
    }

    /**
     * {@code DELETE  /district-masters/:id} : delete the "id" districtMaster.
     *
     * @param id the id of the districtMasterDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/district-masters/{id}")
    public ResponseEntity<Void> deleteDistrictMaster(@PathVariable Long id) {
        log.debug("REST request to delete DistrictMaster : {}", id);
        districtMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/district-masters?query=:query} : search for the districtMaster corresponding
     * to the query.
     *
     * @param query the query of the districtMaster search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/district-masters")
    public ResponseEntity<List<DistrictMasterDTO>> searchDistrictMasters(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of DistrictMasters for query {}", query);
        Page<DistrictMasterDTO> page = districtMasterService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
