package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.OrganisationDocumentService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.OrganisationDocumentDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.micrometer.core.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.OrganisationDocument}.
 */
@RestController
@RequestMapping("/api")
public class OrganisationDocumentResource {

    private final Logger log = LoggerFactory.getLogger(OrganisationDocumentResource.class);

    private static final String ENTITY_NAME = "organisationDocument";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrganisationDocumentService organisationDocumentService;

    public OrganisationDocumentResource(OrganisationDocumentService organisationDocumentService) {
        this.organisationDocumentService = organisationDocumentService;
    }

    /**
     * {@code POST  /organisation-documents} : Create a new organisationDocument.
     *
     * @param organisationDocumentDTO the organisationDocumentDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new organisationDocumentDTO, or with status {@code 400 (Bad Request)} if the organisationDocument has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/organisation-documents")
    public ResponseEntity<OrganisationDocumentDTO> createOrganisationDocument(@Valid @RequestBody OrganisationDocumentDTO organisationDocumentDTO) throws URISyntaxException {
        log.debug("REST request to save OrganisationDocument : {}", organisationDocumentDTO);
        if (organisationDocumentDTO.getId() != null) {
            throw new BadRequestAlertException("A new organisationDocument cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrganisationDocumentDTO result = organisationDocumentService.save(organisationDocumentDTO);
        return ResponseEntity.created(new URI("/api/organisation-documents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /organisation-documents} : Updates an existing organisationDocument.
     *
     * @param organisationDocumentDTO the organisationDocumentDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated organisationDocumentDTO,
     * or with status {@code 400 (Bad Request)} if the organisationDocumentDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the organisationDocumentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/organisation-documents")
    public ResponseEntity<OrganisationDocumentDTO> updateOrganisationDocument(@Valid @RequestBody OrganisationDocumentDTO organisationDocumentDTO) throws URISyntaxException {
        log.debug("REST request to update OrganisationDocument : {}", organisationDocumentDTO);
        if (organisationDocumentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrganisationDocumentDTO result = organisationDocumentService.save(organisationDocumentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, organisationDocumentDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /organisation-documents} : get all the organisationDocuments.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of organisationDocuments in body.
     */
    @GetMapping("/organisation-documents")
    public ResponseEntity<List<OrganisationDocumentDTO>> getAllOrganisationDocuments(Pageable pageable) {
        log.debug("REST request to get a page of OrganisationDocuments");
        Page<OrganisationDocumentDTO> page = organisationDocumentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /organisation-documents/:id} : get the "id" organisationDocument.
     *
     * @param id the id of the organisationDocumentDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the organisationDocumentDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/organisation-documents/{id}")
    public ResponseEntity<OrganisationDocumentDTO> getOrganisationDocument(@PathVariable Long id) {
        log.debug("REST request to get OrganisationDocument : {}", id);
        Optional<OrganisationDocumentDTO> organisationDocumentDTO = organisationDocumentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(organisationDocumentDTO);
    }

    /**
     * {@code DELETE  /organisation-documents/:id} : delete the "id" organisationDocument.
     *
     * @param id the id of the organisationDocumentDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/organisation-documents/{id}")
    public ResponseEntity<Void> deleteOrganisationDocument(@PathVariable Long id) {
        log.debug("REST request to delete OrganisationDocument : {}", id);
        organisationDocumentService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/organisation-documents?query=:query} : search for the organisationDocument corresponding
     * to the query.
     *
     * @param query the query of the organisationDocument search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/organisation-documents")
    public ResponseEntity<List<OrganisationDocumentDTO>> searchOrganisationDocuments(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of OrganisationDocuments for query {}", query);
        Page<OrganisationDocumentDTO> page = organisationDocumentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    @RequestMapping(value = "organisationTypeId", params = "id", method = RequestMethod.GET)
    @Timed
    public List<OrganisationDocumentDTO> getDocumentByOrgisastionTypeId(@RequestParam Long id) {
        log.debug("REST request to get all Organisation Documents");
        return organisationDocumentService.findOrganisationDocumentByOrganisationTypeMasterId(id);
    }
}
