package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.SAXException;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;


//import static jdk.nashorn.internal.parser.TokenType.XML;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.ApplicationMaster}.
 */
@RestController
@RequestMapping("/api")
public class verifyUserResource {

    private final Logger log = LoggerFactory.getLogger(verifyUserResource.class);

    private static final String ENTITY_NAME = "applicationMaster";

    private final UserApplicationService userApplicationService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ApplicationMasterService applicationMasterService;

    private final UserDetailsService userDetailsService;

    private  final OrganisationMasterService organisationMasterService;

    private final eSignService eSignService;

    private final VerifyEsignXmlService verifyEsignXmlService;

    private final SignerService signerService;


    public verifyUserResource(UserApplicationService userApplicationService, ApplicationMasterService applicationMasterService, UserDetailsService userDetailsService, OrganisationMasterService organisationMasterService, eSignService eSignService, VerifyEsignXmlService verifyEsignXmlService, SignerService signerService) {
        this.userApplicationService = userApplicationService;
        this.applicationMasterService = applicationMasterService;
        this.userDetailsService = userDetailsService;
        this.organisationMasterService = organisationMasterService;
        this.eSignService = eSignService;
        this.verifyEsignXmlService = verifyEsignXmlService;
        this.signerService = signerService;
    }

    @RequestMapping(value = "verifyUser", params = {"userCodeId", "lastAction"}, method=RequestMethod.GET)
    public  String verifyUser(String userCodeId, String lastAction) throws TransformerException, ParserConfigurationException, IOException, CertificateException, NoSuchAlgorithmException, XMLSignatureException, UnrecoverableEntryException, InvalidAlgorithmParameterException, SAXException, MarshalException, KeyStoreException {
       String  UserDetails =  signerService.verifyUser(userCodeId, lastAction);
       System.out.println("Response = " + UserDetails);

       String ResponseSigner = "";

        System.out.println("ResponseInforrrrrrrrrrrrrrrrrrrrrrrrr" + UserDetails);

       if (UserDetails.equals("000")){
           ResponseSigner = "Success";
       }
       else {
           ResponseSigner = UserDetails;
       }
        System.out.println("ResponseInforrrrrrrrrrrrrr222222222222222222" + UserDetails);
       return ResponseSigner;
    }

//    @RequestMapping(value = "verifySign", params = "xmlSignOutput", method=RequestMethod.GET)
    @PostMapping("/verifySign")
    public Boolean verifySign(@RequestBody String xmlSignOutput) {
        Boolean verifyStatus =  verifyEsignXmlService.verify(xmlSignOutput);
        System.out.println("Response = " + verifyStatus);
//        if (!StringUtils.isEmpty(verifyStatus)) {
//            return true;
//        }
        return verifyStatus;
    }

    @RequestMapping(value = "account", params = "xmlDoc", method=RequestMethod.GET)
    public  Boolean accountXml(String xmlDoc) throws TransformerException, ParserConfigurationException, IOException, CertificateException, NoSuchAlgorithmException, XMLSignatureException, UnrecoverableEntryException, InvalidAlgorithmParameterException, SAXException, MarshalException, KeyStoreException {
        String  accountxml =  signerService.signXML(xmlDoc);
        System.out.println("Response = " + accountxml);
        if (!StringUtils.isEmpty(accountxml)) {
            return true;
        }
        return false;
    }

    @RequestMapping(value = "accountSigner", params = {"userCodeId", "lastAction", "signerId", "accountStatus"}, method=RequestMethod.GET)
    public  String verifyUser(String userCodeId, String lastAction, String signerId, String accountStatus) throws TransformerException, ParserConfigurationException, IOException, CertificateException, NoSuchAlgorithmException, XMLSignatureException, UnrecoverableEntryException, InvalidAlgorithmParameterException, SAXException, MarshalException, KeyStoreException {
        String  UserDetails =  signerService.accountSigner(userCodeId, lastAction, signerId, accountStatus);
        System.out.println("Response = " + UserDetails);

        String ResponseSigner = "";

        System.out.println("ResponseInforrrrrrrrrrrrrrrrrrrrrrrrr" + UserDetails);

        if (UserDetails.equals("000")){
            ResponseSigner = "Success";
        }
        else {
            ResponseSigner = UserDetails;
        }
        System.out.println("ResponseInforrrrrrrrrrrrrr222222222222222222" + UserDetails);
        return ResponseSigner;
    }

    @RequestMapping(value = "updateSigner", params = {"userCodeId", "lastAction", "signerId"}, method=RequestMethod.GET)
    public  String updateSigner(String userCodeId, String lastAction, String signerId) throws TransformerException, ParserConfigurationException, IOException, CertificateException, NoSuchAlgorithmException, XMLSignatureException, UnrecoverableEntryException, InvalidAlgorithmParameterException, SAXException, MarshalException, KeyStoreException {
        String  UserDetails =  signerService.updateSigner(userCodeId, lastAction, signerId);
        System.out.println("Response = " + UserDetails);

        String ResponseSigner = "";

        System.out.println("ResponseInforrrrrrrrrrrrrrrrrrrrrrrrr" + UserDetails);

        if (UserDetails.equals("000")){
            ResponseSigner = "Success";
        }
        else {
            ResponseSigner = UserDetails;
        }
        System.out.println("ResponseInforrrrrrrrrrrrrr222222222222222222" + UserDetails);
        return ResponseSigner;
    }
}



