package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.security.SecurityUtils;
import com.hartron.esignharyana.service.UserApplicationService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.UserApplicationDTO;
import com.hartron.esignharyana.service.dto.UserApplicationCriteria;
import com.hartron.esignharyana.service.UserApplicationQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.UserApplication}.
 */
@RestController
@RequestMapping("/api")
public class UserApplicationResource {

    private final Logger log = LoggerFactory.getLogger(UserApplicationResource.class);

    private static final String ENTITY_NAME = "userApplication";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserApplicationService userApplicationService;

    private final UserApplicationQueryService userApplicationQueryService;

    public UserApplicationResource(UserApplicationService userApplicationService, UserApplicationQueryService userApplicationQueryService) {
        this.userApplicationService = userApplicationService;
        this.userApplicationQueryService = userApplicationQueryService;
    }

    /**
     * {@code POST  /user-applications} : Create a new userApplication.
     *
     * @param userApplicationDTO the userApplicationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userApplicationDTO, or with status {@code 400 (Bad Request)} if the userApplication has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-applications")
    public ResponseEntity<UserApplicationDTO> createUserApplication(@RequestBody UserApplicationDTO userApplicationDTO) throws URISyntaxException {
        log.debug("REST request to save UserApplication : {}", userApplicationDTO);
        if (userApplicationDTO.getId() != null) {
            throw new BadRequestAlertException("A new userApplication cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserApplicationDTO result = userApplicationService.save(userApplicationDTO);
        return ResponseEntity.created(new URI("/api/user-applications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-applications} : Updates an existing userApplication.
     *
     * @param userApplicationDTO the userApplicationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userApplicationDTO,
     * or with status {@code 400 (Bad Request)} if the userApplicationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userApplicationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-applications")
    public ResponseEntity<UserApplicationDTO> updateUserApplication(@RequestBody UserApplicationDTO userApplicationDTO) throws URISyntaxException {
        log.debug("REST request to update UserApplication : {}", userApplicationDTO);
        if (userApplicationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserApplicationDTO result = userApplicationService.save(userApplicationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, userApplicationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-applications} : get all the userApplications.
     *

     * @param pageable the pagination information.

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userApplications in body.
     */
    @GetMapping("/user-applications")
    public ResponseEntity<List<UserApplicationDTO>> getAllUserApplications(UserApplicationCriteria criteria, Pageable pageable) {
        log.debug("REST request to get UserApplications by criteria: {}", criteria);
        Page<UserApplicationDTO> page = userApplicationQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /user-applications/count} : count all the userApplications.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/user-applications/count")
    public ResponseEntity<Long> countUserApplications(UserApplicationCriteria criteria) {
        log.debug("REST request to count UserApplications by criteria: {}", criteria);
        return ResponseEntity.ok().body(userApplicationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /user-applications/:id} : get the "id" userApplication.
     *
     * @param id the id of the userApplicationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userApplicationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-applications/{id}")
    public ResponseEntity<UserApplicationDTO> getUserApplication(@PathVariable Long id) {
        log.debug("REST request to get UserApplication : {}", id);
        Optional<UserApplicationDTO> userApplicationDTO = userApplicationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userApplicationDTO);
    }

    /**
     * {@code DELETE  /user-applications/:id} : delete the "id" userApplication.
     *
     * @param id the id of the userApplicationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-applications/{id}")
    public ResponseEntity<Void> deleteUserApplication(@PathVariable Long id) {
        log.debug("REST request to delete UserApplication : {}", id);
        userApplicationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/user-applications?query=:query} : search for the userApplication corresponding
     * to the query.
     *
     * @param query the query of the userApplication search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/user-applications")
    public ResponseEntity<List<UserApplicationDTO>> searchUserApplications(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of UserApplications for query {}", query);
        Page<UserApplicationDTO> page = userApplicationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @RequestMapping(value = "userApplicationByUserLogin")
    public List<UserApplicationDTO> getUserApplicationByLogin() {
        return userApplicationService.findUserApplicationByUserLogin(SecurityUtils.getCurrentUserLogin().get());
    }

    @RequestMapping(value = "userApplicationByUserId", params = "userId", method = RequestMethod.GET)
    public List<UserApplicationDTO> getApplicationByUserId(@RequestParam Long userId) {

        return userApplicationService.findApplicationByUserId(userId);
    }
}
