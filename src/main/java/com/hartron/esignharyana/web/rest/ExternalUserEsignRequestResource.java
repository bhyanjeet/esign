package com.hartron.esignharyana.web.rest;

import com.hartron.esignharyana.service.ExternalUserEsignRequestService;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import com.hartron.esignharyana.service.dto.ExternalUserEsignRequestDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hartron.esignharyana.domain.ExternalUserEsignRequest}.
 */
@RestController
@RequestMapping("/api")
public class ExternalUserEsignRequestResource {

    private final Logger log = LoggerFactory.getLogger(ExternalUserEsignRequestResource.class);

    private static final String ENTITY_NAME = "externalUserEsignRequest";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExternalUserEsignRequestService externalUserEsignRequestService;

    public ExternalUserEsignRequestResource(ExternalUserEsignRequestService externalUserEsignRequestService) {
        this.externalUserEsignRequestService = externalUserEsignRequestService;
    }

    /**
     * {@code POST  /external-user-esign-requests} : Create a new externalUserEsignRequest.
     *
     * @param externalUserEsignRequestDTO the externalUserEsignRequestDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new externalUserEsignRequestDTO, or with status {@code 400 (Bad Request)} if the externalUserEsignRequest has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/external-user-esign-requests")
    public ResponseEntity<ExternalUserEsignRequestDTO> createExternalUserEsignRequest(@RequestBody ExternalUserEsignRequestDTO externalUserEsignRequestDTO) throws URISyntaxException {
        log.debug("REST request to save ExternalUserEsignRequest : {}", externalUserEsignRequestDTO);
        if (externalUserEsignRequestDTO.getId() != null) {
            throw new BadRequestAlertException("A new externalUserEsignRequest cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExternalUserEsignRequestDTO result = externalUserEsignRequestService.save(externalUserEsignRequestDTO);
        return ResponseEntity.created(new URI("/api/external-user-esign-requests/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /external-user-esign-requests} : Updates an existing externalUserEsignRequest.
     *
     * @param externalUserEsignRequestDTO the externalUserEsignRequestDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated externalUserEsignRequestDTO,
     * or with status {@code 400 (Bad Request)} if the externalUserEsignRequestDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the externalUserEsignRequestDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/external-user-esign-requests")
    public ResponseEntity<ExternalUserEsignRequestDTO> updateExternalUserEsignRequest(@RequestBody ExternalUserEsignRequestDTO externalUserEsignRequestDTO) throws URISyntaxException {
        log.debug("REST request to update ExternalUserEsignRequest : {}", externalUserEsignRequestDTO);
        if (externalUserEsignRequestDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ExternalUserEsignRequestDTO result = externalUserEsignRequestService.save(externalUserEsignRequestDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, externalUserEsignRequestDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /external-user-esign-requests} : get all the externalUserEsignRequests.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of externalUserEsignRequests in body.
     */
    @GetMapping("/external-user-esign-requests")
    public ResponseEntity<List<ExternalUserEsignRequestDTO>> getAllExternalUserEsignRequests(Pageable pageable) {
        log.debug("REST request to get a page of ExternalUserEsignRequests");
        Page<ExternalUserEsignRequestDTO> page = externalUserEsignRequestService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /external-user-esign-requests/:id} : get the "id" externalUserEsignRequest.
     *
     * @param id the id of the externalUserEsignRequestDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the externalUserEsignRequestDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/external-user-esign-requests/{id}")
    public ResponseEntity<ExternalUserEsignRequestDTO> getExternalUserEsignRequest(@PathVariable Long id) {
        log.debug("REST request to get ExternalUserEsignRequest : {}", id);
        Optional<ExternalUserEsignRequestDTO> externalUserEsignRequestDTO = externalUserEsignRequestService.findOne(id);
        return ResponseUtil.wrapOrNotFound(externalUserEsignRequestDTO);
    }

    /**
     * {@code DELETE  /external-user-esign-requests/:id} : delete the "id" externalUserEsignRequest.
     *
     * @param id the id of the externalUserEsignRequestDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/external-user-esign-requests/{id}")
    public ResponseEntity<Void> deleteExternalUserEsignRequest(@PathVariable Long id) {
        log.debug("REST request to delete ExternalUserEsignRequest : {}", id);
        externalUserEsignRequestService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/external-user-esign-requests?query=:query} : search for the externalUserEsignRequest corresponding
     * to the query.
     *
     * @param query the query of the externalUserEsignRequest search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/external-user-esign-requests")
    public ResponseEntity<List<ExternalUserEsignRequestDTO>> searchExternalUserEsignRequests(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ExternalUserEsignRequests for query {}", query);
        Page<ExternalUserEsignRequestDTO> page = externalUserEsignRequestService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
