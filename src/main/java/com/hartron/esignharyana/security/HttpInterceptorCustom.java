package com.hartron.esignharyana.security;

import com.hartron.esignharyana.domain.UserAuthDetails;
import com.hartron.esignharyana.repository.UserAuthDetailsRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Component
public class HttpInterceptorCustom extends HandlerInterceptorAdapter
{
    private final UserAuthDetailsRepository userAuthDetailsRepository;

    public HttpInterceptorCustom(UserAuthDetailsRepository userAuthDetailsRepository) {
        this.userAuthDetailsRepository = userAuthDetailsRepository;
    }

    @Override
    public boolean preHandle(HttpServletRequest requestServlet, HttpServletResponse responseServlet, Object handler) throws Exception
    {
        String header_authorization = requestServlet.getHeader("Authorization");
        String token = (StringUtils.isEmpty(header_authorization) ? null : header_authorization.split(" ")[1]);
        if(StringUtils.isEmpty(token) || requestServlet.getServletPath().equalsIgnoreCase("/api/authenticate")) {
            return true;
        } else {
            List<UserAuthDetails> userAuthDetailsList = this.userAuthDetailsRepository.getAllByAuthToken(token);
            if(userAuthDetailsList.size() > 0) {
                return requestServlet.getHeader("User-Agent").equalsIgnoreCase(userAuthDetailsList.get(0).getDevice());
            } else {
                return false;
            }
        }
    }
}
