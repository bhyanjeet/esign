package com.hartron.esignharyana.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String ORG_NODAL = "ROLE_ORG_NODAL";

    public static final String ORG_USER = "ROLE_ORG_USER";

    public static final String ASP_NODAL = "ROLE_ASP_NODAL";

    private AuthoritiesConstants() {
    }
}
