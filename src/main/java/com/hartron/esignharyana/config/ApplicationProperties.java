package com.hartron.esignharyana.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Esignharyana.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private final Sms sms = new Sms();

    public Sms getSms() {
        return sms;
    }

    public static class Sms {
        private String username = "";
        private String password = "";
        private String senderId = "";
        private String secureKey = "";

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getSenderId() {
            return senderId;
        }

        public void setSenderId(String senderId) {
            this.senderId = senderId;
        }

        public String getSecureKey() {
            return secureKey;
        }

        public void setSecureKey(String secureKey) {
            this.secureKey = secureKey;
        }
    }

}
