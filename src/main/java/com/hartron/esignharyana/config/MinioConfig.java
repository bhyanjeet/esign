package com.hartron.esignharyana.config;

import io.github.jhipster.config.JHipsterConstants;
import io.minio.MinioClient;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "minio",ignoreUnknownFields = false)
@Profile({JHipsterConstants.SPRING_PROFILE_DEVELOPMENT, JHipsterConstants.SPRING_PROFILE_PRODUCTION })
public class MinioConfig {
    String url;
    String accesskey;
    String secretkey;
    MinioClient minioClient;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAccesskey() {
        return accesskey;
    }

    public void setAccesskey(String accesskey) {
        this.accesskey = accesskey;
    }

    public String getSecretkey() {
        return secretkey;
    }

    public void setSecretkey(String secretkey) {
        this.secretkey = secretkey;
    }

    @Bean
    public MinioClient generateClient() throws InvalidPortException, InvalidEndpointException {
        minioClient=new MinioClient(url, accesskey,secretkey);
        return minioClient;
    }

    public MinioClient getMinioClient() {
        return minioClient;
    }
}
