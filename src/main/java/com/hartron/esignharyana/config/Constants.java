package com.hartron.esignharyana.config;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";
    public static final String ALPHABET_REGEX = "^[a-zA-Z ]*$";
    public static final String NUMERIC_REGEX = "^[0-9]*$";
    public static final String EMAIL_REGEX = "^[A-Za-z0-9+_.-]+@(.+)$";
    public static final String CODE_REGEX = "^[A-Za-z0-9() ]*$";
    public static final String NUMALPHA_REGEX = "^[A-Za-z0-9-()\\/]*$";
    public static final String ALPHANUM_REGEX = "^[A-Za-z0-9-,\\/#().&: ]*$";

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String ANONYMOUS_USER = "anonymoususer";

    private Constants() {
    }
}
