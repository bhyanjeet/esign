package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.ApplicationLogs;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ApplicationLogs entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ApplicationLogsRepository extends JpaRepository<ApplicationLogs, Long>, JpaSpecificationExecutor<ApplicationLogs> {

}
