package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.ApplicationMaster;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the ApplicationMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ApplicationMasterRepository extends JpaRepository<ApplicationMaster, Long> {


    Optional<ApplicationMaster> findByApplicationName(String applicationName);
}
