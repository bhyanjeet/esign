package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.OrganisationDocument;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OrganisationDocument entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrganisationDocumentRepository extends JpaRepository<OrganisationDocument, Long> {

}
