package com.hartron.esignharyana.repository;

import com.hartron.esignharyana.domain.EsignError;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the EsignError entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EsignErrorRepository extends JpaRepository<EsignError, Long> {

}
