package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.UserApplication;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the UserApplication entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserApplicationRepository extends JpaRepository<UserApplication, Long>, JpaSpecificationExecutor<UserApplication> {


    List<UserApplication> findAllByUserId(Long findByUserId);
}
