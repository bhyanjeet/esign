package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.EsignTransation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EsignTransation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EsignTransationRepository extends JpaRepository<EsignTransation, Long>, JpaSpecificationExecutor<EsignTransation> {

}
