package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.OrganisationTypeMaster;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link OrganisationTypeMaster} entity.
 */
public interface OrganisationTypeMasterSearchRepository extends ElasticsearchRepository<OrganisationTypeMaster, Long> {
}
