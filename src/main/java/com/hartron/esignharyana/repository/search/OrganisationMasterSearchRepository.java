package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.OrganisationMaster;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data Elasticsearch repository for the {@link OrganisationMaster} entity.
 */
public interface OrganisationMasterSearchRepository extends ElasticsearchRepository<OrganisationMaster, Long> {

    Page<OrganisationMaster> findAllByStatus(String status, Pageable pageable);

    Page<OrganisationMaster> findAllByStatusAndVerifiedBy(String status, String verifyBy, Pageable pageable);

    Optional<OrganisationMaster> findByUserId(Integer userId);

    Optional<OrganisationMaster> findByCreatedBy(String userLogin);

    Optional<OrganisationMaster> findOrganisationMasterByOrganisationIdCode(String oranisationIdCode);

    List<OrganisationMaster> findAllByOrganisationIdCode(String oranisationIdCode);
}
