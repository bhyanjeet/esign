package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.Otp;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.Optional;

/**
 * Spring Data Elasticsearch repository for the {@link Otp} entity.
 */
public interface OtpSearchRepository extends ElasticsearchRepository<Otp, Long> {

    Optional<Otp> findOtpByMobileNumber(Long mobileNumber);
}
