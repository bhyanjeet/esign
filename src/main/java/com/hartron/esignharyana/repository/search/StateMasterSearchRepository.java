package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.StateMaster;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link StateMaster} entity.
 */
public interface StateMasterSearchRepository extends ElasticsearchRepository<StateMaster, Long> {
}
