package com.hartron.esignharyana.repository.search;

import com.hartron.esignharyana.domain.UserAuthDetails;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link UserAuthDetails} entity.
 */
public interface UserAuthDetailsSearchRepository extends ElasticsearchRepository<UserAuthDetails, Long> {
}
