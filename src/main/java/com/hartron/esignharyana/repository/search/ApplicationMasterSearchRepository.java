package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.ApplicationMaster;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 * Spring Data Elasticsearch repository for the {@link ApplicationMaster} entity.
 */
public interface ApplicationMasterSearchRepository extends ElasticsearchRepository<ApplicationMaster, Long> {
    List<ApplicationMaster> findByCreatedBy(String userLogin);

    Page<ApplicationMaster> findApplicationByStatus(String status, Pageable pageable);
    List<ApplicationMaster> findAllByCreatedByAndStatus(String createdBy, String status);

    boolean findByApplicationIdCode(String applicationIdCode);
}

