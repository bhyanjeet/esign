package com.hartron.esignharyana.repository.search;

import com.hartron.esignharyana.domain.EsignError;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link EsignError} entity.
 */
public interface EsignErrorSearchRepository extends ElasticsearchRepository<EsignError, Long> {
}
