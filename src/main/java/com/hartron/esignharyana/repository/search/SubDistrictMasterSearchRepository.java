package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.SubDistrictMaster;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link SubDistrictMaster} entity.
 */
public interface SubDistrictMasterSearchRepository extends ElasticsearchRepository<SubDistrictMaster, Long> {
}
