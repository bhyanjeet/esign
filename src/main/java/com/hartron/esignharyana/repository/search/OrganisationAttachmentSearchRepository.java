package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.OrganisationAttachment;
import com.hartron.esignharyana.service.dto.OrganisationAttachmentDTO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 * Spring Data Elasticsearch repository for the {@link OrganisationAttachment} entity.
 */
public interface OrganisationAttachmentSearchRepository extends ElasticsearchRepository<OrganisationAttachment, Long> {

    List<OrganisationAttachment> findAllByOrganisationMasterId(Long id);
}
