package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.EsignTransation;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.Optional;

/**
 * Spring Data Elasticsearch repository for the {@link EsignTransation} entity.
 */
public interface EsignTransationSearchRepository extends ElasticsearchRepository<EsignTransation, Long> {

    Optional<EsignTransation> findEsignTransationByOrganisationIdCode(String organisationIdCode);

    Optional<EsignTransation> findByUserCodeId(String userCodeId);

    Optional<EsignTransation> findByUserCodeIdAndRequestType(String userCodeId, String esign_responde);

    Optional<EsignTransation> findByTxnId(String txn_eSignResponse);

    Optional<EsignTransation> findByTxnIdAndRequestType(String txnId, String requestType);
}
