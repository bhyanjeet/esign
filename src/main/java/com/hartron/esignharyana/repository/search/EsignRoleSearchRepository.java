package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.EsignRole;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link EsignRole} entity.
 */
public interface EsignRoleSearchRepository extends ElasticsearchRepository<EsignRole, Long> {
}
