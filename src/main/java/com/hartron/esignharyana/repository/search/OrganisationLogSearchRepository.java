package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.OrganisationLog;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link OrganisationLog} entity.
 */
public interface OrganisationLogSearchRepository extends ElasticsearchRepository<OrganisationLog, Long> {
}
