package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.DesignationMaster;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link DesignationMaster} entity.
 */
public interface DesignationMasterSearchRepository extends ElasticsearchRepository<DesignationMaster, Long> {
}
