package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.OrganisationDocument;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data Elasticsearch repository for the {@link OrganisationDocument} entity.
 */
public interface OrganisationDocumentSearchRepository extends ElasticsearchRepository<OrganisationDocument, Long> {

    List<OrganisationDocument> findOrganisationDocumentByOrganisationTypeMasterId(Long id);
}
