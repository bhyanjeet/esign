package com.hartron.esignharyana.repository.search;

import com.hartron.esignharyana.domain.ExternalUserEsignRequest;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.Optional;

/**
 * Spring Data Elasticsearch repository for the {@link ExternalUserEsignRequest} entity.
 */
public interface ExternalUserEsignRequestSearchRepository extends ElasticsearchRepository<ExternalUserEsignRequest, Long> {

    Optional<ExternalUserEsignRequest> findByTxn(String txn);



}
