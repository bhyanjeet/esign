package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.UserDetails;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data Elasticsearch repository for the {@link UserDetails} entity.
 */
public interface UserDetailsSearchRepository extends ElasticsearchRepository<UserDetails, Long> {

     List<UserDetails> findByCreatedBy(String userLogin);

     List<UserDetails> findByStatus(String status);

     Optional<UserDetails> findByUserIdCode(String userIdCode);

     List<UserDetails> findAllByUserIdCode(String userIdCode);

    Optional<UserDetails> findByUserOfficialEmail(String userOfficialEmail);

    Optional<UserDetails> findByUserPhoneMobile(String userPhoneMobile);

    Optional<UserDetails> findByPan(String pan);
}
