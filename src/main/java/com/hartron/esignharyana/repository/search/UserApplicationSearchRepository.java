package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.UserApplication;
import com.hartron.esignharyana.service.dto.UserApplicationDTO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data Elasticsearch repository for the {@link UserApplication} entity.
 */
public interface UserApplicationSearchRepository extends ElasticsearchRepository<UserApplication, Long> {

    List<UserApplication> findApplicationByUserLogin(String userLogin);
    List<UserApplication> findApplicationByUserId(Long userId);
    Optional<UserApplication> findUserApplicationByApplicationCodeIdAndUserCodeId(String applicationCodeId, String userCodeId);

}
