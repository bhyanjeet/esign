package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.BlockMaster;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link BlockMaster} entity.
 */
public interface BlockMasterSearchRepository extends ElasticsearchRepository<BlockMaster, Long> {
}
