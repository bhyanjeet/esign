package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.ApplicationLogs;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link ApplicationLogs} entity.
 */
public interface ApplicationLogsSearchRepository extends ElasticsearchRepository<ApplicationLogs, Long> {
}
