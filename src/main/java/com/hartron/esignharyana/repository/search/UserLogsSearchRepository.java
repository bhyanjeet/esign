package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.UserLogs;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link UserLogs} entity.
 */
public interface UserLogsSearchRepository extends ElasticsearchRepository<UserLogs, Long> {
}
