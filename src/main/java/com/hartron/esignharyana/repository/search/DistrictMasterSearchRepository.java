package com.hartron.esignharyana.repository.search;
import com.hartron.esignharyana.domain.DistrictMaster;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link DistrictMaster} entity.
 */
public interface DistrictMasterSearchRepository extends ElasticsearchRepository<DistrictMaster, Long> {
}
