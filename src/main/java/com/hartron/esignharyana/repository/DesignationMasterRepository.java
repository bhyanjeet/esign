package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.DesignationMaster;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DesignationMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DesignationMasterRepository extends JpaRepository<DesignationMaster, Long> {

}
