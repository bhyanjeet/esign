package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.UserLogs;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the UserLogs entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserLogsRepository extends JpaRepository<UserLogs, Long>, JpaSpecificationExecutor<UserLogs> {

}
