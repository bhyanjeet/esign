package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.UserDetails;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the UserDetails entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserDetailsRepository extends JpaRepository<UserDetails, Long> {

    Optional<UserDetails> findByUserOfficialEmail(String userOfficialEmail);

    Optional<UserDetails> findByUserPhoneMobile(String userPhoneMobile);

    Optional<UserDetails> findByPan(String pan);
}
