package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.OrganisationTypeMaster;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OrganisationTypeMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrganisationTypeMasterRepository extends JpaRepository<OrganisationTypeMaster, Long> {

}
