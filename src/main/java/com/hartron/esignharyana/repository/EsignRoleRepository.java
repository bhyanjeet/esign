package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.EsignRole;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EsignRole entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EsignRoleRepository extends JpaRepository<EsignRole, Long> {

}
