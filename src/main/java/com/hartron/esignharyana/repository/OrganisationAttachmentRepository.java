package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.OrganisationAttachment;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OrganisationAttachment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrganisationAttachmentRepository extends JpaRepository<OrganisationAttachment, Long> {

}
