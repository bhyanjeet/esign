package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.OrganisationLog;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OrganisationLog entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrganisationLogRepository extends JpaRepository<OrganisationLog, Long> {

}
