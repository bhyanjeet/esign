package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.SubDistrictMaster;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SubDistrictMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubDistrictMasterRepository extends JpaRepository<SubDistrictMaster, Long> {

}
