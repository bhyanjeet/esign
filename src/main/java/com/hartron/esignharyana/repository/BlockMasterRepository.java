package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.BlockMaster;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the BlockMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BlockMasterRepository extends JpaRepository<BlockMaster, Long> {

}
