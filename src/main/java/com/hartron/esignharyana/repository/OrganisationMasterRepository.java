package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.OrganisationMaster;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the OrganisationMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrganisationMasterRepository extends JpaRepository<OrganisationMaster, Long> {


    Optional<OrganisationMaster> findByOrganisationName(String organisationName);

    Optional<OrganisationMaster> findByOrganisationCorrespondenceEmail(String organisationCorrespondenceEmail);

    Optional<OrganisationMaster> findByNodalOfficerOfficialEmail(String organisationCorrespondenceEmail);

    Optional<OrganisationMaster> findByOrganisationCorrespondencePhone1(String organisationCorrespondencePhone1);

    Optional<OrganisationMaster> findByOrganisationNodalOfficerPhoneMobile(String organisationNodalOfficerPhoneMobile);

    Optional<OrganisationMaster> findByPan(String pan);
}
