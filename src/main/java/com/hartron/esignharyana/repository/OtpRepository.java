package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.Otp;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the Otp entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OtpRepository extends JpaRepository<Otp, Long> {
    Optional<Otp> findOtpByMobileNumber(Long mobileNumber);
}
