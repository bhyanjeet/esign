package com.hartron.esignharyana.repository;
import com.hartron.esignharyana.domain.DistrictMaster;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DistrictMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DistrictMasterRepository extends JpaRepository<DistrictMaster, Long> {

}
