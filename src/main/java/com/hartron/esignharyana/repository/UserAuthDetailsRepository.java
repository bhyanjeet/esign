package com.hartron.esignharyana.repository;

import com.hartron.esignharyana.domain.UserAuthDetails;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Spring Data  repository for the UserAuthDetails entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserAuthDetailsRepository extends JpaRepository<UserAuthDetails, Long> {
    List<UserAuthDetails> getAllByAuthToken(String token);
}
