package com.hartron.esignharyana.repository;

import com.hartron.esignharyana.domain.ExternalUserEsignRequest;

import com.hartron.esignharyana.service.dto.ExternalUserEsignRequestDTO;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the ExternalUserEsignRequest entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExternalUserEsignRequestRepository extends JpaRepository<ExternalUserEsignRequest, Long> {

    Optional<ExternalUserEsignRequestDTO> findByTxn(String txn);
}
