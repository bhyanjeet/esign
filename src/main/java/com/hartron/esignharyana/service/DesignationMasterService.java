package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.DesignationMasterDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.DesignationMaster}.
 */
public interface DesignationMasterService {

    /**
     * Save a designationMaster.
     *
     * @param designationMasterDTO the entity to save.
     * @return the persisted entity.
     */
    DesignationMasterDTO save(DesignationMasterDTO designationMasterDTO);

    /**
     * Get all the designationMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DesignationMasterDTO> findAll(Pageable pageable);


    /**
     * Get the "id" designationMaster.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DesignationMasterDTO> findOne(Long id);

    /**
     * Delete the "id" designationMaster.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the designationMaster corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DesignationMasterDTO> search(String query, Pageable pageable);
}
