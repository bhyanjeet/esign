package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.OtpDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.Otp}.
 */
public interface OtpService {

    /**
     * Save a otp.
     *
     * @param otpDTO the entity to save.
     * @return the persisted entity.
     */
    OtpDTO save(OtpDTO otpDTO);

    /**
     * Get all the otps.
     *
     * @return the list of entities.
     */
    List<OtpDTO> findAll();


    /**
     * Get the "id" otp.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OtpDTO> findOne(Long id);

    /**
     * Delete the "id" otp.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the otp corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<OtpDTO> search(String query);

    Boolean sendOtpByMobile(Long mobile);

    Boolean verifyOTPByMobile(Long mobile, Integer otp);
}
