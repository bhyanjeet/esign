package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.ApplicationMasterDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.ApplicationMaster}.
 */
public interface ApplicationMasterService {

    /**
     * Save a applicationMaster.
     *
     * @param applicationMasterDTO the entity to save.
     * @return the persisted entity.
     */
    ApplicationMasterDTO save(ApplicationMasterDTO applicationMasterDTO);

    /**
     * Get all the applicationMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ApplicationMasterDTO> findAll(Pageable pageable);


    /**
     * Get the "id" applicationMaster.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ApplicationMasterDTO> findOne(Long id);

    /**
     * Delete the "id" applicationMaster.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the applicationMaster corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ApplicationMasterDTO> search(String query, Pageable pageable);

    List<ApplicationMasterDTO> findApplicationByUserLogin(String userLogin);

    List<ApplicationMasterDTO> findApplicationRecordByStatus(String status);

    List<ApplicationMasterDTO> findAllRecordByCreatedByAndStatus(String createdBy, String status);
}
