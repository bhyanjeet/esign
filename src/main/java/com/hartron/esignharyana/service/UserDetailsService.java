package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.UserDetailsDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.UserDetails}.
 */
public interface UserDetailsService {

    /**
     * Save a userDetails.
     *
     * @param userDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    UserDetailsDTO save(UserDetailsDTO userDetailsDTO);

    /**
     * Get all the userDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserDetailsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" userDetails.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserDetailsDTO> findOne(Long id);

    Optional<UserDetailsDTO> findOneById(Long id);

    /**
     * Delete the "id" userDetails.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the userDetails corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserDetailsDTO> search(String query, Pageable pageable);

    List<UserDetailsDTO> findUserByUserLogin (String userLogin);

    List<UserDetailsDTO> findUserByUserCodeId (String userCodeId);

    List<UserDetailsDTO> findVerifiedUser (String status);

    Optional<UserDetailsDTO> findUserDetailsByUserCodeId(String userCodeId);


}
