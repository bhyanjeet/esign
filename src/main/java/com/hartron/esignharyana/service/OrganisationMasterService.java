package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.OrganisationMasterDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.OrganisationMaster}.
 */
public interface OrganisationMasterService {

    /**
     * Save a organisationMaster.
     *
     * @param organisationMasterDTO the entity to save.
     * @return the persisted entity.
     */
    OrganisationMasterDTO save(OrganisationMasterDTO organisationMasterDTO);

    /**
     * Get all the organisationMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrganisationMasterDTO> findAll(Pageable pageable);


    /**
     * Get the "id" organisationMaster.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrganisationMasterDTO> findOne(Long id);

    Optional<OrganisationMasterDTO> findOneById(Long id);

    /**
     * Delete the "id" organisationMaster.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the organisationMaster corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrganisationMasterDTO> search(String query, Pageable pageable);

    List<OrganisationMasterDTO> findAllRecordByStatusAndVerifyBy(String status, String verifyBy);

    List<OrganisationMasterDTO> findAllRecordByStatus(String status);

    Optional<OrganisationMasterDTO> findOneByUserId(Integer userId);

    Optional<OrganisationMasterDTO> findOneByUserLogin(String userLogin);

    Optional<OrganisationMasterDTO> findOrganisationByOranisationIdCode(String oranisationIdCode);

    List<OrganisationMasterDTO> findByOranisationIdCode(String oranisationIdCode);
}
