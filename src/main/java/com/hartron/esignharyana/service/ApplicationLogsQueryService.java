package com.hartron.esignharyana.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hartron.esignharyana.domain.ApplicationLogs;
import com.hartron.esignharyana.domain.*; // for static metamodels
import com.hartron.esignharyana.repository.ApplicationLogsRepository;
import com.hartron.esignharyana.repository.search.ApplicationLogsSearchRepository;
import com.hartron.esignharyana.service.dto.ApplicationLogsCriteria;
import com.hartron.esignharyana.service.dto.ApplicationLogsDTO;
import com.hartron.esignharyana.service.mapper.ApplicationLogsMapper;

/**
 * Service for executing complex queries for {@link ApplicationLogs} entities in the database.
 * The main input is a {@link ApplicationLogsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ApplicationLogsDTO} or a {@link Page} of {@link ApplicationLogsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ApplicationLogsQueryService extends QueryService<ApplicationLogs> {

    private final Logger log = LoggerFactory.getLogger(ApplicationLogsQueryService.class);

    private final ApplicationLogsRepository applicationLogsRepository;

    private final ApplicationLogsMapper applicationLogsMapper;

    private final ApplicationLogsSearchRepository applicationLogsSearchRepository;

    public ApplicationLogsQueryService(ApplicationLogsRepository applicationLogsRepository, ApplicationLogsMapper applicationLogsMapper, ApplicationLogsSearchRepository applicationLogsSearchRepository) {
        this.applicationLogsRepository = applicationLogsRepository;
        this.applicationLogsMapper = applicationLogsMapper;
        this.applicationLogsSearchRepository = applicationLogsSearchRepository;
    }

    /**
     * Return a {@link List} of {@link ApplicationLogsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ApplicationLogsDTO> findByCriteria(ApplicationLogsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ApplicationLogs> specification = createSpecification(criteria);
        return applicationLogsMapper.toDto(applicationLogsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ApplicationLogsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ApplicationLogsDTO> findByCriteria(ApplicationLogsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ApplicationLogs> specification = createSpecification(criteria);
        return applicationLogsRepository.findAll(specification, page)
            .map(applicationLogsMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ApplicationLogsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ApplicationLogs> specification = createSpecification(criteria);
        return applicationLogsRepository.count(specification);
    }

    /**
     * Function to convert {@link ApplicationLogsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ApplicationLogs> createSpecification(ApplicationLogsCriteria criteria) {
        Specification<ApplicationLogs> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ApplicationLogs_.id));
            }
            if (criteria.getActionTaken() != null) {
                specification = specification.and(buildStringSpecification(criteria.getActionTaken(), ApplicationLogs_.actionTaken));
            }
            if (criteria.getActionTakenBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getActionTakenBy(), ApplicationLogs_.actionTakenBy));
            }
            if (criteria.getActionTakenOnApplication() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getActionTakenOnApplication(), ApplicationLogs_.actionTakenOnApplication));
            }
            if (criteria.getActionTakenOnDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getActionTakenOnDate(), ApplicationLogs_.actionTakenOnDate));
            }
            if (criteria.getRemarks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemarks(), ApplicationLogs_.remarks));
            }
        }
        return specification;
    }
}
