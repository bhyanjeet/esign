package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.OrganisationTypeMasterDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.OrganisationTypeMaster}.
 */
public interface OrganisationTypeMasterService {

    /**
     * Save a organisationTypeMaster.
     *
     * @param organisationTypeMasterDTO the entity to save.
     * @return the persisted entity.
     */
    OrganisationTypeMasterDTO save(OrganisationTypeMasterDTO organisationTypeMasterDTO);

    /**
     * Get all the organisationTypeMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrganisationTypeMasterDTO> findAll(Pageable pageable);


    /**
     * Get the "id" organisationTypeMaster.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrganisationTypeMasterDTO> findOne(Long id);

    /**
     * Delete the "id" organisationTypeMaster.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the organisationTypeMaster corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrganisationTypeMasterDTO> search(String query, Pageable pageable);
}
