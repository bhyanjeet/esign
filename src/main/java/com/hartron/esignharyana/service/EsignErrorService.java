package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.EsignErrorDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.EsignError}.
 */
public interface EsignErrorService {

    /**
     * Save a esignError.
     *
     * @param esignErrorDTO the entity to save.
     * @return the persisted entity.
     */
    EsignErrorDTO save(EsignErrorDTO esignErrorDTO);

    /**
     * Get all the esignErrors.
     *
     * @return the list of entities.
     */
    List<EsignErrorDTO> findAll();

    /**
     * Get the "id" esignError.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EsignErrorDTO> findOne(Long id);

    /**
     * Delete the "id" esignError.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the esignError corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @return the list of entities.
     */
    List<EsignErrorDTO> search(String query);
}
