package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.OrganisationAttachmentDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.OrganisationAttachment}.
 */
public interface OrganisationAttachmentService {

    /**
     * Save a organisationAttachment.
     *
     * @param organisationAttachmentDTO the entity to save.
     * @return the persisted entity.
     */
    OrganisationAttachmentDTO save(OrganisationAttachmentDTO organisationAttachmentDTO);

    /**
     * Get all the organisationAttachments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrganisationAttachmentDTO> findAll(Pageable pageable);

    List<OrganisationAttachmentDTO> findAllByOrganisation(Long id);


    /**
     * Get the "id" organisationAttachment.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrganisationAttachmentDTO> findOne(Long id);

    /**
     * Delete the "id" organisationAttachment.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the organisationAttachment corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrganisationAttachmentDTO> search(String query, Pageable pageable);
}
