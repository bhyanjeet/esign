package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.EsignTransationDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.EsignTransation}.
 */
public interface EsignTransationService {

    /**
     * Save a esignTransation.
     *
     * @param esignTransationDTO the entity to save.
     * @return the persisted entity.
     */
    EsignTransationDTO save(EsignTransationDTO esignTransationDTO);

    /**
     * Get all the esignTransations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EsignTransationDTO> findAll(Pageable pageable);


    /**
     * Get the "id" esignTransation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EsignTransationDTO> findOne(Long id);

    /**
     * Delete the "id" esignTransation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the esignTransation corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EsignTransationDTO> search(String query, Pageable pageable);

    Optional<EsignTransationDTO> findEsignTransactionByorganisationIdCode(String organisationIdCode);

    EsignTransationDTO saveTransaction(EsignTransationDTO esignTransationDTO) throws IOException, ParserConfigurationException, SAXException;
}
