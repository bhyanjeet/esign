package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.DistrictMasterDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.DistrictMaster}.
 */
public interface DistrictMasterService {

    /**
     * Save a districtMaster.
     *
     * @param districtMasterDTO the entity to save.
     * @return the persisted entity.
     */
    DistrictMasterDTO save(DistrictMasterDTO districtMasterDTO);

    /**
     * Get all the districtMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DistrictMasterDTO> findAll(Pageable pageable);


    /**
     * Get the "id" districtMaster.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DistrictMasterDTO> findOne(Long id);

    /**
     * Delete the "id" districtMaster.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the districtMaster corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DistrictMasterDTO> search(String query, Pageable pageable);
}
