package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.EsignRoleDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.EsignRole}.
 */
public interface EsignRoleService {

    /**
     * Save a esignRole.
     *
     * @param esignRoleDTO the entity to save.
     * @return the persisted entity.
     */
    EsignRoleDTO save(EsignRoleDTO esignRoleDTO);

    /**
     * Get all the esignRoles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EsignRoleDTO> findAll(Pageable pageable);


    /**
     * Get the "id" esignRole.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EsignRoleDTO> findOne(Long id);

    /**
     * Delete the "id" esignRole.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the esignRole corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EsignRoleDTO> search(String query, Pageable pageable);
}
