package com.hartron.esignharyana.service.dto;
import com.hartron.esignharyana.config.Constants;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.OrganisationTypeMaster} entity.
 */
public class OrganisationTypeMasterDTO implements Serializable {

    private Long id;

    @NotNull
    private String organisationTypeDetail;

    @NotNull
    private String organisationTypeAbbreviation;

    private String createdBy;

    private LocalDate createdOn;

    private String lastUpdatedBy;

    private LocalDate lastUpdatedOn;

    private String verifiedBy;

    private LocalDate verifiedOn;

    private String remarks;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrganisationTypeDetail() {
        return organisationTypeDetail;
    }

    public void setOrganisationTypeDetail(String organisationTypeDetail) {
        this.organisationTypeDetail = organisationTypeDetail;
    }

    public String getOrganisationTypeAbbreviation() {
        return organisationTypeAbbreviation;
    }

    public void setOrganisationTypeAbbreviation(String organisationTypeAbbreviation) {
        this.organisationTypeAbbreviation = organisationTypeAbbreviation;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public LocalDate getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public LocalDate getVerifiedOn() {
        return verifiedOn;
    }

    public void setVerifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrganisationTypeMasterDTO organisationTypeMasterDTO = (OrganisationTypeMasterDTO) o;
        if (organisationTypeMasterDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), organisationTypeMasterDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OrganisationTypeMasterDTO{" +
            "id=" + getId() +
            ", organisationTypeDetail='" + getOrganisationTypeDetail() + "'" +
            ", organisationTypeAbbreviation='" + getOrganisationTypeAbbreviation() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastUpdatedBy='" + getLastUpdatedBy() + "'" +
            ", lastUpdatedOn='" + getLastUpdatedOn() + "'" +
            ", verifiedBy='" + getVerifiedBy() + "'" +
            ", verifiedOn='" + getVerifiedOn() + "'" +
            ", remarks='" + getRemarks() + "'" +
            "}";
    }
}
