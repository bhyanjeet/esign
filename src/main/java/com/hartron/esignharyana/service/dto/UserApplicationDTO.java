package com.hartron.esignharyana.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.UserApplication} entity.
 */
public class UserApplicationDTO implements Serializable {

    private Long id;

    private Long userId;

    private String userLogin;

    private String createdBy;

    private LocalDate createdOn;

    private String updatedBy;

    private LocalDate updatedOn;

    private String verifiedBy;

    private LocalDate verifiedOn;

    private String userCodeId;

    private String applicationCodeId;

    private Long applicationMasterId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDate getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDate updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public LocalDate getVerifiedOn() {
        return verifiedOn;
    }

    public void setVerifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getUserCodeId() {
        return userCodeId;
    }

    public void setUserCodeId(String userCodeId) {
        this.userCodeId = userCodeId;
    }

    public String getApplicationCodeId() {
        return applicationCodeId;
    }

    public void setApplicationCodeId(String applicationCodeId) {
        this.applicationCodeId = applicationCodeId;
    }

    public Long getApplicationMasterId() {
        return applicationMasterId;
    }

    public void setApplicationMasterId(Long applicationMasterId) {
        this.applicationMasterId = applicationMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserApplicationDTO userApplicationDTO = (UserApplicationDTO) o;
        if (userApplicationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userApplicationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserApplicationDTO{" +
            "id=" + getId() +
            ", userId=" + getUserId() +
            ", userLogin='" + getUserLogin() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", updatedOn='" + getUpdatedOn() + "'" +
            ", verifiedBy='" + getVerifiedBy() + "'" +
            ", verifiedOn='" + getVerifiedOn() + "'" +
            ", userCodeId='" + getUserCodeId() + "'" +
            ", applicationCodeId='" + getApplicationCodeId() + "'" +
            ", applicationMaster=" + getApplicationMasterId() +
            "}";
    }
}
