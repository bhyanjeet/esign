package com.hartron.esignharyana.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.EsignError} entity.
 */
public class EsignErrorDTO implements Serializable {

    private Long id;

    private String errorCode;

    private String errorMessage;

    private String errorStage;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorStage() {
        return errorStage;
    }

    public void setErrorStage(String errorStage) {
        this.errorStage = errorStage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EsignErrorDTO esignErrorDTO = (EsignErrorDTO) o;
        if (esignErrorDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), esignErrorDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EsignErrorDTO{" +
            "id=" + getId() +
            ", errorCode='" + getErrorCode() + "'" +
            ", errorMessage='" + getErrorMessage() + "'" +
            ", errorStage='" + getErrorStage() + "'" +
            "}";
    }
}
