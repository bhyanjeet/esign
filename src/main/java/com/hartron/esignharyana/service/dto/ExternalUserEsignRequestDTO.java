package com.hartron.esignharyana.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.ExternalUserEsignRequest} entity.
 */
public class ExternalUserEsignRequestDTO implements Serializable {

    private Long id;

    private String userCodeId;

    private String applicationIdCode;

    private String responseUrl;

    private String redirectUrl;

    private String ts;

    private String signerId;

    private String docInfo;

    private String docUrl;

    private String docHash;

    private String requestStatus;

    private String requestTime;

    private String aspId;

    private String aspIp;

    private String refData;

    public String getTxn() {
        return txn;
    }

    public void setTxn(String txn) {
        this.txn = txn;
    }

    private String txn;

    private String txnRef;

    public String getAspId() {
        return aspId;
    }

    public void setAspId(String aspId) {
        this.aspId = aspId;
    }

    public String getAspIp() {
        return aspIp;
    }

    public void setAspIp(String aspIp) {
        this.aspIp = aspIp;
    }

    public String getRefData() {
        return refData;
    }

    public void setRefData(String refData) {
        this.refData = refData;
    }

    public String getTxnRef() {
        return txnRef;
    }

    public void setTxnRef(String txnRef) {
        this.txnRef = txnRef;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserCodeId() {
        return userCodeId;
    }

    public void setUserCodeId(String userCodeId) {
        this.userCodeId = userCodeId;
    }

    public String getApplicationIdCode() {
        return applicationIdCode;
    }

    public void setApplicationIdCode(String applicationIdCode) {
        this.applicationIdCode = applicationIdCode;
    }

    public String getResponseUrl() {
        return responseUrl;
    }

    public void setResponseUrl(String responseUrl) {
        this.responseUrl = responseUrl;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getSignerId() {
        return signerId;
    }

    public void setSignerId(String signerId) {
        this.signerId = signerId;
    }

    public String getDocInfo() {
        return docInfo;
    }

    public void setDocInfo(String docInfo) {
        this.docInfo = docInfo;
    }

    public String getDocUrl() {
        return docUrl;
    }

    public void setDocUrl(String docUrl) {
        this.docUrl = docUrl;
    }

    public String getDocHash() {
        return docHash;
    }

    public void setDocHash(String docHash) {
        this.docHash = docHash;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ExternalUserEsignRequestDTO externalUserEsignRequestDTO = (ExternalUserEsignRequestDTO) o;
        if (externalUserEsignRequestDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), externalUserEsignRequestDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ExternalUserEsignRequestDTO{" +
            "id=" + getId() +
            ", userCodeId='" + getUserCodeId() + "'" +
            ", applicationIdCode='" + getApplicationIdCode() + "'" +
            ", responseUrl='" + getResponseUrl() + "'" +
            ", redirectUrl='" + getRedirectUrl() + "'" +
            ", ts='" + getTs() + "'" +
            ", signerId='" + getSignerId() + "'" +
            ", docInfo='" + getDocInfo() + "'" +
            ", docUrl='" + getDocUrl() + "'" +
            ", docHash='" + getDocHash() + "'" +
            ", requestStatus='" + getRequestStatus() + "'" +
            ", requestTime='" + getRequestTime() + "'" +
            ", txn='" + getTxn() + "'" +
            ", txnRef='" + getTxnRef() + "'" +
            "}";
    }
}
