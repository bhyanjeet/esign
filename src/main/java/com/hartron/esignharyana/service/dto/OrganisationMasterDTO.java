package com.hartron.esignharyana.service.dto;
import com.hartron.esignharyana.config.Constants;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.OrganisationMaster} entity.
 */
public class OrganisationMasterDTO implements Serializable {

    private Long id;

    @NotNull
    private String organisationName;

    @NotNull
    private String organisationAbbreviation;

    @NotNull
    private String organisationCorrespondenceEmail;

    @NotNull
    private String organisationCorrespondencePhone1;

    private String organisationCorrespondencePhone2;

    private String organisationCorrespondenceAddress;

    private String organisationWebsite;

    private String organisationNodalOfficerName;

    private String organisationNodalOfficerPhoneMobile;

    private String organisationNodalOfficerPhoneLandline;

    private String nodalOfficerOfficialEmail;

    private String organisationNodalOfficerAlternativeEmail;

    private String createdBy;

    private LocalDate createdOn;

    private String lastUpdatedBy;

    private LocalDate lastUpdatedOn;

    private String verifiedBy;

    private LocalDate verifiedOn;

    private String remarks;

    private Integer userId;

    private String stage;

    private String status;

    private String organisationIdCode;

    private String organisationEmployeeId;

    private LocalDate dateOfBirth;

    private String gender;

    private String pan;

    private String aadhaar;

    private String orgnisationEmpolyeeCardAttchment;

    private String panAttachment;

    private String photographAttachment;

    private String aadhaarAttachment;


    private Long organisationTypeMasterId;

    private Long stateMasterId;

    private Long districtMasterId;

    private Long subDistrictMasterId;

    private Long departmentId;

    private String pinCode;

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public Long getId() {
        return id;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrganisationName() {
        return organisationName;
    }

    public void setOrganisationName(String organisationName) {
        this.organisationName = organisationName;
    }

    public String getOrganisationAbbreviation() {
        return organisationAbbreviation;
    }

    public void setOrganisationAbbreviation(String organisationAbbreviation) {
        this.organisationAbbreviation = organisationAbbreviation;
    }

    public String getOrganisationCorrespondenceEmail() {
        return organisationCorrespondenceEmail;
    }

    public void setOrganisationCorrespondenceEmail(String organisationCorrespondenceEmail) {
        this.organisationCorrespondenceEmail = organisationCorrespondenceEmail;
    }

    public String getOrganisationCorrespondencePhone1() {
        return organisationCorrespondencePhone1;
    }

    public void setOrganisationCorrespondencePhone1(String organisationCorrespondencePhone1) {
        this.organisationCorrespondencePhone1 = organisationCorrespondencePhone1;
    }

    public String getOrganisationCorrespondencePhone2() {
        return organisationCorrespondencePhone2;
    }

    public void setOrganisationCorrespondencePhone2(String organisationCorrespondencePhone2) {
        this.organisationCorrespondencePhone2 = organisationCorrespondencePhone2;
    }

    public String getOrganisationCorrespondenceAddress() {
        return organisationCorrespondenceAddress;
    }

    public void setOrganisationCorrespondenceAddress(String organisationCorrespondenceAddress) {
        this.organisationCorrespondenceAddress = organisationCorrespondenceAddress;
    }

    public String getOrganisationWebsite() {
        return organisationWebsite;
    }

    public void setOrganisationWebsite(String organisationWebsite) {
        this.organisationWebsite = organisationWebsite;
    }

    public String getOrganisationNodalOfficerName() {
        return organisationNodalOfficerName;
    }

    public void setOrganisationNodalOfficerName(String organisationNodalOfficerName) {
        this.organisationNodalOfficerName = organisationNodalOfficerName;
    }

    public String getOrganisationNodalOfficerPhoneMobile() {
        return organisationNodalOfficerPhoneMobile;
    }

    public void setOrganisationNodalOfficerPhoneMobile(String organisationNodalOfficerPhoneMobile) {
        this.organisationNodalOfficerPhoneMobile = organisationNodalOfficerPhoneMobile;
    }

    public String getOrganisationNodalOfficerPhoneLandline() {
        return organisationNodalOfficerPhoneLandline;
    }

    public void setOrganisationNodalOfficerPhoneLandline(String organisationNodalOfficerPhoneLandline) {
        this.organisationNodalOfficerPhoneLandline = organisationNodalOfficerPhoneLandline;
    }

    public String getNodalOfficerOfficialEmail() {
        return nodalOfficerOfficialEmail;
    }

    public void setNodalOfficerOfficialEmail(String nodalOfficerOfficialEmail) {
        this.nodalOfficerOfficialEmail = nodalOfficerOfficialEmail;
    }

    public String getOrganisationNodalOfficerAlternativeEmail() {
        return organisationNodalOfficerAlternativeEmail;
    }

    public void setOrganisationNodalOfficerAlternativeEmail(String organisationNodalOfficerAlternativeEmail) {
        this.organisationNodalOfficerAlternativeEmail = organisationNodalOfficerAlternativeEmail;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public LocalDate getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public LocalDate getVerifiedOn() {
        return verifiedOn;
    }

    public void setVerifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrganisationIdCode() {
        return organisationIdCode;
    }

    public void setOrganisationIdCode(String organisationIdCode) {
        this.organisationIdCode = organisationIdCode;
    }

    public String getOrganisationEmployeeId() {
        return organisationEmployeeId;
    }

    public void setOrganisationEmployeeId(String organisationEmployeeId) {
        this.organisationEmployeeId = organisationEmployeeId;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getAadhaar() {
        return aadhaar;
    }

    public void setAadhaar(String aadhaar) {
        this.aadhaar = aadhaar;
    }

    public String getOrgnisationEmpolyeeCardAttchment() {
        return orgnisationEmpolyeeCardAttchment;
    }

    public void setOrgnisationEmpolyeeCardAttchment(String orgnisationEmpolyeeCardAttchment) {
        this.orgnisationEmpolyeeCardAttchment = orgnisationEmpolyeeCardAttchment;
    }

    public String getPanAttachment() {
        return panAttachment;
    }

    public void setPanAttachment(String panAttachment) {
        this.panAttachment = panAttachment;
    }

    public String getPhotographAttachment() {
        return photographAttachment;
    }

    public void setPhotographAttachment(String photographAttachment) {
        this.photographAttachment = photographAttachment;
    }

    public String getAadhaarAttachment() {
        return aadhaarAttachment;
    }

    public void setAadhaarAttachment(String aadhaarAttachment) {
        this.aadhaarAttachment = aadhaarAttachment;
    }

    public Long getOrganisationTypeMasterId() {
        return organisationTypeMasterId;
    }

    public void setOrganisationTypeMasterId(Long organisationTypeMasterId) {
        this.organisationTypeMasterId = organisationTypeMasterId;
    }

    public Long getStateMasterId() {
        return stateMasterId;
    }

    public void setStateMasterId(Long stateMasterId) {
        this.stateMasterId = stateMasterId;
    }

    public Long getDistrictMasterId() {
        return districtMasterId;
    }

    public void setDistrictMasterId(Long districtMasterId) {
        this.districtMasterId = districtMasterId;
    }

    public Long getSubDistrictMasterId() {
        return subDistrictMasterId;
    }

    public void setSubDistrictMasterId(Long subDistrictMasterId) {
        this.subDistrictMasterId = subDistrictMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrganisationMasterDTO organisationMasterDTO = (OrganisationMasterDTO) o;
        if (organisationMasterDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), organisationMasterDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OrganisationMasterDTO{" +
            "id=" + getId() +
            ", organisationName='" + getOrganisationName() + "'" +
            ", organisationAbbreviation='" + getOrganisationAbbreviation() + "'" +
            ", organisationCorrespondenceEmail='" + getOrganisationCorrespondenceEmail() + "'" +
            ", organisationCorrespondencePhone1='" + getOrganisationCorrespondencePhone1() + "'" +
            ", organisationCorrespondencePhone2='" + getOrganisationCorrespondencePhone2() + "'" +
            ", organisationCorrespondenceAddress='" + getOrganisationCorrespondenceAddress() + "'" +
            ", organisationWebsite='" + getOrganisationWebsite() + "'" +
            ", organisationNodalOfficerName='" + getOrganisationNodalOfficerName() + "'" +
            ", organisationNodalOfficerPhoneMobile='" + getOrganisationNodalOfficerPhoneMobile() + "'" +
            ", organisationNodalOfficerPhoneLandline='" + getOrganisationNodalOfficerPhoneLandline() + "'" +
            ", nodalOfficerOfficialEmail='" + getNodalOfficerOfficialEmail() + "'" +
            ", organisationNodalOfficerAlternativeEmail='" + getOrganisationNodalOfficerAlternativeEmail() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastUpdatedBy='" + getLastUpdatedBy() + "'" +
            ", lastUpdatedOn='" + getLastUpdatedOn() + "'" +
            ", verifiedBy='" + getVerifiedBy() + "'" +
            ", verifiedOn='" + getVerifiedOn() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", userId=" + getUserId() +
            ", stage='" + getStage() + "'" +
            ", status='" + getStatus() + "'" +
            ", organisationIdCode='" + getOrganisationIdCode() + "'" +
            ", organisationEmployeeId='" + getOrganisationEmployeeId() + "'" +
            ", dateOfBirth='" + getDateOfBirth() + "'" +
            ", gender='" + getGender() + "'" +
            ", pan='" + getPan() + "'" +
            ", aadhaar='" + getAadhaar() + "'" +
            ", orgnisationEmpolyeeCardAttchment='" + getOrgnisationEmpolyeeCardAttchment() + "'" +
            ", panAttachment='" + getPanAttachment() + "'" +
            ", photographAttachment='" + getPhotographAttachment() + "'" +
            ", aadhaarAttachment='" + getAadhaarAttachment() + "'" +
            ", organisationTypeMaster=" + getOrganisationTypeMasterId() +
            ", stateMaster=" + getStateMasterId() +
            ", districtMaster=" + getDistrictMasterId() +
            ", subDistrictMaster=" + getSubDistrictMasterId() +
            ", department=" + getDepartmentId() +
            ", pinCode=" + getPinCode() +
            "}";
    }
}
