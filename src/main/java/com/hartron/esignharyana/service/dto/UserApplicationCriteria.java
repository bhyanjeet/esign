package com.hartron.esignharyana.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.hartron.esignharyana.domain.UserApplication} entity. This class is used
 * in {@link com.hartron.esignharyana.web.rest.UserApplicationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /user-applications?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UserApplicationCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter userId;

    private StringFilter userLogin;

    private StringFilter createdBy;

    private LocalDateFilter createdOn;

    private StringFilter updatedBy;

    private LocalDateFilter updatedOn;

    private StringFilter verifiedBy;

    private LocalDateFilter verifiedOn;

    private StringFilter userCodeId;

    private StringFilter applicationCodeId;

    private LongFilter applicationMasterId;

    public UserApplicationCriteria(){
    }

    public UserApplicationCriteria(UserApplicationCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.userLogin = other.userLogin == null ? null : other.userLogin.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdOn = other.createdOn == null ? null : other.createdOn.copy();
        this.updatedBy = other.updatedBy == null ? null : other.updatedBy.copy();
        this.updatedOn = other.updatedOn == null ? null : other.updatedOn.copy();
        this.verifiedBy = other.verifiedBy == null ? null : other.verifiedBy.copy();
        this.verifiedOn = other.verifiedOn == null ? null : other.verifiedOn.copy();
        this.userCodeId = other.userCodeId == null ? null : other.userCodeId.copy();
        this.applicationCodeId = other.applicationCodeId == null ? null : other.applicationCodeId.copy();
        this.applicationMasterId = other.applicationMasterId == null ? null : other.applicationMasterId.copy();
    }

    @Override
    public UserApplicationCriteria copy() {
        return new UserApplicationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public StringFilter getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(StringFilter userLogin) {
        this.userLogin = userLogin;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateFilter getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateFilter createdOn) {
        this.createdOn = createdOn;
    }

    public StringFilter getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(StringFilter updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDateFilter getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDateFilter updatedOn) {
        this.updatedOn = updatedOn;
    }

    public StringFilter getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(StringFilter verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public LocalDateFilter getVerifiedOn() {
        return verifiedOn;
    }

    public void setVerifiedOn(LocalDateFilter verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public StringFilter getUserCodeId() {
        return userCodeId;
    }

    public void setUserCodeId(StringFilter userCodeId) {
        this.userCodeId = userCodeId;
    }

    public StringFilter getApplicationCodeId() {
        return applicationCodeId;
    }

    public void setApplicationCodeId(StringFilter applicationCodeId) {
        this.applicationCodeId = applicationCodeId;
    }

    public LongFilter getApplicationMasterId() {
        return applicationMasterId;
    }

    public void setApplicationMasterId(LongFilter applicationMasterId) {
        this.applicationMasterId = applicationMasterId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserApplicationCriteria that = (UserApplicationCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(userLogin, that.userLogin) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdOn, that.createdOn) &&
            Objects.equals(updatedBy, that.updatedBy) &&
            Objects.equals(updatedOn, that.updatedOn) &&
            Objects.equals(verifiedBy, that.verifiedBy) &&
            Objects.equals(verifiedOn, that.verifiedOn) &&
            Objects.equals(userCodeId, that.userCodeId) &&
            Objects.equals(applicationCodeId, that.applicationCodeId) &&
            Objects.equals(applicationMasterId, that.applicationMasterId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        userId,
        userLogin,
        createdBy,
        createdOn,
        updatedBy,
        updatedOn,
        verifiedBy,
        verifiedOn,
        userCodeId,
        applicationCodeId,
        applicationMasterId
        );
    }

    @Override
    public String toString() {
        return "UserApplicationCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (userLogin != null ? "userLogin=" + userLogin + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (createdOn != null ? "createdOn=" + createdOn + ", " : "") +
                (updatedBy != null ? "updatedBy=" + updatedBy + ", " : "") +
                (updatedOn != null ? "updatedOn=" + updatedOn + ", " : "") +
                (verifiedBy != null ? "verifiedBy=" + verifiedBy + ", " : "") +
                (verifiedOn != null ? "verifiedOn=" + verifiedOn + ", " : "") +
                (userCodeId != null ? "userCodeId=" + userCodeId + ", " : "") +
                (applicationCodeId != null ? "applicationCodeId=" + applicationCodeId + ", " : "") +
                (applicationMasterId != null ? "applicationMasterId=" + applicationMasterId + ", " : "") +
            "}";
    }

}
