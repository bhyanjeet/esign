package com.hartron.esignharyana.service.dto;
import com.hartron.esignharyana.config.Constants;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.ApplicationLogs} entity.
 */
public class ApplicationLogsDTO implements Serializable {

    private Long id;

    private String actionTaken;

    private String actionTakenBy;

    private Long actionTakenOnApplication;

    private LocalDate actionTakenOnDate;

    private String remarks;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    public String getActionTakenBy() {
        return actionTakenBy;
    }

    public void setActionTakenBy(String actionTakenBy) {
        this.actionTakenBy = actionTakenBy;
    }

    public Long getActionTakenOnApplication() {
        return actionTakenOnApplication;
    }

    public void setActionTakenOnApplication(Long actionTakenOnApplication) {
        this.actionTakenOnApplication = actionTakenOnApplication;
    }

    public LocalDate getActionTakenOnDate() {
        return actionTakenOnDate;
    }

    public void setActionTakenOnDate(LocalDate actionTakenOnDate) {
        this.actionTakenOnDate = actionTakenOnDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ApplicationLogsDTO applicationLogsDTO = (ApplicationLogsDTO) o;
        if (applicationLogsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), applicationLogsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ApplicationLogsDTO{" +
            "id=" + getId() +
            ", actionTaken='" + getActionTaken() + "'" +
            ", actionTakenBy='" + getActionTakenBy() + "'" +
            ", actionTakenOnApplication=" + getActionTakenOnApplication() +
            ", actionTakenOnDate='" + getActionTakenOnDate() + "'" +
            ", remarks='" + getRemarks() + "'" +
            "}";
    }
}
