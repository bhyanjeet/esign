package com.hartron.esignharyana.service.dto;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.OrganisationDocument} entity.
 */
public class OrganisationDocumentDTO implements Serializable {

    private Long id;

    @NotNull
    private String documentTitle;

    private String createdBy;

    private LocalDate createdOn;

    private String verifiedBy;

    private LocalDate verifiedOn;

    private String remarks;

    private String documentRelatedTo;


    private Long organisationTypeMasterId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocumentTitle() {
        return documentTitle;
    }

    public void setDocumentTitle(String documentTitle) {
        this.documentTitle = documentTitle;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public LocalDate getVerifiedOn() {
        return verifiedOn;
    }

    public void setVerifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDocumentRelatedTo() {
        return documentRelatedTo;
    }

    public void setDocumentRelatedTo(String documentRelatedTo) {
        this.documentRelatedTo = documentRelatedTo;
    }

    public Long getOrganisationTypeMasterId() {
        return organisationTypeMasterId;
    }

    public void setOrganisationTypeMasterId(Long organisationTypeMasterId) {
        this.organisationTypeMasterId = organisationTypeMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrganisationDocumentDTO organisationDocumentDTO = (OrganisationDocumentDTO) o;
        if (organisationDocumentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), organisationDocumentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OrganisationDocumentDTO{" +
            "id=" + getId() +
            ", documentTitle='" + getDocumentTitle() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", verifiedBy='" + getVerifiedBy() + "'" +
            ", verifiedOn='" + getVerifiedOn() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", documentRelatedTo='" + getDocumentRelatedTo() + "'" +
            ", organisationTypeMaster=" + getOrganisationTypeMasterId() +
            "}";
    }
}
