package com.hartron.esignharyana.service.dto;
import java.io.Serializable;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.UserDetails} entity.
 */
public class EsignFormRequestDTO implements Serializable {


    private String aspid;

    public String getAspid() {
        return aspid;
    }

    public void setAspid(String aspid) {
        this.aspid = aspid;
    }

    public String getReq_data() {
        return req_data;
    }

    public void setReq_data(String req_data) {
        this.req_data = req_data;
    }

    public String getTxnref() {
        return txnref;
    }

    public void setTxnref(String txnref) {
        this.txnref = txnref;
    }

    public String getAsp_ip() {
        return asp_ip;
    }

    public void setAsp_ip(String asp_ip) {
        this.asp_ip = asp_ip;
    }

    private String req_data;


    private String txnref;

    private String asp_ip;


}
