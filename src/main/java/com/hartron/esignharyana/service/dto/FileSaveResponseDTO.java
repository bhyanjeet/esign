package com.hartron.esignharyana.service.dto;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

public class FileSaveResponseDTO implements Serializable {

    private String panAttachmentName;

    private String photoGraphAttachmentName;

    private String empIdCardAttachmentName;

    public String getPanAttachmentName() {
        return panAttachmentName;
    }

    public void setPanAttachmentName(String panAttachmentName) {
        this.panAttachmentName = panAttachmentName;
    }

    public String getPhotoGraphAttachmentName() {
        return photoGraphAttachmentName;
    }

    public void setPhotoGraphAttachmentName(String photoGraphAttachmentName) {
        this.photoGraphAttachmentName = photoGraphAttachmentName;
    }

    public String getEmpIdCardAttachmentName() {
        return empIdCardAttachmentName;
    }

    public void setEmpIdCardAttachmentName(String empIdCardAttachmentName) {
        this.empIdCardAttachmentName = empIdCardAttachmentName;
    }
}
