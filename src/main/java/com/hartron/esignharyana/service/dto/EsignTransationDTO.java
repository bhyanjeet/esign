package com.hartron.esignharyana.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.EsignTransation} entity.
 */
public class EsignTransationDTO implements Serializable {

    private Long id;

    private String txnId;

    private String userCodeId;

    private String applicationCodeId;

    private String orgResponseSuccessURL;

    private String orgResponseFailURL;

    private String orgStatus;

    private LocalDate requestDate;

    private String organisationIdCode;

    private String signerId;

    private String requestType;

    private String requestStatus;

    private String responseType;

    private String responseStatus;

    private String responseCode;

    private String requestXml;

    private String responseXml;

    private LocalDate responseDate;

    private String ts;

    public String getReqData() {
        return reqData;
    }

    public void setReqData(String reqData) {
        this.reqData = reqData;
    }

    private String reqData;




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getUserCodeId() {
        return userCodeId;
    }

    public void setUserCodeId(String userCodeId) {
        this.userCodeId = userCodeId;
    }

    public String getApplicationCodeId() {
        return applicationCodeId;
    }

    public void setApplicationCodeId(String applicationCodeId) {
        this.applicationCodeId = applicationCodeId;
    }

    public String getOrgResponseSuccessURL() {
        return orgResponseSuccessURL;
    }

    public void setOrgResponseSuccessURL(String orgResponseSuccessURL) {
        this.orgResponseSuccessURL = orgResponseSuccessURL;
    }

    public String getOrgResponseFailURL() {
        return orgResponseFailURL;
    }

    public void setOrgResponseFailURL(String orgResponseFailURL) {
        this.orgResponseFailURL = orgResponseFailURL;
    }

    public String getOrgStatus() {
        return orgStatus;
    }

    public void setOrgStatus(String orgStatus) {
        this.orgStatus = orgStatus;
    }

    public LocalDate getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(LocalDate requestDate) {
        this.requestDate = requestDate;
    }

    public String getOrganisationIdCode() {
        return organisationIdCode;
    }

    public void setOrganisationIdCode(String organisationIdCode) {
        this.organisationIdCode = organisationIdCode;
    }

    public String getSignerId() {
        return signerId;
    }

    public void setSignerId(String signerId) {
        this.signerId = signerId;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getRequestXml() {
        return requestXml;
    }

    public void setRequestXml(String requestXml) {
        this.requestXml = requestXml;
    }

    public String getResponseXml() {
        return responseXml;
    }

    public void setResponseXml(String responseXml) {
        this.responseXml = responseXml;
    }

    public LocalDate getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(LocalDate responseDate) {
        this.responseDate = responseDate;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EsignTransationDTO esignTransationDTO = (EsignTransationDTO) o;
        if (esignTransationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), esignTransationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EsignTransationDTO{" +
            "id=" + getId() +
            ", txnId='" + getTxnId() + "'" +
            ", userCodeId='" + getUserCodeId() + "'" +
            ", applicationCodeId='" + getApplicationCodeId() + "'" +
            ", orgResponseSuccessURL='" + getOrgResponseSuccessURL() + "'" +
            ", orgResponseFailURL='" + getOrgResponseFailURL() + "'" +
            ", orgStatus='" + getOrgStatus() + "'" +
            ", requestDate='" + getRequestDate() + "'" +
            ", organisationIdCode='" + getOrganisationIdCode() + "'" +
            ", signerId='" + getSignerId() + "'" +
            ", requestType='" + getRequestType() + "'" +
            ", requestStatus='" + getRequestStatus() + "'" +
            ", responseType='" + getResponseType() + "'" +
            ", responseStatus='" + getResponseStatus() + "'" +
            ", responseCode='" + getResponseCode() + "'" +
            ", requestXml='" + getRequestXml() + "'" +
            ", responseXml='" + getResponseXml() + "'" +
            ", responseDate='" + getResponseDate() + "'" +
            ", ts='" + getTs() + "'" +
            "}";
    }
}
