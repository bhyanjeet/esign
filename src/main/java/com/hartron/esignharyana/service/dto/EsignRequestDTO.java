package com.hartron.esignharyana.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.UserDetails} entity.
 */
public class EsignRequestDTO implements Serializable {


    private String userCodeId;

    private String signerid;

    private String responseUrl;


    private String redirectUrl;

    private String docInfo;

    public String getUserCodeId() {
        return userCodeId;
    }

    public String getSignerid() {
        return signerid;
    }

    public void setUserCodeId(String userCodeId) {
        this.userCodeId = userCodeId;
    }

    public void setSignerid(String signerid) {
        this.signerid = signerid;
    }

    public String getResponseUrl() {
        return responseUrl;
    }

    public void setResponseUrl(String responseUrl) {
        this.responseUrl = responseUrl;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getDocInfo() {
        return docInfo;
    }

    public void setDocInfo(String docInfo) {
        this.docInfo = docInfo;
    }

    private String aspid;

    public String getAspid() {
        return aspid;
    }

    public void setAspid(String aspid) {
        this.aspid = aspid;
    }

    public String getReq_data() {
        return req_data;
    }

    public void setReq_data(String req_data) {
        this.req_data = req_data;
    }

    public String getTxnref() {
        return txnref;
    }

    public void setTxnref(String txnref) {
        this.txnref = txnref;
    }

    public String getAsp_ip() {
        return asp_ip;
    }

    public void setAsp_ip(String asp_ip) {
        this.asp_ip = asp_ip;
    }

    private String req_data;


    private String txnref;

    private String asp_ip;
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EsignRequestDTO esignRequestDTO = (EsignRequestDTO) o;
        if (esignRequestDTO.getSignerid() == null || getSignerid() == null) {
            return false;
        }
        return Objects.equals(getSignerid(), esignRequestDTO.getSignerid());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getSignerid());
    }

    @Override
    public String toString() {
        return "EsignRequestDTO{" +
            "signerid=" + getSignerid() +
            ", responseUrl='" + getResponseUrl() + "'" +
            ", redirectUrl='" + getRedirectUrl() + "'" +
            ", docInfo='" + getDocInfo() + "'" +

            "}";
    }


}
