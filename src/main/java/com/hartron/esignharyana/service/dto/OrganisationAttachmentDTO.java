package com.hartron.esignharyana.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.OrganisationAttachment} entity.
 */
public class OrganisationAttachmentDTO implements Serializable {

    private Long id;

    private String attachment;


    private Long organisationMasterId;

    private Long organisationDocumentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public Long getOrganisationMasterId() {
        return organisationMasterId;
    }

    public void setOrganisationMasterId(Long organisationMasterId) {
        this.organisationMasterId = organisationMasterId;
    }

    public Long getOrganisationDocumentId() {
        return organisationDocumentId;
    }

    public void setOrganisationDocumentId(Long organisationDocumentId) {
        this.organisationDocumentId = organisationDocumentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrganisationAttachmentDTO organisationAttachmentDTO = (OrganisationAttachmentDTO) o;
        if (organisationAttachmentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), organisationAttachmentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OrganisationAttachmentDTO{" +
            "id=" + getId() +
            ", attachment='" + getAttachment() + "'" +
            ", organisationMaster=" + getOrganisationMasterId() +
            ", organisationDocument=" + getOrganisationDocumentId() +
            "}";
    }
}
