package com.hartron.esignharyana.service.dto;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.Otp} entity.
 */
public class OtpDTO implements Serializable {

    private Long id;

    private Integer otp;

    private Long mobileNumber;

    private ZonedDateTime requestTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getOtp() {
        return otp;
    }

    public void setOtp(Integer otp) {
        this.otp = otp;
    }

    public Long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(Long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public ZonedDateTime getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(ZonedDateTime requestTime) {
        this.requestTime = requestTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OtpDTO otpDTO = (OtpDTO) o;
        if (otpDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), otpDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OtpDTO{" +
            "id=" + getId() +
            ", otp=" + getOtp() +
            ", mobileNumber=" + getMobileNumber() +
            ", requestTime='" + getRequestTime() + "'" +
            "}";
    }
}
