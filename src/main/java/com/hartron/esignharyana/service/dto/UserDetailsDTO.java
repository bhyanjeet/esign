package com.hartron.esignharyana.service.dto;
import com.hartron.esignharyana.config.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.UserDetails} entity.
 */
public class UserDetailsDTO implements Serializable {

    private Long id;

    private String userFullName;

    @NotNull
    private String userPhoneMobile;

    private String userPhoneLandline;

    @NotNull
    private String userOfficialEmail;

    private String userAlternativeEmail;

    private String userLoginPassword;

    private String userLoginPasswordSalt;

    private String userTransactionPassword;

    private String userTransactionPasswordSalt;

    private String createdBy;

    private LocalDate createdOn;

    private String lastUpdatedBy;

    private LocalDate lastUpdatedOn;

    private String verifiedBy;

    private LocalDate verifiedOn;

    private String remarks;

    private String userIdCode;

    private String status;

    private String username;

    private String userPIN;

    private String organisationUnit;

    private String address;

    private String counrty;

    private String postalCode;

    private String photograph;

    private LocalDate dob;

    private String gender;

    private String pan;

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    private String accountStatus;

    public String getSignerId() {
        return signerId;
    }

    public void setSignerId(String signerId) {
        this.signerId = signerId;
    }

    public String getLastAction() {
        return lastAction;
    }

    public void setLastAction(String lastAction) {
        this.lastAction = lastAction;
    }

    private String aadhaar;

    private String signerId;

    private String lastAction;

    private int applicationUserMasterId[];

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId=employeeId;
    }

    public String getEmployeeCardAttachment() {
        return employeeCardAttachment;
    }

    public void setEmployeeCardAttachment(String employeeCardAttachment) {
        this.employeeCardAttachment=employeeCardAttachment;
    }

    private String panAttachment;

    private String employeeId;

    private String employeeCardAttachment;

    public String getDesignationName() {
        return designationName;
    }

    public void setDesignationName(String designationName) {
        this.designationName=designationName;
    }

    private Long designationMasterId;

    private String designationName;

    private Long organisationMasterId;

    private Long esignRoleId;

    private Long applicationMasterId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getUserPhoneMobile() {
        return userPhoneMobile;
    }

    public void setUserPhoneMobile(String userPhoneMobile) {
        this.userPhoneMobile = userPhoneMobile;
    }

    public String getUserPhoneLandline() {
        return userPhoneLandline;
    }

    public void setUserPhoneLandline(String userPhoneLandline) {
        this.userPhoneLandline = userPhoneLandline;
    }

    public String getUserOfficialEmail() {
        return userOfficialEmail;
    }

    public void setUserOfficialEmail(String userOfficialEmail) {
        this.userOfficialEmail = userOfficialEmail;
    }

    public String getUserAlternativeEmail() {
        return userAlternativeEmail;
    }

    public void setUserAlternativeEmail(String userAlternativeEmail) {
        this.userAlternativeEmail = userAlternativeEmail;
    }

    public String getUserLoginPassword() {
        return userLoginPassword;
    }

    public void setUserLoginPassword(String userLoginPassword) {
        this.userLoginPassword = userLoginPassword;
    }

    public String getUserLoginPasswordSalt() {
        return userLoginPasswordSalt;
    }

    public void setUserLoginPasswordSalt(String userLoginPasswordSalt) {
        this.userLoginPasswordSalt = userLoginPasswordSalt;
    }

    public String getUserTransactionPassword() {
        return userTransactionPassword;
    }

    public void setUserTransactionPassword(String userTransactionPassword) {
        this.userTransactionPassword = userTransactionPassword;
    }

    public String getUserTransactionPasswordSalt() {
        return userTransactionPasswordSalt;
    }

    public void setUserTransactionPasswordSalt(String userTransactionPasswordSalt) {
        this.userTransactionPasswordSalt = userTransactionPasswordSalt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public LocalDate getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public LocalDate getVerifiedOn() {
        return verifiedOn;
    }

    public void setVerifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUserIdCode() {
        return userIdCode;
    }

    public void setUserIdCode(String userIdCode) {
        this.userIdCode = userIdCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserPIN() {
        return userPIN;
    }

    public void setUserPIN(String userPIN) {
        this.userPIN = userPIN;
    }

    public String getOrganisationUnit() {
        return organisationUnit;
    }

    public void setOrganisationUnit(String organisationUnit) {
        this.organisationUnit = organisationUnit;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCounrty() {
        return counrty;
    }

    public void setCounrty(String counrty) {
        this.counrty = counrty;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhotograph() {
        return photograph;
    }

    public void setPhotograph(String photograph) {
        this.photograph = photograph;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getAadhaar() {
        return aadhaar;
    }

    public void setAadhaar(String aadhaar) {
        this.aadhaar = aadhaar;
    }

    public String getPanAttachment() {
        return panAttachment;
    }

    public void setPanAttachment(String panAttachment) {
        this.panAttachment = panAttachment;
    }

    public Long getDesignationMasterId() {
        return designationMasterId;
    }

    public void setDesignationMasterId(Long designationMasterId) {
        this.designationMasterId = designationMasterId;
    }

    public Long getOrganisationMasterId() {
        return organisationMasterId;
    }

    public void setOrganisationMasterId(Long organisationMasterId) {
        this.organisationMasterId = organisationMasterId;
    }

    public Long getEsignRoleId() {
        return esignRoleId;
    }

    public void setEsignRoleId(Long esignRoleId) {
        this.esignRoleId = esignRoleId;
    }

    public Long getApplicationMasterId() {
        return applicationMasterId;
    }

    public void setApplicationMasterId(Long applicationMasterId) {
        this.applicationMasterId = applicationMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserDetailsDTO userDetailsDTO = (UserDetailsDTO) o;
        if (userDetailsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userDetailsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserDetailsDTO{" +
            "id=" + getId() +
            ", userFullName='" + getUserFullName() + "'" +
            ", userPhoneMobile='" + getUserPhoneMobile() + "'" +
            ", userPhoneLandline='" + getUserPhoneLandline() + "'" +
            ", userOfficialEmail='" + getUserOfficialEmail() + "'" +
            ", userAlternativeEmail='" + getUserAlternativeEmail() + "'" +
            ", userLoginPassword='" + getUserLoginPassword() + "'" +
            ", userLoginPasswordSalt='" + getUserLoginPasswordSalt() + "'" +
            ", userTransactionPassword='" + getUserTransactionPassword() + "'" +
            ", userTransactionPasswordSalt='" + getUserTransactionPasswordSalt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastUpdatedBy='" + getLastUpdatedBy() + "'" +
            ", lastUpdatedOn='" + getLastUpdatedOn() + "'" +
            ", verifiedBy='" + getVerifiedBy() + "'" +
            ", verifiedOn='" + getVerifiedOn() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", userIdCode='" + getUserIdCode() + "'" +
            ", status='" + getStatus() + "'" +
            ", username='" + getUsername() + "'" +
            ", userPIN='" + getUserPIN() + "'" +
            ", organisationUnit='" + getOrganisationUnit() + "'" +
            ", address='" + getAddress() + "'" +
            ", counrty='" + getCounrty() + "'" +
            ", postalCode='" + getPostalCode() + "'" +
            ", photograph='" + getPhotograph() + "'" +
            ", dob='" + getDob() + "'" +
            ", gender='" + getGender() + "'" +
            ", pan='" + getPan() + "'" +
            ", aadhaar='" + getAadhaar() + "'" +
            ", panAttachment='" + getPanAttachment() + "'" +
            ", employeeId='" + getEmployeeId() + "'" +
            ", employeeCardAttachment='" + getEmployeeCardAttachment() + "'" +
            ", designationMaster=" + getDesignationMasterId() +
            ", designationName=" + getDesignationName() +
            ", organisationMaster=" + getOrganisationMasterId() +
            ", esignRole=" + getEsignRoleId() +
            ", applicationMaster=" + getApplicationMasterId() +
            ", signerId=" + getSignerId() +
            ", lastAction=" + getLastAction() +
            ", accountStatus=" + getAccountStatus() +
            "}";
    }
    public int[] getApplicationUserMasterId() {
        return applicationUserMasterId;
    }

    public void setApplicationUserMasterId(int[] applicationUserMasterId) {
        this.applicationUserMasterId = applicationUserMasterId;
    }
}
