package com.hartron.esignharyana.service.dto;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

public class MinioFileSaveDTO implements Serializable {

    private String fileName;

    private MultipartFile file;

    private String fileType;

     public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
