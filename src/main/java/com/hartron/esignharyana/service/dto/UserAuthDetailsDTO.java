package com.hartron.esignharyana.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.UserAuthDetails} entity.
 */
public class UserAuthDetailsDTO implements Serializable {

    private Long id;

    private String login;

    private String device;

    private String authToken;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserAuthDetailsDTO userAuthDetailsDTO = (UserAuthDetailsDTO) o;
        if (userAuthDetailsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userAuthDetailsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserAuthDetailsDTO{" +
            "id=" + getId() +
            ", login='" + getLogin() + "'" +
            ", device='" + getDevice() + "'" +
            ", authToken='" + getAuthToken() + "'" +
            "}";
    }
}
