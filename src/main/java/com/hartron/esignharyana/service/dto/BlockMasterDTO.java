package com.hartron.esignharyana.service.dto;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.BlockMaster} entity.
 */
public class BlockMasterDTO implements Serializable {

    private Long id;

    private String blockCode;

    @NotNull
    private String blockName;

    private LocalDate createdOn;

    private String lastUpdatedBy;

    private LocalDate lastUpdatedOn;

    private String verifiedBy;

    private LocalDate verifiedOn;

    private String remarks;


    private Long stateMasterId;

    private Long districtMasterId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBlockCode() {
        return blockCode;
    }

    public void setBlockCode(String blockCode) {
        this.blockCode = blockCode;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public LocalDate getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public LocalDate getVerifiedOn() {
        return verifiedOn;
    }

    public void setVerifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Long getStateMasterId() {
        return stateMasterId;
    }

    public void setStateMasterId(Long stateMasterId) {
        this.stateMasterId = stateMasterId;
    }

    public Long getDistrictMasterId() {
        return districtMasterId;
    }

    public void setDistrictMasterId(Long districtMasterId) {
        this.districtMasterId = districtMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BlockMasterDTO blockMasterDTO = (BlockMasterDTO) o;
        if (blockMasterDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), blockMasterDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BlockMasterDTO{" +
            "id=" + getId() +
            ", blockCode='" + getBlockCode() + "'" +
            ", blockName='" + getBlockName() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastUpdatedBy='" + getLastUpdatedBy() + "'" +
            ", lastUpdatedOn='" + getLastUpdatedOn() + "'" +
            ", verifiedBy='" + getVerifiedBy() + "'" +
            ", verifiedOn='" + getVerifiedOn() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", stateMaster=" + getStateMasterId() +
            ", districtMaster=" + getDistrictMasterId() +
            "}";
    }
}
