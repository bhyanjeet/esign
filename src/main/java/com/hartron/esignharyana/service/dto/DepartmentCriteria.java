package com.hartron.esignharyana.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.hartron.esignharyana.domain.Department} entity. This class is used
 * in {@link com.hartron.esignharyana.web.rest.DepartmentResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /departments?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DepartmentCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter departmentName;

    private LocalDateFilter createdOn;

    private StringFilter createdBy;

    private LocalDateFilter updatedOn;

    private StringFilter updatedBy;

    private StringFilter status;

    private StringFilter remark;

    private StringFilter whiteListIp1;

    private StringFilter whiteListIp2;

    public DepartmentCriteria() {
    }

    public DepartmentCriteria(DepartmentCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.departmentName = other.departmentName == null ? null : other.departmentName.copy();
        this.createdOn = other.createdOn == null ? null : other.createdOn.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.updatedOn = other.updatedOn == null ? null : other.updatedOn.copy();
        this.updatedBy = other.updatedBy == null ? null : other.updatedBy.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.remark = other.remark == null ? null : other.remark.copy();
        this.whiteListIp1 = other.whiteListIp1 == null ? null : other.whiteListIp1.copy();
        this.whiteListIp2 = other.whiteListIp2 == null ? null : other.whiteListIp2.copy();
    }

    @Override
    public DepartmentCriteria copy() {
        return new DepartmentCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(StringFilter departmentName) {
        this.departmentName = departmentName;
    }

    public LocalDateFilter getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateFilter createdOn) {
        this.createdOn = createdOn;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateFilter getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDateFilter updatedOn) {
        this.updatedOn = updatedOn;
    }

    public StringFilter getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(StringFilter updatedBy) {
        this.updatedBy = updatedBy;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public StringFilter getRemark() {
        return remark;
    }

    public void setRemark(StringFilter remark) {
        this.remark = remark;
    }

    public StringFilter getWhiteListIp1() {
        return whiteListIp1;
    }

    public void setWhiteListIp1(StringFilter whiteListIp1) {
        this.whiteListIp1 = whiteListIp1;
    }

    public StringFilter getWhiteListIp2() {
        return whiteListIp2;
    }

    public void setWhiteListIp2(StringFilter whiteListIp2) {
        this.whiteListIp2 = whiteListIp2;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DepartmentCriteria that = (DepartmentCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(departmentName, that.departmentName) &&
            Objects.equals(createdOn, that.createdOn) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(updatedOn, that.updatedOn) &&
            Objects.equals(updatedBy, that.updatedBy) &&
            Objects.equals(status, that.status) &&
            Objects.equals(remark, that.remark) &&
            Objects.equals(whiteListIp1, that.whiteListIp1) &&
            Objects.equals(whiteListIp2, that.whiteListIp2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        departmentName,
        createdOn,
        createdBy,
        updatedOn,
        updatedBy,
        status,
        remark,
        whiteListIp1,
        whiteListIp2
        );
    }

    @Override
    public String toString() {
        return "DepartmentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (departmentName != null ? "departmentName=" + departmentName + ", " : "") +
                (createdOn != null ? "createdOn=" + createdOn + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (updatedOn != null ? "updatedOn=" + updatedOn + ", " : "") +
                (updatedBy != null ? "updatedBy=" + updatedBy + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (remark != null ? "remark=" + remark + ", " : "") +
                (whiteListIp1 != null ? "whiteListIp1=" + whiteListIp1 + ", " : "") +
                (whiteListIp2 != null ? "whiteListIp2=" + whiteListIp2 + ", " : "") +
            "}";
    }

}
