package com.hartron.esignharyana.service.dto;
import com.hartron.esignharyana.config.Constants;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.DesignationMaster} entity.
 */
public class DesignationMasterDTO implements Serializable {

    private Long id;

    @Pattern(regexp = Constants.ALPHABET_REGEX)
    @Size(max = 50)
    private String designationName;

    private String designationAbbreviation;

    private String createdBy;

    private LocalDate createdOn;

    private String lastUpdatedBy;

    private LocalDate lastUpdatedOn;

    private String verifiedBy;

    private LocalDate verifiedOn;

    private String remarks;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignationName() {
        return designationName;
    }

    public void setDesignationName(String designationName) {
        this.designationName = designationName;
    }

    public String getDesignationAbbreviation() {
        return designationAbbreviation;
    }

    public void setDesignationAbbreviation(String designationAbbreviation) {
        this.designationAbbreviation = designationAbbreviation;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public LocalDate getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(LocalDate lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public LocalDate getVerifiedOn() {
        return verifiedOn;
    }

    public void setVerifiedOn(LocalDate verifiedOn) {
        this.verifiedOn = verifiedOn;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DesignationMasterDTO designationMasterDTO = (DesignationMasterDTO) o;
        if (designationMasterDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), designationMasterDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DesignationMasterDTO{" +
            "id=" + getId() +
            ", designationName='" + getDesignationName() + "'" +
            ", designationAbbreviation='" + getDesignationAbbreviation() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", lastUpdatedBy='" + getLastUpdatedBy() + "'" +
            ", lastUpdatedOn='" + getLastUpdatedOn() + "'" +
            ", verifiedBy='" + getVerifiedBy() + "'" +
            ", verifiedOn='" + getVerifiedOn() + "'" +
            ", remarks='" + getRemarks() + "'" +
            "}";
    }
}
