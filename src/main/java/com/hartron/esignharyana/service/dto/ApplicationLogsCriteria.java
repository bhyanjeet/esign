package com.hartron.esignharyana.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.hartron.esignharyana.domain.ApplicationLogs} entity. This class is used
 * in {@link com.hartron.esignharyana.web.rest.ApplicationLogsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /application-logs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ApplicationLogsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter actionTaken;

    private StringFilter actionTakenBy;

    private LongFilter actionTakenOnApplication;

    private LocalDateFilter actionTakenOnDate;

    private StringFilter remarks;

    public ApplicationLogsCriteria(){
    }

    public ApplicationLogsCriteria(ApplicationLogsCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.actionTaken = other.actionTaken == null ? null : other.actionTaken.copy();
        this.actionTakenBy = other.actionTakenBy == null ? null : other.actionTakenBy.copy();
        this.actionTakenOnApplication = other.actionTakenOnApplication == null ? null : other.actionTakenOnApplication.copy();
        this.actionTakenOnDate = other.actionTakenOnDate == null ? null : other.actionTakenOnDate.copy();
        this.remarks = other.remarks == null ? null : other.remarks.copy();
    }

    @Override
    public ApplicationLogsCriteria copy() {
        return new ApplicationLogsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(StringFilter actionTaken) {
        this.actionTaken = actionTaken;
    }

    public StringFilter getActionTakenBy() {
        return actionTakenBy;
    }

    public void setActionTakenBy(StringFilter actionTakenBy) {
        this.actionTakenBy = actionTakenBy;
    }

    public LongFilter getActionTakenOnApplication() {
        return actionTakenOnApplication;
    }

    public void setActionTakenOnApplication(LongFilter actionTakenOnApplication) {
        this.actionTakenOnApplication = actionTakenOnApplication;
    }

    public LocalDateFilter getActionTakenOnDate() {
        return actionTakenOnDate;
    }

    public void setActionTakenOnDate(LocalDateFilter actionTakenOnDate) {
        this.actionTakenOnDate = actionTakenOnDate;
    }

    public StringFilter getRemarks() {
        return remarks;
    }

    public void setRemarks(StringFilter remarks) {
        this.remarks = remarks;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ApplicationLogsCriteria that = (ApplicationLogsCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(actionTaken, that.actionTaken) &&
            Objects.equals(actionTakenBy, that.actionTakenBy) &&
            Objects.equals(actionTakenOnApplication, that.actionTakenOnApplication) &&
            Objects.equals(actionTakenOnDate, that.actionTakenOnDate) &&
            Objects.equals(remarks, that.remarks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        actionTaken,
        actionTakenBy,
        actionTakenOnApplication,
        actionTakenOnDate,
        remarks
        );
    }

    @Override
    public String toString() {
        return "ApplicationLogsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (actionTaken != null ? "actionTaken=" + actionTaken + ", " : "") +
                (actionTakenBy != null ? "actionTakenBy=" + actionTakenBy + ", " : "") +
                (actionTakenOnApplication != null ? "actionTakenOnApplication=" + actionTakenOnApplication + ", " : "") +
                (actionTakenOnDate != null ? "actionTakenOnDate=" + actionTakenOnDate + ", " : "") +
                (remarks != null ? "remarks=" + remarks + ", " : "") +
            "}";
    }

}
