package com.hartron.esignharyana.service.dto;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.UserLogs} entity.
 */
public class UserLogsDTO implements Serializable {

    private Long id;

    private String actionTaken;

    private String actionTakenBy;

    private Long actionTakenOnUser;

    private LocalDate actionTakenOnDate;

    private String remarks;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    public String getActionTakenBy() {
        return actionTakenBy;
    }

    public void setActionTakenBy(String actionTakenBy) {
        this.actionTakenBy = actionTakenBy;
    }

    public Long getActionTakenOnUser() {
        return actionTakenOnUser;
    }

    public void setActionTakenOnUser(Long actionTakenOnUser) {
        this.actionTakenOnUser = actionTakenOnUser;
    }

    public LocalDate getActionTakenOnDate() {
        return actionTakenOnDate;
    }

    public void setActionTakenOnDate(LocalDate actionTakenOnDate) {
        this.actionTakenOnDate = actionTakenOnDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserLogsDTO userLogsDTO = (UserLogsDTO) o;
        if (userLogsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userLogsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserLogsDTO{" +
            "id=" + getId() +
            ", actionTaken='" + getActionTaken() + "'" +
            ", actionTakenBy='" + getActionTakenBy() + "'" +
            ", actionTakenOnUser=" + getActionTakenOnUser() +
            ", actionTakenOnDate='" + getActionTakenOnDate() + "'" +
            ", remarks='" + getRemarks() + "'" +
            "}";
    }
}
