package com.hartron.esignharyana.service.dto;
import com.hartron.esignharyana.config.Constants;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hartron.esignharyana.domain.OrganisationLog} entity.
 */
public class OrganisationLogDTO implements Serializable {

    private Long id;

    private Long organisationId;

    private String createdByLogin;

    private ZonedDateTime createdDate;

    private String verifyByName;

    private ZonedDateTime verifyDate;

    private String remarks;

    private String status;

    private String verifyByLogin;

    private String createdByName;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrganisationId() {
        return organisationId;
    }

    public void setOrganisationId(Long organisationId) {
        this.organisationId = organisationId;
    }

    public String getCreatedByLogin() {
        return createdByLogin;
    }

    public void setCreatedByLogin(String createdByLogin) {
        this.createdByLogin = createdByLogin;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getVerifyByName() {
        return verifyByName;
    }

    public void setVerifyByName(String verifyByName) {
        this.verifyByName = verifyByName;
    }

    public ZonedDateTime getVerifyDate() {
        return verifyDate;
    }

    public void setVerifyDate(ZonedDateTime verifyDate) {
        this.verifyDate = verifyDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVerifyByLogin() {
        return verifyByLogin;
    }

    public void setVerifyByLogin(String verifyByLogin) {
        this.verifyByLogin = verifyByLogin;
    }

    public String getCreatedByName() {
        return createdByName;
    }

    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrganisationLogDTO organisationLogDTO = (OrganisationLogDTO) o;
        if (organisationLogDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), organisationLogDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OrganisationLogDTO{" +
            "id=" + getId() +
            ", organisationId=" + getOrganisationId() +
            ", createdByLogin='" + getCreatedByLogin() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", verifyByName='" + getVerifyByName() + "'" +
            ", verifyDate='" + getVerifyDate() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", status='" + getStatus() + "'" +
            ", verifyByLogin='" + getVerifyByLogin() + "'" +
            ", createdByName='" + getCreatedByName() + "'" +
            "}";
    }
}
