package com.hartron.esignharyana.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.hartron.esignharyana.domain.EsignTransation} entity. This class is used
 * in {@link com.hartron.esignharyana.web.rest.EsignTransationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /esign-transations?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EsignTransationCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter txnId;

    private StringFilter userCodeId;

    private StringFilter applicationCodeId;

    private StringFilter orgResponseSuccessURL;

    private StringFilter orgResponseFailURL;

    private StringFilter orgStatus;

    private LocalDateFilter requestDate;

    private StringFilter organisationIdCode;

    private StringFilter signerId;

    private StringFilter requestType;

    private StringFilter requestStatus;

    private StringFilter responseType;

    private StringFilter responseStatus;

    private StringFilter responseCode;

    private StringFilter requestXml;

    private StringFilter responseXml;

    private LocalDateFilter responseDate;

    private StringFilter ts;

    public EsignTransationCriteria(){
    }

    public EsignTransationCriteria(EsignTransationCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.txnId = other.txnId == null ? null : other.txnId.copy();
        this.userCodeId = other.userCodeId == null ? null : other.userCodeId.copy();
        this.applicationCodeId = other.applicationCodeId == null ? null : other.applicationCodeId.copy();
        this.orgResponseSuccessURL = other.orgResponseSuccessURL == null ? null : other.orgResponseSuccessURL.copy();
        this.orgResponseFailURL = other.orgResponseFailURL == null ? null : other.orgResponseFailURL.copy();
        this.orgStatus = other.orgStatus == null ? null : other.orgStatus.copy();
        this.requestDate = other.requestDate == null ? null : other.requestDate.copy();
        this.organisationIdCode = other.organisationIdCode == null ? null : other.organisationIdCode.copy();
        this.signerId = other.signerId == null ? null : other.signerId.copy();
        this.requestType = other.requestType == null ? null : other.requestType.copy();
        this.requestStatus = other.requestStatus == null ? null : other.requestStatus.copy();
        this.responseType = other.responseType == null ? null : other.responseType.copy();
        this.responseStatus = other.responseStatus == null ? null : other.responseStatus.copy();
        this.responseCode = other.responseCode == null ? null : other.responseCode.copy();
        this.requestXml = other.requestXml == null ? null : other.requestXml.copy();
        this.responseXml = other.responseXml == null ? null : other.responseXml.copy();
        this.responseDate = other.responseDate == null ? null : other.responseDate.copy();
        this.ts = other.ts == null ? null : other.ts.copy();
    }

    @Override
    public EsignTransationCriteria copy() {
        return new EsignTransationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTxnId() {
        return txnId;
    }

    public void setTxnId(StringFilter txnId) {
        this.txnId = txnId;
    }

    public StringFilter getUserCodeId() {
        return userCodeId;
    }

    public void setUserCodeId(StringFilter userCodeId) {
        this.userCodeId = userCodeId;
    }

    public StringFilter getApplicationCodeId() {
        return applicationCodeId;
    }

    public void setApplicationCodeId(StringFilter applicationCodeId) {
        this.applicationCodeId = applicationCodeId;
    }

    public StringFilter getOrgResponseSuccessURL() {
        return orgResponseSuccessURL;
    }

    public void setOrgResponseSuccessURL(StringFilter orgResponseSuccessURL) {
        this.orgResponseSuccessURL = orgResponseSuccessURL;
    }

    public StringFilter getOrgResponseFailURL() {
        return orgResponseFailURL;
    }

    public void setOrgResponseFailURL(StringFilter orgResponseFailURL) {
        this.orgResponseFailURL = orgResponseFailURL;
    }

    public StringFilter getOrgStatus() {
        return orgStatus;
    }

    public void setOrgStatus(StringFilter orgStatus) {
        this.orgStatus = orgStatus;
    }

    public LocalDateFilter getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(LocalDateFilter requestDate) {
        this.requestDate = requestDate;
    }

    public StringFilter getOrganisationIdCode() {
        return organisationIdCode;
    }

    public void setOrganisationIdCode(StringFilter organisationIdCode) {
        this.organisationIdCode = organisationIdCode;
    }

    public StringFilter getSignerId() {
        return signerId;
    }

    public void setSignerId(StringFilter signerId) {
        this.signerId = signerId;
    }

    public StringFilter getRequestType() {
        return requestType;
    }

    public void setRequestType(StringFilter requestType) {
        this.requestType = requestType;
    }

    public StringFilter getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(StringFilter requestStatus) {
        this.requestStatus = requestStatus;
    }

    public StringFilter getResponseType() {
        return responseType;
    }

    public void setResponseType(StringFilter responseType) {
        this.responseType = responseType;
    }

    public StringFilter getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(StringFilter responseStatus) {
        this.responseStatus = responseStatus;
    }

    public StringFilter getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(StringFilter responseCode) {
        this.responseCode = responseCode;
    }

    public StringFilter getRequestXml() {
        return requestXml;
    }

    public void setRequestXml(StringFilter requestXml) {
        this.requestXml = requestXml;
    }

    public StringFilter getResponseXml() {
        return responseXml;
    }

    public void setResponseXml(StringFilter responseXml) {
        this.responseXml = responseXml;
    }

    public LocalDateFilter getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(LocalDateFilter responseDate) {
        this.responseDate = responseDate;
    }

    public StringFilter getTs() {
        return ts;
    }

    public void setTs(StringFilter ts) {
        this.ts = ts;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EsignTransationCriteria that = (EsignTransationCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(txnId, that.txnId) &&
            Objects.equals(userCodeId, that.userCodeId) &&
            Objects.equals(applicationCodeId, that.applicationCodeId) &&
            Objects.equals(orgResponseSuccessURL, that.orgResponseSuccessURL) &&
            Objects.equals(orgResponseFailURL, that.orgResponseFailURL) &&
            Objects.equals(orgStatus, that.orgStatus) &&
            Objects.equals(requestDate, that.requestDate) &&
            Objects.equals(organisationIdCode, that.organisationIdCode) &&
            Objects.equals(signerId, that.signerId) &&
            Objects.equals(requestType, that.requestType) &&
            Objects.equals(requestStatus, that.requestStatus) &&
            Objects.equals(responseType, that.responseType) &&
            Objects.equals(responseStatus, that.responseStatus) &&
            Objects.equals(responseCode, that.responseCode) &&
            Objects.equals(requestXml, that.requestXml) &&
            Objects.equals(responseXml, that.responseXml) &&
            Objects.equals(responseDate, that.responseDate) &&
            Objects.equals(ts, that.ts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        txnId,
        userCodeId,
        applicationCodeId,
        orgResponseSuccessURL,
        orgResponseFailURL,
        orgStatus,
        requestDate,
        organisationIdCode,
        signerId,
        requestType,
        requestStatus,
        responseType,
        responseStatus,
        responseCode,
        requestXml,
        responseXml,
        responseDate,
        ts
        );
    }

    @Override
    public String toString() {
        return "EsignTransationCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (txnId != null ? "txnId=" + txnId + ", " : "") +
                (userCodeId != null ? "userCodeId=" + userCodeId + ", " : "") +
                (applicationCodeId != null ? "applicationCodeId=" + applicationCodeId + ", " : "") +
                (orgResponseSuccessURL != null ? "orgResponseSuccessURL=" + orgResponseSuccessURL + ", " : "") +
                (orgResponseFailURL != null ? "orgResponseFailURL=" + orgResponseFailURL + ", " : "") +
                (orgStatus != null ? "orgStatus=" + orgStatus + ", " : "") +
                (requestDate != null ? "requestDate=" + requestDate + ", " : "") +
                (organisationIdCode != null ? "organisationIdCode=" + organisationIdCode + ", " : "") +
                (signerId != null ? "signerId=" + signerId + ", " : "") +
                (requestType != null ? "requestType=" + requestType + ", " : "") +
                (requestStatus != null ? "requestStatus=" + requestStatus + ", " : "") +
                (responseType != null ? "responseType=" + responseType + ", " : "") +
                (responseStatus != null ? "responseStatus=" + responseStatus + ", " : "") +
                (responseCode != null ? "responseCode=" + responseCode + ", " : "") +
                (requestXml != null ? "requestXml=" + requestXml + ", " : "") +
                (responseXml != null ? "responseXml=" + responseXml + ", " : "") +
                (responseDate != null ? "responseDate=" + responseDate + ", " : "") +
                (ts != null ? "ts=" + ts + ", " : "") +
            "}";
    }

}
