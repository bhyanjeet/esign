package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.ApplicationLogsDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.ApplicationLogs}.
 */
public interface SendEsignRequestService {

    void sendOrgKYCRequest(String xmlSignOutput) throws IOException;
}
