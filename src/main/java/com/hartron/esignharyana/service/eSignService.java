package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.EsignRequestDTO;
import com.itextpdf.text.DocumentException;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OperatorCreationException;
import org.xml.sax.SAXException;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.UserDetails}.
 */
public interface eSignService {



    EsignRequestDTO createeSignXML(EsignRequestDTO esignRequestDTO) throws ParserConfigurationException, TransformerException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, XMLSignatureException, MarshalException, IOException, KeyStoreException, SAXException, UnrecoverableEntryException, CertificateException, DocumentException, SignatureException, InvalidKeyException, CMSException, OperatorCreationException, NoSuchProviderException;
   // String createeSignXML() throws ParserConfigurationException, TransformerException, IOException;
    String signXML(String xmlDoc) throws ParserConfigurationException, TransformerException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IOException, UnrecoverableEntryException, KeyStoreException, MarshalException, XMLSignatureException, SAXException, CertificateException, org.xml.sax.SAXException;
   // String createSignerXML(String signedeSignXML) throws ParserConfigurationException, TransformerException, MarshalException, XMLSignatureException, IOException, SAXException, KeyStoreException, CertificateException, UnrecoverableEntryException, org.xml.sax.SAXException;
  String signStatus(String userCodeId) throws ParserConfigurationException, TransformerException, IOException, CertificateException, NoSuchAlgorithmException, XMLSignatureException, UnrecoverableEntryException, InvalidAlgorithmParameterException, SAXException, MarshalException, KeyStoreException;


    String base64Base(String resData);
}
