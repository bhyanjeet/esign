package com.hartron.esignharyana.service;

import org.springframework.web.multipart.MultipartFile;


/**
 * Service Interface for managing Clients.
 */
public interface MinioFileService
{

void minioSave(MultipartFile file, String filename);

    String multipleFileMinioSave(MultipartFile multipartFile, String s);
}
