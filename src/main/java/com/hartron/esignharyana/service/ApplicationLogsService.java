package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.ApplicationLogsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.ApplicationLogs}.
 */
public interface ApplicationLogsService {

    /**
     * Save a applicationLogs.
     *
     * @param applicationLogsDTO the entity to save.
     * @return the persisted entity.
     */
    ApplicationLogsDTO save(ApplicationLogsDTO applicationLogsDTO);

    /**
     * Get all the applicationLogs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ApplicationLogsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" applicationLogs.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ApplicationLogsDTO> findOne(Long id);

    /**
     * Delete the "id" applicationLogs.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the applicationLogs corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ApplicationLogsDTO> search(String query, Pageable pageable);
}
