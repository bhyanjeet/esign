package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.OrganisationLogDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.OrganisationLog}.
 */
public interface OrganisationLogService {

    /**
     * Save a organisationLog.
     *
     * @param organisationLogDTO the entity to save.
     * @return the persisted entity.
     */
    OrganisationLogDTO save(OrganisationLogDTO organisationLogDTO);

    /**
     * Get all the organisationLogs.
     *
     * @return the list of entities.
     */
    List<OrganisationLogDTO> findAll();


    /**
     * Get the "id" organisationLog.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrganisationLogDTO> findOne(Long id);

    /**
     * Delete the "id" organisationLog.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the organisationLog corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @return the list of entities.
     */
    List<OrganisationLogDTO> search(String query);
}
