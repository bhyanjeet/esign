package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.service.OrganisationLogService;
import com.hartron.esignharyana.domain.OrganisationLog;
import com.hartron.esignharyana.repository.OrganisationLogRepository;
import com.hartron.esignharyana.repository.search.OrganisationLogSearchRepository;
import com.hartron.esignharyana.service.dto.OrganisationLogDTO;
import com.hartron.esignharyana.service.mapper.OrganisationLogMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link OrganisationLog}.
 */
@Service
@Transactional
public class OrganisationLogServiceImpl implements OrganisationLogService {

    private final Logger log = LoggerFactory.getLogger(OrganisationLogServiceImpl.class);

    private final OrganisationLogRepository organisationLogRepository;

    private final OrganisationLogMapper organisationLogMapper;

    private final OrganisationLogSearchRepository organisationLogSearchRepository;

    public OrganisationLogServiceImpl(OrganisationLogRepository organisationLogRepository, OrganisationLogMapper organisationLogMapper, OrganisationLogSearchRepository organisationLogSearchRepository) {
        this.organisationLogRepository = organisationLogRepository;
        this.organisationLogMapper = organisationLogMapper;
        this.organisationLogSearchRepository = organisationLogSearchRepository;
    }

    /**
     * Save a organisationLog.
     *
     * @param organisationLogDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OrganisationLogDTO save(OrganisationLogDTO organisationLogDTO) {
        log.debug("Request to save OrganisationLog : {}", organisationLogDTO);
        OrganisationLog organisationLog = organisationLogMapper.toEntity(organisationLogDTO);
        organisationLog = organisationLogRepository.save(organisationLog);
        OrganisationLogDTO result = organisationLogMapper.toDto(organisationLog);
        organisationLogSearchRepository.save(organisationLog);
        return result;
    }

    /**
     * Get all the organisationLogs.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<OrganisationLogDTO> findAll() {
        log.debug("Request to get all OrganisationLogs");
        return organisationLogRepository.findAll().stream()
            .map(organisationLogMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one organisationLog by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OrganisationLogDTO> findOne(Long id) {
        log.debug("Request to get OrganisationLog : {}", id);
        return organisationLogRepository.findById(id)
            .map(organisationLogMapper::toDto);
    }

    /**
     * Delete the organisationLog by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrganisationLog : {}", id);
        organisationLogRepository.deleteById(id);
        organisationLogSearchRepository.deleteById(id);
    }

    /**
     * Search for the organisationLog corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<OrganisationLogDTO> search(String query) {
        log.debug("Request to search OrganisationLogs for query {}", query);
        return StreamSupport
            .stream(organisationLogSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(organisationLogMapper::toDto)
            .collect(Collectors.toList());
    }
}
