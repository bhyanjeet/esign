package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.service.OrganisationAttachmentService;
import com.hartron.esignharyana.domain.OrganisationAttachment;
import com.hartron.esignharyana.repository.OrganisationAttachmentRepository;
import com.hartron.esignharyana.repository.search.OrganisationAttachmentSearchRepository;
import com.hartron.esignharyana.service.dto.OrganisationAttachmentDTO;
import com.hartron.esignharyana.service.mapper.OrganisationAttachmentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link OrganisationAttachment}.
 */
@Service
@Transactional
public class OrganisationAttachmentServiceImpl implements OrganisationAttachmentService {

    private final Logger log = LoggerFactory.getLogger(OrganisationAttachmentServiceImpl.class);

    private final OrganisationAttachmentRepository organisationAttachmentRepository;

    private final OrganisationAttachmentMapper organisationAttachmentMapper;

    private final OrganisationAttachmentSearchRepository organisationAttachmentSearchRepository;

    public OrganisationAttachmentServiceImpl(OrganisationAttachmentRepository organisationAttachmentRepository, OrganisationAttachmentMapper organisationAttachmentMapper, OrganisationAttachmentSearchRepository organisationAttachmentSearchRepository) {
        this.organisationAttachmentRepository = organisationAttachmentRepository;
        this.organisationAttachmentMapper = organisationAttachmentMapper;
        this.organisationAttachmentSearchRepository = organisationAttachmentSearchRepository;
    }

    /**
     * Save a organisationAttachment.
     *
     * @param organisationAttachmentDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OrganisationAttachmentDTO save(OrganisationAttachmentDTO organisationAttachmentDTO) {
        log.debug("Request to save OrganisationAttachment : {}", organisationAttachmentDTO);
        OrganisationAttachment organisationAttachment = organisationAttachmentMapper.toEntity(organisationAttachmentDTO);
        organisationAttachment = organisationAttachmentRepository.save(organisationAttachment);
        OrganisationAttachmentDTO result = organisationAttachmentMapper.toDto(organisationAttachment);
        organisationAttachmentSearchRepository.save(organisationAttachment);
        return result;
    }

    /**
     * Get all the organisationAttachments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrganisationAttachmentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrganisationAttachments");
        return organisationAttachmentRepository.findAll(pageable)
            .map(organisationAttachmentMapper::toDto);
    }

    @Override
    public List<OrganisationAttachmentDTO> findAllByOrganisation(Long id) {
        return organisationAttachmentSearchRepository.findAllByOrganisationMasterId(id).stream().map(organisationAttachmentMapper::toDto).collect(Collectors.toList());
    }


    /**
     * Get one organisationAttachment by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OrganisationAttachmentDTO> findOne(Long id) {
        log.debug("Request to get OrganisationAttachment : {}", id);
        return organisationAttachmentRepository.findById(id)
            .map(organisationAttachmentMapper::toDto);
    }

    /**
     * Delete the organisationAttachment by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrganisationAttachment : {}", id);
        organisationAttachmentRepository.deleteById(id);
        organisationAttachmentSearchRepository.deleteById(id);
    }

    /**
     * Search for the organisationAttachment corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrganisationAttachmentDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of OrganisationAttachments for query {}", query);
        return organisationAttachmentSearchRepository.search(queryStringQuery(query), pageable)
            .map(organisationAttachmentMapper::toDto);
    }
}
