package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.domain.OrganisationMaster;
import com.hartron.esignharyana.domain.User;
import com.hartron.esignharyana.repository.*;
import com.hartron.esignharyana.repository.search.*;
import com.hartron.esignharyana.service.*;
import com.hartron.esignharyana.domain.UserApplication;
import com.hartron.esignharyana.service.dto.ApplicationMasterDTO;
import com.hartron.esignharyana.service.dto.UserApplicationDTO;
import com.hartron.esignharyana.service.dto.UserDetailsDTO;
import com.hartron.esignharyana.service.mapper.UserApplicationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link UserApplication}.
 */
@Service
@Transactional
public class UserApplicationServiceImpl implements UserApplicationService {

    private final Logger log = LoggerFactory.getLogger(UserApplicationServiceImpl.class);

    private final UserApplicationRepository userApplicationRepository;

    private final UserApplicationMapper userApplicationMapper;

    private final UserApplicationSearchRepository userApplicationSearchRepository;




    public UserApplicationServiceImpl(UserApplicationRepository userApplicationRepository, UserApplicationMapper userApplicationMapper, UserApplicationSearchRepository userApplicationSearchRepository) {
        this.userApplicationRepository = userApplicationRepository;
        this.userApplicationMapper = userApplicationMapper;
        this.userApplicationSearchRepository = userApplicationSearchRepository;

    }

    /**
     * Save a userApplication.
     *
     * @param userApplicationDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UserApplicationDTO save(UserApplicationDTO userApplicationDTO) {
                log.debug("Request to save UserApplication : {}", userApplicationDTO);
        UserApplication userApplication = userApplicationMapper.toEntity(userApplicationDTO);
        userApplication = userApplicationRepository.save(userApplication);
        UserApplicationDTO result = userApplicationMapper.toDto(userApplication);
        userApplicationSearchRepository.save(userApplication);
        return result;
    }

    /**
     * Get all the userApplications.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserApplicationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserApplications");
        return userApplicationRepository.findAll(pageable)
            .map(userApplicationMapper::toDto);
    }


    /**
     * Get one userApplication by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserApplicationDTO> findOne(Long id) {
        log.debug("Request to get UserApplication : {}", id);
        return userApplicationRepository.findById(id)
            .map(userApplicationMapper::toDto);
    }

    /**
     * Delete the userApplication by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserApplication : {}", id);
        userApplicationRepository.deleteById(id);
        userApplicationSearchRepository.deleteById(id);
    }

    /**
     * Search for the userApplication corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserApplicationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of UserApplications for query {}", query);
        return userApplicationSearchRepository.search(queryStringQuery(query), pageable)
            .map(userApplicationMapper::toDto);
    }

    public List<UserApplicationDTO> findUserApplicationByUserLogin(String userLogin) {
        return userApplicationSearchRepository.findApplicationByUserLogin(userLogin).stream().map(userApplicationMapper::toDto).collect(Collectors.toList());
    }


    public List<UserApplicationDTO> findApplicationByUserId(Long id) {
        return userApplicationSearchRepository.findApplicationByUserId(id).stream().map(userApplicationMapper::toDto).collect(Collectors.toList());
    }

    public Optional<UserApplicationDTO> findApplicationCodeUserCode(String applicationCodeId, String userCodeId){
        return userApplicationSearchRepository.findUserApplicationByApplicationCodeIdAndUserCodeId(applicationCodeId,userCodeId).map(userApplicationMapper::toDto);
    }
}
