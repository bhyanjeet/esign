package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.domain.EsignTransation;
import com.hartron.esignharyana.domain.UserDetails;
import com.hartron.esignharyana.repository.EsignTransationRepository;
import com.hartron.esignharyana.repository.search.ApplicationMasterSearchRepository;
import com.hartron.esignharyana.repository.search.EsignTransationSearchRepository;
import com.hartron.esignharyana.repository.search.UserDetailsSearchRepository;
import com.hartron.esignharyana.service.EsignDemoService;
import com.hartron.esignharyana.service.dto.EsignRequestDTO;
import com.hartron.esignharyana.service.dto.ExternalUserEsignRequestDTO;
import com.hartron.esignharyana.service.mapper.UserDetailsMapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.bouncycastle.cms.CMSSignedData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import sun.security.pkcs.PKCS7;
import sun.security.pkcs.SignerInfo;
import sun.security.util.DerOutputStream;
import sun.security.util.DerValue;
import sun.security.x509.AlgorithmId;
import sun.security.x509.X500Name;

import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.keyinfo.X509IssuerSerial;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Service Implementation for managing {@link UserDetails}.
 */
@Service
@Transactional
public class ESignDemoServiceImpl implements EsignDemoService {

    private final Logger log = LoggerFactory.getLogger(ESignDemoServiceImpl.class);

    private String txnId = "";

    private final UserDetailsMapper userDetailsMapper;

    private final UserDetailsSearchRepository userDetailsSearchRepository;

    private final EsignTransationRepository esignTransationRepository;

    private final EsignTransationSearchRepository esignTransationSearchRepository;

    private final ApplicationMasterSearchRepository applicationMasterSearchRepository;


    private final static String ASP_REDIRECT_URL = "https://esign.hartron.org.in";
    private final static String ASP_RESPONSE_URL = "https://esign.hartron.org.in/api/sign/response";
    private final static String ASP_IP = "::1";
    private final static String ASP_ID = "CSORG1000002";



    private String userRequestedXmlTs = "";

    private String userRequestedXmlTxn = "";
    private String userRequestedXmlDocInfo = "";
    private String userRequestedXmlDocUrl = "";
    private String userRequestedXmlDocHashHexa = "";
    private String userRequestedXmlSignerId = "";

    //variables for Signerid validation check for signer status xml

    private String SIGNERID_STATUS_ERROR = "";
    private String SIGNERID_STATUS_INFO = "";







    public ESignDemoServiceImpl(UserDetailsMapper userDetailsMapper, UserDetailsSearchRepository userDetailsSearchRepository, EsignTransationRepository esignTransationRepository, EsignTransationSearchRepository esignTransationSearchRepository, ApplicationMasterSearchRepository applicationMasterSearchRepository) {

        this.userDetailsMapper = userDetailsMapper;
        this.userDetailsSearchRepository = userDetailsSearchRepository;

        this.esignTransationRepository = esignTransationRepository;
        this.esignTransationSearchRepository = esignTransationSearchRepository;
        this.applicationMasterSearchRepository = applicationMasterSearchRepository;
    }



    public ExternalUserEsignRequestDTO saveEsignRequest(ExternalUserEsignRequestDTO externalUserEsignRequestDTO) throws NoSuchAlgorithmException, ParserConfigurationException, IOException, SAXException, TransformerException, XMLSignatureException, MarshalException, KeyStoreException, UnrecoverableEntryException, CertificateException, InvalidAlgorithmParameterException {




        String eSignXML =  createESignXML(externalUserEsignRequestDTO);
        String signedeSignXML = signXML(eSignXML);
        // encode string using Base64 encoder
        Base64.Encoder encoderXml = Base64.getEncoder();
        String encodedXml = encoderXml.encodeToString(signedeSignXML.getBytes());

        System.out.println("Encoded refr Data: " + encodedXml);
        Base64.Encoder encoderTxnRef = Base64.getEncoder();
        String txnRef = encoderTxnRef.encodeToString((txnId +"|" + externalUserEsignRequestDTO.getRedirectUrl()).getBytes());


        externalUserEsignRequestDTO.setAspIp("::1");

        externalUserEsignRequestDTO.setDocInfo(externalUserEsignRequestDTO.getDocInfo());

        externalUserEsignRequestDTO.setAspId("CSORG1000002");

        externalUserEsignRequestDTO.setRefData(encodedXml);
        externalUserEsignRequestDTO.getUserCodeId();
        externalUserEsignRequestDTO.setTxnRef(txnRef);

         String url = "http://localhost:9001/esign-final-form-request/" + ASP_ID + "/" +  ASP_IP + "/" + txnRef + "/" + encodedXml;

        System.out.println("uuuuuuuuuuuuuurllllllll" + url);


//checking user and application is present or not in record
//        boolean checkUser = checkUser(externalUserEsignRequestDTO.getUserCodeId());
//        if (!checkUser){
//            throw new BadRequestAlertException("User Not Present!", "userDetails", "invalidUser");
//        }
//
//        boolean checkApplication = checkApplication(externalUserEsignRequestDTO.getApplicationIdCode());
//
//        if (!checkApplication){
//            throw new BadRequestAlertException("Application Not Present!", "applicationMaster", "invalidAppliation");
//        }
//

//check Signerid status

        String signerStatusXml = createSignerStatusXml(externalUserEsignRequestDTO.getSignerId());

        System.out.println("ssssssssssignerStatusXmllllllllllll");
        System.out.println(signerStatusXml);
        System.out.println("ssssssssssignerStatusXmllllllllllll");


        System.out.println("ssssssssssignedStatusXmlddddddddddddddd");
        String signedStatusXml = signXML(signerStatusXml);
        System.out.println("ssssssssssignedStatusXmlddddddddddddddd");

        String checkSignerRespone = checkSignerStatus(signedStatusXml);

        checkSignerResponeStatus(checkSignerRespone);

        System.out.println("ssssssssssignerstatusssssssssssssss");
        System.out.println(checkSignerRespone);
        System.out.println("ssssssssssignerstatusssssssssssssss");



        if (!SIGNERID_STATUS_ERROR.equals(000)){

//            Base64.Encoder encoderXml = Base64.getEncoder();
//            String encodedXml = encoderXml.encodeToString(signedeSignXML.getBytes());
//
//            System.out.println("Encoded refr Data: " + encodedXml);
//            Base64.Encoder encoderTxnRef = Base64.getEncoder();
//            String txnRef = encoderTxnRef.encodeToString((txnId +"|" + externalUserEsignRequestDTO.getRedirectUrl()).getBytes());
//
//
//            externalUserEsignRequestDTO.setAspIp("::1");
//
//            externalUserEsignRequestDTO.setDocInfo(externalUserEsignRequestDTO.getDocInfo());
//
//            externalUserEsignRequestDTO.setAspId("CSORG1000002");
//
//            externalUserEsignRequestDTO.setRefData(encodedXml);
//            externalUserEsignRequestDTO.getUserCodeId();
//            externalUserEsignRequestDTO.setTxnRef(txnRef);

        }

//


        return  externalUserEsignRequestDTO;
    }

    public  boolean checkUser(String userCodeId){
        boolean isRecordPersent = false;
        userDetailsSearchRepository.findByUserIdCode(userCodeId);
        return isRecordPersent;
    }

    public  boolean checkApplication(String applicationIdCode){
        boolean isRecordPersent = false;
        isRecordPersent =   applicationMasterSearchRepository.findByApplicationIdCode(applicationIdCode);
        return isRecordPersent;
    }


    public String createSignerStatusXml(String signerId) throws  ParserConfigurationException, TransformerException, MarshalException, XMLSignatureException, IOException, SAXException, KeyStoreException, CertificateException, UnrecoverableEntryException {



//        log.debug("REST request to save ApplicationMaster : {}");

        Date now = new Date();
        String format3 = new SimpleDateFormat("yyMMddHHmmss", Locale.ENGLISH).format(now);
        String randomNo = RandomStringUtils.randomNumeric(6);
        String txnId = format3 + randomNo;

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;

        dBuilder = dbFactory.newDocumentBuilder();

        Document doc = dBuilder.newDocument();
        doc.setXmlStandalone(true);
        StringWriter writer = new StringWriter();

        Element rootElement = doc.createElement("Signer");

        doc.appendChild(rootElement);

        Attr api = doc.createAttribute("api");
        api.setValue("status");
        rootElement.setAttributeNode(api);

        Attr orgType = doc.createAttribute("orgType");
        orgType.setValue("ORG");
        rootElement.setAttributeNode(orgType);

        Attr orgId = doc.createAttribute("orgId");
        orgId.setValue("CSORG1000002");
        rootElement.setAttributeNode(orgId);

        Attr eKycType = doc.createAttribute("eKycType");
        eKycType.setValue("ORG1");
        rootElement.setAttributeNode(eKycType);

        Attr ver = doc.createAttribute("ver");
        ver.setValue("3.2");
        rootElement.setAttributeNode(ver);

        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String formattedDate = myDateObj.format(myFormatObj);

        Attr ts = doc.createAttribute("ts");

        ts.setValue(formattedDate);
        rootElement.setAttributeNode(ts);

        Attr txn = doc.createAttribute("txn");
        txn.setValue(txnId);
        rootElement.setAttributeNode(txn);


        Attr aspId = doc.createAttribute("aspId");
        aspId.setValue("CSORG1000002");
        rootElement.setAttributeNode(aspId);

        Attr signerIdAttr = doc.createAttribute("signerId");
        signerIdAttr.setValue(signerId);
        rootElement.setAttributeNode(signerIdAttr);

        Attr kycData = doc.createAttribute("kycData");
        kycData.setValue("N");
        rootElement.setAttributeNode(kycData);
//        rootElement.appendChild(doc.createTextNode(encodedString));


        // create the xml file
        //transform the DOM Object to an XML File
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(doc);
        //StreamResult streamResult = new StreamResult(new File(xmlFilePath));

        // If you use
        StreamResult result = new StreamResult(writer);
        // the output will be pushed to the standard output ...
        // You can use that for debugging

        transformer.transform(domSource, result);


        return writer.toString();


    }

    public String checkSignerStatus(String signerStatusXml) throws IOException {

        String request = signerStatusXml;

        URL url = new URL("http://117.255.216.162:8097/api/signer");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        // Set timeout as per needs
        connection.setConnectTimeout(20000);
        connection.setReadTimeout(20000);

        // Set DoOutput to true if you want to use URLConnection for output.
        // Default is false
        connection.setDoOutput(true);

        connection.setUseCaches(true);
        connection.setRequestMethod("POST");

        // Set Headers
//        connection.setRequestProperty("Accept", "application/xml");
        connection.setRequestProperty("Content-Type", "application/xml");
        connection.setRequestProperty("Cache-Control", "no-cache");
        connection.setRequestProperty("AspId","CSORG1000002");
        connection.setRequestProperty("Body", "xml");

        // Write XML
        OutputStream outputStream = connection.getOutputStream();
        byte[] b = request.getBytes("UTF-8");
        outputStream.write(b);
        outputStream.flush();
        outputStream.close();

        // Read XML
        InputStream inputStream = connection.getInputStream();
        byte[] res = new byte[2048];
        int i = 0;
        StringBuilder response = new StringBuilder();
        while ((i = inputStream.read(res)) != -1) {
            response.append(new String(res, 0, i));
        }
        inputStream.close();

        System.out.println("Response= " + response.toString());
        return response.toString();
    }

    public void checkSignerResponeStatus(String checkSignerRespone) throws ParserConfigurationException, IOException, SAXException {


        DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
        // dbf.setNamespaceAware(true);
        Document doc1 =
            dbf1.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(checkSignerRespone), "UTF-8"));
        doc1.setXmlStandalone(true);
        Element element1 = (Element) doc1.getElementsByTagName("SignerRes").item(0);
         SIGNERID_STATUS_INFO =    element1.getAttribute("info");
         SIGNERID_STATUS_ERROR =    element1.getAttribute("error");
    }

    public String createESignXML(ExternalUserEsignRequestDTO externalUserEsignRequestDTO) throws ParserConfigurationException, TransformerException, IOException, NoSuchAlgorithmException, SAXException {

        String eSignXML = "";
        //EsignRequestDTO esignRequestDTO

        Date now = new Date();

        String format3 = new SimpleDateFormat("yyMMddHHmmss", Locale.ENGLISH).format(now);
        String randomNo = RandomStringUtils.randomNumeric(6);
        String txnId = format3 + randomNo;

        System.out.printf("createtrnidddddddddddd@@@@@@@@@@@@@@@@@@", txnId);


        //User XML Creation Work start from here

        MessageDigest md = MessageDigest.getInstance("SHA-256"); //SHA, MD2, MD5, SHA-256, SHA-384...

        String hex = checksum("/var/Board-Notification.pdf", md);


        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;

        dBuilder = dbFactory.newDocumentBuilder();

        Document doc = dBuilder.newDocument();

        doc.setXmlStandalone(true);

        StringWriter writer = new StringWriter();

        Element rootElement = doc.createElement("Esign");

        doc.appendChild(rootElement);

        Attr ver = doc.createAttribute("ver");
        ver.setValue("3.2");
        rootElement.setAttributeNode(ver);

        Attr signerid = doc.createAttribute("signerid");
        signerid.setValue(externalUserEsignRequestDTO.getSignerId());
        rootElement.setAttributeNode(signerid);



        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String formattedDate = myDateObj.format(myFormatObj);


        Attr ts = doc.createAttribute("ts");
        ts.setValue(formattedDate);
        rootElement.setAttributeNode(ts);

        Attr txn = doc.createAttribute("txn");
        txn.setValue(txnId);
        rootElement.setAttributeNode(txn);

        Attr maxWaitPeriod = doc.createAttribute("maxWaitPeriod");
        maxWaitPeriod.setValue("1440");
        rootElement.setAttributeNode(maxWaitPeriod);

        Attr aspId = doc.createAttribute("aspId");
        aspId.setValue("CSORG1000002");
        rootElement.setAttributeNode(aspId);

        Attr responseUrl = doc.createAttribute("responseUrl");
        responseUrl.setValue(externalUserEsignRequestDTO.getResponseUrl());
        rootElement.setAttributeNode(responseUrl);

        Attr redirectUrl = doc.createAttribute("redirectUrl");
        redirectUrl.setValue(externalUserEsignRequestDTO.getRedirectUrl());
        rootElement.setAttributeNode(redirectUrl);

        Attr signingAlgorithm = doc.createAttribute("signingAlgorithm");
        signingAlgorithm.setValue("RSA");
        rootElement.setAttributeNode(signingAlgorithm);



        Element DocsinfoElement = doc.createElement("Docs");
        rootElement.appendChild(DocsinfoElement);



        Element InputHash = doc.createElement("InputHash");

        InputHash.appendChild(doc.createTextNode(hex));


        Attr id = doc.createAttribute("id");
        id.setValue("1");
        InputHash.setAttributeNode(id);

        Attr hashAlgorithm = doc.createAttribute("hashAlgorithm");
        hashAlgorithm.setValue("SHA256");
        InputHash.setAttributeNode(hashAlgorithm);

        Attr docInfo = doc.createAttribute("docInfo");
        docInfo.setValue(externalUserEsignRequestDTO.getDocInfo());
        InputHash.setAttributeNode(docInfo);

        Attr docUrl = doc.createAttribute("docUrl");
        docUrl.setValue("http://htwb.hartron.io/wp-content/uploads/2018/06/Board-Notification.pdf");
        InputHash.setAttributeNode(docUrl);

        Attr responseSigType = doc.createAttribute("responseSigType");
        responseSigType.setValue("pkcs7");
        InputHash.setAttributeNode(responseSigType);

        DocsinfoElement.appendChild(InputHash);

        // create the xml file
        //transform the DOM Object to an XML File
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(doc);
        //StreamResult streamResult = new StreamResult(new File(xmlFilePath));

        // If you use
        StreamResult result = new StreamResult(writer);
        // the output will be pushed to the standard output ...
        // You can use that for debugging

        transformer.transform(domSource, result);

//           TransformerFactory transformerFactory = TransformerFactory.newInstance();

        eSignXML = writer.toString();
        return   eSignXML;
    }




    public byte[] createSignature() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, SignatureException, UnrecoverableKeyException, InvalidKeyException {

        String STORENAME = "/var/DocSignerpkcs.p7b";
        String STOREPASS = "12345678";

        //First load the keystore object by providing the p12 file path
        KeyStore clientStore = KeyStore.getInstance("PKCS7");
        //replace testPass with the p12 password/pin
        clientStore.load(new FileInputStream(STORENAME), STOREPASS.toCharArray());

        Enumeration<String> aliases = clientStore.aliases();
        String aliaz = "";
        while(aliases.hasMoreElements()){
            aliaz = aliases.nextElement();
            if(clientStore.isKeyEntry(aliaz)){
                break;
            }
        }
        X509Certificate c = (X509Certificate)clientStore.getCertificate(aliaz);

        //Data to sign
        byte[] dataToSign = "SigmaWorld".getBytes();
        //compute signature:
        Signature signature = Signature.getInstance("Sha2WithRSA");
        signature.initSign((PrivateKey)clientStore.getKey(aliaz, STOREPASS.toCharArray()));
        signature.update(dataToSign);
        byte[] signedData = signature.sign();

        //load X500Name
        X500Name xName      = X500Name.asX500Name(c.getSubjectX500Principal());
        //load serial number
        BigInteger serial   = c.getSerialNumber();
        //laod digest algorithm
        AlgorithmId digestAlgorithmId = new AlgorithmId(AlgorithmId.SHA_oid);
        //load signing algorithm
        AlgorithmId signAlgorithmId = new AlgorithmId(AlgorithmId.RSAEncryption_oid);

        //Create SignerInfo:
        SignerInfo sInfo = new SignerInfo(xName, serial, digestAlgorithmId, signAlgorithmId, signedData);
        //Create ContentInfo:
        sun.security.pkcs.ContentInfo cInfo = new sun.security.pkcs.ContentInfo(sun.security.pkcs.ContentInfo.DIGESTED_DATA_OID, new DerValue(DerValue.tag_OctetString, dataToSign));
        //Create PKCS7 Signed data
        PKCS7 p7 = new PKCS7(new AlgorithmId[] { digestAlgorithmId }, cInfo,
            new X509Certificate[] { c },
            new SignerInfo[] { sInfo });
        //Write PKCS7 to bYteArray
        ByteArrayOutputStream bOut = new DerOutputStream();
        p7.encodeSignedData(bOut);
        byte[] encodedPKCS7 = bOut.toByteArray();

        System.out.println("pksc77777777777777777777777777777777777" + encodedPKCS7);

        return encodedPKCS7;

    }




    public static byte[] extractData(byte[] pkcs7Data) {
        try {
            CMSSignedData cmsSignedData = new CMSSignedData(pkcs7Data);
            ByteArrayOutputStream bs = new ByteArrayOutputStream();
            cmsSignedData.getSignedContent().write(bs);
            return bs.toByteArray();
        } catch (Exception e) {}
        return new byte[0];
    }




    public String setCSCparamaters(String signedeSignXML, HttpServletResponse response) throws IOException {

        Base64.Encoder encoderTxnRef = Base64.getEncoder();
        String txnRef = encoderTxnRef.encodeToString((txnId +"|"+ ASP_REDIRECT_URL).getBytes());

        System.out.println("txnReffffffffffffff" + txnRef);




        Base64.Encoder encoder = Base64.getEncoder();
        String res_data = encoder.encodeToString(signedeSignXML.getBytes());

        System.out.println("endddres_dataaaaaaaaa" + txnRef);

       // String url = "http://localhost:9001/esign-final-form-request/" + ASP_ID + "/" +  ASP_IP + "/" + txx + "/" + res_data;

        response.sendRedirect(txnRef);


        return txnRef;

    }




    public String signXML(String xmlDoc) throws ParserConfigurationException, TransformerException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IOException, UnrecoverableEntryException, KeyStoreException, MarshalException, XMLSignatureException, SAXException, CertificateException {
        // Create a DOM XMLSignatureFactory that will be used to generate the
        // enveloped signature
        XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

        // Create a Reference to the enveloped document (in this case we are
        // signing the whole document, so a URI of "" signifies that) and
        // also specify the SHA256 digest algorithm and the ENVELOPED Transform.
        Reference ref = fac.newReference
            ("", fac.newDigestMethod(DigestMethod.SHA256, null),
                Collections.singletonList
                    (fac.newTransform
                        (Transform.ENVELOPED, (TransformParameterSpec) null)),
                null, null);

        // Create the SignedInfo
        SignedInfo si = fac.newSignedInfo
            (fac.newCanonicalizationMethod
                    (CanonicalizationMethod.EXCLUSIVE,
                        (C14NMethodParameterSpec) null),
                fac.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", null),
                Collections.singletonList(ref));


        KeyStore p12 = KeyStore.getInstance("pkcs12");
        p12.load(new FileInputStream("/var/DocSigner.pfx"), "12345678".toCharArray());


        Enumeration e = p12.aliases();
        String alias = (String) e.nextElement();
        System.out.println("Alias certifikata:" + alias);
        Key privateKey = p12.getKey(alias, "12345678".toCharArray());



        KeyStore.PrivateKeyEntry keyEntry
            = (KeyStore.PrivateKeyEntry) p12.getEntry(alias, new KeyStore.PasswordProtection("12345678".toCharArray()));

        X509Certificate cert = (X509Certificate) keyEntry.getCertificate();

        KeyInfoFactory kif = fac.getKeyInfoFactory();

        System.out.println(cert.getSerialNumber());

        X509IssuerSerial x509IssuerSerial = kif.newX509IssuerSerial(cert.getSubjectX500Principal().getName(), cert.getSerialNumber());

        List x509Content = new ArrayList();
        System.out.println("ime: " + cert.getSubjectX500Principal().getName());
        x509Content.add(cert.getSubjectX500Principal().getName());
        x509Content.add(x509IssuerSerial);

        X509Data xd = kif.newX509Data(x509Content);
        KeyInfo ki = kif.newKeyInfo(Collections.singletonList(xd));
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document doc =
            dbf.newDocumentBuilder().parse(IOUtils.toInputStream(String.valueOf(xmlDoc), "UTF-8"));
        DOMSignContext dsc = new DOMSignContext(privateKey, doc.getDocumentElement());
        XMLSignature signature = fac.newXMLSignature(si, ki);
        signature.sign(dsc);
        String xmlSignatureOutput = signature.toString();

        String signedXMLObject =  writeXmlDocumentToXmlFile(doc);
        DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document doc1 =
            dbf1.newDocumentBuilder().parse(IOUtils.toInputStream(String.valueOf(signedXMLObject), "UTF-8"));
        doc1.setXmlStandalone(true);
        Element element = (Element) doc1.getElementsByTagName("KeyInfo").item(0);
        element.getParentNode().removeChild(element);
        doc1.normalize();

        String signedXMLObjectWithoutKeyInfo = writeXmlDocumentToXmlFile(doc1);

        System.out.println("Signed XML Without Key Infoooooooooooo :" + signedXMLObjectWithoutKeyInfo);

        return signedXMLObjectWithoutKeyInfo;


    }


//Hexa code start from here




    private static String checksum(String filepath, MessageDigest md) throws IOException {

        // file hashing with DigestInputStream
        try (DigestInputStream dis = new DigestInputStream(new FileInputStream(filepath), md)) {
            while (dis.read() != -1) ; //empty loop to clear the data
            md = dis.getMessageDigest();
        }

        // bytes to hex
        StringBuilder result = new StringBuilder();
        for (byte b : md.digest()) {
            result.append(String.format("%02x", b));       }


        System.out.println("haxrrrrrrrrrrrrrrrrrrrr" + result.toString());


        return result.toString();

    }

    private String  writeXmlDocumentToXmlFile(Document xmlDocument)    {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        String signedXMLObject ="";
        try {
            transformer = tf.newTransformer();

            // Uncomment if you do not require XML declaration
            // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            //A character stream that collects its output in a string buffer,
            //which can then be used to construct a string.
            StringWriter writer = new StringWriter();

            //transform document to string
            transformer.transform(new DOMSource(xmlDocument), new StreamResult(writer));

            String xmlString = writer.getBuffer().toString();
            System.out.println(xmlString);
            signedXMLObject = xmlString;
            //Print to console or logs
        }
        catch (TransformerException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return signedXMLObject;
    }

    public String saveTransaction(String signedeSignXML, EsignRequestDTO esignRequestDTO) throws ParserConfigurationException, IOException, SAXException {


        System.out.println("oooooooooooooooooooooooooooooooo" + esignRequestDTO);

        DocumentBuilderFactory df_eSignRequest= DocumentBuilderFactory.newInstance();
        Document doc_eSignRequest =
            df_eSignRequest.newDocumentBuilder().parse(IOUtils.toInputStream(String.valueOf(signedeSignXML), "UTF-8"));
        doc_eSignRequest.setXmlStandalone(true);
        Element element_eSignRequest = (Element) doc_eSignRequest.getElementsByTagName("Esign").item(0);
        String txn_eSignRequest =    element_eSignRequest.getAttribute("txn");

        String ts = element_eSignRequest.getAttribute("ts");

        EsignTransation esignTransation = new EsignTransation();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        esignTransation.setTxnId(txn_eSignRequest);
        esignTransation.setUserCodeId("USER_000011");
        esignTransation.setTs(ts);
        //esignTransation.setRequestXml(signedeSignXML);
        esignTransation.setRequestType("Esign Request");
        esignTransation.setRequestStatus("Create");

        System.out.println("save_trnsactionnnnnnnnnnnnnnnnnnn" + esignTransation);

        esignTransationRepository.save(esignTransation);
        esignTransationSearchRepository.save(esignTransation);

        return esignTransation.toString();

    }


    public static PublicKey GetPublicKey() throws FileNotFoundException, KeyStoreException, CertificateException {



        PublicKey publicKey = null;
        FileInputStream is = null;

        try
        {
            is = new FileInputStream("/var/DSCPublicKey.cer");
//            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
//            keystore.load(is, "12345678".toCharArray());
//            String alias = "Alias";
//            Key key = keystore.getKey(alias, "password".toCharArray());
//            if (key instanceof PrivateKey)
//            {

//                 Get certificate of public key
//                java.security.Certificate cert = (java.security.Certificate) keystore.getCertificate(alias);
            CertificateFactory fact = CertificateFactory.getInstance("X.509");
            X509Certificate cert =  (X509Certificate) fact.generateCertificate(is);

            // Get public key
            publicKey = cert.getPublicKey();

//            }
        }
        catch (FileNotFoundException ex)
        {
        }
        catch ( IOException ex)
        {
        }
        finally
        {
            try
            {
                is.close();
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }


        return publicKey;

    }

}

