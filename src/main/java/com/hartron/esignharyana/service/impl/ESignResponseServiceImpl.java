package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.domain.EsignTransation;
import com.hartron.esignharyana.domain.UserDetails;
import com.hartron.esignharyana.repository.EsignTransationRepository;
import com.hartron.esignharyana.repository.search.EsignTransationSearchRepository;
import com.hartron.esignharyana.repository.search.ExternalUserEsignRequestSearchRepository;
import com.hartron.esignharyana.repository.search.UserDetailsSearchRepository;
import com.hartron.esignharyana.service.EsignResponseService;
import com.hartron.esignharyana.service.dto.EsignTransationDTO;
import com.hartron.esignharyana.service.dto.ExternalUserEsignRequestDTO;
import com.hartron.esignharyana.service.mapper.EsignTransationMapper;
import com.hartron.esignharyana.service.mapper.ExternalUserEsignRequestMapper;
import com.hartron.esignharyana.service.mapper.UserDetailsMapper;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.keyinfo.X509IssuerSerial;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Service Implementation for managing {@link UserDetails}.
 */
@Service
@Transactional
public class ESignResponseServiceImpl implements EsignResponseService {

    private final Logger log = LoggerFactory.getLogger(ESignResponseServiceImpl.class);

    private String txnId = "";

    private final UserDetailsMapper userDetailsMapper;

    private final UserDetailsSearchRepository userDetailsSearchRepository;

    private final EsignTransationRepository esignTransationRepository;

    private final EsignTransationSearchRepository esignTransationSearchRepository;

    private final EsignTransationMapper esignTransationMapper;

    private final ExternalUserEsignRequestSearchRepository externalUserEsignRequestSearchRepository;

    private final ExternalUserEsignRequestMapper externalUserEsignRequestMapper;

    private String CHECK_ESIGN_RESPONSE_STATUS = "";
    private String ESIGN_RESPONSE_TXNID = "";
    private String ESIGN_RESPONSE_DOCSIGNATURE ="";
    private  String ESIGN_RESPONSE_TITLE = "";
    private  String EXTERNAL_USER_RESPONSE_URL="";
    private  String EXTERNAL_USER_REDIRECT_URL="";
    private static final String POST_URL_ESIGN_STATUS = "http://dsp.csccloud.in/esignauth/sign/status";




    public ESignResponseServiceImpl(UserDetailsMapper userDetailsMapper, UserDetailsSearchRepository userDetailsSearchRepository, EsignTransationRepository esignTransationRepository, EsignTransationSearchRepository esignTransationSearchRepository, EsignTransationMapper esignTransationMapper, ExternalUserEsignRequestSearchRepository externalUserEsignRequestSearchRepository, ExternalUserEsignRequestMapper externalUserEsignRequestMapper) {

        this.userDetailsMapper = userDetailsMapper;
        this.userDetailsSearchRepository = userDetailsSearchRepository;

        this.esignTransationRepository = esignTransationRepository;
        this.esignTransationSearchRepository = esignTransationSearchRepository;
        this.esignTransationMapper = esignTransationMapper;
        this.externalUserEsignRequestSearchRepository = externalUserEsignRequestSearchRepository;
        this.externalUserEsignRequestMapper = externalUserEsignRequestMapper;
    }



    public String saveEsignResponse(String esignResponse, HttpServletResponse response) throws ParserConfigurationException, IOException, SAXException, TransformerException, JSONException, DocumentException {

      //  String[] split_response = esignResponse.split("=", 2);

       // System.out.println("split_responsessssssssss" + split_response);

      //  String final_string  = split_response[1];

     //   System.out.println("final_stringfffffff" + final_string);

      //  Base64.Decoder decoder = Base64.getDecoder();

      //  JSONObject decodedResponse = new JSONObject(decoder.decode(final_string));

     //   System.out.println("decodedResponsesesesesese" + decodedResponse);


      //  String res_xml = decodedResponse.get("res_xml").toString();

      //  System.out.println("responsejSON" + res_xml);

      //  Base64.Decoder decoder1 = Base64.getDecoder();

     //   String lastResponseXml = new String(decoder1.decode(res_xml));


     //   System.out.println("lastResponseXmlllllll" + lastResponseXml);



        String getDocSignature = getDocSignature(esignResponse);

        String signPDF = signPdf(getDocSignature);


        //String EsignRespone =  esignResponse();

        // String saveTransaction = saveTransaction(EsignRespone);


        return  signPDF;

    }

    public String getDocSignature(String lastResponseXml) throws ParserConfigurationException, IOException, SAXException {

        String DocSignature = "";


        DocumentBuilderFactory df_eSignResponse = DocumentBuilderFactory.newInstance();
        Document doc_eSignResponse =
            df_eSignResponse.newDocumentBuilder().parse(IOUtils.toInputStream(String.valueOf(lastResponseXml), "UTF-8"));
        doc_eSignResponse.setXmlStandalone(true);
        Element element_eSignResponse = (Element) doc_eSignResponse.getElementsByTagName("EsignResp").item(0);
        // encode string using Base64 encoder


        // Decoding string

        Element element_DocSignature = (Element) doc_eSignResponse.getElementsByTagName("DocSignature").item(0);
        //String abc = element_UserX509Certificate.getTextContent(element_UserX509Certificate);

         DocSignature =  element_DocSignature.getTextContent();

        return  DocSignature;

    }



    public String saveEsignRedirect(String saveEsignRedirect, HttpServletResponse response) throws ParserConfigurationException, IOException, SAXException, TransformerException, JSONException, CertificateException, MarshalException, NoSuchAlgorithmException, KeyStoreException, XMLSignatureException, UnrecoverableEntryException, InvalidAlgorithmParameterException, DocumentException, InterruptedException {

        String sendEsignStatus = "";

        String[] split_response = saveEsignRedirect.split("=", 2);

        byte[] decodedBytes = Base64.decodeBase64(split_response[1].getBytes());

        String decoded = new String(decodedBytes, StandardCharsets.UTF_8) ;

        JSONObject stringToJshon = new JSONObject(decoded);

        String res_xml = stringToJshon.get("res_xml").toString();

        System.out.println(res_xml);

        byte[] decodedBytesForResXml = Base64.decodeBase64(res_xml);

        String decodedForResXml = new String(decodedBytesForResXml, StandardCharsets.UTF_8);

        System.out.println(decodedForResXml);

        String saveTransaction = saveTransaction(decodedForResXml);

        System.out.println("trnsaaveeeeeeeeeeeeeee" + saveTransaction);

        if (CHECK_ESIGN_RESPONSE_STATUS.equals("0")){
            String  signStatusXml = signStatus(ESIGN_RESPONSE_TXNID);
            response.sendRedirect("http://stagingesign.hartron.org.in/esign-response-data/");

        }

        if (CHECK_ESIGN_RESPONSE_STATUS.equals("2")){
            String  signStatusXml = signStatus(ESIGN_RESPONSE_TXNID);
            String reqData = getReqData(signStatusXml);
            response.sendRedirect("http://stagingesign.hartron.org.in/esign-response-data");
            System.out.println("begore slepingggggggggggggggggggggg");
            TimeUnit.SECONDS.sleep(30);
            System.out.println("after slepingggggggggggggggggggggg");
            sendEsignStatus = sendEsignStatus(signStatusXml);
        }

        return sendEsignStatus;

    }

    public String signPdf(String DocSignature) throws IOException, DocumentException {
        // encode string using Base64 encoder
        String signPdf = "";
    //    Base64.Decoder decoder1 = Base64.getDecoder();

        byte[] decodedBytes = Base64.decodeBase64(DocSignature.getBytes());

        String dStr1 = new String(decodedBytes, StandardCharsets.UTF_8) ;


        // Decoding string
       // String dStr1 = new String(decoder1.decode(DocSignature.getBytes("UTF8")));


        // Creating the reader and the stamper


        String fileInputPath = "/var/input.pdf";
        String Outputfilepath = "/var/output.pdf";


        PdfReader readerpdf = new PdfReader(fileInputPath);
        OutputStream fout = new FileOutputStream(Outputfilepath);

        PdfStamper stamperpdf = PdfStamper.createSignature(readerpdf, fout, '\0');
        PdfSignatureAppearance appearance = stamperpdf.getSignatureAppearance();
        appearance.setReason("I Approve this file");
        appearance.setContact("7357695886");
        appearance.setLocation("Chandigarh");
        appearance.setSignatureCreator("CSC");

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, 5);
        appearance.setSignDate(cal);
        appearance.setCertificationLevel(PdfSignatureAppearance.NOT_CERTIFIED);
        appearance.setImage(null);
        appearance.setVisibleSignature(new com.itextpdf.text.Rectangle(36, 748, 144, 780), 1, "sig");
        SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date=appearance.getSignDate().getTime();
        appearance.setLayer2Text("Digitally Signed By CSC" + "\n"+"Reason: "+appearance.getReason()+"\n"+"Location: "+appearance.getLocation()+"\nSigned on "+ format.format(date));
        int contentEstimated = 8192;
        HashMap<PdfName, Integer> exc = new HashMap<>();
        exc.put(PdfName.CONTENTS, contentEstimated * 2 + 2);
        PdfSignature dic = new PdfSignature(PdfName.ADOBE_PPKLITE, PdfName.ADBE_PKCS7_DETACHED);
        dic.setReason(appearance.getReason());
        dic.setLocation(appearance.getLocation());
        dic.setSignatureCreator(appearance.getSignatureCreator());
        appearance.setCryptoDictionary(dic);
        appearance.preClose(exc);

        byte[] paddedSig = new byte[contentEstimated];
        byte[] PKCS7Response = Base64.decodeBase64(DocSignature.getBytes());

        System.out.println("PKCS7Responseeeeeeeeeeeeeeee" + PKCS7Response);

        System.arraycopy(PKCS7Response, 0, paddedSig, 0, PKCS7Response.length);
        PdfDictionary dic2 = new PdfDictionary();
        dic2.put(PdfName.CONTENTS, new PdfString(paddedSig).setHexWriting(true));

        System.out.println("PdfNameeeeeeeeeeeeee" + PdfName.CONTENTS);

        appearance.close(dic2);

        return  signPdf.toString();


    }


    public String saveTransaction(String EsignRespone) throws ParserConfigurationException, IOException, SAXException, CertificateException, InvalidAlgorithmParameterException, TransformerException, KeyStoreException, MarshalException, XMLSignatureException, NoSuchAlgorithmException, UnrecoverableEntryException {

        System.out.println("enter into save methoddddd");
        DocumentBuilderFactory df_eSignRequest= DocumentBuilderFactory.newInstance();
        Document doc_eSignRequest =
            df_eSignRequest.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(EsignRespone), "UTF-8"));
        doc_eSignRequest.setXmlStandalone(true);
        Element element_eSignRequest = (Element) doc_eSignRequest.getElementsByTagName("EsignResp").item(0);

        String txnId =    element_eSignRequest.getAttribute("txn");
        String ts = element_eSignRequest.getAttribute("ts");
        String status = element_eSignRequest.getAttribute("status");
        String resCode = element_eSignRequest.getAttribute("resCode");

        CHECK_ESIGN_RESPONSE_STATUS = status;
        ESIGN_RESPONSE_TXNID = txnId;
        String requestType = "Esign Request";

        Optional<EsignTransationDTO> esignTransationDTO =  esignTransationSearchRepository.findByTxnIdAndRequestType(txnId, requestType).map(esignTransationMapper::toDto);
        Optional<ExternalUserEsignRequestDTO> externalUserEsignRequestDTO =  externalUserEsignRequestSearchRepository.findByTxn(txnId).map(externalUserEsignRequestMapper::toDto);

        EXTERNAL_USER_RESPONSE_URL = externalUserEsignRequestDTO.get().getResponseUrl();
        EXTERNAL_USER_REDIRECT_URL = externalUserEsignRequestDTO.get().getRedirectUrl();


       String userCodeId =  esignTransationDTO.get().getUserCodeId();
       String applicationIdCode =  esignTransationDTO.get().getApplicationCodeId();
       String signerId = esignTransationDTO.get().getSignerId();

        EsignTransationDTO esignTransationDTO1 = new EsignTransationDTO();
        esignTransationDTO1.setTxnId(txnId);
        esignTransationDTO1.setTs(ts);
        esignTransationDTO1.setRequestType("Esign Response");
        esignTransationDTO1.setResponseStatus(status);
        esignTransationDTO1.setResponseCode(resCode);
        esignTransationDTO1.setSignerId(signerId);
        esignTransationDTO1.setUserCodeId(userCodeId);
        esignTransationDTO1.setApplicationCodeId(applicationIdCode);

        System.out.println("esignTransationDTO1111111" + esignTransationDTO1);

        EsignTransation esignTransation = esignTransationMapper.toEntity(esignTransationDTO1);
        esignTransationRepository.save(esignTransation);
        esignTransationSearchRepository.save(esignTransation);

        return esignTransation.toString();
    }


    public String sendEsignStatus(String signStatusXml) throws IOException {
        String request = signStatusXml;

        URL url = new URL(POST_URL_ESIGN_STATUS);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        // Set timeout as per needs
        connection.setConnectTimeout(20000);
        connection.setReadTimeout(20000);

        // Set DoOutput to true if you want to use URLConnection for output.
        // Default is false
        connection.setDoOutput(true);

        connection.setUseCaches(true);
        connection.setRequestMethod("POST");

        // Set Headers
//        connection.setRequestProperty("Accept", "application/xml");
        connection.setRequestProperty("Content-Type", "application/xml");
        connection.setRequestProperty("Cache-Control", "no-cache");
        connection.setRequestProperty("aspid","CSORG1000002");


        // Write XML
        OutputStream outputStream = connection.getOutputStream();
        byte[] b = request.getBytes("UTF-8");
        outputStream.write(b);
        outputStream.flush();
        outputStream.close();

        // Read XML
        InputStream inputStream = connection.getInputStream();
        byte[] res = new byte[2048];
        int i = 0;
        StringBuilder response = new StringBuilder();
        while ((i = inputStream.read(res)) != -1) {
            response.append(new String(res, 0, i));
        }
        inputStream.close();

        System.out.println("Response= " + response.toString());
        return response.toString();
    }




    public String signStatus(String txnId) throws ParserConfigurationException, TransformerException, IOException, CertificateException, NoSuchAlgorithmException, XMLSignatureException, UnrecoverableEntryException, InvalidAlgorithmParameterException, SAXException, MarshalException, KeyStoreException {

        String EsignStatusXML = "";

        String esign_responde = "Esign Response";

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder1;

        dBuilder1 = dbFactory.newDocumentBuilder();

        Document doc = dBuilder1.newDocument();

        doc.setXmlStandalone(true);

        StringWriter writer = new StringWriter();

        Element rootElement = doc.createElement("EsignStatus");

        doc.appendChild(rootElement);

        Attr ver = doc.createAttribute("ver");
        ver.setValue("3.2");
        rootElement.setAttributeNode(ver);

        Attr txn = doc.createAttribute("txn");
        txn.setValue(txnId);
        rootElement.setAttributeNode(txn);

        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String formattedDate = myDateObj.format(myFormatObj);


        Attr ts = doc.createAttribute("ts");
        ts.setValue(formattedDate);
        rootElement.setAttributeNode(ts);


        Attr aspId = doc.createAttribute("aspId");
        aspId.setValue("CSORG1000002");
        rootElement.setAttributeNode(aspId);

        // create the xml file
        //transform the DOM Object to an XML File
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(doc);
        //StreamResult streamResult = new StreamResult(new File(xmlFilePath));

        // If you use
        StreamResult result = new StreamResult(writer);
        // the output will be pushed to the standard output ...
        // You can use that for debugging

        transformer.transform(domSource, result);

//           TransformerFactory transformerFactory = TransformerFactory.newInstance();

        EsignStatusXML = writer.toString();

        System.out.println("withoutsignnnnnnnnnnnnnnnnn" + EsignStatusXML);

        String EsignStatusXMLForSignature = signXML(EsignStatusXML);

        System.out.println("Signed xmlllllllllllllllllllll" + EsignStatusXMLForSignature);

        //String sendEsignStatus = sendEsignStatus(EsignStatusXMLForSignature);


        return EsignStatusXMLForSignature;
    }


    public String getReqData(String signStatusXml){

        String res_data = Base64.encodeBase64String(signStatusXml.getBytes());
        return res_data;
    }


    public String signXML(String xmlDoc) throws ParserConfigurationException, TransformerException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IOException, UnrecoverableEntryException, KeyStoreException, MarshalException, XMLSignatureException, SAXException, CertificateException {
        // Create a DOM XMLSignatureFactory that will be used to generate the
        // enveloped signature
        XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

        // Create a Reference to the enveloped document (in this case we are
        // signing the whole document, so a URI of "" signifies that) and
        // also specify the SHA256 digest algorithm and the ENVELOPED Transform.
        Reference ref = fac.newReference
            ("", fac.newDigestMethod(DigestMethod.SHA256, null),
                Collections.singletonList
                    (fac.newTransform
                        (Transform.ENVELOPED, (TransformParameterSpec) null)),
                null, null);

        // Create the SignedInfo
        SignedInfo si = fac.newSignedInfo
            (fac.newCanonicalizationMethod
                    (CanonicalizationMethod.EXCLUSIVE,
                        (C14NMethodParameterSpec) null),
                fac.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", null),
                Collections.singletonList(ref));


        KeyStore p12 = KeyStore.getInstance("pkcs12");
        p12.load(new FileInputStream("/var/DocSigner.pfx"), "12345678".toCharArray());


        Enumeration e = p12.aliases();
        String alias = (String) e.nextElement();
        System.out.println("Alias certifikata:" + alias);
        Key privateKey = p12.getKey(alias, "12345678".toCharArray());



        KeyStore.PrivateKeyEntry keyEntry
            = (KeyStore.PrivateKeyEntry) p12.getEntry(alias, new KeyStore.PasswordProtection("12345678".toCharArray()));

        X509Certificate cert = (X509Certificate) keyEntry.getCertificate();

        KeyInfoFactory kif = fac.getKeyInfoFactory();

        System.out.println(cert.getSerialNumber());

        X509IssuerSerial x509IssuerSerial = kif.newX509IssuerSerial(cert.getSubjectX500Principal().getName(), cert.getSerialNumber());

        List x509Content = new ArrayList();
        System.out.println("ime: " + cert.getSubjectX500Principal().getName());
        x509Content.add(cert.getSubjectX500Principal().getName());
        x509Content.add(x509IssuerSerial);

        X509Data xd = kif.newX509Data(x509Content);
        KeyInfo ki = kif.newKeyInfo(Collections.singletonList(xd));
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document doc =
            dbf.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(xmlDoc), "UTF-8"));
        DOMSignContext dsc = new DOMSignContext(privateKey, doc.getDocumentElement());
        XMLSignature signature = fac.newXMLSignature(si, ki);
        signature.sign(dsc);
        String xmlSignatureOutput = signature.toString();

        String signedXMLObject =  writeXmlDocumentToXmlFile(doc);
        DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document doc1 =
            dbf1.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(signedXMLObject), "UTF-8"));
        doc1.setXmlStandalone(true);
        Element element = (Element) doc1.getElementsByTagName("KeyInfo").item(0);
        element.getParentNode().removeChild(element);
        doc1.normalize();

        String signedXMLObjectWithoutKeyInfo = writeXmlDocumentToXmlFile(doc1);

        System.out.println("Signed XML Without Key Infoooooooooooo :" + signedXMLObjectWithoutKeyInfo);

        return signedXMLObjectWithoutKeyInfo;


    }


    private String  writeXmlDocumentToXmlFile(Document xmlDocument)
    {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        String signedXMLObject ="";
        try {
            transformer = tf.newTransformer();

            // Uncomment if you do not require XML declaration
            // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            //A character stream that collects its output in a string buffer,
            //which can then be used to construct a string.
            StringWriter writer = new StringWriter();

            //transform document to string
            transformer.transform(new DOMSource(xmlDocument), new StreamResult(writer));

            String xmlString = writer.getBuffer().toString();
            System.out.println(xmlString);
            signedXMLObject = xmlString;
            //Print to console or logs
        }
        catch (TransformerException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return signedXMLObject;
    }





}




