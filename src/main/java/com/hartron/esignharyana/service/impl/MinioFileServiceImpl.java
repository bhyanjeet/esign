package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.config.MinioConfig;
import com.hartron.esignharyana.service.MinioFileService;
import io.minio.MinioClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;


/**
 * Service Interface for managing Clients.
 */
@Service
public class MinioFileServiceImpl implements MinioFileService
{
    private final Logger log = LoggerFactory.getLogger(MinioFileServiceImpl.class);

    private final MinioConfig minioConfig;

    public MinioFileServiceImpl(MinioConfig minioConfig) {
        this.minioConfig = minioConfig;
    }

    public void minioSave(MultipartFile file, String filename)
    {
        try
        {
            InputStream is=file.getInputStream();

            MinioClient minioClient = minioConfig.getMinioClient();

            boolean isExist = minioClient.bucketExists("esignharyana");
            if(isExist) {
                log.warn("Bucket already exists.");
            }
            else {
                minioClient.makeBucket("esignharyana");
            }

            // Upload the zip file to the bucket with putObject
            minioClient.putObject("esignharyana",filename, is, is.available(),"application/octet-stream");
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }

    }
    public String multipleFileMinioSave(MultipartFile file, String filename)
    {
        try
        {
            InputStream is=file.getInputStream();

            MinioClient minioClient = minioConfig.getMinioClient();


            boolean isExist = minioClient.bucketExists("esignharyana");
            if(isExist) {
                log.warn("Bucket already exists.");
            }
            else {
                minioClient.makeBucket("esignharyana");
            }

            // Upload the zip file to the bucket with putObject
            minioClient.putObject("esignharyana",filename.toString(), is, is.available(),"application/octet-stream");
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
        return filename;
    }
}

