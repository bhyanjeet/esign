package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.service.OrganisationDocumentService;
import com.hartron.esignharyana.domain.OrganisationDocument;
import com.hartron.esignharyana.repository.OrganisationDocumentRepository;
import com.hartron.esignharyana.repository.search.OrganisationDocumentSearchRepository;
import com.hartron.esignharyana.service.dto.OrganisationDocumentDTO;
import com.hartron.esignharyana.service.mapper.OrganisationDocumentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link OrganisationDocument}.
 */
@Service
@Transactional
public class OrganisationDocumentServiceImpl implements OrganisationDocumentService {

    private final Logger log = LoggerFactory.getLogger(OrganisationDocumentServiceImpl.class);

    private final OrganisationDocumentRepository organisationDocumentRepository;

    private final OrganisationDocumentMapper organisationDocumentMapper;

    private final OrganisationDocumentSearchRepository organisationDocumentSearchRepository;


    public OrganisationDocumentServiceImpl(OrganisationDocumentRepository organisationDocumentRepository, OrganisationDocumentMapper organisationDocumentMapper, OrganisationDocumentSearchRepository organisationDocumentSearchRepository) {
        this.organisationDocumentRepository = organisationDocumentRepository;
        this.organisationDocumentMapper = organisationDocumentMapper;
        this.organisationDocumentSearchRepository = organisationDocumentSearchRepository;
    }

    /**
     * Save a organisationDocument.
     *
     * @param organisationDocumentDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OrganisationDocumentDTO save(OrganisationDocumentDTO organisationDocumentDTO) {
        log.debug("Request to save OrganisationDocument : {}", organisationDocumentDTO);
        OrganisationDocument organisationDocument = organisationDocumentMapper.toEntity(organisationDocumentDTO);
        organisationDocument = organisationDocumentRepository.save(organisationDocument);
        OrganisationDocumentDTO result = organisationDocumentMapper.toDto(organisationDocument);
        organisationDocumentSearchRepository.save(organisationDocument);
        return result;
    }

    /**
     * Get all the organisationDocuments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrganisationDocumentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrganisationDocuments");
        return organisationDocumentRepository.findAll(pageable)
            .map(organisationDocumentMapper::toDto);
    }


    /**
     * Get one organisationDocument by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OrganisationDocumentDTO> findOne(Long id) {
        log.debug("Request to get OrganisationDocument : {}", id);
        return organisationDocumentRepository.findById(id)
            .map(organisationDocumentMapper::toDto);
    }

    /**
     * Delete the organisationDocument by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrganisationDocument : {}", id);
        organisationDocumentRepository.deleteById(id);
        organisationDocumentSearchRepository.deleteById(id);
    }

    /**
     * Search for the organisationDocument corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrganisationDocumentDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of OrganisationDocuments for query {}", query);
        return organisationDocumentSearchRepository.search(queryStringQuery(query), pageable)
            .map(organisationDocumentMapper::toDto);
    }

    public List<OrganisationDocumentDTO> findOrganisationDocumentByOrganisationTypeMasterId(Long id) {
        return organisationDocumentSearchRepository.findOrganisationDocumentByOrganisationTypeMasterId(id).stream().map(organisationDocumentMapper::toDto).collect(Collectors.toList());
    }
}
