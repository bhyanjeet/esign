package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.service.BlockMasterService;
import com.hartron.esignharyana.domain.BlockMaster;
import com.hartron.esignharyana.repository.BlockMasterRepository;
import com.hartron.esignharyana.repository.search.BlockMasterSearchRepository;
import com.hartron.esignharyana.service.dto.BlockMasterDTO;
import com.hartron.esignharyana.service.mapper.BlockMasterMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link BlockMaster}.
 */
@Service
@Transactional
public class BlockMasterServiceImpl implements BlockMasterService {

    private final Logger log = LoggerFactory.getLogger(BlockMasterServiceImpl.class);

    private final BlockMasterRepository blockMasterRepository;

    private final BlockMasterMapper blockMasterMapper;

    private final BlockMasterSearchRepository blockMasterSearchRepository;

    public BlockMasterServiceImpl(BlockMasterRepository blockMasterRepository, BlockMasterMapper blockMasterMapper, BlockMasterSearchRepository blockMasterSearchRepository) {
        this.blockMasterRepository = blockMasterRepository;
        this.blockMasterMapper = blockMasterMapper;
        this.blockMasterSearchRepository = blockMasterSearchRepository;
    }

    /**
     * Save a blockMaster.
     *
     * @param blockMasterDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public BlockMasterDTO save(BlockMasterDTO blockMasterDTO) {
        log.debug("Request to save BlockMaster : {}", blockMasterDTO);
        BlockMaster blockMaster = blockMasterMapper.toEntity(blockMasterDTO);
        blockMaster = blockMasterRepository.save(blockMaster);
        BlockMasterDTO result = blockMasterMapper.toDto(blockMaster);
        blockMasterSearchRepository.save(blockMaster);
        return result;
    }

    /**
     * Get all the blockMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BlockMasterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BlockMasters");
        return blockMasterRepository.findAll(pageable)
            .map(blockMasterMapper::toDto);
    }


    /**
     * Get one blockMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BlockMasterDTO> findOne(Long id) {
        log.debug("Request to get BlockMaster : {}", id);
        return blockMasterRepository.findById(id)
            .map(blockMasterMapper::toDto);
    }

    /**
     * Delete the blockMaster by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete BlockMaster : {}", id);
        blockMasterRepository.deleteById(id);
        blockMasterSearchRepository.deleteById(id);
    }

    /**
     * Search for the blockMaster corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BlockMasterDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of BlockMasters for query {}", query);
        return blockMasterSearchRepository.search(queryStringQuery(query), pageable)
            .map(blockMasterMapper::toDto);
    }
}
