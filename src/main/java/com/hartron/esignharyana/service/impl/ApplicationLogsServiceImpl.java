package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.service.ApplicationLogsService;
import com.hartron.esignharyana.domain.ApplicationLogs;
import com.hartron.esignharyana.repository.ApplicationLogsRepository;
import com.hartron.esignharyana.repository.search.ApplicationLogsSearchRepository;
import com.hartron.esignharyana.service.dto.ApplicationLogsDTO;
import com.hartron.esignharyana.service.mapper.ApplicationLogsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link ApplicationLogs}.
 */
@Service
@Transactional
public class ApplicationLogsServiceImpl implements ApplicationLogsService {

    private final Logger log = LoggerFactory.getLogger(ApplicationLogsServiceImpl.class);

    private final ApplicationLogsRepository applicationLogsRepository;

    private final ApplicationLogsMapper applicationLogsMapper;

    private final ApplicationLogsSearchRepository applicationLogsSearchRepository;

    public ApplicationLogsServiceImpl(ApplicationLogsRepository applicationLogsRepository, ApplicationLogsMapper applicationLogsMapper, ApplicationLogsSearchRepository applicationLogsSearchRepository) {
        this.applicationLogsRepository = applicationLogsRepository;
        this.applicationLogsMapper = applicationLogsMapper;
        this.applicationLogsSearchRepository = applicationLogsSearchRepository;
    }

    /**
     * Save a applicationLogs.
     *
     * @param applicationLogsDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ApplicationLogsDTO save(ApplicationLogsDTO applicationLogsDTO) {
        log.debug("Request to save ApplicationLogs : {}", applicationLogsDTO);
        ApplicationLogs applicationLogs = applicationLogsMapper.toEntity(applicationLogsDTO);
        applicationLogs = applicationLogsRepository.save(applicationLogs);
        ApplicationLogsDTO result = applicationLogsMapper.toDto(applicationLogs);
        applicationLogsSearchRepository.save(applicationLogs);
        return result;
    }

    /**
     * Get all the applicationLogs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ApplicationLogsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ApplicationLogs");
        return applicationLogsRepository.findAll(pageable)
            .map(applicationLogsMapper::toDto);
    }


    /**
     * Get one applicationLogs by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ApplicationLogsDTO> findOne(Long id) {
        log.debug("Request to get ApplicationLogs : {}", id);
        return applicationLogsRepository.findById(id)
            .map(applicationLogsMapper::toDto);
    }

    /**
     * Delete the applicationLogs by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ApplicationLogs : {}", id);
        applicationLogsRepository.deleteById(id);
        applicationLogsSearchRepository.deleteById(id);
    }

    /**
     * Search for the applicationLogs corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ApplicationLogsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ApplicationLogs for query {}", query);
        return applicationLogsSearchRepository.search(queryStringQuery(query), pageable)
            .map(applicationLogsMapper::toDto);
    }
}
