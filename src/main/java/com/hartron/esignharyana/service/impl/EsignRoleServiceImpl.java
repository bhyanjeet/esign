package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.service.EsignRoleService;
import com.hartron.esignharyana.domain.EsignRole;
import com.hartron.esignharyana.repository.EsignRoleRepository;
import com.hartron.esignharyana.repository.search.EsignRoleSearchRepository;
import com.hartron.esignharyana.service.dto.EsignRoleDTO;
import com.hartron.esignharyana.service.mapper.EsignRoleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link EsignRole}.
 */
@Service
@Transactional
public class EsignRoleServiceImpl implements EsignRoleService {

    private final Logger log = LoggerFactory.getLogger(EsignRoleServiceImpl.class);

    private final EsignRoleRepository esignRoleRepository;

    private final EsignRoleMapper esignRoleMapper;

    private final EsignRoleSearchRepository esignRoleSearchRepository;

    public EsignRoleServiceImpl(EsignRoleRepository esignRoleRepository, EsignRoleMapper esignRoleMapper, EsignRoleSearchRepository esignRoleSearchRepository) {
        this.esignRoleRepository = esignRoleRepository;
        this.esignRoleMapper = esignRoleMapper;
        this.esignRoleSearchRepository = esignRoleSearchRepository;
    }

    /**
     * Save a esignRole.
     *
     * @param esignRoleDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EsignRoleDTO save(EsignRoleDTO esignRoleDTO) {
        log.debug("Request to save EsignRole : {}", esignRoleDTO);
        EsignRole esignRole = esignRoleMapper.toEntity(esignRoleDTO);
        esignRole = esignRoleRepository.save(esignRole);
        EsignRoleDTO result = esignRoleMapper.toDto(esignRole);
        esignRoleSearchRepository.save(esignRole);
        return result;
    }

    /**
     * Get all the esignRoles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EsignRoleDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EsignRoles");
        return esignRoleRepository.findAll(pageable)
            .map(esignRoleMapper::toDto);
    }


    /**
     * Get one esignRole by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EsignRoleDTO> findOne(Long id) {
        log.debug("Request to get EsignRole : {}", id);
        return esignRoleRepository.findById(id)
            .map(esignRoleMapper::toDto);
    }

    /**
     * Delete the esignRole by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EsignRole : {}", id);
        esignRoleRepository.deleteById(id);
        esignRoleSearchRepository.deleteById(id);
    }

    /**
     * Search for the esignRole corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EsignRoleDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of EsignRoles for query {}", query);
        return esignRoleSearchRepository.search(queryStringQuery(query), pageable)
            .map(esignRoleMapper::toDto);
    }
}
