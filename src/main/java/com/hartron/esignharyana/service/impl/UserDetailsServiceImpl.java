package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.config.Constants;
import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.repository.*;
import com.hartron.esignharyana.repository.search.*;
import com.hartron.esignharyana.security.AuthoritiesConstants;
import com.hartron.esignharyana.security.SecurityUtils;
import com.hartron.esignharyana.service.*;
import com.hartron.esignharyana.service.dto.UserDetailsDTO;
import com.hartron.esignharyana.service.mapper.ApplicationMasterMapper;
import com.hartron.esignharyana.service.mapper.OrganisationMasterMapper;
import com.hartron.esignharyana.service.mapper.UserDetailsMapper;
import com.hartron.esignharyana.service.util.RandomUtil;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link UserDetails}.
 */
@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    private final UserDetailsRepository userDetailsRepository;

    private final UserDetailsMapper userDetailsMapper;

    private final UserDetailsSearchRepository userDetailsSearchRepository;

    private final OrganisationMasterMapper organisationMasterMapper;

    private final OrganisationMasterSearchRepository organisationMasterSearchRepository;

    private final UserService userService;

    private final UserRepository userRepository;

    private final UserSearchRepository userSearchRepository;

    private final PasswordEncoder passwordEncoder;

    private final UserApplicationService userApplicationService;

    private final UserApplicationSearchRepository userApplicationSearchRepository;

    private final UserApplicationRepository userApplicationRepository;


    private final ApplicationMasterRepository applicationMasterRepository;

    private final ApplicationMasterSearchRepository applicationMasterSearchRepository;

    private final ApplicationMasterMapper applicationMasterMapper;

    private final UserLogsService userLogsService;

    private final UserLogsSearchRepository userLogsSearchRepository;

    private final UserLogsRepository userLogsRepository;

    private final DesignationMasterRepository designationMasterRepository;

    public UserDetailsServiceImpl(UserDetailsRepository userDetailsRepository, UserDetailsMapper userDetailsMapper, UserDetailsSearchRepository userDetailsSearchRepository, OrganisationMasterMapper organisationMasterMapper, OrganisationMasterSearchRepository organisationMasterSearchRepository, UserService userService, UserRepository userRepository, UserSearchRepository userSearchRepository, PasswordEncoder passwordEncoder, UserApplicationService userApplicationService, UserApplicationSearchRepository userApplicationSearchRepository, UserApplicationRepository userApplicationRepository, ApplicationMasterRepository applicationMasterRepository, ApplicationMasterSearchRepository applicationMasterSearchRepository, ApplicationMasterMapper applicationMasterMapper, UserLogsService userLogsService, UserLogsSearchRepository userLogsSearchRepository, UserLogsRepository userLogsRepository, DesignationMasterRepository designationMasterRepository, MailService mailService) {
        this.userDetailsRepository = userDetailsRepository;
        this.userDetailsMapper = userDetailsMapper;
        this.userDetailsSearchRepository = userDetailsSearchRepository;
        this.organisationMasterMapper = organisationMasterMapper;
        this.organisationMasterSearchRepository = organisationMasterSearchRepository;
        this.userService = userService;
        this.userRepository = userRepository;
        this.userSearchRepository = userSearchRepository;
        this.passwordEncoder = passwordEncoder;
        this.userApplicationService = userApplicationService;
        this.userApplicationSearchRepository = userApplicationSearchRepository;
        this.userApplicationRepository = userApplicationRepository;

        this.applicationMasterRepository = applicationMasterRepository;
        this.applicationMasterSearchRepository = applicationMasterSearchRepository;
        this.applicationMasterMapper = applicationMasterMapper;

        this.userLogsService = userLogsService;
        this.userLogsSearchRepository = userLogsSearchRepository;
        this.userLogsRepository = userLogsRepository;
        this.designationMasterRepository=designationMasterRepository;
    }

    /**
     * Save a userDetails.
     *
     * @param userDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UserDetailsDTO save(UserDetailsDTO userDetailsDTO) {
        UserLogs userLogs = new UserLogs();
        log.debug("Request to save UserDetails : {}", userDetailsDTO);
        Optional<User> user = userService.getUserWithAuthorities();
        String login = user.get().getLogin();

        Optional<OrganisationMaster> organisation = organisationMasterSearchRepository.findOrganisationMasterByOrganisationIdCode(login);
        String orgnisationAbbreviation = organisation.get().getOrganisationAbbreviation();

        if(userDetailsDTO.getId() == null){

                User userNew = new User();
                Long loginId = organisation.get().getId();
                userDetailsDTO.setOrganisationMasterId(loginId);
                userDetailsDTO.setCreatedBy(login);

            Long totalCount = new Long(0);

            List<UserDetails> list = userDetailsRepository.findAll();

            if (!list.isEmpty()) {
                UserDetails lastRecord = list.get(list.size() - 1);
                Long lastId = lastRecord.getId();
                totalCount = lastId;
                totalCount = totalCount + 1;
            }else {
                totalCount = new Long(1);
            }
                if(totalCount<=99999){
                    String formatedNumber = "USER" + String.format("%06d", totalCount);
                    userDetailsDTO.setUserIdCode(formatedNumber);
                } else {
                    userDetailsDTO.setUserIdCode("USER" + totalCount);
                }

                userDetailsDTO.setCreatedOn(LocalDate.now());
                userDetailsDTO.setStatus("Pending");

                userLogs.setActionTaken("Created");
                userLogs.setActionTakenBy(login);
                userLogs.setActionTakenOnDate(LocalDate.now());
                userLogs.setRemarks("User Created");
                validateInput(userDetailsDTO);

            UserDetails userDetails = userDetailsMapper.toEntity(userDetailsDTO);
            UserDetailsDTO result = userDetailsMapper.toDto(userDetails);


            userDetails = userDetailsRepository.save(userDetails);

            if (userDetails.getId()!=null){
                userDetailsSearchRepository.save(userDetails);
                userLogs.setActionTakenOnUser(userDetails.getId());

                userLogsRepository.save(userLogs);
                userLogsSearchRepository.save(userLogs);

//                String userLogin = orgnisationAbbreviation + "_" + userDetialId;

                userNew.setFirstName(userDetails.getUserFullName());
                userNew.setEmail(userDetails.getUserOfficialEmail());
                userNew.setLangKey(Constants.DEFAULT_LANGUAGE);

                Authority authority = new Authority();
                authority.setName(AuthoritiesConstants.ORG_USER);

                Set<Authority> authorities = userNew.getAuthorities();
                authorities.add(authority);

                userNew.setAuthorities(authorities);
                userNew.setLogin(userDetails.getUserIdCode());
                userNew.setActivated(true);
                String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
                userNew.setPassword(encryptedPassword);
                userNew.setResetKey(RandomUtil.generateResetKey());
                userNew.setResetDate(Instant.now());

                userRepository.save(userNew);
                userSearchRepository.save(userNew);
            }
            return result;
        }
        else {
            String status = "Pending";
            userDetailsDTO.setLastUpdatedBy(SecurityUtils.getCurrentUserLogin().get());
            userDetailsDTO.setLastUpdatedOn(LocalDate.now());
            userLogs.setActionTakenOnUser(userDetailsDTO.getId());
            if (userDetailsDTO.getStatus().equals(status)){
                userLogs.setActionTaken("Updated");
            } else {
                userLogs.setActionTaken(userDetailsDTO.getStatus());
            }
            if (StringUtils.isEmpty(userDetailsDTO.getRemarks())){
                userLogs.setRemarks("User Updated");
            } else {
                userLogs.setRemarks(userDetailsDTO.getRemarks());
            }
            userLogs.setActionTakenBy(login);
            userLogs.setActionTakenOnDate(LocalDate.now());
            userLogs = userLogsRepository.save(userLogs);
            userLogsSearchRepository.save(userLogs);
            userDetailsRepository.save(userDetailsMapper.toEntity(userDetailsDTO));
            userDetailsSearchRepository.save(userDetailsMapper.toEntity(userDetailsDTO));


                    String userLogin = orgnisationAbbreviation + "_" + userDetailsDTO.getId();
                    UserApplication userApplication = new UserApplication();
                    userApplication.setUserId(userDetailsDTO.getId());
                    userApplication.setUserCodeId(userDetailsDTO.getUserIdCode());



            return userDetailsMapper.toDto(userDetailsSearchRepository.save(userDetailsMapper.toEntity(userDetailsDTO)));


        }

    }

    private void validateInput(UserDetailsDTO userDetailsDTO) {

        Optional<UserDetails> findUserOfficialEmail = userDetailsRepository.findByUserOfficialEmail(userDetailsDTO.getUserOfficialEmail());
        if (findUserOfficialEmail.isPresent()){
            throw new BadRequestAlertException("Email Allready Exists!", "userDetails", "invalidEmail");
        }

        Optional<UserDetails> findUserPhoneMobile = userDetailsRepository.findByUserPhoneMobile(userDetailsDTO.getUserPhoneMobile());
        if (findUserPhoneMobile.isPresent()){
            throw new BadRequestAlertException("Phone Number Allready Exists!", "userDetails", "invalidUserPhoneMobile");
        }

        Optional<UserDetails> findUserPan = userDetailsRepository.findByPan(userDetailsDTO.getPan());
        if (findUserPan.isPresent()){
            throw new BadRequestAlertException("PAN Number Allready Exists!", "userDetails", "invalidUserPanNumber");
        }

        Optional<UserDetails> findUserOfficialEmailSearch = userDetailsSearchRepository.findByUserOfficialEmail(userDetailsDTO.getUserOfficialEmail());
        if (findUserOfficialEmailSearch.isPresent()){
            throw new BadRequestAlertException("Email Allready Exists!", "userDetails", "invalidEmail");
        }

        Optional<UserDetails> findUserPhoneMobileSearch = userDetailsSearchRepository.findByUserPhoneMobile(userDetailsDTO.getUserPhoneMobile());
        if (findUserPhoneMobileSearch.isPresent()){
            throw new BadRequestAlertException("Phone Number Allready Exists!", "userDetails", "invalidUserPhoneMobile");
        }

        Optional<UserDetails> findUserPanSearch = userDetailsSearchRepository.findByPan(userDetailsDTO.getPan());
        if (findUserPanSearch.isPresent()){
            throw new BadRequestAlertException("PAN Number Allready Exists!", "userDetails", "invalidUserPanNumber");
        }


        String pancard = userDetailsDTO.getPanAttachment();

        if (!pancard.isEmpty()){
            String[] pancardLength = pancard.split("\\.");

            if (pancardLength.length>2){
                throw new BadRequestAlertException("Invalid Pan Attachment file format !", "userDetails", "invalidPanAttachment");
            }

            String panType = pancardLength[1];

            if (panType.equals("jpg") || panType.equals("JPG") || panType.equals("PDF") || panType.equals("pdf")){
            }else {
                throw new BadRequestAlertException("Invalid Pan Attachment file format !", "userDetails", "invalidPanAttachment");
            }
        }


        String EmpolyeeCardAttchment = userDetailsDTO.getEmployeeCardAttachment();

        if (!EmpolyeeCardAttchment.isEmpty()){

            String[] EmpolyeeCardAttchmentLength = EmpolyeeCardAttchment.split("\\.");

            if (EmpolyeeCardAttchmentLength.length>2){
                throw new BadRequestAlertException("Invalid Empolyee Card Attachment format !", "userDetails", "invalidEmpolyeeCardAttchment");
            }

            String EmpolyeeCardAttchmentType = EmpolyeeCardAttchmentLength[1];

            if (EmpolyeeCardAttchmentType.equals("jpg") || EmpolyeeCardAttchmentType.equals("JPG") || EmpolyeeCardAttchmentType.equals("PDF") || EmpolyeeCardAttchmentType.equals("pdf")){
            }else {
                throw new BadRequestAlertException("Invalid Empolyee Card Attachment format !", "userDetails", "invalidEmpolyeeCardAttchment");
            }

        }


        String PhotographAttachment = userDetailsDTO.getPhotograph();

        if (!PhotographAttachment.isEmpty()){

            String[] PhotographAttachmentLength = PhotographAttachment.split("\\.");

            if (PhotographAttachmentLength.length>2){
                throw new BadRequestAlertException("Invalid Photograph Attachment format !", "userDetails", "invalidPhotographAttachment");
            }

            String PhotographAttachmentType = PhotographAttachmentLength[1];

            if (PhotographAttachmentType.equals("jpg") || PhotographAttachmentType.equals("JPG")){
            }else {
                throw new BadRequestAlertException("Invalid Photograph Attachment format !", "userDetails", "invalidPhotographAttachment");
            }

        }
    }

    /**
     * Get all the userDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserDetailsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserDetails");
        return userDetailsRepository.findAll(pageable)
            .map(userDetailsMapper::toDto);
    }


    /**
     * Get one userDetails by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserDetailsDTO> findOne(Long id) {
        log.debug("Request to get UserDetails : {}", id);
        return userDetailsRepository.findById(id)
            .map(userDetailsMapper::toDto);
    }

    /**
     * Delete the userDetails by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserDetails : {}", id);
        userDetailsRepository.deleteById(id);
        userDetailsSearchRepository.deleteById(id);
    }

    /**
     * Search for the userDetails corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserDetailsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of UserDetails for query {}", query);
        return userDetailsSearchRepository.search(queryStringQuery(query), pageable)
            .map(userDetailsMapper::toDto);
    }

    public List<UserDetailsDTO> findUserByUserLogin(String userLogin) {
        List<UserDetailsDTO> userDetailsDTOList = userDetailsSearchRepository.findByCreatedBy(userLogin).stream().map(userDetailsMapper::toDto).collect(Collectors.toList());
        if(userDetailsDTOList.size() > 0) {
            for (UserDetailsDTO userDetailsDTO: userDetailsDTOList) {
                if(userDetailsDTO.getId() != null) {
                    if(userDetailsDTO.getDesignationMasterId() != null) {
                        Optional<DesignationMaster> designationMaster = designationMasterRepository.findById(userDetailsDTO.getDesignationMasterId());
                        designationMaster.ifPresent(designationMaster1 -> userDetailsDTO.setDesignationName(designationMaster1.getDesignationName()));
                    }
                }
            }
        }

        return userDetailsDTOList;
    }

    @Override
    public List<UserDetailsDTO> findUserByUserCodeId(String userCodeId) {
        return userDetailsSearchRepository.findAllByUserIdCode(userCodeId).stream().map(userDetailsMapper::toDto).collect(Collectors.toList());
    }

    public List<UserDetailsDTO> findVerifiedUser(String status) {
        return userDetailsSearchRepository.findByStatus(status).stream().map(userDetailsMapper::toDto).collect(Collectors.toList());
    }
    public Optional<UserDetailsDTO> findUserDetailsByUserCodeId(String userCodeId) {
        return userDetailsSearchRepository.findByUserIdCode(userCodeId).map(userDetailsMapper::toDto);
    }

    @Override
    public Optional<UserDetailsDTO> findOneById(Long id) {
        log.debug("Request to get UserDetails : {}", id);
        Optional<UserDetailsDTO> userDetailsDTO = userDetailsRepository.findById(id)
            .map(userDetailsMapper::toDto);
        String login = userDetailsDTO.get().getCreatedBy();
        String currentUser = SecurityUtils.getCurrentUserLogin().get();
        String status = userDetailsDTO.get().getStatus();
        Boolean currentRole = SecurityUtils.isCurrentUserInRole("ROLE_ORG_NODAL");
        Optional<UserDetailsDTO> userDetailsDTO1 = Optional.of(new UserDetailsDTO());
        if (currentRole) {
            if(login.equalsIgnoreCase(currentUser)) {
                userDetailsDTO1 = userDetailsDTO;
            }
        } else {
            if(status.equalsIgnoreCase("Verified")) {
                userDetailsDTO1 = userDetailsDTO;
            }
        }

        return userDetailsDTO1;
    }

}
