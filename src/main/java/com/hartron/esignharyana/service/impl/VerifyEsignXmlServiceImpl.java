package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.service.VerifyEsignXmlService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStoreException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

@Service
@Transactional
public class VerifyEsignXmlServiceImpl implements VerifyEsignXmlService {

    private final Logger log = LoggerFactory.getLogger(VerifyEsignXmlServiceImpl.class);


    @Override
    public boolean verify(String xmlSignOutput)
    {

        boolean verificationResult = false;

        try
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            Document signedDocument = dbf.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(xmlSignOutput), "UTF-8"));

            NodeList nl = signedDocument.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");

            if (nl.getLength() == 0)
            {
                throw new IllegalArgumentException("Cannot find Signature element");
            }

            XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

            DOMValidateContext valContext = new DOMValidateContext(GetPublicKey(), nl.item(0));
            XMLSignature signature = fac.unmarshalXMLSignature(valContext);

            verificationResult = signature.validate(valContext);

        }
        catch (Exception e)
        {
            System.out.println("Error while verifying digital siganature" + e.getMessage());
            e.printStackTrace();
        }

        return verificationResult;
    }

    public static PublicKey GetPublicKey() throws FileNotFoundException, KeyStoreException, CertificateException {



        PublicKey publicKey = null;
        FileInputStream is = null;

        try
        {
            is = new FileInputStream("/var/DSCPublicKey.cer");
//            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
//            keystore.load(is, "12345678".toCharArray());
//            String alias = "Alias";
//            Key key = keystore.getKey(alias, "password".toCharArray());
//            if (key instanceof PrivateKey)
//            {

//                 Get certificate of public key
//                java.security.Certificate cert = (java.security.Certificate) keystore.getCertificate(alias);
            CertificateFactory fact = CertificateFactory.getInstance("X.509");
            X509Certificate cert =  (X509Certificate) fact.generateCertificate(is);

            // Get public key
            publicKey = cert.getPublicKey();

//            }
        }
        catch (FileNotFoundException ex)
        {
        }
        catch ( IOException ex)
        {
        }
        finally
        {
            try
            {
                is.close();
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }


        return publicKey;

    }
}
