package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.config.MinioConfig;
import com.hartron.esignharyana.domain.EsignTransation;
import com.hartron.esignharyana.domain.UserDetails;
import com.hartron.esignharyana.repository.EsignTransationRepository;
import com.hartron.esignharyana.repository.UserDetailsRepository;
import com.hartron.esignharyana.repository.search.EsignTransationSearchRepository;
import com.hartron.esignharyana.repository.search.UserDetailsSearchRepository;
import com.hartron.esignharyana.security.SecurityUtils;
import com.hartron.esignharyana.service.EsignTransationService;
import com.hartron.esignharyana.service.MinioFileService;
import com.hartron.esignharyana.service.SignerService;
import com.hartron.esignharyana.service.UserDetailsService;
import com.hartron.esignharyana.service.dto.UserDetailsDTO;
import com.hartron.esignharyana.service.mapper.UserDetailsMapper;
import io.minio.MinioClient;
import io.minio.errors.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParserException;

import javax.xml.bind.DatatypeConverter;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.keyinfo.X509IssuerSerial;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Service Implementation for managing {@link UserDetails}.
 */
@Service
@Transactional
public class SignerServiceImpl implements SignerService {

    private final Logger log = LoggerFactory.getLogger(SignerServiceImpl.class);



    private final UserDetailsMapper userDetailsMapper;

    private final UserDetailsSearchRepository userDetailsSearchRepository;

    private static final String POST_URL_SIGNER = "http://dsp.csccloud.in/esignauth/api/signer";

    private static final String POST_URL_SIGN_WITH_HTTP = "http://dsp.csccloud.in/esignauth/sign";

    private static final String POST_URL_SIGN_WITH_HTTPS = "https://dsp.csccloud.in/esignauth/sign";

    private final MinioFileService minioFileService;

    private final MinioConfig minioConfig;

    private final EsignTransationService esignTransationService;

    private final EsignTransationRepository esignTransationRepository;

    private final EsignTransationSearchRepository esignTransationSearchRepository;

    private final UserDetailsService userDetailsService;

    private final UserDetailsRepository userDetailsRepository;

    private String certSerialNumber = "18359477";

    public SignerServiceImpl(UserDetailsMapper userDetailsMapper, UserDetailsSearchRepository userDetailsSearchRepository, MinioFileService minioFileService, MinioConfig minioConfig, EsignTransationService esignTransationService, EsignTransationRepository esignTransationRepository, EsignTransationSearchRepository esignTransationSearchRepository, UserDetailsRepository userDetailsRepository, UserDetailsService userDetailsService, UserDetailsRepository userDetailsRepository1) {

        this.userDetailsMapper = userDetailsMapper;
        this.userDetailsSearchRepository = userDetailsSearchRepository;
        this.minioFileService = minioFileService;
        this.minioConfig = minioConfig;
        this.esignTransationService = esignTransationService;
        this.esignTransationRepository = esignTransationRepository;
        this.esignTransationSearchRepository = esignTransationSearchRepository;
        this.userDetailsService = userDetailsService;
        this.userDetailsRepository = userDetailsRepository1;
    }

    public String verifyUser(String userCodeId, String lastAction) throws ParserConfigurationException, TransformerException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, XMLSignatureException, MarshalException, IOException, KeyStoreException, SAXException, UnrecoverableEntryException, CertificateException {

        String OrgKYCXML =  "";

            OrgKYCXML =  createOrgKycXML(userCodeId);
            String signedOrgKycXML = signXML(OrgKYCXML);
            String signerXML = createSignerXML(signedOrgKycXML);
            String signerXMLSignatured = signXML(signerXML);
            System.out.println("444444444444444444444444" + signerXMLSignatured);



        String SignerResponse = sendOrgKYCRequest(signerXMLSignatured);

        DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
       // dbf.setNamespaceAware(true);
        Document doc1 =
            dbf1.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(SignerResponse), "UTF-8"));
        doc1.setXmlStandalone(true);
        Element element1 = (Element) doc1.getElementsByTagName("SignerRes").item(0);
        String responseInfo =    element1.getAttribute("info");
      //  Element element = (Element) doc1.getAttributes("SignerRe");

        if (responseInfo.equals("Success")) {

            Optional<UserDetailsDTO> userDetails = userDetailsService.findUserDetailsByUserCodeId(userCodeId);
            if(userDetails.isPresent()) {
                UserDetailsDTO userDetailsDTO = userDetails.get();
                userDetailsDTO.setStatus("Verified");
                userDetailsDTO.setVerifiedBy(SecurityUtils.getCurrentUserLogin().get());
                userDetailsDTO.setVerifiedOn(LocalDate.now());
                userDetailsDTO.setLastAction("Create");
                userDetailsService.save(userDetailsDTO);
            }

        }
        if(!responseInfo.isEmpty()) {

            saveSignerResponse(userCodeId, lastAction, SignerResponse);
        }

        String saveTransaction = saveSignerRequest(userCodeId, lastAction, signerXMLSignatured);

        System.out.println("ResponseInfoooooooooooooooooooooooooooo" + responseInfo);

        return responseInfo;
    }

    public String accountSigner(String userCodeId, String lastAction, String signerId, String accountStatus) throws ParserConfigurationException, TransformerException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, XMLSignatureException, MarshalException, IOException, KeyStoreException, SAXException, UnrecoverableEntryException, CertificateException {

        String OrgKYCXML =  "";

        OrgKYCXML =  createOrgKycXML(userCodeId);
        String signedOrgKycXML = signXML(OrgKYCXML);
        String signerXML = accountSignerXML(signedOrgKycXML, signerId, accountStatus);
        String signerXMLSignatured = signXML(signerXML);
        System.out.println("444444444444444444444444" + signerXMLSignatured);

        String SignerResponse = sendOrgKYCRequest(signerXMLSignatured);

        DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
        // dbf.setNamespaceAware(true);
        Document doc1 =
            dbf1.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(SignerResponse), "UTF-8"));
        doc1.setXmlStandalone(true);
        Element element1 = (Element) doc1.getElementsByTagName("SignerRes").item(0);
        String responseInfo =    element1.getAttribute("info");
        //  Element element = (Element) doc1.getAttributes("SignerRe");

        if (responseInfo.equals("Success")) {

            Optional<UserDetailsDTO> userDetails = userDetailsService.findUserDetailsByUserCodeId(userCodeId);
            if(userDetails.isPresent()) {
                UserDetailsDTO userDetailsDTO = userDetails.get();
                userDetailsDTO.setStatus("Verified");
                userDetailsDTO.setVerifiedBy(SecurityUtils.getCurrentUserLogin().get());
                userDetailsDTO.setVerifiedOn(LocalDate.now());
                userDetailsDTO.setLastAction(lastAction);
                userDetailsDTO.setAccountStatus(accountStatus);
                userDetailsService.save(userDetailsDTO);
            }

        }
        if(!responseInfo.isEmpty()) {

            saveSignerResponse(userCodeId, lastAction, SignerResponse);
        }

        String saveTransaction = saveSignerRequest(userCodeId, lastAction, signerXMLSignatured);

        System.out.println("ResponseInfoooooooooooooooooooooooooooo" + responseInfo);

        return responseInfo;
    }

    public String updateSigner(String userCodeId, String lastAction, String signerId) throws ParserConfigurationException, TransformerException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, XMLSignatureException, MarshalException, IOException, KeyStoreException, SAXException, UnrecoverableEntryException, CertificateException {

        String OrgKYCXML =  "";

        String signerXMLSignatured = "";
        if (lastAction.equalsIgnoreCase("Update")) {
            OrgKYCXML =  createOrgKycXML(userCodeId);
            String signedOrgKycXML = signXML(OrgKYCXML);
            String signerXML = updateSignerXML(signedOrgKycXML, signerId);
            signerXMLSignatured = signXML(signerXML);
            System.out.println("444444444444444444444444" + signerXMLSignatured);
        } else if (lastAction.equalsIgnoreCase("Status")) {
            OrgKYCXML =  createOrgKycXML(userCodeId);
            String signedOrgKycXML = signXML(OrgKYCXML);
            String signerXML = statusSignerXML(signedOrgKycXML, signerId);
            signerXMLSignatured = signXML(signerXML);
            System.out.println("555555555555555555555" + signerXMLSignatured);
        }

        String SignerResponse = sendOrgKYCRequest(signerXMLSignatured);

        DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
        // dbf.setNamespaceAware(true);
        Document doc1 =
            dbf1.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(SignerResponse), "UTF-8"));
        doc1.setXmlStandalone(true);
        Element element1 = (Element) doc1.getElementsByTagName("SignerRes").item(0);
        String responseInfo =    element1.getAttribute("info");
        //  Element element = (Element) doc1.getAttributes("SignerRe");

        if (responseInfo.equals("Success")) {

            Optional<UserDetailsDTO> userDetails = userDetailsService.findUserDetailsByUserCodeId(userCodeId);
            if(userDetails.isPresent()) {
                UserDetailsDTO userDetailsDTO = userDetails.get();
                userDetailsDTO.setStatus("Updated");
                userDetailsDTO.setVerifiedBy(SecurityUtils.getCurrentUserLogin().get());
                userDetailsDTO.setVerifiedOn(LocalDate.now());
                userDetailsDTO.setLastAction(lastAction);
                userDetailsService.save(userDetailsDTO);
            }

        }
        if(!responseInfo.isEmpty()) {

            saveSignerResponse(userCodeId, lastAction, SignerResponse);
        }

        String saveTransaction = saveSignerRequest(userCodeId, lastAction, signerXMLSignatured);

        System.out.println("ResponseInfoooooooooooooooooooooooooooo" + responseInfo);

        return responseInfo;
    }

    public String saveSignerRequest(String userCodeId, String lastAction, String signerXMLSignatured) throws ParserConfigurationException, IOException, SAXException {

        DocumentBuilderFactory signerXML_Request = DocumentBuilderFactory.newInstance();
        // dbf.setNamespaceAware(true);
        Document doc_signerXML_Request =
            signerXML_Request.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(signerXMLSignatured), "UTF-8"));
        doc_signerXML_Request.setXmlStandalone(true);
        Element element = (Element) doc_signerXML_Request.getElementsByTagName("Signer").item(0);
        String txnId_request =    element.getAttribute("txn");
        String ts_request = element.getAttribute("ts");


        EsignTransation esignTransation = new EsignTransation();

        esignTransation.setUserCodeId(userCodeId);
        esignTransation.setTxnId(txnId_request);
        esignTransation.setRequestStatus(lastAction);
        esignTransation.setRequestType("Signer");
        esignTransation.setTs(ts_request);

        esignTransationRepository.save(esignTransation);
        esignTransationSearchRepository.save(esignTransation);

        return esignTransation.toString();
    }

    public String saveSignerResponse(String userCodeId, String lastAction, String SignerResponse) throws ParserConfigurationException, IOException, SAXException {

        DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
        // dbf.setNamespaceAware(true);
        Document doc1 =
            dbf1.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(SignerResponse), "UTF-8"));
        doc1.setXmlStandalone(true);
        Element element1 = (Element) doc1.getElementsByTagName("SignerRes").item(0);
        String responseInfo =    element1.getAttribute("info");
        String txnId_request = element1.getAttribute("txn");
        String resCode = element1.getAttribute("resCode");
        String ts_response = element1.getAttribute("ts");

        EsignTransation esignTransation = new EsignTransation();

        esignTransation.setUserCodeId(userCodeId);
        esignTransation.setTxnId(txnId_request);
        esignTransation.setResponseType("Signer");
        esignTransation.setResponseStatus(responseInfo);
        esignTransation.setResponseCode(resCode);
        esignTransation.setTs(ts_response);

        esignTransationRepository.save(esignTransation);
        esignTransationSearchRepository.save(esignTransation);

        return esignTransation.toString();
    }


    public String getFileFromMinio(String filename) {
        String url = null;
        try {
            MinioClient minioClient = minioConfig.getMinioClient();
            url = DatatypeConverter.printBase64Binary(IOUtils.toByteArray(minioClient.getObject("esignharyana",filename)));
        } catch (InvalidBucketNameException | NoSuchAlgorithmException | IOException | XmlPullParserException | InvalidKeyException | NoResponseException | io.minio.errors.InternalException | ErrorResponseException | InsufficientDataException | InvalidArgumentException e) {
        }
        return url;
    }
    
    public String createOrgKycXML(String userCodeId) throws ParserConfigurationException, TransformerException, IOException {

        Optional<UserDetailsDTO> userDetails = userDetailsSearchRepository.findByUserIdCode(userCodeId).map(userDetailsMapper::toDto);

        String orgKYCXML = "";

        if (userDetails.isPresent()){

            String imageName = userDetails.get().getPhotograph();
            String minioImage =  getFileFromMinio(imageName);

            Date now = new Date();
            String format3 = new SimpleDateFormat("yyMMddHHmmss", Locale.ENGLISH).format(now);
            String randomNo = RandomStringUtils.randomNumeric(6);
            String txnId = format3 + randomNo;


            //User XML Creation Work start from here
          //  byte[] fileContent = FileUtils.readFileToByteArray(new File("/var/jeet.jpg"));

          //  String encodedString = Base64.getEncoder().encodeToString(fileContent);


            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder;

            dBuilder = dbFactory.newDocumentBuilder();

            Document doc = dBuilder.newDocument();

            doc.setXmlStandalone(true);

            StringWriter writer = new StringWriter();

            Element rootElement = doc.createElement("OrgKYC");

            doc.appendChild(rootElement);

            Attr ver = doc.createAttribute("ver");
            ver.setValue("1.0");
            rootElement.setAttributeNode(ver);



            LocalDateTime myDateObj = LocalDateTime.now();
            DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
            String formattedDate = myDateObj.format(myFormatObj);


            Attr ts = doc.createAttribute("ts");
            ts.setValue(formattedDate);
            rootElement.setAttributeNode(ts);

            Attr txn = doc.createAttribute("txn");
            txn.setValue(txnId);
            rootElement.setAttributeNode(txn);

            Attr kycOrgId = doc.createAttribute("KycOrgId");
            kycOrgId.setValue("CSORG1000002");
            rootElement.setAttributeNode(kycOrgId);

            Attr orgName = doc.createAttribute("orgName");
            orgName.setValue("HARYANA STATE ELECTRONIC DEV CORPN LTD HARTRON");
            rootElement.setAttributeNode(orgName);

            Attr orgSigName = doc.createAttribute("orgSigName");
            orgSigName.setValue("Rajendar Singh");
            rootElement.setAttributeNode(orgSigName);

            Attr orgSigCertId = doc.createAttribute("orgSigCertId");
            orgSigCertId.setValue(certSerialNumber);
            rootElement.setAttributeNode(orgSigCertId);

            Element KYCinfoElement = doc.createElement("KYCInfo");
            rootElement.appendChild(KYCinfoElement);

            Attr employeeId = doc.createAttribute("employeeId");
            employeeId.setValue(userDetails.get().getEmployeeId());
            KYCinfoElement.setAttributeNode(employeeId);

            Attr Name = doc.createAttribute("name");
            Name.setValue(userDetails.get().getUserFullName());
            KYCinfoElement.setAttributeNode(Name);

            Attr UserMobile = doc.createAttribute("mobile");
            UserMobile.setValue(userDetails.get().getUserPhoneMobile());
            KYCinfoElement.setAttributeNode(UserMobile);

            Attr email = doc.createAttribute("email");
            email.setValue(userDetails.get().getUserOfficialEmail());
            KYCinfoElement.setAttributeNode(email);

            Attr dateOfBirth = doc.createAttribute("dateOfBirth");
            dateOfBirth.setValue(userDetails.get().getDob().toString());
            KYCinfoElement.setAttributeNode(dateOfBirth);


            Attr gender = doc.createAttribute("gender");
            gender.setValue(userDetails.get().getGender());
            KYCinfoElement.setAttributeNode(gender);

            Attr pan = doc.createAttribute("pan");
            pan.setValue(userDetails.get().getPan());
            KYCinfoElement.setAttributeNode(pan);

            Attr Aadhaar = doc.createAttribute("Aadhaar");
            Aadhaar.setValue(userDetails.get().getAadhaar());
            KYCinfoElement.setAttributeNode(Aadhaar);


            Attr address = doc.createAttribute("address");
            address.setValue("Panchkula");
            KYCinfoElement.setAttributeNode(address);

            Attr postalCode = doc.createAttribute("postalCode");
            postalCode.setValue("125001");
            KYCinfoElement.setAttributeNode(postalCode);

            Attr stateProvince = doc.createAttribute("stateProvince");
            stateProvince.setValue("Haryana");
            KYCinfoElement.setAttributeNode(stateProvince);

            Attr country = doc.createAttribute("country");
            country.setValue("India");
            KYCinfoElement.setAttributeNode(country);

            Element photo = doc.createElement("Photo");
            photo.appendChild(doc.createTextNode(minioImage));

            Attr format = doc.createAttribute("format");
            format.setValue("JPG");
            photo.setAttributeNode(format);



            rootElement.appendChild(photo);


            Element physicalVerification = doc.createElement("physicalVerification");
            physicalVerification.appendChild(doc.createTextNode("I hereby certify the physical verfication of " +  userDetails.get().getUserFullName()));

            rootElement.appendChild(physicalVerification);

            String xmlString = doc.toString().replaceAll("\\<\\?xml(.+?)\\?\\>","").trim();

            // create the xml file
            //transform the DOM Object to an XML File
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            //StreamResult streamResult = new StreamResult(new File(xmlFilePath));

            // If you use
            StreamResult result = new StreamResult(writer);
            // the output will be pushed to the standard output ...
            // You can use that for debugging

            transformer.transform(domSource, result);

//           TransformerFactory transformerFactory = TransformerFactory.newInstance();

            orgKYCXML = writer.toString();
        }
        return   orgKYCXML;
    }
    public String signXML(String xmlDoc) throws ParserConfigurationException, TransformerException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IOException, UnrecoverableEntryException, KeyStoreException, MarshalException, XMLSignatureException, SAXException, CertificateException {
        // Create a DOM XMLSignatureFactory that will be used to generate the
        // enveloped signature
        XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

        // Create a Reference to the enveloped document (in this case we are
        // signing the whole document, so a URI of "" signifies that) and
        // also specify the SHA256 digest algorithm and the ENVELOPED Transform.
        Reference ref = fac.newReference
            ("", fac.newDigestMethod(DigestMethod.SHA256, null),
                Collections.singletonList
                    (fac.newTransform
                        (Transform.ENVELOPED, (TransformParameterSpec) null)),
                null, null);

        // Create the SignedInfo
        SignedInfo si = fac.newSignedInfo
            (fac.newCanonicalizationMethod
                    (CanonicalizationMethod.EXCLUSIVE,
                        (C14NMethodParameterSpec) null),
                fac.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", null),
                Collections.singletonList(ref));


        KeyStore p12 = KeyStore.getInstance("pkcs12");
        p12.load(new FileInputStream("/var/DocSigner.pfx"), "12345678".toCharArray());


        Enumeration e = p12.aliases();
        String alias = (String) e.nextElement();
        System.out.println("Alias certifikata:" + alias);
        Key privateKey = p12.getKey(alias, "12345678".toCharArray());



        KeyStore.PrivateKeyEntry keyEntry
            = (KeyStore.PrivateKeyEntry) p12.getEntry(alias, new KeyStore.PasswordProtection("12345678".toCharArray()));

        X509Certificate cert = (X509Certificate) keyEntry.getCertificate();

        KeyInfoFactory kif = fac.getKeyInfoFactory();

        X509IssuerSerial x509IssuerSerial = kif.newX509IssuerSerial(cert.getSubjectX500Principal().getName(), cert.getSerialNumber());

        List x509Content = new ArrayList();
        System.out.println("ime: " + cert.getSubjectX500Principal().getName());
        x509Content.add(cert.getSubjectX500Principal().getName());
        x509Content.add(x509IssuerSerial);

        X509Data xd = kif.newX509Data(x509Content);
        KeyInfo ki = kif.newKeyInfo(Collections.singletonList(xd));
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document doc =
            dbf.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(xmlDoc), "UTF-8"));
        DOMSignContext dsc = new DOMSignContext(privateKey, doc.getDocumentElement());
        XMLSignature signature = fac.newXMLSignature(si, ki);
        signature.sign(dsc);
        String xmlSignatureOutput = signature.toString();

        String signedXMLObject =  writeXmlDocumentToXmlFile(doc);
        DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document doc1 =
            dbf1.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(signedXMLObject), "UTF-8"));
        doc1.setXmlStandalone(true);
        Element element = (Element) doc1.getElementsByTagName("KeyInfo").item(0);
        element.getParentNode().removeChild(element);
        doc1.normalize();

        String signedXMLObjectWithoutKeyInfo = writeXmlDocumentToXmlFile(doc1);

        System.out.println("Signed XML Without Key Infoooooooooooo :" + signedXMLObjectWithoutKeyInfo);

        return signedXMLObjectWithoutKeyInfo;


        }

    private String  writeXmlDocumentToXmlFile(Document xmlDocument)
    {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        String signedXMLObject ="";
        try {
            transformer = tf.newTransformer();

            // Uncomment if you do not require XML declaration
            // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            //A character stream that collects its output in a string buffer,
            //which can then be used to construct a string.
            StringWriter writer = new StringWriter();

            //transform document to string
            transformer.transform(new DOMSource(xmlDocument), new StreamResult(writer));

            String xmlString = writer.getBuffer().toString();
            System.out.println(xmlString);
            signedXMLObject = xmlString;
            //Print to console or logs
        }
        catch (TransformerException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return signedXMLObject;
    }

    public String createSignerXML(String signedOrgKyc) throws  ParserConfigurationException, TransformerException, MarshalException, XMLSignatureException, IOException, SAXException, KeyStoreException, CertificateException, UnrecoverableEntryException {

        Date now = new Date();
        String format3 = new SimpleDateFormat("yyMMddHHmmss", Locale.ENGLISH).format(now);
        String randomNo = RandomStringUtils.randomNumeric(6);
        String txnId = format3 + randomNo;

        String encodedString = Base64.getEncoder().encodeToString(signedOrgKyc.getBytes());

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;

        dBuilder = dbFactory.newDocumentBuilder();

        Document doc = dBuilder.newDocument();

        StringWriter writer = new StringWriter();

        Element rootElement = doc.createElement("Signer");

        doc.appendChild(rootElement);

        Attr api = doc.createAttribute("api");
        api.setValue("create");
        rootElement.setAttributeNode(api);

        Attr orgType = doc.createAttribute("orgType");
        orgType.setValue("ORG");
        rootElement.setAttributeNode(orgType);

        Attr orgId = doc.createAttribute("orgId");
        orgId.setValue("CSORG1000002");
        rootElement.setAttributeNode(orgId);

        Attr eKycType = doc.createAttribute("eKycType");
        eKycType.setValue("ORG1");
        rootElement.setAttributeNode(eKycType);

        Attr ver = doc.createAttribute("ver");
        ver.setValue("3.2");
        rootElement.setAttributeNode(ver);

        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String formattedDate = myDateObj.format(myFormatObj);

        Attr ts = doc.createAttribute("ts");

        ts.setValue(formattedDate);
        rootElement.setAttributeNode(ts);

        Attr txn = doc.createAttribute("txn");
        txn.setValue(txnId);
        rootElement.setAttributeNode(txn);


        Attr aspId = doc.createAttribute("aspId");
        aspId.setValue("CSORG1000002");
        rootElement.setAttributeNode(aspId);

        Attr kycData = doc.createAttribute("kycData");
        kycData.setValue("Y");
        rootElement.setAttributeNode(kycData);
        rootElement.appendChild(doc.createTextNode(encodedString));


        // create the xml file
        //transform the DOM Object to an XML File
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(doc);
        //StreamResult streamResult = new StreamResult(new File(xmlFilePath));

        // If you use
        StreamResult result = new StreamResult(writer);
        // the output will be pushed to the standard output ...
        // You can use that for debugging

        transformer.transform(domSource, result);


        return writer.toString();


        }

    public String updateSignerXML(String signedOrgKyc, String signerIdval) throws  ParserConfigurationException, TransformerException, MarshalException, XMLSignatureException, IOException, SAXException, KeyStoreException, CertificateException, UnrecoverableEntryException {



//        log.debug("REST request to save ApplicationMaster : {}");

        Date now = new Date();
        String format3 = new SimpleDateFormat("yyMMddHHmmss", Locale.ENGLISH).format(now);
        String randomNo = RandomStringUtils.randomNumeric(6);
        String txnId = format3 + randomNo;


        String encodedString = Base64.getEncoder().encodeToString(signedOrgKyc.getBytes());

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;

        dBuilder = dbFactory.newDocumentBuilder();

        Document doc = dBuilder.newDocument();
        doc.setXmlStandalone(true);
        StringWriter writer = new StringWriter();

        Element rootElement = doc.createElement("Signer");

        doc.appendChild(rootElement);

        Attr api = doc.createAttribute("api");
        api.setValue("update");
        rootElement.setAttributeNode(api);

        Attr orgType = doc.createAttribute("orgType");
        orgType.setValue("ORG");
        rootElement.setAttributeNode(orgType);

        Attr orgId = doc.createAttribute("orgId");
        orgId.setValue("CSORG1000002");
        rootElement.setAttributeNode(orgId);

        Attr eKycType = doc.createAttribute("eKycType");
        eKycType.setValue("ORG1");
        rootElement.setAttributeNode(eKycType);

        Attr ver = doc.createAttribute("ver");
        ver.setValue("3.2");
        rootElement.setAttributeNode(ver);

        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String formattedDate = myDateObj.format(myFormatObj);

        Attr ts = doc.createAttribute("ts");

        ts.setValue(formattedDate);
        rootElement.setAttributeNode(ts);

        Attr txn = doc.createAttribute("txn");
        txn.setValue(txnId);
        rootElement.setAttributeNode(txn);


        Attr aspId = doc.createAttribute("aspId");
        aspId.setValue("CSORG1000002");
        rootElement.setAttributeNode(aspId);

        Attr signerId = doc.createAttribute("signerId");
        signerId.setValue(signerIdval);
        rootElement.setAttributeNode(signerId);

        Attr kycData = doc.createAttribute("kycData");
        kycData.setValue("Y");
        rootElement.setAttributeNode(kycData);
        rootElement.appendChild(doc.createTextNode(encodedString));


        // create the xml file
        //transform the DOM Object to an XML File
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(doc);
        //StreamResult streamResult = new StreamResult(new File(xmlFilePath));

        // If you use
        StreamResult result = new StreamResult(writer);
        // the output will be pushed to the standard output ...
        // You can use that for debugging

        transformer.transform(domSource, result);


        return writer.toString();


    }

    public String statusSignerXML(String signedOrgKyc, String signerId) throws  ParserConfigurationException, TransformerException, MarshalException, XMLSignatureException, IOException, SAXException, KeyStoreException, CertificateException, UnrecoverableEntryException {



//        log.debug("REST request to save ApplicationMaster : {}");

        Date now = new Date();
        String format3 = new SimpleDateFormat("yyMMddHHmmss", Locale.ENGLISH).format(now);
        String randomNo = RandomStringUtils.randomNumeric(6);
        String txnId = format3 + randomNo;



        String encodedString = Base64.getEncoder().encodeToString(signedOrgKyc.getBytes());

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;

        dBuilder = dbFactory.newDocumentBuilder();

        Document doc = dBuilder.newDocument();
        doc.setXmlStandalone(true);
        StringWriter writer = new StringWriter();

        Element rootElement = doc.createElement("Signer");

        doc.appendChild(rootElement);

        Attr api = doc.createAttribute("api");
        api.setValue("status");
        rootElement.setAttributeNode(api);

        Attr orgType = doc.createAttribute("orgType");
        orgType.setValue("ORG");
        rootElement.setAttributeNode(orgType);

        Attr orgId = doc.createAttribute("orgId");
        orgId.setValue("CSORG1000002");
        rootElement.setAttributeNode(orgId);

        Attr eKycType = doc.createAttribute("eKycType");
        eKycType.setValue("ORG1");
        rootElement.setAttributeNode(eKycType);

        Attr ver = doc.createAttribute("ver");
        ver.setValue("3.2");
        rootElement.setAttributeNode(ver);

        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String formattedDate = myDateObj.format(myFormatObj);

        Attr ts = doc.createAttribute("ts");

        ts.setValue(formattedDate);
        rootElement.setAttributeNode(ts);

        Attr txn = doc.createAttribute("txn");
        txn.setValue(txnId);
        rootElement.setAttributeNode(txn);


        Attr aspId = doc.createAttribute("aspId");
        aspId.setValue("CSORG1000002");
        rootElement.setAttributeNode(aspId);

        Attr signerIdAttr = doc.createAttribute("signerId");
        signerIdAttr.setValue(signerId);
        rootElement.setAttributeNode(signerIdAttr);

        Attr kycData = doc.createAttribute("kycData");
        kycData.setValue("N");
        rootElement.setAttributeNode(kycData);
//        rootElement.appendChild(doc.createTextNode(encodedString));


        // create the xml file
        //transform the DOM Object to an XML File
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(doc);
        //StreamResult streamResult = new StreamResult(new File(xmlFilePath));

        // If you use
        StreamResult result = new StreamResult(writer);
        // the output will be pushed to the standard output ...
        // You can use that for debugging

        transformer.transform(domSource, result);


        return writer.toString();


    }

    public String accountSignerXML(String signedOrgKyc, String signerIdval, String accountStatus) throws  ParserConfigurationException, TransformerException, MarshalException, XMLSignatureException, IOException, SAXException, KeyStoreException, CertificateException, UnrecoverableEntryException {



//        log.debug("REST request to save ApplicationMaster : {}");

        Date now = new Date();
        String format3 = new SimpleDateFormat("yyMMddHHmmss", Locale.ENGLISH).format(now);
        String randomNo = RandomStringUtils.randomNumeric(6);
        String txnId = format3 + randomNo;


        String encodedString = Base64.getEncoder().encodeToString(signedOrgKyc.getBytes());

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;

        dBuilder = dbFactory.newDocumentBuilder();

        Document doc = dBuilder.newDocument();
        doc.setXmlStandalone(true);
        StringWriter writer = new StringWriter();

        Element rootElement = doc.createElement("Signer");

        doc.appendChild(rootElement);

        Attr api = doc.createAttribute("api");
        api.setValue("account");
        rootElement.setAttributeNode(api);

        Attr orgType = doc.createAttribute("orgType");
        orgType.setValue("ORG");
        rootElement.setAttributeNode(orgType);

        Attr orgId = doc.createAttribute("orgId");
        orgId.setValue("CSORG1000002");
        rootElement.setAttributeNode(orgId);

        Attr eKycType = doc.createAttribute("eKycType");
        eKycType.setValue("ORG1");
        rootElement.setAttributeNode(eKycType);

        Attr ver = doc.createAttribute("ver");
        ver.setValue("3.2");
        rootElement.setAttributeNode(ver);

        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String formattedDate = myDateObj.format(myFormatObj);

        Attr ts = doc.createAttribute("ts");

        ts.setValue(formattedDate);
        rootElement.setAttributeNode(ts);

        Attr txn = doc.createAttribute("txn");
        txn.setValue(txnId);
        rootElement.setAttributeNode(txn);


        Attr aspId = doc.createAttribute("aspId");
        aspId.setValue("CSORG1000002");
        rootElement.setAttributeNode(aspId);

        Attr signerId = doc.createAttribute("signerId");
        signerId.setValue(signerIdval);
        rootElement.setAttributeNode(signerId);

        Attr action = doc.createAttribute("action");
        action.setValue(accountStatus);
        rootElement.setAttributeNode(action);

        Attr kycData = doc.createAttribute("kycData");
        kycData.setValue("N");
        rootElement.setAttributeNode(kycData);
//        rootElement.appendChild(doc.createTextNode(encodedString));


        // create the xml file
        //transform the DOM Object to an XML File
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(doc);
        //StreamResult streamResult = new StreamResult(new File(xmlFilePath));

        // If you use
        StreamResult result = new StreamResult(writer);
        // the output will be pushed to the standard output ...
        // You can use that for debugging

        transformer.transform(domSource, result);


        return writer.toString();


    }

    public boolean verify(String signedOrgKycXML)
    {

        boolean verificationResult = false;

        try
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            Document signedDocument = dbf.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(signedOrgKycXML), "UTF-8"));

            NodeList nl = signedDocument.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");

            if (nl.getLength() == 0)
            {
                throw new IllegalArgumentException("Cannot find Signature element");
            }

            XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

            DOMValidateContext valContext = new DOMValidateContext(GetPublicKey(), nl.item(0));
            XMLSignature signature = fac.unmarshalXMLSignature(valContext);

            verificationResult = signature.validate(valContext);

        }
        catch (Exception e)
        {
            System.out.println("Error while verifying digital siganature" + e.getMessage());
            e.printStackTrace();
        }

        return verificationResult;
    }

    public static PublicKey GetPublicKey() throws FileNotFoundException, KeyStoreException, CertificateException {

        PublicKey publicKey = null;
        FileInputStream is = null;

        try
        {
            is = new FileInputStream("/var/DSCPublicKey.cer");
//            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
//            keystore.load(is, "12345678".toCharArray());
//            String alias = "Alias";
//            Key key = keystore.getKey(alias, "password".toCharArray());
//            if (key instanceof PrivateKey)
//            {

//                 Get certificate of public key
//                java.security.Certificate cert = (java.security.Certificate) keystore.getCertificate(alias);
            CertificateFactory fact = CertificateFactory.getInstance("X.509");
            X509Certificate cert =  (X509Certificate) fact.generateCertificate(is);

            // Get public key
            publicKey = cert.getPublicKey();

//            }
        }
        catch (FileNotFoundException ex)
        {
        }
        catch ( IOException ex)
        {
        }
        finally
        {
            try
            {
                is.close();
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }


        return publicKey;

    }

    private String sendOrgKYCRequest(String xmlSignOutput) throws IOException {


        String request = xmlSignOutput;

        URL url = new URL("http://dsp.csccloud.in/esignauth/api/signer");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        // Set timeout as per needs
        connection.setConnectTimeout(20000);
        connection.setReadTimeout(20000);

        // Set DoOutput to true if you want to use URLConnection for output.
        // Default is false
        connection.setDoOutput(true);

        connection.setUseCaches(true);
        connection.setRequestMethod("POST");

        // Set Headers
//        connection.setRequestProperty("Accept", "application/xml");
        connection.setRequestProperty("Content-Type", "application/xml");
        connection.setRequestProperty("Cache-Control", "no-cache");
        connection.setRequestProperty("AspId","CSORG1000002");
        connection.setRequestProperty("Body", "xml");

        // Write XML
        OutputStream outputStream = connection.getOutputStream();
        byte[] b = request.getBytes("UTF-8");
        outputStream.write(b);
        outputStream.flush();
        outputStream.close();

        // Read XML
        InputStream inputStream = connection.getInputStream();
        byte[] res = new byte[2048];
        int i = 0;
        StringBuilder response = new StringBuilder();
        while ((i = inputStream.read(res)) != -1) {
            response.append(new String(res, 0, i));
        }
        inputStream.close();

        System.out.println("Response= " + response.toString());
        return response.toString();
    }

    }


