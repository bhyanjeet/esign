package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.service.ExternalUserEsignRequestService;
import com.hartron.esignharyana.domain.ExternalUserEsignRequest;
import com.hartron.esignharyana.repository.ExternalUserEsignRequestRepository;
import com.hartron.esignharyana.repository.search.ExternalUserEsignRequestSearchRepository;
import com.hartron.esignharyana.service.dto.ExternalUserEsignRequestDTO;
import com.hartron.esignharyana.service.mapper.ExternalUserEsignRequestMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link ExternalUserEsignRequest}.
 */
@Service
@Transactional
public class ExternalUserEsignRequestServiceImpl implements ExternalUserEsignRequestService {

    private final Logger log = LoggerFactory.getLogger(ExternalUserEsignRequestServiceImpl.class);

    private final ExternalUserEsignRequestRepository externalUserEsignRequestRepository;

    private final ExternalUserEsignRequestMapper externalUserEsignRequestMapper;

    private final ExternalUserEsignRequestSearchRepository externalUserEsignRequestSearchRepository;

    public ExternalUserEsignRequestServiceImpl(ExternalUserEsignRequestRepository externalUserEsignRequestRepository, ExternalUserEsignRequestMapper externalUserEsignRequestMapper, ExternalUserEsignRequestSearchRepository externalUserEsignRequestSearchRepository) {
        this.externalUserEsignRequestRepository = externalUserEsignRequestRepository;
        this.externalUserEsignRequestMapper = externalUserEsignRequestMapper;
        this.externalUserEsignRequestSearchRepository = externalUserEsignRequestSearchRepository;
    }

    /**
     * Save a externalUserEsignRequest.
     *
     * @param externalUserEsignRequestDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ExternalUserEsignRequestDTO save(ExternalUserEsignRequestDTO externalUserEsignRequestDTO) {
        log.debug("Request to save ExternalUserEsignRequest : {}", externalUserEsignRequestDTO);
        ExternalUserEsignRequest externalUserEsignRequest = externalUserEsignRequestMapper.toEntity(externalUserEsignRequestDTO);
        externalUserEsignRequest = externalUserEsignRequestRepository.save(externalUserEsignRequest);
        ExternalUserEsignRequestDTO result = externalUserEsignRequestMapper.toDto(externalUserEsignRequest);
        externalUserEsignRequestSearchRepository.save(externalUserEsignRequest);
        return result;
    }

    /**
     * Get all the externalUserEsignRequests.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ExternalUserEsignRequestDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ExternalUserEsignRequests");
        return externalUserEsignRequestRepository.findAll(pageable)
            .map(externalUserEsignRequestMapper::toDto);
    }

    /**
     * Get one externalUserEsignRequest by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ExternalUserEsignRequestDTO> findOne(Long id) {
        log.debug("Request to get ExternalUserEsignRequest : {}", id);
        return externalUserEsignRequestRepository.findById(id)
            .map(externalUserEsignRequestMapper::toDto);
    }

    /**
     * Delete the externalUserEsignRequest by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ExternalUserEsignRequest : {}", id);
        externalUserEsignRequestRepository.deleteById(id);
        externalUserEsignRequestSearchRepository.deleteById(id);
    }

    /**
     * Search for the externalUserEsignRequest corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ExternalUserEsignRequestDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ExternalUserEsignRequests for query {}", query);
        return externalUserEsignRequestSearchRepository.search(queryStringQuery(query), pageable)
            .map(externalUserEsignRequestMapper::toDto);
    }
}
