package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.service.DistrictMasterService;
import com.hartron.esignharyana.domain.DistrictMaster;
import com.hartron.esignharyana.repository.DistrictMasterRepository;
import com.hartron.esignharyana.repository.search.DistrictMasterSearchRepository;
import com.hartron.esignharyana.service.dto.DistrictMasterDTO;
import com.hartron.esignharyana.service.mapper.DistrictMasterMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link DistrictMaster}.
 */
@Service
@Transactional
public class DistrictMasterServiceImpl implements DistrictMasterService {

    private final Logger log = LoggerFactory.getLogger(DistrictMasterServiceImpl.class);

    private final DistrictMasterRepository districtMasterRepository;

    private final DistrictMasterMapper districtMasterMapper;

    private final DistrictMasterSearchRepository districtMasterSearchRepository;

    public DistrictMasterServiceImpl(DistrictMasterRepository districtMasterRepository, DistrictMasterMapper districtMasterMapper, DistrictMasterSearchRepository districtMasterSearchRepository) {
        this.districtMasterRepository = districtMasterRepository;
        this.districtMasterMapper = districtMasterMapper;
        this.districtMasterSearchRepository = districtMasterSearchRepository;
    }

    /**
     * Save a districtMaster.
     *
     * @param districtMasterDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DistrictMasterDTO save(DistrictMasterDTO districtMasterDTO) {
        log.debug("Request to save DistrictMaster : {}", districtMasterDTO);
        DistrictMaster districtMaster = districtMasterMapper.toEntity(districtMasterDTO);
        districtMaster = districtMasterRepository.save(districtMaster);
        DistrictMasterDTO result = districtMasterMapper.toDto(districtMaster);
        districtMasterSearchRepository.save(districtMaster);
        return result;
    }

    /**
     * Get all the districtMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DistrictMasterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DistrictMasters");
        return districtMasterRepository.findAll(pageable)
            .map(districtMasterMapper::toDto);
    }


    /**
     * Get one districtMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DistrictMasterDTO> findOne(Long id) {
        log.debug("Request to get DistrictMaster : {}", id);
        return districtMasterRepository.findById(id)
            .map(districtMasterMapper::toDto);
    }

    /**
     * Delete the districtMaster by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DistrictMaster : {}", id);
        districtMasterRepository.deleteById(id);
        districtMasterSearchRepository.deleteById(id);
    }

    /**
     * Search for the districtMaster corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DistrictMasterDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DistrictMasters for query {}", query);
        return districtMasterSearchRepository.search(queryStringQuery(query), pageable)
            .map(districtMasterMapper::toDto);
    }
}
