package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.service.SubDistrictMasterService;
import com.hartron.esignharyana.domain.SubDistrictMaster;
import com.hartron.esignharyana.repository.SubDistrictMasterRepository;
import com.hartron.esignharyana.repository.search.SubDistrictMasterSearchRepository;
import com.hartron.esignharyana.service.dto.SubDistrictMasterDTO;
import com.hartron.esignharyana.service.mapper.SubDistrictMasterMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link SubDistrictMaster}.
 */
@Service
@Transactional
public class SubDistrictMasterServiceImpl implements SubDistrictMasterService {

    private final Logger log = LoggerFactory.getLogger(SubDistrictMasterServiceImpl.class);

    private final SubDistrictMasterRepository subDistrictMasterRepository;

    private final SubDistrictMasterMapper subDistrictMasterMapper;

    private final SubDistrictMasterSearchRepository subDistrictMasterSearchRepository;

    public SubDistrictMasterServiceImpl(SubDistrictMasterRepository subDistrictMasterRepository, SubDistrictMasterMapper subDistrictMasterMapper, SubDistrictMasterSearchRepository subDistrictMasterSearchRepository) {
        this.subDistrictMasterRepository = subDistrictMasterRepository;
        this.subDistrictMasterMapper = subDistrictMasterMapper;
        this.subDistrictMasterSearchRepository = subDistrictMasterSearchRepository;
    }

    /**
     * Save a subDistrictMaster.
     *
     * @param subDistrictMasterDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public SubDistrictMasterDTO save(SubDistrictMasterDTO subDistrictMasterDTO) {
        log.debug("Request to save SubDistrictMaster : {}", subDistrictMasterDTO);
        SubDistrictMaster subDistrictMaster = subDistrictMasterMapper.toEntity(subDistrictMasterDTO);
        subDistrictMaster = subDistrictMasterRepository.save(subDistrictMaster);
        SubDistrictMasterDTO result = subDistrictMasterMapper.toDto(subDistrictMaster);
        subDistrictMasterSearchRepository.save(subDistrictMaster);
        return result;
    }

    /**
     * Get all the subDistrictMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SubDistrictMasterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SubDistrictMasters");
        return subDistrictMasterRepository.findAll(pageable)
            .map(subDistrictMasterMapper::toDto);
    }


    /**
     * Get one subDistrictMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SubDistrictMasterDTO> findOne(Long id) {
        log.debug("Request to get SubDistrictMaster : {}", id);
        return subDistrictMasterRepository.findById(id)
            .map(subDistrictMasterMapper::toDto);
    }

    /**
     * Delete the subDistrictMaster by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SubDistrictMaster : {}", id);
        subDistrictMasterRepository.deleteById(id);
        subDistrictMasterSearchRepository.deleteById(id);
    }

    /**
     * Search for the subDistrictMaster corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SubDistrictMasterDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SubDistrictMasters for query {}", query);
        return subDistrictMasterSearchRepository.search(queryStringQuery(query), pageable)
            .map(subDistrictMasterMapper::toDto);
    }
}
