package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.service.UserAuthDetailsService;
import com.hartron.esignharyana.domain.UserAuthDetails;
import com.hartron.esignharyana.repository.UserAuthDetailsRepository;
import com.hartron.esignharyana.repository.search.UserAuthDetailsSearchRepository;
import com.hartron.esignharyana.service.dto.UserAuthDetailsDTO;
import com.hartron.esignharyana.service.mapper.UserAuthDetailsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link UserAuthDetails}.
 */
@Service
@Transactional
public class UserAuthDetailsServiceImpl implements UserAuthDetailsService {

    private final Logger log = LoggerFactory.getLogger(UserAuthDetailsServiceImpl.class);

    private final UserAuthDetailsRepository userAuthDetailsRepository;

    private final UserAuthDetailsMapper userAuthDetailsMapper;

    private final UserAuthDetailsSearchRepository userAuthDetailsSearchRepository;

    public UserAuthDetailsServiceImpl(UserAuthDetailsRepository userAuthDetailsRepository, UserAuthDetailsMapper userAuthDetailsMapper, UserAuthDetailsSearchRepository userAuthDetailsSearchRepository) {
        this.userAuthDetailsRepository = userAuthDetailsRepository;
        this.userAuthDetailsMapper = userAuthDetailsMapper;
        this.userAuthDetailsSearchRepository = userAuthDetailsSearchRepository;
    }

    /**
     * Save a userAuthDetails.
     *
     * @param userAuthDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UserAuthDetailsDTO save(UserAuthDetailsDTO userAuthDetailsDTO) {
        log.debug("Request to save UserAuthDetails : {}", userAuthDetailsDTO);
        UserAuthDetails userAuthDetails = userAuthDetailsMapper.toEntity(userAuthDetailsDTO);
        userAuthDetails = userAuthDetailsRepository.save(userAuthDetails);
        UserAuthDetailsDTO result = userAuthDetailsMapper.toDto(userAuthDetails);
        userAuthDetailsSearchRepository.save(userAuthDetails);
        return result;
    }

    /**
     * Get all the userAuthDetails.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<UserAuthDetailsDTO> findAll() {
        log.debug("Request to get all UserAuthDetails");
        return userAuthDetailsRepository.findAll().stream()
            .map(userAuthDetailsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one userAuthDetails by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserAuthDetailsDTO> findOne(Long id) {
        log.debug("Request to get UserAuthDetails : {}", id);
        return userAuthDetailsRepository.findById(id)
            .map(userAuthDetailsMapper::toDto);
    }

    /**
     * Delete the userAuthDetails by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserAuthDetails : {}", id);
        userAuthDetailsRepository.deleteById(id);
        userAuthDetailsSearchRepository.deleteById(id);
    }

    /**
     * Search for the userAuthDetails corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<UserAuthDetailsDTO> search(String query) {
        log.debug("Request to search UserAuthDetails for query {}", query);
        return StreamSupport
            .stream(userAuthDetailsSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(userAuthDetailsMapper::toDto)
            .collect(Collectors.toList());
    }

    @Override
    public List<UserAuthDetails> getAllByToken(String authToken) {
        return userAuthDetailsRepository.getAllByAuthToken(authToken);
    }
}
