package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.domain.EsignTransation;
import com.hartron.esignharyana.domain.ExternalUserEsignRequest;
import com.hartron.esignharyana.domain.UserDetails;
import com.hartron.esignharyana.repository.EsignTransationRepository;
import com.hartron.esignharyana.repository.ExternalUserEsignRequestRepository;
import com.hartron.esignharyana.repository.search.ApplicationMasterSearchRepository;
import com.hartron.esignharyana.repository.search.EsignTransationSearchRepository;
import com.hartron.esignharyana.repository.search.ExternalUserEsignRequestSearchRepository;
import com.hartron.esignharyana.repository.search.UserDetailsSearchRepository;
import com.hartron.esignharyana.service.EsignUserRequestService;
import com.hartron.esignharyana.service.dto.EsignRequestDTO;
import com.hartron.esignharyana.service.dto.ExternalUserEsignRequestDTO;
import com.hartron.esignharyana.service.mapper.ExternalUserEsignRequestMapper;
import com.hartron.esignharyana.service.mapper.UserDetailsMapper;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.cms.SignedData;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.*;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.util.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import sun.security.pkcs.PKCS7;
import sun.security.pkcs.SignerInfo;
import sun.security.util.DerOutputStream;
import sun.security.util.DerValue;
import sun.security.x509.AlgorithmId;
import sun.security.x509.X500Name;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.keyinfo.X509IssuerSerial;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Service Implementation for managing {@link UserDetails}.
 */
@Service
@Transactional
public class ESignUserRequestServiceImpl implements EsignUserRequestService {

    private final Logger log = LoggerFactory.getLogger(ESignUserRequestServiceImpl.class);

    private String txnId = "";

    private final UserDetailsMapper userDetailsMapper;

    private final UserDetailsSearchRepository userDetailsSearchRepository;

    private final EsignTransationRepository esignTransationRepository;

    private final EsignTransationSearchRepository esignTransationSearchRepository;

    private final ExternalUserEsignRequestRepository externalUserEsignRequestRepository;

    private final ExternalUserEsignRequestMapper externalUserEsignRequestMapper;

    private final ExternalUserEsignRequestSearchRepository externalUserEsignRequestSearchRepository;

    private final ApplicationMasterSearchRepository applicationMasterSearchRepository;

    private final static String ASP_REDIRECT_URL = "http://stagingesign.hartron.org.in/api/sign/redirect";
    private final static String ASP_RESPONSE_URL = "http://stagingesign.hartron.org.in/api/sign/response";
    private final static String ASP_IP = "::1";
    private final static String ASP_ID = "CSORG1000002";

    private static final String POST_URL_SIGNER = "http://dsp.csccloud.in/esignauth/api/signer";

    private static final String POST_URL_SIGN_WITH_HTTP = "http://dsp.csccloud.in/esignauth/sign";

    private static final String POST_URL_SIGN_WITH_HTTPS = "https://dsp.csccloud.in/esignauth/sign";





    private String userRequestedXmlTs = "";

    private String userRequestedXmlTxn = "";
    private String userRequestedXmlDocInfo = "";
    private String userRequestedXmlDocUrl = "";
    private String userRequestedXmlDocHashHexa = "";
    private String userRequestedXmlSignerId = "";
    private String userRequestedTxnRef = "";
    private String getUserRequestedXmlRedirectUrl= "";
    private String getUserRequestedXmlResponseUrl= "";
    private String errorMessage = "";





    public ESignUserRequestServiceImpl(UserDetailsMapper userDetailsMapper, UserDetailsSearchRepository userDetailsSearchRepository, EsignTransationRepository esignTransationRepository, EsignTransationSearchRepository esignTransationSearchRepository, ExternalUserEsignRequestRepository externalUserEsignRequestRepository, ExternalUserEsignRequestMapper externalUserEsignRequestMapper, ExternalUserEsignRequestSearchRepository externalUserEsignRequestSearchRepository, ApplicationMasterSearchRepository applicationMasterSearchRepository) {

        this.userDetailsMapper = userDetailsMapper;
        this.userDetailsSearchRepository = userDetailsSearchRepository;

        this.esignTransationRepository = esignTransationRepository;
        this.esignTransationSearchRepository = esignTransationSearchRepository;
        this.externalUserEsignRequestRepository = externalUserEsignRequestRepository;
        this.externalUserEsignRequestMapper = externalUserEsignRequestMapper;
        this.externalUserEsignRequestSearchRepository = externalUserEsignRequestSearchRepository;
        this.applicationMasterSearchRepository = applicationMasterSearchRepository;
    }

    public boolean verifySign(String xml){

        boolean verifySign = verify(xml);

        return verifySign;
    }

    public String sendEsignRequestToCSC(String userCodeId, String applicationIdCode, String responseUrl, String txn, String req_data, HttpServletRequest request, HttpServletResponse response) throws NoSuchAlgorithmException, ParserConfigurationException, IOException, SAXException, TransformerException, XMLSignatureException, MarshalException, KeyStoreException, UnrecoverableEntryException, CertificateException, InvalidAlgorithmParameterException {

        String decodedUserRequestXml = decodeUserRequestXml(req_data);
        getUserXmlContents(decodedUserRequestXml);
        userRequestedTxnRef = createTxnRef(userRequestedXmlTxn, ASP_REDIRECT_URL);



//        boolean checkUserIsPresent = checkUserIsPresent(userCodeId);
//        if (!checkUserIsPresent) {
//            errorMessage = "UserNotPresent";
//            return errorMessage;
//        }

//        boolean checkApplicationIsPrenent = checkApplicationIsPrenent(applicationIdCode);
//        if (!checkApplicationIsPrenent) {
//            errorMessage = "AppliationNotPresent";
//            return errorMessage;
//        }

        System.out.println("userRequestedXmlSignerIdddddddddddd" + userRequestedXmlSignerId);


        String signerStatusXml = statusSignerXML();
       // System.out.println("signerStatussssss" + signerStatusXml);

        String signXML = signXML(signerStatusXml);

        System.out.println("signeDDDDDDDDDDDDDDStatussssss" + signXML);

        String signerStatusRespone = sendSignerStatusRequest(signXML);
        System.out.println("signeDDDDDDDDDDDDDDStatussssss" + signerStatusRespone);
//
      //  String signerStatusResponeCode = getSignerStatusResponeCode(signerStatusRespone);

      //  System.out.println("signerStatusResponeCodeeeeeeeeee" + signerStatusResponeCode);

//        if (signerStatusResponeCode.equals("902")){
//            errorMessage = "InvaidSignerId";
//            response.sendRedirect("http://localhost:9001/user-esign-request-form/" + errorMessage);
//            return errorMessage;
//        }

      //  boolean checkSignerStatus = statusSignerXML(userRequestedXmlSignerId);

        boolean checkTxnIsPersent = checkTxnIsPersent(userRequestedXmlTxn);
        if (!checkTxnIsPersent) {
            ExternalUserEsignRequestDTO saveUserRequest = saveUserRequest(userCodeId, applicationIdCode, responseUrl, txn, response);
            response.sendRedirect("http://stagingesign.hartron.org.in/esign-final-form-request/" + userRequestedXmlTxn);
        }else {
            errorMessage = "TxnAllreadyPresent";
            response.sendRedirect("http://stagingesign.hartron.org.in/user-esign-request-form/" + errorMessage);
            return errorMessage;
        }

        return  userRequestedTxnRef;

    }

    public boolean checkTxnIsPersent(String txn){
        boolean isTxnPersent = false;
        Optional<ExternalUserEsignRequestDTO> checkTxnDTO1 =  externalUserEsignRequestSearchRepository.findByTxn(txn).map(externalUserEsignRequestMapper::toDto);

        if (checkTxnDTO1.isPresent()){
            isTxnPersent = true;
        }
        return isTxnPersent;
    }

//    public boolean checkUserIsPresent(String userCodeId){
//        boolean isUserPresent = false;
//        Optional<UserDetails> userDetails =   this.userDetailsSearchRepository.findByUserIdCode(userCodeId);
//            if (userDetails.isPresent()){
//                isUserPresent = true;
//            }
//        return isUserPresent;
//    }

//    public boolean checkApplicationIsPrenent(String applicationIdCode){
//        boolean isApplicatinPresent = false;
//        boolean applicationMaster =   this.applicationMasterSearchRepository.findByApplicationIdCode(applicationIdCode);
//        if (applicationMaster){
//            isApplicatinPresent = true;
//        }
//        return isApplicatinPresent;
//    }

    public String statusSignerXML() throws  ParserConfigurationException, TransformerException, MarshalException, XMLSignatureException, IOException, SAXException, KeyStoreException, CertificateException, UnrecoverableEntryException {

        Date now = new Date();
        String format3 = new SimpleDateFormat("yyMMddHHmmss", Locale.ENGLISH).format(now);
        String randomNo = RandomStringUtils.randomNumeric(6);
        String txnId = format3 + randomNo;

//        log.debug("REST request to save ApplicationMaster : {}");


        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;

        dBuilder = dbFactory.newDocumentBuilder();

        Document doc = dBuilder.newDocument();
        doc.setXmlStandalone(true);
        StringWriter writer = new StringWriter();

        Element rootElement = doc.createElement("Signer");

        doc.appendChild(rootElement);

        Attr api = doc.createAttribute("api");
        api.setValue("status");
        rootElement.setAttributeNode(api);

        Attr orgType = doc.createAttribute("orgType");
        orgType.setValue("ORG");
        rootElement.setAttributeNode(orgType);

        Attr orgId = doc.createAttribute("orgId");
        orgId.setValue("CSORG1000002");
        rootElement.setAttributeNode(orgId);

        Attr eKycType = doc.createAttribute("eKycType");
        eKycType.setValue("ORG1");
        rootElement.setAttributeNode(eKycType);

        Attr ver = doc.createAttribute("ver");
        ver.setValue("1.0");
        rootElement.setAttributeNode(ver);

        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String formattedDate = myDateObj.format(myFormatObj);

        Attr ts = doc.createAttribute("ts");

        ts.setValue(formattedDate);
        rootElement.setAttributeNode(ts);

        Attr txn = doc.createAttribute("txn");
        txn.setValue(txnId);
        rootElement.setAttributeNode(txn);


        Attr aspId = doc.createAttribute("aspId");
        aspId.setValue("CSORG1000002");
        rootElement.setAttributeNode(aspId);

        Attr signerIdAttr = doc.createAttribute("signerid");
        signerIdAttr.setValue(userRequestedXmlSignerId);
        rootElement.setAttributeNode(signerIdAttr);

        Attr kycData = doc.createAttribute("kycData");
        kycData.setValue("N");
        rootElement.setAttributeNode(kycData);
//        rootElement.appendChild(doc.createTextNode(encodedString));


        // create the xml file
        //transform the DOM Object to an XML File
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(doc);
        //StreamResult streamResult = new StreamResult(new File(xmlFilePath));

        // If you use
        StreamResult result = new StreamResult(writer);
        // the output will be pushed to the standard output ...
        // You can use that for debugging

        transformer.transform(domSource, result);


        return writer.toString();


    }

    public String sendSignerStatusRequest(String signerStatusXml) throws IOException {
        String request = signerStatusXml;

        System.out.println("requestxmllllllllllll" + request);

        URL url = new URL(POST_URL_SIGNER);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        // Set timeout as per needs
        connection.setConnectTimeout(20000);
        connection.setReadTimeout(20000);

        // Set DoOutput to true if you want to use URLConnection for output.
        // Default is false
        connection.setDoOutput(true);

        connection.setUseCaches(true);
        connection.setRequestMethod("POST");

        // Set Headers
//        connection.setRequestProperty("Accept", "application/xml");
        connection.setRequestProperty("Content-Type", "application/xml");
        connection.setRequestProperty("Cache-Control", "no-cache");
        connection.setRequestProperty("AspId","CSORG1000002");
        connection.setRequestProperty("Body", "xml");

        // Write XML
        OutputStream outputStream = connection.getOutputStream();
        byte[] b = request.getBytes("UTF-8");
        outputStream.write(b);
        outputStream.flush();
        outputStream.close();

        // Read XML
        InputStream inputStream = connection.getInputStream();
        byte[] res = new byte[2048];
        int i = 0;
        StringBuilder response = new StringBuilder();
        while ((i = inputStream.read(res)) != -1) {
            response.append(new String(res, 0, i));
        }
        inputStream.close();

        System.out.println("Response= " + response.toString());
        return response.toString();
    }


    public String getSignerStatusResponeCode(String signerStatusRespone) throws ParserConfigurationException, IOException, SAXException {

        String error = "";

        DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
        // dbf.setNamespaceAware(true);
        Document doc1 =
            dbf1.newDocumentBuilder().parse(org.apache.commons.io.IOUtils.toInputStream(String.valueOf(signerStatusRespone), "UTF-8"));
        doc1.setXmlStandalone(true);
        Element element1 = (Element) doc1.getElementsByTagName("SignerRes").item(0);
        String responseInfo =    element1.getAttribute("info");
        error =    element1.getAttribute("error");
        //  Element element = (Element) doc1.getAttributes("SignerRe");
return error;

    }

    public String createTxnRef(String userRequestedXmlTxn, String ASP_REDIRECT_URL){
        userRequestedTxnRef = Base64.encodeBase64String((userRequestedXmlTxn +"|"+ ASP_REDIRECT_URL).getBytes());
        return userRequestedTxnRef;
    }

    public ExternalUserEsignRequestDTO saveUserRequest(String userCodeId, String applicationIdCode, String responseUrl, String txn, HttpServletResponse response) throws IOException, SAXException, ParserConfigurationException {

        ExternalUserEsignRequestDTO externalUserEsignRequestDTO = new ExternalUserEsignRequestDTO();

        externalUserEsignRequestDTO.setTxnRef(userRequestedTxnRef);
        externalUserEsignRequestDTO.setApplicationIdCode(applicationIdCode);
        externalUserEsignRequestDTO.setDocHash(userRequestedXmlDocHashHexa);
        externalUserEsignRequestDTO.setUserCodeId(userCodeId);
        externalUserEsignRequestDTO.setAspId(ASP_ID);
        externalUserEsignRequestDTO.setAspIp(ASP_IP);
        externalUserEsignRequestDTO.setDocInfo(userRequestedXmlDocInfo);
        externalUserEsignRequestDTO.setDocUrl(userRequestedXmlDocUrl);
        externalUserEsignRequestDTO.setSignerId(userRequestedXmlSignerId);
        externalUserEsignRequestDTO.setTs(userRequestedXmlTs);
        externalUserEsignRequestDTO.setTxn(userRequestedXmlTxn);
        externalUserEsignRequestDTO.setResponseUrl(getUserRequestedXmlResponseUrl);
        externalUserEsignRequestDTO.setRedirectUrl(getUserRequestedXmlRedirectUrl);

        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String formattedDate = myDateObj.format(myFormatObj);

        externalUserEsignRequestDTO.setRequestTime(formattedDate);

        ExternalUserEsignRequest externalUserEsignRequest = externalUserEsignRequestMapper.toEntity(externalUserEsignRequestDTO);
        externalUserEsignRequest = externalUserEsignRequestRepository.save(externalUserEsignRequest);
        ExternalUserEsignRequestDTO result = externalUserEsignRequestMapper.toDto(externalUserEsignRequest);
        externalUserEsignRequestSearchRepository.save(externalUserEsignRequest);
        return result;

    }


    public ExternalUserEsignRequestDTO findByTxn(String txn) throws NoSuchAlgorithmException, ParserConfigurationException, IOException, SAXException, TransformerException, DocumentException, XMLSignatureException, MarshalException, KeyStoreException, UnrecoverableEntryException, CertificateException, InvalidAlgorithmParameterException {
        System.out.println("ttttttttttttxnnnnnnnnnnnnn" + txn);
        Optional<ExternalUserEsignRequestDTO> externalUserEsignRequestDTO =  externalUserEsignRequestSearchRepository.findByTxn(txn).map(externalUserEsignRequestMapper::toDto);
        ExternalUserEsignRequestDTO externalUserEsignRequestDTO1 = new ExternalUserEsignRequestDTO();

if (externalUserEsignRequestDTO.isPresent()) {
    String getDocInfo = externalUserEsignRequestDTO.get().getDocInfo();
    String getDocUrl = externalUserEsignRequestDTO.get().getDocUrl();
    String getDocHash = externalUserEsignRequestDTO.get().getDocHash();
    String gerSignerId = externalUserEsignRequestDTO.get().getSignerId();
    String getTxn = externalUserEsignRequestDTO.get().getTxn();
    String getTs = externalUserEsignRequestDTO.get().getTs();
    String getTxnRef = externalUserEsignRequestDTO.get().getTxnRef();
    String getUserCodeId = externalUserEsignRequestDTO.get().getUserCodeId();
    String getAppIdCode = externalUserEsignRequestDTO.get().getApplicationIdCode();

    String createEsignXml = createESignXML(getDocInfo, getDocUrl, getDocHash, gerSignerId, getTxn, getTs);

    String signXML = signXML(createEsignXml);

    String getReqdata = getReqdata(signXML);

    externalUserEsignRequestDTO1.setAspId(ASP_ID);
    externalUserEsignRequestDTO1.setAspIp(ASP_IP);
    externalUserEsignRequestDTO1.setTxnRef(getTxnRef);
    externalUserEsignRequestDTO1.setRefData(getReqdata);
    externalUserEsignRequestDTO1.setUserCodeId(getUserCodeId);
    externalUserEsignRequestDTO1.setDocUrl(getDocUrl);
    externalUserEsignRequestDTO1.setApplicationIdCode(getAppIdCode);


}

        return externalUserEsignRequestDTO1;
    }

    public  String getReqdata(String signedPdf){
        String res_data = Base64.encodeBase64String(signedPdf.getBytes());
        return res_data;
    }



    public String getEsignResponseData(String EsignRespone) throws IOException, ParserConfigurationException, SAXException, DocumentException, NoSuchAlgorithmException, CertificateException, KeyStoreException, InvalidKeyException, SignatureException, OperatorCreationException, CMSException {


        DocumentBuilderFactory df_eSignResponse = DocumentBuilderFactory.newInstance();
        Document doc_eSignResponse =
            df_eSignResponse.newDocumentBuilder().parse(IOUtils.toInputStream(String.valueOf(EsignRespone), "UTF-8"));
        doc_eSignResponse.setXmlStandalone(true);
        Element element_eSignResponse = (Element) doc_eSignResponse.getElementsByTagName("EsignResp").item(0);
        String status =    element_eSignResponse.getAttribute("status");
        String ts =    element_eSignResponse.getAttribute("ts");
        String txn =    element_eSignResponse.getAttribute("txn");
        String resCode =    element_eSignResponse.getAttribute("resCode");
        String error =    element_eSignResponse.getAttribute("error");

        Element element_UserX509Certificate = (Element) doc_eSignResponse.getElementsByTagName("UserX509Certificate").item(0);

        //String abc = element_UserX509Certificate.getTextContent(element_UserX509Certificate);

        String UserX509Certificate =  element_UserX509Certificate.getTextContent();

        // encode string using Base64 encoder


        // Decoding string
        byte[] bytes_dStr = Base64.decodeBase64(UserX509Certificate.getBytes());
        String dStr = new String(bytes_dStr);



        // String pemdata = "-----BEGIN ENCRYPTED PRIVATE KEY-----" +  "\n" + dStr + "\n" + "-----END ENCRYPTED PRIVATE KEY-----";

        //    System.out.println("decoded_UserX509Certificateeeeeeeeeeeeee" + pemdata);

        Element element_DocSignature = (Element) doc_eSignResponse.getElementsByTagName("DocSignature").item(0);
        //String abc = element_UserX509Certificate.getTextContent(element_UserX509Certificate);

        String DocSignature =  element_DocSignature.getTextContent();


        String pemdata_DocSignature = "-----BEGIN PKCS7-----" +  "\n" + DocSignature + "\n" + "-----END PKCS7-----";

        System.out.println("pemdata_DocSignature" + pemdata_DocSignature);


        //  System.out.println("encodeddataaaaaaaaaaaaaa" + DocSignature);

        //  Base64.Decoder decoder1 = Base64.getDecoder();

        // String dStr2 = "MIIQvQYJKoZIhvcNAQcCoIIQrjCCEKoCAQExDzANBglghkgBZQMEAgEFADALBgkqhkiG9w0BBwGggg4JMIIDIzCCAgugAwIBAgICJ60wDQYJKoZIhvcNAQELBQAwOjELMAkGA1UEBhMCSU4xEjAQBgNVBAoTCUluZGlhIFBLSTEXMBUGA1UEAxMOQ0NBIEluZGlhIDIwMTQwHhcNMTQwMzA1MTAxMDQ5WhcNMjQwMzA1MTAxMDQ5WjA6MQswCQYDVQQGEwJJTjESMBAGA1UEChMJSW5kaWEgUEtJMRcwFQYDVQQDEw5DQ0EgSW5kaWEgMjAxNDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAN7IUL2K/yINrn+sglna9CkJ1AVrbJYBvsylsCF3vhStQC9kb7t4FwX7s+6AAMSakL5GUDJxVVNhMqf/2paerAzFACVNR1AiMLsG7ima4pCDhFn7t9052BQRbLBCPg4wekx6j+QULQFeW9ViLV7hjkEhKffeuoc3YaDmkkPSmA2mz6QKbUWYUu4PqQPRCrkiDH0ikdqR9eyYhWyuI7Gm/pc0atYnp1sru3rtLCaLS0ST/N/ELDEUUY2wgxglgoqEEdMhSSBL1CzaA8Ck9PErpnqC7VL+sbSyAKeJ9n56FttQzkwYjdOHMrgJRZaPb2i5VoVo1ZFkQF3ZKfiJ25VH5+8CAwEAAaMzMDEwDwYDVR0TAQH/BAUwAwEB/zARBgNVHQ4ECgQIQrjFz22zV+EwCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBCwUAA4IBAQAdAUjv0myKyt8GC1niIZplrlksOWIR6yXLg4BhFj4ziULxsGK4Jj0sIJGCkNJeHl+Ng9UlU5EI+r89DRdrGBTF/I+g3RHcViPtOne9xEgWRMRYtWD7QZe5FvoSSGkW9aV6D4iGLPBQML6FDUkQzW9CYDCFgGC2+awRMx61dQVXiFv3Nbkqa1Pejcel8NMAmxjfm5nZMd3Ft13hy3fNF6UzsOnBtMbyZWhS8Koj2KFfSUGX+M/DS1TG2ZujwKKXCuKq7+67m0WF6zohoHJbqjkmKX34zkuFnoXaXco9NkOi0RBvLCiqR2lKfzLM7B69bje+z0EqnRNo5+s8PWSdy+xtMIIEvzCCA6egAwIBAgICJ9EwDQYJKoZIhvcNAQELBQAwOjELMAkGA1UEBhMCSU4xEjAQBgNVBAoTCUluZGlhIFBLSTEXMBUGA1UEAxMOQ0NBIEluZGlhIDIwMTQwHhcNMTkwNTE1MDkxODE1WhcNMjQwMzA1MDYzMDAwWjCB2DELMAkGA1UEBhMCSU4xMDAuBgNVBAoTJ0NTQyBlLUdvdmVybmFuY2UgU2VydmljZXMgSW5kaWEgTGltaXRlZDEdMBsGA1UECxMUQ2VydGlmeWluZyBBdXRob3JpdHkxDzANBgNVBBETBjExMDAwMzEOMAwGA1UECBMFREVMSEkxEzARBgNVBAkTCkxPREhJIFJPQUQxLDAqBgNVBDMTI0VsZWN0cm9uaWNzIE5pa2V0YW4sIDYsIENHTyBDb21wbGV4MRQwEgYDVQQDEwtDU0MgQ0EgMjAxNDCCASEwDQYJKoZIhvcNAQEBBQADggEOADCCAQkCggEBANTWZaRtgf69A5b72maevy8gbMEXfb1Kyq3nzeHCe4JomhbSM7JOWc9KoA84pgei8US6AeqXMAbIKI3sRU4eoFt01NU9GTQlag8+rsytz0PnUSZzoOTArKVjDKITwEx4HIA5cC/G4bnUCWoNT+1jEydmuGJY64dNWZxWWJayQAERyhFXSILUL1Vt6UyrvUvDQ7fxsGvxmmTl//kLpsvyVBq/fRkmBPmkNm3fNqhIXpd8dfcRUL+8SqOr6OP0/yYSJVHY3tyGtK5EY680KJeOzBpaUZuyLsAfGtmQ1fAjOxH4yPeFLJPDflzpdlELvsY9E6gbnu+NJpqXO/4/qpgEFmkCAhERo4IBLzCCASswEwYDVR0jBAwwCoAIQrjFz22zV+EwgYAGCCsGAQUFBwEBBHQwcjAeBggrBgEFBQcwAYYSaHR0cDovL29jdnMuZ292LmluMFAGCCsGAQUFBzAChkRodHRwOi8vd3d3LmNjYS5nb3YuaW4vY2NhL3NpdGVzL2RlZmF1bHQvZmlsZXMvZmlsZXMvQ0NBSW5kaWEyMDE0LmNlcjASBgNVHSAECzAJMAcGBWCCZGQCMEYGA1UdHwQ/MD0wO6A5oDeGNWh0dHA6Ly9jY2EuZ292LmluL3J3L3Jlc291cmNlcy9DQ0FJbmRpYTIwMTRMYXRlc3QuY3JsMBEGA1UdDgQKBAhAPvlXr12fazAOBgNVHQ8BAf8EBAMCAQYwEgYDVR0TAQH/BAgwBgEB/wIBADANBgkqhkiG9w0BAQsFAAOCAQEAnvlnm3AV8sdWNQzlAsnN5sLavjNCH8FHfIJf4xFfBdES3ITug0ZRfUjbvZFKrLXdtzcEXKFKYUjNzEeyWRjdKUDlFF751/2LSrjbov9VDsql0YZ/SGH5bJPOX/dYE6amIz5lbRrX87dyDPDfstWFap3zTClTNqO8StletdABTNxlyco+9y61Gw0q5adDgsKi/aGmYjq9ka16dZ1C3TtqwcRuJnkZub2K6VhwE4V+nPPFenarPx97cf+zx9hJVOAd+BEfFTc8dJ1J81xriHAw6+7wTuwQgYUdQNcLOOpgzyjrE0u7VKhG18tK6PEehX/VXrUSbKgZOxiZE9XkWuyXSTCCBhswggUDoAMCAQICAgCYMA0GCSqGSIb3DQEBCwUAMIHYMQswCQYDVQQGEwJJTjEwMC4GA1UEChMnQ1NDIGUtR292ZXJuYW5jZSBTZXJ2aWNlcyBJbmRpYSBMaW1pdGVkMR0wGwYDVQQLExRDZXJ0aWZ5aW5nIEF1dGhvcml0eTEPMA0GA1UEERMGMTEwMDAzMQ4wDAYDVQQIEwVERUxISTETMBEGA1UECRMKTE9ESEkgUk9BRDEsMCoGA1UEMxMjRWxlY3Ryb25pY3MgTmlrZXRhbiwgNiwgQ0dPIENvbXBsZXgxFDASBgNVBAMTC0NTQyBDQSAyMDE0MB4XDTE5MDgyODE4MjU0OVoXDTE5MDgyODE4NTU0OVowgfExCzAJBgNVBAYTAklOMQ4wDAYDVQQIDAVEZWxoaTERMA8GA1UECgwIUGVyc29uYWwxGDAWBgNVBAMMD0FiaGlzaGVrIFJhbmphbjEPMA0GA1UEEQwGMTEwMDAzMUkwRwYDVQQtDEAzNTU1NTBhZWRjZjY2NmFmYTJmNDhkMjQ5OTk3N2YyNjRhYjg0NTVmM2M2MzVlY2JjZWYwMzJkNmFlNzBjNzMzMUkwRwYDVQRBDEAyMzM2OTZlOGJlMjg5Njc2YzM0MTIyNDg2M2IwYzYwYTE2OWQ0MzVmNDNkZDZhODZmMjg2YjY3MzhmMzcwYmU0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAroNJVkmNbe1G5XopRJTCB7QdpkZAQjFRvPQ8MpuafTMLk8juF8E3l6lpMGVOxDJlfhcl3vM0BCe4Hf+e8vAN+Quc9FsfiaksPwzjVPUKkNo41qlrZ7R+KT31XV1n7GfWZKj5ITugCRqW1IMZEwcB4OWheh5WURK/XPOywEXkrabZZzx6g9utWJHQEsBEuEoxVpQt1iro8J9qj4zt36rCwhzAdksRwHZPWbHZKg5FKmD83cBZEfXT0EYqN0cTHxgnsT4MzPz/zIq+XXrWGcwYBSVQTX3v6MQEjRK64O9qeXtWLkKPEQQCQuBqlqQbCIB9uPWi1ccF8qmb3h61XolLTwIDAQABo4IB0jCCAc4wCQYDVR0TBAIwADAdBgNVHQ4EFgQUFMUKR5bW6y5oSzktSjKovC/ig1YwEwYDVR0jBAwwCoAIQD75V69dn2swDgYDVR0PAQH/BAQDAgbAMEsGA1UdHwREMEIwQKA+oDyGOmh0dHBzOi8vY3NjLmdvdi5pbi9lc2lnbi9yZXBvc2l0b3J5LzIwMTkvQ1NDX0NBXzIwMTQtMS5jcmwwggEuBgNVHSAEggElMIIBITCB8gYHYIJkZAEJAjCB5jA0BggrBgEFBQcCARYoaHR0cHM6Ly9jc2MuZ292LmluL2VzaWduL0NTQ0NBLUNQU18yLnBkZjCBrQYIKwYBBQUHAgIwgaAwLBYoQ1NDIGUtR292ZXJuYW5jZSBTZXJ2aWNlcyBJbmRpYSBMaW1pdGVkLjAAGnBUaGlzIENQUyBpcyBvd25lZCBieSBDU0MgQ0EgYW5kIHVzZXJzIGFyZSByZXF1ZXN0ZWQgdG8gcmVhZCBDUFMgYmVmb3JlIHVzaW5nIHRoZSBDU0MgQ0EncyBjZXJ0aWZpY2F0aW9uIHNlcnZpY2VzMCoGB2CCZGQCBAEwHzAdBggrBgEFBQcCAjARGg9DU0MgQ0EgS1lDIDIwMTkwDQYJKoZIhvcNAQELBQADggEBAIEL14JeY2OTcQ60icncR+C5uVXRUFgQm92xkxC/ieSdgxOg43W7aQFoevlWSJRsuYS76BpFpgHhXlAkD5QRTkXpe144kMpe3uMeysb9X7ptAnkhMxV8u/aH/UnGpFgrebntgF/qpPLMw2XJkiTHaDyAmEp12V2QxJPeC8Yz7YxU5S55FAjb/T2US5rcwip25H8eitDSq1JIGBz3Ght61OR+Z9QkBKRdhZr2s8YPYeRfpjAu7FL3X0Gi1JL1ur1kx2RHqhA3nb4SYtVXLr9tewbTkBVGdCNtHb/9dcr0WIcKAvKEn59c3LpzHq5T5kFDbVC1ZKbl9aPS4MU1Bb/lfdQxggJ4MIICdAIBATCB3zCB2DELMAkGA1UEBhMCSU4xMDAuBgNVBAoTJ0NTQyBlLUdvdmVybmFuY2UgU2VydmljZXMgSW5kaWEgTGltaXRlZDEdMBsGA1UECxMUQ2VydGlmeWluZyBBdXRob3JpdHkxDzANBgNVBBETBjExMDAwMzEOMAwGA1UECBMFREVMSEkxEzARBgNVBAkTCkxPREhJIFJPQUQxLDAqBgNVBDMTI0VsZWN0cm9uaWNzIE5pa2V0YW4sIDYsIENHTyBDb21wbGV4MRQwEgYDVQQDEwtDU0MgQ0EgMjAxNAICAJgwDQYJYIZIAWUDBAIBBQCgazAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBME8GCSqGSIb3DQEJBDFCBEBmZmJlZDczN2UwYzBkZjgyMWM4YTM0MmM3YTkyZGQwYTMwNzEyOWQ1YTlkODMxZjY4NDY2OGJiNzlmMDk4Yzc0MA0GCSqGSIb3DQEBAQUABIIBAHKDpMPcHyZn17Xz1c7NDVRtj/yhbBOhuJDSyLZMIO3A9/BSCenav3Zi66rEAaDjYej6mG34VeEgYxMU6r1seFkyc9bBSZ4c+Q2TESWvvpyFpXUskDXNITzFIQk/C1Dud0AIOwUwK/dI1ShZBL3tocMqu5iPmTV62n00PRStcH1n9KgbXr5ojxLTCts+1gavFxedRgkn8OtUfHmdLyU1A/0iNvn7Q/U4tT2uVz9fTi3clbCIelzOBHGLAPe3X2ft87qlLJ9vDDqX/Dbamk/QTcohBibdR10GExfyKyjOXW+e2uPGrJW+MbCYAfy5EzOrgvr1gxoy4ktpdJAVV3Ntg10=";

        // System.out.println("ddddencodeddataaaaaaaaaaaaaa" + DocSignature);



        // byte[] signedByte = Base64.getEncoder().encode(DocSignature.getBytes(Charset.forName("UTF-8")));

        ;

        //  System.out.println("byteesssssssssssss" + Arrays.toString(signedByte));


//        File f = new File("/var/www/esign/signature.p7b");
        byte[] buffer = new byte[(int) DocSignature.length()];
        DataInputStream in = new DataInputStream(IOUtils.toInputStream(String.valueOf(DocSignature), "UTF-8")) {
            @Override
            public int read() throws IOException {
                return 0;
            }
        };
        in.readFully(buffer);
        in.close();
        CMSSignedData signature = new CMSSignedData(buffer);

        System.out.println("signatureeeeeeeeeeeeeeeeeeeee" + signature);


        // Decoding string
        byte[] bytes_dStr0 = Base64.encodeBase64(DocSignature.getBytes());

        String dStr0 = new String(bytes_dStr0);


        System.out.println("decodedddddddd" + dStr0);



        byte[] DocSignatureInByte = dStr0.getBytes();

        System.out.println("without-----byteesssssssssssss" + DocSignatureInByte);
        System.out.println("byteesssssssssssss" + Arrays.toString(DocSignatureInByte));


        Security.addProvider(new BouncyCastleProvider());

        // String toVerify = "MIIQvQYJKoZIhvcNAQcCoIIQrjCCEKoCAQExDzANBglghkgBZQMEAgEFADALBgkqhkiG9w0BBwGggg4JMIIDIzCCAgugAwIBAgICJ60wDQYJKoZIhvcNAQELBQAwOjELMAkGA1UEBhMCSU4xEjAQBgNVBAoTCUluZGlhIFBLSTEXMBUGA1UEAxMOQ0NBIEluZGlhIDIwMTQwHhcNMTQwMzA1MTAxMDQ5WhcNMjQwMzA1MTAxMDQ5WjA6MQswCQYDVQQGEwJJTjESMBAGA1UEChMJSW5kaWEgUEtJMRcwFQYDVQQDEw5DQ0EgSW5kaWEgMjAxNDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAN7IUL2K/yINrn+sglna9CkJ1AVrbJYBvsylsCF3vhStQC9kb7t4FwX7s+6AAMSakL5GUDJxVVNhMqf/2paerAzFACVNR1AiMLsG7ima4pCDhFn7t9052BQRbLBCPg4wekx6j+QULQFeW9ViLV7hjkEhKffeuoc3YaDmkkPSmA2mz6QKbUWYUu4PqQPRCrkiDH0ikdqR9eyYhWyuI7Gm/pc0atYnp1sru3rtLCaLS0ST/N/ELDEUUY2wgxglgoqEEdMhSSBL1CzaA8Ck9PErpnqC7VL+sbSyAKeJ9n56FttQzkwYjdOHMrgJRZaPb2i5VoVo1ZFkQF3ZKfiJ25VH5+8CAwEAAaMzMDEwDwYDVR0TAQH/BAUwAwEB/zARBgNVHQ4ECgQIQrjFz22zV+EwCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBCwUAA4IBAQAdAUjv0myKyt8GC1niIZplrlksOWIR6yXLg4BhFj4ziULxsGK4Jj0sIJGCkNJeHl+Ng9UlU5EI+r89DRdrGBTF/I+g3RHcViPtOne9xEgWRMRYtWD7QZe5FvoSSGkW9aV6D4iGLPBQML6FDUkQzW9CYDCFgGC2+awRMx61dQVXiFv3Nbkqa1Pejcel8NMAmxjfm5nZMd3Ft13hy3fNF6UzsOnBtMbyZWhS8Koj2KF";

        byte[] b = DocSignature.getBytes(Charset.forName("UTF-8"));
        //   byte[] b1 = toVerify.getBytes();
        //byte[] b = string.getBytes(StandardCharsets.UTF_8);

        // System.out.println("byteesssssssssssssbb" + b);


        byte[] dataBytes = Base64.encodeBase64(DocSignature.getBytes());

        //   System.out.println("byteesssssssssssss" + dataBytes);

        byte[] pkcs7Bytes = DocSignature.getBytes();

        System.out.println("pkcs7Bytes" + pkcs7Bytes);

        ASN1InputStream asn1is = new ASN1InputStream(pkcs7Bytes);

        System.out.println("asn1is" + asn1is);

        ContentInfo pkcs7Info = ContentInfo.getInstance(asn1is.readObject());

        System.out.println("pkcs7Info" + pkcs7Info);

        SignedData signedData = SignedData.getInstance(pkcs7Info.getContent());

        System.out.println("signedData" + signedData);

        CMSSignedData cmsSignedData = new CMSSignedData(pkcs7Bytes);
        SignerInformationStore signers = cmsSignedData.getSignerInfos();

//        CMSSignedData s = new CMSSignedData(dataBytes);
//        SignerInformationStore signers = s.getSignerInfos();
        SignerInformation signerInfo = (SignerInformation)signers.getSigners().iterator().next();

        System.out.println("ssssssssssssssssssssssss" + signerInfo);


        FileInputStream fis = new FileInputStream("/var/dummyCer.cer");
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        X509Certificate cert = (X509Certificate)cf.generateCertificates(fis).iterator().next();

        boolean result = signerInfo.verify(new JcaSimpleSignerInfoVerifierBuilder().setProvider("SUN").build(cert.getPublicKey()));
        System.out.println("Verified: "+result);




        return DocSignature;



    }


    public byte[] createSignature() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, SignatureException, UnrecoverableKeyException, InvalidKeyException {

        String STORENAME = "/var/DocSignerpkcs.p7b";
        String STOREPASS = "12345678";

        //First load the keystore object by providing the p12 file path
        KeyStore clientStore = KeyStore.getInstance("PKCS7");
        //replace testPass with the p12 password/pin
        clientStore.load(new FileInputStream(STORENAME), STOREPASS.toCharArray());

        Enumeration<String> aliases = clientStore.aliases();
        String aliaz = "";
        while(aliases.hasMoreElements()){
            aliaz = aliases.nextElement();
            if(clientStore.isKeyEntry(aliaz)){
                break;
            }
        }
        X509Certificate c = (X509Certificate)clientStore.getCertificate(aliaz);

        //Data to sign
        byte[] dataToSign = "SigmaWorld".getBytes();
        //compute signature:
        Signature signature = Signature.getInstance("Sha2WithRSA");
        signature.initSign((PrivateKey)clientStore.getKey(aliaz, STOREPASS.toCharArray()));
        signature.update(dataToSign);
        byte[] signedData = signature.sign();

        //load X500Name
        X500Name xName      = X500Name.asX500Name(c.getSubjectX500Principal());
        //load serial number
        BigInteger serial   = c.getSerialNumber();
        //laod digest algorithm
        AlgorithmId digestAlgorithmId = new AlgorithmId(AlgorithmId.SHA_oid);
        //load signing algorithm
        AlgorithmId signAlgorithmId = new AlgorithmId(AlgorithmId.RSAEncryption_oid);

        //Create SignerInfo:
        SignerInfo sInfo = new SignerInfo(xName, serial, digestAlgorithmId, signAlgorithmId, signedData);
        //Create ContentInfo:
        sun.security.pkcs.ContentInfo cInfo = new sun.security.pkcs.ContentInfo(sun.security.pkcs.ContentInfo.DIGESTED_DATA_OID, new DerValue(DerValue.tag_OctetString, dataToSign));
        //Create PKCS7 Signed data
        PKCS7 p7 = new PKCS7(new AlgorithmId[] { digestAlgorithmId }, cInfo,
            new X509Certificate[] { c },
            new SignerInfo[] { sInfo });
        //Write PKCS7 to bYteArray
        ByteArrayOutputStream bOut = new DerOutputStream();
        p7.encodeSignedData(bOut);
        byte[] encodedPKCS7 = bOut.toByteArray();

        System.out.println("pksc77777777777777777777777777777777777" + encodedPKCS7);

        return encodedPKCS7;

    }


    public void verifySignedData(byte[] signedData) throws CMSException, CertificateException, FileNotFoundException, NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {

//Corresponding class of signed_data is CMSSignedData

        CMSSignedData signature = new CMSSignedData(signedData);
        Store cs = signature.getCertificates();
        SignerInformationStore signers = signature.getSignerInfos();
        Collection c = signers.getSigners();
        Iterator it = c.iterator();

        //the following array will contain the content of xml document
        byte[] data = null;

        while (it.hasNext()) {
            SignerInformation signer = (SignerInformation) it.next();
            Collection certCollection = cs.getMatches(signer.getSID());
            Iterator certIt = certCollection.iterator();
            X509CertificateHolder cert = (X509CertificateHolder) certIt.next();

            CMSProcessable sc = signature.getSignedContent();
            data = (byte[]) sc.getContent();

            CertificateFactory certificatefactory = CertificateFactory.getInstance("X.509");

// Open the certificate file
            FileInputStream fileinputstream = new FileInputStream("var/DSCPublicKey.cer");

//get CA public key
            PublicKey pk = certificatefactory.generateCertificate(fileinputstream).getPublicKey();

            X509Certificate myCA = new JcaX509CertificateConverter().setProvider("BC").getCertificate(cert);

            myCA.verify(pk);
            System.out.println("Verfication done successfully ");

        }

    }


    public static byte[] extractData(byte[] pkcs7Data) {
        try {
            CMSSignedData cmsSignedData = new CMSSignedData(pkcs7Data);
            ByteArrayOutputStream bs = new ByteArrayOutputStream();
            cmsSignedData.getSignedContent().write(bs);
            return bs.toByteArray();
        } catch (Exception e) {}
        return new byte[0];
    }


    public boolean verifyEsignResponse(String EsignRespone) throws IOException, DocumentException {

        boolean verificationResult = false;

        try
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            Document signedDocument = dbf.newDocumentBuilder().parse(IOUtils.toInputStream(String.valueOf(EsignRespone), "UTF-8"));

            NodeList nl = signedDocument.getElementsByTagNameNS(XMLSignature.XMLNS, "Signatures");

            if (nl.getLength() == 0)
            {
                throw new IllegalArgumentException("Cannot find Signature element");
            }

            XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

            DOMValidateContext valContext = new DOMValidateContext(GetPublicKey(), nl.item(0));
            XMLSignature signature = fac.unmarshalXMLSignature(valContext);

            verificationResult = signature.validate(valContext);

        }
        catch (Exception e)
        {
            System.out.println("Error while verifying digital siganature" + e.getMessage());
            e.printStackTrace();
        }

        return verificationResult;





    }


    public String signPdf(String DocSignature) throws IOException, DocumentException {

        String signPdf = "";

        // Decoding string
        byte[] bytes_dStr1 = Base64.decodeBase64(DocSignature.getBytes("UTF8"));

        String dStr1 = new String(bytes_dStr1);



        // Creating the reader and the stamper

        String fileInputPath = "/var/input.pdf";
        String Outputfilepath = "/var/output.pdf";

        PdfReader readerpdf = new PdfReader(fileInputPath);
        OutputStream fout = new FileOutputStream(Outputfilepath);

        PdfStamper stamperpdf = PdfStamper.createSignature(readerpdf, fout, '\0');
        PdfSignatureAppearance appearance = stamperpdf.getSignatureAppearance();
        appearance.setReason("I Approve this file");
        appearance.setContact("7357695886");
        appearance.setLocation("Chandigarh");
        appearance.setSignatureCreator("CSC");

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, 5);
        appearance.setSignDate(cal);
        appearance.setCertificationLevel(PdfSignatureAppearance.NOT_CERTIFIED);
        appearance.setImage(null);
        appearance.setVisibleSignature(new com.itextpdf.text.Rectangle(36, 748, 144, 780), 1, "sig");
        SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date=appearance.getSignDate().getTime();
        appearance.setLayer2Text("Digitally Signed By CSC" + "\n"+"Reason: "+appearance.getReason()+"\n"+"Location: "+appearance.getLocation()+"\nSigned on "+ format.format(date));
        int contentEstimated = 8192;
        HashMap<PdfName, Integer> exc = new HashMap<>();
        exc.put(PdfName.CONTENTS, contentEstimated * 2 + 2);
        PdfSignature dic = new PdfSignature(PdfName.ADOBE_PPKLITE, PdfName.ADBE_PKCS7_DETACHED);
        dic.setReason(appearance.getReason());
        dic.setLocation(appearance.getLocation());
        dic.setSignatureCreator(appearance.getSignatureCreator());
        appearance.setCryptoDictionary(dic);
        appearance.preClose(exc);

        byte[] paddedSig = new byte[contentEstimated];
        byte[] PKCS7Response = Base64.decodeBase64(DocSignature.getBytes("UTF8"));

        System.out.println("PKCS7Responseeeeeeeeeeeeeeee" + PKCS7Response);

        System.arraycopy(PKCS7Response, 0, paddedSig, 0, PKCS7Response.length);
        PdfDictionary dic2 = new PdfDictionary();
        dic2.put(PdfName.CONTENTS, new PdfString(paddedSig).setHexWriting(true));

        System.out.println("PdfNameeeeeeeeeeeeee" + PdfName.CONTENTS);

        appearance.close(dic2);

        return  signPdf.toString();


    }



    public String createESignXML(String getDocInfo,String getDocUrl,String getDocHash,String gerSignerId,String getTxn,String getTs) throws ParserConfigurationException, TransformerException, IOException, NoSuchAlgorithmException, SAXException {



        String eSignXML = "";

        Date now = new Date();
        String format3 = new SimpleDateFormat("yyMMddHHmmss", Locale.ENGLISH).format(now);
        String randomNo = RandomStringUtils.randomNumeric(6);
        String txnId = format3 + randomNo;


        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;

        dBuilder = dbFactory.newDocumentBuilder();

        Document doc = dBuilder.newDocument();

        doc.setXmlStandalone(true);

        StringWriter writer = new StringWriter();

        Element rootElement = doc.createElement("Esign");

        doc.appendChild(rootElement);

        Attr ver = doc.createAttribute("ver");
        ver.setValue("3.2");
        rootElement.setAttributeNode(ver);

        Attr signerid = doc.createAttribute("signerid");
        signerid.setValue(gerSignerId);
        rootElement.setAttributeNode(signerid);

        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String formattedDate = myDateObj.format(myFormatObj);


        Attr ts = doc.createAttribute("ts");
        ts.setValue(getTs);
        rootElement.setAttributeNode(ts);

        Attr txn1 = doc.createAttribute("txn");
        txn1.setValue(userRequestedXmlTxn);
        rootElement.setAttributeNode(txn1);

        Attr maxWaitPeriod = doc.createAttribute("maxWaitPeriod");
        maxWaitPeriod.setValue("1440");
        rootElement.setAttributeNode(maxWaitPeriod);

        Attr aspId = doc.createAttribute("aspId");
        aspId.setValue("CSORG1000002");
        rootElement.setAttributeNode(aspId);

        Attr responseUrl = doc.createAttribute("responseUrl");
        responseUrl.setValue(ASP_RESPONSE_URL);
        rootElement.setAttributeNode(responseUrl);

        Attr redirectUrl = doc.createAttribute("redirectUrl");
        redirectUrl.setValue(ASP_REDIRECT_URL);
        rootElement.setAttributeNode(redirectUrl);

        Attr signingAlgorithm = doc.createAttribute("signingAlgorithm");
        signingAlgorithm.setValue("RSA");
        rootElement.setAttributeNode(signingAlgorithm);



        Element DocsinfoElement = doc.createElement("Docs");
        rootElement.appendChild(DocsinfoElement);



        Element InputHash = doc.createElement("InputHash");

        InputHash.appendChild(doc.createTextNode(getDocHash));


        Attr id = doc.createAttribute("id");
        id.setValue("1");
        InputHash.setAttributeNode(id);

        Attr hashAlgorithm = doc.createAttribute("hashAlgorithm");
        hashAlgorithm.setValue("SHA256");
        InputHash.setAttributeNode(hashAlgorithm);

        Attr docInfo = doc.createAttribute("docInfo");
        docInfo.setValue(getDocInfo);
        InputHash.setAttributeNode(docInfo);

        Attr docUrl = doc.createAttribute("docUrl");
        docUrl.setValue(getDocUrl);
        InputHash.setAttributeNode(docUrl);

        Attr responseSigType = doc.createAttribute("responseSigType");
        responseSigType.setValue("pkcs7");
        InputHash.setAttributeNode(responseSigType);

        DocsinfoElement.appendChild(InputHash);

        // create the xml file
        //transform the DOM Object to an XML File
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(doc);
        //StreamResult streamResult = new StreamResult(new File(xmlFilePath));

        // If you use
        StreamResult result = new StreamResult(writer);
        // the output will be pushed to the standard output ...
        // You can use that for debugging

        transformer.transform(domSource, result);

//           TransformerFactory transformerFactory = TransformerFactory.newInstance();

        eSignXML = writer.toString();




        return   eSignXML;
    }

    public String setCSCparamaters(String signedeSignXML, String txn, HttpServletResponse response) throws IOException {

        String txnRef = Base64.encodeBase64String((txn +"|"+ ASP_REDIRECT_URL).getBytes());
        String res_data = Base64.encodeBase64String(signedeSignXML.getBytes());
        return txnRef;
    }

    public  String decodeUserRequestXml(String req_data) throws IOException {

        System.out.println("d1234decodeUserRequestXml" + req_data);

        String decodedUserRequestXml = "";

        System.out.printf("req_data@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        System.out.printf(req_data);
        System.out.printf("req_data@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");


        byte[] decodedBytesForResXml = Base64.decodeBase64(req_data.getBytes());

        System.out.printf("decodedBytesForResXml@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        System.out.println(decodedBytesForResXml);
        System.out.printf("decodedBytesForResXml@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

         decodedUserRequestXml = new String(decodedBytesForResXml, StandardCharsets.UTF_8) ;

        System.out.printf("decodedUserRequestXml@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        System.out.printf(decodedUserRequestXml);
        System.out.printf("decodedUserRequestXml@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

        //Get Required Elements From User Requested XML
        return decodedUserRequestXml;

    }


    public void getUserXmlContents(String decodedUserRequestXml) throws ParserConfigurationException, IOException, SAXException {

        DocumentBuilderFactory df_eSignResponse = DocumentBuilderFactory.newInstance();
        Document doc_eSignResponse =
            df_eSignResponse.newDocumentBuilder().parse(IOUtils.toInputStream(String.valueOf(decodedUserRequestXml), "UTF-8"));
        doc_eSignResponse.setXmlStandalone(true);
        Element element_eSignResponse = (Element) doc_eSignResponse.getElementsByTagName("Esign").item(0);
        Element element_InputHash = (Element) doc_eSignResponse.getElementsByTagName("InputHash").item(0);

         userRequestedXmlDocHashHexa =  element_InputHash.getTextContent();

        userRequestedXmlDocInfo = element_InputHash.getAttribute("docInfo");
        userRequestedXmlDocUrl = element_InputHash.getAttribute("docUrl");
        userRequestedXmlDocInfo = element_InputHash.getAttribute("docInfo");
        userRequestedXmlTs =    element_eSignResponse.getAttribute("ts");
        userRequestedXmlTxn =    element_eSignResponse.getAttribute("txn");
        userRequestedXmlSignerId =    element_eSignResponse.getAttribute("signerid");
        getUserRequestedXmlRedirectUrl =    element_eSignResponse.getAttribute("redirectUrl");
        getUserRequestedXmlResponseUrl =    element_eSignResponse.getAttribute("responseUrl");




    }


    public String sendEsignStatus(String EsignStatusXML) throws IOException {

        String request = EsignStatusXML;

        URL url = new URL("http://117.255.216.162:8097/api/signer");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        // Set timeout as per needs
        connection.setConnectTimeout(20000);
        connection.setReadTimeout(20000);

        // Set DoOutput to true if you want to use URLConnection for output.
        // Default is false
        connection.setDoOutput(true);

        connection.setUseCaches(true);
        connection.setRequestMethod("POST");

        // Set Headers
//        connection.setRequestProperty("Accept", "application/xml");
        connection.setRequestProperty("Content-Type", "application/xml");
        connection.setRequestProperty("Cache-Control", "no-cache");
        connection.setRequestProperty("AspId","CSORG1000002");
        connection.setRequestProperty("Body", "xml");

        // Write XML
        OutputStream outputStream = connection.getOutputStream();
        byte[] b = request.getBytes("UTF-8");
        outputStream.write(b);
        outputStream.flush();
        outputStream.close();

        // Read XML
        InputStream inputStream = connection.getInputStream();
        byte[] res = new byte[2048];
        int i = 0;
        StringBuilder response = new StringBuilder();
        while ((i = inputStream.read(res)) != -1) {
            response.append(new String(res, 0, i));
        }
        inputStream.close();

        System.out.println("Response= " + response.toString());
        return response.toString();


    }


    public String signXML(String xmlDoc) throws ParserConfigurationException, TransformerException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IOException, UnrecoverableEntryException, KeyStoreException, MarshalException, XMLSignatureException, SAXException, CertificateException {
        // Create a DOM XMLSignatureFactory that will be used to generate the
        // enveloped signature
        XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

        // Create a Reference to the enveloped document (in this case we are
        // signing the whole document, so a URI of "" signifies that) and
        // also specify the SHA256 digest algorithm and the ENVELOPED Transform.
        Reference ref = fac.newReference
            ("", fac.newDigestMethod(DigestMethod.SHA256, null),
                Collections.singletonList
                    (fac.newTransform
                        (Transform.ENVELOPED, (TransformParameterSpec) null)),
                null, null);

        // Create the SignedInfo
        SignedInfo si = fac.newSignedInfo
            (fac.newCanonicalizationMethod
                    (CanonicalizationMethod.EXCLUSIVE,
                        (C14NMethodParameterSpec) null),
                fac.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", null),
                Collections.singletonList(ref));


        KeyStore p12 = KeyStore.getInstance("pkcs12");
        p12.load(new FileInputStream("/var/DocSigner.pfx"), "12345678".toCharArray());


        Enumeration e = p12.aliases();
        String alias = (String) e.nextElement();
        System.out.println("Alias certifikata:" + alias);
        Key privateKey = p12.getKey(alias, "12345678".toCharArray());



        KeyStore.PrivateKeyEntry keyEntry
            = (KeyStore.PrivateKeyEntry) p12.getEntry(alias, new KeyStore.PasswordProtection("12345678".toCharArray()));

        X509Certificate cert = (X509Certificate) keyEntry.getCertificate();

        KeyInfoFactory kif = fac.getKeyInfoFactory();

        System.out.println(cert.getSerialNumber());

        X509IssuerSerial x509IssuerSerial = kif.newX509IssuerSerial(cert.getSubjectX500Principal().getName(), cert.getSerialNumber());

        List x509Content = new ArrayList();
        System.out.println("ime: " + cert.getSubjectX500Principal().getName());
        x509Content.add(cert.getSubjectX500Principal().getName());
        x509Content.add(x509IssuerSerial);

        X509Data xd = kif.newX509Data(x509Content);
        KeyInfo ki = kif.newKeyInfo(Collections.singletonList(xd));
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document doc =
            dbf.newDocumentBuilder().parse(IOUtils.toInputStream(String.valueOf(xmlDoc), "UTF-8"));
        DOMSignContext dsc = new DOMSignContext(privateKey, doc.getDocumentElement());
        XMLSignature signature = fac.newXMLSignature(si, ki);
        signature.sign(dsc);
        String xmlSignatureOutput = signature.toString();

        String signedXMLObject =  writeXmlDocumentToXmlFile(doc);
        DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document doc1 =
            dbf1.newDocumentBuilder().parse(IOUtils.toInputStream(String.valueOf(signedXMLObject), "UTF-8"));
        doc1.setXmlStandalone(true);
        Element element = (Element) doc1.getElementsByTagName("KeyInfo").item(0);
        element.getParentNode().removeChild(element);
        doc1.normalize();

        String signedXMLObjectWithoutKeyInfo = writeXmlDocumentToXmlFile(doc1);

        return signedXMLObjectWithoutKeyInfo;


    }


//Hexa code start from here

    private static String checksum(String filepath, MessageDigest md) throws IOException {

        // file hashing with DigestInputStream
        try (DigestInputStream dis = new DigestInputStream(new FileInputStream(filepath), md)) {
            while (dis.read() != -1) ; //empty loop to clear the data
            md = dis.getMessageDigest();
        }

        // bytes to hex
        StringBuilder result = new StringBuilder();
        for (byte b : md.digest()) {
            result.append(String.format("%02x", b));       }



        return result.toString();

    }


    private String  writeXmlDocumentToXmlFile(Document xmlDocument)
    {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        String signedXMLObject ="";
        try {
            transformer = tf.newTransformer();

            // Uncomment if you do not require XML declaration
            // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            //A character stream that collects its output in a string buffer,
            //which can then be used to construct a string.
            StringWriter writer = new StringWriter();

            //transform document to string
            transformer.transform(new DOMSource(xmlDocument), new StreamResult(writer));

            String xmlString = writer.getBuffer().toString();
            System.out.println(xmlString);
            signedXMLObject = xmlString;
            //Print to console or logs
        }
        catch (TransformerException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return signedXMLObject;
    }

    public String saveTransaction(String signedeSignXML, EsignRequestDTO esignRequestDTO) throws ParserConfigurationException, IOException, SAXException {


        DocumentBuilderFactory df_eSignRequest= DocumentBuilderFactory.newInstance();
        Document doc_eSignRequest =
            df_eSignRequest.newDocumentBuilder().parse(IOUtils.toInputStream(String.valueOf(signedeSignXML), "UTF-8"));
        doc_eSignRequest.setXmlStandalone(true);
        Element element_eSignRequest = (Element) doc_eSignRequest.getElementsByTagName("Esign").item(0);
        String txn_eSignRequest =    element_eSignRequest.getAttribute("txn");

        String ts = element_eSignRequest.getAttribute("ts");

        EsignTransation esignTransation = new EsignTransation();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        esignTransation.setTxnId(txn_eSignRequest);
        esignTransation.setUserCodeId("USER_000011");
        esignTransation.setTs(ts);
        //esignTransation.setRequestXml(signedeSignXML);
        esignTransation.setRequestType("Esign Request");
        esignTransation.setRequestStatus("Create");

        esignTransationRepository.save(esignTransation);
        esignTransationSearchRepository.save(esignTransation);

        return esignTransation.toString();

    }


    public boolean verify(String signedOrgKycXML)
    {

        boolean verificationResult = false;

        try
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            Document signedDocument = dbf.newDocumentBuilder().parse(IOUtils.toInputStream(String.valueOf(signedOrgKycXML), "UTF-8"));

            NodeList nl = signedDocument.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");

            if (nl.getLength() == 0)
            {
                throw new IllegalArgumentException("Cannot find Signature element");
            }

            XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

            DOMValidateContext valContext = new DOMValidateContext(GetPublicKey(), nl.item(0));
            XMLSignature signature = fac.unmarshalXMLSignature(valContext);

            verificationResult = signature.validate(valContext);

        }
        catch (Exception e)
        {
            System.out.println("Error while verifying digital siganature" + e.getMessage());
            e.printStackTrace();
        }

        return verificationResult;
    }

    public static PublicKey GetPublicKey() throws FileNotFoundException, KeyStoreException, CertificateException {

        PublicKey publicKey = null;
        FileInputStream is = null;

        try
        {
            is = new FileInputStream("/var/DSCPublicKey.cer");
//            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
//            keystore.load(is, "12345678".toCharArray());
//            String alias = "Alias";
//            Key key = keystore.getKey(alias, "password".toCharArray());
//            if (key instanceof PrivateKey)
//            {

//                 Get certificate of public key
//                java.security.Certificate cert = (java.security.Certificate) keystore.getCertificate(alias);
            CertificateFactory fact = CertificateFactory.getInstance("X.509");
            X509Certificate cert =  (X509Certificate) fact.generateCertificate(is);

            // Get public key
            publicKey = cert.getPublicKey();

//            }
        }
        catch (FileNotFoundException ex)
        {
        }
        catch ( IOException ex)
        {
        }
        finally
        {
            try
            {
                is.close();
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }


        return publicKey;

    }

}

