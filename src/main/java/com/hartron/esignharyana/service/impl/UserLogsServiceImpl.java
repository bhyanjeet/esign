package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.service.UserLogsService;
import com.hartron.esignharyana.domain.UserLogs;
import com.hartron.esignharyana.repository.UserLogsRepository;
import com.hartron.esignharyana.repository.search.UserLogsSearchRepository;
import com.hartron.esignharyana.service.dto.UserLogsDTO;
import com.hartron.esignharyana.service.mapper.UserLogsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link UserLogs}.
 */
@Service
@Transactional
public class UserLogsServiceImpl implements UserLogsService {

    private final Logger log = LoggerFactory.getLogger(UserLogsServiceImpl.class);

    private final UserLogsRepository userLogsRepository;

    private final UserLogsMapper userLogsMapper;

    private final UserLogsSearchRepository userLogsSearchRepository;

    public UserLogsServiceImpl(UserLogsRepository userLogsRepository, UserLogsMapper userLogsMapper, UserLogsSearchRepository userLogsSearchRepository) {
        this.userLogsRepository = userLogsRepository;
        this.userLogsMapper = userLogsMapper;
        this.userLogsSearchRepository = userLogsSearchRepository;
    }

    /**
     * Save a userLogs.
     *
     * @param userLogsDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UserLogsDTO save(UserLogsDTO userLogsDTO) {
        log.debug("Request to save UserLogs : {}", userLogsDTO);
        UserLogs userLogs = userLogsMapper.toEntity(userLogsDTO);
        userLogs = userLogsRepository.save(userLogs);
        UserLogsDTO result = userLogsMapper.toDto(userLogs);
        userLogsSearchRepository.save(userLogs);
        return result;
    }

    /**
     * Get all the userLogs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserLogsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserLogs");
        return userLogsRepository.findAll(pageable)
            .map(userLogsMapper::toDto);
    }


    /**
     * Get one userLogs by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserLogsDTO> findOne(Long id) {
        log.debug("Request to get UserLogs : {}", id);
        return userLogsRepository.findById(id)
            .map(userLogsMapper::toDto);
    }

    /**
     * Delete the userLogs by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserLogs : {}", id);
        userLogsRepository.deleteById(id);
        userLogsSearchRepository.deleteById(id);
    }

    /**
     * Search for the userLogs corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserLogsDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of UserLogs for query {}", query);
        return userLogsSearchRepository.search(queryStringQuery(query), pageable)
            .map(userLogsMapper::toDto);
    }
}
