package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.service.OtpService;
import com.hartron.esignharyana.domain.Otp;
import com.hartron.esignharyana.repository.OtpRepository;
import com.hartron.esignharyana.repository.search.OtpSearchRepository;
import com.hartron.esignharyana.service.OtpSmsService;
import com.hartron.esignharyana.service.dto.OtpDTO;
import com.hartron.esignharyana.service.mapper.OtpMapper;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Otp}.
 */
@Service
@Transactional
public class OtpServiceImpl implements OtpService {

    private final Logger log = LoggerFactory.getLogger(OtpServiceImpl.class);

    private final OtpRepository otpRepository;

    private final OtpMapper otpMapper;

    private final OtpSearchRepository otpSearchRepository;

    private final OtpSmsService otpSmsService;

    public OtpServiceImpl(OtpRepository otpRepository, OtpMapper otpMapper, OtpSearchRepository otpSearchRepository, OtpSmsService otpSmsService) {
        this.otpRepository = otpRepository;
        this.otpMapper = otpMapper;
        this.otpSearchRepository = otpSearchRepository;
        this.otpSmsService = otpSmsService;
    }

    /**
     * Save a otp.
     *
     * @param otpDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OtpDTO save(OtpDTO otpDTO) {
        log.debug("Request to save Otp : {}", otpDTO);
        Otp otp = otpMapper.toEntity(otpDTO);
        otp = otpRepository.save(otp);
        OtpDTO result = otpMapper.toDto(otp);
        otpSearchRepository.save(otp);
        return result;
    }

    /**
     * Get all the otps.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<OtpDTO> findAll() {
        log.debug("Request to get all Otps");
        return otpRepository.findAll().stream()
            .map(otpMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one otp by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OtpDTO> findOne(Long id) {
        log.debug("Request to get Otp : {}", id);
        return otpRepository.findById(id)
            .map(otpMapper::toDto);
    }

    /**
     * Delete the otp by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Otp : {}", id);
        otpRepository.deleteById(id);
        otpSearchRepository.deleteById(id);
    }

    /**
     * Search for the otp corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<OtpDTO> search(String query) {
        log.debug("Request to search Otps for query {}", query);
        return StreamSupport
            .stream(otpSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(otpMapper::toDto)
            .collect(Collectors.toList());
    }

    @Override
    public  Boolean sendOtpByMobile(Long mobileNumber) {
        Optional<Otp> otpOld = otpSearchRepository.findOtpByMobileNumber(mobileNumber);
        otpOld.ifPresent(otp -> otpRepository.deleteById(otp.getId()));

        String OTP = RandomStringUtils.randomNumeric(6);
        Otp otp = new Otp();
        otp.setOtp(Integer.valueOf(OTP));
        otp.setMobileNumber(mobileNumber);
        otp.setRequestTime(ZonedDateTime.now());

        JSONObject object = new JSONObject();
        if (otp.getOtp() != null) {
            String content = otp.getOtp() + " " + "is your OTP for ESign verification. This OTP is valid for 10 minutes";
            try {
                otpSmsService.sendSingleSMS(String.valueOf(otp.getMobileNumber()), content);
            } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            otpRepository.save(otp);
            try {
                object.put("sending", true);
            }catch (Exception e) {
                e.getStackTrace();
            }
            return true;
        } else {
            try {
                object.put("not sending", false);
            }catch (Exception e) {
                e.getStackTrace();
            }
            return false;
        }
    }

    @Override
    public  Boolean verifyOTPByMobile(Long mobileNumber, Integer OTP)  {

        Optional<Otp> otp = otpRepository.findOtpByMobileNumber(mobileNumber);
        JSONObject object = new JSONObject();
        if(otp.get().getOtp().equals(OTP) && ZonedDateTime.now().isBefore(otp.get().getRequestTime().plusSeconds(600))){
            try {
                object.put("verified" , true);
                otpRepository.deleteById(otp.get().getId());
            }catch (Exception e) {
                e.getStackTrace();
            }
            return true;
        } else if(otp.get().getOtp().equals(OTP) && ZonedDateTime.now().isAfter(otp.get().getRequestTime().plusSeconds(600))) {
            try {
                object.put("time out" , false);
                otpRepository.deleteById(otp.get().getId());
            }catch (Exception e) {
                e.getStackTrace();
            }
            return false;
        }
        else{
            try {
                object.put("not verified" , false);

            }catch (Exception e) {
                e.getStackTrace();
            }
            return false;
        }
    }
}
