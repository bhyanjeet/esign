package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.domain.ApplicationLogs;
import com.hartron.esignharyana.domain.OrganisationMaster;
import com.hartron.esignharyana.domain.User;
import com.hartron.esignharyana.repository.ApplicationLogsRepository;
import com.hartron.esignharyana.repository.search.ApplicationLogsSearchRepository;
import com.hartron.esignharyana.repository.search.OrganisationMasterSearchRepository;
import com.hartron.esignharyana.security.SecurityUtils;
import com.hartron.esignharyana.service.ApplicationLogsService;
import com.hartron.esignharyana.service.ApplicationMasterService;
import com.hartron.esignharyana.domain.ApplicationMaster;
import com.hartron.esignharyana.repository.ApplicationMasterRepository;
import com.hartron.esignharyana.repository.search.ApplicationMasterSearchRepository;
import com.hartron.esignharyana.service.UserService;
import com.hartron.esignharyana.service.dto.ApplicationMasterDTO;
import com.hartron.esignharyana.service.mapper.ApplicationMasterMapper;
import com.hartron.esignharyana.service.mapper.OrganisationMasterMapper;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link ApplicationMaster}.
 */
@Service
@Transactional
public class ApplicationMasterServiceImpl implements ApplicationMasterService {

    private final Logger log = LoggerFactory.getLogger(ApplicationMasterServiceImpl.class);

    private final ApplicationMasterRepository applicationMasterRepository;

    private final ApplicationMasterMapper applicationMasterMapper;

    private final ApplicationMasterSearchRepository applicationMasterSearchRepository;

    private final OrganisationMasterMapper organisationMasterMapper;

    private final OrganisationMasterSearchRepository organisationMasterSearchRepository;

    private final UserService userService;

    private final ApplicationLogsService applicationLogsService;

    private final ApplicationLogsRepository applicationLogsRepository;

    private final ApplicationLogsSearchRepository applicationLogsSearchRepository;


    public ApplicationMasterServiceImpl(ApplicationMasterRepository applicationMasterRepository, ApplicationMasterMapper applicationMasterMapper, ApplicationMasterSearchRepository applicationMasterSearchRepository, OrganisationMasterMapper organisationMasterMapper, OrganisationMasterSearchRepository organisationMasterSearchRepository, UserService userService, ApplicationLogsService applicationLogsService, ApplicationLogsRepository applicationLogsRepository, ApplicationLogsSearchRepository applicationLogsSearchRepository) {
        this.applicationMasterRepository = applicationMasterRepository;
        this.applicationMasterMapper = applicationMasterMapper;
        this.applicationMasterSearchRepository = applicationMasterSearchRepository;
        this.organisationMasterMapper = organisationMasterMapper;
        this.organisationMasterSearchRepository = organisationMasterSearchRepository;
        this.userService = userService;
        this.applicationLogsService = applicationLogsService;
        this.applicationLogsRepository = applicationLogsRepository;
        this.applicationLogsSearchRepository = applicationLogsSearchRepository;
    }

    /**
     * Save a applicationMaster.
     *
     * @param applicationMasterDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ApplicationMasterDTO save(ApplicationMasterDTO applicationMasterDTO) {
        ApplicationLogs applicationLogs = new ApplicationLogs();
        log.debug("Request to save ApplicationMaster : {}", applicationMasterDTO);
        Optional<User> user = userService.getUserWithAuthorities();
        if (applicationMasterDTO.getId() == null) {

                String login = user.get().getLogin();
                Optional<OrganisationMaster> organisation = organisationMasterSearchRepository.findByCreatedBy(login);
                Long loginId = organisation.get().getId();
                applicationMasterDTO.setOrganisationMasterId(loginId);
                applicationMasterDTO.setCreatedBy(login);
                applicationMasterDTO.setCreatedOn(LocalDate.now());
                applicationMasterDTO.setStatus("Pending");
                Long totalCount = applicationMasterRepository.count();
                totalCount = totalCount+1;
                if(totalCount<=99999){
                    String formatedNumber = "APP_" + String.format("%06d", totalCount);
                    applicationMasterDTO.setApplicationIdCode(formatedNumber);
                } else {
                    applicationMasterDTO.setApplicationIdCode("APP_" + totalCount);
                }

                applicationLogs.setActionTaken("created");
                applicationLogs.setRemarks("Application created");
                applicationLogs.setActionTakenBy(login);
                applicationLogs.setActionTakenOnDate(LocalDate.now());

            Optional<ApplicationMaster> findName = applicationMasterRepository.findByApplicationName(applicationMasterDTO.getApplicationName());
            if (findName.isPresent()){
                throw new BadRequestAlertException("Application Allready Exists!", "applicationMaster", "invalidApplicationName");
            }
        }
        else {
            String status  = "Pending";
            applicationMasterDTO.setLastUpdatedBy(SecurityUtils.getCurrentUserLogin().get());
            applicationMasterDTO.setLastUpdatedOn(LocalDate.now());
            applicationLogs.setActionTakenBy(SecurityUtils.getCurrentUserLogin().get());
            applicationLogs.setActionTakenOnDate(LocalDate.now());
            applicationLogs.setRemarks(applicationMasterDTO.getRemarks());
            if (applicationMasterDTO.getStatus().equals(status)){
                applicationLogs.setActionTaken("Updated");
            }
            else {
                applicationLogs.setActionTaken(applicationMasterDTO.getStatus());
            }
        }
        ApplicationMaster applicationMaster = applicationMasterMapper.toEntity(applicationMasterDTO);

        applicationMaster = applicationMasterRepository.save(applicationMaster);
        ApplicationMasterDTO result = applicationMasterMapper.toDto(applicationMaster);
        applicationMaster =  applicationMasterSearchRepository.save(applicationMaster);
        applicationLogs.setActionTakenOnApplication(applicationMaster.getId());
        applicationLogsRepository.save(applicationLogs);
        applicationLogsSearchRepository.save(applicationLogs);

        Long applicationId = applicationMaster.getId();


        return result;
    }

    /**
     * Get all the applicationMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ApplicationMasterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ApplicationMasters");
        return applicationMasterRepository.findAll(pageable)
            .map(applicationMasterMapper::toDto);
    }


    /**
     * Get one applicationMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ApplicationMasterDTO> findOne(Long id) {
        log.debug("Request to get ApplicationMaster : {}", id);
        return applicationMasterRepository.findById(id)
            .map(applicationMasterMapper::toDto);
    }

    /**
     * Delete the applicationMaster by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ApplicationMaster : {}", id);
        applicationMasterRepository.deleteById(id);
        applicationMasterSearchRepository.deleteById(id);
    }

    /**
     * Search for the applicationMaster corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ApplicationMasterDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ApplicationMasters for query {}", query);
        return applicationMasterSearchRepository.search(queryStringQuery(query), pageable)
            .map(applicationMasterMapper::toDto);
    }

    public List<ApplicationMasterDTO> findApplicationByUserLogin(String userLogin) {
        return applicationMasterSearchRepository.findByCreatedBy(userLogin).stream().map(applicationMasterMapper::toDto).collect(Collectors.toList());
    }

    public List<ApplicationMasterDTO> findApplicationRecordByStatus(String status) {
        return applicationMasterSearchRepository.findApplicationByStatus(status, PageRequest.of(0, 100)).stream().map(applicationMasterMapper::toDto).collect(Collectors.toList());
    }

    public List<ApplicationMasterDTO> findAllRecordByCreatedByAndStatus(String createdBy, String status){
        return applicationMasterSearchRepository.findAllByCreatedByAndStatus(createdBy,status).stream().map(applicationMasterMapper::toDto).collect(Collectors.toList());
    }
}
