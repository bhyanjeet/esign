package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.config.Constants;
import com.hartron.esignharyana.domain.Authority;
import com.hartron.esignharyana.domain.OrganisationLog;
import com.hartron.esignharyana.domain.OrganisationMaster;
import com.hartron.esignharyana.domain.User;
import com.hartron.esignharyana.repository.DepartmentRepository;
import com.hartron.esignharyana.repository.OrganisationLogRepository;
import com.hartron.esignharyana.repository.OrganisationMasterRepository;
import com.hartron.esignharyana.repository.UserRepository;
import com.hartron.esignharyana.repository.search.OrganisationLogSearchRepository;
import com.hartron.esignharyana.repository.search.OrganisationMasterSearchRepository;
import com.hartron.esignharyana.repository.search.UserSearchRepository;
import com.hartron.esignharyana.security.AuthoritiesConstants;
import com.hartron.esignharyana.security.SecurityUtils;
import com.hartron.esignharyana.service.*;
import com.hartron.esignharyana.service.dto.OrganisationMasterDTO;
import com.hartron.esignharyana.service.mapper.OrganisationMasterMapper;
import com.hartron.esignharyana.service.util.RandomUtil;
import com.hartron.esignharyana.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link OrganisationMaster}.
 */
@Service
@Transactional
public class OrganisationMasterServiceImpl implements OrganisationMasterService {

    private final Logger log = LoggerFactory.getLogger(OrganisationMasterServiceImpl.class);

    private final OrganisationMasterRepository organisationMasterRepository;

    private final OrganisationMasterMapper organisationMasterMapper;

    private final OrganisationMasterSearchRepository organisationMasterSearchRepository;

    private final OrganisationLogService organisationLogService;

    private final UserService userService;

    private final UserRepository userRepository;

    private final UserSearchRepository userSearchRepository;

    private final OrganisationLogRepository organisationLogRepository;

    private final OrganisationLogSearchRepository organisationLogSearchRepository;

    private final DepartmentRepository departmentRepository;

    private final PasswordEncoder passwordEncoder;

    private final MailService mailService;

    public OrganisationMasterServiceImpl(OrganisationMasterRepository organisationMasterRepository, OrganisationMasterMapper organisationMasterMapper, OrganisationMasterSearchRepository organisationMasterSearchRepository, OrganisationLogService organisationLogService, UserService userService, UserRepository userRepository, UserSearchRepository userSearchRepository, OrganisationLogRepository organisationLogRepository, OrganisationLogSearchRepository organisationLogSearchRepository, DepartmentRepository departmentRepository, PasswordEncoder passwordEncoder, MailService mailService) {
        this.organisationMasterRepository = organisationMasterRepository;
        this.organisationMasterMapper = organisationMasterMapper;
        this.organisationMasterSearchRepository = organisationMasterSearchRepository;
        this.organisationLogService = organisationLogService;
        this.userService = userService;
        this.userRepository = userRepository;
        this.userSearchRepository = userSearchRepository;
        this.organisationLogRepository = organisationLogRepository;
        this.organisationLogSearchRepository = organisationLogSearchRepository;
        this.departmentRepository = departmentRepository;
        this.passwordEncoder = passwordEncoder;
        this.mailService = mailService;
    }

    /**
     * Save a organisationMaster.
     *
     * @param organisationMasterDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OrganisationMasterDTO save(OrganisationMasterDTO organisationMasterDTO) {
        log.debug("Request to save OrganisationMaster : {}", organisationMasterDTO);
        OrganisationLog organisationLog = new OrganisationLog();
        Optional<User> user = userService.getUserWithAuthorities();
        if (organisationMasterDTO.getId() == null) {
            organisationMasterDTO.setCreatedBy(user.get().getLogin());
            organisationMasterDTO.setCreatedOn(LocalDate.now());
            organisationMasterDTO.setStatus("Pending");

            organisationLog.setCreatedByLogin(user.get().getLogin());
            organisationLog.setCreatedDate(ZonedDateTime.now());
            organisationLog.setStatus("Created");
            organisationLog.setRemarks("Organisation Created");

            Long totalCount = new Long(0);

            List<OrganisationMaster> list = organisationMasterRepository.findAll();

            if (!list.isEmpty()) {

                OrganisationMaster lastRecord = list.get(list.size() - 1);

                Long lastId = lastRecord.getId();

                totalCount = lastId;

                totalCount = totalCount + 1;
            }else {

                totalCount = new  Long(1);



            }

            if(totalCount<=99999){
                String formatedNumber = "DEP" + String.format("%06d", totalCount);
               organisationMasterDTO.setOrganisationIdCode(formatedNumber);
            } else {
               organisationMasterDTO.setOrganisationIdCode("DEP" + totalCount);
           }

            Optional<OrganisationMaster> findName = organisationMasterRepository.findByOrganisationName(organisationMasterDTO.getOrganisationName());
            if (findName.isPresent()){
                throw new BadRequestAlertException("Organisation Allready Exists!", "organisationMaster", "invalidOrganisationName");
            }
            Optional<OrganisationMaster> findEmail = organisationMasterRepository.findByOrganisationCorrespondenceEmail(organisationMasterDTO.getOrganisationCorrespondenceEmail());
            if (findEmail.isPresent()){
                throw new BadRequestAlertException("Organisation Correspondence Email Allready Exists!", "organisationMaster", "invalidOfficialEmail");
            }
            Optional<OrganisationMaster> findOfficialEmail = organisationMasterRepository.findByNodalOfficerOfficialEmail(organisationMasterDTO.getNodalOfficerOfficialEmail());
            if (findOfficialEmail.isPresent()){
                throw new BadRequestAlertException("Nodal Officer's Official Email Allready Exists!", "organisationMaster", "invalidOfficialEmail");
            }
            Optional<OrganisationMaster> correspondencePhone = organisationMasterRepository.findByOrganisationCorrespondencePhone1(organisationMasterDTO.getOrganisationCorrespondencePhone1());
            if (correspondencePhone.isPresent()){
                throw new BadRequestAlertException("Mobile Number Allready Exists!", "organisationMaster", "invalidMobileNumber");
            }
            Optional<OrganisationMaster> nodalMobile = organisationMasterRepository.findByOrganisationNodalOfficerPhoneMobile(organisationMasterDTO.getOrganisationNodalOfficerPhoneMobile());
            if (nodalMobile.isPresent()){
                throw new BadRequestAlertException("Nodal Officer's Mobile Number Allready Exists!", "organisationMaster", "invalidNodalOfficerMobileNumber");
            }

            Optional<OrganisationMaster> pan = organisationMasterRepository.findByPan(organisationMasterDTO.getPan());
            if (pan.isPresent()){
                throw new BadRequestAlertException("PAN Number Allready Exists!", "organisationMaster", "invalidPanNumber");
            }

            String pancard  = "";
            if (!organisationMasterDTO.getPanAttachment().isEmpty()){
                 pancard = organisationMasterDTO.getPanAttachment();
            }else {
                throw new BadRequestAlertException("Please Select Valid Pan Attachment file format !", "organisationMaster", "invalidPanAttachment");
            }

            if (!pancard.isEmpty()){
                String[] pancardLength = pancard.split("\\.");

                if (pancardLength.length>2){
                    throw new BadRequestAlertException("Invalid Pan Attachment file format !", "organisationMaster", "invalidPanAttachment");
                }

                String panType = pancardLength[1];

                if (panType.equals("jpg") || panType.equals("JPG") || panType.equals("PDF") || panType.equals("pdf")){
                }else {
                    throw new BadRequestAlertException("Invalid Pan Attachment file format !", "organisationMaster", "invalidPanAttachment");
                }
            }

            String EmpolyeeCardAttchment  = "";
            if (!organisationMasterDTO.getPanAttachment().isEmpty()){
                EmpolyeeCardAttchment = organisationMasterDTO.getPanAttachment();
            }else {
                throw new BadRequestAlertException("Please Select Valid Empolyee Card Attachment file format !", "organisationMaster", "invalidPanAttachment");
            }

            if (!EmpolyeeCardAttchment.isEmpty()){

                String[] EmpolyeeCardAttchmentLength = EmpolyeeCardAttchment.split("\\.");

                if (EmpolyeeCardAttchmentLength.length>2){
                    throw new BadRequestAlertException("Invalid Empolyee Card Attachment format !", "organisationMaster", "invalidEmpolyeeCardAttchment");
                }

                String EmpolyeeCardAttchmentType = EmpolyeeCardAttchmentLength[1];

                if (EmpolyeeCardAttchmentType.equals("jpg") || EmpolyeeCardAttchmentType.equals("JPG") || EmpolyeeCardAttchmentType.equals("PDF") || EmpolyeeCardAttchmentType.equals("pdf")){
                }else {
                    throw new BadRequestAlertException("Invalid Empolyee Card Attachment format !", "organisationMaster", "invalidEmpolyeeCardAttchment");
                }

            }

            String PhotographAttachment  = "";
            if (!organisationMasterDTO.getPanAttachment().isEmpty()){
                PhotographAttachment = organisationMasterDTO.getPanAttachment();
            }else {
                throw new BadRequestAlertException("Please Select Valid Empolyee Card Attachment file format !", "organisationMaster", "invalidPanAttachment");
            }

            if (!PhotographAttachment.isEmpty()){
                String[] PhotographAttachmentLength = PhotographAttachment.split("\\.");
                if (PhotographAttachmentLength.length>2){
                    throw new BadRequestAlertException("Invalid Photograph Attachment format !", "organisationMaster", "invalidPhotographAttachment");
                }

                String PhotographAttachmentType = PhotographAttachmentLength[1];

                if (PhotographAttachmentType.equals("jpg") || PhotographAttachmentType.equals("JPG")){
                }else {
                    throw new BadRequestAlertException("Invalid Photograph Attachment format !", "organisationMaster", "invalidPhotographAttachment");
                }

            }

            OrganisationMaster organisationMaster = organisationMasterMapper.toEntity(organisationMasterDTO);

            organisationMaster = organisationMasterRepository.save(organisationMaster);
            OrganisationMasterDTO result = organisationMasterMapper.toDto(organisationMaster);
            organisationMasterSearchRepository.save(organisationMaster);
            organisationLog.setOrganisationId(organisationMaster.getId());
            organisationLogRepository.save(organisationLog);
            organisationLogSearchRepository.save(organisationLog);

            User userNew = new User();
            userNew.setFirstName(organisationMaster.getOrganisationNodalOfficerName());
            userNew.setEmail(organisationMaster.getNodalOfficerOfficialEmail());
            userNew.setLangKey(Constants.DEFAULT_LANGUAGE);
            Authority authority = new Authority();
            authority.setName(AuthoritiesConstants.ORG_NODAL);

            Set<Authority> authorities = userNew.getAuthorities();
            authorities.add(authority);

            userNew.setAuthorities(authorities);
            userNew.setLogin(organisationMaster.getOrganisationIdCode());
            userNew.setActivated(true);
            String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
            userNew.setPassword(encryptedPassword);
            userNew.setActivated(true);
            userNew.setResetKey(RandomUtil.generateResetKey());
            userNew.setResetDate(Instant.now());

            userRepository.save(userNew);
            userSearchRepository.save(userNew);
            mailService.sendCreationEmail(userNew);

            return result;


        } else {
            String status = "Pending";
            organisationMasterDTO.setLastUpdatedBy(SecurityUtils.getCurrentUserLogin().get());
            organisationMasterDTO.setLastUpdatedOn(LocalDate.now());

            organisationLog.setCreatedDate(ZonedDateTime.now());
            organisationLog.setCreatedByLogin(SecurityUtils.getCurrentUserLogin().get());
            organisationLog.setRemarks(organisationMasterDTO.getRemarks());

            if (organisationMasterDTO.getStatus().equals(status)){
                organisationLog.setStatus("Updated");
            }else {
                organisationLog.setStatus(organisationMasterDTO.getStatus());
            }

            OrganisationMaster organisationMaster = organisationMasterMapper.toEntity(organisationMasterDTO);

            organisationMaster = organisationMasterRepository.save(organisationMaster);
            OrganisationMasterDTO result = organisationMasterMapper.toDto(organisationMaster);
            organisationMasterSearchRepository.save(organisationMaster);
            organisationLog.setOrganisationId(organisationMaster.getId());
            organisationLogRepository.save(organisationLog);
            organisationLogSearchRepository.save(organisationLog);

            Long organisationId = organisationMaster.getId();

            return result;

        }

        //Data set into Log table
//        organisationLogDTO.setOrganisationId(organisationMaster.getId());
//        if (organisationMasterDTO.getId() == null) {
//            organisationMasterDTO.setCreatedBy(user.getCreatedBy());
//            organisationMasterDTO.setCreatedOn(LocalDate.now());
//
//            organisationLogDTO.setCreatedByLogin(user.getCreatedBy());
//            organisationLogDTO.setCreatedByName(user.getFirstName());
//            organisationLogDTO.setCreatedDate(ZonedDateTime.now());
//
//        } else {
//            organisationMasterDTO.setLastUpdatedBy(user.getLogin());
//            organisationMasterDTO.setLastUpdatedOn(LocalDate.now());
//
//            organisationLogDTO.setVerifyByLogin(user.getLogin());
//            organisationLogDTO.setVerifyByName(user.getFirstName());
//            organisationLogDTO.setVerifyDate(ZonedDateTime.now());
//        }
//        organisationLogDTO.setRemarks(organisationMaster.getRemarks());

    }

    /**
     * Get all the organisationMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrganisationMasterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrganisationMasters");
        return organisationMasterRepository.findAll(pageable)
            .map(organisationMasterMapper::toDto);
    }


    /**
     * Get one organisationMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OrganisationMasterDTO> findOne(Long id) {
        log.debug("Request to get OrganisationMaster : {}", id);
        return organisationMasterRepository.findById(id)
            .map(organisationMasterMapper::toDto);
    }

    /**
     * Delete the organisationMaster by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrganisationMaster : {}", id);
        organisationMasterRepository.deleteById(id);
        organisationMasterSearchRepository.deleteById(id);
    }

    /**
     * Search for the organisationMaster corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrganisationMasterDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of OrganisationMasters for query {}", query);
        return organisationMasterSearchRepository.search(queryStringQuery(query), pageable)
            .map(organisationMasterMapper::toDto);
    }

    public List<OrganisationMasterDTO> findAllRecordByStatusAndVerifyBy(String status, String verifyBy){
        return organisationMasterSearchRepository.findAllByStatusAndVerifiedBy(status,verifyBy, PageRequest.of(0, 100)).stream().map(organisationMasterMapper::toDto).collect(Collectors.toList());
    }

    public List<OrganisationMasterDTO> findAllRecordByStatus(String status) {
        return organisationMasterSearchRepository.findAllByStatus(status,PageRequest.of(0, 100)).stream().map(organisationMasterMapper::toDto).collect(Collectors.toList());
    }

    public Optional<OrganisationMasterDTO> findOneByUserId(Integer userId) {
        return organisationMasterSearchRepository.findByUserId(userId)
            .map(organisationMasterMapper::toDto);
    }

    @Override
    public Optional<OrganisationMasterDTO> findOneByUserLogin(String userLogin) {
        return organisationMasterSearchRepository.findByCreatedBy(userLogin).map(organisationMasterMapper::toDto);
    }

    public Optional<OrganisationMasterDTO> findOrganisationByOranisationIdCode(String oranisationIdCode) {
        return organisationMasterSearchRepository.findOrganisationMasterByOrganisationIdCode(oranisationIdCode)
            .map(organisationMasterMapper::toDto);
    }

    @Override
    public List<OrganisationMasterDTO> findByOranisationIdCode(String oranisationIdCode) {
        return organisationMasterSearchRepository.findAllByOrganisationIdCode(oranisationIdCode).stream().map(organisationMasterMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public Optional<OrganisationMasterDTO> findOneById(Long id) {
        log.debug("Request to get OrganisationMaster : {}", id);
        Optional<OrganisationMasterDTO> organisationMasterDTO = organisationMasterRepository.findById(id).map(organisationMasterMapper::toDto);
        String currentUser = SecurityUtils.getCurrentUserLogin().get();
        Boolean currentRole = SecurityUtils.isCurrentUserInRole("ROLE_ORG_NODAL");
        String orgIdCode = organisationMasterDTO.get().getOrganisationIdCode();
        Optional<OrganisationMasterDTO> organisationMasterDTO1 = Optional.of(new OrganisationMasterDTO());

        if(currentRole) {

            if(currentUser.equalsIgnoreCase(orgIdCode))
            {
                organisationMasterDTO1 = organisationMasterDTO;

            }
        }
        else {
            organisationMasterDTO1 = organisationMasterDTO;
        }

        return organisationMasterDTO1;
    }
}
