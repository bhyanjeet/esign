package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.service.EsignErrorService;
import com.hartron.esignharyana.domain.EsignError;
import com.hartron.esignharyana.repository.EsignErrorRepository;
import com.hartron.esignharyana.repository.search.EsignErrorSearchRepository;
import com.hartron.esignharyana.service.dto.EsignErrorDTO;
import com.hartron.esignharyana.service.mapper.EsignErrorMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link EsignError}.
 */
@Service
@Transactional
public class EsignErrorServiceImpl implements EsignErrorService {

    private final Logger log = LoggerFactory.getLogger(EsignErrorServiceImpl.class);

    private final EsignErrorRepository esignErrorRepository;

    private final EsignErrorMapper esignErrorMapper;

    private final EsignErrorSearchRepository esignErrorSearchRepository;

    public EsignErrorServiceImpl(EsignErrorRepository esignErrorRepository, EsignErrorMapper esignErrorMapper, EsignErrorSearchRepository esignErrorSearchRepository) {
        this.esignErrorRepository = esignErrorRepository;
        this.esignErrorMapper = esignErrorMapper;
        this.esignErrorSearchRepository = esignErrorSearchRepository;
    }

    /**
     * Save a esignError.
     *
     * @param esignErrorDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EsignErrorDTO save(EsignErrorDTO esignErrorDTO) {
        log.debug("Request to save EsignError : {}", esignErrorDTO);
        EsignError esignError = esignErrorMapper.toEntity(esignErrorDTO);
        esignError = esignErrorRepository.save(esignError);
        EsignErrorDTO result = esignErrorMapper.toDto(esignError);
        esignErrorSearchRepository.save(esignError);
        return result;
    }

    /**
     * Get all the esignErrors.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<EsignErrorDTO> findAll() {
        log.debug("Request to get all EsignErrors");
        return esignErrorRepository.findAll().stream()
            .map(esignErrorMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one esignError by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EsignErrorDTO> findOne(Long id) {
        log.debug("Request to get EsignError : {}", id);
        return esignErrorRepository.findById(id)
            .map(esignErrorMapper::toDto);
    }

    /**
     * Delete the esignError by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EsignError : {}", id);
        esignErrorRepository.deleteById(id);
        esignErrorSearchRepository.deleteById(id);
    }

    /**
     * Search for the esignError corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<EsignErrorDTO> search(String query) {
        log.debug("Request to search EsignErrors for query {}", query);
        return StreamSupport
            .stream(esignErrorSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(esignErrorMapper::toDto)
            .collect(Collectors.toList());
    }
}
