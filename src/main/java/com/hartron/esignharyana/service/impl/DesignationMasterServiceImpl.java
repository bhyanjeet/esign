package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.service.DesignationMasterService;
import com.hartron.esignharyana.domain.DesignationMaster;
import com.hartron.esignharyana.repository.DesignationMasterRepository;
import com.hartron.esignharyana.repository.search.DesignationMasterSearchRepository;
import com.hartron.esignharyana.service.dto.DesignationMasterDTO;
import com.hartron.esignharyana.service.mapper.DesignationMasterMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link DesignationMaster}.
 */
@Service
@Transactional
public class DesignationMasterServiceImpl implements DesignationMasterService {

    private final Logger log = LoggerFactory.getLogger(DesignationMasterServiceImpl.class);

    private final DesignationMasterRepository designationMasterRepository;

    private final DesignationMasterMapper designationMasterMapper;

    private final DesignationMasterSearchRepository designationMasterSearchRepository;

    public DesignationMasterServiceImpl(DesignationMasterRepository designationMasterRepository, DesignationMasterMapper designationMasterMapper, DesignationMasterSearchRepository designationMasterSearchRepository) {
        this.designationMasterRepository = designationMasterRepository;
        this.designationMasterMapper = designationMasterMapper;
        this.designationMasterSearchRepository = designationMasterSearchRepository;
    }

    /**
     * Save a designationMaster.
     *
     * @param designationMasterDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DesignationMasterDTO save(DesignationMasterDTO designationMasterDTO) {
        log.debug("Request to save DesignationMaster : {}", designationMasterDTO);
        DesignationMaster designationMaster = designationMasterMapper.toEntity(designationMasterDTO);
        designationMaster = designationMasterRepository.save(designationMaster);
        DesignationMasterDTO result = designationMasterMapper.toDto(designationMaster);
        designationMasterSearchRepository.save(designationMaster);
        return result;
    }

    /**
     * Get all the designationMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DesignationMasterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DesignationMasters");
        return designationMasterRepository.findAll(pageable)
            .map(designationMasterMapper::toDto);
    }


    /**
     * Get one designationMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DesignationMasterDTO> findOne(Long id) {
        log.debug("Request to get DesignationMaster : {}", id);
        return designationMasterRepository.findById(id)
            .map(designationMasterMapper::toDto);
    }

    /**
     * Delete the designationMaster by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DesignationMaster : {}", id);
        designationMasterRepository.deleteById(id);
        designationMasterSearchRepository.deleteById(id);
    }

    /**
     * Search for the designationMaster corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DesignationMasterDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DesignationMasters for query {}", query);
        return designationMasterSearchRepository.search(queryStringQuery(query), pageable)
            .map(designationMasterMapper::toDto);
    }
}
