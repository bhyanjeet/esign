package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.service.OrganisationTypeMasterService;
import com.hartron.esignharyana.domain.OrganisationTypeMaster;
import com.hartron.esignharyana.repository.OrganisationTypeMasterRepository;
import com.hartron.esignharyana.repository.search.OrganisationTypeMasterSearchRepository;
import com.hartron.esignharyana.service.dto.OrganisationTypeMasterDTO;
import com.hartron.esignharyana.service.mapper.OrganisationTypeMasterMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link OrganisationTypeMaster}.
 */
@Service
@Transactional
public class OrganisationTypeMasterServiceImpl implements OrganisationTypeMasterService {

    private final Logger log = LoggerFactory.getLogger(OrganisationTypeMasterServiceImpl.class);

    private final OrganisationTypeMasterRepository organisationTypeMasterRepository;

    private final OrganisationTypeMasterMapper organisationTypeMasterMapper;

    private final OrganisationTypeMasterSearchRepository organisationTypeMasterSearchRepository;

    public OrganisationTypeMasterServiceImpl(OrganisationTypeMasterRepository organisationTypeMasterRepository, OrganisationTypeMasterMapper organisationTypeMasterMapper, OrganisationTypeMasterSearchRepository organisationTypeMasterSearchRepository) {
        this.organisationTypeMasterRepository = organisationTypeMasterRepository;
        this.organisationTypeMasterMapper = organisationTypeMasterMapper;
        this.organisationTypeMasterSearchRepository = organisationTypeMasterSearchRepository;
    }

    /**
     * Save a organisationTypeMaster.
     *
     * @param organisationTypeMasterDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OrganisationTypeMasterDTO save(OrganisationTypeMasterDTO organisationTypeMasterDTO) {
        log.debug("Request to save OrganisationTypeMaster : {}", organisationTypeMasterDTO);
        OrganisationTypeMaster organisationTypeMaster = organisationTypeMasterMapper.toEntity(organisationTypeMasterDTO);
        organisationTypeMaster = organisationTypeMasterRepository.save(organisationTypeMaster);
        OrganisationTypeMasterDTO result = organisationTypeMasterMapper.toDto(organisationTypeMaster);
        organisationTypeMasterSearchRepository.save(organisationTypeMaster);
        return result;
    }

    /**
     * Get all the organisationTypeMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrganisationTypeMasterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrganisationTypeMasters");
        return organisationTypeMasterRepository.findAll(pageable)
            .map(organisationTypeMasterMapper::toDto);
    }


    /**
     * Get one organisationTypeMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OrganisationTypeMasterDTO> findOne(Long id) {
        log.debug("Request to get OrganisationTypeMaster : {}", id);
        return organisationTypeMasterRepository.findById(id)
            .map(organisationTypeMasterMapper::toDto);
    }

    /**
     * Delete the organisationTypeMaster by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrganisationTypeMaster : {}", id);
        organisationTypeMasterRepository.deleteById(id);
        organisationTypeMasterSearchRepository.deleteById(id);
    }

    /**
     * Search for the organisationTypeMaster corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrganisationTypeMasterDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of OrganisationTypeMasters for query {}", query);
        return organisationTypeMasterSearchRepository.search(queryStringQuery(query), pageable)
            .map(organisationTypeMasterMapper::toDto);
    }
}
