package com.hartron.esignharyana.service.impl;

import com.hartron.esignharyana.domain.EsignTransation;
import com.hartron.esignharyana.repository.EsignTransationRepository;
import com.hartron.esignharyana.repository.search.EsignTransationSearchRepository;
import com.hartron.esignharyana.service.EsignTransationService;
import com.hartron.esignharyana.service.dto.EsignTransationDTO;
import com.hartron.esignharyana.service.mapper.EsignTransationMapper;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link EsignTransation}.
 */
@Service
@Transactional
public class EsignTransationServiceImpl implements EsignTransationService {

    private final Logger log = LoggerFactory.getLogger(EsignTransationServiceImpl.class);

    private final EsignTransationRepository esignTransationRepository;

    private final EsignTransationMapper esignTransationMapper;

    private final EsignTransationSearchRepository esignTransationSearchRepository;

    public EsignTransationServiceImpl(EsignTransationRepository esignTransationRepository, EsignTransationMapper esignTransationMapper, EsignTransationSearchRepository esignTransationSearchRepository) {
        this.esignTransationRepository = esignTransationRepository;
        this.esignTransationMapper = esignTransationMapper;
        this.esignTransationSearchRepository = esignTransationSearchRepository;
    }

    /**
     * Save a esignTransation.
     *
     * @param esignTransationDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EsignTransationDTO save(EsignTransationDTO esignTransationDTO) {
        log.debug("Request to save EsignTransation : {}", esignTransationDTO);
        EsignTransation esignTransation = esignTransationMapper.toEntity(esignTransationDTO);
        esignTransation = esignTransationRepository.save(esignTransation);
        EsignTransationDTO result = esignTransationMapper.toDto(esignTransation);
        esignTransationSearchRepository.save(esignTransation);
        return result;
    }

    /**
     * Get all the esignTransations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EsignTransationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EsignTransations");
        return esignTransationRepository.findAll(pageable)
            .map(esignTransationMapper::toDto);
    }


    /**
     * Get one esignTransation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EsignTransationDTO> findOne(Long id) {
        log.debug("Request to get EsignTransation : {}", id);
        return esignTransationRepository.findById(id)
            .map(esignTransationMapper::toDto);
    }

    /**
     * Delete the esignTransation by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EsignTransation : {}", id);
        esignTransationRepository.deleteById(id);
        esignTransationSearchRepository.deleteById(id);
    }

    /**
     * Search for the esignTransation corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EsignTransationDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of EsignTransations for query {}", query);
        return esignTransationSearchRepository.search(queryStringQuery(query), pageable)
            .map(esignTransationMapper::toDto);
    }

    @Override
    public Optional<EsignTransationDTO> findEsignTransactionByorganisationIdCode(String organisationIdCode) {
        return esignTransationSearchRepository.findEsignTransationByOrganisationIdCode(organisationIdCode)
            .map(esignTransationMapper::toDto);
    }

    public EsignTransationDTO saveTransaction(EsignTransationDTO esignTransationDTO) throws IOException, ParserConfigurationException, SAXException {

        EsignTransationDTO esignTransationDTO1 = new EsignTransationDTO();
        String userCodeId = esignTransationDTO.getUserCodeId();
        String applicationCodeId = esignTransationDTO.getApplicationCodeId();
        String reqdata =  esignTransationDTO.getReqData();

        String decodeEsignXml = decodeEsignXml(reqdata);


        System.out.println("oooooooooooooooooooooooooooooooo" + esignTransationDTO);
        System.out.println("appIdCodeeeeeeeeeeeee" + applicationCodeId);

        DocumentBuilderFactory df_eSignRequest= DocumentBuilderFactory.newInstance();
        Document doc_eSignRequest =
            df_eSignRequest.newDocumentBuilder().parse(IOUtils.toInputStream(String.valueOf(decodeEsignXml), "UTF-8"));
        doc_eSignRequest.setXmlStandalone(true);
        Element element_eSignRequest = (Element) doc_eSignRequest.getElementsByTagName("Esign").item(0);
        String txn_eSignRequest =    element_eSignRequest.getAttribute("txn");
        String signerId  =    element_eSignRequest.getAttribute("signerid");

        String ts = element_eSignRequest.getAttribute("ts");


        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");

        esignTransationDTO1.setTxnId(txn_eSignRequest);
        esignTransationDTO1.setUserCodeId(userCodeId);
        esignTransationDTO1.setApplicationCodeId(applicationCodeId);
        esignTransationDTO1.setTs(ts);
        esignTransationDTO1.setSignerId(signerId);
        //esignTransation.setRequestXml(signedeSignXML);
        esignTransationDTO1.setRequestType("Esign Request");
        esignTransationDTO1.setRequestStatus("Create");

        System.out.println("save_trnsactionnnnnnnnnnnnnnnnnnn" + esignTransationDTO1);
        EsignTransation esignTransation = esignTransationMapper.toEntity(esignTransationDTO1);
        esignTransationRepository.save(esignTransation);
        esignTransationSearchRepository.save(esignTransation);

        return esignTransationDTO1;

    }

    public String decodeEsignXml(String reqdata){
        Base64.Decoder decoder = Base64.getDecoder();
        // Decoding string
        String decodedXml  = new String(decoder.decode(reqdata));

        System.out.println("decodedEsignXmllllllllllllllllllll" + decodedXml);

        return decodedXml;
    }


}
