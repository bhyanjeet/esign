package com.hartron.esignharyana.service;

import java.io.IOException;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.ApplicationLogs}.
 */
public interface VerifyEsignXmlService {

    boolean verify(String xmlSignOutput);
}
