package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.ExternalUserEsignRequestDTO;
import com.itextpdf.text.DocumentException;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.UserDetails}.
 */
public interface EsignUserRequestService {

    //String checkEsignRequest(String userCodeId, String applicationCodeId, HttpServletResponse response, HttpServletRequest request) throws IOException;

    String  sendEsignRequestToCSC(String userCodeId, String applicationIdCode, String responseUrl, String txn, String req_data, HttpServletRequest request, HttpServletResponse response) throws NoSuchAlgorithmException, ParserConfigurationException, IOException, SAXException, TransformerException, XMLSignatureException, MarshalException, KeyStoreException, UnrecoverableEntryException, CertificateException, InvalidAlgorithmParameterException;

   ExternalUserEsignRequestDTO findByTxn(String txn) throws NoSuchAlgorithmException, ParserConfigurationException, IOException, SAXException, TransformerException, DocumentException, XMLSignatureException, MarshalException, KeyStoreException, UnrecoverableEntryException, CertificateException, InvalidAlgorithmParameterException;


    boolean verifySign(String xml);

}
