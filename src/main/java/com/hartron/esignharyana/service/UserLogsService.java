package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.UserLogsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.UserLogs}.
 */
public interface UserLogsService {

    /**
     * Save a userLogs.
     *
     * @param userLogsDTO the entity to save.
     * @return the persisted entity.
     */
    UserLogsDTO save(UserLogsDTO userLogsDTO);

    /**
     * Get all the userLogs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserLogsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" userLogs.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserLogsDTO> findOne(Long id);

    /**
     * Delete the "id" userLogs.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the userLogs corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserLogsDTO> search(String query, Pageable pageable);
}
