package com.hartron.esignharyana.service;

import com.hartron.esignharyana.domain.UserAuthDetails;
import com.hartron.esignharyana.service.dto.UserAuthDetailsDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.UserAuthDetails}.
 */
public interface UserAuthDetailsService {

    /**
     * Save a userAuthDetails.
     *
     * @param userAuthDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    UserAuthDetailsDTO save(UserAuthDetailsDTO userAuthDetailsDTO);

    /**
     * Get all the userAuthDetails.
     *
     * @return the list of entities.
     */
    List<UserAuthDetailsDTO> findAll();

    /**
     * Get the "id" userAuthDetails.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserAuthDetailsDTO> findOne(Long id);

    /**
     * Delete the "id" userAuthDetails.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the userAuthDetails corresponding to the query.
     *
     * @param query the query of the search.
     *
     * @return the list of entities.
     */
    List<UserAuthDetailsDTO> search(String query);

    List<UserAuthDetails> getAllByToken(String authToken);
}
