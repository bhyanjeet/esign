package com.hartron.esignharyana.service;

import com.hartron.esignharyana.config.ApplicationProperties;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;
import javax.net.ssl.SSLContext;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

@Service
public class OtpSmsService {

    private final ApplicationProperties applicationProperties;

    public OtpSmsService(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public void sendSingleSMS(String mobileNumber, String message) throws UnsupportedEncodingException, NoSuchAlgorithmException {
//        Acknowledgements acknowledgements=new Acknowledgements();
        String username = applicationProperties.getSms().getUsername();
        String password = applicationProperties.getSms().getPassword();
        String senderId = applicationProperties.getSms().getSenderId();
        String secureKey = applicationProperties.getSms().getSecureKey();
        String response = null;
        String genratedhashKey = hashGenerator(username, senderId, message, secureKey);
        String encryptedPassword = MD5(password);


        Callable<String> callable = () ->{
            try {
                SSLSocketFactory sf=null;
                SSLContext context=null;
                context=SSLContext.getInstance("TLSv1.2");
                context.init(null, null, null);
                sf=new SSLSocketFactory(context, SSLSocketFactory.STRICT_HOSTNAME_VERIFIER);
                Scheme scheme=new Scheme("https",443,sf);
                HttpClient client=new DefaultHttpClient();
                client.getConnectionManager().getSchemeRegistry().register(scheme);
                HttpPost post=new HttpPost("https://msdgweb.mgov.gov.in/esms/sendsmsrequest");
                List<NameValuePair> nameValuePairs=new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("mobileno", mobileNumber));
                nameValuePairs.add(new BasicNameValuePair("senderid", senderId));
                nameValuePairs.add(new BasicNameValuePair("content", message));
                nameValuePairs.add(new BasicNameValuePair("smsservicetype", "singlemsg"));
                nameValuePairs.add(new BasicNameValuePair("username", username));
                nameValuePairs.add(new BasicNameValuePair("password", encryptedPassword));
                nameValuePairs.add(new BasicNameValuePair("key", genratedhashKey));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                System.out.print("sending sms...");
                HttpResponse response1=client.execute(post);

                System.out.print("sms sent...");
                BufferedReader rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
                String line;
                StringBuffer buffer = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    buffer.append(line).append("\n");
                }
                rd.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return "success";
        };
        ExecutorService executorService = null;
        try{
            executorService = Executors.newSingleThreadExecutor();
            Future<String> future = executorService.submit(callable);
            future.get(10, TimeUnit.SECONDS);

        }catch (InterruptedException e){
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            executorService.shutdownNow();
        }
    }


    protected String hashGenerator(String userName, String senderId, String content, String secureKey) {
//         TODO Auto-generated method stub
        StringBuffer finalString = new StringBuffer();
        finalString.append(userName.trim()).append(senderId.trim()).append(content.trim()).append(secureKey.trim());
        String hashGen = finalString.toString();
        StringBuffer sb = null;
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-512");
            md.update(hashGen.getBytes());
            byte byteData[] = md.digest();
            //convert the byte to hex format method 1
            sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return sb.toString();
    }

    /****
     * Method  to convert Normal Plain Text Password to MD5 encrypted password
     ***/

    private static String MD5(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md;
        md = MessageDigest.getInstance("SHA-1");
        byte[] md5 = new byte[64];
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        md5 = md.digest();
        return convertedToHex(md5);
    }

    private static String convertedToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();

        for (int i = 0; i < data.length; i++) {
            int halfOfByte = (data[i] >>> 4) & 0x0F;
            int twoHalfBytes = 0;

            do {
                if ((0 <= halfOfByte) && (halfOfByte <= 9)) {
                    buf.append((char) ('0' + halfOfByte));
                } else {
                    buf.append((char) ('a' + (halfOfByte - 10)));
                }

                halfOfByte = data[i] & 0x0F;

            } while (twoHalfBytes++ < 1);
        }
        return buf.toString();
    }
}
