package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.OrganisationLogDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link OrganisationLog} and its DTO {@link OrganisationLogDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OrganisationLogMapper extends EntityMapper<OrganisationLogDTO, OrganisationLog> {



    default OrganisationLog fromId(Long id) {
        if (id == null) {
            return null;
        }
        OrganisationLog organisationLog = new OrganisationLog();
        organisationLog.setId(id);
        return organisationLog;
    }
}
