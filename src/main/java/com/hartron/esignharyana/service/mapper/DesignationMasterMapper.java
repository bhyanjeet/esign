package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.DesignationMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DesignationMaster} and its DTO {@link DesignationMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DesignationMasterMapper extends EntityMapper<DesignationMasterDTO, DesignationMaster> {



    default DesignationMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        DesignationMaster designationMaster = new DesignationMaster();
        designationMaster.setId(id);
        return designationMaster;
    }
}
