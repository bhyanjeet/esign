package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.UserLogsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserLogs} and its DTO {@link UserLogsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UserLogsMapper extends EntityMapper<UserLogsDTO, UserLogs> {



    default UserLogs fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserLogs userLogs = new UserLogs();
        userLogs.setId(id);
        return userLogs;
    }
}
