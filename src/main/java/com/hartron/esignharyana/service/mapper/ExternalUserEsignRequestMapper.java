package com.hartron.esignharyana.service.mapper;


import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.ExternalUserEsignRequestDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ExternalUserEsignRequest} and its DTO {@link ExternalUserEsignRequestDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ExternalUserEsignRequestMapper extends EntityMapper<ExternalUserEsignRequestDTO, ExternalUserEsignRequest> {



    default ExternalUserEsignRequest fromId(Long id) {
        if (id == null) {
            return null;
        }
        ExternalUserEsignRequest externalUserEsignRequest = new ExternalUserEsignRequest();
        externalUserEsignRequest.setId(id);
        return externalUserEsignRequest;
    }
}
