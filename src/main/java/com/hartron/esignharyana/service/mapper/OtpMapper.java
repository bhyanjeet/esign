package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.OtpDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Otp} and its DTO {@link OtpDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OtpMapper extends EntityMapper<OtpDTO, Otp> {



    default Otp fromId(Long id) {
        if (id == null) {
            return null;
        }
        Otp otp = new Otp();
        otp.setId(id);
        return otp;
    }
}
