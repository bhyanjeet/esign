package com.hartron.esignharyana.service.mapper;


import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.UserAuthDetailsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserAuthDetails} and its DTO {@link UserAuthDetailsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UserAuthDetailsMapper extends EntityMapper<UserAuthDetailsDTO, UserAuthDetails> {



    default UserAuthDetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserAuthDetails userAuthDetails = new UserAuthDetails();
        userAuthDetails.setId(id);
        return userAuthDetails;
    }
}
