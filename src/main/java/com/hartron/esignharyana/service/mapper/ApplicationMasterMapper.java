package com.hartron.esignharyana.service.mapper;


import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.ApplicationMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ApplicationMaster} and its DTO {@link ApplicationMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {OrganisationMasterMapper.class})
public interface ApplicationMasterMapper extends EntityMapper<ApplicationMasterDTO, ApplicationMaster> {

    @Mapping(source = "organisationMaster.id", target = "organisationMasterId")
    ApplicationMasterDTO toDto(ApplicationMaster applicationMaster);

    @Mapping(source = "organisationMasterId", target = "organisationMaster")
    ApplicationMaster toEntity(ApplicationMasterDTO applicationMasterDTO);

    default ApplicationMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        ApplicationMaster applicationMaster = new ApplicationMaster();
        applicationMaster.setId(id);
        return applicationMaster;
    }
}
