package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.BlockMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BlockMaster} and its DTO {@link BlockMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {StateMasterMapper.class, DistrictMasterMapper.class})
public interface BlockMasterMapper extends EntityMapper<BlockMasterDTO, BlockMaster> {

    @Mapping(source = "stateMaster.id", target = "stateMasterId")
    @Mapping(source = "districtMaster.id", target = "districtMasterId")
    BlockMasterDTO toDto(BlockMaster blockMaster);

    @Mapping(source = "stateMasterId", target = "stateMaster")
    @Mapping(source = "districtMasterId", target = "districtMaster")
    BlockMaster toEntity(BlockMasterDTO blockMasterDTO);

    default BlockMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        BlockMaster blockMaster = new BlockMaster();
        blockMaster.setId(id);
        return blockMaster;
    }
}
