package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.OrganisationTypeMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link OrganisationTypeMaster} and its DTO {@link OrganisationTypeMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OrganisationTypeMasterMapper extends EntityMapper<OrganisationTypeMasterDTO, OrganisationTypeMaster> {



    default OrganisationTypeMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        OrganisationTypeMaster organisationTypeMaster = new OrganisationTypeMaster();
        organisationTypeMaster.setId(id);
        return organisationTypeMaster;
    }
}
