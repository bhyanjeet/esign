package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.DistrictMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DistrictMaster} and its DTO {@link DistrictMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {StateMasterMapper.class})
public interface DistrictMasterMapper extends EntityMapper<DistrictMasterDTO, DistrictMaster> {

    @Mapping(source = "stateMaster.id", target = "stateMasterId")
    DistrictMasterDTO toDto(DistrictMaster districtMaster);

    @Mapping(source = "stateMasterId", target = "stateMaster")
    DistrictMaster toEntity(DistrictMasterDTO districtMasterDTO);

    default DistrictMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        DistrictMaster districtMaster = new DistrictMaster();
        districtMaster.setId(id);
        return districtMaster;
    }
}
