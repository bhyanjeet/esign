package com.hartron.esignharyana.service.mapper;


import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.EsignErrorDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link EsignError} and its DTO {@link EsignErrorDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EsignErrorMapper extends EntityMapper<EsignErrorDTO, EsignError> {



    default EsignError fromId(Long id) {
        if (id == null) {
            return null;
        }
        EsignError esignError = new EsignError();
        esignError.setId(id);
        return esignError;
    }
}
