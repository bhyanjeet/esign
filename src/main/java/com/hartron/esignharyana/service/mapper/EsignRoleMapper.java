package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.EsignRoleDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link EsignRole} and its DTO {@link EsignRoleDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EsignRoleMapper extends EntityMapper<EsignRoleDTO, EsignRole> {



    default EsignRole fromId(Long id) {
        if (id == null) {
            return null;
        }
        EsignRole esignRole = new EsignRole();
        esignRole.setId(id);
        return esignRole;
    }
}
