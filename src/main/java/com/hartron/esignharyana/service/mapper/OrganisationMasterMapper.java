package com.hartron.esignharyana.service.mapper;


import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.OrganisationMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link OrganisationMaster} and its DTO {@link OrganisationMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {OrganisationTypeMasterMapper.class, StateMasterMapper.class, DistrictMasterMapper.class, SubDistrictMasterMapper.class, DepartmentMapper.class})
public interface OrganisationMasterMapper extends EntityMapper<OrganisationMasterDTO, OrganisationMaster> {

    @Mapping(source = "organisationTypeMaster.id", target = "organisationTypeMasterId")
    @Mapping(source = "stateMaster.id", target = "stateMasterId")
    @Mapping(source = "districtMaster.id", target = "districtMasterId")
    @Mapping(source = "subDistrictMaster.id", target = "subDistrictMasterId")
    @Mapping(source = "department.id", target = "departmentId")
    OrganisationMasterDTO toDto(OrganisationMaster organisationMaster);

    @Mapping(source = "organisationTypeMasterId", target = "organisationTypeMaster")
    @Mapping(source = "stateMasterId", target = "stateMaster")
    @Mapping(source = "districtMasterId", target = "districtMaster")
    @Mapping(source = "subDistrictMasterId", target = "subDistrictMaster")
    @Mapping(source = "departmentId", target = "department")
    OrganisationMaster toEntity(OrganisationMasterDTO organisationMasterDTO);

    default OrganisationMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        OrganisationMaster organisationMaster = new OrganisationMaster();
        organisationMaster.setId(id);
        return organisationMaster;
    }
}
