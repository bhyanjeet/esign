package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.OrganisationDocumentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link OrganisationDocument} and its DTO {@link OrganisationDocumentDTO}.
 */
@Mapper(componentModel = "spring", uses = {OrganisationTypeMasterMapper.class})
public interface OrganisationDocumentMapper extends EntityMapper<OrganisationDocumentDTO, OrganisationDocument> {

    @Mapping(source = "organisationTypeMaster.id", target = "organisationTypeMasterId")
    OrganisationDocumentDTO toDto(OrganisationDocument organisationDocument);

    @Mapping(source = "organisationTypeMasterId", target = "organisationTypeMaster")
    OrganisationDocument toEntity(OrganisationDocumentDTO organisationDocumentDTO);

    default OrganisationDocument fromId(Long id) {
        if (id == null) {
            return null;
        }
        OrganisationDocument organisationDocument = new OrganisationDocument();
        organisationDocument.setId(id);
        return organisationDocument;
    }
}
