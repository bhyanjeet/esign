package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.StateMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link StateMaster} and its DTO {@link StateMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface StateMasterMapper extends EntityMapper<StateMasterDTO, StateMaster> {



    default StateMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        StateMaster stateMaster = new StateMaster();
        stateMaster.setId(id);
        return stateMaster;
    }
}
