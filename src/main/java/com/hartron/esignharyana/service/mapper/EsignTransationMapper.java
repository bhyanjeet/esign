package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.EsignTransationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link EsignTransation} and its DTO {@link EsignTransationDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EsignTransationMapper extends EntityMapper<EsignTransationDTO, EsignTransation> {



    default EsignTransation fromId(Long id) {
        if (id == null) {
            return null;
        }
        EsignTransation esignTransation = new EsignTransation();
        esignTransation.setId(id);
        return esignTransation;
    }
}
