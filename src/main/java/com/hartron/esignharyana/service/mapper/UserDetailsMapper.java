package com.hartron.esignharyana.service.mapper;


import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.UserDetailsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserDetails} and its DTO {@link UserDetailsDTO}.
 */
@Mapper(componentModel = "spring", uses = {DesignationMasterMapper.class, OrganisationMasterMapper.class, EsignRoleMapper.class, ApplicationMasterMapper.class})
public interface UserDetailsMapper extends EntityMapper<UserDetailsDTO, UserDetails> {

    @Mapping(source = "designationMaster.id", target = "designationMasterId")
    @Mapping(source = "organisationMaster.id", target = "organisationMasterId")
    @Mapping(source = "esignRole.id", target = "esignRoleId")
    @Mapping(source = "applicationMaster.id", target = "applicationMasterId")
    UserDetailsDTO toDto(UserDetails userDetails);

    @Mapping(source = "designationMasterId", target = "designationMaster")
    @Mapping(source = "organisationMasterId", target = "organisationMaster")
    @Mapping(source = "esignRoleId", target = "esignRole")
    @Mapping(source = "applicationMasterId", target = "applicationMaster")
    UserDetails toEntity(UserDetailsDTO userDetailsDTO);

    default UserDetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserDetails userDetails = new UserDetails();
        userDetails.setId(id);
        return userDetails;
    }
}
