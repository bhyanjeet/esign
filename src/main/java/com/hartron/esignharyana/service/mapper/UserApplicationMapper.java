package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.UserApplicationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserApplication} and its DTO {@link UserApplicationDTO}.
 */
@Mapper(componentModel = "spring", uses = {ApplicationMasterMapper.class})
public interface UserApplicationMapper extends EntityMapper<UserApplicationDTO, UserApplication> {

    @Mapping(source = "applicationMaster.id", target = "applicationMasterId")
    UserApplicationDTO toDto(UserApplication userApplication);

    @Mapping(source = "applicationMasterId", target = "applicationMaster")
    UserApplication toEntity(UserApplicationDTO userApplicationDTO);

    default UserApplication fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserApplication userApplication = new UserApplication();
        userApplication.setId(id);
        return userApplication;
    }
}
