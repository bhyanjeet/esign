package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.SubDistrictMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link SubDistrictMaster} and its DTO {@link SubDistrictMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {StateMasterMapper.class, DistrictMasterMapper.class})
public interface SubDistrictMasterMapper extends EntityMapper<SubDistrictMasterDTO, SubDistrictMaster> {

    @Mapping(source = "stateMaster.id", target = "stateMasterId")
    @Mapping(source = "districtMaster.id", target = "districtMasterId")
    SubDistrictMasterDTO toDto(SubDistrictMaster subDistrictMaster);

    @Mapping(source = "stateMasterId", target = "stateMaster")
    @Mapping(source = "districtMasterId", target = "districtMaster")
    SubDistrictMaster toEntity(SubDistrictMasterDTO subDistrictMasterDTO);

    default SubDistrictMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        SubDistrictMaster subDistrictMaster = new SubDistrictMaster();
        subDistrictMaster.setId(id);
        return subDistrictMaster;
    }
}
