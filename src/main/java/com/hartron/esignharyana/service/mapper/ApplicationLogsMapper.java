package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.ApplicationLogsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ApplicationLogs} and its DTO {@link ApplicationLogsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ApplicationLogsMapper extends EntityMapper<ApplicationLogsDTO, ApplicationLogs> {



    default ApplicationLogs fromId(Long id) {
        if (id == null) {
            return null;
        }
        ApplicationLogs applicationLogs = new ApplicationLogs();
        applicationLogs.setId(id);
        return applicationLogs;
    }
}
