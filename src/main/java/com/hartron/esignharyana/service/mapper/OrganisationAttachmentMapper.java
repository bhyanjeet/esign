package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.*;
import com.hartron.esignharyana.service.dto.OrganisationAttachmentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link OrganisationAttachment} and its DTO {@link OrganisationAttachmentDTO}.
 */
@Mapper(componentModel = "spring", uses = {OrganisationMasterMapper.class, OrganisationDocumentMapper.class})
public interface OrganisationAttachmentMapper extends EntityMapper<OrganisationAttachmentDTO, OrganisationAttachment> {

    @Mapping(source = "organisationMaster.id", target = "organisationMasterId")
    @Mapping(source = "organisationDocument.id", target = "organisationDocumentId")
    OrganisationAttachmentDTO toDto(OrganisationAttachment organisationAttachment);

    @Mapping(source = "organisationMasterId", target = "organisationMaster")
    @Mapping(source = "organisationDocumentId", target = "organisationDocument")
    OrganisationAttachment toEntity(OrganisationAttachmentDTO organisationAttachmentDTO);

    default OrganisationAttachment fromId(Long id) {
        if (id == null) {
            return null;
        }
        OrganisationAttachment organisationAttachment = new OrganisationAttachment();
        organisationAttachment.setId(id);
        return organisationAttachment;
    }
}
