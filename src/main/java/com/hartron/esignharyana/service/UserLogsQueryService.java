package com.hartron.esignharyana.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hartron.esignharyana.domain.UserLogs;
import com.hartron.esignharyana.domain.*; // for static metamodels
import com.hartron.esignharyana.repository.UserLogsRepository;
import com.hartron.esignharyana.repository.search.UserLogsSearchRepository;
import com.hartron.esignharyana.service.dto.UserLogsCriteria;
import com.hartron.esignharyana.service.dto.UserLogsDTO;
import com.hartron.esignharyana.service.mapper.UserLogsMapper;

/**
 * Service for executing complex queries for {@link UserLogs} entities in the database.
 * The main input is a {@link UserLogsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserLogsDTO} or a {@link Page} of {@link UserLogsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserLogsQueryService extends QueryService<UserLogs> {

    private final Logger log = LoggerFactory.getLogger(UserLogsQueryService.class);

    private final UserLogsRepository userLogsRepository;

    private final UserLogsMapper userLogsMapper;

    private final UserLogsSearchRepository userLogsSearchRepository;

    public UserLogsQueryService(UserLogsRepository userLogsRepository, UserLogsMapper userLogsMapper, UserLogsSearchRepository userLogsSearchRepository) {
        this.userLogsRepository = userLogsRepository;
        this.userLogsMapper = userLogsMapper;
        this.userLogsSearchRepository = userLogsSearchRepository;
    }

    /**
     * Return a {@link List} of {@link UserLogsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UserLogsDTO> findByCriteria(UserLogsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UserLogs> specification = createSpecification(criteria);
        return userLogsMapper.toDto(userLogsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link UserLogsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserLogsDTO> findByCriteria(UserLogsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UserLogs> specification = createSpecification(criteria);
        return userLogsRepository.findAll(specification, page)
            .map(userLogsMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UserLogsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UserLogs> specification = createSpecification(criteria);
        return userLogsRepository.count(specification);
    }

    /**
     * Function to convert {@link UserLogsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<UserLogs> createSpecification(UserLogsCriteria criteria) {
        Specification<UserLogs> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), UserLogs_.id));
            }
            if (criteria.getActionTaken() != null) {
                specification = specification.and(buildStringSpecification(criteria.getActionTaken(), UserLogs_.actionTaken));
            }
            if (criteria.getActionTakenBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getActionTakenBy(), UserLogs_.actionTakenBy));
            }
            if (criteria.getActionTakenOnUser() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getActionTakenOnUser(), UserLogs_.actionTakenOnUser));
            }
            if (criteria.getActionTakenOnDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getActionTakenOnDate(), UserLogs_.actionTakenOnDate));
            }
            if (criteria.getRemarks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemarks(), UserLogs_.remarks));
            }
        }
        return specification;
    }
}
