package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.SubDistrictMasterDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.SubDistrictMaster}.
 */
public interface SubDistrictMasterService {

    /**
     * Save a subDistrictMaster.
     *
     * @param subDistrictMasterDTO the entity to save.
     * @return the persisted entity.
     */
    SubDistrictMasterDTO save(SubDistrictMasterDTO subDistrictMasterDTO);

    /**
     * Get all the subDistrictMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SubDistrictMasterDTO> findAll(Pageable pageable);


    /**
     * Get the "id" subDistrictMaster.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SubDistrictMasterDTO> findOne(Long id);

    /**
     * Delete the "id" subDistrictMaster.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the subDistrictMaster corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SubDistrictMasterDTO> search(String query, Pageable pageable);
}
