package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.UserApplicationDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.UserApplication}.
 */
public interface UserApplicationService {

    /**
     * Save a userApplication.
     *
     * @param userApplicationDTO the entity to save.
     * @return the persisted entity.
     */
    UserApplicationDTO save(UserApplicationDTO userApplicationDTO);

    /**
     * Get all the userApplications.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserApplicationDTO> findAll(Pageable pageable);


    /**
     * Get the "id" userApplication.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserApplicationDTO> findOne(Long id);

    /**
     * Delete the "id" userApplication.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the userApplication corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserApplicationDTO> search(String query, Pageable pageable);

    List<UserApplicationDTO> findUserApplicationByUserLogin(String userLogin);

    List<UserApplicationDTO> findApplicationByUserId(Long userId);

    Optional<UserApplicationDTO>findApplicationCodeUserCode(String applicationCodeId, String userCodeId);
}
