package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.ExternalUserEsignRequestDTO;
import org.xml.sax.SAXException;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.UserDetails}.
 */
public interface EsignDemoService {


    ExternalUserEsignRequestDTO  saveEsignRequest(ExternalUserEsignRequestDTO externalUserEsignRequestDTO) throws NoSuchAlgorithmException, ParserConfigurationException, IOException, SAXException, TransformerException, XMLSignatureException, MarshalException, KeyStoreException, UnrecoverableEntryException, CertificateException, InvalidAlgorithmParameterException;

}
