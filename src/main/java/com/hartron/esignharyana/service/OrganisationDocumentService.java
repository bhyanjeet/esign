package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.OrganisationDocumentDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.OrganisationDocument}.
 */
public interface OrganisationDocumentService {

    /**
     * Save a organisationDocument.
     *
     * @param organisationDocumentDTO the entity to save.
     * @return the persisted entity.
     */
    OrganisationDocumentDTO save(OrganisationDocumentDTO organisationDocumentDTO);

    /**
     * Get all the organisationDocuments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrganisationDocumentDTO> findAll(Pageable pageable);


    /**
     * Get the "id" organisationDocument.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrganisationDocumentDTO> findOne(Long id);

    /**
     * Delete the "id" organisationDocument.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the organisationDocument corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrganisationDocumentDTO> search(String query, Pageable pageable);

    List<OrganisationDocumentDTO> findOrganisationDocumentByOrganisationTypeMasterId(Long id);
}
