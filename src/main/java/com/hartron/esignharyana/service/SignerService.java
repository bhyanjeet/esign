package com.hartron.esignharyana.service;

import org.xml.sax.SAXException;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.UserDetails}.
 */
public interface SignerService {

    String verifyUser(String userCodeId, String lastAction) throws ParserConfigurationException, TransformerException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, XMLSignatureException, MarshalException, IOException, KeyStoreException, SAXException, UnrecoverableEntryException, CertificateException;
    String accountSigner(String userCodeId, String lastAction, String signerId, String accountStatus) throws ParserConfigurationException, TransformerException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, XMLSignatureException, MarshalException, IOException, KeyStoreException, SAXException, UnrecoverableEntryException, CertificateException;
    String createOrgKycXML(String userCodeId) throws ParserConfigurationException, TransformerException, IOException;
    String signXML(String xmlDoc) throws ParserConfigurationException, TransformerException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IOException, UnrecoverableEntryException, KeyStoreException, MarshalException, XMLSignatureException, SAXException, CertificateException;
    String createSignerXML(String signedOrgKycXML) throws ParserConfigurationException, TransformerException, MarshalException, XMLSignatureException, IOException, SAXException, KeyStoreException, CertificateException, UnrecoverableEntryException;
    String updateSigner(String userCodeId, String lastAction, String signerId) throws ParserConfigurationException, TransformerException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, XMLSignatureException, MarshalException, IOException, KeyStoreException, SAXException, UnrecoverableEntryException, CertificateException;


}
