package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.BlockMasterDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.BlockMaster}.
 */
public interface BlockMasterService {

    /**
     * Save a blockMaster.
     *
     * @param blockMasterDTO the entity to save.
     * @return the persisted entity.
     */
    BlockMasterDTO save(BlockMasterDTO blockMasterDTO);

    /**
     * Get all the blockMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BlockMasterDTO> findAll(Pageable pageable);


    /**
     * Get the "id" blockMaster.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BlockMasterDTO> findOne(Long id);

    /**
     * Delete the "id" blockMaster.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the blockMaster corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BlockMasterDTO> search(String query, Pageable pageable);
}
