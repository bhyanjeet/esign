package com.hartron.esignharyana.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hartron.esignharyana.domain.EsignTransation;
import com.hartron.esignharyana.domain.*; // for static metamodels
import com.hartron.esignharyana.repository.EsignTransationRepository;
import com.hartron.esignharyana.repository.search.EsignTransationSearchRepository;
import com.hartron.esignharyana.service.dto.EsignTransationCriteria;
import com.hartron.esignharyana.service.dto.EsignTransationDTO;
import com.hartron.esignharyana.service.mapper.EsignTransationMapper;

/**
 * Service for executing complex queries for {@link EsignTransation} entities in the database.
 * The main input is a {@link EsignTransationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EsignTransationDTO} or a {@link Page} of {@link EsignTransationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EsignTransationQueryService extends QueryService<EsignTransation> {

    private final Logger log = LoggerFactory.getLogger(EsignTransationQueryService.class);

    private final EsignTransationRepository esignTransationRepository;

    private final EsignTransationMapper esignTransationMapper;

    private final EsignTransationSearchRepository esignTransationSearchRepository;

    public EsignTransationQueryService(EsignTransationRepository esignTransationRepository, EsignTransationMapper esignTransationMapper, EsignTransationSearchRepository esignTransationSearchRepository) {
        this.esignTransationRepository = esignTransationRepository;
        this.esignTransationMapper = esignTransationMapper;
        this.esignTransationSearchRepository = esignTransationSearchRepository;
    }

    /**
     * Return a {@link List} of {@link EsignTransationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EsignTransationDTO> findByCriteria(EsignTransationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EsignTransation> specification = createSpecification(criteria);
        return esignTransationMapper.toDto(esignTransationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link EsignTransationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EsignTransationDTO> findByCriteria(EsignTransationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EsignTransation> specification = createSpecification(criteria);
        return esignTransationRepository.findAll(specification, page)
            .map(esignTransationMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EsignTransationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EsignTransation> specification = createSpecification(criteria);
        return esignTransationRepository.count(specification);
    }

    /**
     * Function to convert {@link EsignTransationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<EsignTransation> createSpecification(EsignTransationCriteria criteria) {
        Specification<EsignTransation> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), EsignTransation_.id));
            }
            if (criteria.getTxnId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTxnId(), EsignTransation_.txnId));
            }
            if (criteria.getUserCodeId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUserCodeId(), EsignTransation_.userCodeId));
            }
            if (criteria.getApplicationCodeId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getApplicationCodeId(), EsignTransation_.applicationCodeId));
            }
            if (criteria.getOrgResponseSuccessURL() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOrgResponseSuccessURL(), EsignTransation_.orgResponseSuccessURL));
            }
            if (criteria.getOrgResponseFailURL() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOrgResponseFailURL(), EsignTransation_.orgResponseFailURL));
            }
            if (criteria.getOrgStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOrgStatus(), EsignTransation_.orgStatus));
            }
            if (criteria.getRequestDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRequestDate(), EsignTransation_.requestDate));
            }
        }
        return specification;
    }
}
