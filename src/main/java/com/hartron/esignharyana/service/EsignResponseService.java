package com.hartron.esignharyana.service;

import com.itextpdf.text.DocumentException;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OperatorCreationException;
import org.json.JSONException;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.UserDetails}.
 */
public interface EsignResponseService {

    String saveEsignResponse(String esignResponse, HttpServletResponse response) throws ParserConfigurationException, TransformerException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, XMLSignatureException, MarshalException, IOException, KeyStoreException, SAXException, UnrecoverableEntryException, CertificateException, DocumentException, SignatureException, InvalidKeyException, CMSException, OperatorCreationException, NoSuchProviderException, JSONException;

    String saveEsignRedirect(String saveEsignRedirect, HttpServletResponse response) throws ParserConfigurationException, TransformerException, IOException, CertificateException, NoSuchAlgorithmException, XMLSignatureException, UnrecoverableEntryException, InvalidAlgorithmParameterException, SAXException, MarshalException, KeyStoreException, JSONException, DocumentException, InterruptedException;


    String sendEsignStatus(String esignXml) throws IOException;
}
