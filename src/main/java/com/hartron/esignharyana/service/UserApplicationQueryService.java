package com.hartron.esignharyana.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hartron.esignharyana.domain.UserApplication;
import com.hartron.esignharyana.domain.*; // for static metamodels
import com.hartron.esignharyana.repository.UserApplicationRepository;
import com.hartron.esignharyana.repository.search.UserApplicationSearchRepository;
import com.hartron.esignharyana.service.dto.UserApplicationCriteria;
import com.hartron.esignharyana.service.dto.UserApplicationDTO;
import com.hartron.esignharyana.service.mapper.UserApplicationMapper;

/**
 * Service for executing complex queries for {@link UserApplication} entities in the database.
 * The main input is a {@link UserApplicationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserApplicationDTO} or a {@link Page} of {@link UserApplicationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserApplicationQueryService extends QueryService<UserApplication> {

    private final Logger log = LoggerFactory.getLogger(UserApplicationQueryService.class);

    private final UserApplicationRepository userApplicationRepository;

    private final UserApplicationMapper userApplicationMapper;

    private final UserApplicationSearchRepository userApplicationSearchRepository;

    public UserApplicationQueryService(UserApplicationRepository userApplicationRepository, UserApplicationMapper userApplicationMapper, UserApplicationSearchRepository userApplicationSearchRepository) {
        this.userApplicationRepository = userApplicationRepository;
        this.userApplicationMapper = userApplicationMapper;
        this.userApplicationSearchRepository = userApplicationSearchRepository;
    }

    /**
     * Return a {@link List} of {@link UserApplicationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UserApplicationDTO> findByCriteria(UserApplicationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UserApplication> specification = createSpecification(criteria);
        return userApplicationMapper.toDto(userApplicationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link UserApplicationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserApplicationDTO> findByCriteria(UserApplicationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UserApplication> specification = createSpecification(criteria);
        return userApplicationRepository.findAll(specification, page)
            .map(userApplicationMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UserApplicationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UserApplication> specification = createSpecification(criteria);
        return userApplicationRepository.count(specification);
    }

    /**
     * Function to convert {@link UserApplicationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<UserApplication> createSpecification(UserApplicationCriteria criteria) {
        Specification<UserApplication> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), UserApplication_.id));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserId(), UserApplication_.userId));
            }
            if (criteria.getUserLogin() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUserLogin(), UserApplication_.userLogin));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), UserApplication_.createdBy));
            }
            if (criteria.getCreatedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedOn(), UserApplication_.createdOn));
            }
            if (criteria.getUpdatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUpdatedBy(), UserApplication_.updatedBy));
            }
            if (criteria.getUpdatedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdatedOn(), UserApplication_.updatedOn));
            }
            if (criteria.getVerifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVerifiedBy(), UserApplication_.verifiedBy));
            }
            if (criteria.getVerifiedOn() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVerifiedOn(), UserApplication_.verifiedOn));
            }
            if (criteria.getUserCodeId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUserCodeId(), UserApplication_.userCodeId));
            }
            if (criteria.getApplicationCodeId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getApplicationCodeId(), UserApplication_.applicationCodeId));
            }
            if (criteria.getApplicationMasterId() != null) {
                specification = specification.and(buildSpecification(criteria.getApplicationMasterId(),
                    root -> root.join(UserApplication_.applicationMaster, JoinType.LEFT).get(ApplicationMaster_.id)));
            }
        }
        return specification;
    }
}
