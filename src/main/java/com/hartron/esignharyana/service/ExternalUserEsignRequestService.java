package com.hartron.esignharyana.service;

import com.hartron.esignharyana.service.dto.ExternalUserEsignRequestDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.hartron.esignharyana.domain.ExternalUserEsignRequest}.
 */
public interface ExternalUserEsignRequestService {

    /**
     * Save a externalUserEsignRequest.
     *
     * @param externalUserEsignRequestDTO the entity to save.
     * @return the persisted entity.
     */
    ExternalUserEsignRequestDTO save(ExternalUserEsignRequestDTO externalUserEsignRequestDTO);

    /**
     * Get all the externalUserEsignRequests.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExternalUserEsignRequestDTO> findAll(Pageable pageable);

    /**
     * Get the "id" externalUserEsignRequest.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ExternalUserEsignRequestDTO> findOne(Long id);

    /**
     * Delete the "id" externalUserEsignRequest.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the externalUserEsignRequest corresponding to the query.
     *
     * @param query the query of the search.
     * 
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExternalUserEsignRequestDTO> search(String query, Pageable pageable);
}
