import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';

import { APPLICATION_ROUTE, ApplicationComponent } from './';

@NgModule({
    imports: [
      EsignharyanaSharedModule,
      RouterModule.forRoot([ APPLICATION_ROUTE ], { useHash: true })
    ],
    declarations: [
      ApplicationComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppApplicationModule {}
