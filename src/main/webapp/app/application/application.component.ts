import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {ApplicationMasterService} from "app/entities/application-master/application-master.service";
import {ApplicationMaster, IApplicationMaster} from "app/shared/model/application-master.model";
import {Observable} from "rxjs";
import {HttpResponse} from "@angular/common/http";
import {Router} from "@angular/router";
import {JhiAlertService} from "ng-jhipster";

@Component({
  selector: 'jhi-application',
  templateUrl: './application.component.html',
  styleUrls: [
    'application.component.scss'
  ]
})
export class ApplicationComponent implements OnInit {
  isSaving: boolean;

  applicationmasters: IApplicationMaster[];

  message: string;

  editForm = this.fb.group({
    id: [],
    applicationName: [null, [Validators.required]],
    remarks: [],
    applicationURL: [],
  });


  constructor(
    protected applicationMasterService: ApplicationMasterService,
    private fb: FormBuilder,
    private router: Router,
    protected jhiAlertService: JhiAlertService,
  ) {
    this.message = 'ApplicationComponent message';
  }

  ngOnInit() {

  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const applicationMaster = this.createFromForm();
    if (applicationMaster.id !== null) {
      this.subscribeToSaveResponse(this.applicationMasterService.update(applicationMaster));
    } else {
      this.subscribeToSaveResponse(this.applicationMasterService.create(applicationMaster));
    }
  }

  private createFromForm(): IApplicationMaster {
    return {
      ...new ApplicationMaster(),
      id: this.editForm.get(['id']).value,
      applicationName: this.editForm.get(['applicationName']).value,
      remarks: this.editForm.get(['remarks']).value,
      applicationURL: this.editForm.get(['applicationURL']).value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IApplicationMaster>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
   // this.previousState();
    alert("Application Created Successfull");
    this.router.navigate(['application-dashboard']);
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }


}
