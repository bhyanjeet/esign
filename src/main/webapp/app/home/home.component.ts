import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import * as $ from 'jquery';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { StateStorageService } from '../core/auth/state-storage.service';
import { LoginService } from '../core/login/login.service';
import {EncrDecrService} from "app/shared/Services/EncrDecrService";

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account;
  authSubscription: Subscription;
  error: string;
  success: boolean;
  captchaSuccess = false;
  authenticationError: boolean;
  password: string;
  rememberMe: boolean;
  username: string;
  credentials: any;

  loginForm = this.fb.group({
    username: [''],
    password: [''],
    rememberMe: [false]
  });

  constructor(
    private accountService: AccountService,
    private eventManager: JhiEventManager,
    private fb: FormBuilder,
    private router: Router,
    private loginService: LoginService,
    private stateStorageService: StateStorageService,
    private EncrDecr: EncrDecrService,
  ) {}

  RemoveSavedPassword() {
    const inputValue = $('#pwd').val();
    const numChars = inputValue.length;
    let showText = '';
    for (let i = 0; i < numChars; i++) {
      showText += '&#8226;';
    }
    $('#password').html(showText);
  }

  ngOnInit() {
    window.history.forward();
    this.accountService.identity().subscribe((account: Account) => {
      this.account = account;
    });
    this.success = false;
    this.captchaSuccess = false;
  }

  login() {



    if (this.captchaSuccess) {

      const encrypted = this.EncrDecr.set('6D24456D6640616362676259646E4578', this.loginForm.get('password').value);
      const credentials = {
        username: this.loginForm.get('username').value,
        password: encrypted,
        rememberMe: this.loginForm.get('rememberMe').value
      };
      this.loginService
        .login(credentials)
        .subscribe(
          () => {
            this.authenticationError = false;
            if (
              this.router.url === '/account/register' ||
              this.router.url.startsWith('/account/activate') ||
              this.router.url.startsWith('/account/reset/')
            ) {
              this.router.navigate(['']);
            }

            this.eventManager.broadcast({
              name: 'authenticationSuccess',
              content: 'Sending Authentication Success'
            });

            // previousState was set in the authExpiredInterceptor before being redirected to login modal.
            // since login is successful, go to stored previousState and clear previousState
            const redirect = this.stateStorageService.getUrl();
            if (redirect) {
              this.stateStorageService.storeUrl(null);
              this.router.navigateByUrl(redirect);
            } else if (this.accountService.hasAnyAuthority(['ROLE_ASP_NODAL'])) {
              this.router.navigate(['organisation-dashboard']);
            } else if (this.accountService.hasAnyAuthority(['ROLE_ORG_NODAL'])) {
              this.router.navigate(['organisation-dashboard']);
            }
          },
          () => {
            this.authenticationError = true;
            this.captchaSuccess = false;
            window.location.reload();
            grecaptcha.reset();
          }
        );
    } else {
      window.location.reload();
    }
  }


  ngOnDestroy() {
    if (this.authSubscription) {
      this.eventManager.destroy(this.authSubscription);
    }
  }
  registerNew() {
    this.router.navigate(['/account/register']);
  }

  requestResetPassword() {
    this.router.navigate(['/account/reset', 'request']);
  }
  isAuthenticated() {
    return this.accountService.isAuthenticated();
  }

  resolved(captchaResponse: string) {
    if (captchaResponse) {
      this.captchaSuccess = true;
    }
  }
}
