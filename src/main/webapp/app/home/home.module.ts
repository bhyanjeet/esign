import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import {RecaptchaModule} from 'ng-recaptcha';

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild([HOME_ROUTE]), RecaptchaModule],
  declarations: [HomeComponent]
})
export class EsignharyanaHomeModule {}
