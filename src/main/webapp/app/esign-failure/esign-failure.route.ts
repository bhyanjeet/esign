import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { EsignFailureComponent } from './esign-failure.component';

export const ESIGN_FAILURE_ROUTE: Route = {
  path: 'esign-failure/:txn',
  component: EsignFailureComponent,
  data: {
    authorities: [],
    pageTitle: 'esign-failure.title'
  },
  canActivate: [UserRouteAccessService]
};
