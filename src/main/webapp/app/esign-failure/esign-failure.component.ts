import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'jhi-esign-failure',
  templateUrl: './esign-failure.component.html',
  styleUrls: [
    'esign-failure.component.scss'
  ]
})
export class EsignFailureComponent implements OnInit {

  message: string;
  txn: any;

  constructor(
    private route: ActivatedRoute,

  ) {
    this.message = 'EsignFailureComponent message';

  }

  ngOnInit(): void {

    this.route.paramMap.subscribe(params => {
      this.txn = params.get("txn");
      alert(this.txn);
    });
  }

}
