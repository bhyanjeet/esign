import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from '../shared/shared.module';

import { ESIGN_FAILURE_ROUTE, EsignFailureComponent } from './';

@NgModule({
    imports: [
      EsignharyanaSharedModule,
      RouterModule.forRoot([ ESIGN_FAILURE_ROUTE ], { useHash: true })
    ],
    declarations: [
      EsignFailureComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppEsignFailureModule {}
