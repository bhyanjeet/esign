import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { UserApplicationComponent } from './user-application.component';
import {ApplicationMasterResolve} from "app/entities/application-master/application-master.route";

export const USER_APPLICATION_ROUTE: Route = {
  path: 'user-application-assign',
  component: UserApplicationComponent,
  resolve: {
    applicationMaster: ApplicationMasterResolve
  },
  data: {
    authorities: [],
    pageTitle: 'user-application.title'
  },
  canActivate: [UserRouteAccessService]
};
