import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AccountService} from "app/core/auth/account.service";
import {User} from "app/core/user/user.model";
import {UserApplicationService} from "app/entities/user-application/user-application.service";
import {IUserApplication} from "app/shared/model/user-application.model";
import {ApplicationMasterService} from "app/entities/application-master/application-master.service";
import {IApplicationMaster} from "app/shared/model/application-master.model";

@Component({
  selector: 'jhi-user-application',
  templateUrl: './user-application.component.html',
  styleUrls: [
    'user-application.component.scss'
  ]
})
export class UserApplicationComponent implements OnInit {

  message: string;
  currentAccount: User;
  userApplications: IUserApplication[];
  applicationMasters: IApplicationMaster;

  userAppData: Array<any> = [];


  constructor(
    protected accountService: AccountService,
    protected userApplicationService: UserApplicationService,
    protected applicationMasterService: ApplicationMasterService,
  ) {
    this.message = 'UserApplicationComponent message';
  }

  ngOnInit() {
    this.accountService.identity().subscribe(account => {
      this.currentAccount = account;

      this.userApplicationService.findUserApplicationByLogin().subscribe((response) => {

      this.userApplications = response.body;

        for (let i = 0 ; i<=this.userApplications.length; i++){
          this.applicationMasterService.find(this.userApplications[i].applicationMasterId).subscribe((response1) => {
            this.applicationMasters = response1.body;
            this.userAppData.push(this.applicationMasters);
          })
        }

      });

    });
  }

}
