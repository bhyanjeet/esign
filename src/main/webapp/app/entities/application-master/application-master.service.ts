import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IApplicationMaster } from 'app/shared/model/application-master.model';
import {IUserDetails} from "app/shared/model/user-details.model";

type EntityResponseType = HttpResponse<IApplicationMaster>;
type EntityArrayResponseType = HttpResponse<IApplicationMaster[]>;

@Injectable({ providedIn: 'root' })
export class ApplicationMasterService {
  public resourceUrl = SERVER_API_URL + 'api/application-masters';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/application-masters';
  public resourceSearchUrlByUserLogin = SERVER_API_URL + 'api/userLogin';
  public resourceSearchUrlByStatus = SERVER_API_URL + 'api/applicationByStatus';
  public resourceSearchUrlByLoginAndStatus = SERVER_API_URL + 'api/applicationByLoginAndStatus';

  constructor(protected http: HttpClient) {}

  create(applicationMaster: IApplicationMaster): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(applicationMaster);
    return this.http
      .post<IApplicationMaster>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(applicationMaster: IApplicationMaster): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(applicationMaster);
    return this.http
      .put<IApplicationMaster>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IApplicationMaster>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IApplicationMaster[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IApplicationMaster[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(applicationMaster: IApplicationMaster): IApplicationMaster {
    const copy: IApplicationMaster = Object.assign({}, applicationMaster, {
      createdOn:
        applicationMaster.createdOn != null && applicationMaster.createdOn.isValid()
          ? applicationMaster.createdOn.format(DATE_FORMAT)
          : null,
      lastUpdatedOn:
        applicationMaster.lastUpdatedOn != null && applicationMaster.lastUpdatedOn.isValid()
          ? applicationMaster.lastUpdatedOn.format(DATE_FORMAT)
          : null,
      verifiedOn:
        applicationMaster.verifiedOn != null && applicationMaster.verifiedOn.isValid()
          ? applicationMaster.verifiedOn.format(DATE_FORMAT)
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
      res.body.lastUpdatedOn = res.body.lastUpdatedOn != null ? moment(res.body.lastUpdatedOn) : null;
      res.body.verifiedOn = res.body.verifiedOn != null ? moment(res.body.verifiedOn) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((applicationMaster: IApplicationMaster) => {
        applicationMaster.createdOn = applicationMaster.createdOn != null ? moment(applicationMaster.createdOn) : null;
        applicationMaster.lastUpdatedOn = applicationMaster.lastUpdatedOn != null ? moment(applicationMaster.lastUpdatedOn) : null;
        applicationMaster.verifiedOn = applicationMaster.verifiedOn != null ? moment(applicationMaster.verifiedOn) : null;
      });
    }
    return res;
  }

  findApplicationByUserId(): Observable<EntityArrayResponseType> {
    return this.http
      .get<IApplicationMaster[]>(`${this.resourceSearchUrlByUserLogin}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  findApplicationByLoginAndStatus(): Observable<EntityArrayResponseType> {
    return this.http
      .get<IApplicationMaster[]>(`${this.resourceSearchUrlByLoginAndStatus}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  getAllApplicationByStatus(status: any): Observable<EntityArrayResponseType> {
    const value = new HttpParams().set('status', status);
    return this.http
      .get<IApplicationMaster[]>(`${this.resourceSearchUrlByStatus}/`, { params: value, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }
}
