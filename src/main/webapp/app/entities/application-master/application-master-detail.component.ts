import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IApplicationMaster } from 'app/shared/model/application-master.model';

@Component({
  selector: 'jhi-application-master-detail',
  templateUrl: './application-master-detail.component.html'
})
export class ApplicationMasterDetailComponent implements OnInit {
  applicationMaster: IApplicationMaster | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ applicationMaster }) => (this.applicationMaster = applicationMaster));
  }

  previousState(): void {
    window.history.back();
  }
}
