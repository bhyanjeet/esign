import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IApplicationMaster } from 'app/shared/model/application-master.model';
import { ApplicationMasterService } from './application-master.service';

@Component({
  templateUrl: './application-master-delete-dialog.component.html'
})
export class ApplicationMasterDeleteDialogComponent {
  applicationMaster?: IApplicationMaster;

  constructor(
    protected applicationMasterService: ApplicationMasterService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.applicationMasterService.delete(id).subscribe(() => {
      this.eventManager.broadcast('applicationMasterListModification');
      this.activeModal.close();
    });
  }
}
