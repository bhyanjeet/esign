import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IApplicationMaster, ApplicationMaster } from 'app/shared/model/application-master.model';
import { ApplicationMasterService } from './application-master.service';
import { IOrganisationMaster } from 'app/shared/model/organisation-master.model';
import { OrganisationMasterService } from 'app/entities/organisation-master/organisation-master.service';

@Component({
  selector: 'jhi-application-master-update',
  templateUrl: './application-master-update.component.html'
})
export class ApplicationMasterUpdateComponent implements OnInit {
  isSaving = false;
  organisationmasters: IOrganisationMaster[] = [];
  createdOnDp: any;
  lastUpdatedOnDp: any;
  verifiedOnDp: any;

  editForm = this.fb.group({
    id: [],
    applicationName: [null, [Validators.required]],
    createdBy: [],
    createdOn: [],
    lastUpdatedBy: [],
    lastUpdatedOn: [],
    verifiedBy: [],
    verifiedOn: [],
    remarks: [],
    applicationURL: [],
    applicationStatus: [],
    applicationIdCode: [null, []],
    status: [],
    certPublicKey: [],
    organisationMasterId: []
  });

  constructor(
    protected applicationMasterService: ApplicationMasterService,
    protected organisationMasterService: OrganisationMasterService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ applicationMaster }) => {
      this.updateForm(applicationMaster);

      this.organisationMasterService
        .query()
        .subscribe((res: HttpResponse<IOrganisationMaster[]>) => (this.organisationmasters = res.body || []));
    });
  }

  updateForm(applicationMaster: IApplicationMaster): void {
    this.editForm.patchValue({
      id: applicationMaster.id,
      applicationName: applicationMaster.applicationName,
      createdBy: applicationMaster.createdBy,
      createdOn: applicationMaster.createdOn,
      lastUpdatedBy: applicationMaster.lastUpdatedBy,
      lastUpdatedOn: applicationMaster.lastUpdatedOn,
      verifiedBy: applicationMaster.verifiedBy,
      verifiedOn: applicationMaster.verifiedOn,
      remarks: applicationMaster.remarks,
      applicationURL: applicationMaster.applicationURL,
      applicationStatus: applicationMaster.applicationStatus,
      applicationIdCode: applicationMaster.applicationIdCode,
      status: applicationMaster.status,
      certPublicKey: applicationMaster.certPublicKey,
      organisationMasterId: applicationMaster.organisationMasterId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const applicationMaster = this.createFromForm();
    if (applicationMaster.id !== undefined) {
      this.subscribeToSaveResponse(this.applicationMasterService.update(applicationMaster));
    } else {
      this.subscribeToSaveResponse(this.applicationMasterService.create(applicationMaster));
    }
  }

  private createFromForm(): IApplicationMaster {
    return {
      ...new ApplicationMaster(),
      id: this.editForm.get(['id'])!.value,
      applicationName: this.editForm.get(['applicationName'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value,
      createdOn: this.editForm.get(['createdOn'])!.value,
      lastUpdatedBy: this.editForm.get(['lastUpdatedBy'])!.value,
      lastUpdatedOn: this.editForm.get(['lastUpdatedOn'])!.value,
      verifiedBy: this.editForm.get(['verifiedBy'])!.value,
      verifiedOn: this.editForm.get(['verifiedOn'])!.value,
      remarks: this.editForm.get(['remarks'])!.value,
      applicationURL: this.editForm.get(['applicationURL'])!.value,
      applicationStatus: this.editForm.get(['applicationStatus'])!.value,
      applicationIdCode: this.editForm.get(['applicationIdCode'])!.value,
      status: this.editForm.get(['status'])!.value,
      certPublicKey: this.editForm.get(['certPublicKey'])!.value,
      organisationMasterId: this.editForm.get(['organisationMasterId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IApplicationMaster>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IOrganisationMaster): any {
    return item.id;
  }
}
