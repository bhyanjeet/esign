import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { ApplicationMasterComponent } from './application-master.component';
import { ApplicationMasterDetailComponent } from './application-master-detail.component';
import { ApplicationMasterUpdateComponent } from './application-master-update.component';
import { ApplicationMasterDeleteDialogComponent } from './application-master-delete-dialog.component';
import { applicationMasterRoute } from './application-master.route';

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(applicationMasterRoute)],
  declarations: [
    ApplicationMasterComponent,
    ApplicationMasterDetailComponent,
    ApplicationMasterUpdateComponent,
    ApplicationMasterDeleteDialogComponent
  ],
  entryComponents: [ApplicationMasterDeleteDialogComponent]
})
export class EsignharyanaApplicationMasterModule {}
