import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IApplicationMaster, ApplicationMaster } from 'app/shared/model/application-master.model';
import { ApplicationMasterService } from './application-master.service';
import { ApplicationMasterComponent } from './application-master.component';
import { ApplicationMasterDetailComponent } from './application-master-detail.component';
import { ApplicationMasterUpdateComponent } from './application-master-update.component';

@Injectable({ providedIn: 'root' })
export class ApplicationMasterResolve implements Resolve<IApplicationMaster> {
  constructor(private service: ApplicationMasterService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IApplicationMaster> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((applicationMaster: HttpResponse<ApplicationMaster>) => {
          if (applicationMaster.body) {
            return of(applicationMaster.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ApplicationMaster());
  }
}

export const applicationMasterRoute: Routes = [
  {
    path: '',
    component: ApplicationMasterComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'ApplicationMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ApplicationMasterDetailComponent,
    resolve: {
      applicationMaster: ApplicationMasterResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ApplicationMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ApplicationMasterUpdateComponent,
    resolve: {
      applicationMaster: ApplicationMasterResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ApplicationMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ApplicationMasterUpdateComponent,
    resolve: {
      applicationMaster: ApplicationMasterResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ApplicationMasters'
    },
    canActivate: [UserRouteAccessService]
  }
];
