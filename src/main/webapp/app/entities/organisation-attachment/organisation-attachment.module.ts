import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { OrganisationAttachmentComponent } from './organisation-attachment.component';
import { OrganisationAttachmentDetailComponent } from './organisation-attachment-detail.component';
import { OrganisationAttachmentUpdateComponent } from './organisation-attachment-update.component';
import {
  OrganisationAttachmentDeletePopupComponent,
  OrganisationAttachmentDeleteDialogComponent
} from './organisation-attachment-delete-dialog.component';
import { organisationAttachmentRoute, organisationAttachmentPopupRoute } from './organisation-attachment.route';

const ENTITY_STATES = [...organisationAttachmentRoute, ...organisationAttachmentPopupRoute];

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    OrganisationAttachmentComponent,
    OrganisationAttachmentDetailComponent,
    OrganisationAttachmentUpdateComponent,
    OrganisationAttachmentDeleteDialogComponent,
    OrganisationAttachmentDeletePopupComponent
  ],
  entryComponents: [OrganisationAttachmentDeleteDialogComponent]
})
export class EsignharyanaOrganisationAttachmentModule {}
