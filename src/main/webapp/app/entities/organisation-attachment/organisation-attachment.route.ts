import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { OrganisationAttachment } from 'app/shared/model/organisation-attachment.model';
import { OrganisationAttachmentService } from './organisation-attachment.service';
import { OrganisationAttachmentComponent } from './organisation-attachment.component';
import { OrganisationAttachmentDetailComponent } from './organisation-attachment-detail.component';
import { OrganisationAttachmentUpdateComponent } from './organisation-attachment-update.component';
import { OrganisationAttachmentDeletePopupComponent } from './organisation-attachment-delete-dialog.component';
import { IOrganisationAttachment } from 'app/shared/model/organisation-attachment.model';

@Injectable({ providedIn: 'root' })
export class OrganisationAttachmentResolve implements Resolve<IOrganisationAttachment> {
  constructor(private service: OrganisationAttachmentService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOrganisationAttachment> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((organisationAttachment: HttpResponse<OrganisationAttachment>) => organisationAttachment.body));
    }
    return of(new OrganisationAttachment());
  }
}

export const organisationAttachmentRoute: Routes = [
  {
    path: '',
    component: OrganisationAttachmentComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'id,asc',
      pageTitle: 'OrganisationAttachments'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: OrganisationAttachmentDetailComponent,
    resolve: {
      organisationAttachment: OrganisationAttachmentResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationAttachments'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: OrganisationAttachmentUpdateComponent,
    resolve: {
      organisationAttachment: OrganisationAttachmentResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationAttachments'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: OrganisationAttachmentUpdateComponent,
    resolve: {
      organisationAttachment: OrganisationAttachmentResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationAttachments'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const organisationAttachmentPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: OrganisationAttachmentDeletePopupComponent,
    resolve: {
      organisationAttachment: OrganisationAttachmentResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationAttachments'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
