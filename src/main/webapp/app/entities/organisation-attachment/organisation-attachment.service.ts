import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOrganisationAttachment } from 'app/shared/model/organisation-attachment.model';

type EntityResponseType = HttpResponse<IOrganisationAttachment>;
type EntityArrayResponseType = HttpResponse<IOrganisationAttachment[]>;

@Injectable({ providedIn: 'root' })
export class OrganisationAttachmentService {
  public resourceUrl = SERVER_API_URL + 'api/organisation-attachments';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/organisation-attachments';
  public resourceUrlByOrganisationMaster = SERVER_API_URL + 'api/organisationAttachmentByOrganisationMaster';


  constructor(protected http: HttpClient) {}

  create(organisationAttachment: IOrganisationAttachment): Observable<EntityResponseType> {
    return this.http.post<IOrganisationAttachment>(this.resourceUrl, organisationAttachment, { observe: 'response' });
  }

  update(organisationAttachment: IOrganisationAttachment): Observable<EntityResponseType> {
    return this.http.put<IOrganisationAttachment>(this.resourceUrl, organisationAttachment, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IOrganisationAttachment>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOrganisationAttachment[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOrganisationAttachment[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }

  getAllAttachmentsByOrganisationMaster(organisationMasterId: any): Observable<EntityArrayResponseType> {
    return this.http
      .get<IOrganisationAttachment[]>(`${this.resourceUrlByOrganisationMaster}?id=${organisationMasterId}`, { observe: 'response' });
  }
}
