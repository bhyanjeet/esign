import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOrganisationAttachment } from 'app/shared/model/organisation-attachment.model';
import { OrganisationAttachmentService } from './organisation-attachment.service';

@Component({
  selector: 'jhi-organisation-attachment-delete-dialog',
  templateUrl: './organisation-attachment-delete-dialog.component.html'
})
export class OrganisationAttachmentDeleteDialogComponent {
  organisationAttachment: IOrganisationAttachment;

  constructor(
    protected organisationAttachmentService: OrganisationAttachmentService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.organisationAttachmentService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'organisationAttachmentListModification',
        content: 'Deleted an organisationAttachment'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-organisation-attachment-delete-popup',
  template: ''
})
export class OrganisationAttachmentDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ organisationAttachment }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(OrganisationAttachmentDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.organisationAttachment = organisationAttachment;
        this.ngbModalRef.result.then(
          () => {
            this.router.navigate(['/organisation-attachment', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          () => {
            this.router.navigate(['/organisation-attachment', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
