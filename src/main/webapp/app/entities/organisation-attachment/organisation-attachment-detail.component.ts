import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOrganisationAttachment } from 'app/shared/model/organisation-attachment.model';

@Component({
  selector: 'jhi-organisation-attachment-detail',
  templateUrl: './organisation-attachment-detail.component.html'
})
export class OrganisationAttachmentDetailComponent implements OnInit {
  organisationAttachment: IOrganisationAttachment;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ organisationAttachment }) => {
      this.organisationAttachment = organisationAttachment;
    });
  }

  previousState() {
    window.history.back();
  }
}
