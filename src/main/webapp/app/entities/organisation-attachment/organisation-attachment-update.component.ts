import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { IOrganisationAttachment, OrganisationAttachment } from 'app/shared/model/organisation-attachment.model';
import { OrganisationAttachmentService } from './organisation-attachment.service';
import { IOrganisationMaster } from 'app/shared/model/organisation-master.model';
import { OrganisationMasterService } from 'app/entities/organisation-master/organisation-master.service';
import { IOrganisationDocument } from 'app/shared/model/organisation-document.model';
import { OrganisationDocumentService } from 'app/entities/organisation-document/organisation-document.service';

@Component({
  selector: 'jhi-organisation-attachment-update',
  templateUrl: './organisation-attachment-update.component.html'
})
export class OrganisationAttachmentUpdateComponent implements OnInit {
  isSaving: boolean;

  organisationmasters: IOrganisationMaster[];

  organisationdocuments: IOrganisationDocument[];

  editForm = this.fb.group({
    id: [],
    attachment: [],
    organisationMasterId: [],
    organisationDocumentId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected organisationAttachmentService: OrganisationAttachmentService,
    protected organisationMasterService: OrganisationMasterService,
    protected organisationDocumentService: OrganisationDocumentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ organisationAttachment }) => {
      this.updateForm(organisationAttachment);
    });
    this.organisationMasterService
      .query()
      .subscribe(
        (res: HttpResponse<IOrganisationMaster[]>) => (this.organisationmasters = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.organisationDocumentService
      .query()
      .subscribe(
        (res: HttpResponse<IOrganisationDocument[]>) => (this.organisationdocuments = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(organisationAttachment: IOrganisationAttachment) {
    this.editForm.patchValue({
      id: organisationAttachment.id,
      attachment: organisationAttachment.attachment,
      organisationMasterId: organisationAttachment.organisationMasterId,
      organisationDocumentId: organisationAttachment.organisationDocumentId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const organisationAttachment = this.createFromForm();
    if (organisationAttachment.id !== undefined) {
      this.subscribeToSaveResponse(this.organisationAttachmentService.update(organisationAttachment));
    } else {
      this.subscribeToSaveResponse(this.organisationAttachmentService.create(organisationAttachment));
    }
  }

  private createFromForm(): IOrganisationAttachment {
    return {
      ...new OrganisationAttachment(),
      id: this.editForm.get(['id']).value,
      attachment: this.editForm.get(['attachment']).value,
      organisationMasterId: this.editForm.get(['organisationMasterId']).value,
      organisationDocumentId: this.editForm.get(['organisationDocumentId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrganisationAttachment>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackOrganisationMasterById(index: number, item: IOrganisationMaster) {
    return item.id;
  }

  trackOrganisationDocumentById(index: number, item: IOrganisationDocument) {
    return item.id;
  }
}
