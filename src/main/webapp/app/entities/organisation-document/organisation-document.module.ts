import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { OrganisationDocumentComponent } from './organisation-document.component';
import { OrganisationDocumentDetailComponent } from './organisation-document-detail.component';
import { OrganisationDocumentUpdateComponent } from './organisation-document-update.component';
import {
  OrganisationDocumentDeletePopupComponent,
  OrganisationDocumentDeleteDialogComponent
} from './organisation-document-delete-dialog.component';
import { organisationDocumentRoute, organisationDocumentPopupRoute } from './organisation-document.route';

const ENTITY_STATES = [...organisationDocumentRoute, ...organisationDocumentPopupRoute];

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    OrganisationDocumentComponent,
    OrganisationDocumentDetailComponent,
    OrganisationDocumentUpdateComponent,
    OrganisationDocumentDeleteDialogComponent,
    OrganisationDocumentDeletePopupComponent
  ],
  entryComponents: [OrganisationDocumentDeleteDialogComponent]
})
export class EsignharyanaOrganisationDocumentModule {}
