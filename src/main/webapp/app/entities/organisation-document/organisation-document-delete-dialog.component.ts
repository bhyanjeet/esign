import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOrganisationDocument } from 'app/shared/model/organisation-document.model';
import { OrganisationDocumentService } from './organisation-document.service';

@Component({
  selector: 'jhi-organisation-document-delete-dialog',
  templateUrl: './organisation-document-delete-dialog.component.html'
})
export class OrganisationDocumentDeleteDialogComponent {
  organisationDocument: IOrganisationDocument;

  constructor(
    protected organisationDocumentService: OrganisationDocumentService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.organisationDocumentService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'organisationDocumentListModification',
        content: 'Deleted an organisationDocument'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-organisation-document-delete-popup',
  template: ''
})
export class OrganisationDocumentDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ organisationDocument }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(OrganisationDocumentDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.organisationDocument = organisationDocument;
        this.ngbModalRef.result.then(
          () => {
            this.router.navigate(['/organisation-document', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          () => {
            this.router.navigate(['/organisation-document', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
