import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IOrganisationDocument, OrganisationDocument } from 'app/shared/model/organisation-document.model';
import { OrganisationDocumentService } from './organisation-document.service';
import { IOrganisationTypeMaster } from 'app/shared/model/organisation-type-master.model';
import { OrganisationTypeMasterService } from 'app/entities/organisation-type-master/organisation-type-master.service';

@Component({
  selector: 'jhi-organisation-document-update',
  templateUrl: './organisation-document-update.component.html'
})
export class OrganisationDocumentUpdateComponent implements OnInit {
  isSaving: boolean;

  organisationtypemasters: IOrganisationTypeMaster[];
  createdOnDp: any;
  verifiedOnDp: any;

  editForm = this.fb.group({
    id: [],
    documentTitle: [null, [Validators.required]],
    createdBy: [],
    createdOn: [],
    verifiedBy: [],
    verifiedOn: [],
    remarks: [],
    documentRelatedTo: [],
    organisationTypeMasterId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected organisationDocumentService: OrganisationDocumentService,
    protected organisationTypeMasterService: OrganisationTypeMasterService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ organisationDocument }) => {
      this.updateForm(organisationDocument);
    });
    this.organisationTypeMasterService
      .query()
      .subscribe(
        (res: HttpResponse<IOrganisationTypeMaster[]>) => (this.organisationtypemasters = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(organisationDocument: IOrganisationDocument) {
    this.editForm.patchValue({
      id: organisationDocument.id,
      documentTitle: organisationDocument.documentTitle,
      createdBy: organisationDocument.createdBy,
      createdOn: organisationDocument.createdOn,
      verifiedBy: organisationDocument.verifiedBy,
      verifiedOn: organisationDocument.verifiedOn,
      remarks: organisationDocument.remarks,
      documentRelatedTo: organisationDocument.documentRelatedTo,
      organisationTypeMasterId: organisationDocument.organisationTypeMasterId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const organisationDocument = this.createFromForm();
    if (organisationDocument.id !== undefined) {
      this.subscribeToSaveResponse(this.organisationDocumentService.update(organisationDocument));
    } else {
      this.subscribeToSaveResponse(this.organisationDocumentService.create(organisationDocument));
    }
  }

  private createFromForm(): IOrganisationDocument {
    return {
      ...new OrganisationDocument(),
      id: this.editForm.get(['id']).value,
      documentTitle: this.editForm.get(['documentTitle']).value,
      createdBy: this.editForm.get(['createdBy']).value,
      createdOn: this.editForm.get(['createdOn']).value,
      verifiedBy: this.editForm.get(['verifiedBy']).value,
      verifiedOn: this.editForm.get(['verifiedOn']).value,
      remarks: this.editForm.get(['remarks']).value,
      documentRelatedTo: this.editForm.get(['documentRelatedTo']).value,
      organisationTypeMasterId: this.editForm.get(['organisationTypeMasterId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrganisationDocument>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackOrganisationTypeMasterById(index: number, item: IOrganisationTypeMaster) {
    return item.id;
  }
}
