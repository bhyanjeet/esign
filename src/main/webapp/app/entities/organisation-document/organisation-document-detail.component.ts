import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOrganisationDocument } from 'app/shared/model/organisation-document.model';

@Component({
  selector: 'jhi-organisation-document-detail',
  templateUrl: './organisation-document-detail.component.html'
})
export class OrganisationDocumentDetailComponent implements OnInit {
  organisationDocument: IOrganisationDocument;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ organisationDocument }) => {
      this.organisationDocument = organisationDocument;
    });
  }

  previousState() {
    window.history.back();
  }
}
