import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOrganisationDocument } from 'app/shared/model/organisation-document.model';

type EntityResponseType = HttpResponse<IOrganisationDocument>;
type EntityArrayResponseType = HttpResponse<IOrganisationDocument[]>;

@Injectable({ providedIn: 'root' })
export class OrganisationDocumentService {
  public resourceUrl = SERVER_API_URL + 'api/organisation-documents';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/organisation-documents';
  public resourceSearchDocsUrl = SERVER_API_URL + 'api/organisationTypeId';

  constructor(protected http: HttpClient) {}

  create(organisationDocument: IOrganisationDocument): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(organisationDocument);
    return this.http
      .post<IOrganisationDocument>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(organisationDocument: IOrganisationDocument): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(organisationDocument);
    return this.http
      .put<IOrganisationDocument>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IOrganisationDocument>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IOrganisationDocument[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IOrganisationDocument[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(organisationDocument: IOrganisationDocument): IOrganisationDocument {
    const copy: IOrganisationDocument = Object.assign({}, organisationDocument, {
      createdOn:
        organisationDocument.createdOn != null && organisationDocument.createdOn.isValid()
          ? organisationDocument.createdOn.format(DATE_FORMAT)
          : null,
      verifiedOn:
        organisationDocument.verifiedOn != null && organisationDocument.verifiedOn.isValid()
          ? organisationDocument.verifiedOn.format(DATE_FORMAT)
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
      res.body.verifiedOn = res.body.verifiedOn != null ? moment(res.body.verifiedOn) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((organisationDocument: IOrganisationDocument) => {
        organisationDocument.createdOn = organisationDocument.createdOn != null ? moment(organisationDocument.createdOn) : null;
        organisationDocument.verifiedOn = organisationDocument.verifiedOn != null ? moment(organisationDocument.verifiedOn) : null;
      });
    }
    return res;
  }

  findDocsByOganisationTypeId(organisationDocumentId: any): Observable<EntityArrayResponseType> {
    return this.http
      .get<IOrganisationDocument[]>(`${this.resourceSearchDocsUrl}?id=${organisationDocumentId}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  }
