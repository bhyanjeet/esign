import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { OrganisationDocument } from 'app/shared/model/organisation-document.model';
import { OrganisationDocumentService } from './organisation-document.service';
import { OrganisationDocumentComponent } from './organisation-document.component';
import { OrganisationDocumentDetailComponent } from './organisation-document-detail.component';
import { OrganisationDocumentUpdateComponent } from './organisation-document-update.component';
import { OrganisationDocumentDeletePopupComponent } from './organisation-document-delete-dialog.component';
import { IOrganisationDocument } from 'app/shared/model/organisation-document.model';

@Injectable({ providedIn: 'root' })
export class OrganisationDocumentResolve implements Resolve<IOrganisationDocument> {
  constructor(private service: OrganisationDocumentService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOrganisationDocument> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((organisationDocument: HttpResponse<OrganisationDocument>) => organisationDocument.body));
    }
    return of(new OrganisationDocument());
  }
}

export const organisationDocumentRoute: Routes = [
  {
    path: '',
    component: OrganisationDocumentComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'id,asc',
      pageTitle: 'OrganisationDocuments'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: OrganisationDocumentDetailComponent,
    resolve: {
      organisationDocument: OrganisationDocumentResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationDocuments'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: OrganisationDocumentUpdateComponent,
    resolve: {
      organisationDocument: OrganisationDocumentResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationDocuments'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: OrganisationDocumentUpdateComponent,
    resolve: {
      organisationDocument: OrganisationDocumentResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationDocuments'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const organisationDocumentPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: OrganisationDocumentDeletePopupComponent,
    resolve: {
      organisationDocument: OrganisationDocumentResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationDocuments'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
