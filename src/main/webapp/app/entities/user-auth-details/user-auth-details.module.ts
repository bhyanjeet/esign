import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { UserAuthDetailsComponent } from './user-auth-details.component';
import { UserAuthDetailsDetailComponent } from './user-auth-details-detail.component';
import { UserAuthDetailsUpdateComponent } from './user-auth-details-update.component';
import { UserAuthDetailsDeleteDialogComponent } from './user-auth-details-delete-dialog.component';
import { userAuthDetailsRoute } from './user-auth-details.route';

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(userAuthDetailsRoute)],
  declarations: [
    UserAuthDetailsComponent,
    UserAuthDetailsDetailComponent,
    UserAuthDetailsUpdateComponent,
    UserAuthDetailsDeleteDialogComponent
  ],
  entryComponents: [UserAuthDetailsDeleteDialogComponent]
})
export class EsignharyanaUserAuthDetailsModule {}
