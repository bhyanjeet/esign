import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUserAuthDetails } from 'app/shared/model/user-auth-details.model';

@Component({
  selector: 'jhi-user-auth-details-detail',
  templateUrl: './user-auth-details-detail.component.html'
})
export class UserAuthDetailsDetailComponent implements OnInit {
  userAuthDetails: IUserAuthDetails | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ userAuthDetails }) => (this.userAuthDetails = userAuthDetails));
  }

  previousState(): void {
    window.history.back();
  }
}
