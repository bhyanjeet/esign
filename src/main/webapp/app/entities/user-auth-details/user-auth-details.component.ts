import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IUserAuthDetails } from 'app/shared/model/user-auth-details.model';
import { UserAuthDetailsService } from './user-auth-details.service';
import { UserAuthDetailsDeleteDialogComponent } from './user-auth-details-delete-dialog.component';

@Component({
  selector: 'jhi-user-auth-details',
  templateUrl: './user-auth-details.component.html'
})
export class UserAuthDetailsComponent implements OnInit, OnDestroy {
  userAuthDetails?: IUserAuthDetails[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected userAuthDetailsService: UserAuthDetailsService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.userAuthDetailsService
        .search({
          query: this.currentSearch
        })
        .subscribe((res: HttpResponse<IUserAuthDetails[]>) => (this.userAuthDetails = res.body || []));
      return;
    }

    this.userAuthDetailsService.query().subscribe((res: HttpResponse<IUserAuthDetails[]>) => (this.userAuthDetails = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInUserAuthDetails();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IUserAuthDetails): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInUserAuthDetails(): void {
    this.eventSubscriber = this.eventManager.subscribe('userAuthDetailsListModification', () => this.loadAll());
  }

  delete(userAuthDetails: IUserAuthDetails): void {
    const modalRef = this.modalService.open(UserAuthDetailsDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.userAuthDetails = userAuthDetails;
  }
}
