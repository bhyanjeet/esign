import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IUserAuthDetails } from 'app/shared/model/user-auth-details.model';
import {createRequestOption} from "app/shared/util/request-util";

type EntityResponseType = HttpResponse<IUserAuthDetails>;
type EntityArrayResponseType = HttpResponse<IUserAuthDetails[]>;

@Injectable({ providedIn: 'root' })
export class UserAuthDetailsService {
  public resourceUrl = SERVER_API_URL + 'api/user-auth-details';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/user-auth-details';

  constructor(protected http: HttpClient) {}

  create(userAuthDetails: IUserAuthDetails): Observable<EntityResponseType> {
    return this.http.post<IUserAuthDetails>(this.resourceUrl, userAuthDetails, { observe: 'response' });
  }

  update(userAuthDetails: IUserAuthDetails): Observable<EntityResponseType> {
    return this.http.put<IUserAuthDetails>(this.resourceUrl, userAuthDetails, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUserAuthDetails>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUserAuthDetails[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUserAuthDetails[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
