import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IUserAuthDetails, UserAuthDetails } from 'app/shared/model/user-auth-details.model';
import { UserAuthDetailsService } from './user-auth-details.service';
import { UserAuthDetailsComponent } from './user-auth-details.component';
import { UserAuthDetailsDetailComponent } from './user-auth-details-detail.component';
import { UserAuthDetailsUpdateComponent } from './user-auth-details-update.component';

@Injectable({ providedIn: 'root' })
export class UserAuthDetailsResolve implements Resolve<IUserAuthDetails> {
  constructor(private service: UserAuthDetailsService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUserAuthDetails> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((userAuthDetails: HttpResponse<UserAuthDetails>) => {
          if (userAuthDetails.body) {
            return of(userAuthDetails.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new UserAuthDetails());
  }
}

export const userAuthDetailsRoute: Routes = [
  {
    path: '',
    component: UserAuthDetailsComponent,
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'UserAuthDetails'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: UserAuthDetailsDetailComponent,
    resolve: {
      userAuthDetails: UserAuthDetailsResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'UserAuthDetails'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: UserAuthDetailsUpdateComponent,
    resolve: {
      userAuthDetails: UserAuthDetailsResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'UserAuthDetails'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: UserAuthDetailsUpdateComponent,
    resolve: {
      userAuthDetails: UserAuthDetailsResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'UserAuthDetails'
    },
    canActivate: [UserRouteAccessService]
  }
];
