import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUserAuthDetails } from 'app/shared/model/user-auth-details.model';
import { UserAuthDetailsService } from './user-auth-details.service';

@Component({
  templateUrl: './user-auth-details-delete-dialog.component.html'
})
export class UserAuthDetailsDeleteDialogComponent {
  userAuthDetails?: IUserAuthDetails;

  constructor(
    protected userAuthDetailsService: UserAuthDetailsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.userAuthDetailsService.delete(id).subscribe(() => {
      this.eventManager.broadcast('userAuthDetailsListModification');
      this.activeModal.close();
    });
  }
}
