import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IUserAuthDetails, UserAuthDetails } from 'app/shared/model/user-auth-details.model';
import { UserAuthDetailsService } from './user-auth-details.service';

@Component({
  selector: 'jhi-user-auth-details-update',
  templateUrl: './user-auth-details-update.component.html'
})
export class UserAuthDetailsUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    login: [],
    device: [],
    authToken: []
  });

  constructor(
    protected userAuthDetailsService: UserAuthDetailsService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ userAuthDetails }) => {
      this.updateForm(userAuthDetails);
    });
  }

  updateForm(userAuthDetails: IUserAuthDetails): void {
    this.editForm.patchValue({
      id: userAuthDetails.id,
      login: userAuthDetails.login,
      device: userAuthDetails.device,
      authToken: userAuthDetails.authToken
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const userAuthDetails = this.createFromForm();
    if (userAuthDetails.id !== undefined) {
      this.subscribeToSaveResponse(this.userAuthDetailsService.update(userAuthDetails));
    } else {
      this.subscribeToSaveResponse(this.userAuthDetailsService.create(userAuthDetails));
    }
  }

  private createFromForm(): IUserAuthDetails {
    return {
      ...new UserAuthDetails(),
      id: this.editForm.get(['id'])!.value,
      login: this.editForm.get(['login'])!.value,
      device: this.editForm.get(['device'])!.value,
      authToken: this.editForm.get(['authToken'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserAuthDetails>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
