import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IBlockMaster, BlockMaster } from 'app/shared/model/block-master.model';
import { BlockMasterService } from './block-master.service';
import { IStateMaster } from 'app/shared/model/state-master.model';
import { StateMasterService } from 'app/entities/state-master/state-master.service';
import { IDistrictMaster } from 'app/shared/model/district-master.model';
import { DistrictMasterService } from 'app/entities/district-master/district-master.service';

@Component({
  selector: 'jhi-block-master-update',
  templateUrl: './block-master-update.component.html'
})
export class BlockMasterUpdateComponent implements OnInit {
  isSaving: boolean;

  statemasters: IStateMaster[];

  districtmasters: IDistrictMaster[];
  createdOnDp: any;
  lastUpdatedOnDp: any;
  verifiedOnDp: any;

  editForm = this.fb.group({
    id: [],
    blockCode: [],
    blockName: [null, [Validators.required]],
    createdOn: [],
    lastUpdatedBy: [],
    lastUpdatedOn: [],
    verifiedBy: [],
    verifiedOn: [],
    remarks: [],
    stateMasterId: [],
    districtMasterId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected blockMasterService: BlockMasterService,
    protected stateMasterService: StateMasterService,
    protected districtMasterService: DistrictMasterService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ blockMaster }) => {
      this.updateForm(blockMaster);
    });
    this.stateMasterService
      .query()
      .subscribe(
        (res: HttpResponse<IStateMaster[]>) => (this.statemasters = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.districtMasterService
      .query()
      .subscribe(
        (res: HttpResponse<IDistrictMaster[]>) => (this.districtmasters = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(blockMaster: IBlockMaster) {
    this.editForm.patchValue({
      id: blockMaster.id,
      blockCode: blockMaster.blockCode,
      blockName: blockMaster.blockName,
      createdOn: blockMaster.createdOn,
      lastUpdatedBy: blockMaster.lastUpdatedBy,
      lastUpdatedOn: blockMaster.lastUpdatedOn,
      verifiedBy: blockMaster.verifiedBy,
      verifiedOn: blockMaster.verifiedOn,
      remarks: blockMaster.remarks,
      stateMasterId: blockMaster.stateMasterId,
      districtMasterId: blockMaster.districtMasterId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const blockMaster = this.createFromForm();
    if (blockMaster.id !== undefined) {
      this.subscribeToSaveResponse(this.blockMasterService.update(blockMaster));
    } else {
      this.subscribeToSaveResponse(this.blockMasterService.create(blockMaster));
    }
  }

  private createFromForm(): IBlockMaster {
    return {
      ...new BlockMaster(),
      id: this.editForm.get(['id']).value,
      blockCode: this.editForm.get(['blockCode']).value,
      blockName: this.editForm.get(['blockName']).value,
      createdOn: this.editForm.get(['createdOn']).value,
      lastUpdatedBy: this.editForm.get(['lastUpdatedBy']).value,
      lastUpdatedOn: this.editForm.get(['lastUpdatedOn']).value,
      verifiedBy: this.editForm.get(['verifiedBy']).value,
      verifiedOn: this.editForm.get(['verifiedOn']).value,
      remarks: this.editForm.get(['remarks']).value,
      stateMasterId: this.editForm.get(['stateMasterId']).value,
      districtMasterId: this.editForm.get(['districtMasterId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBlockMaster>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackStateMasterById(index: number, item: IStateMaster) {
    return item.id;
  }

  trackDistrictMasterById(index: number, item: IDistrictMaster) {
    return item.id;
  }
}
