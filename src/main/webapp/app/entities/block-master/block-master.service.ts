import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBlockMaster } from 'app/shared/model/block-master.model';

type EntityResponseType = HttpResponse<IBlockMaster>;
type EntityArrayResponseType = HttpResponse<IBlockMaster[]>;

@Injectable({ providedIn: 'root' })
export class BlockMasterService {
  public resourceUrl = SERVER_API_URL + 'api/block-masters';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/block-masters';

  constructor(protected http: HttpClient) {}

  create(blockMaster: IBlockMaster): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(blockMaster);
    return this.http
      .post<IBlockMaster>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(blockMaster: IBlockMaster): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(blockMaster);
    return this.http
      .put<IBlockMaster>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBlockMaster>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBlockMaster[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBlockMaster[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(blockMaster: IBlockMaster): IBlockMaster {
    const copy: IBlockMaster = Object.assign({}, blockMaster, {
      createdOn: blockMaster.createdOn != null && blockMaster.createdOn.isValid() ? blockMaster.createdOn.format(DATE_FORMAT) : null,
      lastUpdatedOn:
        blockMaster.lastUpdatedOn != null && blockMaster.lastUpdatedOn.isValid() ? blockMaster.lastUpdatedOn.format(DATE_FORMAT) : null,
      verifiedOn: blockMaster.verifiedOn != null && blockMaster.verifiedOn.isValid() ? blockMaster.verifiedOn.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
      res.body.lastUpdatedOn = res.body.lastUpdatedOn != null ? moment(res.body.lastUpdatedOn) : null;
      res.body.verifiedOn = res.body.verifiedOn != null ? moment(res.body.verifiedOn) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((blockMaster: IBlockMaster) => {
        blockMaster.createdOn = blockMaster.createdOn != null ? moment(blockMaster.createdOn) : null;
        blockMaster.lastUpdatedOn = blockMaster.lastUpdatedOn != null ? moment(blockMaster.lastUpdatedOn) : null;
        blockMaster.verifiedOn = blockMaster.verifiedOn != null ? moment(blockMaster.verifiedOn) : null;
      });
    }
    return res;
  }
}
