import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { BlockMaster } from 'app/shared/model/block-master.model';
import { BlockMasterService } from './block-master.service';
import { BlockMasterComponent } from './block-master.component';
import { BlockMasterDetailComponent } from './block-master-detail.component';
import { BlockMasterUpdateComponent } from './block-master-update.component';
import { BlockMasterDeletePopupComponent } from './block-master-delete-dialog.component';
import { IBlockMaster } from 'app/shared/model/block-master.model';

@Injectable({ providedIn: 'root' })
export class BlockMasterResolve implements Resolve<IBlockMaster> {
  constructor(private service: BlockMasterService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBlockMaster> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((blockMaster: HttpResponse<BlockMaster>) => blockMaster.body));
    }
    return of(new BlockMaster());
  }
}

export const blockMasterRoute: Routes = [
  {
    path: '',
    component: BlockMasterComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'id,asc',
      pageTitle: 'BlockMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BlockMasterDetailComponent,
    resolve: {
      blockMaster: BlockMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'BlockMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BlockMasterUpdateComponent,
    resolve: {
      blockMaster: BlockMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'BlockMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BlockMasterUpdateComponent,
    resolve: {
      blockMaster: BlockMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'BlockMasters'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const blockMasterPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: BlockMasterDeletePopupComponent,
    resolve: {
      blockMaster: BlockMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'BlockMasters'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
