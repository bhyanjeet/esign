import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { BlockMasterComponent } from './block-master.component';
import { BlockMasterDetailComponent } from './block-master-detail.component';
import { BlockMasterUpdateComponent } from './block-master-update.component';
import { BlockMasterDeletePopupComponent, BlockMasterDeleteDialogComponent } from './block-master-delete-dialog.component';
import { blockMasterRoute, blockMasterPopupRoute } from './block-master.route';

const ENTITY_STATES = [...blockMasterRoute, ...blockMasterPopupRoute];

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    BlockMasterComponent,
    BlockMasterDetailComponent,
    BlockMasterUpdateComponent,
    BlockMasterDeleteDialogComponent,
    BlockMasterDeletePopupComponent
  ],
  entryComponents: [BlockMasterDeleteDialogComponent]
})
export class EsignharyanaBlockMasterModule {}
