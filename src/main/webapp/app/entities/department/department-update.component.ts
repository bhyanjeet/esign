import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IDepartment, Department } from 'app/shared/model/department.model';
import { DepartmentService } from './department.service';

@Component({
  selector: 'jhi-department-update',
  templateUrl: './department-update.component.html'
})
export class DepartmentUpdateComponent implements OnInit {
  isSaving = false;
  createdOnDp: any;
  updatedOnDp: any;

  editForm = this.fb.group({
    id: [],
    departmentName: [],
    createdOn: [],
    createdBy: [],
    updatedOn: [],
    updatedBy: [],
    status: [],
    remark: [],
    whiteListIp1: [],
    whiteListIp2: []
  });

  constructor(protected departmentService: DepartmentService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ department }) => {
      this.updateForm(department);
    });
  }

  updateForm(department: IDepartment): void {
    this.editForm.patchValue({
      id: department.id,
      departmentName: department.departmentName,
      createdOn: department.createdOn,
      createdBy: department.createdBy,
      updatedOn: department.updatedOn,
      updatedBy: department.updatedBy,
      status: department.status,
      remark: department.remark,
      whiteListIp1: department.whiteListIp1,
      whiteListIp2: department.whiteListIp2
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const department = this.createFromForm();
    if (department.id !== undefined) {
      this.subscribeToSaveResponse(this.departmentService.update(department));
    } else {
      this.subscribeToSaveResponse(this.departmentService.create(department));
    }
  }

  private createFromForm(): IDepartment {
    return {
      ...new Department(),
      id: this.editForm.get(['id'])!.value,
      departmentName: this.editForm.get(['departmentName'])!.value,
      createdOn: this.editForm.get(['createdOn'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value,
      updatedOn: this.editForm.get(['updatedOn'])!.value,
      updatedBy: this.editForm.get(['updatedBy'])!.value,
      status: this.editForm.get(['status'])!.value,
      remark: this.editForm.get(['remark'])!.value,
      whiteListIp1: this.editForm.get(['whiteListIp1'])!.value,
      whiteListIp2: this.editForm.get(['whiteListIp2'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDepartment>>) {
    result.subscribe((res: HttpResponse<any>) => this.onSaveSuccess(res.body), response => this.onSaveError(response));
  }

  protected onSaveSuccess(result) {

    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(response) {
    this.isSaving = false;
    alert(response.error.title);
  }
}
