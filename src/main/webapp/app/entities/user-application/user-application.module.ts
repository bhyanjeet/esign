import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { UserApplicationComponent } from './user-application.component';
import { UserApplicationDetailComponent } from './user-application-detail.component';
import { UserApplicationUpdateComponent } from './user-application-update.component';
import { UserApplicationDeletePopupComponent, UserApplicationDeleteDialogComponent } from './user-application-delete-dialog.component';
import { userApplicationRoute, userApplicationPopupRoute } from './user-application.route';

const ENTITY_STATES = [...userApplicationRoute, ...userApplicationPopupRoute];

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    UserApplicationComponent,
    UserApplicationDetailComponent,
    UserApplicationUpdateComponent,
    UserApplicationDeleteDialogComponent,
    UserApplicationDeletePopupComponent
  ],
  entryComponents: [UserApplicationDeleteDialogComponent]
})
export class EsignharyanaUserApplicationModule {}
