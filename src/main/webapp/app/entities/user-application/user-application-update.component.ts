import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IUserApplication, UserApplication } from 'app/shared/model/user-application.model';
import { UserApplicationService } from './user-application.service';
import { IApplicationMaster } from 'app/shared/model/application-master.model';
import { ApplicationMasterService } from 'app/entities/application-master/application-master.service';

@Component({
  selector: 'jhi-user-application-update',
  templateUrl: './user-application-update.component.html'
})
export class UserApplicationUpdateComponent implements OnInit {
  isSaving: boolean;

  applicationmasters: IApplicationMaster[];
  createdOnDp: any;
  updatedOnDp: any;
  verifiedOnDp: any;

  editForm = this.fb.group({
    id: [],
    userId: [],
    userLogin: [],
    createdBy: [],
    createdOn: [],
    updatedBy: [],
    updatedOn: [],
    verifiedBy: [],
    verifiedOn: [],
    userCodeId: [],
    applicationCodeId: [],
    applicationMasterId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected userApplicationService: UserApplicationService,
    protected applicationMasterService: ApplicationMasterService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ userApplication }) => {
      this.updateForm(userApplication);
    });
    this.applicationMasterService
      .query()
      .subscribe(
        (res: HttpResponse<IApplicationMaster[]>) => (this.applicationmasters = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(userApplication: IUserApplication) {
    this.editForm.patchValue({
      id: userApplication.id,
      userId: userApplication.userId,
      userLogin: userApplication.userLogin,
      createdBy: userApplication.createdBy,
      createdOn: userApplication.createdOn,
      updatedBy: userApplication.updatedBy,
      updatedOn: userApplication.updatedOn,
      verifiedBy: userApplication.verifiedBy,
      verifiedOn: userApplication.verifiedOn,
      userCodeId: userApplication.userCodeId,
      applicationCodeId: userApplication.applicationCodeId,
      applicationMasterId: userApplication.applicationMasterId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const userApplication = this.createFromForm();
    if (userApplication.id !== undefined) {
      this.subscribeToSaveResponse(this.userApplicationService.update(userApplication));
    } else {
      this.subscribeToSaveResponse(this.userApplicationService.create(userApplication));
    }
  }

  private createFromForm(): IUserApplication {
    return {
      ...new UserApplication(),
      id: this.editForm.get(['id']).value,
      userId: this.editForm.get(['userId']).value,
      userLogin: this.editForm.get(['userLogin']).value,
      createdBy: this.editForm.get(['createdBy']).value,
      createdOn: this.editForm.get(['createdOn']).value,
      updatedBy: this.editForm.get(['updatedBy']).value,
      updatedOn: this.editForm.get(['updatedOn']).value,
      verifiedBy: this.editForm.get(['verifiedBy']).value,
      verifiedOn: this.editForm.get(['verifiedOn']).value,
      userCodeId: this.editForm.get(['userCodeId']).value,
      applicationCodeId: this.editForm.get(['applicationCodeId']).value,
      applicationMasterId: this.editForm.get(['applicationMasterId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserApplication>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackApplicationMasterById(index: number, item: IApplicationMaster) {
    return item.id;
  }
}
