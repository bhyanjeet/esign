import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUserApplication } from 'app/shared/model/user-application.model';

@Component({
  selector: 'jhi-user-application-detail',
  templateUrl: './user-application-detail.component.html'
})
export class UserApplicationDetailComponent implements OnInit {
  userApplication: IUserApplication;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userApplication }) => {
      this.userApplication = userApplication;
    });
  }

  previousState() {
    window.history.back();
  }
}
