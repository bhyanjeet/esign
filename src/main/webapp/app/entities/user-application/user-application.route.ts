import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserApplication } from 'app/shared/model/user-application.model';
import { UserApplicationService } from './user-application.service';
import { UserApplicationComponent } from './user-application.component';
import { UserApplicationDetailComponent } from './user-application-detail.component';
import { UserApplicationUpdateComponent } from './user-application-update.component';
import { UserApplicationDeletePopupComponent } from './user-application-delete-dialog.component';
import { IUserApplication } from 'app/shared/model/user-application.model';

@Injectable({ providedIn: 'root' })
export class UserApplicationResolve implements Resolve<IUserApplication> {
  constructor(private service: UserApplicationService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUserApplication> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((userApplication: HttpResponse<UserApplication>) => userApplication.body));
    }
    return of(new UserApplication());
  }
}

export const userApplicationRoute: Routes = [
  {
    path: '',
    component: UserApplicationComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'id,asc',
      pageTitle: 'UserApplications'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: UserApplicationDetailComponent,
    resolve: {
      userApplication: UserApplicationResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'UserApplications'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: UserApplicationUpdateComponent,
    resolve: {
      userApplication: UserApplicationResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'UserApplications'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: UserApplicationUpdateComponent,
    resolve: {
      userApplication: UserApplicationResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'UserApplications'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const userApplicationPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: UserApplicationDeletePopupComponent,
    resolve: {
      userApplication: UserApplicationResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'UserApplications'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
