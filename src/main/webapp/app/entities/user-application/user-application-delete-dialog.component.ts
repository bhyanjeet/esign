import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUserApplication } from 'app/shared/model/user-application.model';
import { UserApplicationService } from './user-application.service';

@Component({
  selector: 'jhi-user-application-delete-dialog',
  templateUrl: './user-application-delete-dialog.component.html'
})
export class UserApplicationDeleteDialogComponent {
  userApplication: IUserApplication;

  constructor(
    protected userApplicationService: UserApplicationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.userApplicationService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'userApplicationListModification',
        content: 'Deleted an userApplication'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-user-application-delete-popup',
  template: ''
})
export class UserApplicationDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userApplication }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(UserApplicationDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.userApplication = userApplication;
        this.ngbModalRef.result.then(
          () => {
            this.router.navigate(['/user-application', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          () => {
            this.router.navigate(['/user-application', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
