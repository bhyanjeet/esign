import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IUserApplication } from 'app/shared/model/user-application.model';
import {IApplicationMaster} from "app/shared/model/application-master.model";
import {IOrganisationDocument} from "app/shared/model/organisation-document.model";

type EntityResponseType = HttpResponse<IUserApplication>;
type EntityArrayResponseType = HttpResponse<IUserApplication[]>;

@Injectable({ providedIn: 'root' })
export class UserApplicationService {
  public resourceUrl = SERVER_API_URL + 'api/user-applications';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/user-applications';
  public resourceSearchUrlUserApplicationByUserLogin = SERVER_API_URL + 'api/userApplicationByUserLogin';
  public resourceSearchUrlUserApplicationByUserId = SERVER_API_URL + 'api/userApplicationByUserId';

  constructor(protected http: HttpClient) {}

  create(userApplication: IUserApplication): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(userApplication);
    return this.http
      .post<IUserApplication>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(userApplication: IUserApplication): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(userApplication);
    return this.http
      .put<IUserApplication>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IUserApplication>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IUserApplication[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IUserApplication[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(userApplication: IUserApplication): IUserApplication {
    const copy: IUserApplication = Object.assign({}, userApplication, {
      createdOn:
        userApplication.createdOn != null && userApplication.createdOn.isValid() ? userApplication.createdOn.format(DATE_FORMAT) : null,
      updatedOn:
        userApplication.updatedOn != null && userApplication.updatedOn.isValid() ? userApplication.updatedOn.format(DATE_FORMAT) : null,
      verifiedOn:
        userApplication.verifiedOn != null && userApplication.verifiedOn.isValid() ? userApplication.verifiedOn.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
      res.body.updatedOn = res.body.updatedOn != null ? moment(res.body.updatedOn) : null;
      res.body.verifiedOn = res.body.verifiedOn != null ? moment(res.body.verifiedOn) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((userApplication: IUserApplication) => {
        userApplication.createdOn = userApplication.createdOn != null ? moment(userApplication.createdOn) : null;
        userApplication.updatedOn = userApplication.updatedOn != null ? moment(userApplication.updatedOn) : null;
        userApplication.verifiedOn = userApplication.verifiedOn != null ? moment(userApplication.verifiedOn) : null;
      });
    }
    return res;
  }

  findUserApplicationByLogin(): Observable<EntityArrayResponseType> {
    return this.http
      .get<IUserApplication[]>(`${this.resourceSearchUrlUserApplicationByUserLogin}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  findApplicationByUserId(userId: any): Observable<EntityArrayResponseType> {
    return this.http
      .get<IUserApplication[]>(`${this.resourceSearchUrlUserApplicationByUserId}?userId=${userId}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

}
