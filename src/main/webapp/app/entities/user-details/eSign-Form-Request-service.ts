import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IUserDetails } from 'app/shared/model/user-details.model';
import {IOrganisationDocument} from "app/shared/model/organisation-document.model";
import {IOrganisationMaster} from "app/shared/model/organisation-master.model";
import {IEsignFormRequest} from "app/shared/model/esign-form-request.model";
import any = jasmine.any;

type EntityResponseType = HttpResponse<IEsignFormRequest>;
type EntityArrayResponseType = HttpResponse<IEsignFormRequest>;

@Injectable({ providedIn: 'root' })
export class ESignFormRequestService {

  public resourceUrleSignFormRequest = SERVER_API_URL + 'api/eSign';
  public resourceUrleSigneSignStatus = SERVER_API_URL + 'api/eSignStatus';

  constructor(protected http: HttpClient) {}

  protected convertDateFromClient(esignFormRequest: IEsignFormRequest): IEsignFormRequest {
    const copy: IEsignFormRequest = Object.assign({}, esignFormRequest, {

    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      // res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
      // res.body.lastUpdatedOn = res.body.lastUpdatedOn != null ? moment(res.body.lastUpdatedOn) : null;
      // res.body.verifiedOn = res.body.verifiedOn != null ? moment(res.body.verifiedOn) : null;
    }
    return res;
  }
  sendRedirect(){
    const responseType = 'text' as 'json';
    return this.http
      .get(this.resourceUrleSigneSignStatus, {
        observe: 'response' as 'body',
        responseType

      });

  }

  eSignRequest(esignFormRequest: IEsignFormRequest): Observable<EntityResponseType> {
     const copy = this.convertDateFromClient(esignFormRequest);
    return this.http
      .post<IEsignFormRequest>(this.resourceUrleSignFormRequest, copy, { observe: 'response' })
       .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }


}
