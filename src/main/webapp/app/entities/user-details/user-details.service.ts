import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IUserDetails } from 'app/shared/model/user-details.model';
import {IOrganisationDocument} from "app/shared/model/organisation-document.model";
import {IOrganisationMaster} from "app/shared/model/organisation-master.model";

type EntityResponseType = HttpResponse<IUserDetails>;
type EntityArrayResponseType = HttpResponse<IUserDetails[]>;

@Injectable({ providedIn: 'root' })
export class UserDetailsService {
  public resourceUrl = SERVER_API_URL + 'api/user-details';
  public resourceUrlById = SERVER_API_URL + 'api/user-detailsById';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/user-details';
  public resourceSearchUrlByUserLogin = SERVER_API_URL + 'api/userByUserLogin';
  public resourceSearchUrlByUserStatus = SERVER_API_URL + 'api/userByUserStatus';
  public resourceSearchUrlVerifyUser = SERVER_API_URL + 'api/verifyUser';
  public resourceUrlByUserCodeId = SERVER_API_URL + 'api/findByUserCodeId';

  constructor(protected http: HttpClient) {}

  create(userDetails: IUserDetails): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(userDetails);
    return this.http
      .post<IUserDetails>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(userDetails: IUserDetails): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(userDetails);
    return this.http
      .put<IUserDetails>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IUserDetails>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  findById(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IUserDetails>(`${this.resourceUrlById}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }


  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IUserDetails[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IUserDetails[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(userDetails: IUserDetails): IUserDetails {
    const copy: IUserDetails = Object.assign({}, userDetails, {
      // createdOn: userDetails.createdOn != null && userDetails.createdOn.isValid() ? userDetails.createdOn.format(DATE_FORMAT) : null,
      // lastUpdatedOn:
      //   userDetails.lastUpdatedOn != null && userDetails.lastUpdatedOn.isValid() ? userDetails.lastUpdatedOn.format(DATE_FORMAT) : null,
      // verifiedOn: userDetails.verifiedOn != null && userDetails.verifiedOn.isValid() ? userDetails.verifiedOn.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
     // res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
     // res.body.lastUpdatedOn = res.body.lastUpdatedOn != null ? moment(res.body.lastUpdatedOn) : null;
     // res.body.verifiedOn = res.body.verifiedOn != null ? moment(res.body.verifiedOn) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((userDetails: IUserDetails) => {
        userDetails.createdOn = userDetails.createdOn != null ? moment(userDetails.createdOn) : null;
        userDetails.lastUpdatedOn = userDetails.lastUpdatedOn != null ? moment(userDetails.lastUpdatedOn) : null;
        userDetails.verifiedOn = userDetails.verifiedOn != null ? moment(userDetails.verifiedOn) : null;
      });
    }
    return res;
  }

  findUserByUserLogin(): Observable<EntityArrayResponseType> {
    return this.http
      .get<IUserDetails[]>(`${this.resourceSearchUrlByUserLogin}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  getVerifiedUserStatusRecord(): Observable<EntityArrayResponseType> {
    return this.http
      .get<IUserDetails[]>(`${this.resourceSearchUrlByUserStatus}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  verifyUser(userCodeId: any) {
    const value = new HttpParams().set('userCodeId', userCodeId);
    return this.http.get<IUserDetails>(`${this.resourceSearchUrlVerifyUser}/`, {
      params: value,
      observe: 'response'
    });
  }

  finByUserCodeId(finByUserCodeId: any): Observable<EntityResponseType> {
    return this.http
      .get<IUserDetails>(`${this.resourceUrlByUserCodeId}/${finByUserCodeId}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

}
