import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IUserDetails, UserDetails } from 'app/shared/model/user-details.model';
import { UserDetailsService } from './user-details.service';
import { IDesignationMaster } from 'app/shared/model/designation-master.model';
import { DesignationMasterService } from 'app/entities/designation-master/designation-master.service';
import { IOrganisationMaster } from 'app/shared/model/organisation-master.model';
import { OrganisationMasterService } from 'app/entities/organisation-master/organisation-master.service';
import { IEsignRole } from 'app/shared/model/esign-role.model';
import { EsignRoleService } from 'app/entities/esign-role/esign-role.service';
import { IApplicationMaster } from 'app/shared/model/application-master.model';
import { ApplicationMasterService } from 'app/entities/application-master/application-master.service';

type SelectableEntity = IDesignationMaster | IOrganisationMaster | IEsignRole | IApplicationMaster;

@Component({
  selector: 'jhi-user-details-update',
  templateUrl: './user-details-update.component.html'
})
export class UserDetailsUpdateComponent implements OnInit {
  isSaving = false;
  designationmasters: IDesignationMaster[] = [];
  organisationmasters: IOrganisationMaster[] = [];
  esignroles: IEsignRole[] = [];
  applicationmasters: IApplicationMaster[] = [];
  createdOnDp: any;
  lastUpdatedOnDp: any;
  verifiedOnDp: any;
  dobDp: any;

  editForm = this.fb.group({
    id: [],
    userFullName: [],
    userPhoneMobile: [null, [Validators.required]],
    userPhoneLandline: [],
    userOfficialEmail: [null, [Validators.required]],
    userAlternativeEmail: [],
    userLoginPassword: [],
    userLoginPasswordSalt: [],
    userTransactionPassword: [],
    userTransactionPasswordSalt: [],
    createdBy: [],
    createdOn: [],
    lastUpdatedBy: [],
    lastUpdatedOn: [],
    verifiedBy: [],
    verifiedOn: [],
    remarks: [],
    userIdCode: [null, []],
    status: [],
    username: [],
    userPIN: [],
    organisationUnit: [],
    address: [],
    counrty: [],
    postalCode: [],
    photograph: [],
    dob: [],
    gender: [],
    pan: [],
    aadhaar: [],
    panAttachment: [],
    employeeId: [],
    employeeCardAttachment: [],
    signerId: [],
    lastAction: [],
    accountStatus: [],
    designationMasterId: [],
    organisationMasterId: [],
    esignRoleId: [],
    applicationMasterId: []
  });

  constructor(
    protected userDetailsService: UserDetailsService,
    protected designationMasterService: DesignationMasterService,
    protected organisationMasterService: OrganisationMasterService,
    protected esignRoleService: EsignRoleService,
    protected applicationMasterService: ApplicationMasterService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ userDetails }) => {
      this.updateForm(userDetails);

      this.designationMasterService
        .query()
        .subscribe((res: HttpResponse<IDesignationMaster[]>) => (this.designationmasters = res.body || []));

      this.organisationMasterService
        .query()
        .subscribe((res: HttpResponse<IOrganisationMaster[]>) => (this.organisationmasters = res.body || []));

      this.esignRoleService.query().subscribe((res: HttpResponse<IEsignRole[]>) => (this.esignroles = res.body || []));

      this.applicationMasterService
        .query()
        .subscribe((res: HttpResponse<IApplicationMaster[]>) => (this.applicationmasters = res.body || []));
    });
  }

  updateForm(userDetails: IUserDetails): void {
    this.editForm.patchValue({
      id: userDetails.id,
      userFullName: userDetails.userFullName,
      userPhoneMobile: userDetails.userPhoneMobile,
      userPhoneLandline: userDetails.userPhoneLandline,
      userOfficialEmail: userDetails.userOfficialEmail,
      userAlternativeEmail: userDetails.userAlternativeEmail,
      userLoginPassword: userDetails.userLoginPassword,
      userLoginPasswordSalt: userDetails.userLoginPasswordSalt,
      userTransactionPassword: userDetails.userTransactionPassword,
      userTransactionPasswordSalt: userDetails.userTransactionPasswordSalt,
      createdBy: userDetails.createdBy,
      createdOn: userDetails.createdOn,
      lastUpdatedBy: userDetails.lastUpdatedBy,
      lastUpdatedOn: userDetails.lastUpdatedOn,
      verifiedBy: userDetails.verifiedBy,
      verifiedOn: userDetails.verifiedOn,
      remarks: userDetails.remarks,
      userIdCode: userDetails.userIdCode,
      status: userDetails.status,
      username: userDetails.username,
      userPIN: userDetails.userPIN,
      organisationUnit: userDetails.organisationUnit,
      address: userDetails.address,
      counrty: userDetails.counrty,
      postalCode: userDetails.postalCode,
      photograph: userDetails.photograph,
      dob: userDetails.dob,
      gender: userDetails.gender,
      pan: userDetails.pan,
      aadhaar: userDetails.aadhaar,
      panAttachment: userDetails.panAttachment,
      employeeId: userDetails.employeeId,
      employeeCardAttachment: userDetails.employeeCardAttachment,
      signerId: userDetails.signerId,
      lastAction: userDetails.lastAction,
      accountStatus: userDetails.accountStatus,
      designationMasterId: userDetails.designationMasterId,
      organisationMasterId: userDetails.organisationMasterId,
      esignRoleId: userDetails.esignRoleId,
      applicationMasterId: userDetails.applicationMasterId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const userDetails = this.createFromForm();
    if (userDetails.id !== undefined) {
      this.subscribeToSaveResponse(this.userDetailsService.update(userDetails));
    } else {
      this.subscribeToSaveResponse(this.userDetailsService.create(userDetails));
    }
  }

  private createFromForm(): IUserDetails {
    return {
      ...new UserDetails(),
      id: this.editForm.get(['id'])!.value,
      userFullName: this.editForm.get(['userFullName'])!.value,
      userPhoneMobile: this.editForm.get(['userPhoneMobile'])!.value,
      userPhoneLandline: this.editForm.get(['userPhoneLandline'])!.value,
      userOfficialEmail: this.editForm.get(['userOfficialEmail'])!.value,
      userAlternativeEmail: this.editForm.get(['userAlternativeEmail'])!.value,
      userLoginPassword: this.editForm.get(['userLoginPassword'])!.value,
      userLoginPasswordSalt: this.editForm.get(['userLoginPasswordSalt'])!.value,
      userTransactionPassword: this.editForm.get(['userTransactionPassword'])!.value,
      userTransactionPasswordSalt: this.editForm.get(['userTransactionPasswordSalt'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value,
      createdOn: this.editForm.get(['createdOn'])!.value,
      lastUpdatedBy: this.editForm.get(['lastUpdatedBy'])!.value,
      lastUpdatedOn: this.editForm.get(['lastUpdatedOn'])!.value,
      verifiedBy: this.editForm.get(['verifiedBy'])!.value,
      verifiedOn: this.editForm.get(['verifiedOn'])!.value,
      remarks: this.editForm.get(['remarks'])!.value,
      userIdCode: this.editForm.get(['userIdCode'])!.value,
      status: this.editForm.get(['status'])!.value,
      username: this.editForm.get(['username'])!.value,
      userPIN: this.editForm.get(['userPIN'])!.value,
      organisationUnit: this.editForm.get(['organisationUnit'])!.value,
      address: this.editForm.get(['address'])!.value,
      counrty: this.editForm.get(['counrty'])!.value,
      postalCode: this.editForm.get(['postalCode'])!.value,
      photograph: this.editForm.get(['photograph'])!.value,
      dob: this.editForm.get(['dob'])!.value,
      gender: this.editForm.get(['gender'])!.value,
      pan: this.editForm.get(['pan'])!.value,
      aadhaar: this.editForm.get(['aadhaar'])!.value,
      panAttachment: this.editForm.get(['panAttachment'])!.value,
      employeeId: this.editForm.get(['employeeId'])!.value,
      employeeCardAttachment: this.editForm.get(['employeeCardAttachment'])!.value,
      signerId: this.editForm.get(['signerId'])!.value,
      lastAction: this.editForm.get(['lastAction'])!.value,
      accountStatus: this.editForm.get(['accountStatus'])!.value,
      designationMasterId: this.editForm.get(['designationMasterId'])!.value,
      organisationMasterId: this.editForm.get(['organisationMasterId'])!.value,
      esignRoleId: this.editForm.get(['esignRoleId'])!.value,
      applicationMasterId: this.editForm.get(['applicationMasterId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserDetails>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
