import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEsignError } from 'app/shared/model/esign-error.model';
import { EsignErrorService } from './esign-error.service';

@Component({
  templateUrl: './esign-error-delete-dialog.component.html'
})
export class EsignErrorDeleteDialogComponent {
  esignError?: IEsignError;

  constructor(
    protected esignErrorService: EsignErrorService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.esignErrorService.delete(id).subscribe(() => {
      this.eventManager.broadcast('esignErrorListModification');
      this.activeModal.close();
    });
  }
}
