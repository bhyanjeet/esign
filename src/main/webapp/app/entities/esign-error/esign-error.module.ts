import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { EsignErrorComponent } from './esign-error.component';
import { EsignErrorDetailComponent } from './esign-error-detail.component';
import { EsignErrorUpdateComponent } from './esign-error-update.component';
import { EsignErrorDeleteDialogComponent } from './esign-error-delete-dialog.component';
import { esignErrorRoute } from './esign-error.route';

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(esignErrorRoute)],
  declarations: [EsignErrorComponent, EsignErrorDetailComponent, EsignErrorUpdateComponent, EsignErrorDeleteDialogComponent],
  entryComponents: [EsignErrorDeleteDialogComponent]
})
export class EsignharyanaEsignErrorModule {}
