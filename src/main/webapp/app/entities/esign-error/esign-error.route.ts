import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IEsignError, EsignError } from 'app/shared/model/esign-error.model';
import { EsignErrorService } from './esign-error.service';
import { EsignErrorComponent } from './esign-error.component';
import { EsignErrorDetailComponent } from './esign-error-detail.component';
import { EsignErrorUpdateComponent } from './esign-error-update.component';

@Injectable({ providedIn: 'root' })
export class EsignErrorResolve implements Resolve<IEsignError> {
  constructor(private service: EsignErrorService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEsignError> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((esignError: HttpResponse<EsignError>) => {
          if (esignError.body) {
            return of(esignError.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new EsignError());
  }
}

export const esignErrorRoute: Routes = [
  {
    path: '',
    component: EsignErrorComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'EsignErrors'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: EsignErrorDetailComponent,
    resolve: {
      esignError: EsignErrorResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'EsignErrors'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: EsignErrorUpdateComponent,
    resolve: {
      esignError: EsignErrorResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'EsignErrors'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: EsignErrorUpdateComponent,
    resolve: {
      esignError: EsignErrorResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'EsignErrors'
    },
    canActivate: [UserRouteAccessService]
  }
];
