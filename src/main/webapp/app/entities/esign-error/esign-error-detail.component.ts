import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEsignError } from 'app/shared/model/esign-error.model';

@Component({
  selector: 'jhi-esign-error-detail',
  templateUrl: './esign-error-detail.component.html'
})
export class EsignErrorDetailComponent implements OnInit {
  esignError: IEsignError | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ esignError }) => (this.esignError = esignError));
  }

  previousState(): void {
    window.history.back();
  }
}
