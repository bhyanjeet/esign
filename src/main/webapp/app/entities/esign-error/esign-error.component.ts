import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IEsignError } from 'app/shared/model/esign-error.model';
import { EsignErrorService } from './esign-error.service';
import { EsignErrorDeleteDialogComponent } from './esign-error-delete-dialog.component';

@Component({
  selector: 'jhi-esign-error',
  templateUrl: './esign-error.component.html'
})
export class EsignErrorComponent implements OnInit, OnDestroy {
  esignErrors?: IEsignError[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected esignErrorService: EsignErrorService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.esignErrorService
        .search({
          query: this.currentSearch
        })
        .subscribe((res: HttpResponse<IEsignError[]>) => (this.esignErrors = res.body || []));
      return;
    }

    this.esignErrorService.query().subscribe((res: HttpResponse<IEsignError[]>) => (this.esignErrors = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInEsignErrors();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IEsignError): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInEsignErrors(): void {
    this.eventSubscriber = this.eventManager.subscribe('esignErrorListModification', () => this.loadAll());
  }

  delete(esignError: IEsignError): void {
    const modalRef = this.modalService.open(EsignErrorDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.esignError = esignError;
  }
}
