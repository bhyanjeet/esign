import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IEsignError } from 'app/shared/model/esign-error.model';

type EntityResponseType = HttpResponse<IEsignError>;
type EntityArrayResponseType = HttpResponse<IEsignError[]>;

@Injectable({ providedIn: 'root' })
export class EsignErrorService {
  public resourceUrl = SERVER_API_URL + 'api/esign-errors';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/esign-errors';

  constructor(protected http: HttpClient) {}

  create(esignError: IEsignError): Observable<EntityResponseType> {
    return this.http.post<IEsignError>(this.resourceUrl, esignError, { observe: 'response' });
  }

  update(esignError: IEsignError): Observable<EntityResponseType> {
    return this.http.put<IEsignError>(this.resourceUrl, esignError, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IEsignError>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEsignError[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEsignError[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
