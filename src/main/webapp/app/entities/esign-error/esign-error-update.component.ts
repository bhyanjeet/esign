import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IEsignError, EsignError } from 'app/shared/model/esign-error.model';
import { EsignErrorService } from './esign-error.service';

@Component({
  selector: 'jhi-esign-error-update',
  templateUrl: './esign-error-update.component.html'
})
export class EsignErrorUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    errorCode: [],
    errorMessage: [],
    errorStage: []
  });

  constructor(protected esignErrorService: EsignErrorService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ esignError }) => {
      this.updateForm(esignError);
    });
  }

  updateForm(esignError: IEsignError): void {
    this.editForm.patchValue({
      id: esignError.id,
      errorCode: esignError.errorCode,
      errorMessage: esignError.errorMessage,
      errorStage: esignError.errorStage
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const esignError = this.createFromForm();
    if (esignError.id !== undefined) {
      this.subscribeToSaveResponse(this.esignErrorService.update(esignError));
    } else {
      this.subscribeToSaveResponse(this.esignErrorService.create(esignError));
    }
  }

  private createFromForm(): IEsignError {
    return {
      ...new EsignError(),
      id: this.editForm.get(['id'])!.value,
      errorCode: this.editForm.get(['errorCode'])!.value,
      errorMessage: this.editForm.get(['errorMessage'])!.value,
      errorStage: this.editForm.get(['errorStage'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEsignError>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
