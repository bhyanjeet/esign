import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { UserLogsComponent } from './user-logs.component';
import { UserLogsDetailComponent } from './user-logs-detail.component';
import { UserLogsUpdateComponent } from './user-logs-update.component';
import { UserLogsDeletePopupComponent, UserLogsDeleteDialogComponent } from './user-logs-delete-dialog.component';
import { userLogsRoute, userLogsPopupRoute } from './user-logs.route';

const ENTITY_STATES = [...userLogsRoute, ...userLogsPopupRoute];

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    UserLogsComponent,
    UserLogsDetailComponent,
    UserLogsUpdateComponent,
    UserLogsDeleteDialogComponent,
    UserLogsDeletePopupComponent
  ],
  entryComponents: [UserLogsDeleteDialogComponent]
})
export class EsignharyanaUserLogsModule {}
