import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUserLogs } from 'app/shared/model/user-logs.model';

@Component({
  selector: 'jhi-user-logs-detail',
  templateUrl: './user-logs-detail.component.html'
})
export class UserLogsDetailComponent implements OnInit {
  userLogs: IUserLogs;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userLogs }) => {
      this.userLogs = userLogs;
    });
  }

  previousState() {
    window.history.back();
  }
}
