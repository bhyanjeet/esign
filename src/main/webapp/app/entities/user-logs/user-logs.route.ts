import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserLogs } from 'app/shared/model/user-logs.model';
import { UserLogsService } from './user-logs.service';
import { UserLogsComponent } from './user-logs.component';
import { UserLogsDetailComponent } from './user-logs-detail.component';
import { UserLogsUpdateComponent } from './user-logs-update.component';
import { UserLogsDeletePopupComponent } from './user-logs-delete-dialog.component';
import { IUserLogs } from 'app/shared/model/user-logs.model';

@Injectable({ providedIn: 'root' })
export class UserLogsResolve implements Resolve<IUserLogs> {
  constructor(private service: UserLogsService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUserLogs> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((userLogs: HttpResponse<UserLogs>) => userLogs.body));
    }
    return of(new UserLogs());
  }
}

export const userLogsRoute: Routes = [
  {
    path: '',
    component: UserLogsComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'id,asc',
      pageTitle: 'UserLogs'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: UserLogsDetailComponent,
    resolve: {
      userLogs: UserLogsResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'UserLogs'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: UserLogsUpdateComponent,
    resolve: {
      userLogs: UserLogsResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'UserLogs'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: UserLogsUpdateComponent,
    resolve: {
      userLogs: UserLogsResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'UserLogs'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const userLogsPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: UserLogsDeletePopupComponent,
    resolve: {
      userLogs: UserLogsResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'UserLogs'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
