import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IUserLogs } from 'app/shared/model/user-logs.model';

type EntityResponseType = HttpResponse<IUserLogs>;
type EntityArrayResponseType = HttpResponse<IUserLogs[]>;

@Injectable({ providedIn: 'root' })
export class UserLogsService {
  public resourceUrl = SERVER_API_URL + 'api/user-logs';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/user-logs';

  constructor(protected http: HttpClient) {}

  create(userLogs: IUserLogs): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(userLogs);
    return this.http
      .post<IUserLogs>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(userLogs: IUserLogs): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(userLogs);
    return this.http
      .put<IUserLogs>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IUserLogs>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IUserLogs[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IUserLogs[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(userLogs: IUserLogs): IUserLogs {
    const copy: IUserLogs = Object.assign({}, userLogs, {
      actionTakenOnDate:
        userLogs.actionTakenOnDate != null && userLogs.actionTakenOnDate.isValid() ? userLogs.actionTakenOnDate.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.actionTakenOnDate = res.body.actionTakenOnDate != null ? moment(res.body.actionTakenOnDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((userLogs: IUserLogs) => {
        userLogs.actionTakenOnDate = userLogs.actionTakenOnDate != null ? moment(userLogs.actionTakenOnDate) : null;
      });
    }
    return res;
  }
}
