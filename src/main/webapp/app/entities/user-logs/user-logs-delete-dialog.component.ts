import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUserLogs } from 'app/shared/model/user-logs.model';
import { UserLogsService } from './user-logs.service';

@Component({
  selector: 'jhi-user-logs-delete-dialog',
  templateUrl: './user-logs-delete-dialog.component.html'
})
export class UserLogsDeleteDialogComponent {
  userLogs: IUserLogs;

  constructor(protected userLogsService: UserLogsService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.userLogsService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'userLogsListModification',
        content: 'Deleted an userLogs'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-user-logs-delete-popup',
  template: ''
})
export class UserLogsDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userLogs }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(UserLogsDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.userLogs = userLogs;
        this.ngbModalRef.result.then(
          () => {
            this.router.navigate(['/user-logs', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          () => {
            this.router.navigate(['/user-logs', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
