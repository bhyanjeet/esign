import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { IUserLogs, UserLogs } from 'app/shared/model/user-logs.model';
import { UserLogsService } from './user-logs.service';

@Component({
  selector: 'jhi-user-logs-update',
  templateUrl: './user-logs-update.component.html'
})
export class UserLogsUpdateComponent implements OnInit {
  isSaving: boolean;
  actionTakenOnDateDp: any;

  editForm = this.fb.group({
    id: [],
    actionTaken: [],
    actionTakenBy: [],
    actionTakenOnUser: [],
    actionTakenOnDate: [],
    remarks: []
  });

  constructor(protected userLogsService: UserLogsService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ userLogs }) => {
      this.updateForm(userLogs);
    });
  }

  updateForm(userLogs: IUserLogs) {
    this.editForm.patchValue({
      id: userLogs.id,
      actionTaken: userLogs.actionTaken,
      actionTakenBy: userLogs.actionTakenBy,
      actionTakenOnUser: userLogs.actionTakenOnUser,
      actionTakenOnDate: userLogs.actionTakenOnDate,
      remarks: userLogs.remarks
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const userLogs = this.createFromForm();
    if (userLogs.id !== undefined) {
      this.subscribeToSaveResponse(this.userLogsService.update(userLogs));
    } else {
      this.subscribeToSaveResponse(this.userLogsService.create(userLogs));
    }
  }

  private createFromForm(): IUserLogs {
    return {
      ...new UserLogs(),
      id: this.editForm.get(['id']).value,
      actionTaken: this.editForm.get(['actionTaken']).value,
      actionTakenBy: this.editForm.get(['actionTakenBy']).value,
      actionTakenOnUser: this.editForm.get(['actionTakenOnUser']).value,
      actionTakenOnDate: this.editForm.get(['actionTakenOnDate']).value,
      remarks: this.editForm.get(['remarks']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserLogs>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
