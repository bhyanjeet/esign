import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { IEsignRole, EsignRole } from 'app/shared/model/esign-role.model';
import { EsignRoleService } from './esign-role.service';

@Component({
  selector: 'jhi-esign-role-update',
  templateUrl: './esign-role-update.component.html'
})
export class EsignRoleUpdateComponent implements OnInit {
  isSaving: boolean;
  createdOnDp: any;
  lastUpdatedOnDp: any;
  verifiedOnDp: any;

  editForm = this.fb.group({
    id: [],
    eSignRoleDetail: [null, [Validators.required]],
    createdBy: [],
    createdOn: [],
    lastUpdatedBy: [],
    lastUpdatedOn: [],
    verifiedBy: [],
    verifiedOn: [],
    remarks: []
  });

  constructor(protected esignRoleService: EsignRoleService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ esignRole }) => {
      this.updateForm(esignRole);
    });
  }

  updateForm(esignRole: IEsignRole) {
    this.editForm.patchValue({
      id: esignRole.id,
      eSignRoleDetail: esignRole.eSignRoleDetail,
      createdBy: esignRole.createdBy,
      createdOn: esignRole.createdOn,
      lastUpdatedBy: esignRole.lastUpdatedBy,
      lastUpdatedOn: esignRole.lastUpdatedOn,
      verifiedBy: esignRole.verifiedBy,
      verifiedOn: esignRole.verifiedOn,
      remarks: esignRole.remarks
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const esignRole = this.createFromForm();
    if (esignRole.id !== undefined) {
      this.subscribeToSaveResponse(this.esignRoleService.update(esignRole));
    } else {
      this.subscribeToSaveResponse(this.esignRoleService.create(esignRole));
    }
  }

  private createFromForm(): IEsignRole {
    return {
      ...new EsignRole(),
      id: this.editForm.get(['id']).value,
      eSignRoleDetail: this.editForm.get(['eSignRoleDetail']).value,
      createdBy: this.editForm.get(['createdBy']).value,
      createdOn: this.editForm.get(['createdOn']).value,
      lastUpdatedBy: this.editForm.get(['lastUpdatedBy']).value,
      lastUpdatedOn: this.editForm.get(['lastUpdatedOn']).value,
      verifiedBy: this.editForm.get(['verifiedBy']).value,
      verifiedOn: this.editForm.get(['verifiedOn']).value,
      remarks: this.editForm.get(['remarks']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEsignRole>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
