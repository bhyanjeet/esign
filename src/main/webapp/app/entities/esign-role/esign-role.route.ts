import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { EsignRole } from 'app/shared/model/esign-role.model';
import { EsignRoleService } from './esign-role.service';
import { EsignRoleComponent } from './esign-role.component';
import { EsignRoleDetailComponent } from './esign-role-detail.component';
import { EsignRoleUpdateComponent } from './esign-role-update.component';
import { EsignRoleDeletePopupComponent } from './esign-role-delete-dialog.component';
import { IEsignRole } from 'app/shared/model/esign-role.model';

@Injectable({ providedIn: 'root' })
export class EsignRoleResolve implements Resolve<IEsignRole> {
  constructor(private service: EsignRoleService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEsignRole> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((esignRole: HttpResponse<EsignRole>) => esignRole.body));
    }
    return of(new EsignRole());
  }
}

export const esignRoleRoute: Routes = [
  {
    path: '',
    component: EsignRoleComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'id,asc',
      pageTitle: 'EsignRoles'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: EsignRoleDetailComponent,
    resolve: {
      esignRole: EsignRoleResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'EsignRoles'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: EsignRoleUpdateComponent,
    resolve: {
      esignRole: EsignRoleResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'EsignRoles'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: EsignRoleUpdateComponent,
    resolve: {
      esignRole: EsignRoleResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'EsignRoles'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const esignRolePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: EsignRoleDeletePopupComponent,
    resolve: {
      esignRole: EsignRoleResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'EsignRoles'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
