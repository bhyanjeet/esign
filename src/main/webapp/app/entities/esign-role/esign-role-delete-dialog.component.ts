import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEsignRole } from 'app/shared/model/esign-role.model';
import { EsignRoleService } from './esign-role.service';

@Component({
  selector: 'jhi-esign-role-delete-dialog',
  templateUrl: './esign-role-delete-dialog.component.html'
})
export class EsignRoleDeleteDialogComponent {
  esignRole: IEsignRole;

  constructor(protected esignRoleService: EsignRoleService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.esignRoleService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'esignRoleListModification',
        content: 'Deleted an esignRole'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-esign-role-delete-popup',
  template: ''
})
export class EsignRoleDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ esignRole }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(EsignRoleDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.esignRole = esignRole;
        this.ngbModalRef.result.then(
          () => {
            this.router.navigate(['/esign-role', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          () => {
            this.router.navigate(['/esign-role', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
