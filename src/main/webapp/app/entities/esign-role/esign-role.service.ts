import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IEsignRole } from 'app/shared/model/esign-role.model';

type EntityResponseType = HttpResponse<IEsignRole>;
type EntityArrayResponseType = HttpResponse<IEsignRole[]>;

@Injectable({ providedIn: 'root' })
export class EsignRoleService {
  public resourceUrl = SERVER_API_URL + 'api/esign-roles';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/esign-roles';

  constructor(protected http: HttpClient) {}

  create(esignRole: IEsignRole): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(esignRole);
    return this.http
      .post<IEsignRole>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(esignRole: IEsignRole): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(esignRole);
    return this.http
      .put<IEsignRole>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IEsignRole>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IEsignRole[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IEsignRole[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(esignRole: IEsignRole): IEsignRole {
    const copy: IEsignRole = Object.assign({}, esignRole, {
      createdOn: esignRole.createdOn != null && esignRole.createdOn.isValid() ? esignRole.createdOn.format(DATE_FORMAT) : null,
      lastUpdatedOn:
        esignRole.lastUpdatedOn != null && esignRole.lastUpdatedOn.isValid() ? esignRole.lastUpdatedOn.format(DATE_FORMAT) : null,
      verifiedOn: esignRole.verifiedOn != null && esignRole.verifiedOn.isValid() ? esignRole.verifiedOn.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
      res.body.lastUpdatedOn = res.body.lastUpdatedOn != null ? moment(res.body.lastUpdatedOn) : null;
      res.body.verifiedOn = res.body.verifiedOn != null ? moment(res.body.verifiedOn) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((esignRole: IEsignRole) => {
        esignRole.createdOn = esignRole.createdOn != null ? moment(esignRole.createdOn) : null;
        esignRole.lastUpdatedOn = esignRole.lastUpdatedOn != null ? moment(esignRole.lastUpdatedOn) : null;
        esignRole.verifiedOn = esignRole.verifiedOn != null ? moment(esignRole.verifiedOn) : null;
      });
    }
    return res;
  }
}
