import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEsignRole } from 'app/shared/model/esign-role.model';

@Component({
  selector: 'jhi-esign-role-detail',
  templateUrl: './esign-role-detail.component.html'
})
export class EsignRoleDetailComponent implements OnInit {
  esignRole: IEsignRole;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ esignRole }) => {
      this.esignRole = esignRole;
    });
  }

  previousState() {
    window.history.back();
  }
}
