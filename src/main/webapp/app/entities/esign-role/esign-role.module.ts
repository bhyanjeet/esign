import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { EsignRoleComponent } from './esign-role.component';
import { EsignRoleDetailComponent } from './esign-role-detail.component';
import { EsignRoleUpdateComponent } from './esign-role-update.component';
import { EsignRoleDeletePopupComponent, EsignRoleDeleteDialogComponent } from './esign-role-delete-dialog.component';
import { esignRoleRoute, esignRolePopupRoute } from './esign-role.route';

const ENTITY_STATES = [...esignRoleRoute, ...esignRolePopupRoute];

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    EsignRoleComponent,
    EsignRoleDetailComponent,
    EsignRoleUpdateComponent,
    EsignRoleDeleteDialogComponent,
    EsignRoleDeletePopupComponent
  ],
  entryComponents: [EsignRoleDeleteDialogComponent]
})
export class EsignharyanaEsignRoleModule {}
