import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { SubDistrictMaster } from 'app/shared/model/sub-district-master.model';
import { SubDistrictMasterService } from './sub-district-master.service';
import { SubDistrictMasterComponent } from './sub-district-master.component';
import { SubDistrictMasterDetailComponent } from './sub-district-master-detail.component';
import { SubDistrictMasterUpdateComponent } from './sub-district-master-update.component';
import { SubDistrictMasterDeletePopupComponent } from './sub-district-master-delete-dialog.component';
import { ISubDistrictMaster } from 'app/shared/model/sub-district-master.model';

@Injectable({ providedIn: 'root' })
export class SubDistrictMasterResolve implements Resolve<ISubDistrictMaster> {
  constructor(private service: SubDistrictMasterService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISubDistrictMaster> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((subDistrictMaster: HttpResponse<SubDistrictMaster>) => subDistrictMaster.body));
    }
    return of(new SubDistrictMaster());
  }
}

export const subDistrictMasterRoute: Routes = [
  {
    path: '',
    component: SubDistrictMasterComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'id,asc',
      pageTitle: 'SubDistrictMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SubDistrictMasterDetailComponent,
    resolve: {
      subDistrictMaster: SubDistrictMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'SubDistrictMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SubDistrictMasterUpdateComponent,
    resolve: {
      subDistrictMaster: SubDistrictMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'SubDistrictMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SubDistrictMasterUpdateComponent,
    resolve: {
      subDistrictMaster: SubDistrictMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'SubDistrictMasters'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const subDistrictMasterPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SubDistrictMasterDeletePopupComponent,
    resolve: {
      subDistrictMaster: SubDistrictMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'SubDistrictMasters'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
