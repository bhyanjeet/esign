import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISubDistrictMaster } from 'app/shared/model/sub-district-master.model';

type EntityResponseType = HttpResponse<ISubDistrictMaster>;
type EntityArrayResponseType = HttpResponse<ISubDistrictMaster[]>;

@Injectable({ providedIn: 'root' })
export class SubDistrictMasterService {
  public resourceUrl = SERVER_API_URL + 'api/sub-district-masters';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/sub-district-masters';

  constructor(protected http: HttpClient) {}

  create(subDistrictMaster: ISubDistrictMaster): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(subDistrictMaster);
    return this.http
      .post<ISubDistrictMaster>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(subDistrictMaster: ISubDistrictMaster): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(subDistrictMaster);
    return this.http
      .put<ISubDistrictMaster>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ISubDistrictMaster>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ISubDistrictMaster[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ISubDistrictMaster[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(subDistrictMaster: ISubDistrictMaster): ISubDistrictMaster {
    const copy: ISubDistrictMaster = Object.assign({}, subDistrictMaster, {
      lastUpdatedOn:
        subDistrictMaster.lastUpdatedOn != null && subDistrictMaster.lastUpdatedOn.isValid()
          ? subDistrictMaster.lastUpdatedOn.format(DATE_FORMAT)
          : null,
      verifiedOn:
        subDistrictMaster.verifiedOn != null && subDistrictMaster.verifiedOn.isValid()
          ? subDistrictMaster.verifiedOn.format(DATE_FORMAT)
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastUpdatedOn = res.body.lastUpdatedOn != null ? moment(res.body.lastUpdatedOn) : null;
      res.body.verifiedOn = res.body.verifiedOn != null ? moment(res.body.verifiedOn) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((subDistrictMaster: ISubDistrictMaster) => {
        subDistrictMaster.lastUpdatedOn = subDistrictMaster.lastUpdatedOn != null ? moment(subDistrictMaster.lastUpdatedOn) : null;
        subDistrictMaster.verifiedOn = subDistrictMaster.verifiedOn != null ? moment(subDistrictMaster.verifiedOn) : null;
      });
    }
    return res;
  }
}
