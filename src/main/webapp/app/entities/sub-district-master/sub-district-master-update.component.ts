import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { ISubDistrictMaster, SubDistrictMaster } from 'app/shared/model/sub-district-master.model';
import { SubDistrictMasterService } from './sub-district-master.service';
import { IStateMaster } from 'app/shared/model/state-master.model';
import { StateMasterService } from 'app/entities/state-master/state-master.service';
import { IDistrictMaster } from 'app/shared/model/district-master.model';
import { DistrictMasterService } from 'app/entities/district-master/district-master.service';

@Component({
  selector: 'jhi-sub-district-master-update',
  templateUrl: './sub-district-master-update.component.html'
})
export class SubDistrictMasterUpdateComponent implements OnInit {
  isSaving: boolean;

  statemasters: IStateMaster[];

  districtmasters: IDistrictMaster[];
  lastUpdatedOnDp: any;
  verifiedOnDp: any;

  editForm = this.fb.group({
    id: [],
    subDistrictCode: [],
    subDistrictName: [null, [Validators.required]],
    lastUpdatedBy: [],
    lastUpdatedOn: [],
    verifiedBy: [],
    verifiedOn: [],
    remarks: [],
    stateMasterId: [],
    districtMasterId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected subDistrictMasterService: SubDistrictMasterService,
    protected stateMasterService: StateMasterService,
    protected districtMasterService: DistrictMasterService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ subDistrictMaster }) => {
      this.updateForm(subDistrictMaster);
    });
    this.stateMasterService
      .query()
      .subscribe(
        (res: HttpResponse<IStateMaster[]>) => (this.statemasters = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.districtMasterService
      .query()
      .subscribe(
        (res: HttpResponse<IDistrictMaster[]>) => (this.districtmasters = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(subDistrictMaster: ISubDistrictMaster) {
    this.editForm.patchValue({
      id: subDistrictMaster.id,
      subDistrictCode: subDistrictMaster.subDistrictCode,
      subDistrictName: subDistrictMaster.subDistrictName,
      lastUpdatedBy: subDistrictMaster.lastUpdatedBy,
      lastUpdatedOn: subDistrictMaster.lastUpdatedOn,
      verifiedBy: subDistrictMaster.verifiedBy,
      verifiedOn: subDistrictMaster.verifiedOn,
      remarks: subDistrictMaster.remarks,
      stateMasterId: subDistrictMaster.stateMasterId,
      districtMasterId: subDistrictMaster.districtMasterId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const subDistrictMaster = this.createFromForm();
    if (subDistrictMaster.id !== undefined) {
      this.subscribeToSaveResponse(this.subDistrictMasterService.update(subDistrictMaster));
    } else {
      this.subscribeToSaveResponse(this.subDistrictMasterService.create(subDistrictMaster));
    }
  }

  private createFromForm(): ISubDistrictMaster {
    return {
      ...new SubDistrictMaster(),
      id: this.editForm.get(['id']).value,
      subDistrictCode: this.editForm.get(['subDistrictCode']).value,
      subDistrictName: this.editForm.get(['subDistrictName']).value,
      lastUpdatedBy: this.editForm.get(['lastUpdatedBy']).value,
      lastUpdatedOn: this.editForm.get(['lastUpdatedOn']).value,
      verifiedBy: this.editForm.get(['verifiedBy']).value,
      verifiedOn: this.editForm.get(['verifiedOn']).value,
      remarks: this.editForm.get(['remarks']).value,
      stateMasterId: this.editForm.get(['stateMasterId']).value,
      districtMasterId: this.editForm.get(['districtMasterId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISubDistrictMaster>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackStateMasterById(index: number, item: IStateMaster) {
    return item.id;
  }

  trackDistrictMasterById(index: number, item: IDistrictMaster) {
    return item.id;
  }
}
