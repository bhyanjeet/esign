import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISubDistrictMaster } from 'app/shared/model/sub-district-master.model';
import { SubDistrictMasterService } from './sub-district-master.service';

@Component({
  selector: 'jhi-sub-district-master-delete-dialog',
  templateUrl: './sub-district-master-delete-dialog.component.html'
})
export class SubDistrictMasterDeleteDialogComponent {
  subDistrictMaster: ISubDistrictMaster;

  constructor(
    protected subDistrictMasterService: SubDistrictMasterService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.subDistrictMasterService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'subDistrictMasterListModification',
        content: 'Deleted an subDistrictMaster'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-sub-district-master-delete-popup',
  template: ''
})
export class SubDistrictMasterDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ subDistrictMaster }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(SubDistrictMasterDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.subDistrictMaster = subDistrictMaster;
        this.ngbModalRef.result.then(
          () => {
            this.router.navigate(['/sub-district-master', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          () => {
            this.router.navigate(['/sub-district-master', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
