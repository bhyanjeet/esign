import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { SubDistrictMasterComponent } from './sub-district-master.component';
import { SubDistrictMasterDetailComponent } from './sub-district-master-detail.component';
import { SubDistrictMasterUpdateComponent } from './sub-district-master-update.component';
import {
  SubDistrictMasterDeletePopupComponent,
  SubDistrictMasterDeleteDialogComponent
} from './sub-district-master-delete-dialog.component';
import { subDistrictMasterRoute, subDistrictMasterPopupRoute } from './sub-district-master.route';

const ENTITY_STATES = [...subDistrictMasterRoute, ...subDistrictMasterPopupRoute];

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    SubDistrictMasterComponent,
    SubDistrictMasterDetailComponent,
    SubDistrictMasterUpdateComponent,
    SubDistrictMasterDeleteDialogComponent,
    SubDistrictMasterDeletePopupComponent
  ],
  entryComponents: [SubDistrictMasterDeleteDialogComponent]
})
export class EsignharyanaSubDistrictMasterModule {}
