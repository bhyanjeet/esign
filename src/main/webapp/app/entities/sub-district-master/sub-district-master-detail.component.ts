import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISubDistrictMaster } from 'app/shared/model/sub-district-master.model';

@Component({
  selector: 'jhi-sub-district-master-detail',
  templateUrl: './sub-district-master-detail.component.html'
})
export class SubDistrictMasterDetailComponent implements OnInit {
  subDistrictMaster: ISubDistrictMaster;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ subDistrictMaster }) => {
      this.subDistrictMaster = subDistrictMaster;
    });
  }

  previousState() {
    window.history.back();
  }
}
