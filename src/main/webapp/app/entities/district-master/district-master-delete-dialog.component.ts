import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDistrictMaster } from 'app/shared/model/district-master.model';
import { DistrictMasterService } from './district-master.service';

@Component({
  selector: 'jhi-district-master-delete-dialog',
  templateUrl: './district-master-delete-dialog.component.html'
})
export class DistrictMasterDeleteDialogComponent {
  districtMaster: IDistrictMaster;

  constructor(
    protected districtMasterService: DistrictMasterService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.districtMasterService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'districtMasterListModification',
        content: 'Deleted an districtMaster'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-district-master-delete-popup',
  template: ''
})
export class DistrictMasterDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ districtMaster }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(DistrictMasterDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.districtMaster = districtMaster;
        this.ngbModalRef.result.then(
          () => {
            this.router.navigate(['/district-master', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          () => {
            this.router.navigate(['/district-master', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
