import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { DistrictMasterComponent } from './district-master.component';
import { DistrictMasterDetailComponent } from './district-master-detail.component';
import { DistrictMasterUpdateComponent } from './district-master-update.component';
import { DistrictMasterDeletePopupComponent, DistrictMasterDeleteDialogComponent } from './district-master-delete-dialog.component';
import { districtMasterRoute, districtMasterPopupRoute } from './district-master.route';

const ENTITY_STATES = [...districtMasterRoute, ...districtMasterPopupRoute];

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    DistrictMasterComponent,
    DistrictMasterDetailComponent,
    DistrictMasterUpdateComponent,
    DistrictMasterDeleteDialogComponent,
    DistrictMasterDeletePopupComponent
  ],
  entryComponents: [DistrictMasterDeleteDialogComponent]
})
export class EsignharyanaDistrictMasterModule {}
