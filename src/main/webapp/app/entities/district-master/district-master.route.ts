import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DistrictMaster } from 'app/shared/model/district-master.model';
import { DistrictMasterService } from './district-master.service';
import { DistrictMasterComponent } from './district-master.component';
import { DistrictMasterDetailComponent } from './district-master-detail.component';
import { DistrictMasterUpdateComponent } from './district-master-update.component';
import { DistrictMasterDeletePopupComponent } from './district-master-delete-dialog.component';
import { IDistrictMaster } from 'app/shared/model/district-master.model';

@Injectable({ providedIn: 'root' })
export class DistrictMasterResolve implements Resolve<IDistrictMaster> {
  constructor(private service: DistrictMasterService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDistrictMaster> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((districtMaster: HttpResponse<DistrictMaster>) => districtMaster.body));
    }
    return of(new DistrictMaster());
  }
}

export const districtMasterRoute: Routes = [
  {
    path: '',
    component: DistrictMasterComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'id,asc',
      pageTitle: 'DistrictMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DistrictMasterDetailComponent,
    resolve: {
      districtMaster: DistrictMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'DistrictMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: DistrictMasterUpdateComponent,
    resolve: {
      districtMaster: DistrictMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'DistrictMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: DistrictMasterUpdateComponent,
    resolve: {
      districtMaster: DistrictMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'DistrictMasters'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const districtMasterPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: DistrictMasterDeletePopupComponent,
    resolve: {
      districtMaster: DistrictMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'DistrictMasters'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
