import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDistrictMaster } from 'app/shared/model/district-master.model';

@Component({
  selector: 'jhi-district-master-detail',
  templateUrl: './district-master-detail.component.html'
})
export class DistrictMasterDetailComponent implements OnInit {
  districtMaster: IDistrictMaster;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ districtMaster }) => {
      this.districtMaster = districtMaster;
    });
  }

  previousState() {
    window.history.back();
  }
}
