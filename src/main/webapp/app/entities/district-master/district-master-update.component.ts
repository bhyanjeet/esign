import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IDistrictMaster, DistrictMaster } from 'app/shared/model/district-master.model';
import { DistrictMasterService } from './district-master.service';
import { IStateMaster } from 'app/shared/model/state-master.model';
import { StateMasterService } from 'app/entities/state-master/state-master.service';

@Component({
  selector: 'jhi-district-master-update',
  templateUrl: './district-master-update.component.html'
})
export class DistrictMasterUpdateComponent implements OnInit {
  isSaving: boolean;

  statemasters: IStateMaster[];
  createdOnDp: any;
  lastUpdatedOnDp: any;
  verifiedOnDp: any;

  editForm = this.fb.group({
    id: [],
    districtName: [],
    districtCode: [],
    createdOn: [],
    lastUpdatedBy: [],
    lastUpdatedOn: [],
    verifiedBy: [],
    verifiedOn: [],
    remarks: [],
    stateMasterId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected districtMasterService: DistrictMasterService,
    protected stateMasterService: StateMasterService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ districtMaster }) => {
      this.updateForm(districtMaster);
    });
    this.stateMasterService
      .query()
      .subscribe(
        (res: HttpResponse<IStateMaster[]>) => (this.statemasters = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(districtMaster: IDistrictMaster) {
    this.editForm.patchValue({
      id: districtMaster.id,
      districtName: districtMaster.districtName,
      districtCode: districtMaster.districtCode,
      createdOn: districtMaster.createdOn,
      lastUpdatedBy: districtMaster.lastUpdatedBy,
      lastUpdatedOn: districtMaster.lastUpdatedOn,
      verifiedBy: districtMaster.verifiedBy,
      verifiedOn: districtMaster.verifiedOn,
      remarks: districtMaster.remarks,
      stateMasterId: districtMaster.stateMasterId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const districtMaster = this.createFromForm();
    if (districtMaster.id !== undefined) {
      this.subscribeToSaveResponse(this.districtMasterService.update(districtMaster));
    } else {
      this.subscribeToSaveResponse(this.districtMasterService.create(districtMaster));
    }
  }

  private createFromForm(): IDistrictMaster {
    return {
      ...new DistrictMaster(),
      id: this.editForm.get(['id']).value,
      districtName: this.editForm.get(['districtName']).value,
      districtCode: this.editForm.get(['districtCode']).value,
      createdOn: this.editForm.get(['createdOn']).value,
      lastUpdatedBy: this.editForm.get(['lastUpdatedBy']).value,
      lastUpdatedOn: this.editForm.get(['lastUpdatedOn']).value,
      verifiedBy: this.editForm.get(['verifiedBy']).value,
      verifiedOn: this.editForm.get(['verifiedOn']).value,
      remarks: this.editForm.get(['remarks']).value,
      stateMasterId: this.editForm.get(['stateMasterId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDistrictMaster>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackStateMasterById(index: number, item: IStateMaster) {
    return item.id;
  }
}
