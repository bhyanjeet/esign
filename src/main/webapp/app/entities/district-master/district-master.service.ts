import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IDistrictMaster } from 'app/shared/model/district-master.model';

type EntityResponseType = HttpResponse<IDistrictMaster>;
type EntityArrayResponseType = HttpResponse<IDistrictMaster[]>;

@Injectable({ providedIn: 'root' })
export class DistrictMasterService {
  public resourceUrl = SERVER_API_URL + 'api/district-masters';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/district-masters';

  constructor(protected http: HttpClient) {}

  create(districtMaster: IDistrictMaster): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(districtMaster);
    return this.http
      .post<IDistrictMaster>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(districtMaster: IDistrictMaster): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(districtMaster);
    return this.http
      .put<IDistrictMaster>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDistrictMaster>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDistrictMaster[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDistrictMaster[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(districtMaster: IDistrictMaster): IDistrictMaster {
    const copy: IDistrictMaster = Object.assign({}, districtMaster, {
      createdOn:
        districtMaster.createdOn != null && districtMaster.createdOn.isValid() ? districtMaster.createdOn.format(DATE_FORMAT) : null,
      lastUpdatedOn:
        districtMaster.lastUpdatedOn != null && districtMaster.lastUpdatedOn.isValid()
          ? districtMaster.lastUpdatedOn.format(DATE_FORMAT)
          : null,
      verifiedOn:
        districtMaster.verifiedOn != null && districtMaster.verifiedOn.isValid() ? districtMaster.verifiedOn.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
      res.body.lastUpdatedOn = res.body.lastUpdatedOn != null ? moment(res.body.lastUpdatedOn) : null;
      res.body.verifiedOn = res.body.verifiedOn != null ? moment(res.body.verifiedOn) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((districtMaster: IDistrictMaster) => {
        districtMaster.createdOn = districtMaster.createdOn != null ? moment(districtMaster.createdOn) : null;
        districtMaster.lastUpdatedOn = districtMaster.lastUpdatedOn != null ? moment(districtMaster.lastUpdatedOn) : null;
        districtMaster.verifiedOn = districtMaster.verifiedOn != null ? moment(districtMaster.verifiedOn) : null;
      });
    }
    return res;
  }
}
