import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOrganisationLog } from 'app/shared/model/organisation-log.model';

@Component({
  selector: 'jhi-organisation-log-detail',
  templateUrl: './organisation-log-detail.component.html'
})
export class OrganisationLogDetailComponent implements OnInit {
  organisationLog: IOrganisationLog;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ organisationLog }) => {
      this.organisationLog = organisationLog;
    });
  }

  previousState() {
    window.history.back();
  }
}
