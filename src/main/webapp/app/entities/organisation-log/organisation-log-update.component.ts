import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IOrganisationLog, OrganisationLog } from 'app/shared/model/organisation-log.model';
import { OrganisationLogService } from './organisation-log.service';

@Component({
  selector: 'jhi-organisation-log-update',
  templateUrl: './organisation-log-update.component.html'
})
export class OrganisationLogUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    organisationId: [],
    createdByLogin: [],
    createdDate: [],
    verifyByName: [],
    verifyDate: [],
    remarks: [],
    status: [],
    verifyByLogin: [],
    createdByName: []
  });

  constructor(
    protected organisationLogService: OrganisationLogService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ organisationLog }) => {
      this.updateForm(organisationLog);
    });
  }

  updateForm(organisationLog: IOrganisationLog) {
    this.editForm.patchValue({
      id: organisationLog.id,
      organisationId: organisationLog.organisationId,
      createdByLogin: organisationLog.createdByLogin,
      createdDate: organisationLog.createdDate != null ? organisationLog.createdDate.format(DATE_TIME_FORMAT) : null,
      verifyByName: organisationLog.verifyByName,
      verifyDate: organisationLog.verifyDate != null ? organisationLog.verifyDate.format(DATE_TIME_FORMAT) : null,
      remarks: organisationLog.remarks,
      status: organisationLog.status,
      verifyByLogin: organisationLog.verifyByLogin,
      createdByName: organisationLog.createdByName
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const organisationLog = this.createFromForm();
    if (organisationLog.id !== undefined) {
      this.subscribeToSaveResponse(this.organisationLogService.update(organisationLog));
    } else {
      this.subscribeToSaveResponse(this.organisationLogService.create(organisationLog));
    }
  }

  private createFromForm(): IOrganisationLog {
    return {
      ...new OrganisationLog(),
      id: this.editForm.get(['id']).value,
      organisationId: this.editForm.get(['organisationId']).value,
      createdByLogin: this.editForm.get(['createdByLogin']).value,
      createdDate:
        this.editForm.get(['createdDate']).value != null ? moment(this.editForm.get(['createdDate']).value, DATE_TIME_FORMAT) : undefined,
      verifyByName: this.editForm.get(['verifyByName']).value,
      verifyDate:
        this.editForm.get(['verifyDate']).value != null ? moment(this.editForm.get(['verifyDate']).value, DATE_TIME_FORMAT) : undefined,
      remarks: this.editForm.get(['remarks']).value,
      status: this.editForm.get(['status']).value,
      verifyByLogin: this.editForm.get(['verifyByLogin']).value,
      createdByName: this.editForm.get(['createdByName']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrganisationLog>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
