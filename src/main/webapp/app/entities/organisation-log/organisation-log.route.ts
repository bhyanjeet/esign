import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { OrganisationLog } from 'app/shared/model/organisation-log.model';
import { OrganisationLogService } from './organisation-log.service';
import { OrganisationLogComponent } from './organisation-log.component';
import { OrganisationLogDetailComponent } from './organisation-log-detail.component';
import { OrganisationLogUpdateComponent } from './organisation-log-update.component';
import { OrganisationLogDeletePopupComponent } from './organisation-log-delete-dialog.component';
import { IOrganisationLog } from 'app/shared/model/organisation-log.model';

@Injectable({ providedIn: 'root' })
export class OrganisationLogResolve implements Resolve<IOrganisationLog> {
  constructor(private service: OrganisationLogService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOrganisationLog> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((organisationLog: HttpResponse<OrganisationLog>) => organisationLog.body));
    }
    return of(new OrganisationLog());
  }
}

export const organisationLogRoute: Routes = [
  {
    path: '',
    component: OrganisationLogComponent,
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationLogs'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: OrganisationLogDetailComponent,
    resolve: {
      organisationLog: OrganisationLogResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationLogs'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: OrganisationLogUpdateComponent,
    resolve: {
      organisationLog: OrganisationLogResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationLogs'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: OrganisationLogUpdateComponent,
    resolve: {
      organisationLog: OrganisationLogResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationLogs'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const organisationLogPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: OrganisationLogDeletePopupComponent,
    resolve: {
      organisationLog: OrganisationLogResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationLogs'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
