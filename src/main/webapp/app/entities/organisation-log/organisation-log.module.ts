import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { OrganisationLogComponent } from './organisation-log.component';
import { OrganisationLogDetailComponent } from './organisation-log-detail.component';
import { OrganisationLogUpdateComponent } from './organisation-log-update.component';
import { OrganisationLogDeletePopupComponent, OrganisationLogDeleteDialogComponent } from './organisation-log-delete-dialog.component';
import { organisationLogRoute, organisationLogPopupRoute } from './organisation-log.route';

const ENTITY_STATES = [...organisationLogRoute, ...organisationLogPopupRoute];

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    OrganisationLogComponent,
    OrganisationLogDetailComponent,
    OrganisationLogUpdateComponent,
    OrganisationLogDeleteDialogComponent,
    OrganisationLogDeletePopupComponent
  ],
  entryComponents: [OrganisationLogDeleteDialogComponent]
})
export class EsignharyanaOrganisationLogModule {}
