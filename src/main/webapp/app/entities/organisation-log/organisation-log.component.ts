import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IOrganisationLog } from 'app/shared/model/organisation-log.model';
import { OrganisationLogService } from './organisation-log.service';

@Component({
  selector: 'jhi-organisation-log',
  templateUrl: './organisation-log.component.html'
})
export class OrganisationLogComponent implements OnInit, OnDestroy {
  organisationLogs: IOrganisationLog[];
  eventSubscriber: Subscription;
  currentSearch: string;

  constructor(
    protected organisationLogService: OrganisationLogService,
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll() {
    if (this.currentSearch) {
      this.organisationLogService
        .search({
          query: this.currentSearch
        })
        .subscribe((res: HttpResponse<IOrganisationLog[]>) => (this.organisationLogs = res.body));
      return;
    }
    this.organisationLogService.query().subscribe((res: HttpResponse<IOrganisationLog[]>) => {
      this.organisationLogs = res.body;
      this.currentSearch = '';
    });
  }

  search(query) {
    if (!query) {
      return this.clear();
    }
    this.currentSearch = query;
    this.loadAll();
  }

  clear() {
    this.currentSearch = '';
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInOrganisationLogs();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IOrganisationLog) {
    return item.id;
  }

  registerChangeInOrganisationLogs() {
    this.eventSubscriber = this.eventManager.subscribe('organisationLogListModification', () => this.loadAll());
  }
}
