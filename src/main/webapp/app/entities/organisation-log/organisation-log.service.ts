import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOrganisationLog } from 'app/shared/model/organisation-log.model';

type EntityResponseType = HttpResponse<IOrganisationLog>;
type EntityArrayResponseType = HttpResponse<IOrganisationLog[]>;

@Injectable({ providedIn: 'root' })
export class OrganisationLogService {
  public resourceUrl = SERVER_API_URL + 'api/organisation-logs';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/organisation-logs';

  constructor(protected http: HttpClient) {}

  create(organisationLog: IOrganisationLog): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(organisationLog);
    return this.http
      .post<IOrganisationLog>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(organisationLog: IOrganisationLog): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(organisationLog);
    return this.http
      .put<IOrganisationLog>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IOrganisationLog>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IOrganisationLog[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IOrganisationLog[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(organisationLog: IOrganisationLog): IOrganisationLog {
    const copy: IOrganisationLog = Object.assign({}, organisationLog, {
      createdDate:
        organisationLog.createdDate != null && organisationLog.createdDate.isValid() ? organisationLog.createdDate.toJSON() : null,
      verifyDate: organisationLog.verifyDate != null && organisationLog.verifyDate.isValid() ? organisationLog.verifyDate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdDate = res.body.createdDate != null ? moment(res.body.createdDate) : null;
      res.body.verifyDate = res.body.verifyDate != null ? moment(res.body.verifyDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((organisationLog: IOrganisationLog) => {
        organisationLog.createdDate = organisationLog.createdDate != null ? moment(organisationLog.createdDate) : null;
        organisationLog.verifyDate = organisationLog.verifyDate != null ? moment(organisationLog.verifyDate) : null;
      });
    }
    return res;
  }
}
