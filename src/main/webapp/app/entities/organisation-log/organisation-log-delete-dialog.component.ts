import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOrganisationLog } from 'app/shared/model/organisation-log.model';
import { OrganisationLogService } from './organisation-log.service';

@Component({
  selector: 'jhi-organisation-log-delete-dialog',
  templateUrl: './organisation-log-delete-dialog.component.html'
})
export class OrganisationLogDeleteDialogComponent {
  organisationLog: IOrganisationLog;

  constructor(
    protected organisationLogService: OrganisationLogService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.organisationLogService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'organisationLogListModification',
        content: 'Deleted an organisationLog'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-organisation-log-delete-popup',
  template: ''
})
export class OrganisationLogDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ organisationLog }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(OrganisationLogDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.organisationLog = organisationLog;
        this.ngbModalRef.result.then(
          () => {
            this.router.navigate(['/organisation-log', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          () => {
            this.router.navigate(['/organisation-log', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
