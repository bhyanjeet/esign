import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { IEsignTransation, EsignTransation } from 'app/shared/model/esign-transation.model';
import { EsignTransationService } from './esign-transation.service';

@Component({
  selector: 'jhi-esign-transation-update',
  templateUrl: './esign-transation-update.component.html'
})
export class EsignTransationUpdateComponent implements OnInit {
  isSaving: boolean;
  requestDateDp: any;
  responseDateDp: any;

  editForm = this.fb.group({
    id: [],
    txnId: [],
    userCodeId: [],
    applicationCodeId: [],
    orgResponseSuccessURL: [],
    orgResponseFailURL: [],
    orgStatus: [],
    requestDate: [],
    organisationIdCode: [],
    signerId: [],
    requestType: [],
    requestStatus: [],
    responseType: [],
    responseStatus: [],
    responseCode: [],
    requestXml: [],
    responseXml: [],
    responseDate: [],
    ts: []
  });

  constructor(
    protected esignTransationService: EsignTransationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ esignTransation }) => {
      this.updateForm(esignTransation);
    });
  }

  updateForm(esignTransation: IEsignTransation) {
    this.editForm.patchValue({
      id: esignTransation.id,
      txnId: esignTransation.txnId,
      userCodeId: esignTransation.userCodeId,
      applicationCodeId: esignTransation.applicationCodeId,
      orgResponseSuccessURL: esignTransation.orgResponseSuccessURL,
      orgResponseFailURL: esignTransation.orgResponseFailURL,
      orgStatus: esignTransation.orgStatus,
      requestDate: esignTransation.requestDate,
      organisationIdCode: esignTransation.organisationIdCode,
      signerId: esignTransation.signerId,
      requestType: esignTransation.requestType,
      requestStatus: esignTransation.requestStatus,
      responseType: esignTransation.responseType,
      responseStatus: esignTransation.responseStatus,
      responseCode: esignTransation.responseCode,
      requestXml: esignTransation.requestXml,
      responseXml: esignTransation.responseXml,
      responseDate: esignTransation.responseDate,
      ts: esignTransation.ts
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const esignTransation = this.createFromForm();
    if (esignTransation.id !== undefined) {
      this.subscribeToSaveResponse(this.esignTransationService.update(esignTransation));
    } else {
      this.subscribeToSaveResponse(this.esignTransationService.create(esignTransation));
    }
  }

  private createFromForm(): IEsignTransation {
    return {
      ...new EsignTransation(),
      id: this.editForm.get(['id']).value,
      txnId: this.editForm.get(['txnId']).value,
      userCodeId: this.editForm.get(['userCodeId']).value,
      applicationCodeId: this.editForm.get(['applicationCodeId']).value,
      orgResponseSuccessURL: this.editForm.get(['orgResponseSuccessURL']).value,
      orgResponseFailURL: this.editForm.get(['orgResponseFailURL']).value,
      orgStatus: this.editForm.get(['orgStatus']).value,
      requestDate: this.editForm.get(['requestDate']).value,
      organisationIdCode: this.editForm.get(['organisationIdCode']).value,
      signerId: this.editForm.get(['signerId']).value,
      requestType: this.editForm.get(['requestType']).value,
      requestStatus: this.editForm.get(['requestStatus']).value,
      responseType: this.editForm.get(['responseType']).value,
      responseStatus: this.editForm.get(['responseStatus']).value,
      responseCode: this.editForm.get(['responseCode']).value,
      requestXml: this.editForm.get(['requestXml']).value,
      responseXml: this.editForm.get(['responseXml']).value,
      responseDate: this.editForm.get(['responseDate']).value,
      ts: this.editForm.get(['ts']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEsignTransation>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
