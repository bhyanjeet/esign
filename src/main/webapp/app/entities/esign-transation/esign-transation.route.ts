import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { EsignTransation } from 'app/shared/model/esign-transation.model';
import { EsignTransationService } from './esign-transation.service';
import { EsignTransationComponent } from './esign-transation.component';
import { EsignTransationDetailComponent } from './esign-transation-detail.component';
import { EsignTransationUpdateComponent } from './esign-transation-update.component';
import { EsignTransationDeletePopupComponent } from './esign-transation-delete-dialog.component';
import { IEsignTransation } from 'app/shared/model/esign-transation.model';

@Injectable({ providedIn: 'root' })
export class EsignTransationResolve implements Resolve<IEsignTransation> {
  constructor(private service: EsignTransationService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEsignTransation> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((esignTransation: HttpResponse<EsignTransation>) => esignTransation.body));
    }
    return of(new EsignTransation());
  }
}

export const esignTransationRoute: Routes = [
  {
    path: '',
    component: EsignTransationComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'id,asc',
      pageTitle: 'EsignTransations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: EsignTransationDetailComponent,
    resolve: {
      esignTransation: EsignTransationResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'EsignTransations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: EsignTransationUpdateComponent,
    resolve: {
      esignTransation: EsignTransationResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'EsignTransations'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: EsignTransationUpdateComponent,
    resolve: {
      esignTransation: EsignTransationResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'EsignTransations'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const esignTransationPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: EsignTransationDeletePopupComponent,
    resolve: {
      esignTransation: EsignTransationResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'EsignTransations'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
