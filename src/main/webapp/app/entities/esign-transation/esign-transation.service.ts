import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IEsignTransation } from 'app/shared/model/esign-transation.model';

type EntityResponseType = HttpResponse<IEsignTransation>;
type EntityArrayResponseType = HttpResponse<IEsignTransation[]>;

@Injectable({ providedIn: 'root' })
export class EsignTransationService {
  public resourceUrl = SERVER_API_URL + 'api/esign-transations';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/esign-transations';
  public resourceUrlSave = SERVER_API_URL + 'api/saveEsignTransaction';

  constructor(protected http: HttpClient) {}

  create(esignTransation: IEsignTransation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(esignTransation);
    return this.http
      .post<IEsignTransation>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(esignTransation: IEsignTransation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(esignTransation);
    return this.http
      .put<IEsignTransation>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IEsignTransation>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IEsignTransation[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IEsignTransation[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(esignTransation: IEsignTransation): IEsignTransation {
    const copy: IEsignTransation = Object.assign({}, esignTransation, {
      requestDate:
        esignTransation.requestDate != null && esignTransation.requestDate.isValid()
          ? esignTransation.requestDate.format(DATE_FORMAT)
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.requestDate = res.body.requestDate != null ? moment(res.body.requestDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((esignTransation: IEsignTransation) => {
        esignTransation.requestDate = esignTransation.requestDate != null ? moment(esignTransation.requestDate) : null;
      });
    }
    return res;
  }

  saveEsignTransaction(esignTransation: IEsignTransation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(esignTransation);
    return this.http
      .post<IEsignTransation>(this.resourceUrlSave, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

}
