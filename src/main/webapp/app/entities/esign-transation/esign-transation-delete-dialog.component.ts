import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEsignTransation } from 'app/shared/model/esign-transation.model';
import { EsignTransationService } from './esign-transation.service';

@Component({
  selector: 'jhi-esign-transation-delete-dialog',
  templateUrl: './esign-transation-delete-dialog.component.html'
})
export class EsignTransationDeleteDialogComponent {
  esignTransation: IEsignTransation;

  constructor(
    protected esignTransationService: EsignTransationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.esignTransationService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'esignTransationListModification',
        content: 'Deleted an esignTransation'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-esign-transation-delete-popup',
  template: ''
})
export class EsignTransationDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ esignTransation }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(EsignTransationDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.esignTransation = esignTransation;
        this.ngbModalRef.result.then(
          () => {
            this.router.navigate(['/esign-transation', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          () => {
            this.router.navigate(['/esign-transation', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
