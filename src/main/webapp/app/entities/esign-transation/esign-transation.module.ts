import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { EsignTransationComponent } from './esign-transation.component';
import { EsignTransationDetailComponent } from './esign-transation-detail.component';
import { EsignTransationUpdateComponent } from './esign-transation-update.component';
import { EsignTransationDeletePopupComponent, EsignTransationDeleteDialogComponent } from './esign-transation-delete-dialog.component';
import { esignTransationRoute, esignTransationPopupRoute } from './esign-transation.route';

const ENTITY_STATES = [...esignTransationRoute, ...esignTransationPopupRoute];

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    EsignTransationComponent,
    EsignTransationDetailComponent,
    EsignTransationUpdateComponent,
    EsignTransationDeleteDialogComponent,
    EsignTransationDeletePopupComponent
  ],
  entryComponents: [EsignTransationDeleteDialogComponent]
})
export class EsignharyanaEsignTransationModule {}
