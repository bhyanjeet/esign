import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEsignTransation } from 'app/shared/model/esign-transation.model';

@Component({
  selector: 'jhi-esign-transation-detail',
  templateUrl: './esign-transation-detail.component.html'
})
export class EsignTransationDetailComponent implements OnInit {
  esignTransation: IEsignTransation;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ esignTransation }) => {
      this.esignTransation = esignTransation;
    });
  }

  previousState() {
    window.history.back();
  }
}
