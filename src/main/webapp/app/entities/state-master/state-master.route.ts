import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { StateMaster } from 'app/shared/model/state-master.model';
import { StateMasterService } from './state-master.service';
import { StateMasterComponent } from './state-master.component';
import { StateMasterDetailComponent } from './state-master-detail.component';
import { StateMasterUpdateComponent } from './state-master-update.component';
import { StateMasterDeletePopupComponent } from './state-master-delete-dialog.component';
import { IStateMaster } from 'app/shared/model/state-master.model';

@Injectable({ providedIn: 'root' })
export class StateMasterResolve implements Resolve<IStateMaster> {
  constructor(private service: StateMasterService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IStateMaster> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((stateMaster: HttpResponse<StateMaster>) => stateMaster.body));
    }
    return of(new StateMaster());
  }
}

export const stateMasterRoute: Routes = [
  {
    path: '',
    component: StateMasterComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'id,asc',
      pageTitle: 'StateMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: StateMasterDetailComponent,
    resolve: {
      stateMaster: StateMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'StateMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: StateMasterUpdateComponent,
    resolve: {
      stateMaster: StateMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'StateMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: StateMasterUpdateComponent,
    resolve: {
      stateMaster: StateMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'StateMasters'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const stateMasterPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: StateMasterDeletePopupComponent,
    resolve: {
      stateMaster: StateMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'StateMasters'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
