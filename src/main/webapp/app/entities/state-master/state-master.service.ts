import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IStateMaster } from 'app/shared/model/state-master.model';

type EntityResponseType = HttpResponse<IStateMaster>;
type EntityArrayResponseType = HttpResponse<IStateMaster[]>;

@Injectable({ providedIn: 'root' })
export class StateMasterService {
  public resourceUrl = SERVER_API_URL + 'api/state-masters';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/state-masters';

  constructor(protected http: HttpClient) {}

  create(stateMaster: IStateMaster): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(stateMaster);
    return this.http
      .post<IStateMaster>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(stateMaster: IStateMaster): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(stateMaster);
    return this.http
      .put<IStateMaster>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IStateMaster>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IStateMaster[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IStateMaster[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(stateMaster: IStateMaster): IStateMaster {
    const copy: IStateMaster = Object.assign({}, stateMaster, {
      createdOn: stateMaster.createdOn != null && stateMaster.createdOn.isValid() ? stateMaster.createdOn.format(DATE_FORMAT) : null,
      lastUpdatedOn:
        stateMaster.lastUpdatedOn != null && stateMaster.lastUpdatedOn.isValid() ? stateMaster.lastUpdatedOn.format(DATE_FORMAT) : null,
      verifiedOn: stateMaster.verifiedOn != null && stateMaster.verifiedOn.isValid() ? stateMaster.verifiedOn.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
      res.body.lastUpdatedOn = res.body.lastUpdatedOn != null ? moment(res.body.lastUpdatedOn) : null;
      res.body.verifiedOn = res.body.verifiedOn != null ? moment(res.body.verifiedOn) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((stateMaster: IStateMaster) => {
        stateMaster.createdOn = stateMaster.createdOn != null ? moment(stateMaster.createdOn) : null;
        stateMaster.lastUpdatedOn = stateMaster.lastUpdatedOn != null ? moment(stateMaster.lastUpdatedOn) : null;
        stateMaster.verifiedOn = stateMaster.verifiedOn != null ? moment(stateMaster.verifiedOn) : null;
      });
    }
    return res;
  }
}
