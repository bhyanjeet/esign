import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { StateMasterComponent } from './state-master.component';
import { StateMasterDetailComponent } from './state-master-detail.component';
import { StateMasterUpdateComponent } from './state-master-update.component';
import { StateMasterDeletePopupComponent, StateMasterDeleteDialogComponent } from './state-master-delete-dialog.component';
import { stateMasterRoute, stateMasterPopupRoute } from './state-master.route';

const ENTITY_STATES = [...stateMasterRoute, ...stateMasterPopupRoute];

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    StateMasterComponent,
    StateMasterDetailComponent,
    StateMasterUpdateComponent,
    StateMasterDeleteDialogComponent,
    StateMasterDeletePopupComponent
  ],
  entryComponents: [StateMasterDeleteDialogComponent]
})
export class EsignharyanaStateMasterModule {}
