import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { IStateMaster, StateMaster } from 'app/shared/model/state-master.model';
import { StateMasterService } from './state-master.service';

@Component({
  selector: 'jhi-state-master-update',
  templateUrl: './state-master-update.component.html'
})
export class StateMasterUpdateComponent implements OnInit {
  isSaving: boolean;
  createdOnDp: any;
  lastUpdatedOnDp: any;
  verifiedOnDp: any;

  editForm = this.fb.group({
    id: [],
    stateCode: [],
    stateName: [null, [Validators.required]],
    createdOn: [],
    lastUpdatedOn: [],
    lastUpdatedBy: [],
    verifiedBy: [],
    verifiedOn: [],
    remarks: [null, [Validators.minLength(0), Validators.maxLength(300)]]
  });

  constructor(protected stateMasterService: StateMasterService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ stateMaster }) => {
      this.updateForm(stateMaster);
    });
  }

  updateForm(stateMaster: IStateMaster) {
    this.editForm.patchValue({
      id: stateMaster.id,
      stateCode: stateMaster.stateCode,
      stateName: stateMaster.stateName,
      createdOn: stateMaster.createdOn,
      lastUpdatedOn: stateMaster.lastUpdatedOn,
      lastUpdatedBy: stateMaster.lastUpdatedBy,
      verifiedBy: stateMaster.verifiedBy,
      verifiedOn: stateMaster.verifiedOn,
      remarks: stateMaster.remarks
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const stateMaster = this.createFromForm();
    if (stateMaster.id !== undefined) {
      this.subscribeToSaveResponse(this.stateMasterService.update(stateMaster));
    } else {
      this.subscribeToSaveResponse(this.stateMasterService.create(stateMaster));
    }
  }

  private createFromForm(): IStateMaster {
    return {
      ...new StateMaster(),
      id: this.editForm.get(['id']).value,
      stateCode: this.editForm.get(['stateCode']).value,
      stateName: this.editForm.get(['stateName']).value,
      createdOn: this.editForm.get(['createdOn']).value,
      lastUpdatedOn: this.editForm.get(['lastUpdatedOn']).value,
      lastUpdatedBy: this.editForm.get(['lastUpdatedBy']).value,
      verifiedBy: this.editForm.get(['verifiedBy']).value,
      verifiedOn: this.editForm.get(['verifiedOn']).value,
      remarks: this.editForm.get(['remarks']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStateMaster>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
