import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOrganisationTypeMaster } from 'app/shared/model/organisation-type-master.model';
import { OrganisationTypeMasterService } from './organisation-type-master.service';

@Component({
  selector: 'jhi-organisation-type-master-delete-dialog',
  templateUrl: './organisation-type-master-delete-dialog.component.html'
})
export class OrganisationTypeMasterDeleteDialogComponent {
  organisationTypeMaster: IOrganisationTypeMaster;

  constructor(
    protected organisationTypeMasterService: OrganisationTypeMasterService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.organisationTypeMasterService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'organisationTypeMasterListModification',
        content: 'Deleted an organisationTypeMaster'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-organisation-type-master-delete-popup',
  template: ''
})
export class OrganisationTypeMasterDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ organisationTypeMaster }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(OrganisationTypeMasterDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.organisationTypeMaster = organisationTypeMaster;
        this.ngbModalRef.result.then(
          () => {
            this.router.navigate(['/organisation-type-master', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          () => {
            this.router.navigate(['/organisation-type-master', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
