import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOrganisationTypeMaster } from 'app/shared/model/organisation-type-master.model';

type EntityResponseType = HttpResponse<IOrganisationTypeMaster>;
type EntityArrayResponseType = HttpResponse<IOrganisationTypeMaster[]>;

@Injectable({ providedIn: 'root' })
export class OrganisationTypeMasterService {
  public resourceUrl = SERVER_API_URL + 'api/organisation-type-masters';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/organisation-type-masters';

  constructor(protected http: HttpClient) {}

  create(organisationTypeMaster: IOrganisationTypeMaster): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(organisationTypeMaster);
    return this.http
      .post<IOrganisationTypeMaster>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(organisationTypeMaster: IOrganisationTypeMaster): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(organisationTypeMaster);
    return this.http
      .put<IOrganisationTypeMaster>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IOrganisationTypeMaster>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IOrganisationTypeMaster[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IOrganisationTypeMaster[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(organisationTypeMaster: IOrganisationTypeMaster): IOrganisationTypeMaster {
    const copy: IOrganisationTypeMaster = Object.assign({}, organisationTypeMaster, {
      createdOn:
        organisationTypeMaster.createdOn != null && organisationTypeMaster.createdOn.isValid()
          ? organisationTypeMaster.createdOn.format(DATE_FORMAT)
          : null,
      lastUpdatedOn:
        organisationTypeMaster.lastUpdatedOn != null && organisationTypeMaster.lastUpdatedOn.isValid()
          ? organisationTypeMaster.lastUpdatedOn.format(DATE_FORMAT)
          : null,
      verifiedOn:
        organisationTypeMaster.verifiedOn != null && organisationTypeMaster.verifiedOn.isValid()
          ? organisationTypeMaster.verifiedOn.format(DATE_FORMAT)
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
      res.body.lastUpdatedOn = res.body.lastUpdatedOn != null ? moment(res.body.lastUpdatedOn) : null;
      res.body.verifiedOn = res.body.verifiedOn != null ? moment(res.body.verifiedOn) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((organisationTypeMaster: IOrganisationTypeMaster) => {
        organisationTypeMaster.createdOn = organisationTypeMaster.createdOn != null ? moment(organisationTypeMaster.createdOn) : null;
        organisationTypeMaster.lastUpdatedOn =
          organisationTypeMaster.lastUpdatedOn != null ? moment(organisationTypeMaster.lastUpdatedOn) : null;
        organisationTypeMaster.verifiedOn = organisationTypeMaster.verifiedOn != null ? moment(organisationTypeMaster.verifiedOn) : null;
      });
    }
    return res;
  }
}
