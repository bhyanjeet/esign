import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOrganisationTypeMaster } from 'app/shared/model/organisation-type-master.model';

@Component({
  selector: 'jhi-organisation-type-master-detail',
  templateUrl: './organisation-type-master-detail.component.html'
})
export class OrganisationTypeMasterDetailComponent implements OnInit {
  organisationTypeMaster: IOrganisationTypeMaster;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ organisationTypeMaster }) => {
      this.organisationTypeMaster = organisationTypeMaster;
    });
  }

  previousState() {
    window.history.back();
  }
}
