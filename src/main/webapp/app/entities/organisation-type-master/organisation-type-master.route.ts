import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { OrganisationTypeMaster } from 'app/shared/model/organisation-type-master.model';
import { OrganisationTypeMasterService } from './organisation-type-master.service';
import { OrganisationTypeMasterComponent } from './organisation-type-master.component';
import { OrganisationTypeMasterDetailComponent } from './organisation-type-master-detail.component';
import { OrganisationTypeMasterUpdateComponent } from './organisation-type-master-update.component';
import { OrganisationTypeMasterDeletePopupComponent } from './organisation-type-master-delete-dialog.component';
import { IOrganisationTypeMaster } from 'app/shared/model/organisation-type-master.model';

@Injectable({ providedIn: 'root' })
export class OrganisationTypeMasterResolve implements Resolve<IOrganisationTypeMaster> {
  constructor(private service: OrganisationTypeMasterService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOrganisationTypeMaster> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((organisationTypeMaster: HttpResponse<OrganisationTypeMaster>) => organisationTypeMaster.body));
    }
    return of(new OrganisationTypeMaster());
  }
}

export const organisationTypeMasterRoute: Routes = [
  {
    path: '',
    component: OrganisationTypeMasterComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'id,asc',
      pageTitle: 'OrganisationTypeMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: OrganisationTypeMasterDetailComponent,
    resolve: {
      organisationTypeMaster: OrganisationTypeMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationTypeMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: OrganisationTypeMasterUpdateComponent,
    resolve: {
      organisationTypeMaster: OrganisationTypeMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationTypeMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: OrganisationTypeMasterUpdateComponent,
    resolve: {
      organisationTypeMaster: OrganisationTypeMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationTypeMasters'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const organisationTypeMasterPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: OrganisationTypeMasterDeletePopupComponent,
    resolve: {
      organisationTypeMaster: OrganisationTypeMasterResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'OrganisationTypeMasters'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
