import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { IOrganisationTypeMaster, OrganisationTypeMaster } from 'app/shared/model/organisation-type-master.model';
import { OrganisationTypeMasterService } from './organisation-type-master.service';

@Component({
  selector: 'jhi-organisation-type-master-update',
  templateUrl: './organisation-type-master-update.component.html'
})
export class OrganisationTypeMasterUpdateComponent implements OnInit {
  isSaving: boolean;
  createdOnDp: any;
  lastUpdatedOnDp: any;
  verifiedOnDp: any;

  editForm = this.fb.group({
    id: [],
    organisationTypeDetail: [null, [Validators.required]],
    organisationTypeAbbreviation: [null, [Validators.required]],
    createdBy: [],
    createdOn: [],
    lastUpdatedBy: [],
    lastUpdatedOn: [],
    verifiedBy: [],
    verifiedOn: [],
    remarks: []
  });

  constructor(
    protected organisationTypeMasterService: OrganisationTypeMasterService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ organisationTypeMaster }) => {
      this.updateForm(organisationTypeMaster);
    });
  }

  updateForm(organisationTypeMaster: IOrganisationTypeMaster) {
    this.editForm.patchValue({
      id: organisationTypeMaster.id,
      organisationTypeDetail: organisationTypeMaster.organisationTypeDetail,
      organisationTypeAbbreviation: organisationTypeMaster.organisationTypeAbbreviation,
      createdBy: organisationTypeMaster.createdBy,
      createdOn: organisationTypeMaster.createdOn,
      lastUpdatedBy: organisationTypeMaster.lastUpdatedBy,
      lastUpdatedOn: organisationTypeMaster.lastUpdatedOn,
      verifiedBy: organisationTypeMaster.verifiedBy,
      verifiedOn: organisationTypeMaster.verifiedOn,
      remarks: organisationTypeMaster.remarks
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const organisationTypeMaster = this.createFromForm();
    if (organisationTypeMaster.id !== undefined) {
      this.subscribeToSaveResponse(this.organisationTypeMasterService.update(organisationTypeMaster));
    } else {
      this.subscribeToSaveResponse(this.organisationTypeMasterService.create(organisationTypeMaster));
    }
  }

  private createFromForm(): IOrganisationTypeMaster {
    return {
      ...new OrganisationTypeMaster(),
      id: this.editForm.get(['id']).value,
      organisationTypeDetail: this.editForm.get(['organisationTypeDetail']).value,
      organisationTypeAbbreviation: this.editForm.get(['organisationTypeAbbreviation']).value,
      createdBy: this.editForm.get(['createdBy']).value,
      createdOn: this.editForm.get(['createdOn']).value,
      lastUpdatedBy: this.editForm.get(['lastUpdatedBy']).value,
      lastUpdatedOn: this.editForm.get(['lastUpdatedOn']).value,
      verifiedBy: this.editForm.get(['verifiedBy']).value,
      verifiedOn: this.editForm.get(['verifiedOn']).value,
      remarks: this.editForm.get(['remarks']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrganisationTypeMaster>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
