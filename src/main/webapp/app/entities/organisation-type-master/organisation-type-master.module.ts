import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { OrganisationTypeMasterComponent } from './organisation-type-master.component';
import { OrganisationTypeMasterDetailComponent } from './organisation-type-master-detail.component';
import { OrganisationTypeMasterUpdateComponent } from './organisation-type-master-update.component';
import {
  OrganisationTypeMasterDeletePopupComponent,
  OrganisationTypeMasterDeleteDialogComponent
} from './organisation-type-master-delete-dialog.component';
import { organisationTypeMasterRoute, organisationTypeMasterPopupRoute } from './organisation-type-master.route';

const ENTITY_STATES = [...organisationTypeMasterRoute, ...organisationTypeMasterPopupRoute];

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    OrganisationTypeMasterComponent,
    OrganisationTypeMasterDetailComponent,
    OrganisationTypeMasterUpdateComponent,
    OrganisationTypeMasterDeleteDialogComponent,
    OrganisationTypeMasterDeletePopupComponent
  ],
  entryComponents: [OrganisationTypeMasterDeleteDialogComponent]
})
export class EsignharyanaOrganisationTypeMasterModule {}
