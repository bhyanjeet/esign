import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IApplicationLogs } from 'app/shared/model/application-logs.model';
import { ApplicationLogsService } from './application-logs.service';

@Component({
  selector: 'jhi-application-logs-delete-dialog',
  templateUrl: './application-logs-delete-dialog.component.html'
})
export class ApplicationLogsDeleteDialogComponent {
  applicationLogs: IApplicationLogs;

  constructor(
    protected applicationLogsService: ApplicationLogsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.applicationLogsService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'applicationLogsListModification',
        content: 'Deleted an applicationLogs'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-application-logs-delete-popup',
  template: ''
})
export class ApplicationLogsDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ applicationLogs }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ApplicationLogsDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.applicationLogs = applicationLogs;
        this.ngbModalRef.result.then(
          () => {
            this.router.navigate(['/application-logs', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          () => {
            this.router.navigate(['/application-logs', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
