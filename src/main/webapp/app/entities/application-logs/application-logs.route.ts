import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApplicationLogs } from 'app/shared/model/application-logs.model';
import { ApplicationLogsService } from './application-logs.service';
import { ApplicationLogsComponent } from './application-logs.component';
import { ApplicationLogsDetailComponent } from './application-logs-detail.component';
import { ApplicationLogsUpdateComponent } from './application-logs-update.component';
import { ApplicationLogsDeletePopupComponent } from './application-logs-delete-dialog.component';
import { IApplicationLogs } from 'app/shared/model/application-logs.model';

@Injectable({ providedIn: 'root' })
export class ApplicationLogsResolve implements Resolve<IApplicationLogs> {
  constructor(private service: ApplicationLogsService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IApplicationLogs> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((applicationLogs: HttpResponse<ApplicationLogs>) => applicationLogs.body));
    }
    return of(new ApplicationLogs());
  }
}

export const applicationLogsRoute: Routes = [
  {
    path: '',
    component: ApplicationLogsComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      defaultSort: 'id,asc',
      pageTitle: 'ApplicationLogs'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ApplicationLogsDetailComponent,
    resolve: {
      applicationLogs: ApplicationLogsResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'ApplicationLogs'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ApplicationLogsUpdateComponent,
    resolve: {
      applicationLogs: ApplicationLogsResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'ApplicationLogs'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ApplicationLogsUpdateComponent,
    resolve: {
      applicationLogs: ApplicationLogsResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'ApplicationLogs'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const applicationLogsPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ApplicationLogsDeletePopupComponent,
    resolve: {
      applicationLogs: ApplicationLogsResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'ApplicationLogs'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
