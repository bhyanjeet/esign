import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IApplicationLogs } from 'app/shared/model/application-logs.model';

type EntityResponseType = HttpResponse<IApplicationLogs>;
type EntityArrayResponseType = HttpResponse<IApplicationLogs[]>;

@Injectable({ providedIn: 'root' })
export class ApplicationLogsService {
  public resourceUrl = SERVER_API_URL + 'api/application-logs';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/application-logs';

  constructor(protected http: HttpClient) {}

  create(applicationLogs: IApplicationLogs): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(applicationLogs);
    return this.http
      .post<IApplicationLogs>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(applicationLogs: IApplicationLogs): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(applicationLogs);
    return this.http
      .put<IApplicationLogs>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IApplicationLogs>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IApplicationLogs[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IApplicationLogs[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(applicationLogs: IApplicationLogs): IApplicationLogs {
    const copy: IApplicationLogs = Object.assign({}, applicationLogs, {
      actionTakenOnDate:
        applicationLogs.actionTakenOnDate != null && applicationLogs.actionTakenOnDate.isValid()
          ? applicationLogs.actionTakenOnDate.format(DATE_FORMAT)
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.actionTakenOnDate = res.body.actionTakenOnDate != null ? moment(res.body.actionTakenOnDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((applicationLogs: IApplicationLogs) => {
        applicationLogs.actionTakenOnDate = applicationLogs.actionTakenOnDate != null ? moment(applicationLogs.actionTakenOnDate) : null;
      });
    }
    return res;
  }
}
