import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { ApplicationLogsComponent } from './application-logs.component';
import { ApplicationLogsDetailComponent } from './application-logs-detail.component';
import { ApplicationLogsUpdateComponent } from './application-logs-update.component';
import { ApplicationLogsDeletePopupComponent, ApplicationLogsDeleteDialogComponent } from './application-logs-delete-dialog.component';
import { applicationLogsRoute, applicationLogsPopupRoute } from './application-logs.route';

const ENTITY_STATES = [...applicationLogsRoute, ...applicationLogsPopupRoute];

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ApplicationLogsComponent,
    ApplicationLogsDetailComponent,
    ApplicationLogsUpdateComponent,
    ApplicationLogsDeleteDialogComponent,
    ApplicationLogsDeletePopupComponent
  ],
  entryComponents: [ApplicationLogsDeleteDialogComponent]
})
export class EsignharyanaApplicationLogsModule {}
