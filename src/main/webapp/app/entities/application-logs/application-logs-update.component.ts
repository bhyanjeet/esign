import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { IApplicationLogs, ApplicationLogs } from 'app/shared/model/application-logs.model';
import { ApplicationLogsService } from './application-logs.service';

@Component({
  selector: 'jhi-application-logs-update',
  templateUrl: './application-logs-update.component.html'
})
export class ApplicationLogsUpdateComponent implements OnInit {
  isSaving: boolean;
  actionTakenOnDateDp: any;

  editForm = this.fb.group({
    id: [],
    actionTaken: [],
    actionTakenBy: [],
    actionTakenOnApplication: [],
    actionTakenOnDate: [],
    remarks: []
  });

  constructor(
    protected applicationLogsService: ApplicationLogsService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ applicationLogs }) => {
      this.updateForm(applicationLogs);
    });
  }

  updateForm(applicationLogs: IApplicationLogs) {
    this.editForm.patchValue({
      id: applicationLogs.id,
      actionTaken: applicationLogs.actionTaken,
      actionTakenBy: applicationLogs.actionTakenBy,
      actionTakenOnApplication: applicationLogs.actionTakenOnApplication,
      actionTakenOnDate: applicationLogs.actionTakenOnDate,
      remarks: applicationLogs.remarks
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const applicationLogs = this.createFromForm();
    if (applicationLogs.id !== undefined) {
      this.subscribeToSaveResponse(this.applicationLogsService.update(applicationLogs));
    } else {
      this.subscribeToSaveResponse(this.applicationLogsService.create(applicationLogs));
    }
  }

  private createFromForm(): IApplicationLogs {
    return {
      ...new ApplicationLogs(),
      id: this.editForm.get(['id']).value,
      actionTaken: this.editForm.get(['actionTaken']).value,
      actionTakenBy: this.editForm.get(['actionTakenBy']).value,
      actionTakenOnApplication: this.editForm.get(['actionTakenOnApplication']).value,
      actionTakenOnDate: this.editForm.get(['actionTakenOnDate']).value,
      remarks: this.editForm.get(['remarks']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IApplicationLogs>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
