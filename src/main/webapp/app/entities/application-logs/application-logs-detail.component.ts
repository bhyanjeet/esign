import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IApplicationLogs } from 'app/shared/model/application-logs.model';

@Component({
  selector: 'jhi-application-logs-detail',
  templateUrl: './application-logs-detail.component.html'
})
export class ApplicationLogsDetailComponent implements OnInit {
  applicationLogs: IApplicationLogs;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ applicationLogs }) => {
      this.applicationLogs = applicationLogs;
    });
  }

  previousState() {
    window.history.back();
  }
}
