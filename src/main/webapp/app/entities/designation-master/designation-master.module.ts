import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { DesignationMasterComponent } from './designation-master.component';
import { DesignationMasterDetailComponent } from './designation-master-detail.component';
import { DesignationMasterUpdateComponent } from './designation-master-update.component';
import {
  DesignationMasterDeletePopupComponent,
  DesignationMasterDeleteDialogComponent
} from './designation-master-delete-dialog.component';
import { designationMasterRoute, designationMasterPopupRoute } from './designation-master.route';

const ENTITY_STATES = [...designationMasterRoute, ...designationMasterPopupRoute];

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    DesignationMasterComponent,
    DesignationMasterDetailComponent,
    DesignationMasterUpdateComponent,
    DesignationMasterDeleteDialogComponent,
    DesignationMasterDeletePopupComponent
  ],
  entryComponents: [DesignationMasterDeleteDialogComponent]
})
export class EsignharyanaDesignationMasterModule {}
