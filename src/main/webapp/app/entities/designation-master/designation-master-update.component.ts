import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { IDesignationMaster, DesignationMaster } from 'app/shared/model/designation-master.model';
import { DesignationMasterService } from './designation-master.service';

@Component({
  selector: 'jhi-designation-master-update',
  templateUrl: './designation-master-update.component.html'
})
export class DesignationMasterUpdateComponent implements OnInit {
  isSaving: boolean;
  createdOnDp: any;
  lastUpdatedOnDp: any;
  verifiedOnDp: any;

  editForm = this.fb.group({
    id: [],
    designationName: [],
    designationAbbreviation: [],
    createdBy: [],
    createdOn: [],
    lastUpdatedBy: [],
    lastUpdatedOn: [],
    verifiedBy: [],
    verifiedOn: [],
    remarks: []
  });

  constructor(
    protected designationMasterService: DesignationMasterService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ designationMaster }) => {
      this.updateForm(designationMaster);
    });
  }

  updateForm(designationMaster: IDesignationMaster) {
    this.editForm.patchValue({
      id: designationMaster.id,
      designationName: designationMaster.designationName,
      designationAbbreviation: designationMaster.designationAbbreviation,
      createdBy: designationMaster.createdBy,
      createdOn: designationMaster.createdOn,
      lastUpdatedBy: designationMaster.lastUpdatedBy,
      lastUpdatedOn: designationMaster.lastUpdatedOn,
      verifiedBy: designationMaster.verifiedBy,
      verifiedOn: designationMaster.verifiedOn,
      remarks: designationMaster.remarks
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const designationMaster = this.createFromForm();
    if (designationMaster.id !== undefined) {
      this.subscribeToSaveResponse(this.designationMasterService.update(designationMaster));
    } else {
      this.subscribeToSaveResponse(this.designationMasterService.create(designationMaster));
    }
  }

  private createFromForm(): IDesignationMaster {
    return {
      ...new DesignationMaster(),
      id: this.editForm.get(['id']).value,
      designationName: this.editForm.get(['designationName']).value,
      designationAbbreviation: this.editForm.get(['designationAbbreviation']).value,
      createdBy: this.editForm.get(['createdBy']).value,
      createdOn: this.editForm.get(['createdOn']).value,
      lastUpdatedBy: this.editForm.get(['lastUpdatedBy']).value,
      lastUpdatedOn: this.editForm.get(['lastUpdatedOn']).value,
      verifiedBy: this.editForm.get(['verifiedBy']).value,
      verifiedOn: this.editForm.get(['verifiedOn']).value,
      remarks: this.editForm.get(['remarks']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDesignationMaster>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
