import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IDesignationMaster } from 'app/shared/model/designation-master.model';

type EntityResponseType = HttpResponse<IDesignationMaster>;
type EntityArrayResponseType = HttpResponse<IDesignationMaster[]>;

@Injectable({ providedIn: 'root' })
export class DesignationMasterService {
  public resourceUrl = SERVER_API_URL + 'api/designation-masters';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/designation-masters';

  constructor(protected http: HttpClient) {}

  create(designationMaster: IDesignationMaster): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(designationMaster);
    return this.http
      .post<IDesignationMaster>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(designationMaster: IDesignationMaster): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(designationMaster);
    return this.http
      .put<IDesignationMaster>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDesignationMaster>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDesignationMaster[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDesignationMaster[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(designationMaster: IDesignationMaster): IDesignationMaster {
    const copy: IDesignationMaster = Object.assign({}, designationMaster, {
      createdOn:
        designationMaster.createdOn != null && designationMaster.createdOn.isValid()
          ? designationMaster.createdOn.format(DATE_FORMAT)
          : null,
      lastUpdatedOn:
        designationMaster.lastUpdatedOn != null && designationMaster.lastUpdatedOn.isValid()
          ? designationMaster.lastUpdatedOn.format(DATE_FORMAT)
          : null,
      verifiedOn:
        designationMaster.verifiedOn != null && designationMaster.verifiedOn.isValid()
          ? designationMaster.verifiedOn.format(DATE_FORMAT)
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
      res.body.lastUpdatedOn = res.body.lastUpdatedOn != null ? moment(res.body.lastUpdatedOn) : null;
      res.body.verifiedOn = res.body.verifiedOn != null ? moment(res.body.verifiedOn) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((designationMaster: IDesignationMaster) => {
        designationMaster.createdOn = designationMaster.createdOn != null ? moment(designationMaster.createdOn) : null;
        designationMaster.lastUpdatedOn = designationMaster.lastUpdatedOn != null ? moment(designationMaster.lastUpdatedOn) : null;
        designationMaster.verifiedOn = designationMaster.verifiedOn != null ? moment(designationMaster.verifiedOn) : null;
      });
    }
    return res;
  }
}
