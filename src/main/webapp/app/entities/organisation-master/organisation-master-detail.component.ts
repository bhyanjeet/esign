import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOrganisationMaster } from 'app/shared/model/organisation-master.model';

@Component({
  selector: 'jhi-organisation-master-detail',
  templateUrl: './organisation-master-detail.component.html'
})
export class OrganisationMasterDetailComponent implements OnInit {
  organisationMaster: IOrganisationMaster | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ organisationMaster }) => (this.organisationMaster = organisationMaster));
  }

  previousState(): void {
    window.history.back();
  }
}
