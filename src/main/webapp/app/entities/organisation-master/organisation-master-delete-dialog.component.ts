import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOrganisationMaster } from 'app/shared/model/organisation-master.model';
import { OrganisationMasterService } from './organisation-master.service';

@Component({
  templateUrl: './organisation-master-delete-dialog.component.html'
})
export class OrganisationMasterDeleteDialogComponent {
  organisationMaster?: IOrganisationMaster;

  constructor(
    protected organisationMasterService: OrganisationMasterService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.organisationMasterService.delete(id).subscribe(() => {
      this.eventManager.broadcast('organisationMasterListModification');
      this.activeModal.close();
    });
  }
}
