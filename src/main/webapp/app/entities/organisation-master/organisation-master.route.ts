import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IOrganisationMaster, OrganisationMaster } from 'app/shared/model/organisation-master.model';
import { OrganisationMasterService } from './organisation-master.service';
import { OrganisationMasterComponent } from './organisation-master.component';
import { OrganisationMasterDetailComponent } from './organisation-master-detail.component';
import { OrganisationMasterUpdateComponent } from './organisation-master-update.component';

@Injectable({ providedIn: 'root' })
export class OrganisationMasterResolve implements Resolve<IOrganisationMaster> {
  constructor(private service: OrganisationMasterService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOrganisationMaster> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((organisationMaster: HttpResponse<OrganisationMaster>) => {
          if (organisationMaster.body) {
            return of(organisationMaster.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new OrganisationMaster());
  }
}

export const organisationMasterRoute: Routes = [
  {
    path: '',
    component: OrganisationMasterComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'OrganisationMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: OrganisationMasterDetailComponent,
    resolve: {
      organisationMaster: OrganisationMasterResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OrganisationMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: OrganisationMasterUpdateComponent,
    resolve: {
      organisationMaster: OrganisationMasterResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OrganisationMasters'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: OrganisationMasterUpdateComponent,
    resolve: {
      organisationMaster: OrganisationMasterResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OrganisationMasters'
    },
    canActivate: [UserRouteAccessService]
  }
];
