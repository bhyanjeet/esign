import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOrganisationMaster } from 'app/shared/model/organisation-master.model';

type EntityResponseType = HttpResponse<IOrganisationMaster>;
type EntityArrayResponseType = HttpResponse<IOrganisationMaster[]>;

@Injectable({ providedIn: 'root' })
export class OrganisationMasterService {

  public resourceUrl = SERVER_API_URL + 'api/organisation-masters';
  public resourceUrlById = SERVER_API_URL + 'api/organisation-masters-By-id';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/organisation-masters';
  public resourceUrlRecordByStatus = SERVER_API_URL + 'api/organisationRecordByStatus';
  public resourceUrlRecordByStatusAndVerifyBy = SERVER_API_URL + 'api/organisationRecordByStatusAndVerifyBy';
  public resourceUrlRecordByUserId = SERVER_API_URL + 'api/organisationRecordByUserId';
  public resourceUrlRecordByOrgIdCode = SERVER_API_URL + 'api/organisationRecordByOrgIdCode';
  public resourceUrlByOrgIdCode = SERVER_API_URL + 'api/organisationsRecordByOrgIdCode';

  getApprovedRecord: any;

  constructor(protected http: HttpClient) {}

  create(organisationMaster: IOrganisationMaster): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(organisationMaster);
    return this.http
      .post<IOrganisationMaster>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(organisationMaster: IOrganisationMaster): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(organisationMaster);
    return this.http
      .put<IOrganisationMaster>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IOrganisationMaster>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IOrganisationMaster[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IOrganisationMaster[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(organisationMaster: IOrganisationMaster): IOrganisationMaster {
    const copy: IOrganisationMaster = Object.assign({}, organisationMaster, {
      // createdOn:
      //   organisationMaster.createdOn != null && organisationMaster.createdOn.isValid()
      //     ? organisationMaster.createdOn.format(DATE_FORMAT)
      //     : null,
      // lastUpdatedOn:
      //   organisationMaster.lastUpdatedOn != null && organisationMaster.lastUpdatedOn.isValid()
      //     ? organisationMaster.lastUpdatedOn.format(DATE_FORMAT)
      //     : null,
      // verifiedOn:
      //   organisationMaster.verifiedOn != null && organisationMaster.verifiedOn.isValid()
      //     ? organisationMaster.verifiedOn.format(DATE_FORMAT)
      //     : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
      res.body.lastUpdatedOn = res.body.lastUpdatedOn != null ? moment(res.body.lastUpdatedOn) : null;
      res.body.verifiedOn = res.body.verifiedOn != null ? moment(res.body.verifiedOn) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((organisationMaster: IOrganisationMaster) => {
        organisationMaster.createdOn = organisationMaster.createdOn != null ? moment(organisationMaster.createdOn) : null;
        organisationMaster.lastUpdatedOn = organisationMaster.lastUpdatedOn != null ? moment(organisationMaster.lastUpdatedOn) : null;
        organisationMaster.verifiedOn = organisationMaster.verifiedOn != null ? moment(organisationMaster.verifiedOn) : null;
      });
    }
    return res;
  }

  getAllRecordByStatus(status: any) {
    const value = new HttpParams().set('status', status);
    return this.http.get<IOrganisationMaster[]>(`${this.resourceUrlRecordByStatus}/`, {
      params: value,
      observe: 'response'
    });
  }

  getAllRecordByStatusAndVerifyBy(status: any, verifyBy: any) {
    const value = new HttpParams().set('status', status).set('verifyBy', verifyBy);
    return this.http.get<IOrganisationMaster[]>(`${this.resourceUrlRecordByStatusAndVerifyBy}/`, {
      params: value,
      observe: 'response'
    });
  }

  findOrganisationByUserId() {
    return this.http
      .get<IOrganisationMaster>(`${this.resourceUrlRecordByUserId}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  getAllRecordByOrgIdCode(oranisationIdCode: any) {
    const value = new HttpParams().set('oranisationIdCode', oranisationIdCode);
    return this.http.get<IOrganisationMaster[]>(`${this.resourceUrlRecordByOrgIdCode}/`, {
      params: value,
      observe: 'response'
    });
  }

  getRecordByOrgIdCode(oranisationIdCode: any) {
    const value = new HttpParams().set('oranisationIdCode', oranisationIdCode);
    return this.http.get(`${this.resourceUrlByOrgIdCode}/`, {
      params: value,
      observe: 'response'
    });
  }

  findById(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IOrganisationMaster>(`${this.resourceUrlById}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }


}
