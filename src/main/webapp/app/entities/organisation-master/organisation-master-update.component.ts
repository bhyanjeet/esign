import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IOrganisationMaster, OrganisationMaster } from 'app/shared/model/organisation-master.model';
import { OrganisationMasterService } from './organisation-master.service';
import { IOrganisationTypeMaster } from 'app/shared/model/organisation-type-master.model';
import { OrganisationTypeMasterService } from 'app/entities/organisation-type-master/organisation-type-master.service';
import { IStateMaster } from 'app/shared/model/state-master.model';
import { StateMasterService } from 'app/entities/state-master/state-master.service';
import { IDistrictMaster } from 'app/shared/model/district-master.model';
import { DistrictMasterService } from 'app/entities/district-master/district-master.service';
import { ISubDistrictMaster } from 'app/shared/model/sub-district-master.model';
import { SubDistrictMasterService } from 'app/entities/sub-district-master/sub-district-master.service';
import { IDepartment } from 'app/shared/model/department.model';
import { DepartmentService } from 'app/entities/department/department.service';

type SelectableEntity = IOrganisationTypeMaster | IStateMaster | IDistrictMaster | ISubDistrictMaster | IDepartment;

@Component({
  selector: 'jhi-organisation-master-update',
  templateUrl: './organisation-master-update.component.html'
})
export class OrganisationMasterUpdateComponent implements OnInit {
  isSaving = false;
  organisationtypemasters: IOrganisationTypeMaster[] = [];
  statemasters: IStateMaster[] = [];
  districtmasters: IDistrictMaster[] = [];
  subdistrictmasters: ISubDistrictMaster[] = [];
  departments: IDepartment[] = [];
  createdOnDp: any;
  lastUpdatedOnDp: any;
  verifiedOnDp: any;
  dateOfBirthDp: any;

  editForm = this.fb.group({
    id: [],
    organisationName: [null, [Validators.required]],
    organisationAbbreviation: [null, [Validators.required]],
    organisationCorrespondenceEmail: [null, [Validators.required]],
    organisationCorrespondencePhone1: [null, [Validators.required]],
    organisationCorrespondencePhone2: [],
    organisationCorrespondenceAddress: [],
    organisationWebsite: [],
    organisationNodalOfficerName: [],
    organisationNodalOfficerPhoneMobile: [],
    organisationNodalOfficerPhoneLandline: [],
    nodalOfficerOfficialEmail: [],
    organisationNodalOfficerAlternativeEmail: [],
    createdBy: [],
    createdOn: [],
    lastUpdatedBy: [],
    lastUpdatedOn: [],
    verifiedBy: [],
    verifiedOn: [],
    remarks: [],
    userId: [],
    stage: [],
    status: [],
    organisationIdCode: [],
    organisationEmployeeId: [],
    dateOfBirth: [],
    gender: [],
    pan: [],
    aadhaar: [],
    orgnisationEmpolyeeCardAttchment: [],
    panAttachment: [],
    photographAttachment: [],
    aadhaarAttachment: [],
    pinCode: [],
    organisationTypeMasterId: [],
    stateMasterId: [],
    districtMasterId: [],
    subDistrictMasterId: [],
    departmentId: []
  });

  constructor(
    protected organisationMasterService: OrganisationMasterService,
    protected organisationTypeMasterService: OrganisationTypeMasterService,
    protected stateMasterService: StateMasterService,
    protected districtMasterService: DistrictMasterService,
    protected subDistrictMasterService: SubDistrictMasterService,
    protected departmentService: DepartmentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ organisationMaster }) => {
      this.updateForm(organisationMaster);

      this.organisationTypeMasterService
        .query()
        .subscribe((res: HttpResponse<IOrganisationTypeMaster[]>) => (this.organisationtypemasters = res.body || []));

      this.stateMasterService.query().subscribe((res: HttpResponse<IStateMaster[]>) => (this.statemasters = res.body || []));

      this.districtMasterService.query().subscribe((res: HttpResponse<IDistrictMaster[]>) => (this.districtmasters = res.body || []));

      this.subDistrictMasterService
        .query()
        .subscribe((res: HttpResponse<ISubDistrictMaster[]>) => (this.subdistrictmasters = res.body || []));

      this.departmentService.query().subscribe((res: HttpResponse<IDepartment[]>) => (this.departments = res.body || []));
    });
  }

  updateForm(organisationMaster: IOrganisationMaster): void {
    this.editForm.patchValue({
      id: organisationMaster.id,
      organisationName: organisationMaster.organisationName,
      organisationAbbreviation: organisationMaster.organisationAbbreviation,
      organisationCorrespondenceEmail: organisationMaster.organisationCorrespondenceEmail,
      organisationCorrespondencePhone1: organisationMaster.organisationCorrespondencePhone1,
      organisationCorrespondencePhone2: organisationMaster.organisationCorrespondencePhone2,
      organisationCorrespondenceAddress: organisationMaster.organisationCorrespondenceAddress,
      organisationWebsite: organisationMaster.organisationWebsite,
      organisationNodalOfficerName: organisationMaster.organisationNodalOfficerName,
      organisationNodalOfficerPhoneMobile: organisationMaster.organisationNodalOfficerPhoneMobile,
      organisationNodalOfficerPhoneLandline: organisationMaster.organisationNodalOfficerPhoneLandline,
      nodalOfficerOfficialEmail: organisationMaster.nodalOfficerOfficialEmail,
      organisationNodalOfficerAlternativeEmail: organisationMaster.organisationNodalOfficerAlternativeEmail,
      createdBy: organisationMaster.createdBy,
      createdOn: organisationMaster.createdOn,
      lastUpdatedBy: organisationMaster.lastUpdatedBy,
      lastUpdatedOn: organisationMaster.lastUpdatedOn,
      verifiedBy: organisationMaster.verifiedBy,
      verifiedOn: organisationMaster.verifiedOn,
      remarks: organisationMaster.remarks,
      userId: organisationMaster.userId,
      stage: organisationMaster.stage,
      status: organisationMaster.status,
      organisationIdCode: organisationMaster.organisationIdCode,
      organisationEmployeeId: organisationMaster.organisationEmployeeId,
      dateOfBirth: organisationMaster.dateOfBirth,
      gender: organisationMaster.gender,
      pan: organisationMaster.pan,
      aadhaar: organisationMaster.aadhaar,
      orgnisationEmpolyeeCardAttchment: organisationMaster.orgnisationEmpolyeeCardAttchment,
      panAttachment: organisationMaster.panAttachment,
      photographAttachment: organisationMaster.photographAttachment,
      aadhaarAttachment: organisationMaster.aadhaarAttachment,
      pinCode: organisationMaster.pinCode,
      organisationTypeMasterId: organisationMaster.organisationTypeMasterId,
      stateMasterId: organisationMaster.stateMasterId,
      districtMasterId: organisationMaster.districtMasterId,
      subDistrictMasterId: organisationMaster.subDistrictMasterId,
      departmentId: organisationMaster.departmentId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const organisationMaster = this.createFromForm();
    if (organisationMaster.id !== undefined) {
      this.subscribeToSaveResponse(this.organisationMasterService.update(organisationMaster));
    } else {
      this.subscribeToSaveResponse(this.organisationMasterService.create(organisationMaster));
    }
  }

  private createFromForm(): IOrganisationMaster {
    return {
      ...new OrganisationMaster(),
      id: this.editForm.get(['id'])!.value,
      organisationName: this.editForm.get(['organisationName'])!.value,
      organisationAbbreviation: this.editForm.get(['organisationAbbreviation'])!.value,
      organisationCorrespondenceEmail: this.editForm.get(['organisationCorrespondenceEmail'])!.value,
      organisationCorrespondencePhone1: this.editForm.get(['organisationCorrespondencePhone1'])!.value,
      organisationCorrespondencePhone2: this.editForm.get(['organisationCorrespondencePhone2'])!.value,
      organisationCorrespondenceAddress: this.editForm.get(['organisationCorrespondenceAddress'])!.value,
      organisationWebsite: this.editForm.get(['organisationWebsite'])!.value,
      organisationNodalOfficerName: this.editForm.get(['organisationNodalOfficerName'])!.value,
      organisationNodalOfficerPhoneMobile: this.editForm.get(['organisationNodalOfficerPhoneMobile'])!.value,
      organisationNodalOfficerPhoneLandline: this.editForm.get(['organisationNodalOfficerPhoneLandline'])!.value,
      nodalOfficerOfficialEmail: this.editForm.get(['nodalOfficerOfficialEmail'])!.value,
      organisationNodalOfficerAlternativeEmail: this.editForm.get(['organisationNodalOfficerAlternativeEmail'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value,
      createdOn: this.editForm.get(['createdOn'])!.value,
      lastUpdatedBy: this.editForm.get(['lastUpdatedBy'])!.value,
      lastUpdatedOn: this.editForm.get(['lastUpdatedOn'])!.value,
      verifiedBy: this.editForm.get(['verifiedBy'])!.value,
      verifiedOn: this.editForm.get(['verifiedOn'])!.value,
      remarks: this.editForm.get(['remarks'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      stage: this.editForm.get(['stage'])!.value,
      status: this.editForm.get(['status'])!.value,
      organisationIdCode: this.editForm.get(['organisationIdCode'])!.value,
      organisationEmployeeId: this.editForm.get(['organisationEmployeeId'])!.value,
      dateOfBirth: this.editForm.get(['dateOfBirth'])!.value,
      gender: this.editForm.get(['gender'])!.value,
      pan: this.editForm.get(['pan'])!.value,
      aadhaar: this.editForm.get(['aadhaar'])!.value,
      orgnisationEmpolyeeCardAttchment: this.editForm.get(['orgnisationEmpolyeeCardAttchment'])!.value,
      panAttachment: this.editForm.get(['panAttachment'])!.value,
      photographAttachment: this.editForm.get(['photographAttachment'])!.value,
      aadhaarAttachment: this.editForm.get(['aadhaarAttachment'])!.value,
      pinCode: this.editForm.get(['pinCode'])!.value,
      organisationTypeMasterId: this.editForm.get(['organisationTypeMasterId'])!.value,
      stateMasterId: this.editForm.get(['stateMasterId'])!.value,
      districtMasterId: this.editForm.get(['districtMasterId'])!.value,
      subDistrictMasterId: this.editForm.get(['subDistrictMasterId'])!.value,
      departmentId: this.editForm.get(['departmentId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrganisationMaster>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
