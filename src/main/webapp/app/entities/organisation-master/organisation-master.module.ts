import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { OrganisationMasterComponent } from './organisation-master.component';
import { OrganisationMasterDetailComponent } from './organisation-master-detail.component';
import { OrganisationMasterUpdateComponent } from './organisation-master-update.component';
import { OrganisationMasterDeleteDialogComponent } from './organisation-master-delete-dialog.component';
import { organisationMasterRoute } from './organisation-master.route';

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(organisationMasterRoute)],
  declarations: [
    OrganisationMasterComponent,
    OrganisationMasterDetailComponent,
    OrganisationMasterUpdateComponent,
    OrganisationMasterDeleteDialogComponent
  ],
  entryComponents: [OrganisationMasterDeleteDialogComponent]
})
export class EsignharyanaOrganisationMasterModule {}
