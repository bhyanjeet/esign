import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars

import { SERVER_API_URL } from 'app/app.constants';
import { IUserDetails } from 'app/shared/model/user-details.model';
import {IOrganisationDocument} from "app/shared/model/organisation-document.model";
import {IOrganisationMaster} from "app/shared/model/organisation-master.model";

type EntityResponseType = HttpResponse<IUserDetails>;
type EntityArrayResponseType = HttpResponse<IUserDetails[]>;

@Injectable({ providedIn: 'root' })
export class VerifyOrgService {
  public resourceSearchUrlVerifyOrg = SERVER_API_URL + 'api/orgKYC';

  constructor(protected http: HttpClient) {}

  verifyOrg(organisationIdCode: any) {

    const value = new HttpParams().set('organisationIdCode', organisationIdCode);
    return this.http.get<IOrganisationMaster>(`${this.resourceSearchUrlVerifyOrg}/`, {
      params: value,
      observe: 'response'
    });
  }
}
