import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { ExternalUserEsignRequestComponent } from './external-user-esign-request.component';
import { ExternalUserEsignRequestDetailComponent } from './external-user-esign-request-detail.component';
import { ExternalUserEsignRequestUpdateComponent } from './external-user-esign-request-update.component';
import { ExternalUserEsignRequestDeleteDialogComponent } from './external-user-esign-request-delete-dialog.component';
import { externalUserEsignRequestRoute } from './external-user-esign-request.route';

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(externalUserEsignRequestRoute)],
  declarations: [
    ExternalUserEsignRequestComponent,
    ExternalUserEsignRequestDetailComponent,
    ExternalUserEsignRequestUpdateComponent,
    ExternalUserEsignRequestDeleteDialogComponent
  ],
  entryComponents: [ExternalUserEsignRequestDeleteDialogComponent]
})
export class EsignharyanaExternalUserEsignRequestModule {}
