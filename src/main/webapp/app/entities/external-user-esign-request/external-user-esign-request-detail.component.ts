import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IExternalUserEsignRequest } from 'app/shared/model/external-user-esign-request.model';

@Component({
  selector: 'jhi-external-user-esign-request-detail',
  templateUrl: './external-user-esign-request-detail.component.html'
})
export class ExternalUserEsignRequestDetailComponent implements OnInit {
  externalUserEsignRequest: IExternalUserEsignRequest | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ externalUserEsignRequest }) => (this.externalUserEsignRequest = externalUserEsignRequest));
  }

  previousState(): void {
    window.history.back();
  }
}
