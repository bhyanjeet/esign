import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IExternalUserEsignRequest } from 'app/shared/model/external-user-esign-request.model';
import { ExternalUserEsignRequestService } from './external-user-esign-request.service';

@Component({
  templateUrl: './external-user-esign-request-delete-dialog.component.html'
})
export class ExternalUserEsignRequestDeleteDialogComponent {
  externalUserEsignRequest?: IExternalUserEsignRequest;

  constructor(
    protected externalUserEsignRequestService: ExternalUserEsignRequestService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.externalUserEsignRequestService.delete(id).subscribe(() => {
      this.eventManager.broadcast('externalUserEsignRequestListModification');
      this.activeModal.close();
    });
  }
}
