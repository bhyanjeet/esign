import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IExternalUserEsignRequest } from 'app/shared/model/external-user-esign-request.model';
import {createRequestOption} from "app/shared/util/request-util";
import {IOrganisationMaster} from "app/shared/model/organisation-master.model";
import {map} from "rxjs/operators";



type EntityResponseType = HttpResponse<IExternalUserEsignRequest>;
type EntityArrayResponseType = HttpResponse<IExternalUserEsignRequest[]>;

@Injectable({ providedIn: 'root' })
export class ExternalUserEsignRequestService {
  public resourceUrl = SERVER_API_URL + 'api/external-user-esign-requests';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/external-user-esign-requests';
  public resourceUrleSignFormRequest = SERVER_API_URL + 'api/userEsignRequest';
  public resourceUrlByTxn = SERVER_API_URL + 'api/findByTxn';

  constructor(protected http: HttpClient) {}

  create(externalUserEsignRequest: IExternalUserEsignRequest): Observable<EntityResponseType> {
    return this.http.post<IExternalUserEsignRequest>(this.resourceUrl, externalUserEsignRequest, { observe: 'response' });
  }

  update(externalUserEsignRequest: IExternalUserEsignRequest): Observable<EntityResponseType> {
    return this.http.put<IExternalUserEsignRequest>(this.resourceUrl, externalUserEsignRequest, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IExternalUserEsignRequest>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IExternalUserEsignRequest[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IExternalUserEsignRequest[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
  protected convertDateFromClient(externalUserEsignRequest: IExternalUserEsignRequest): IExternalUserEsignRequest {
    const copy: IExternalUserEsignRequest = Object.assign({}, externalUserEsignRequest, {

    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      // res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
      // res.body.lastUpdatedOn = res.body.lastUpdatedOn != null ? moment(res.body.lastUpdatedOn) : null;
      // res.body.verifiedOn = res.body.verifiedOn != null ? moment(res.body.verifiedOn) : null;
    }
    return res;
  }


  // sendForEsign(externalUserEsignRequest: IExternalUserEsignRequest): Observable<EntityResponseType> {
  //   const copy = this.convertDateFromClient(externalUserEsignRequest);
  //   return this.http
  //     .post<IExternalUserEsignRequest>(this.resourceUrleSignFormRequest, copy, { observe: 'response' })
  //     .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  // }

  sendForEsign(externalUserEsignRequest: IExternalUserEsignRequest): Observable<EntityResponseType> {
    return this.http.post<IExternalUserEsignRequest>(this.resourceUrleSignFormRequest, externalUserEsignRequest, { observe: 'response' });
  }


  findByTxn(txn: any): Observable<EntityResponseType> {
    const value = new HttpParams().set('Txn', txn);
    return this.http
      .get<IExternalUserEsignRequest>(`${this.resourceUrlByTxn}`, {params: value, observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

}
