import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IExternalUserEsignRequest, ExternalUserEsignRequest } from 'app/shared/model/external-user-esign-request.model';
import { ExternalUserEsignRequestService } from './external-user-esign-request.service';
import { ExternalUserEsignRequestComponent } from './external-user-esign-request.component';
import { ExternalUserEsignRequestDetailComponent } from './external-user-esign-request-detail.component';
import { ExternalUserEsignRequestUpdateComponent } from './external-user-esign-request-update.component';

@Injectable({ providedIn: 'root' })
export class ExternalUserEsignRequestResolve implements Resolve<IExternalUserEsignRequest> {
  constructor(private service: ExternalUserEsignRequestService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IExternalUserEsignRequest> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((externalUserEsignRequest: HttpResponse<ExternalUserEsignRequest>) => {
          if (externalUserEsignRequest.body) {
            return of(externalUserEsignRequest.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ExternalUserEsignRequest());
  }
}

export const externalUserEsignRequestRoute: Routes = [
  {
    path: '',
    component: ExternalUserEsignRequestComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'ExternalUserEsignRequests'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ExternalUserEsignRequestDetailComponent,
    resolve: {
      externalUserEsignRequest: ExternalUserEsignRequestResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ExternalUserEsignRequests'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ExternalUserEsignRequestUpdateComponent,
    resolve: {
      externalUserEsignRequest: ExternalUserEsignRequestResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ExternalUserEsignRequests'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ExternalUserEsignRequestUpdateComponent,
    resolve: {
      externalUserEsignRequest: ExternalUserEsignRequestResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'ExternalUserEsignRequests'
    },
    canActivate: [UserRouteAccessService]
  }
];
