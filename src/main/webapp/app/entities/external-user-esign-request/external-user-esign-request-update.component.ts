import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IExternalUserEsignRequest, ExternalUserEsignRequest } from 'app/shared/model/external-user-esign-request.model';
import { ExternalUserEsignRequestService } from './external-user-esign-request.service';

@Component({
  selector: 'jhi-external-user-esign-request-update',
  templateUrl: './external-user-esign-request-update.component.html'
})
export class ExternalUserEsignRequestUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    userCodeId: [],
    applicationIdCode: [],
    responseUrl: [],
    redirectUrl: [],
    ts: [],
    signerId: [],
    docInfo: [],
    docUrl: [],
    docHash: [],
    requestStatus: [],
    requestTime: [],
    txn: [],
    txnRef: []
  });

  constructor(
    protected externalUserEsignRequestService: ExternalUserEsignRequestService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ externalUserEsignRequest }) => {
      this.updateForm(externalUserEsignRequest);
    });
  }

  updateForm(externalUserEsignRequest: IExternalUserEsignRequest): void {
    this.editForm.patchValue({
      id: externalUserEsignRequest.id,
      userCodeId: externalUserEsignRequest.userCodeId,
      applicationIdCode: externalUserEsignRequest.applicationIdCode,
      responseUrl: externalUserEsignRequest.responseUrl,
      redirectUrl: externalUserEsignRequest.redirectUrl,
      ts: externalUserEsignRequest.ts,
      signerId: externalUserEsignRequest.signerId,
      docInfo: externalUserEsignRequest.docInfo,
      docUrl: externalUserEsignRequest.docUrl,
      docHash: externalUserEsignRequest.docHash,
      requestStatus: externalUserEsignRequest.requestStatus,
      requestTime: externalUserEsignRequest.requestTime,
      txn: externalUserEsignRequest.txn,
      txnRef: externalUserEsignRequest.txnRef
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const externalUserEsignRequest = this.createFromForm();
    if (externalUserEsignRequest.id !== undefined) {
      this.subscribeToSaveResponse(this.externalUserEsignRequestService.update(externalUserEsignRequest));
    } else {
      this.subscribeToSaveResponse(this.externalUserEsignRequestService.create(externalUserEsignRequest));
    }
  }

  private createFromForm(): IExternalUserEsignRequest {
    return {
      ...new ExternalUserEsignRequest(),
      id: this.editForm.get(['id'])!.value,
      userCodeId: this.editForm.get(['userCodeId'])!.value,
      applicationIdCode: this.editForm.get(['applicationIdCode'])!.value,
      responseUrl: this.editForm.get(['responseUrl'])!.value,
      redirectUrl: this.editForm.get(['redirectUrl'])!.value,
      ts: this.editForm.get(['ts'])!.value,
      signerId: this.editForm.get(['signerId'])!.value,
      docInfo: this.editForm.get(['docInfo'])!.value,
      docUrl: this.editForm.get(['docUrl'])!.value,
      docHash: this.editForm.get(['docHash'])!.value,
      requestStatus: this.editForm.get(['requestStatus'])!.value,
      requestTime: this.editForm.get(['requestTime'])!.value,
      txn: this.editForm.get(['txn'])!.value,
      txnRef: this.editForm.get(['txnRef'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExternalUserEsignRequest>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
