import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'state-master',
        loadChildren: () => import('./state-master/state-master.module').then(m => m.EsignharyanaStateMasterModule)
      },
      {
        path: 'district-master',
        loadChildren: () => import('./district-master/district-master.module').then(m => m.EsignharyanaDistrictMasterModule)
      },
      {
        path: 'sub-district-master',
        loadChildren: () => import('./sub-district-master/sub-district-master.module').then(m => m.EsignharyanaSubDistrictMasterModule)
      },
      {
        path: 'block-master',
        loadChildren: () => import('./block-master/block-master.module').then(m => m.EsignharyanaBlockMasterModule)
      },
      {
        path: 'organisation-type-master',
        loadChildren: () =>
          import('./organisation-type-master/organisation-type-master.module').then(m => m.EsignharyanaOrganisationTypeMasterModule)
      },
      {
        path: 'designation-master',
        loadChildren: () => import('./designation-master/designation-master.module').then(m => m.EsignharyanaDesignationMasterModule)
      },
      {
        path: 'esign-role',
        loadChildren: () => import('./esign-role/esign-role.module').then(m => m.EsignharyanaEsignRoleModule)
      },
      {
        path: 'organisation-master',
        loadChildren: () => import('./organisation-master/organisation-master.module').then(m => m.EsignharyanaOrganisationMasterModule)
      },
      {
        path: 'user-details',
        loadChildren: () => import('./user-details/user-details.module').then(m => m.EsignharyanaUserDetailsModule)
      },
      {
        path: 'organisation-document',
        loadChildren: () =>
          import('./organisation-document/organisation-document.module').then(m => m.EsignharyanaOrganisationDocumentModule)
      },
      {
        path: 'application-master',
        loadChildren: () => import('./application-master/application-master.module').then(m => m.EsignharyanaApplicationMasterModule)
      },
      {
        path: 'otp',
        loadChildren: () => import('./otp/otp.module').then(m => m.EsignharyanaOtpModule)
      },
      {
        path: 'organisation-log',
        loadChildren: () => import('./organisation-log/organisation-log.module').then(m => m.EsignharyanaOrganisationLogModule)
      },
      {
        path: 'organisation-attachment',
        loadChildren: () =>
          import('./organisation-attachment/organisation-attachment.module').then(m => m.EsignharyanaOrganisationAttachmentModule)
      },
      {
        path: 'user-application',
        loadChildren: () => import('./user-application/user-application.module').then(m => m.EsignharyanaUserApplicationModule)
      },
      {
        path: 'application-logs',
        loadChildren: () => import('./application-logs/application-logs.module').then(m => m.EsignharyanaApplicationLogsModule)
      },
      {
        path: 'user-logs',
        loadChildren: () => import('./user-logs/user-logs.module').then(m => m.EsignharyanaUserLogsModule)
      },
      {
        path: 'esign-transation',
        loadChildren: () => import('./esign-transation/esign-transation.module').then(m => m.EsignharyanaEsignTransationModule)
      },
      {
        path: 'department',
        loadChildren: () => import('./department/department.module').then(m => m.EsignharyanaDepartmentModule)
      },
      {
        path: 'user-auth-details',
        loadChildren: () => import('./user-auth-details/user-auth-details.module').then(m => m.EsignharyanaUserAuthDetailsModule)
      },
      {
        path: 'external-user-esign-request',
        loadChildren: () =>
          import('./external-user-esign-request/external-user-esign-request.module').then(m => m.EsignharyanaExternalUserEsignRequestModule)
      },
      {
        path: 'esign-error',
        loadChildren: () => import('./esign-error/esign-error.module').then(m => m.EsignharyanaEsignErrorModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class EsignharyanaEntityModule {}
