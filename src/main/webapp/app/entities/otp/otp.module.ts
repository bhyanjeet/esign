import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { OtpComponent } from './otp.component';
import { OtpDetailComponent } from './otp-detail.component';
import { OtpUpdateComponent } from './otp-update.component';
import { OtpDeletePopupComponent, OtpDeleteDialogComponent } from './otp-delete-dialog.component';
import { otpRoute, otpPopupRoute } from './otp.route';

const ENTITY_STATES = [...otpRoute, ...otpPopupRoute];

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [OtpComponent, OtpDetailComponent, OtpUpdateComponent, OtpDeleteDialogComponent, OtpDeletePopupComponent],
  entryComponents: [OtpDeleteDialogComponent]
})
export class EsignharyanaOtpModule {}
