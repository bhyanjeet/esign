import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOtp } from 'app/shared/model/otp.model';

@Component({
  selector: 'jhi-otp-detail',
  templateUrl: './otp-detail.component.html'
})
export class OtpDetailComponent implements OnInit {
  otp: IOtp;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ otp }) => {
      this.otp = otp;
    });
  }

  previousState() {
    window.history.back();
  }
}
