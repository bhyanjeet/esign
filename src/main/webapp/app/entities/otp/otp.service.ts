import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOtp } from 'app/shared/model/otp.model';

type EntityResponseType = HttpResponse<IOtp>;
type EntityArrayResponseType = HttpResponse<IOtp[]>;

@Injectable({ providedIn: 'root' })
export class OtpService {
  public resourceUrl = SERVER_API_URL + 'api/otps';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/otps';
  public resourceUrlSendOtp = SERVER_API_URL + 'api/sendOtp';
  public resourceUrlVerifyOtp = SERVER_API_URL + 'api/verifyOtp';

  constructor(protected http: HttpClient) {}

  create(otp: IOtp): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(otp);
    return this.http
      .post<IOtp>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(otp: IOtp): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(otp);
    return this.http
      .put<IOtp>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IOtp>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IOtp[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IOtp[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(otp: IOtp): IOtp {
    const copy: IOtp = Object.assign({}, otp, {
      requestTime: otp.requestTime != null && otp.requestTime.isValid() ? otp.requestTime.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.requestTime = res.body.requestTime != null ? moment(res.body.requestTime) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((otp: IOtp) => {
        otp.requestTime = otp.requestTime != null ? moment(otp.requestTime) : null;
      });
    }
    return res;
  }
  sendOtpByMobile(mobile: any): Observable<any> {
    const value = new HttpParams().set('mobile', mobile);
    return this.http.get<any>(`${this.resourceUrlSendOtp}`, { params: value, observe: 'response' });
  }

  verifyOtpByMobile(mobile: any, otp: any): Observable<any> {
    const value = new HttpParams().set('mobile', mobile).set('otp', otp);
    return this.http.get<boolean>(`${this.resourceUrlVerifyOtp}`, { params: value, observe: 'response' });
  }
}
