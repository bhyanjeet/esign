import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Otp } from 'app/shared/model/otp.model';
import { OtpService } from './otp.service';
import { OtpComponent } from './otp.component';
import { OtpDetailComponent } from './otp-detail.component';
import { OtpUpdateComponent } from './otp-update.component';
import { OtpDeletePopupComponent } from './otp-delete-dialog.component';
import { IOtp } from 'app/shared/model/otp.model';

@Injectable({ providedIn: 'root' })
export class OtpResolve implements Resolve<IOtp> {
  constructor(private service: OtpService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOtp> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((otp: HttpResponse<Otp>) => otp.body));
    }
    return of(new Otp());
  }
}

export const otpRoute: Routes = [
  {
    path: '',
    component: OtpComponent,
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'Otps'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: OtpDetailComponent,
    resolve: {
      otp: OtpResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'Otps'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: OtpUpdateComponent,
    resolve: {
      otp: OtpResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'Otps'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: OtpUpdateComponent,
    resolve: {
      otp: OtpResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'Otps'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const otpPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: OtpDeletePopupComponent,
    resolve: {
      otp: OtpResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'Otps'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
