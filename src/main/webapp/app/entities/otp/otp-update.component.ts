import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IOtp, Otp } from 'app/shared/model/otp.model';
import { OtpService } from './otp.service';

@Component({
  selector: 'jhi-otp-update',
  templateUrl: './otp-update.component.html'
})
export class OtpUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    otp: [],
    mobileNumber: [],
    requestTime: []
  });

  constructor(protected otpService: OtpService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ otp }) => {
      this.updateForm(otp);
    });
  }

  updateForm(otp: IOtp) {
    this.editForm.patchValue({
      id: otp.id,
      otp: otp.otp,
      mobileNumber: otp.mobileNumber,
      requestTime: otp.requestTime != null ? otp.requestTime.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const otp = this.createFromForm();
    if (otp.id !== undefined) {
      this.subscribeToSaveResponse(this.otpService.update(otp));
    } else {
      this.subscribeToSaveResponse(this.otpService.create(otp));
    }
  }

  private createFromForm(): IOtp {
    return {
      ...new Otp(),
      id: this.editForm.get(['id']).value,
      otp: this.editForm.get(['otp']).value,
      mobileNumber: this.editForm.get(['mobileNumber']).value,
      requestTime:
        this.editForm.get(['requestTime']).value != null ? moment(this.editForm.get(['requestTime']).value, DATE_TIME_FORMAT) : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOtp>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
