import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IOtp } from 'app/shared/model/otp.model';
import { OtpService } from './otp.service';

@Component({
  selector: 'jhi-otp',
  templateUrl: './otp.component.html'
})
export class OtpComponent implements OnInit, OnDestroy {
  otps: IOtp[];
  eventSubscriber: Subscription;
  currentSearch: string;

  constructor(protected otpService: OtpService, protected eventManager: JhiEventManager, protected activatedRoute: ActivatedRoute) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll() {
    if (this.currentSearch) {
      this.otpService
        .search({
          query: this.currentSearch
        })
        .subscribe((res: HttpResponse<IOtp[]>) => (this.otps = res.body));
      return;
    }
    this.otpService.query().subscribe((res: HttpResponse<IOtp[]>) => {
      this.otps = res.body;
      this.currentSearch = '';
    });
  }

  search(query) {
    if (!query) {
      return this.clear();
    }
    this.currentSearch = query;
    this.loadAll();
  }

  clear() {
    this.currentSearch = '';
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.registerChangeInOtps();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IOtp) {
    return item.id;
  }

  registerChangeInOtps() {
    this.eventSubscriber = this.eventManager.subscribe('otpListModification', () => this.loadAll());
  }
}
