import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOtp } from 'app/shared/model/otp.model';
import { OtpService } from './otp.service';

@Component({
  selector: 'jhi-otp-delete-dialog',
  templateUrl: './otp-delete-dialog.component.html'
})
export class OtpDeleteDialogComponent {
  otp: IOtp;

  constructor(protected otpService: OtpService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.otpService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'otpListModification',
        content: 'Deleted an otp'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-otp-delete-popup',
  template: ''
})
export class OtpDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ otp }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(OtpDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.otp = otp;
        this.ngbModalRef.result.then(
          () => {
            this.router.navigate(['/otp', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          () => {
            this.router.navigate(['/otp', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
