import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from '../shared/shared.module';
import {ESIGN_RESPONSE_DATA_ROUTE} from "app/esign-response-data/esign-response-data.route";
import {EsignResponseDataComponent} from "app/esign-response-data/esign-response-data.component";
import {NgxSpinnerModule} from "ngx-spinner";


@NgModule({
  imports: [
    EsignharyanaSharedModule,
    RouterModule.forRoot([ESIGN_RESPONSE_DATA_ROUTE], {useHash: true}),
    NgxSpinnerModule
  ],
    declarations: [
      EsignResponseDataComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppEsignResponseDataModule {}
