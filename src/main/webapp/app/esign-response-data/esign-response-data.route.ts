import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { EsignResponseDataComponent } from './esign-response-data.component';

export const ESIGN_RESPONSE_DATA_ROUTE: Route = {
  path: 'esign-response-data',
  component: EsignResponseDataComponent,
  data: {
    authorities: ['ROLE_ADMIN'],
    pageTitle: 'esign-response-data.title'
  },
};
