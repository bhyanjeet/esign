import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IEsignTransation } from 'app/shared/model/esign-transation.model';
import {IOrganisationMaster} from "app/shared/model/organisation-master.model";

type EntityResponseType = HttpResponse<IEsignTransation>;
type EntityArrayResponseType = HttpResponse<IEsignTransation[]>;

@Injectable({ providedIn: 'root' })
export class EsignResponseDataService {
  public resourceUrl = SERVER_API_URL + 'api/esign/response-data';

  constructor(protected http: HttpClient) {}

  findResData(resData: any) {
    const value = new HttpParams().set('resData', resData);
    return this.http.get(`${this.resourceUrl}`, {
      params: value,
      observe: 'response'
    });
  }
}
