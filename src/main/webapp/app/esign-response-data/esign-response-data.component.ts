import {Component, OnInit} from '@angular/core';
import {EsignResponseDataService} from "app/esign-response-data/esign-response-data.service";
import {Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'jhi-esign-response-data',
  templateUrl: './esign-response-data.component.html',
  styleUrls: [
    'esign-response-data.component.scss'
  ]
})
export class EsignResponseDataComponent implements OnInit {

  message: string;
  resData: any;
  data: any;
  showLoadingIndicator: boolean;

  constructor(private esignResponseDataService: EsignResponseDataService,
              private router: Router, private spinner: NgxSpinnerService) {
    this.message = 'EsignResponseDataComponent message';
  }

  ngOnInit() {
    this.spinner.show();

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 30000);

    this.resData = '234567';
    this.esignResponseDataService.findResData(this.resData).subscribe(data => {
      console.log(this.resData);
      // this.spinner.hide();
    });
  }

}
