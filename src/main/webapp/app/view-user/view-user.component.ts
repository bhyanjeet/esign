import {Component, OnInit} from '@angular/core';
import {IUserDetails} from "app/shared/model/user-details.model";
import {ActivatedRoute} from "@angular/router";
import {DesignationMasterService} from "../entities/designation-master/designation-master.service";
import {HttpResponse} from "@angular/common/http";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {DomSanitizer} from "@angular/platform-browser";
import {MinioUploadService} from "../shared/Services/minioUpload.service";

@Component({
  selector: 'jhi-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: [
    'view-user.component.scss'
  ]
})
export class ViewUserComponent implements OnInit {
  userDetails: IUserDetails;
  designationId: any;
  designation: any;
  url: any;

  message: string;
  photographName: any;
  photourl: any;
  checkvalue: boolean;

  constructor(protected activatedRoute: ActivatedRoute, protected designationService: DesignationMasterService,
              protected minioUploadService: MinioUploadService,
              protected sanitizer: DomSanitizer,
              protected modalService: NgbModal) {
    this.message = 'ViewUserComponent message';
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userDetails }) => {

      this.userDetails = userDetails;
      if (this.userDetails.userIdCode!==null){
        this.checkvalue= true;
      }else {
        this.checkvalue =false;
      }

      this.designationId = this.userDetails.designationMasterId;
      this.designationService.find(this.designationId).subscribe(res => {
        this.designation = res.body;
      });

        this.photographName = this.userDetails.photograph;

        const fileExtension = this.photographName.split('.');
        const extensionFirst = fileExtension[1];
        const extension = fileExtension[2];
        if (extensionFirst === 'pdf' || extension === 'pdf') {
          this.minioUploadService.getMinioFileUrl(this.photographName).subscribe((result: HttpResponse<any>) => {
            this.photourl = this.sanitizer.bypassSecurityTrustResourceUrl('data:application/pdf;base64,' + result.body);

          });
        } else {
          this.minioUploadService.getMinioFileUrl(this.photographName).subscribe((result: HttpResponse<any>) => {
            this.photourl = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + result.body);

          });
        }


      });

  }

  getMinioImage(fileName, content) {
    console.log(fileName);
    const fileExtension = fileName.split('.');
    const extensionFirst = fileExtension[1];
    const extension = fileExtension[2];
    if (extensionFirst === 'pdf' || extension === 'pdf') {
      this.minioUploadService.getMinioFileUrl(fileName).subscribe((result: HttpResponse<any>) => {
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl('data:application/pdf;base64,' + result.body);
        this.openModal(content);
      });
    } else {
      this.minioUploadService.getMinioFileUrl(fileName).subscribe((result: HttpResponse<any>) => {
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + result.body);
        this.openModal(content);
      });
    }
  }
  openModal(content) {
    this.modalService.open(content, {size: 'xl', backdrop: 'static'});
  }

  previous() {
    window.history.back();
  }
}
