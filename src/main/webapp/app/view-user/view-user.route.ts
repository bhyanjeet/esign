import {ActivatedRouteSnapshot, Resolve, Route} from '@angular/router';

import { ViewUserComponent } from './view-user.component';
import {Injectable} from "@angular/core";
import {IUserDetails, UserDetails} from "app/shared/model/user-details.model";
import {UserDetailsService} from "app/entities/user-details/user-details.service";
import {Observable, of} from "rxjs";
import {map} from "rxjs/operators";
import {HttpResponse} from "@angular/common/http";
import {UserRouteAccessService} from "app/core/auth/user-route-access-service";

@Injectable({ providedIn: 'root' })
export class UserDetailsResolve implements Resolve<IUserDetails> {
  constructor(private service: UserDetailsService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUserDetails> {
    const id = route.params['id'];
    if (id) {
      return this.service.findById(id).pipe(map((userDetails: HttpResponse<UserDetails>) => userDetails.body));
    }
    return of(new UserDetails());
  }
}

export const VIEW_USER_ROUTE: Route =
  {
    path: 'view-user/:id/view',
    component: ViewUserComponent,
    resolve: {
      userDetails: UserDetailsResolve
    },
    data: {
      authorities: ['ROLE_ORG_NODAL','ROLE_ASP_NODAL'],
      pageTitle: 'view-user.title'
    },

    canActivate: [UserRouteAccessService]
  };
