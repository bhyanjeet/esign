import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';

import { VIEW_USER_ROUTE, ViewUserComponent } from './';

@NgModule({
    imports: [
      EsignharyanaSharedModule,
      RouterModule.forRoot([ VIEW_USER_ROUTE ], { useHash: true })
    ],
    declarations: [
      ViewUserComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppViewUserModule {}
