import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { PasswordService } from './password.service';
import * as $ from 'jquery';
import {EncrDecrService} from "app/shared/Services/EncrDecrService";

@Component({
  selector: 'jhi-password',
  templateUrl: './password.component.html'
})
export class PasswordComponent implements OnInit {
  doNotMatch: string;
  error: string;
  success: string;
  account$: Observable<Account>;
  passwordForm = this.fb.group({
    currentPassword: ['', [Validators.required]],
    newPassword: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
    confirmPassword: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]]
  });

  constructor(private passwordService: PasswordService, private accountService: AccountService, private fb: FormBuilder, private EncrDecr: EncrDecrService) {}

  ngOnInit() {
    this.account$ = this.accountService.identity();
  }

  changePassword() {
    const newPassword = this.passwordForm.get(['newPassword']).value;
    if (newPassword !== this.passwordForm.get(['confirmPassword']).value) {
      this.error = null;
      this.success = null;
      this.doNotMatch = 'ERROR';
    } else {
      this.doNotMatch = null;
      const newEncrypted = this.EncrDecr.set('6D24456D6640616362676259646E4578', newPassword);
      const oldEncrypted = this.EncrDecr.set('6D24456D6640616362676259646E4578', this.passwordForm.get(['currentPassword']).value);
      this.passwordService.save(newEncrypted, oldEncrypted).subscribe(
        () => {
          this.error = null;
          this.success = 'OK';
        },
        () => {
          this.success = null;
          this.error = 'ERROR';
        }
      );
    }
  }

  RemoveSavedPassword(id, idNew) {
    const inputValue = $('#' + id).val();
    const numChars = inputValue.length;
    let showText = '';
    for (let i = 0; i < numChars; i++) {
      showText += '&#8226;';
    }
    $('#' + idNew).html(showText);
  }
}
