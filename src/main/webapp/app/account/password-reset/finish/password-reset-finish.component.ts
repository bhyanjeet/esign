import {Component, OnInit, AfterViewInit, Renderer, ElementRef} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';

import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import {LoginModalService} from 'app/core/login/login-modal.service';
import {PasswordResetFinishService} from './password-reset-finish.service';
import {HttpErrorResponse} from "@angular/common/http";
import {EncrDecrService} from "app/shared/Services/EncrDecrService";
import * as $ from 'jquery';

@Component({
  selector: 'jhi-password-reset-finish',
  templateUrl: './password-reset-finish.component.html',
  styleUrls: ['password-reset-finish.scss']
})
export class PasswordResetFinishComponent implements OnInit, AfterViewInit {
  doNotMatch: string;
  error: string;
  keyMissing: boolean;
  success: string;
  modalRef: NgbModalRef;
  key: string;

  passwordForm = this.fb.group({
    newPassword: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
    confirmPassword: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]]
  });

  constructor(
    private passwordResetFinishService: PasswordResetFinishService,
    private loginModalService: LoginModalService,
    private route: ActivatedRoute,
    private elementRef: ElementRef,
    private renderer: Renderer,
    private fb: FormBuilder,
    private EncrDecr: EncrDecrService
  ) {
  }

  RemoveSavedPassword() {
    const inputValue = $('#pwd').val();
    const numChars = inputValue.length;
    let showText = '';
    for (let i = 0; i < numChars; i++) {
      showText += '&#8226;';
    }
    $('#password').html(showText);
  }

  RemoveSavedConfirmPassword() {
    const inputValue = $('#confirmpwd').val();
    const numChars = inputValue.length;
    let showText = '';
    for (let i = 0; i < numChars; i++) {
      showText += '&#8226;';
    }
    $('#confirmPassword').html(showText);
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.key = params['key'];
    });
    this.keyMissing = !this.key;
  }

  ngAfterViewInit() {
    if (this.elementRef.nativeElement.querySelector('#password') != null) {
      this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#password'), 'focus', []);
    }
  }

  finishReset() {
    this.doNotMatch = null;
    this.error = null;
    const password = this.passwordForm.get(['newPassword']).value;
    const confirmPassword = this.passwordForm.get(['confirmPassword']).value;
    if (password !== confirmPassword) {
      this.doNotMatch = 'ERROR';
    } else {
      const newEncrypted = this.EncrDecr.set('6D24456D6640616362676259646E4578', password);
      this.passwordResetFinishService.save({key: this.key, newPassword: newEncrypted}).subscribe(
        () => {
          this.success = 'OK';
        },
        response => {
          this.processError(response);
          // this.success = null;
          // this.error = 'ERROR';
        }
      );
    }
  }

  private processError(response: HttpErrorResponse) {
    alert(response.error.title);
    this.success = null;
    this.error = 'ERROR';
  }


  login() {
    this.modalRef = this.loginModalService.open();
  }
}
