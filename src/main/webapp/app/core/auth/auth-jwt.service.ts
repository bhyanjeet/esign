import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

import { SERVER_API_URL } from 'app/app.constants';
import {Router} from "@angular/router";

@Injectable({ providedIn: 'root' })
export class AuthServerProvider {
  constructor(private http: HttpClient, private $localStorage: LocalStorageService, private $sessionStorage: SessionStorageService, private router: Router) {}

  getToken() {
    return this.$localStorage.retrieve('authenticationToken') || this.$sessionStorage.retrieve('authenticationToken');
  }

  login(credentials): Observable<any> {
    const data = {
      username: credentials.username,
      password: credentials.password,
      rememberMe: credentials.rememberMe
    };

    function authenticateSuccess(resp) {
      const bearerToken = resp.headers.get('Authorization');
      if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
        const jwt = bearerToken.slice(7, bearerToken.length);
        this.storeAuthenticationToken(jwt, credentials.rememberMe);
        return jwt;
      }
    }

    return this.http.post(SERVER_API_URL + 'api/authenticate', data, { observe: 'response' }).pipe(map(authenticateSuccess.bind(this)));
  }

  storeAuthenticationToken(jwt, rememberMe) {
    if (rememberMe) {
      this.$localStorage.store('authenticationToken', jwt);
    } else {
      this.$sessionStorage.store('authenticationToken', jwt);
    }
  }

  // logout(): Observable<any> {
  //   return new Observable(observer => {
  //     this.$localStorage.clear('authenticationToken');
  //     this.$sessionStorage.clear('authenticationToken');
  //     observer.complete();
  //   });
  // }
  logoutToken() {
    return new Promise(resolve => {
      if (
        sessionStorage.getItem('jhi-authenticationtoken') != null ||
        localStorage.getItem('jhi-authenticationtoken') != null
      ) {
        this.http.post(SERVER_API_URL + 'api/logout', {}, { observe: 'response' }).subscribe(
          () => {
            this.clearToken();
            resolve(true);
          },
          error => {
            this.clearToken();
            resolve(true);
          }
        );
      } else {
        this.clearToken();
        resolve(true);
      }
    });
  }
  clearToken() {
    sessionStorage.clear();
    localStorage.clear();
    localStorage.setItem('jhi-authenticationtoken', '');
    sessionStorage.setItem('jhi-authenticationtoken', '');
    this.$sessionStorage.clear('authenticationtoken');
    this.$sessionStorage.clear('jhi-authenticationtoken');
    // this.router.navigate(['']).then(() => {});
    window.location.href = '/';
  }
}
