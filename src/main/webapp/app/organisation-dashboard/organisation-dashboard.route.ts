import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { OrganisationDashboardComponent } from './organisation-dashboard.component';

export const ORGANISATION_DASHBOARD_ROUTE: Route = {
  path: 'organisation-dashboard',
  component: OrganisationDashboardComponent,
  data: {
    authorities: ['ROLE_ORG_NODAL','ROLE_ADMIN','ROLE_ASP_NODAL'],
    pageTitle: 'organisation-dashboard.title'
  },
  canActivate: [UserRouteAccessService]
};
