import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';

import { ORGANISATION_UPDATE_ROUTE, OrganisationUpdateComponent } from './';

@NgModule({
    imports: [
      EsignharyanaSharedModule,
      RouterModule.forRoot([ ORGANISATION_UPDATE_ROUTE ], { useHash: true })
    ],
    declarations: [
      OrganisationUpdateComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppOrganisationUpdateModule {}
