import {Component, OnInit} from '@angular/core';
import {IOrganisationMaster} from "app/shared/model/organisation-master.model";
import {OrganisationMasterService} from "app/entities/organisation-master/organisation-master.service";
import {AccountService} from "app/core/auth/account.service";
import {User} from "app/core/user/user.model";
import {HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";


@Component({
  selector: 'jhi-organisation-dashboard',
  templateUrl: './organisation-dashboard.component.html',
  styleUrls: [
    'organisation-dashboard.component.scss'
  ]
})
export class OrganisationDashboardComponent implements OnInit {
  organisationmasters: IOrganisationMaster[];
  currentAccount: User;

  message: string;
  organisation: {};
  userId: number;
  organisationRecord: any;
  currentSearch: string;
  routeData: any;
  page: any;
  previousPage: any;
  reverse: boolean | boolean;
  predicate: any;

  constructor(
    private organisationMasterService: OrganisationMasterService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    private accountService: AccountService
  ) {
    this.routeData = this.activatedRoute.data.subscribe(data => {
      //this.page = data.pagingParams.page;
      // this.previousPage = data.pagingParams.page;
      // this.reverse = data.pagingParams.ascending;
      // this.predicate = data.pagingParams.predicate;
    });

    this.message = 'OrganisationDashboardComponent message';
  }

  ngOnInit() {
    this.accountService.identity().subscribe(account => {
      this.currentAccount= account;

      if (this.accountService.hasAnyAuthority(['ROLE_ADMIN','ROLE_ASP_NODAL'])) {
        this.organisationMasterService.query().subscribe((res: HttpResponse<IOrganisationMaster[]>) => {
          this.organisationmasters = res.body;
        });
      } else if(this.accountService.hasAnyAuthority('ROLE_ORG_NODAL')) {
        this.organisationMasterService.getAllRecordByOrgIdCode(this.currentAccount.login).subscribe((response: HttpResponse<IOrganisationMaster[]>) => {
          this.organisationmasters = response.body;
        });
      }
    });
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrganisationMaster>>) {
    //result.subscribe((res: HttpResponse<any>) => this.onSaveSuccess(res.body), () => this.onSaveError());
  }

}
