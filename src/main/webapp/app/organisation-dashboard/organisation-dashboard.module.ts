import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';

import { ORGANISATION_DASHBOARD_ROUTE, OrganisationDashboardComponent } from './';

@NgModule({
    imports: [
      EsignharyanaSharedModule,
      RouterModule.forRoot([ ORGANISATION_DASHBOARD_ROUTE ], { useHash: true })
    ],
    declarations: [
      OrganisationDashboardComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppOrganisationDashboardModule {}
