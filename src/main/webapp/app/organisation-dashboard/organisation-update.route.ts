import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { OrganisationUpdateComponent } from './organisation-update.component';
import {OrganisationMasterResolve} from "app/entities/organisation-master/organisation-master.route";

export const ORGANISATION_UPDATE_ROUTE: Route = {
  path: 'organisation-update/:id/edit',
  component: OrganisationUpdateComponent,
  resolve: {
    organisationMaster: OrganisationMasterResolve
  },
  data: {
    authorities: [],
    pageTitle: 'organisation-update.title'
  },
  canActivate: [UserRouteAccessService]
};
