import {Component, OnInit} from '@angular/core';
import {OrganisationMasterService} from "app/entities/organisation-master/organisation-master.service";
import {ActivatedRoute} from "@angular/router";
import {HttpResponse} from "@angular/common/http";
import {IOrganisationMaster, OrganisationMaster} from "app/shared/model/organisation-master.model";
import {FormBuilder, Validators} from "@angular/forms";
import {OrganisationTypeMasterService} from "app/entities/organisation-type-master/organisation-type-master.service";
import {StateMasterService} from "app/entities/state-master/state-master.service";
import {DistrictMasterService} from "app/entities/district-master/district-master.service";
import {SubDistrictMasterService} from "app/entities/sub-district-master/sub-district-master.service";
import {IOrganisationTypeMaster} from "app/shared/model/organisation-type-master.model";
import {IStateMaster} from "app/shared/model/state-master.model";
import {IDistrictMaster} from "app/shared/model/district-master.model";
import {ISubDistrictMaster} from "app/shared/model/sub-district-master.model";
import {Observable} from "rxjs";
import {JhiAlertService} from "ng-jhipster";
import {IOrganisationDocument} from "app/shared/model/organisation-document.model";
import {OrganisationDocumentService} from "app/entities/organisation-document/organisation-document.service";
import {MinioUploadService} from "app/shared/Services/minioUpload.service";

@Component({
  selector: 'jhi-organisation-update',
  templateUrl: './organisation-update.component.html',
  styleUrls: [
    'organisation-update.component.scss'
  ]
})
export class OrganisationUpdateComponent implements OnInit {
  isSaving: boolean;
  message: string;
  organisationId: any;
  organisationMaster: IOrganisationMaster;
  organisationtypemasters: IOrganisationTypeMaster[];
  statemasters: IStateMaster[];
  districtmasters: IDistrictMaster[];
  subdistrictmasters: ISubDistrictMaster[];
  docs: any;
  docss: IOrganisationDocument[];
  arrayFile: any[] = [];
  attachmentFiles: any;
  organisationNodalOfficerPhoneMobile: any;
  otp: any;

  editForm = this.fb.group({
    id: [],
    organisationName: [null, [Validators.required]],
    organisationAbbreviation: [null, [Validators.required]],
    organisationCorrespondenceEmail: [null, [Validators.required]],
    organisationCorrespondencePhone1: [null, [Validators.required]],
    organisationCorrespondencePhone2: [],
    organisationCorrespondenceAddress: [],
    organisationWebsite: [],
    organisationNodalOfficerName: [],
    organisationNodalOfficerPhoneMobile: [],
    organisationNodalOfficerPhoneLandline: [],
    nodalOfficerOfficialEmail: [],
    organisationNodalOfficerAlternativeEmail: [],
    createdBy: [],
    createdOn: [],
    lastUpdatedBy: [],
    lastUpdatedOn: [],
    verifiedBy: [],
    verifiedOn: [],
    remarks: [],
    userId: [],
    stage: [],
    status: [],
    organisationTypeMasterId: [],
    stateMasterId: [],
    districtMasterId: [],
    subDistrictMasterId: []
  });

  constructor(
    protected organisationMasterService: OrganisationMasterService,
    protected organisationTypeMasterService: OrganisationTypeMasterService,
    protected organisationDocumentService: OrganisationDocumentService,
    protected stateMasterService: StateMasterService,
    protected districtMasterService: DistrictMasterService,
    protected subDistrictMasterService: SubDistrictMasterService,
    protected activatedRoute: ActivatedRoute,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    protected jhiAlertService: JhiAlertService,
    protected minioUploadService: MinioUploadService,
  ) {
    this.message = 'OrganisationUpdateComponent message';
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ organisationMaster }) => {
      if(organisationMaster.id) {
        this.onChange(organisationMaster.organisationTypeMasterId);
      }
      this.updateForm(organisationMaster);
      this.organisationMaster = organisationMaster;
    });

    this.organisationTypeMasterService.query().subscribe(data => {
      this.organisationtypemasters = data.body;
    });
    this.stateMasterService.query().subscribe(data => {
      this.statemasters = data.body;
    });
    this.districtMasterService.query().subscribe(data => {
      this.districtmasters = data.body;
    });
    this.subDistrictMasterService.query().subscribe(data => {
      this.subdistrictmasters = data.body;
    });
  }

  onChange(orgTypeId) {
    // alert("dd");
    this.docs = orgTypeId;
   // this.organisationDocumentService.findDocsByOganisationTypeId(orgTypeId).subscribe((res1: HttpResponse<IOrganisationDocument[]>) => {
    this.organisationDocumentService.query().subscribe((res1: HttpResponse<IOrganisationDocument[]>) => {
      this.docss = res1.body;
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const organisationMaster = this.createFromForm();
    this.subscribeToSaveResponse(this.organisationMasterService.update(organisationMaster));
  }

  private createFromForm(): IOrganisationMaster {
    return {
      ...new OrganisationMaster(),
      id: this.editForm.get(['id']).value,
      organisationName: this.editForm.get(['organisationName']).value,
      organisationAbbreviation: this.editForm.get(['organisationAbbreviation']).value,
      organisationCorrespondenceEmail: this.editForm.get(['organisationCorrespondenceEmail']).value,
      organisationCorrespondencePhone1: this.editForm.get(['organisationCorrespondencePhone1']).value,
      organisationCorrespondencePhone2: this.editForm.get(['organisationCorrespondencePhone2']).value,
      organisationCorrespondenceAddress: this.editForm.get(['organisationCorrespondenceAddress']).value,
      organisationWebsite: this.editForm.get(['organisationWebsite']).value,
      organisationNodalOfficerName: this.editForm.get(['organisationNodalOfficerName']).value,
      organisationNodalOfficerPhoneMobile: this.editForm.get(['organisationNodalOfficerPhoneMobile']).value,
      organisationNodalOfficerPhoneLandline: this.editForm.get(['organisationNodalOfficerPhoneLandline']).value,
      nodalOfficerOfficialEmail: this.editForm.get(['nodalOfficerOfficialEmail']).value,
      organisationNodalOfficerAlternativeEmail: this.editForm.get(['organisationNodalOfficerAlternativeEmail']).value,
      createdBy: this.editForm.get(['createdBy']).value,
      createdOn: this.editForm.get(['createdOn']).value,
      lastUpdatedBy: this.editForm.get(['lastUpdatedBy']).value,
      lastUpdatedOn: this.editForm.get(['lastUpdatedOn']).value,
      verifiedBy: this.editForm.get(['verifiedBy']).value,
      verifiedOn: this.editForm.get(['verifiedOn']).value,
      remarks: this.editForm.get(['remarks']).value,
      userId: this.editForm.get(['userId']).value,
      stage: this.editForm.get(['stage']).value,
      status: this.editForm.get(['status']).value,
      organisationTypeMasterId: this.editForm.get(['organisationTypeMasterId']).value,
      stateMasterId: this.editForm.get(['stateMasterId']).value,
      districtMasterId: this.editForm.get(['districtMasterId']).value,
      subDistrictMasterId: this.editForm.get(['subDistrictMasterId']).value
    };
  }


  updateForm(organisationMaster: IOrganisationMaster) {
    this.editForm.patchValue({
      id: organisationMaster.id,
      organisationName: organisationMaster.organisationName,
      organisationAbbreviation: organisationMaster.organisationAbbreviation,
      organisationCorrespondenceEmail: organisationMaster.organisationCorrespondenceEmail,
      organisationCorrespondencePhone1: organisationMaster.organisationCorrespondencePhone1,
      organisationCorrespondencePhone2: organisationMaster.organisationCorrespondencePhone2,
      organisationCorrespondenceAddress: organisationMaster.organisationCorrespondenceAddress,
      organisationWebsite: organisationMaster.organisationWebsite,
      organisationNodalOfficerName: organisationMaster.organisationNodalOfficerName,
      organisationNodalOfficerPhoneMobile: organisationMaster.organisationNodalOfficerPhoneMobile,
      organisationNodalOfficerPhoneLandline: organisationMaster.organisationNodalOfficerPhoneLandline,
      nodalOfficerOfficialEmail: organisationMaster.nodalOfficerOfficialEmail,
      organisationNodalOfficerAlternativeEmail: organisationMaster.organisationNodalOfficerAlternativeEmail,
      createdBy: organisationMaster.createdBy,
      createdOn: organisationMaster.createdOn,
      lastUpdatedBy: organisationMaster.lastUpdatedBy,
      lastUpdatedOn: organisationMaster.lastUpdatedOn,
      verifiedBy: organisationMaster.verifiedBy,
      verifiedOn: organisationMaster.verifiedOn,
      remarks: organisationMaster.remarks,
      userId: organisationMaster.userId,
      stage: organisationMaster.stage,
      status: organisationMaster.status,
      organisationTypeMasterId: organisationMaster.organisationTypeMasterId,
      stateMasterId: organisationMaster.stateMasterId,
      districtMasterId: organisationMaster.districtMasterId,
      subDistrictMasterId: organisationMaster.subDistrictMasterId
    });
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrganisationMaster>>) {
    result.subscribe((res: HttpResponse<any>) => this.onSaveSuccess(res.body), () => this.onSaveError());
  }

  protected onSaveSuccess(result) {
    if(result) {
      this.saveMultipleFile(result);
      this.previousState();
      alert("Organisation Updated Successfully");
    }
    this.isSaving = false;
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  uploadFile(filesData, documentId) {
    this.arrayFile.push({filesData, documentId});
  }

  saveMultipleFile(result) {
    if (this.arrayFile.length > 0) {
      this.minioUploadService.createMultipleFile(this.arrayFile, result.id).subscribe((res: HttpResponse<any>) => {
        this.attachmentFiles = res.body;
    });

  }
}
}

