import { Component, OnInit } from '@angular/core';
import { OrganisationMasterService } from '../entities/organisation-master/organisation-master.service';
import { IOrganisationMaster } from '../shared/model/organisation-master.model';
import { ITEMS_PER_PAGE } from '../shared/constants/pagination.constants';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { JhiParseLinks } from 'ng-jhipster';
import { AccountService } from '../core/auth/account.service';
import { User } from '../core/user/user.model';
import {Observable} from "rxjs";
import {Router} from "@angular/router";
import {VerifyOrgService} from "app/entities/organisation-master/verify-org-service";
import {IUserDetails} from "app/shared/model/user-details.model";

@Component({
  selector: 'jhi-asp-dashboard',
  templateUrl: './asp-dashboard.component.html',
  styleUrls: ['asp-dashboard.scss']
})
export class AspDashboardComponent implements OnInit {
  page: any;
  itemsPerPage: any;
  predicate: any;
  reverse: any;
  links: any;
  totalItems: any;
  organisationMasters: IOrganisationMaster[];
  currentAccount: User;
  status: any;
  pendingLength: IOrganisationMaster[];
  approvedLength: IOrganisationMaster[];
  rejectedLength: IOrganisationMaster[];
  organisationId: any;
  remark: any;
  remarks: [];
  isSaving: boolean;
  organisationMaster: any;
  createdOn: any;

  constructor(
    protected organisationMasterService: OrganisationMasterService,
    protected parseLinks: JhiParseLinks,
    protected accountService: AccountService,
    protected verifyOrgService: VerifyOrgService,
    private router: Router,
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.organisationMasters = [];
    this.status = 'Pending';
  }

  ngOnInit() {
    this.accountService.identity().subscribe(account => {
      this.currentAccount = account;
    });
    this.organisationMasterService
      .getAllRecordByStatus('Pending')
      .subscribe((res: HttpResponse<IOrganisationMaster[]>) => {
        this.organisationMasters = res.body;
      });
    this.getLenghtByStatus();

    return;
  }
  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }
  protected paginateOrganisationMasters(data: IOrganisationMaster[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.organisationMasters = data;
  }

  getOrganisationId(organisationMaster){
    this.organisationMaster = organisationMaster;
  }

   rejectPopup(organisationMaster){
    this.organisationMaster = organisationMaster;
  }

  approveOrganisation(organisationMaster){
    organisationMaster.verifiedBy = this.currentAccount.login;
    organisationMaster.createdOn = organisationMaster.createdOn.toLocaleString();
    organisationMaster.remark = this.remarks;
    organisationMaster.status = 'Approved';
    this.subscribeToSaveResponse(this.organisationMasterService.update(organisationMaster));
  }

   rejectOrganisation(organisationMaster){
     organisationMaster.verifiedBy = this.currentAccount.login;
    organisationMaster.remark = this.remarks;
    organisationMaster.status = 'Rejected';
    this.subscribeToSaveResponse(this.organisationMasterService.update(organisationMaster));
  }

  getLenghtByStatus(){
      this.organisationMasterService
        .getAllRecordByStatus('Pending')
        .subscribe((res1: HttpResponse<IOrganisationMaster[]>) => {
          this.pendingLength = res1.body;
        });
    this.organisationMasterService
      .getAllRecordByStatus('Approved')
      .subscribe((res1: HttpResponse<IOrganisationMaster[]>) => {
        this.approvedLength = res1.body;
      });

    this.organisationMasterService
      .getAllRecordByStatus('Rejected')
      .subscribe((res1: HttpResponse<IOrganisationMaster[]>) => {
        this.rejectedLength = res1.body;
      });
  }

  getRecordBystatus(status) {
    const verifyBy = this.currentAccount.login;
    this.status = status;
    if (status === 'Pending') {
      this.organisationMasterService
        .getAllRecordByStatus('Pending')
        .subscribe((res: HttpResponse<IOrganisationMaster[]>) => {
          this.organisationMasters = res.body;
        });
    } else if (status === 'Approved') {
      this.organisationMasterService
        .getAllRecordByStatusAndVerifyBy(status, verifyBy)
        .subscribe((res: HttpResponse<IOrganisationMaster[]>) => {
          this.organisationMasters = res.body;
        });

    } else if (status === 'Rejected') {
      this.organisationMasterService
        .getAllRecordByStatusAndVerifyBy(status, verifyBy)
        .subscribe((res: HttpResponse<IOrganisationMaster[]>) => {
          this.organisationMasters = res.body;
        });
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrganisationMaster>>) {
    result.subscribe((res: HttpResponse<any>) => this.onSaveSuccess(res.body), () => this.onSaveError());
  }

  protected onSaveSuccess(result) {
    this.isSaving = false;
    alert("Organisation Confirmation Request Submitted Successfully");
    window.location.reload();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
