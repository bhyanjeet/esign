import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ASP_DASHBOARD_ROUTE, AspDashboardComponent } from './';
import { EsignharyanaSharedModule } from '../shared/shared.module';

@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forRoot([ASP_DASHBOARD_ROUTE], { useHash: true })],
  declarations: [AspDashboardComponent],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppAspDashboardModule {}
