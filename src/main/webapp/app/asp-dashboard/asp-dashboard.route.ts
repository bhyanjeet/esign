import { Route } from '@angular/router';

import { AspDashboardComponent } from './';
import { UserRouteAccessService } from '../core/auth/user-route-access-service';

export const ASP_DASHBOARD_ROUTE: Route = {
  path: 'asp-dashboard',
  component: AspDashboardComponent,
  data: {
    authorities: ['ROLE_ASP_NODAL'],
    pageTitle: 'asp-dashboard.title'
  },
  canActivate: [UserRouteAccessService]
};
