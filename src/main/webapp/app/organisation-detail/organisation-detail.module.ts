import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

// import { EsignharyanaSharedModule } from 'app/shared';

import { ORGANISATION_DETAIL_ROUTE, OrganisationDetailComponent } from './';
import {EsignharyanaSharedModule} from "app/shared/shared.module";

@NgModule({
    imports: [
      EsignharyanaSharedModule,
      RouterModule.forRoot([ ORGANISATION_DETAIL_ROUTE ], { useHash: true })
    ],
    declarations: [
      OrganisationDetailComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppOrganisationDetailModule {}
