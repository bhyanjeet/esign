import {ActivatedRouteSnapshot, Resolve, Route, Routes} from '@angular/router';

//import { UserRouteAccessService } from 'app/core';
import { OrganisationDetailComponent } from './organisation-detail.component';
import {OrganisationMasterDetailComponent} from "app/entities/organisation-master/organisation-master-detail.component";
import {UserRouteAccessService} from "app/core/auth/user-route-access-service";
import {Injectable} from "@angular/core";
import {IOrganisationMaster, OrganisationMaster} from "app/shared/model/organisation-master.model";
import {OrganisationMasterService} from "app/entities/organisation-master/organisation-master.service";
import {Observable, of} from "rxjs";
import {map} from "rxjs/operators";
import {HttpResponse} from "@angular/common/http";

@Injectable({ providedIn: 'root' })
export class OrganisationMasterResolve implements Resolve<IOrganisationMaster> {
  constructor(private service: OrganisationMasterService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOrganisationMaster> {
    const id = route.params['id'];
    if (id) {
      return this.service.findById(id).pipe(map((organisationMaster: HttpResponse<OrganisationMaster>) => organisationMaster.body));
    }
    return of(new OrganisationMaster());
  }
}

export const ORGANISATION_DETAIL_ROUTE: Route =
  {
  path: 'organisation-detail/:id/view',
  component: OrganisationDetailComponent,
    resolve: {
      organisationMaster: OrganisationMasterResolve
    },
    data: {
    authorities: ['ROLE_ASP_NODAL','ROLE_ORG_NODAL'],
    pageTitle: 'organisation-detail.title'
  },

  canActivate: [UserRouteAccessService]
};


