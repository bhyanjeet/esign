import {Component, NgModule, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IOrganisationMaster } from 'app/shared/model/organisation-master.model';
import { CommonModule } from '@angular/common';
import {IOrganisationTypeMaster} from "app/shared/model/organisation-type-master.model";
import {OrganisationTypeMasterService} from "app/entities/organisation-type-master/organisation-type-master.service";
import {HttpResponse} from "@angular/common/http";
import {IOrganisationDocument} from "app/shared/model/organisation-document.model";
import {IStateMaster, StateMaster} from "app/shared/model/state-master.model";
import {StateMasterService} from "app/entities/state-master/state-master.service";
import {nextMonthDisabled} from "@ng-bootstrap/ng-bootstrap/datepicker/datepicker-tools";
import {IDistrictMaster} from "app/shared/model/district-master.model";
import {DistrictMasterService} from "app/entities/district-master/district-master.service";
import {SubDistrictMasterService} from "app/entities/sub-district-master/sub-district-master.service";
import {ISubDistrictMaster} from "app/shared/model/sub-district-master.model";
import {OrganisationAttachmentService} from "../entities/organisation-attachment/organisation-attachment.service";
import {IOrganisationAttachment} from "../shared/model/organisation-attachment.model";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {DomSanitizer} from "@angular/platform-browser";
import {MinioUploadService} from "../shared/Services/minioUpload.service";
import {AccountService} from "app/core/auth/account.service";
import {OrganisationMasterService} from "app/entities/organisation-master/organisation-master.service";



@Component({
  selector: 'jhi-organisation-detail',
  templateUrl: './organisation-detail.component.html',
  styleUrls: [
    'organisation-detail.component.scss'
  ]
})
export class OrganisationDetailComponent implements OnInit {
  organisationMaster: IOrganisationMaster;
  organisationTypeMaster: IOrganisationTypeMaster = {};
  stateMaster: IStateMaster;
  districtMaster: IDistrictMaster;
  subDistrictMaster: ISubDistrictMaster;
  private editForm: any;
  url: any;
  organisationAttachments: any;
  currentUser: any;
  checkvalue: boolean;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected  organisationTypeMasterService: OrganisationTypeMasterService,
    protected  organisationMasterService: OrganisationMasterService,
    protected  stateMasterService: StateMasterService,
    protected districtMasterService: DistrictMasterService,
    protected subDistrictMasterService: SubDistrictMasterService,
    protected organisationAttachmentService: OrganisationAttachmentService,
    protected minioUploadService: MinioUploadService,
    protected sanitizer: DomSanitizer,
    protected modalService: NgbModal,
    protected accountService: AccountService
) {}

  ngOnInit() {
    this.accountService.identity().subscribe(data => {
      this.currentUser = data;

      this.activatedRoute.data.subscribe(({ organisationMaster }) => {
        this.organisationMaster = organisationMaster;



        if (this.organisationMaster.organisationIdCode!==null){
          this.checkvalue= true;
        }else {
          this.checkvalue =false;
        }




        this.organisationMasterService.find(this.organisationMaster.id).subscribe(res => {
          this.organisationTypeMaster = res.body;
        });

        this.stateMasterService.find(this.organisationMaster.stateMasterId).subscribe(res1 =>{
          this.stateMaster = res1.body;
        });

        this.districtMasterService.find(this.organisationMaster.districtMasterId).subscribe(res2 =>{
          this.districtMaster = res2.body;
        });

        this.subDistrictMasterService.find(this.organisationMaster.subDistrictMasterId).subscribe(res3 =>{
          this.subDistrictMaster = res3.body;
        });
        this.organisationAttachmentService.getAllAttachmentsByOrganisationMaster(this.organisationMaster.id).subscribe((res4: HttpResponse<IOrganisationAttachment[]>) => {
          this.organisationAttachments = res4.body;
        });

      });
    });
  }

  getMinioImage(fileName, content) {
    console.log(fileName);
    const fileExtension = fileName.split('.');
    const extensionFirst = fileExtension[1];
    const extension = fileExtension[2];
    if (extensionFirst === 'pdf' || extension === 'pdf') {
      this.minioUploadService.getMinioFileUrl(fileName).subscribe((result: HttpResponse<any>) => {
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl('data:application/pdf;base64,' + result.body);
        this.openModal(content);
      });
    } else {
      this.minioUploadService.getMinioFileUrl(fileName).subscribe((result: HttpResponse<any>) => {
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' + result.body);
        this.openModal(content);
      });
    }
  }

  openModal(content) {
    this.modalService.open(content, {size: 'xl', backdrop: 'static'});
  }

  goBack(){
    window.history.back();
  }

  previousState() {
    window.history.back();
  }
}
