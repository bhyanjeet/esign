import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EsignharyanaSharedModule } from 'app/shared/shared.module';

import { ORGANISATION_ROUTE, OrganisationComponent } from './';
@NgModule({
  imports: [EsignharyanaSharedModule, RouterModule.forChild(ORGANISATION_ROUTE)],
  declarations: [OrganisationComponent,

  ],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppOrganisationModule {}
