import {ActivatedRouteSnapshot, Resolve, Routes} from '@angular/router';

import {UserRouteAccessService} from 'app/core/auth/user-route-access-service';
import {OrganisationComponent} from './organisation.component';
import {OrganisationMasterService} from "../entities/organisation-master/organisation-master.service";
import {Injectable} from "@angular/core";
import {IOrganisationMaster, OrganisationMaster} from "../shared/model/organisation-master.model";
import {Observable, of} from "rxjs/index";
import {map} from "rxjs/internal/operators";
import {HttpResponse} from "@angular/common/http";

@Injectable({providedIn: 'root'})
export class OrganisationMasterResolve implements Resolve<IOrganisationMaster> {
  constructor(private service: OrganisationMasterService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<IOrganisationMaster> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((organisationMaster: HttpResponse<OrganisationMaster>) => organisationMaster.body));
    }
    return of(new OrganisationMaster());
  }
}

export const ORGANISATION_ROUTE: Routes = [
  {
    path: 'organisation',
    component: OrganisationComponent,
    data: {
      authorities: ['ROLE_ASP_NODAL'],
      pageTitle: 'organisation.title'
    },
    resolve: {
      organisationMaster: OrganisationMasterResolve
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'organisation/:id/edit',
    component: OrganisationComponent,
    data: {
      authorities: ['ROLE_ASP_NODAL','ROLE_ORG_NODAL'],
      pageTitle: 'organisation.title'
    },
    resolve: {
      organisationMaster: OrganisationMasterResolve
    },
    canActivate: [UserRouteAccessService]
  },

];

