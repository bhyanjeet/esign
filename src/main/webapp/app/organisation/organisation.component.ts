import {Component, OnInit} from '@angular/core';
import {OrganisationTypeMasterService} from 'app/entities/organisation-type-master/organisation-type-master.service';
import {FormBuilder, Validators} from '@angular/forms';
import {StateMasterService} from 'app/entities/state-master/state-master.service';
import {DistrictMasterService} from 'app/entities/district-master/district-master.service';
import {SubDistrictMasterService} from 'app/entities/sub-district-master/sub-district-master.service';
import {IStateMaster} from 'app/shared/model/state-master.model';
import {IDistrictMaster} from 'app/shared/model/district-master.model';
import {ISubDistrictMaster} from 'app/shared/model/sub-district-master.model';
import {HttpResponse} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {JhiAlertService} from 'ng-jhipster';
import {IOrganisationMaster, OrganisationMaster} from 'app/shared/model/organisation-master.model';
import {Observable} from 'rxjs';
import {OrganisationMasterService} from 'app/entities/organisation-master/organisation-master.service';
import {OrganisationDocumentService} from 'app/entities/organisation-document/organisation-document.service';
import {IOrganisationDocument} from 'app/shared/model/organisation-document.model';
import {AccountService} from '../core/auth/account.service';
import {User} from '../core/user/user.model';
import {MinioUploadService} from '../shared/Services/minioUpload.service';
import {OrganisationAttachmentService} from '../entities/organisation-attachment/organisation-attachment.service';
import {OtpService} from "app/entities/otp/otp.service";
import {IOtp} from "app/shared/model/otp.model";
import {IDepartment} from "app/shared/model/department.model";
import {DepartmentService} from "app/entities/department/department.service";

@Component({
  selector: 'jhi-organisation',
  templateUrl: './organisation.component.html',
  styleUrls: ['organisation.component.scss']
})
export class OrganisationComponent implements OnInit {
  departments: IDepartment[];
  statemasters: IStateMaster[];
  districtmasters: IDistrictMaster[];
  subdistrictmasters: ISubDistrictMaster[];
  isSaving: boolean;
  docs: any;
  docss: any;
  currentAccount: User;
  arrayFile: any[] = [];
  allFileArray: Array<any> = [];
  pushFile: Array<any> = [];
  attachmentFiles: any;
  verifyotp: any;
  otp: IOtp[];
  mobileNumber: any;
  organisationNodalOfficerPhoneMobile: any;
  orgnisationId: any;
  org: any;
  orgnisationCreatedBy: any;
  organisationIdCode: any;
  deptId: any;
  deptName: any;
  errorEmailExists: string;
  errorPhoneNoExists: string;
  error: string;
  errorOrganisationNameExists: string;
  errorNodalPhoneNoExists: string;
  departmentId: any = null;
  stateMasterId: any = null;
  districtMasterId: any = null;
  subDistrictMasterId: any = null;

  editForm = this.fb.group({
    id: [],
    organisationName: [],
    organisationAbbreviation: [],
    organisationCorrespondenceEmail: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    organisationCorrespondencePhone1: ['', Validators.required],
    organisationCorrespondencePhone2: [],
    organisationCorrespondenceAddress: [],
    organisationWebsite: [],
    organisationNodalOfficerName: [],
    organisationNodalOfficerPhoneMobile: ['', Validators.required],
    organisationNodalOfficerPhoneLandline: [],
    nodalOfficerOfficialEmail: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    organisationNodalOfficerAlternativeEmail: [],
    createdBy: [],
    createdOn: [],
    lastUpdatedBy: [],
    lastUpdatedOn: [],
    verifiedBy: [],
    verifiedOn: [],
    remarks: [],
    userId: [],
    stage: [],
    status: [],
    organisationIdCode: [],
    organisationTypeMasterId: [],
    stateMasterId: [],
    districtMasterId: [],
    subDistrictMasterId: [],
    organisationEmployeeId: [],
    dateOfBirth: [],
    gender: [],
    pan: [],
    aadhaar: [],
    orgnisationEmpolyeeCardAttchment: [],
    panAttachment: [],
    photographAttachment: [],
    aadhaarAttachment: [],
    departmentId: [],
  });
  panFileName: any;
  photograph: File;
  panCardAttcahment: File;
  employeeIdCardAAttachment: File;
  empFileName: any;
  imageName: any;
  attachment: any;
  errorFileExension: string;

  constructor(
    protected organisationMasterService: OrganisationMasterService,
    protected organisationTypeMasterService: OrganisationTypeMasterService,
    protected stateMasterService: StateMasterService,
    protected districtMasterService: DistrictMasterService,
    protected subDistrictMasterService: SubDistrictMasterService,
    protected activatedRoute: ActivatedRoute,
    protected jhiAlertService: JhiAlertService,
    protected organisationDocumentService: OrganisationDocumentService,
    protected accountService: AccountService,
    protected minioUploadService: MinioUploadService,
    protected organisationAttachmentService: OrganisationAttachmentService,
    protected departmentService: DepartmentService,
    protected otpService: OtpService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.departmentService.query().subscribe(data => {
      this.departments = data.body;
    });
    this.stateMasterService.query().subscribe(data => {
      this.statemasters = data.body;
    });
    this.districtMasterService.query().subscribe(data => {
      this.districtmasters = data.body;
    });
    this.subDistrictMasterService.query().subscribe(data => {
      this.subdistrictmasters = data.body;
    });
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(({organisationMaster}) => {
      this.updateForm(organisationMaster);
      this.orgnisationId = organisationMaster.id;
      this.orgnisationCreatedBy = organisationMaster.createdBy;
      this.organisationIdCode = organisationMaster.organisationIdCode;
    });

    this.accountService.identity().subscribe(account => {
      this.currentAccount = account;
    });

    this.departmentService
      .query()
      .subscribe(
        (res: HttpResponse<IDepartment[]>) => (this.departments = res.body)
      );
    this.stateMasterService
      .query()
      .subscribe(
        (res: HttpResponse<IStateMaster[]>) => (this.statemasters = res.body)
      );
    this.districtMasterService
      .query()
      .subscribe(
        (res: HttpResponse<IDistrictMaster[]>) => (this.districtmasters = res.body)
      );
    this.subDistrictMasterService
      .query()
      .subscribe(
        (res: HttpResponse<ISubDistrictMaster[]>) => (this.subdistrictmasters = res.body)
      );
  }


  onChange(orgTypeId) {
    this.docs = orgTypeId;
    // this.organisationDocumentService.findDocsByOganisationTypeId(orgTypeId).subscribe((res1: HttpResponse<IOrganisationDocument[]>) => {
    this.organisationDocumentService.query().subscribe((res1: HttpResponse<IOrganisationDocument[]>) => {
      this.docss = res1.body;
    });
  }

  previousState() {
    window.history.back();
  }

  // sendOTP(organisationNodalOfficerPhoneMobile){
  //   this.mobileNumber = this.organisationNodalOfficerPhoneMobile;
  //   this.otpService.sendOtpByMobile(organisationNodalOfficerPhoneMobile).subscribe((response) => {
  //     this.otp = response.body;
  //     console.log("otppppppppppp", this.otp);
  //   });
  //   }

  saveData() {
    const organisationMaster = this.createFromForm();
    organisationMaster.organisationAbbreviation = "HAR";
    organisationMaster.status = "Updated";
    organisationMaster.userId = this.currentAccount.id;
    organisationMaster.createdBy = this.orgnisationCreatedBy;
    organisationMaster.organisationIdCode = this.organisationIdCode;
    alert(this.panFileName);
    alert(this.imageName);
    alert(this.empFileName);
    if(this.panFileName ===""){
      alert("Pan attachement file not uploaded!");
      return;
    }
    if(this.imageName ===""){
      alert("User Photograph file not uploaded!");
      return;
    }
    if(this.empFileName ===""){
      alert("Employee Card Attachment file not uploaded!");
      return;
    }
    if (this.panFileName || this.imageName || this.empFileName) {
      const filesArray = [];
      if (this.panFileName) {
        filesArray.push({file: this.panCardAttcahment, type: 'pan'});
      }
      if (this.imageName) {
        filesArray.push({file: this.photograph, type: 'photograph'});
      }
      if (this.empFileName) {
        filesArray.push({file: this.employeeIdCardAAttachment, type: 'empidCard'});
      }
      this.minioUploadService.createMultipleFiles(filesArray).subscribe(res => {
        const file = res.body;
        organisationMaster.photographAttachment = file.photoGraphAttachmentName;
        organisationMaster.orgnisationEmpolyeeCardAttchment = file.empIdCardAttachmentName;
        organisationMaster.panAttachment = file.panAttachmentName;
        this.subscribeToSaveResponse(this.organisationMasterService.update(organisationMaster));
      },
        resError => {
        alert(resError.error.title);
        }
      );
    } else {
      this.subscribeToSaveResponse(this.organisationMasterService.update(organisationMaster));
    }
  }


  verifyOTP() {
    const organisationMaster = this.createFromForm();
    organisationMaster.organisationAbbreviation = 'HAR';
    organisationMaster.userId = this.currentAccount.id;
    this.deptId = organisationMaster.departmentId;
    this.departmentService.find(this.deptId).subscribe(res => {
      this.deptName = res.body;
      organisationMaster.organisationName = this.deptName.departmentName;
    });
    setTimeout(() => {
      if (this.panFileName || this.imageName || this.empFileName) {
        const filesArray = [];
        if (this.panFileName) {
          filesArray.push({file: this.panCardAttcahment, type: 'pan'});
        }
        if (this.imageName) {
          filesArray.push({file: this.photograph, type: 'photograph'});
        }
        if (this.empFileName) {
          filesArray.push({file: this.employeeIdCardAAttachment, type: 'empidCard'});
        }
        this.minioUploadService.createMultipleFiles(filesArray).subscribe(res => {
          const file = res.body;
          organisationMaster.photographAttachment = file.photoGraphAttachmentName;
          organisationMaster.orgnisationEmpolyeeCardAttchment = file.empIdCardAttachmentName;
          organisationMaster.panAttachment = file.panAttachmentName;
          this.subscribeToSaveResponse(this.organisationMasterService.create(organisationMaster));
        },
          resError => {
              alert(resError.error.title);
          }
        );
      } else {
        this.subscribeToSaveResponse(this.organisationMasterService.create(organisationMaster));
      }
    },1000);

  }


  //
  // verifyOTP(mobile,otp){
  //   this.verifyotp = otp;
  //   this.otpService.verifyOtpByMobile(mobile,otp).subscribe((resotp) => {
  //     if (resotp.body === false){
  //
  //       alert("You have Entered wrong OTP Please Verify again with valid OTP");
  //     } else {
  //       this.isSaving = true;
  //       const organisationMaster = this.createFromForm();
  //       organisationMaster.organisationAbbreviation = 'HAR';
  //       organisationMaster.userId = this.currentAccount.id;
  //       if (organisationMaster.id !== undefined) {
  //         this.subscribeToSaveResponse(this.organisationMasterService.update(organisationMaster));
  //       } else {
  //         this.subscribeToSaveResponse(this.organisationMasterService.create(organisationMaster));
  //       }
  //     }
  //   });
  // }

  // reSendOTP(organisationNodalOfficerPhoneMobile){
  //   this.sendOTP(organisationNodalOfficerPhoneMobile);
  // }

  // save() {
  //   this.isSaving = true;
  //   const organisationMaster = this.createFromForm();
  //   organisationMaster.organisationAbbreviation = 'HAR';
  //   organisationMaster.userId = this.currentAccount.id;
  //
  //   if (organisationMaster.id !== null) {
  //     this.subscribeToSaveResponse(this.organisationMasterService.update(organisationMaster));
  //   } else {
  //     this.subscribeToSaveResponse(this.organisationMasterService.create(organisationMaster));
  //   }
  // }


  uploadFile(fileData, fileType) {
    if (fileType === 'panAttachment') {
      this.panCardAttcahment = fileData.item(0);
      const panAttachmentName = fileData.item(0).name;
      const panAttachmentLength =  panAttachmentName.split(".");

      if (panAttachmentLength.length>2){
        alert("Invalid File Format. File should be like abc.jpg!");
        return;
      }
      const  panfileType = panAttachmentLength[1];

      if (panfileType === "jpg" || panfileType === "JPG"){
        this.panFileName = this.panCardAttcahment.name;
      }
      else {
        alert("Oops! Incorrect File Type. Please select only JPG/jpg file");
        return null;
      }


    } else if (fileType === 'orgnisationEmpolyeeCardAttchment') {
      this.employeeIdCardAAttachment = fileData.item(0);
      const panAttachmentName = fileData.item(0).name;

      const panAttachmentLength =  panAttachmentName.split(".");

      if (panAttachmentLength.length>2){
        alert("Invalid File Format. File should be like abc.jpg!");
        return null;
      }
      const  panfileType = panAttachmentLength[1];

      if (panfileType === "jpg" || panfileType === "JPG"){
        this.empFileName = this.employeeIdCardAAttachment.name;
      }
      else {
        alert("Oops! Incorrect File Type. Please select only JPG/jpg file");
        return null;
      }


    } else {
      this.photograph = fileData.item(0);
      const panAttachmentName = fileData.item(0).name;
      const panAttachmentLength =  panAttachmentName.split(".");

      if (panAttachmentLength.length>2){
        alert("Invalid File Format. File should be like abc.jpg!");
        return null;
      }
      const  panfileType = panAttachmentLength[1];

      if (panfileType === "jpg" || panfileType === "JPG"){
        this.imageName = this.photograph.name;
      }
      else {
        alert("Oops! Incorrect File Type. Please select only JPG/jpg file");
        return null;
      }

    }
  }

  private createFromForm(): IOrganisationMaster {
    return {
      ...new OrganisationMaster(),
      id: this.editForm.get(['id']).value,
      organisationName: this.editForm.get(['organisationName']).value,
      organisationCorrespondenceEmail: this.editForm.get(['organisationCorrespondenceEmail']).value,
      organisationCorrespondencePhone1: this.editForm.get(['organisationCorrespondencePhone1']).value,
      organisationCorrespondencePhone2: this.editForm.get(['organisationCorrespondencePhone2']).value,
      organisationCorrespondenceAddress: this.editForm.get(['organisationCorrespondenceAddress']).value,
      organisationWebsite: this.editForm.get(['organisationWebsite']).value,
      organisationNodalOfficerName: this.editForm.get(['organisationNodalOfficerName']).value,
      organisationNodalOfficerPhoneMobile: this.editForm.get(['organisationNodalOfficerPhoneMobile']).value,
      organisationNodalOfficerPhoneLandline: this.editForm.get(['organisationNodalOfficerPhoneLandline']).value,
      nodalOfficerOfficialEmail: this.editForm.get(['nodalOfficerOfficialEmail']).value,
      remarks: this.editForm.get(['remarks']).value,
      organisationTypeMasterId: this.editForm.get(['organisationTypeMasterId']).value,
      stateMasterId: this.editForm.get(['stateMasterId']).value,
      districtMasterId: this.editForm.get(['districtMasterId']).value,
      subDistrictMasterId: this.editForm.get(['subDistrictMasterId']).value,
      organisationEmployeeId: this.editForm.get(['organisationEmployeeId']).value,
      dateOfBirth: this.editForm.get(['dateOfBirth']).value,
      gender: this.editForm.get(['gender']).value,
      pan: this.editForm.get(['pan']).value,
      aadhaar: this.editForm.get(['aadhaar']).value,
      orgnisationEmpolyeeCardAttchment: this.editForm.get(['orgnisationEmpolyeeCardAttchment']).value,
      panAttachment: this.editForm.get(['panAttachment']).value,
      photographAttachment: this.editForm.get(['photographAttachment']).value,
      aadhaarAttachment: this.editForm.get(['aadhaarAttachment']).value,
      departmentId: this.editForm.get(['departmentId']).value
    };
  }

  updateForm(organisationMaster: IOrganisationMaster) {
    this.editForm.patchValue({
      id: organisationMaster.id,
      organisationName: organisationMaster.organisationName,
      organisationAbbreviation: organisationMaster.organisationAbbreviation,
      organisationCorrespondenceEmail: organisationMaster.organisationCorrespondenceEmail,
      organisationCorrespondencePhone1: organisationMaster.organisationCorrespondencePhone1,
      organisationCorrespondencePhone2: organisationMaster.organisationCorrespondencePhone2,
      organisationCorrespondenceAddress: organisationMaster.organisationCorrespondenceAddress,
      organisationWebsite: organisationMaster.organisationWebsite,
      organisationNodalOfficerName: organisationMaster.organisationNodalOfficerName,
      organisationNodalOfficerPhoneMobile: organisationMaster.organisationNodalOfficerPhoneMobile,
      organisationNodalOfficerPhoneLandline: organisationMaster.organisationNodalOfficerPhoneLandline,
      nodalOfficerOfficialEmail: organisationMaster.nodalOfficerOfficialEmail,
      organisationNodalOfficerAlternativeEmail: organisationMaster.organisationNodalOfficerAlternativeEmail,
      createdBy: organisationMaster.createdBy,
      createdOn: organisationMaster.createdOn,
      lastUpdatedBy: organisationMaster.lastUpdatedBy,
      lastUpdatedOn: organisationMaster.lastUpdatedOn,
      verifiedBy: organisationMaster.verifiedBy,
      verifiedOn: organisationMaster.verifiedOn,
      remarks: organisationMaster.remarks,
      userId: organisationMaster.userId,
      stage: organisationMaster.stage,
      status: organisationMaster.status,
      organisationIdCode: organisationMaster.organisationIdCode,
      organisationEmployeeId: organisationMaster.organisationEmployeeId,
      dateOfBirth: organisationMaster.dateOfBirth,
      gender: organisationMaster.gender,
      pan: organisationMaster.pan,
      aadhaar: organisationMaster.aadhaar,
      orgnisationEmpolyeeCardAttchment: organisationMaster.orgnisationEmpolyeeCardAttchment,
      panAttachment: organisationMaster.panAttachment,
      photographAttachment: organisationMaster.photographAttachment,
      aadhaarAttachment: organisationMaster.aadhaarAttachment,
      organisationTypeMasterId: organisationMaster.organisationTypeMasterId,
      stateMasterId: organisationMaster.stateMasterId,
      districtMasterId: organisationMaster.districtMasterId,
      subDistrictMasterId: organisationMaster.subDistrictMasterId,
      departmentId: organisationMaster.departmentId
    });
  }


  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrganisationMaster>>) {
    result.subscribe((res: HttpResponse<any>) => this.onSaveSuccess(res.body), response => this.onSaveError(response));
  }

  protected onSaveSuccess(result) {
    if (result) {
      alert("Your Organisation Request Has Been Submmitted Successfully");
      this.saveMultipleFile(result);
      this.router.navigate(['asp-dashboard']);
    }
    this.isSaving = false;
  }

  protected onSaveError(response) {
    this.isSaving = false;
    alert(response.error.title);
  }

  // uploadFile(filesData, documentId) {
  //   let selectedfile = filesData[0];
  //   this.arrayFile.push({ selectedfile, documentId });
  //   selectedfile = null;
  // }

  saveMultipleFile(result) {
    if (this.arrayFile.length > 0) {
      this.minioUploadService.createMultipleFile(this.arrayFile, result.id).subscribe((res: HttpResponse<any>) => {
        this.attachmentFiles = res.body;
      });
    }
  }
}
