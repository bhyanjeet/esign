import {Component, OnInit} from '@angular/core';
import {UserDetailsService} from "app/entities/user-details/user-details.service";
import {IUserDetails} from "app/shared/model/user-details.model";
import {HttpResponse} from "@angular/common/http";

@Component({
  selector: 'jhi-asp-user-dashboard',
  templateUrl: './asp-user-dashboard.component.html',
  styleUrls: [
    'asp-user-dashboard.component.scss'
  ]
})
export class AspUserDashboardComponent implements OnInit {

  userdetails: IUserDetails;
  userDetails: IUserDetails[];

  message: string;

  constructor(
    protected userDetailsService: UserDetailsService,
  ) {
    this.message = 'AspUserDashboardComponent message';
  }

  ngOnInit() {
    this.userDetailsService
      .getVerifiedUserStatusRecord()
      .subscribe((res: HttpResponse<IUserDetails[]>) => {
        this.userDetails = res.body;
      });

  }
}
