import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { AspUserDashboardComponent } from './asp-user-dashboard.component';

export const ASP_USER_DASHBOARD_ROUTE: Route = {
  path: 'asp-user-dashboard',
  component: AspUserDashboardComponent,
  data: {
    authorities: ['ROLE_ASP_NODAL'],
    pageTitle: 'asp-user-dashboard.title'
  },
  canActivate: [UserRouteAccessService]
};
