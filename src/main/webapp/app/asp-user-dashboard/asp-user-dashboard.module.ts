import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';

import { ASP_USER_DASHBOARD_ROUTE, AspUserDashboardComponent } from './';

@NgModule({
    imports: [
      EsignharyanaSharedModule,
      RouterModule.forRoot([ ASP_USER_DASHBOARD_ROUTE ], { useHash: true })
    ],
    declarations: [
      AspUserDashboardComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppAspUserDashboardModule {}
