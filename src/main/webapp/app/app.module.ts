import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { EsignharyanaSharedModule } from 'app/shared/shared.module';
import { EsignharyanaCoreModule } from 'app/core/core.module';
import { EsignharyanaAppRoutingModule } from './app-routing.module';
import { EsignharyanaHomeModule } from './home/home.module';
import { EsignharyanaEntityModule } from './entities/entity.module';
import { EsignharyanaAppOrganisationModule } from 'app/organisation';
import { EsignharyanaAppAspDashboardModule } from 'app/asp-dashboard';
import { EsignharyanaAppOrganisationDetailModule } from 'app/organisation-detail';
import { EsignharyanaAppApplicationModule } from 'app/application';
import { EsignharyanaAppApplicationDashboardModule } from 'app/application-dashboard';
import { EsignharyanaAppApplicationDetailModule } from 'app/application-detail';
import { EsignharyanaAppUserModule } from 'app/user';
import { EsignharyanaAppUserDashboardModule } from 'app/user-dashboard';
import { EsignharyanaAppViewUserModule } from 'app/view-user';
import { EsignharyanaAppOrganisationDashboardModule } from 'app/organisation-dashboard';
import { EsignharyanaAppAspApplicationDashboardModule } from 'app/asp-application-dashboard';
import { EsignharyanaAppAspUserDashboardModule } from 'app/asp-user-dashboard';
import { EsignharyanaAppOrganisationUpdateModule } from 'app/organisation-dashboard';
import { EsignharyanaAppApplicationUpdateModule } from 'app/application-dashboard';
import { EsignharyanaAppUserUpdateModule } from 'app/user-dashboard';
import { EsignharyanaAppUserApplicationModule } from 'app/user-application';
import { EsignharyanaAppEsignFormRequestModule } from 'app/esign-form-request';
import { EsignharyanaAppEsignFinalFormRequestModule } from 'app/esign-final-form-request';
import { EsignharyanaAppUserEsignRequestFormModule } from './user-esign-request-form/user-esign-request-form.module';
import { EsignharyanaAppEsignResponseDataModule } from './esign-response-data/esign-response-data.module';

import { EsignharyanaAppEsignFailureModule } from './esign-failure/esign-failure.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { JhiMainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';


@NgModule({
  imports: [
    BrowserModule,
    EsignharyanaSharedModule,
    EsignharyanaCoreModule,
    EsignharyanaHomeModule,
    EsignharyanaAppOrganisationModule,
    EsignharyanaAppAspDashboardModule,
        EsignharyanaAppOrganisationDetailModule,
        EsignharyanaAppApplicationModule,
        EsignharyanaAppApplicationDashboardModule,
        EsignharyanaAppApplicationDetailModule,
        EsignharyanaAppUserModule,
        EsignharyanaAppUserDashboardModule,
        EsignharyanaAppViewUserModule,
        EsignharyanaAppOrganisationDashboardModule,
        EsignharyanaAppAspApplicationDashboardModule,
        EsignharyanaAppAspUserDashboardModule,
        EsignharyanaAppOrganisationUpdateModule,
        EsignharyanaAppApplicationUpdateModule,
        EsignharyanaAppUserUpdateModule,
        EsignharyanaAppUserApplicationModule,
        EsignharyanaAppEsignFormRequestModule,
        EsignharyanaAppEsignFinalFormRequestModule,
        EsignharyanaAppUserEsignRequestFormModule,
        EsignharyanaAppEsignResponseDataModule,

        EsignharyanaAppEsignFailureModule,
        // jhipster-needle-angular-add-module JHipster will add new module here
    EsignharyanaEntityModule,
    EsignharyanaAppRoutingModule
  ],
  declarations: [JhiMainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [JhiMainComponent]
})
export class EsignharyanaAppModule {}
