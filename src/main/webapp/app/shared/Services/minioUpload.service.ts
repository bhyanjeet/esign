import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';

@Injectable({ providedIn: 'root' })
export class MinioUploadService {
  private fileUpload = SERVER_API_URL + 'api/minioFileSave';
  private minioFileGetResourceUrl = SERVER_API_URL + 'api/getAttachment';
  private multiplefileUpload = SERVER_API_URL + 'api/saveMultipleAttachment';
  private multiplefileUploadBulk = SERVER_API_URL + 'api/minioFileSave/bulk';
  constructor(private http: HttpClient) {}

  create(file: any, fileName: any): Observable<any> {
    const responseType = 'text' as 'json';
    const uploadData = new FormData();
    uploadData.append('file', file);
    uploadData.append('filename', fileName);
    return this.http.post(this.fileUpload, uploadData, {
      observe: 'response' as 'body',
      responseType

    });
  }
  createMultipleFile(files: any, organisationId: any): Observable<any> {
    const uploadData = new FormData();
    for (const uploadFile of files) {
      uploadData.append(uploadFile.documentId, uploadFile.selectedfile);
    }
    uploadData.set('organisationId', organisationId);
    return this.http.post(this.multiplefileUpload, uploadData, { observe: 'response' });
  }
  createMultipleFiles(files: any): Observable<any> {
    const uploadData = new FormData();
    for (const uploadFile of files) {
        uploadData.append(uploadFile.type, uploadFile.file)
    }
    return this.http.post<any>(this.multiplefileUploadBulk, uploadData, { observe: 'response' });
  }

  getMinioFileUrl(fileName: string): Observable<any> {
    const responseType = 'text' as 'json';
    const value = new HttpParams().set('filename', fileName);
    return this.http.get<any>(`${this.minioFileGetResourceUrl}/`, {
      params: value,
      observe: 'response' as 'body',
      responseType
    });
  }
}
