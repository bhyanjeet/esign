import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class CheckInputService {
    checkV(elem) {
        if (
            elem.val().includes('<') ||
            elem.val().includes('<script') ||
            elem.val().includes('alert(') ||
            elem.val().includes('.js') ||
            elem.val().includes('.exe') ||
            elem.val().includes('.ts') ||
            elem.val().includes('.sh') ||
            elem.val().includes('.jar') ||
            elem.val().includes('.class') ||
            elem.val().includes('.class') ||
            elem.val().includes('bash') ||
            elem.val().includes('./') ||
            elem.val().includes('.tar.gz') ||
            elem.val().includes('.deb') ||
            elem.val().includes('.rpm') ||
            elem.val().includes('.ko') ||
            elem.val().includes('.php') ||
            elem.val().includes('.py') ||
            elem.val().includes('.tar')
        ) {
            elem.val(null);
            alert('Please insert relevant information in this field.');
        }
    }
}
