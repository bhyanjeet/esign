import { NgModule } from '@angular/core';
import { EsignharyanaSharedLibsModule } from './shared-libs.module';
import { JhiAlertComponent } from './alert/alert.component';
import { JhiAlertErrorComponent } from './alert/alert-error.component';
import { JhiLoginModalComponent } from './login/login.component';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import {RecaptchaModule} from 'ng-recaptcha';

@NgModule({
  imports: [EsignharyanaSharedLibsModule, RecaptchaModule.forRoot()],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent, JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [EsignharyanaSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent, JhiLoginModalComponent, HasAnyAuthorityDirective, RecaptchaModule]
})
export class EsignharyanaSharedModule {}
