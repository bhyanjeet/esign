export interface IUserAuthDetails {
  id?: number;
  login?: string;
  device?: string;
  authToken?: string;
}

export class UserAuthDetails implements IUserAuthDetails {
  constructor(public id?: number, public login?: string, public device?: string, public authToken?: string) {}
}
