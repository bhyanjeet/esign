import { Moment } from 'moment';

export interface ISubDistrictMaster {
  id?: number;
  subDistrictCode?: string;
  subDistrictName?: string;
  lastUpdatedBy?: string;
  lastUpdatedOn?: Moment;
  verifiedBy?: string;
  verifiedOn?: Moment;
  remarks?: string;
  stateMasterId?: number;
  districtMasterId?: number;
}

export class SubDistrictMaster implements ISubDistrictMaster {
  constructor(
    public id?: number,
    public subDistrictCode?: string,
    public subDistrictName?: string,
    public lastUpdatedBy?: string,
    public lastUpdatedOn?: Moment,
    public verifiedBy?: string,
    public verifiedOn?: Moment,
    public remarks?: string,
    public stateMasterId?: number,
    public districtMasterId?: number
  ) {}
}
