export interface IEsignError {
  id?: number;
  errorCode?: string;
  errorMessage?: string;
  errorStage?: string;
}

export class EsignError implements IEsignError {
  constructor(public id?: number, public errorCode?: string, public errorMessage?: string, public errorStage?: string) {}
}
