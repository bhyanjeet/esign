import { Moment } from 'moment';

export interface IUserApplication {
  id?: number;
  userId?: number;
  userLogin?: string;
  createdBy?: string;
  createdOn?: Moment;
  updatedBy?: string;
  updatedOn?: Moment;
  verifiedBy?: string;
  verifiedOn?: Moment;
  userCodeId?: string;
  applicationCodeId?: string;
  applicationMasterId?: number;
}

export class UserApplication implements IUserApplication {
  constructor(
    public id?: number,
    public userId?: number,
    public userLogin?: string,
    public createdBy?: string,
    public createdOn?: Moment,
    public updatedBy?: string,
    public updatedOn?: Moment,
    public verifiedBy?: string,
    public verifiedOn?: Moment,
    public userCodeId?: string,
    public applicationCodeId?: string,
    public applicationMasterId?: number
  ) {}
}
