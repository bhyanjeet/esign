import { Moment } from 'moment';

export interface IDistrictMaster {
  id?: number;
  districtName?: string;
  districtCode?: string;
  createdOn?: Moment;
  lastUpdatedBy?: string;
  lastUpdatedOn?: Moment;
  verifiedBy?: string;
  verifiedOn?: Moment;
  remarks?: string;
  stateMasterId?: number;
}

export class DistrictMaster implements IDistrictMaster {
  constructor(
    public id?: number,
    public districtName?: string,
    public districtCode?: string,
    public createdOn?: Moment,
    public lastUpdatedBy?: string,
    public lastUpdatedOn?: Moment,
    public verifiedBy?: string,
    public verifiedOn?: Moment,
    public remarks?: string,
    public stateMasterId?: number
  ) {}
}
