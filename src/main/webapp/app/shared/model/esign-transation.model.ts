import { Moment } from 'moment';

export interface IEsignTransation {
  id?: number;
  txnId?: string;
  userCodeId?: string;
  applicationCodeId?: string;
  orgResponseSuccessURL?: string;
  orgResponseFailURL?: string;
  orgStatus?: string;
  requestDate?: Moment;
  organisationIdCode?: string;
  signerId?: string;
  requestType?: string;
  requestStatus?: string;
  responseType?: string;
  responseStatus?: string;
  responseCode?: string;
  requestXml?: string;
  responseXml?: string;
  responseDate?: Moment;
  ts?: string;
  reqData?: string;

}

export class EsignTransation implements IEsignTransation {
  constructor(
    public id?: number,
    public txnId?: string,
    public userCodeId?: string,
    public applicationCodeId?: string,
    public orgResponseSuccessURL?: string,
    public orgResponseFailURL?: string,
    public orgStatus?: string,
    public requestDate?: Moment,
    public organisationIdCode?: string,
    public signerId?: string,
    public requestType?: string,
    public requestStatus?: string,
    public responseType?: string,
    public responseStatus?: string,
    public responseCode?: string,
    public requestXml?: string,
    public responseXml?: string,
    public responseDate?: Moment,
    public ts?: string,
    public reqData?: string
  ) {}
}
