import { Moment } from 'moment';

export interface IUserLogs {
  id?: number;
  actionTaken?: string;
  actionTakenBy?: string;
  actionTakenOnUser?: number;
  actionTakenOnDate?: Moment;
  remarks?: string;
}

export class UserLogs implements IUserLogs {
  constructor(
    public id?: number,
    public actionTaken?: string,
    public actionTakenBy?: string,
    public actionTakenOnUser?: number,
    public actionTakenOnDate?: Moment,
    public remarks?: string
  ) {}
}
