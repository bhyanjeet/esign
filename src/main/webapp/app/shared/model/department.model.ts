import { Moment } from 'moment';

export interface IDepartment {
  id?: number;
  departmentName?: string;
  createdOn?: Moment;
  createdBy?: string;
  updatedOn?: Moment;
  updatedBy?: string;
  status?: string;
  remark?: string;
  whiteListIp1?: string;
  whiteListIp2?: string;
}

export class Department implements IDepartment {
  constructor(
    public id?: number,
    public departmentName?: string,
    public createdOn?: Moment,
    public createdBy?: string,
    public updatedOn?: Moment,
    public updatedBy?: string,
    public status?: string,
    public remark?: string,
    public whiteListIp1?: string,
    public whiteListIp2?: string
  ) {}
}
