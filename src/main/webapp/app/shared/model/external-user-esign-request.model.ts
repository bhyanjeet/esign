export interface IExternalUserEsignRequest {
  id?: number;
  userCodeId?: string;
  applicationIdCode?: string;
  responseUrl?: string;
  redirectUrl?: string;
  ts?: string;
  signerId?: string;
  docInfo?: string;
  docUrl?: string;
  docHash?: string;
  requestStatus?: string;
  requestTime?: string;
  txn?: string;
  txnRef?: string;
  aspId?: string;
  aspIp?: string;
  refData?: string;
}

export class ExternalUserEsignRequest implements IExternalUserEsignRequest {
  constructor(
    public id?: number,
    public userCodeId?: string,
    public applicationIdCode?: string,
    public responseUrl?: string,
    public redirectUrl?: string,
    public ts?: string,
    public signerId?: string,
    public docInfo?: string,
    public docUrl?: string,
    public docHash?: string,
    public requestStatus?: string,
    public requestTime?: string,
    public txn?: string,
    public txnRef?: string,
    public aspId?: string,
    public aspIp?: string,
    public refData?: string
  ) {}
}
