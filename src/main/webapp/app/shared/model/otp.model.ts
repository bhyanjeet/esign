import { Moment } from 'moment';

export interface IOtp {
  id?: number;
  otp?: number;
  mobileNumber?: number;
  requestTime?: Moment;
}

export class Otp implements IOtp {
  constructor(public id?: number, public otp?: number, public mobileNumber?: number, public requestTime?: Moment) {}
}
