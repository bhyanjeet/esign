import { Moment } from 'moment';

export interface IOrganisationLog {
  id?: number;
  organisationId?: number;
  createdByLogin?: string;
  createdDate?: Moment;
  verifyByName?: string;
  verifyDate?: Moment;
  remarks?: string;
  status?: string;
  verifyByLogin?: string;
  createdByName?: string;
}

export class OrganisationLog implements IOrganisationLog {
  constructor(
    public id?: number,
    public organisationId?: number,
    public createdByLogin?: string,
    public createdDate?: Moment,
    public verifyByName?: string,
    public verifyDate?: Moment,
    public remarks?: string,
    public status?: string,
    public verifyByLogin?: string,
    public createdByName?: string
  ) {}
}
