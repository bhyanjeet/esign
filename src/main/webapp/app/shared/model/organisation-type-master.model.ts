import { Moment } from 'moment';

export interface IOrganisationTypeMaster {
  id?: number;
  organisationTypeDetail?: string;
  organisationTypeAbbreviation?: string;
  createdBy?: string;
  createdOn?: Moment;
  lastUpdatedBy?: string;
  lastUpdatedOn?: Moment;
  verifiedBy?: string;
  verifiedOn?: Moment;
  remarks?: string;
}

export class OrganisationTypeMaster implements IOrganisationTypeMaster {
  constructor(
    public id?: number,
    public organisationTypeDetail?: string,
    public organisationTypeAbbreviation?: string,
    public createdBy?: string,
    public createdOn?: Moment,
    public lastUpdatedBy?: string,
    public lastUpdatedOn?: Moment,
    public verifiedBy?: string,
    public verifiedOn?: Moment,
    public remarks?: string
  ) {}
}
