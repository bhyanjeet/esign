import { Moment } from 'moment';

export interface IApplicationMaster {
  id?: number;
  applicationName?: string;
  createdBy?: string;
  createdOn?: Moment;
  lastUpdatedBy?: string;
  lastUpdatedOn?: Moment;
  verifiedBy?: string;
  verifiedOn?: Moment;
  remarks?: string;
  applicationURL?: string;
  applicationStatus?: string;
  applicationIdCode?: string;
  status?: string;
  certPublicKey?: string;
  organisationMasterId?: number;
}

export class ApplicationMaster implements IApplicationMaster {
  constructor(
    public id?: number,
    public applicationName?: string,
    public createdBy?: string,
    public createdOn?: Moment,
    public lastUpdatedBy?: string,
    public lastUpdatedOn?: Moment,
    public verifiedBy?: string,
    public verifiedOn?: Moment,
    public remarks?: string,
    public applicationURL?: string,
    public applicationStatus?: string,
    public applicationIdCode?: string,
    public status?: string,
    public certPublicKey?: string,
    public organisationMasterId?: number
  ) {}
}
