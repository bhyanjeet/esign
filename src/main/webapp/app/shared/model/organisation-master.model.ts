import { Moment } from 'moment';

export interface IOrganisationMaster {
  id?: number;
  organisationName?: string;
  organisationAbbreviation?: string;
  organisationCorrespondenceEmail?: string;
  organisationCorrespondencePhone1?: string;
  organisationCorrespondencePhone2?: string;
  organisationCorrespondenceAddress?: string;
  organisationWebsite?: string;
  organisationNodalOfficerName?: string;
  organisationNodalOfficerPhoneMobile?: string;
  organisationNodalOfficerPhoneLandline?: string;
  nodalOfficerOfficialEmail?: string;
  organisationNodalOfficerAlternativeEmail?: string;
  createdBy?: string;
  createdOn?: Moment;
  lastUpdatedBy?: string;
  lastUpdatedOn?: Moment;
  verifiedBy?: string;
  verifiedOn?: Moment;
  remarks?: string;
  userId?: number;
  stage?: string;
  status?: string;
  organisationIdCode?: string;
  organisationEmployeeId?: string;
  dateOfBirth?: Moment;
  gender?: string;
  pan?: string;
  aadhaar?: string;
  orgnisationEmpolyeeCardAttchment?: string;
  panAttachment?: string;
  photographAttachment?: string;
  aadhaarAttachment?: string;
  pinCode?: string;
  organisationTypeMasterId?: number;
  stateMasterId?: number;
  districtMasterId?: number;
  subDistrictMasterId?: number;
  departmentId?: number;
}

export class OrganisationMaster implements IOrganisationMaster {
  constructor(
    public id?: number,
    public organisationName?: string,
    public organisationAbbreviation?: string,
    public organisationCorrespondenceEmail?: string,
    public organisationCorrespondencePhone1?: string,
    public organisationCorrespondencePhone2?: string,
    public organisationCorrespondenceAddress?: string,
    public organisationWebsite?: string,
    public organisationNodalOfficerName?: string,
    public organisationNodalOfficerPhoneMobile?: string,
    public organisationNodalOfficerPhoneLandline?: string,
    public nodalOfficerOfficialEmail?: string,
    public organisationNodalOfficerAlternativeEmail?: string,
    public createdBy?: string,
    public createdOn?: Moment,
    public lastUpdatedBy?: string,
    public lastUpdatedOn?: Moment,
    public verifiedBy?: string,
    public verifiedOn?: Moment,
    public remarks?: string,
    public userId?: number,
    public stage?: string,
    public status?: string,
    public organisationIdCode?: string,
    public organisationEmployeeId?: string,
    public dateOfBirth?: Moment,
    public gender?: string,
    public pan?: string,
    public aadhaar?: string,
    public orgnisationEmpolyeeCardAttchment?: string,
    public panAttachment?: string,
    public photographAttachment?: string,
    public aadhaarAttachment?: string,
    public pinCode?: string,
    public organisationTypeMasterId?: number,
    public stateMasterId?: number,
    public districtMasterId?: number,
    public subDistrictMasterId?: number,
    public departmentId?: number
  ) {}
}
