import { Moment } from 'moment';

export interface IBlockMaster {
  id?: number;
  blockCode?: string;
  blockName?: string;
  createdOn?: Moment;
  lastUpdatedBy?: string;
  lastUpdatedOn?: Moment;
  verifiedBy?: string;
  verifiedOn?: Moment;
  remarks?: string;
  stateMasterId?: number;
  districtMasterId?: number;
}

export class BlockMaster implements IBlockMaster {
  constructor(
    public id?: number,
    public blockCode?: string,
    public blockName?: string,
    public createdOn?: Moment,
    public lastUpdatedBy?: string,
    public lastUpdatedOn?: Moment,
    public verifiedBy?: string,
    public verifiedOn?: Moment,
    public remarks?: string,
    public stateMasterId?: number,
    public districtMasterId?: number
  ) {}
}
