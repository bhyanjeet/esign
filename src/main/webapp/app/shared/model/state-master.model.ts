import { Moment } from 'moment';

export interface IStateMaster {
  id?: number;
  stateCode?: string;
  stateName?: string;
  createdOn?: Moment;
  lastUpdatedOn?: Moment;
  lastUpdatedBy?: string;
  verifiedBy?: string;
  verifiedOn?: Moment;
  remarks?: string;
}

export class StateMaster implements IStateMaster {
  constructor(
    public id?: number,
    public stateCode?: string,
    public stateName?: string,
    public createdOn?: Moment,
    public lastUpdatedOn?: Moment,
    public lastUpdatedBy?: string,
    public verifiedBy?: string,
    public verifiedOn?: Moment,
    public remarks?: string
  ) {}
}
