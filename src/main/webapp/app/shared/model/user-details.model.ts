import { Moment } from 'moment';

export interface IUserDetails {
  id?: number;
  userFullName?: string;
  userPhoneMobile?: string;
  userPhoneLandline?: string;
  userOfficialEmail?: string;
  userAlternativeEmail?: string;
  userLoginPassword?: string;
  userLoginPasswordSalt?: string;
  userTransactionPassword?: string;
  userTransactionPasswordSalt?: string;
  createdBy?: string;
  createdOn?: Moment;
  lastUpdatedBy?: string;
  lastUpdatedOn?: Moment;
  verifiedBy?: string;
  verifiedOn?: Moment;
  remarks?: string;
  userIdCode?: string;
  status?: string;
  username?: string;
  userPIN?: string;
  organisationUnit?: string;
  address?: string;
  counrty?: string;
  postalCode?: string;
  photograph?: string;
  dob?: Moment;
  gender?: string;
  pan?: string;
  aadhaar?: string;
  panAttachment?: string;
  employeeId?: string;
  employeeCardAttachment?: string;
  signerId?: string;
  lastAction?: string;
  accountStatus?: string;
  designationMasterId?: number;
  organisationMasterId?: number;
  esignRoleId?: number;
  applicationMasterId?: number;
}

export class UserDetails implements IUserDetails {
  constructor(
    public id?: number,
    public userFullName?: string,
    public userPhoneMobile?: string,
    public userPhoneLandline?: string,
    public userOfficialEmail?: string,
    public userAlternativeEmail?: string,
    public userLoginPassword?: string,
    public userLoginPasswordSalt?: string,
    public userTransactionPassword?: string,
    public userTransactionPasswordSalt?: string,
    public createdBy?: string,
    public createdOn?: Moment,
    public lastUpdatedBy?: string,
    public lastUpdatedOn?: Moment,
    public verifiedBy?: string,
    public verifiedOn?: Moment,
    public remarks?: string,
    public userIdCode?: string,
    public status?: string,
    public username?: string,
    public userPIN?: string,
    public organisationUnit?: string,
    public address?: string,
    public counrty?: string,
    public postalCode?: string,
    public photograph?: string,
    public dob?: Moment,
    public gender?: string,
    public pan?: string,
    public aadhaar?: string,
    public panAttachment?: string,
    public employeeId?: string,
    public employeeCardAttachment?: string,
    public signerId?: string,
    public lastAction?: string,
    public accountStatus?: string,
    public designationMasterId?: number,
    public organisationMasterId?: number,
    public esignRoleId?: number,
    public applicationMasterId?: number
  ) {}
}
