import { Moment } from 'moment';

export interface IDesignationMaster {
  id?: number;
  designationName?: string;
  designationAbbreviation?: string;
  createdBy?: string;
  createdOn?: Moment;
  lastUpdatedBy?: string;
  lastUpdatedOn?: Moment;
  verifiedBy?: string;
  verifiedOn?: Moment;
  remarks?: string;
}

export class DesignationMaster implements IDesignationMaster {
  constructor(
    public id?: number,
    public designationName?: string,
    public designationAbbreviation?: string,
    public createdBy?: string,
    public createdOn?: Moment,
    public lastUpdatedBy?: string,
    public lastUpdatedOn?: Moment,
    public verifiedBy?: string,
    public verifiedOn?: Moment,
    public remarks?: string
  ) {}
}
