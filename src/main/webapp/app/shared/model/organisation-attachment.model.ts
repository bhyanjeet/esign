export interface IOrganisationAttachment {
  id?: number;
  attachment?: string;
  organisationMasterId?: number;
  organisationDocumentId?: number;
}

export class OrganisationAttachment implements IOrganisationAttachment {
  constructor(
    public id?: number,
    public attachment?: string,
    public organisationMasterId?: number,
    public organisationDocumentId?: number
  ) {}
}
