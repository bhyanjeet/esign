import { Moment } from 'moment';

export interface IOrganisationDocument {
  id?: number;
  documentTitle?: string;
  createdBy?: string;
  createdOn?: Moment;
  verifiedBy?: string;
  verifiedOn?: Moment;
  remarks?: string;
  documentRelatedTo?: string;
  organisationTypeMasterId?: number;
}

export class OrganisationDocument implements IOrganisationDocument {
  constructor(
    public id?: number,
    public documentTitle?: string,
    public createdBy?: string,
    public createdOn?: Moment,
    public verifiedBy?: string,
    public verifiedOn?: Moment,
    public remarks?: string,
    public documentRelatedTo?: string,
    public organisationTypeMasterId?: number
  ) {}
}
