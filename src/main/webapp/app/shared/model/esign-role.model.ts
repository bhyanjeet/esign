import { Moment } from 'moment';

export interface IEsignRole {
  id?: number;
  eSignRoleDetail?: string;
  createdBy?: string;
  createdOn?: Moment;
  lastUpdatedBy?: string;
  lastUpdatedOn?: Moment;
  verifiedBy?: string;
  verifiedOn?: Moment;
  remarks?: string;
}

export class EsignRole implements IEsignRole {
  constructor(
    public id?: number,
    public eSignRoleDetail?: string,
    public createdBy?: string,
    public createdOn?: Moment,
    public lastUpdatedBy?: string,
    public lastUpdatedOn?: Moment,
    public verifiedBy?: string,
    public verifiedOn?: Moment,
    public remarks?: string
  ) {}
}
