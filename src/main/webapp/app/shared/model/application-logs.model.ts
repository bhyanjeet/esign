import { Moment } from 'moment';

export interface IApplicationLogs {
  id?: number;
  actionTaken?: string;
  actionTakenBy?: string;
  actionTakenOnApplication?: number;
  actionTakenOnDate?: Moment;
  remarks?: string;
}

export class ApplicationLogs implements IApplicationLogs {
  constructor(
    public id?: number,
    public actionTaken?: string,
    public actionTakenBy?: string,
    public actionTakenOnApplication?: number,
    public actionTakenOnDate?: Moment,
    public remarks?: string
  ) {}
}
