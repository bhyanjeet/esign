import { Moment } from 'moment';

export interface IEsignFormRequest {
  signerid?: string;
  responseUrl?: string;
  redirectUrl?: string;
  docInfo?: string;
  asp_ip?: string;
  aspid?: string;
  req_data?: string;
  txnref?: string;
}

export class EsignFormRequest implements IEsignFormRequest {
  constructor(
    public signerid?: string,
    public responseUrl?: string,
    public redirectUrl?: string,
    public docInfo?: string,
    public asp_ip?: string,
    public aspid?: string,
    public req_data?: string,
    public txnref?: string

  ) {}
}
