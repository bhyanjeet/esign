import {ActivatedRouteSnapshot, Resolve, Route, Router} from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { UserEsignRequestFormComponent } from './user-esign-request-form.component';
import {Injectable} from "@angular/core";
import {ExternalUserEsignRequest, IExternalUserEsignRequest} from "app/shared/model/external-user-esign-request.model";
import {ExternalUserEsignRequestService} from "app/entities/external-user-esign-request/external-user-esign-request.service";
import {EMPTY, Observable, of} from "rxjs";
import {flatMap} from "rxjs/operators";
import {HttpResponse} from "@angular/common/http";

@Injectable({ providedIn: 'root' })
export class ExternalUserEsignRequestResolve implements Resolve<IExternalUserEsignRequest> {
  constructor(private service: ExternalUserEsignRequestService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IExternalUserEsignRequest> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((externalUserEsignRequest: HttpResponse<ExternalUserEsignRequest>) => {
          if (externalUserEsignRequest.body) {
            return of(externalUserEsignRequest.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ExternalUserEsignRequest());
  }
}

export const USER_ESIGN_REQUEST_FORM_ROUTE: Route = {
  path: 'user-esign-request-form/:errorMessage',
  component: UserEsignRequestFormComponent,
  resolve: {
    externalUserEsignRequest: ExternalUserEsignRequestResolve,
  },
  data: {
    authorities: [],
    pageTitle: 'user-esign-request-form.title'
  },
  canActivate: [UserRouteAccessService]
};
