import {Component, OnInit} from '@angular/core';
import {ExternalUserEsignRequestService} from "app/entities/external-user-esign-request/external-user-esign-request.service";
import {ActivatedRoute} from "@angular/router";
import {FormBuilder} from "@angular/forms";
import {ExternalUserEsignRequest, IExternalUserEsignRequest} from "app/shared/model/external-user-esign-request.model";
import {Observable} from "rxjs";
import {HttpResponse} from "@angular/common/http";
import {IEsignFormRequest} from "app/shared/model/esign-form-request.model";
import { Router } from '@angular/router';
import {UserDetailsService} from "app/entities/user-details/user-details.service";
import {IUserDetails} from "app/shared/model/user-details.model";

@Component({
  selector: 'jhi-user-esign-request-form',
  templateUrl: './user-esign-request-form.component.html',
  styleUrls: [
    'user-esign-request-form.component.scss'
  ]
})
export class UserEsignRequestFormComponent implements OnInit {

  isSaving = false;

  editForm = this.fb.group({
    id: [],
    userCodeId: [],
    applicationIdCode: [],
    responseUrl: [],
    redirectUrl: [],
    ts: [],
    signerId: [],
    docInfo: [],
    docUrl: [],
    docHash: [],
    requestStatus: [],
    requestTime: [],
    aspId: [],
    aspIp: [],
    txnRef: [],
    refData: []

  });
   eSignFormRequest: any;
   hideForm: boolean;
   show: boolean;
   aspIdd: any;
  aspIpp: any;
  txnReff: any;
  reqDataa: any;
  docUrl: string;
  userCodeId: string;
  userDetails: IUserDetails;
  errorMessage: any;
  userErrorMessage: string;

  constructor(
    protected externalUserEsignRequestService: ExternalUserEsignRequestService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    protected userDetailsService: UserDetailsService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.errorMessage = params.get("errorMessage");
      if (this.errorMessage==="TxnAllreadyPresent"){
        this.userErrorMessage = "This Txn Id allready requested! Please try with another Txn Id";
      }
      if (this.errorMessage==="UserNotPresent"){
        this.userErrorMessage = "This User Id not found! Please try again with valid User Id";
      }
      if (this.errorMessage==="ApplicationNotPresent"){
        this.userErrorMessage = "This Application Id not found! Please try again with valid Application Id";
      }
      if (this.errorMessage==="SignerIdNotFound"){
        this.userErrorMessage = "This Signer Id not found! Please try again with valid Signer Id";
      }
    });
    this.hideForm = true;
    this.show = false;

  }

  updateForm(externalUserEsignRequest: IExternalUserEsignRequest): void {
    this.editForm.patchValue({
      id: externalUserEsignRequest.id,
      userCodeId: externalUserEsignRequest.userCodeId,
      applicationIdCode: externalUserEsignRequest.applicationIdCode,
      responseUrl: externalUserEsignRequest.responseUrl,
      redirectUrl: externalUserEsignRequest.redirectUrl,
      ts: externalUserEsignRequest.ts,
      signerId: externalUserEsignRequest.signerId,
      docInfo: externalUserEsignRequest.docInfo,
      docUrl: externalUserEsignRequest.docUrl,
      docHash: externalUserEsignRequest.docHash,
      requestStatus: externalUserEsignRequest.requestStatus,
      requestTime: externalUserEsignRequest.requestTime,
    });
  }

  previousState(): void {
    window.history.back();
  }



  // save() {
  //   const externalUserEsignRequest = this.createFromForm();
  //   this.externalUserEsignRequestService.sendForEsign(externalUserEsignRequest).subscribe((res: HttpResponse<IExternalUserEsignRequest>) => {
  //     this.eSignFormRequest = res.body;
  //      this.aspIdd = res.body.aspId;
  //     this.aspIpp = res.body.aspIp;
  //     this.txnReff = res.body.txnRef;
  //     this.reqDataa = res.body.refData;
  //     this.docUrl = res.body.docUrl;
  //     this.userCodeId = res.body.userCodeId;
  //
  //     if (this.aspIdd !==""){
  //       this.hideForm = false;
  //       this.show = true;
  //     }
  //
  //     if (this.userCodeId !==""){
  //      this.userDetailsService.finByUserCodeId(this.userCodeId).subscribe((response) => {
  //        this.userDetails = response.body;
  //        console.log("uuuuuuuuuuu", this.userDetails);
  //      });
  //   }
  // });
  // }

  // sendDocUrl(docUrl){
  //   alert(docUrl);
  //   this.docUrl = docUrl;
  // }


  private createFromForm(): IExternalUserEsignRequest {
    return {
      ...new ExternalUserEsignRequest(),
      id: this.editForm.get(['id'])!.value,
      userCodeId: this.editForm.get(['userCodeId'])!.value,
      applicationIdCode: this.editForm.get(['applicationIdCode'])!.value,
      responseUrl: this.editForm.get(['responseUrl'])!.value,
      redirectUrl: this.editForm.get(['redirectUrl'])!.value,
      ts: this.editForm.get(['ts'])!.value,
      signerId: this.editForm.get(['signerId'])!.value,
      docInfo: this.editForm.get(['docInfo'])!.value,
      docUrl: this.editForm.get(['docUrl'])!.value,
      docHash: this.editForm.get(['docHash'])!.value,
      requestStatus: this.editForm.get(['requestStatus'])!.value,
      requestTime: this.editForm.get(['requestTime'])!.value,
      aspId: this.editForm.get(['aspId'])!.value,
      aspIp: this.editForm.get(['aspIp'])!.value,
      txnRef: this.editForm.get(['txnRef'])!.value,
      refData: this.editForm.get(['refData'])!.value

    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExternalUserEsignRequest>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

}
