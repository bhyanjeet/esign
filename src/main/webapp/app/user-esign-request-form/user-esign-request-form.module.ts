import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';

import {UserEsignRequestFormComponent} from "app/user-esign-request-form/user-esign-request-form.component";
import {USER_ESIGN_REQUEST_FORM_ROUTE} from "app/user-esign-request-form/user-esign-request-form.route";



@NgModule({
    imports: [
      EsignharyanaSharedModule,
      RouterModule.forRoot([ USER_ESIGN_REQUEST_FORM_ROUTE ], { useHash: true })
    ],
    declarations: [
      UserEsignRequestFormComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppUserEsignRequestFormModule {}
