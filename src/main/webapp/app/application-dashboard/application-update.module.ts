import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';

import { APPLICATION_UPDATE_ROUTE, ApplicationUpdateComponent } from './';

@NgModule({
    imports: [
      EsignharyanaSharedModule,
      RouterModule.forRoot([ APPLICATION_UPDATE_ROUTE ], { useHash: true })
    ],
    declarations: [
      ApplicationUpdateComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppApplicationUpdateModule {}
