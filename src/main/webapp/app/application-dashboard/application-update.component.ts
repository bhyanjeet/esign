import {Component, OnInit} from '@angular/core';
import {IOrganisationMaster} from "app/shared/model/organisation-master.model";
import {FormBuilder, Validators} from "@angular/forms";
import {JhiAlertService} from "ng-jhipster";
import {ApplicationMasterService} from "app/entities/application-master/application-master.service";
import {OrganisationMasterService} from "app/entities/organisation-master/organisation-master.service";
import {ActivatedRoute} from "@angular/router";
import {HttpErrorResponse, HttpResponse} from "@angular/common/http";
import {ApplicationMaster, IApplicationMaster} from "app/shared/model/application-master.model";
import {Observable} from "rxjs";
import {ApplicationLogsService} from "app/entities/application-logs/application-logs.service";

@Component({
  selector: 'jhi-application-update',
  templateUrl: './application-update.component.html',
  styleUrls: [
    'application-update.component.scss'
  ]
})
export class ApplicationUpdateComponent implements OnInit {
  isSaving: boolean;
  organisationmasters: IOrganisationMaster[];
  createdOnDp: any;
  lastUpdatedOnDp: any;
  verifiedOnDp: any;
  message: string;

  editForm = this.fb.group({
    id: [],
    applicationName: [null, [Validators.required]],
    createdBy: [],
    createdOn: [],
    lastUpdatedBy: [],
    lastUpdatedOn: [],
    verifiedBy: [],
    verifiedOn: [],
    remarks: [],
    applicationURL: [],
    applicationStatus: [],
    applicationIdCode: [null, []],
    status: [],
    organisationMasterId: []
  });
   applicationLogs: any;


  constructor(
    protected jhiAlertService: JhiAlertService,
    protected applicationMasterService: ApplicationMasterService,
    protected applicationLogsService: ApplicationLogsService,
    protected organisationMasterService: OrganisationMasterService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder

  ) {
    this.message = 'ApplicationUpdateComponent message';
  }

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ applicationMaster }) => {
      this.updateForm(applicationMaster);
    });
    this.organisationMasterService
      .query()
      .subscribe(
        (res: HttpResponse<IOrganisationMaster[]>) => (this.organisationmasters = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(applicationMaster: IApplicationMaster) {
    this.editForm.patchValue({
      id: applicationMaster.id,
      applicationName: applicationMaster.applicationName,
      createdBy: applicationMaster.createdBy,
      createdOn: applicationMaster.createdOn,
      lastUpdatedBy: applicationMaster.lastUpdatedBy,
      lastUpdatedOn: applicationMaster.lastUpdatedOn,
      verifiedBy: applicationMaster.verifiedBy,
      verifiedOn: applicationMaster.verifiedOn,
      remarks: applicationMaster.remarks,
      applicationURL: applicationMaster.applicationURL,
      applicationStatus: applicationMaster.applicationStatus,
      applicationIdCode: applicationMaster.applicationIdCode,
      status: applicationMaster.status,
      organisationMasterId: applicationMaster.organisationMasterId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const applicationMaster = this.createFromForm();
    this.subscribeToSaveResponse(this.applicationMasterService.update(applicationMaster));
  }

  private createFromForm(): IApplicationMaster {
    return {
      ...new ApplicationMaster(),
      id: this.editForm.get(['id']).value,
      applicationName: this.editForm.get(['applicationName']).value,
      createdBy: this.editForm.get(['createdBy']).value,
      createdOn: this.editForm.get(['createdOn']).value,
      lastUpdatedBy: this.editForm.get(['lastUpdatedBy']).value,
      lastUpdatedOn: this.editForm.get(['lastUpdatedOn']).value,
      verifiedBy: this.editForm.get(['verifiedBy']).value,
      verifiedOn: this.editForm.get(['verifiedOn']).value,
      remarks: this.editForm.get(['remarks']).value,
      applicationURL: this.editForm.get(['applicationURL']).value,
      applicationStatus: this.editForm.get(['applicationStatus']).value,
      applicationIdCode: this.editForm.get(['applicationIdCode']).value,
      status: this.editForm.get(['status']).value,
      organisationMasterId: this.editForm.get(['organisationMasterId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IApplicationMaster>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    alert("Application Updated Successfully");
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

}
