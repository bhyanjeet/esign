import {Component, OnInit} from '@angular/core';
import { ApplicationMasterService } from 'app/entities/application-master/application-master.service';
import {IApplicationMaster} from "app/shared/model/application-master.model";
import {HttpResponse} from "@angular/common/http";

@Component({
  selector: 'jhi-application-dashboard',
  templateUrl: './application-dashboard.component.html',
  styleUrls: [
    'application-dashboard.component.scss'
  ]
})
export class ApplicationDashboardComponent implements OnInit {

applicationMasters: IApplicationMaster[];

  message: string;
  currentSearch: any;
  page: number;
  itemsPerPage: number;
  applicationmasters: IApplicationMaster[];

  constructor(
    protected applicationMasterService: ApplicationMasterService,
  ) {
    this.message = 'ApplicationDashboardComponent message';
  }


  ngOnInit() {
    this.applicationMasterService.findApplicationByUserId().subscribe((res1: HttpResponse< IApplicationMaster[]>) => {
      this.applicationMasters = res1.body;
    });
  }


}
