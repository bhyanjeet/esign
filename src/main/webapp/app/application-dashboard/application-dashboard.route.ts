import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ApplicationDashboardComponent } from './application-dashboard.component';

export const APPLICATION_DASHBOARD_ROUTE: Route = {
  path: 'application-dashboard',
  component: ApplicationDashboardComponent,
  data: {
    authorities: [],
    pageTitle: 'application-dashboard.title'
  },
  canActivate: [UserRouteAccessService]
};
