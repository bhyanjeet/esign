import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ApplicationUpdateComponent } from './application-update.component';
import {ApplicationMasterResolve} from "app/entities/application-master/application-master.route";

export const APPLICATION_UPDATE_ROUTE: Route = {
  path: 'application-update/:id/edit',
  component: ApplicationUpdateComponent,
  resolve: {
    applicationMaster: ApplicationMasterResolve
  },
  data: {
    authorities: [],
    pageTitle: 'application-update.title'
  },
  canActivate: [UserRouteAccessService]
};
