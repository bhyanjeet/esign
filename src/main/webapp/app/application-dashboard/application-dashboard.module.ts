import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';

import { APPLICATION_DASHBOARD_ROUTE, ApplicationDashboardComponent } from './';

@NgModule({
    imports: [
      EsignharyanaSharedModule,
      RouterModule.forRoot([ APPLICATION_DASHBOARD_ROUTE ], { useHash: true })
    ],
    declarations: [
      ApplicationDashboardComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppApplicationDashboardModule {}
