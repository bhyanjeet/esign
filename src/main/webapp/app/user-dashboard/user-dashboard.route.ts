import {Route} from '@angular/router';

import {UserRouteAccessService} from 'app/core/auth/user-route-access-service';
import {UserDashboardComponent} from './user-dashboard.component';

export const USER_DASHBOARD_ROUTE: Route = {
  path: 'user-dashboard',
  component: UserDashboardComponent,
  data: {
    authorities: ['ROLE_ORG_NODAL','ROLE_ORG_USER'],
    pageTitle: 'user-dashboard.title'
  },
  canActivate: [UserRouteAccessService]
};
