import {Component, OnInit} from '@angular/core';
import {UserDetailsService} from 'app/entities/user-details//user-details.service';
import {IUserDetails} from "app/shared/model/user-details.model";
import {Observable} from "rxjs";
import {HttpResponse} from "@angular/common/http";
import {IOrganisationMaster} from "app/shared/model/organisation-master.model";
import {AccountService} from "app/core/auth/account.service";
import {User} from "app/core/user/user.model";
import {DesignationMasterService} from "app/entities/designation-master/designation-master.service";
import {OrganisationMasterService} from "app/entities/organisation-master/organisation-master.service";
import {IDesignationMaster} from "app/shared/model/designation-master.model";
import {VerifyUserService} from "app/entities/user-details/verify-user-service";

@Component({
  selector: 'jhi-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: [
    'user-dashboard.component.scss'
  ]
})
export class UserDashboardComponent implements OnInit {
  userDetails: IUserDetails[];
  userDetail: IUserDetails[];

  message: string;
  remarks: any;
  isSaving: boolean;
  currentAccount: User;
  userLenght: any;
  allUserVerify: Array<any> = [];
  userData: Array<any> = [];
  desinationMasters: IDesignationMaster;
  organisationMasters: IOrganisationMaster;
  loadOrganisationMasters: IOrganisationMaster;
  signerResponse: any;
  signerId: any;
  accountStatus: any;

  constructor(
    protected userDetailsService: UserDetailsService,
    protected accountService: AccountService,
    protected designationMasterService: DesignationMasterService,
    protected organisationMasterService: OrganisationMasterService,
    protected verifyUserService: VerifyUserService
  ) {
    this.message = 'UserDashboardComponent message';

  }

  approveAllPopup(userDetails) {
    this.userLenght = userDetails.length;
    this.userDetail = userDetails;
  }

  // approveAllUser(userDetail){
  //   this.userLenght = userDetail.length;
  //   userDetail.forEach((userDetail,index) => {
  //     userDetail.verifiedBy = this.currentAccount.login;
  //     userDetail.remark = this.remarks;
  //     userDetail.status = 'Approved';
  //
  //     this.subscribeToSaveResponse(this.userDetailsService.update(userDetail));
  //   });
  //
  // }

  approvePopup(userDetail) {
    this.userDetail = userDetail;
  }

  rejectPopup(userDetail) {
    this.userDetail = userDetail;
  }
  accountPopup(userDetail) {
    this.userDetail = userDetail;
  }

  approveUser(userDetail) {
    userDetail.verifiedBy = this.currentAccount.login;
    userDetail.remark = this.remarks;
    userDetail.lastAction = 'Create';
    this.signerId = "1234";
    // this.subscribeToSaveResponse(this.userDetailsService.update(userDetail));
    this.verifyUserService.verifyUser(userDetail.userIdCode, userDetail.lastAction).subscribe((response3) => {
      this.organisationMasters = response3.body;
      if (this.organisationMasters === 'Success') {
        alert('Your Request has been verified');
        window.location.reload();
      } else {
        alert(this.organisationMasters);
      }
      console.log("responseeeeeeeeeeeeeeeeee", this.organisationMasters);
    });

  }

  rejectUser(userDetail) {
    userDetail.verifiedBy = this.currentAccount.login;
    userDetail.remark = this.remarks;
    userDetail.status = 'Rejected';
   this.userDetailsService.update(userDetail);
  }

  previousState() {
    // window.history.back();

  }

  ngOnInit() {
    this.accountService.identity().subscribe(account => {
      this.currentAccount = account;
    });

    this.userDetailsService.findUserByUserLogin().subscribe((response) => {
      this.userDetails = response.body;

      this.organisationMasterService.find(this.userDetails[0].organisationMasterId).subscribe((response1) => {
        this.loadOrganisationMasters = response1.body;
      });

      // for (let i = 0 ; i<=this.userDetails.length; i++){
      //   this.designationMasterService.find(this.userDetails[i].designationMasterId).subscribe((response1) => {
      //     this.desinationMasters = response1.body;
      //     this.userData.push(this.desinationMasters);
      //
      //     console.log("uuuuuuuuu", this.userData);
      //   })
      // }


    });
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserDetails>>) {
    result.subscribe((res: HttpResponse<any>) => this.onSaveSuccess(res.body), () => this.onSaveError());
  }

  protected onSaveSuccess(result) {
    this.isSaving = false;
    alert("Your Request has been Submitted Successfully");
    window.location.reload();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  saveAccountAction(userDetail,accountStatus) {
    userDetail.verifiedBy = this.currentAccount.login;
    userDetail.remark = this.remarks;
    userDetail.lastAction = 'Account';
    this.signerId = "7357695886@m.csc";
    // this.subscribeToSaveResponse(this.userDetailsService.update(userDetail));
    this.verifyUserService.signerAccount(userDetail.userIdCode, userDetail.lastAction, this.signerId, accountStatus).subscribe((response3) => {
      this.organisationMasters = response3.body;
      if (this.organisationMasters === 'Success') {
        alert('Your Request has been verified');
        window.location.reload();
      } else {
        alert(this.organisationMasters);
      }
      console.log("responseeeeeeeeeeeeeeeeee", this.organisationMasters);
    });
  }

  updateSigner(userDetail) {
    userDetail.verifiedBy = this.currentAccount.login;
    userDetail.remark = this.remarks;
    userDetail.lastAction = 'Update';
    this.signerId = "7357695886@m.csc";
    // this.subscribeToSaveResponse(this.userDetailsService.update(userDetail));
    this.verifyUserService.updateAndStatus(userDetail.userIdCode, userDetail.lastAction, this.signerId).subscribe((response3) => {
      this.organisationMasters = response3.body;
      if (this.organisationMasters === 'Success') {
        alert('Your Request has been updated');
        window.location.reload();
      } else {
        alert(this.organisationMasters);
      }
      console.log("responseeeeeeeeeeeeeeeeee", this.organisationMasters);
    });
  }

  signerStatus(userDetail) {
    userDetail.verifiedBy = this.currentAccount.login;
    userDetail.remark = this.remarks;
    userDetail.lastAction = 'Status';
    this.signerId = "9999888822@m.csc";
    // this.subscribeToSaveResponse(this.userDetailsService.update(userDetail));
    this.verifyUserService.updateAndStatus(userDetail.userIdCode, userDetail.lastAction, this.signerId).subscribe((response3) => {
      this.organisationMasters = response3.body;
      if (this.organisationMasters === 'Success') {
        alert('Your Request has been verified');
        window.location.reload();
      } else {
        alert(this.organisationMasters);
      }
      console.log("responseeeeeeeeeeeeeeeeee", this.organisationMasters);
    });
  }
}
