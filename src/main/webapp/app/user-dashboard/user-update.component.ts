import {Component, OnInit} from '@angular/core';
import {IDesignationMaster} from "app/shared/model/designation-master.model";
import {IOrganisationMaster} from "app/shared/model/organisation-master.model";
import {IEsignRole} from "app/shared/model/esign-role.model";
import {IApplicationMaster} from "app/shared/model/application-master.model";
import {FormBuilder, Validators} from "@angular/forms";
import {JhiAlertService} from "ng-jhipster";
import {UserDetailsService} from "app/entities/user-details/user-details.service";
import {DesignationMasterService} from "app/entities/designation-master/designation-master.service";
import {OrganisationMasterService} from "app/entities/organisation-master/organisation-master.service";
import {EsignRoleService} from "app/entities/esign-role/esign-role.service";
import {ApplicationMasterService} from "app/entities/application-master/application-master.service";
import {ActivatedRoute, Route} from "@angular/router";
import {HttpErrorResponse, HttpResponse} from "@angular/common/http";
import {IUserDetails, UserDetails} from "app/shared/model/user-details.model";
import {Observable} from "rxjs";
import { Router } from '@angular/router';
import {UserApplicationService} from "app/entities/user-application/user-application.service";
import {IUserApplication} from "app/shared/model/user-application.model";
@Component({
  selector: 'jhi-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: [
    'user-update.component.scss'
  ]
})
export class UserUpdateComponent implements OnInit {
  isSaving: boolean;
  designationmasters: IDesignationMaster[];
  organisationmasters: IOrganisationMaster[];
  esignroles: IEsignRole[];
  applicationmasters: IApplicationMaster[];
  createdOnDp: any;
  lastUpdatedOnDp: any;
  verifiedOnDp: any;
  message: string;

  editForm = this.fb.group({
    id: [],
    userFullName: [],
    userPhoneMobile: [null, [Validators.required]],
    userPhoneLandline: [],
    userOfficialEmail: [null, [Validators.required]],
    userAlternativeEmail: [],
    userLoginPassword: [],
    userLoginPasswordSalt: [],
    userTransactionPassword: [],
    userTransactionPasswordSalt: [],
    createdBy: [],
    createdOn: [],
    lastUpdatedBy: [],
    lastUpdatedOn: [],
    verifiedBy: [],
    verifiedOn: [],
    remarks: [],
    userIdCode: [null, []],
    status: [],
    designationMasterId: [],
    organisationMasterId: [],
    esignRoleId: [],
    applicationMasterId: [],
    userPIN: [],
    organisationUnit: [],
    address: [],
    counrty: [],
    postalCode: [],
    photograph: [],
    dob: [],
    gender: [],
    pan: [],
    aadhaar: [],
    panAttachment: []

  });
   userDetailId: any;
   userLogin: any;
  userAppData: Array<any> = [];
  userApplications: IUserApplication[] = [];
  applicationMasters: IApplicationMaster[];
  applicationMasterss: IApplicationMaster[];
  arrayFile: number[] = [];
  userApplicationSave:IUserApplication;
  userDetailResponse: IUserDetails;
  userIdCode: string;
  applicationIdCode: string;

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected userDetailsService: UserDetailsService,
    protected designationMasterService: DesignationMasterService,
    protected organisationMasterService: OrganisationMasterService,
    protected esignRoleService: EsignRoleService,
    protected applicationMasterService: ApplicationMasterService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private userApplicationService: UserApplicationService,

  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.userDetailId = params['id'];


      this.applicationMasterService.findApplicationByLoginAndStatus().subscribe((response) => {
        this.applicationMasters = response.body;

      });
      this.userApplicationService.findApplicationByUserId(this.userDetailId).subscribe((response1) => {
        this.userApplications = response1.body;

      });
    });

    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ userDetails }) => {
      this.updateForm(userDetails);
    });
    this.designationMasterService
      .query()
      .subscribe(
        (res: HttpResponse<IDesignationMaster[]>) => (this.designationmasters = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.organisationMasterService
      .query()
      .subscribe(
        (res: HttpResponse<IOrganisationMaster[]>) => (this.organisationmasters = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.esignRoleService
      .query()
      .subscribe((res: HttpResponse<IEsignRole[]>) => (this.esignroles = res.body), (res: HttpErrorResponse) => this.onError(res.message));
    this.applicationMasterService
      .query()
      .subscribe(
        (res: HttpResponse<IApplicationMaster[]>) => (this.applicationmasters = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  isAssigned(id: number): boolean {
    const filteredList = this.userApplications.filter(x => x.applicationMasterId === id);
    return filteredList && filteredList.length > 0;
  }

  updateForm(userDetails: IUserDetails) {
    this.editForm.patchValue({
      id: userDetails.id,
      userFullName: userDetails.userFullName,
      userPhoneMobile: userDetails.userPhoneMobile,
      userPhoneLandline: userDetails.userPhoneLandline,
      userOfficialEmail: userDetails.userOfficialEmail,
      userAlternativeEmail: userDetails.userAlternativeEmail,
      userLoginPassword: userDetails.userLoginPassword,
      userLoginPasswordSalt: userDetails.userLoginPasswordSalt,
      userTransactionPassword: userDetails.userTransactionPassword,
      userTransactionPasswordSalt: userDetails.userTransactionPasswordSalt,
      createdBy: userDetails.createdBy,
      createdOn: userDetails.createdOn,
      lastUpdatedBy: userDetails.lastUpdatedBy,
      lastUpdatedOn: userDetails.lastUpdatedOn,
      verifiedBy: userDetails.verifiedBy,
      verifiedOn: userDetails.verifiedOn,
      remarks: userDetails.remarks,
      userIdCode: userDetails.userIdCode,
      status: userDetails.status,
      designationMasterId: userDetails.designationMasterId,
      organisationMasterId: userDetails.organisationMasterId,
      esignRoleId: userDetails.esignRoleId,
      applicationMasterId: userDetails.applicationMasterId
    });
  }

  previousState() {
    window.history.back();
  }

  checkApplication(appID, isChecked: boolean) {
    if (isChecked) {
      alert(appID);
      const userDetailsUpdate = this.createFromForm();
      userDetailsUpdate.applicationMasterId = appID;
      this.subscribeToUpdateResponse(this.userDetailsService.update(userDetailsUpdate));
    } else {
      const filteredList = this.userApplications.filter(x => x.applicationMasterId === appID);
      if(filteredList.length>0) {
        this.userApplicationService.delete(filteredList[0].id).subscribe(() => {
        });
      }
    }
  }
  save() {
    this.isSaving = true;
    const userDetails = this.createFromForm();

    userDetails.applicationMasterId = this.arrayFile[0];

      this.subscribeToSaveResponse(this.userDetailsService.update(userDetails));

  }

  private createFromForm(): IUserDetails {
    return {
      ...new UserDetails(),
      id: this.editForm.get(['id']).value,
      userFullName: this.editForm.get(['userFullName']).value,
      userPhoneMobile: this.editForm.get(['userPhoneMobile']).value,
      userPhoneLandline: this.editForm.get(['userPhoneLandline']).value,
      userOfficialEmail: this.editForm.get(['userOfficialEmail']).value,
      userAlternativeEmail: this.editForm.get(['userAlternativeEmail']).value,
      userLoginPassword: this.editForm.get(['userLoginPassword']).value,
      userLoginPasswordSalt: this.editForm.get(['userLoginPasswordSalt']).value,
      userTransactionPassword: this.editForm.get(['userTransactionPassword']).value,
      userTransactionPasswordSalt: this.editForm.get(['userTransactionPasswordSalt']).value,
      createdBy: this.editForm.get(['createdBy']).value,
      createdOn: this.editForm.get(['createdOn']).value,
      lastUpdatedBy: this.editForm.get(['lastUpdatedBy']).value,
      lastUpdatedOn: this.editForm.get(['lastUpdatedOn']).value,
      verifiedBy: this.editForm.get(['verifiedBy']).value,
      verifiedOn: this.editForm.get(['verifiedOn']).value,
      remarks: this.editForm.get(['remarks']).value,
      userIdCode: this.editForm.get(['userIdCode']).value,
      status: this.editForm.get(['status']).value,
      designationMasterId: this.editForm.get(['designationMasterId']).value,
      organisationMasterId: this.editForm.get(['organisationMasterId']).value,
      esignRoleId: this.editForm.get(['esignRoleId']).value,
      applicationMasterId: this.editForm.get(['applicationMasterId']).value,
      counrty: this.editForm.get(['counrty']).value,
      userPIN: this.editForm.get(['userPIN']).value,
      postalCode: this.editForm.get(['postalCode']).value,
      photograph: this.editForm.get(['photograph']).value,
      dob: this.editForm.get(['dob']).value,
      gender: this.editForm.get(['gender']).value,
      pan: this.editForm.get(['pan']).value,
      aadhaar: this.editForm.get(['aadhaar']).value,
      panAttachment: this.editForm.get(['panAttachment']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserDetails>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected subscribeToUpdateResponse(result: Observable<HttpResponse<IUserDetails>>) {
    result.subscribe(() => this.onUpdateSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    alert("User Updated Successfully");
    this.previousState();
  }

  protected onUpdateSuccess() {
    this.isSaving = false;

  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

}
