import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { UserUpdateComponent } from './user-update.component';
import {UserDetailsResolve} from "app/entities/user-details/user-details.route";

export const USER_UPDATE_ROUTE: Route = {
  path: 'user-update/:id/edit',
  component: UserUpdateComponent,
  resolve: {
    userDetails: UserDetailsResolve
  },
  data: {
    authorities: [],
    pageTitle: 'user-update.title'
  },
  canActivate: [UserRouteAccessService]
};
