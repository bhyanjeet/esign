import{Component, OnInit}from '@angular/core';
import {FormBuilder, Validators}from "@angular/forms";
import {IDesignationMaster} from "app/shared/model/designation-master.model";
import {IUserDetails, UserDetails} from "app/shared/model/user-details.model";
import {UserDetailsService}from "app/entities/user-details/user-details.service";
import {Observable}from "rxjs";
import {HttpErrorResponse, HttpResponse}from "@angular/common/http";
import {IApplicationMaster}from "app/shared/model/application-master.model";
import {ApplicationMasterService}from "app/entities/application-master/application-master.service";
import {ActivatedRoute, Router}from "@angular/router";
import {DesignationMasterService}from "app/entities/designation-master/designation-master.service";
import {JhiAlertService}from "ng-jhipster";
import {MinioUploadService}from "../shared/Services/minioUpload.service";
import {CheckInputService}from "app/shared/Services/checkInput.service";

@Component({
selector: 'jhi-user',
templateUrl: './user.component.html',
styleUrls: [
'user.component.scss'
]
})
export class UserComponent implements OnInit {
isSaving: boolean;

userdetails: IUserDetails[];
image: File;
imageName: any;
pan: File;
panFileName: any;
attachment: any;
emp: File;
empFileName: any;
getUserDetails: any;
userDetailsId: any;
errorEmailExists: string;
errorPhoneNoExists: string;
error: string;
message: string;
designationMasterId: any = null;

editForm = this.fb.group({
id: [],
userFullName: [],
userPhoneMobile: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
    userPhoneLandline: [],
    userOfficialEmail: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    userAlternativeEmail: [],
    userLoginPassword: [],
    userLoginPasswordSalt: [],
    userTransactionPassword: [],
    userTransactionPasswordSalt: [],
    createdBy: [],
    createdOn: [],
    lastUpdatedBy: [],
    lastUpdatedOn: [],
    verifiedBy: [],
    verifiedOn: [],
    remarks: [],
    userPIN: [],
    organisationUnit: [],
    address: [],
    counrty: [],
    postalCode: [],
    photograph: [],
    dob: [],
    gender: [],
    pan: [],
    aadhaar: [],
    designationMasterId: [],
    organisationMasterId: [],
    esignRoleId: [],
    applicationMasterId: [],
    panAttachment: [],
    employeeCardAttachment: [],
    employeeId: []
  });
  applicationMasters: IApplicationMaster[];
  applcationId: any;
  arrayFile: number[] = [];
  designationmasters: IDesignationMaster[];

  constructor(
    protected applicationMasterService: ApplicationMasterService,
    protected userDetailsService: UserDetailsService,
    protected activatedRoute: ActivatedRoute,
    private route: ActivatedRoute,
    protected designationMasterService: DesignationMasterService,
    protected jhiAlertService: JhiAlertService,
    private router: Router,
    private fb: FormBuilder,
    private minioUploadService: MinioUploadService,
    private checkInputService: CheckInputService
  ) {
    this.message = 'UserComponent message';
  }

  ngOnInit() {

    // const jqueryVariable = this;
    // $('input').change(function() {
    //   jqueryVariable.checkInputService.checkV($(this));
    // });
    // $('textarea').change(function() {
    //   jqueryVariable.checkInputService.checkV($(this));
    // });

    this.isSaving = false;
    this.activatedRoute.data.subscribe(({userDetails}) => {
      this.updateForm(userDetails);
    });

    this.route.params.subscribe(params => {
      this.userDetailsId = params['id'];
    });

    this.userDetailsService.find(this.userDetailsId).subscribe(response => {
      this.getUserDetails = response.body;
    });

    this.applicationMasterService.findApplicationByLoginAndStatus().subscribe((res1: HttpResponse<IApplicationMaster[]>) => {
      this.applicationMasters = res1.body;
    });

    this.designationMasterService
      .query()
      .subscribe(
        (res: HttpResponse<IDesignationMaster[]>) => (this.designationmasters = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  previousState() {
    window.history.back();
  }

  // checkApplication(appID, isChecked: boolean){
  //   let selectedApp: number = appID;
  //   if (isChecked){
  //     this.arrayFile.push(selectedApp);
  //     selectedApp = null;
  //   } else {
  //     this.arrayFile.pop();
  //     selectedApp = null;
  //   }
  // }

  save() {
    this.isSaving = true;
    const userDetails = this.createFromForm();
    userDetails.applicationMasterId = this.applcationId;
    if (this.panFileName || this.imageName || this.empFileName) {
      const filesArray = [];
      if (this.panFileName) {
        filesArray.push({file: this.pan, type: 'pan'});
      }
      if (this.imageName) {
        filesArray.push({file: this.image, type: 'photograph'});
      }
      if (this.empFileName) {
        filesArray.push({file: this.emp, type: 'empidCard'});
      }
      this.minioUploadService.createMultipleFiles(filesArray).subscribe(res => {
        const file = res.body;
        userDetails.photograph = file.photoGraphAttachmentName;
        userDetails.employeeCardAttachment = file.empIdCardAttachmentName;
        userDetails.panAttachment = file.panAttachmentName;
        this.saveuserDetails(userDetails);
      });
    } else {
      this.saveuserDetails(userDetails)
    }
  }

  saveuserDetails(userDetails) {
    if (userDetails.id !== undefined) {
      this.subscribeToSaveResponse(this.userDetailsService.update(userDetails));
    } else {
      this.subscribeToSaveResponse(this.userDetailsService.create(userDetails));
    }
  }

  updateForm(userDetails: IUserDetails) {
    this.editForm.patchValue({
      id: userDetails.id,
      userFullName: userDetails.userFullName,
      userPhoneMobile: userDetails.userPhoneMobile,
      userPhoneLandline: userDetails.userPhoneLandline,
      userOfficialEmail: userDetails.userOfficialEmail,
      userAlternativeEmail: userDetails.userAlternativeEmail,
      userLoginPassword: userDetails.userLoginPassword,
      userLoginPasswordSalt: userDetails.userLoginPasswordSalt,
      userTransactionPassword: userDetails.userTransactionPassword,
      userTransactionPasswordSalt: userDetails.userTransactionPasswordSalt,
      createdBy: userDetails.createdBy,
      createdOn: userDetails.createdOn,
      lastUpdatedBy: userDetails.lastUpdatedBy,
      lastUpdatedOn: userDetails.lastUpdatedOn,
      verifiedBy: userDetails.verifiedBy,
      verifiedOn: userDetails.verifiedOn,
      remarks: userDetails.remarks,
      userIdCode: userDetails.userIdCode,
      status: userDetails.status,
      designationMasterId: userDetails.designationMasterId,
      organisationMasterId: userDetails.organisationMasterId,
      esignRoleId: userDetails.esignRoleId,
      applicationMasterId: userDetails.applicationMasterId,
      employeeId: userDetails.employeeId,
      gender: userDetails.gender,
      pan: userDetails.pan,
      aadhaar: userDetails.aadhaar,
      dob: userDetails.dob
    });
  }

  private createFromForm(): IUserDetails {
    return {
      ...new UserDetails(),
      id: this.editForm.get(['id']).value,
      userFullName: this.editForm.get(['userFullName']).value,
      userPhoneMobile: this.editForm.get(['userPhoneMobile']).value,
      userPhoneLandline: this.editForm.get(['userPhoneLandline']).value,
      userOfficialEmail: this.editForm.get(['userOfficialEmail']).value,
      userAlternativeEmail: this.editForm.get(['userAlternativeEmail']).value,
      userLoginPassword: this.editForm.get(['userLoginPassword']).value,
      userLoginPasswordSalt: this.editForm.get(['userLoginPasswordSalt']).value,
      userTransactionPassword: this.editForm.get(['userTransactionPassword']).value,
      userTransactionPasswordSalt: this.editForm.get(['userTransactionPasswordSalt']).value,
      createdBy: this.editForm.get(['createdBy']).value,
      createdOn: this.editForm.get(['createdOn']).value,
      lastUpdatedBy: this.editForm.get(['lastUpdatedBy']).value,
      lastUpdatedOn: this.editForm.get(['lastUpdatedOn']).value,
      verifiedBy: this.editForm.get(['verifiedBy']).value,
      verifiedOn: this.editForm.get(['verifiedOn']).value,
      remarks: this.editForm.get(['remarks']).value,
      designationMasterId: this.editForm.get(['designationMasterId']).value,
      organisationMasterId: this.editForm.get(['organisationMasterId']).value,
      esignRoleId: this.editForm.get(['esignRoleId']).value,
      applicationMasterId: this.editForm.get(['applicationMasterId']).value,
      counrty: this.editForm.get(['counrty']).value,
      userPIN: this.editForm.get(['userPIN']).value,
      postalCode: this.editForm.get(['postalCode']).value,
      photograph: this.editForm.get(['photograph']).value,
      dob: this.editForm.get(['dob']).value,
      gender: this.editForm.get(['gender']).value,
      pan: this.editForm.get(['pan']).value,
      aadhaar: this.editForm.get(['aadhaar']).value,
      panAttachment: this.editForm.get(['panAttachment']).value,
      employeeCardAttachment: this.editForm.get(['employeeCardAttachment']).value,
      employeeId: this.editForm.get(['employeeId']).value

    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserDetails>>) {
    result.subscribe(res => this.onSaveSuccess(res), response => this.onSaveError(response));
    return result;
  }

  protected onSaveSuccess(result) {
    this.isSaving = false;
    alert("User has been created successfully");
    this.router.navigate(['user-dashboard']);
  }

  protected onSaveError(response) {
    this.isSaving = false;
    alert(response.error.title);
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  uploadFile(fileData, fileType) {
    if (fileType === 'panAttachment') {
      this.pan = fileData.item(0);
      const panAttachmentName = fileData.item(0).name;
      const panAttachmentLength =  panAttachmentName.split(".");

      if (panAttachmentLength.length>2){
        alert("Invalid File Format. File should be like abc.jpg!");
        return;
      }
      const  panfileType = panAttachmentLength[1];

      if (panfileType === "jpg" || panfileType === "JPG" ){
        this.panFileName = this.pan.name;
      }
      else {
        alert("Oops! Incorrect File Type. Please select only JPG/jpg file");
        return null;
      }


    } else if (fileType === 'employeeCardAttachment') {
      this.emp = fileData.item(0);
      const panAttachmentName = fileData.item(0).name;

      const panAttachmentLength =  panAttachmentName.split(".");

      if (panAttachmentLength.length>2){
        alert("Invalid File Format. File should be like abc.jpg!");
        return null;
      }
      const  panfileType = panAttachmentLength[1];

      if (panfileType === "jpg" || panfileType === "JPG" ){
        this.empFileName = this.emp.name;
      }
      else {
        alert("Oops! Incorrect File Type. Please select only JPG/jpg file");
        return null;
      }



    } else {
      this.image = fileData.item(0);
      const panAttachmentName = fileData.item(0).name;
      const panAttachmentLength =  panAttachmentName.split(".");

      if (panAttachmentLength.length>2){
        alert("Invalid File Format. File should be like abc.jpg!");
        return null;
      }
      const  panfileType = panAttachmentLength[1];

      if (panfileType === "jpg" || panfileType === "JPG"){
        this.imageName = this.image.name;
      }
      else {
        alert("Oops! Incorrect File Type. Please select only JPG/jpg file");
        return null;
      }
    }
  }
}
