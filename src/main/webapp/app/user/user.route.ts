import {ActivatedRouteSnapshot, Resolve, Route, Routes} from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { UserComponent } from './user.component';
import {IUserDetails, UserDetails} from "../shared/model/user-details.model";
import {Observable, of} from "rxjs/index";
import {HttpResponse} from "@angular/common/http";
import {UserDetailsService} from "../entities/user-details/user-details.service";
import {map} from "rxjs/internal/operators";
import {Injectable} from "@angular/core";

@Injectable({ providedIn: 'root' })
export class UserDetailsResolve implements Resolve<IUserDetails> {
  constructor(private service: UserDetailsService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUserDetails> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((userDetails: HttpResponse<UserDetails>) => userDetails.body));
    }
    return of(new UserDetails());
  }
}

export const USER_ROUTE: Routes = [
  {
    path: 'user',
    component: UserComponent,
    data: {
      authorities: ['ROLE_ORG_NODAL'],
      pageTitle: 'user.title'
    },
    resolve: {
      userDetails: UserDetailsResolve
    },
    canActivate: [UserRouteAccessService]
  },

  {
    path: 'user/:id/edit',
    component: UserComponent,
    data: {
      authorities: ['ROLE_ORG_NODAL','ROLE_ORG_USER'],
      pageTitle: 'user.title'
    },
    resolve: {
      userDetails: UserDetailsResolve
    },
    canActivate: [UserRouteAccessService]
  }
];
