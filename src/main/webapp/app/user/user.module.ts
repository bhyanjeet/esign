import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';

import { USER_ROUTE, UserComponent } from './';

@NgModule({
    imports: [
      EsignharyanaSharedModule,
      RouterModule.forChild( USER_ROUTE )
    ],
    declarations: [
      UserComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppUserModule {}
