import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {ExternalUserEsignRequestService} from "app/entities/external-user-esign-request/external-user-esign-request.service";
import {IEsignFormRequest} from "app/shared/model/esign-form-request.model";
import {IExternalUserEsignRequest} from "app/shared/model/external-user-esign-request.model";
import {UserDetailsService} from "app/entities/user-details/user-details.service";
import {IUserDetails} from "app/shared/model/user-details.model";
import {EsignTransation, IEsignTransation} from "app/shared/model/esign-transation.model";
import {FormBuilder} from "@angular/forms";
import {EsignTransationService} from "app/entities/esign-transation/esign-transation.service";

@Component({
  selector: 'jhi-esign-final-form-request',
  templateUrl: './esign-final-form-request.component.html',
  styleUrls: [
    'esign-final-form-request.component.scss'
  ]
})
export class EsignFinalFormRequestComponent implements OnInit {


  message: string;
  aspIp: string;
  aspId: string;
  txnref: string;
  reqData: string;
  externalUserDetail: any;
  txn: any
  userCodeId: string;
  userDetails: IUserDetails;
  docUrl: string;
  applicationIdCode: string;
  esignTransaction:IEsignTransation

  constructor(
    protected externalUserEsignRequestService: ExternalUserEsignRequestService,
    protected userDetailsService: UserDetailsService,
    protected esignTransationService: EsignTransationService,

    private route: ActivatedRoute,
    protected  http: HttpClient,
    private fb: FormBuilder
  ) {
    this.message = 'EsignFinalFormRequestComponent message';
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.txn = params.get("txn");

      // this.externalUserEsignRequestService.findByTxnRef(this.txnref).subscribe((response) => {
      //   this.externalUserDetail = response.body;
      //   console.log("uuuuuuuuuuu", this.externalUserDetail);
      // });

      this.externalUserEsignRequestService.findByTxn(this.txn).subscribe((res: HttpResponse<IExternalUserEsignRequest>) => {
        this.externalUserDetail = res.body;
        this.aspId = res.body.aspId;
        this.aspIp = res.body.aspIp;
        this.reqData = res.body.refData;
        this.txnref = res.body.txnRef;
        this.userCodeId = res.body.userCodeId;
        this.applicationIdCode = res.body.applicationIdCode;
        this.docUrl = res.body.docUrl;

        console.log("uuuu" + this.externalUserDetail);

        if (this.userCodeId!==""){
          this.userDetailsService.finByUserCodeId(this.userCodeId).subscribe((res1:HttpResponse<IUserDetails>) => {
            this.userDetails  = res1.body;
            console.log(this.userDetails);

          })
        }
      });
    });
  }

  save() {
    const esignTransation = this.createFromForm();

    console.log(esignTransation);
    this.esignTransationService.saveEsignTransaction(esignTransation).subscribe();
  }


  private createFromForm(): IEsignTransation {
    return {
      ...new EsignTransation(),
      userCodeId:this.userCodeId,
      applicationCodeId:this.applicationIdCode,
      reqData:this.reqData

      }
    }

}
