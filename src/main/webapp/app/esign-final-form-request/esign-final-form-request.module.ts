import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from '../shared/shared.module';

import { ESIGN_FINAL_FORM_REQUEST_ROUTE, EsignFinalFormRequestComponent } from './';

@NgModule({
  imports: [
    EsignharyanaSharedModule,
    RouterModule.forRoot([ ESIGN_FINAL_FORM_REQUEST_ROUTE ], { useHash: true })
  ],
  declarations: [
    EsignFinalFormRequestComponent,
  ],
  entryComponents: [
  ],
  providers: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppEsignFinalFormRequestModule {}
