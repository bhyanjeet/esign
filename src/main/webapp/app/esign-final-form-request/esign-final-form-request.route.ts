import { Route } from '@angular/router';

import { UserRouteAccessService } from '../core/auth/user-route-access-service';
import { EsignFinalFormRequestComponent } from './esign-final-form-request.component';

export const ESIGN_FINAL_FORM_REQUEST_ROUTE: Route = {
  path: 'esign-final-form-request/:txn',
  //path: 'esign-final-form-request/:aspid/:asp_ip/:txnref/:req_data',
  component: EsignFinalFormRequestComponent,
  data: {
    authorities: [],
    pageTitle: 'esign-final-form-request.title'
  },
};
