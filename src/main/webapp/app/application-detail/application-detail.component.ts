import {Component, OnInit} from '@angular/core';
import {ApplicationMasterService} from "app/entities/application-master/application-master.service";
import {ActivatedRoute} from "@angular/router";
import {IApplicationMaster} from "app/shared/model/application-master.model";

@Component({
  selector: 'jhi-application-detail',
  templateUrl: './application-detail.component.html',
  styleUrls: [
    'application-detail.component.scss'
  ]
})
export class ApplicationDetailComponent implements OnInit {

  message: string;
  applicationMaster: IApplicationMaster;

  constructor(
    protected applicationMasterService: ApplicationMasterService,
    protected activatedRoute: ActivatedRoute,
  ) {
    this.message = 'ApplicationDetailComponent message';
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ applicationMaster }) => {
      this.applicationMaster = applicationMaster;
    });

  }

}
