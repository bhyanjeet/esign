import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ApplicationDetailComponent } from './application-detail.component';
import {ApplicationMasterResolve} from "app/entities/application-master/application-master.route";

export const APPLICATION_DETAIL_ROUTE: Route = {
  path: 'application-detail/:id/view',
  component: ApplicationDetailComponent,
  resolve: {
    applicationMaster: ApplicationMasterResolve
  },
  data: {
    authorities: [],
    pageTitle: 'application-detail.title'
  },
  canActivate: [UserRouteAccessService]
};
