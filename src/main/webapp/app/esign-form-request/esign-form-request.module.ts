import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from '../shared/shared.module';

import { ESIGN_FORM_REQUEST_ROUTE, EsignFormRequestComponent } from './';

@NgModule({
    imports: [
      EsignharyanaSharedModule,
      RouterModule.forRoot([ ESIGN_FORM_REQUEST_ROUTE ], { useHash: true })
    ],
    declarations: [
      EsignFormRequestComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppEsignFormRequestModule {}
