import {Component, OnInit} from '@angular/core';
import {IOrganisationMaster, OrganisationMaster} from "app/shared/model/organisation-master.model";
import {FormBuilder} from "@angular/forms";
import {ESignFormRequestService} from "app/entities/user-details/eSign-Form-Request-service";
import {EsignFormRequest, IEsignFormRequest} from "app/shared/model/esign-form-request.model";
import {HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {IUserDetails} from "app/shared/model/user-details.model";


@Component({
  selector: 'jhi-esign-form-request',
  templateUrl: './esign-form-request.component.html',
  styleUrls: [
    'esign-form-request.component.scss'
  ]
})
export class EsignFormRequestComponent implements OnInit {

  message: string;
  signerid: any;
  responseUrl: any;
  redirectUrl: any;
  docInfo: any;


  editForm = this.fb.group({
    signerid: [],
    responseUrl: [],
    redirectUrl: [],
    docInfo: [],
  });
  eSignFormRequest: any;
  isSaving: boolean;

  constructor(
    private fb: FormBuilder,
    protected eSignFormRequestService: ESignFormRequestService,
  ) {
    this.message = 'EsignFormRequestComponent message';
  }

  ngOnInit() {
    this.eSignFormRequest = "";
  }

  save() {
  //  alert("asdas");

    //this.eSignFormRequestService.sendRedirect().subscribe();

    const esignFormRequest = this.createFromForm();
    console.log("11ffffffffff", esignFormRequest);
    this.eSignFormRequestService.eSignRequest(esignFormRequest).subscribe((res: HttpResponse<IEsignFormRequest>) => {
      this.eSignFormRequest = res.body;
      console.log("ffffffffff", this.eSignFormRequest);

    });


  }


  private createFromForm(): IEsignFormRequest {
    return {
      ...new EsignFormRequest(),
      signerid: this.editForm.get(['signerid']).value,
      responseUrl: this.editForm.get(['responseUrl']).value,
      redirectUrl: this.editForm.get(['redirectUrl']).value,
      docInfo: this.editForm.get(['docInfo']).value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEsignFormRequest>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    //this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }


}
