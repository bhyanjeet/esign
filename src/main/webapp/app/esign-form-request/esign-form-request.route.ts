import { Route } from '@angular/router';

import { UserRouteAccessService } from '../core/auth/user-route-access-service';
import { EsignFormRequestComponent } from './esign-form-request.component';

export const ESIGN_FORM_REQUEST_ROUTE: Route = {
  path: 'esign-form-request',
  component: EsignFormRequestComponent,
  data: {
    authorities: [],
    pageTitle: 'esign-form-request.title'
  },
  canActivate: [UserRouteAccessService]
};
