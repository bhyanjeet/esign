import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EsignharyanaSharedModule } from 'app/shared/shared.module';

import { ASP_APPLICATION_DASHBOARD_ROUTE, AspApplicationDashboardComponent } from './';

@NgModule({
    imports: [
      EsignharyanaSharedModule,
      RouterModule.forRoot([ ASP_APPLICATION_DASHBOARD_ROUTE ], { useHash: true })
    ],
    declarations: [
      AspApplicationDashboardComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EsignharyanaAppAspApplicationDashboardModule {}
