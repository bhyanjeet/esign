import {Component, OnInit} from '@angular/core';
import {HttpHeaders, HttpResponse} from "@angular/common/http";
import {IApplicationMaster} from "app/shared/model/application-master.model";
import {Observable, Subscription} from "rxjs";
import {ApplicationMasterService} from "app/entities/application-master/application-master.service";
import {JhiEventManager, JhiParseLinks} from "ng-jhipster";
import {ActivatedRoute, Router} from "@angular/router";
import {IOrganisationMaster} from "app/shared/model/organisation-master.model";
import {AccountService} from "app/core/auth/account.service";
import { User } from '../core/user/user.model';
import {ApplicationLogsService} from "app/entities/application-logs/application-logs.service";

@Component({
  selector: 'jhi-asp-application-dashboard',
  templateUrl: './asp-application-dashboard.component.html',
  styleUrls: [
    'asp-application-dashboard.component.scss'
  ]
})
export class AspApplicationDashboardComponent implements OnInit {
  applicationMasters: IApplicationMaster[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  currentSearch: User;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;
  message: string;
  status: any;
  pendingLength: IApplicationMaster[];
  approvedLength: IApplicationMaster[];
  rejectedLength: IApplicationMaster[];
  isSaving: boolean;
  remarks: any;
  currentAccount: any;
  applicationMaster: IApplicationMaster[];
  applicationLogs: any = {};



  constructor(
  protected applicationMasterService: ApplicationMasterService,
  protected applicationLogsService: ApplicationLogsService,
  protected parseLinks: JhiParseLinks,
  protected activatedRoute: ActivatedRoute,
  protected router: Router,
  protected eventManager: JhiEventManager,
  protected accountService: AccountService,
  ) {
    this.itemsPerPage = 5;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    this.message = 'AspApplicationDashboardComponent message';
  });
}


  ngOnInit() {
    this.accountService.identity().subscribe(account => {
      this.currentAccount = account;
    });

    this.loadAll();

    this.applicationMasterService
      .getAllApplicationByStatus('Pending')
      .subscribe((res1: HttpResponse<IOrganisationMaster[]>) => {
        this.pendingLength = res1.body;
      });
    this.applicationMasterService
      .getAllApplicationByStatus('Approved')
      .subscribe((res1: HttpResponse<IOrganisationMaster[]>) => {
        this.approvedLength = res1.body;
      });

    this.applicationMasterService
      .getAllApplicationByStatus('Rejected')
      .subscribe((res1: HttpResponse<IOrganisationMaster[]>) => {
        this.rejectedLength = res1.body;
      });
  }

  previousState() {
    window.history.back();
  }


  getApplicationBystatus(status) {
    this.status = status;
      this.applicationMasterService
        .getAllApplicationByStatus(this.status)
        .subscribe((res: HttpResponse<IApplicationMaster[]>) => {
          this.applicationMasters = res.body;
    });
  }


  loadAll() {
    if (this.currentSearch) {
      this.applicationMasterService
        .search({
          page: this.page - 1,
          query: this.currentSearch,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<IApplicationMaster[]>) => this.paginateApplicationMasters(res.body, res.headers));
      return;
    }
    this.applicationMasterService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<IApplicationMaster[]>) => this.paginateApplicationMasters(res.body, res.headers));
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/application-master'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        search: this.currentSearch,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  approvePopup(applicationMaster){
    this.applicationMaster = applicationMaster;

  }

  rejectPopup(applicationMaster){
    this.applicationMaster = applicationMaster;

  }

  approveApplication(applicationMaster){
    applicationMaster.verifiedBy = this.currentAccount.login;
    applicationMaster.remark = this.remarks;
    applicationMaster.status = 'Approved';
    this.subscribeToSaveResponse(this.applicationMasterService.update(applicationMaster));
  }

  rejectApplication(applicationMaster){
    applicationMaster.verifiedBy = this.currentAccount.login;
    applicationMaster.remark = this.remarks;
    applicationMaster.status = 'Rejected';
    this.subscribeToSaveResponse(this.applicationMasterService.update(applicationMaster));
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateApplicationMasters(data: IApplicationMaster[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.applicationMasters = data;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IApplicationMaster>>) {
    result.subscribe((res: HttpResponse<any>) => this.onSaveSuccess(res.body), () => this.onSaveError());
  }

  protected onSaveSuccess(result) {
    this.isSaving = false;
    alert("Application Confirmation Request Submitted Successfully");
    window.location.reload();
  }

  protected onSaveError() {
    this.isSaving = false;
    this.loadAll();
  }

}
