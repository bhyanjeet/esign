import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { AspApplicationDashboardComponent } from './asp-application-dashboard.component';
import {JhiResolvePagingParams} from "ng-jhipster";

export const ASP_APPLICATION_DASHBOARD_ROUTE: Route = {
  path: 'asp-application-dashboard',
  component: AspApplicationDashboardComponent,
  resolve: {
    pagingParams: JhiResolvePagingParams
  },
  data: {
    authorities: ['ROLE_ASP_NODAL'],
    defaultSort: 'id,asc',
    pageTitle: 'ApplicationMasters'
  },
  canActivate: [UserRouteAccessService]
};
