package com.hartron.esignharyana.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserApplication.class)
public abstract class UserApplication_ {

	public static volatile SingularAttribute<UserApplication, String> userLogin;
	public static volatile SingularAttribute<UserApplication, ApplicationMaster> applicationMaster;
	public static volatile SingularAttribute<UserApplication, String> updatedBy;
	public static volatile SingularAttribute<UserApplication, LocalDate> verifiedOn;
	public static volatile SingularAttribute<UserApplication, String> createdBy;
	public static volatile SingularAttribute<UserApplication, String> applicationCodeId;
	public static volatile SingularAttribute<UserApplication, String> userCodeId;
	public static volatile SingularAttribute<UserApplication, Long> id;
	public static volatile SingularAttribute<UserApplication, LocalDate> updatedOn;
	public static volatile SingularAttribute<UserApplication, String> verifiedBy;
	public static volatile SingularAttribute<UserApplication, Long> userId;
	public static volatile SingularAttribute<UserApplication, LocalDate> createdOn;

	public static final String USER_LOGIN = "userLogin";
	public static final String APPLICATION_MASTER = "applicationMaster";
	public static final String UPDATED_BY = "updatedBy";
	public static final String VERIFIED_ON = "verifiedOn";
	public static final String CREATED_BY = "createdBy";
	public static final String APPLICATION_CODE_ID = "applicationCodeId";
	public static final String USER_CODE_ID = "userCodeId";
	public static final String ID = "id";
	public static final String UPDATED_ON = "updatedOn";
	public static final String VERIFIED_BY = "verifiedBy";
	public static final String USER_ID = "userId";
	public static final String CREATED_ON = "createdOn";

}

