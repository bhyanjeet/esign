package com.hartron.esignharyana.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserDetails.class)
public abstract class UserDetails_ {

	public static volatile SingularAttribute<UserDetails, String> userLoginPassword;
	public static volatile SingularAttribute<UserDetails, String> gender;
	public static volatile SingularAttribute<UserDetails, String> organisationUnit;
	public static volatile SingularAttribute<UserDetails, String> counrty;
	public static volatile SingularAttribute<UserDetails, String> postalCode;
	public static volatile SingularAttribute<UserDetails, String> employeeCardAttachment;
	public static volatile SingularAttribute<UserDetails, DesignationMaster> designationMaster;
	public static volatile SingularAttribute<UserDetails, String> userTransactionPassword;
	public static volatile SingularAttribute<UserDetails, LocalDate> createdOn;
	public static volatile SingularAttribute<UserDetails, String> userIdCode;
	public static volatile SingularAttribute<UserDetails, OrganisationMaster> organisationMaster;
	public static volatile SingularAttribute<UserDetails, String> panAttachment;
	public static volatile SingularAttribute<UserDetails, String> accountStatus;
	public static volatile SingularAttribute<UserDetails, String> userPhoneMobile;
	public static volatile SingularAttribute<UserDetails, String> userAlternativeEmail;
	public static volatile SingularAttribute<UserDetails, String> userLoginPasswordSalt;
	public static volatile SingularAttribute<UserDetails, LocalDate> verifiedOn;
	public static volatile SingularAttribute<UserDetails, String> lastAction;
	public static volatile SingularAttribute<UserDetails, LocalDate> lastUpdatedOn;
	public static volatile SingularAttribute<UserDetails, String> aadhaar;
	public static volatile SingularAttribute<UserDetails, Long> id;
	public static volatile SingularAttribute<UserDetails, String> pan;
	public static volatile SingularAttribute<UserDetails, String> lastUpdatedBy;
	public static volatile SingularAttribute<UserDetails, String> userTransactionPasswordSalt;
	public static volatile SingularAttribute<UserDetails, ApplicationMaster> applicationMaster;
	public static volatile SingularAttribute<UserDetails, String> address;
	public static volatile SingularAttribute<UserDetails, String> userFullName;
	public static volatile SingularAttribute<UserDetails, String> userOfficialEmail;
	public static volatile SingularAttribute<UserDetails, String> employeeId;
	public static volatile SingularAttribute<UserDetails, String> verifiedBy;
	public static volatile SingularAttribute<UserDetails, String> userPIN;
	public static volatile SingularAttribute<UserDetails, String> photograph;
	public static volatile SingularAttribute<UserDetails, String> userPhoneLandline;
	public static volatile SingularAttribute<UserDetails, String> signerId;
	public static volatile SingularAttribute<UserDetails, String> createdBy;
	public static volatile SingularAttribute<UserDetails, EsignRole> esignRole;
	public static volatile SingularAttribute<UserDetails, LocalDate> dob;
	public static volatile SingularAttribute<UserDetails, String> remarks;
	public static volatile SingularAttribute<UserDetails, String> status;
	public static volatile SingularAttribute<UserDetails, String> username;

	public static final String USER_LOGIN_PASSWORD = "userLoginPassword";
	public static final String GENDER = "gender";
	public static final String ORGANISATION_UNIT = "organisationUnit";
	public static final String COUNRTY = "counrty";
	public static final String POSTAL_CODE = "postalCode";
	public static final String EMPLOYEE_CARD_ATTACHMENT = "employeeCardAttachment";
	public static final String DESIGNATION_MASTER = "designationMaster";
	public static final String USER_TRANSACTION_PASSWORD = "userTransactionPassword";
	public static final String CREATED_ON = "createdOn";
	public static final String USER_ID_CODE = "userIdCode";
	public static final String ORGANISATION_MASTER = "organisationMaster";
	public static final String PAN_ATTACHMENT = "panAttachment";
	public static final String ACCOUNT_STATUS = "accountStatus";
	public static final String USER_PHONE_MOBILE = "userPhoneMobile";
	public static final String USER_ALTERNATIVE_EMAIL = "userAlternativeEmail";
	public static final String USER_LOGIN_PASSWORD_SALT = "userLoginPasswordSalt";
	public static final String VERIFIED_ON = "verifiedOn";
	public static final String LAST_ACTION = "lastAction";
	public static final String LAST_UPDATED_ON = "lastUpdatedOn";
	public static final String AADHAAR = "aadhaar";
	public static final String ID = "id";
	public static final String PAN = "pan";
	public static final String LAST_UPDATED_BY = "lastUpdatedBy";
	public static final String USER_TRANSACTION_PASSWORD_SALT = "userTransactionPasswordSalt";
	public static final String APPLICATION_MASTER = "applicationMaster";
	public static final String ADDRESS = "address";
	public static final String USER_FULL_NAME = "userFullName";
	public static final String USER_OFFICIAL_EMAIL = "userOfficialEmail";
	public static final String EMPLOYEE_ID = "employeeId";
	public static final String VERIFIED_BY = "verifiedBy";
	public static final String USER_PI_N = "userPIN";
	public static final String PHOTOGRAPH = "photograph";
	public static final String USER_PHONE_LANDLINE = "userPhoneLandline";
	public static final String SIGNER_ID = "signerId";
	public static final String CREATED_BY = "createdBy";
	public static final String ESIGN_ROLE = "esignRole";
	public static final String DOB = "dob";
	public static final String REMARKS = "remarks";
	public static final String STATUS = "status";
	public static final String USERNAME = "username";

}

