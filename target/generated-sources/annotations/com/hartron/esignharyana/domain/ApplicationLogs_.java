package com.hartron.esignharyana.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ApplicationLogs.class)
public abstract class ApplicationLogs_ {

	public static volatile SingularAttribute<ApplicationLogs, String> actionTaken;
	public static volatile SingularAttribute<ApplicationLogs, String> actionTakenBy;
	public static volatile SingularAttribute<ApplicationLogs, LocalDate> actionTakenOnDate;
	public static volatile SingularAttribute<ApplicationLogs, Long> id;
	public static volatile SingularAttribute<ApplicationLogs, Long> actionTakenOnApplication;
	public static volatile SingularAttribute<ApplicationLogs, String> remarks;

	public static final String ACTION_TAKEN = "actionTaken";
	public static final String ACTION_TAKEN_BY = "actionTakenBy";
	public static final String ACTION_TAKEN_ON_DATE = "actionTakenOnDate";
	public static final String ID = "id";
	public static final String ACTION_TAKEN_ON_APPLICATION = "actionTakenOnApplication";
	public static final String REMARKS = "remarks";

}

