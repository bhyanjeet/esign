package com.hartron.esignharyana.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SubDistrictMaster.class)
public abstract class SubDistrictMaster_ {

	public static volatile SingularAttribute<SubDistrictMaster, String> subDistrictCode;
	public static volatile SingularAttribute<SubDistrictMaster, String> lastUpdatedBy;
	public static volatile SingularAttribute<SubDistrictMaster, StateMaster> stateMaster;
	public static volatile SingularAttribute<SubDistrictMaster, LocalDate> verifiedOn;
	public static volatile SingularAttribute<SubDistrictMaster, String> subDistrictName;
	public static volatile SingularAttribute<SubDistrictMaster, LocalDate> lastUpdatedOn;
	public static volatile SingularAttribute<SubDistrictMaster, Long> id;
	public static volatile SingularAttribute<SubDistrictMaster, String> verifiedBy;
	public static volatile SingularAttribute<SubDistrictMaster, DistrictMaster> districtMaster;
	public static volatile SingularAttribute<SubDistrictMaster, String> remarks;

	public static final String SUB_DISTRICT_CODE = "subDistrictCode";
	public static final String LAST_UPDATED_BY = "lastUpdatedBy";
	public static final String STATE_MASTER = "stateMaster";
	public static final String VERIFIED_ON = "verifiedOn";
	public static final String SUB_DISTRICT_NAME = "subDistrictName";
	public static final String LAST_UPDATED_ON = "lastUpdatedOn";
	public static final String ID = "id";
	public static final String VERIFIED_BY = "verifiedBy";
	public static final String DISTRICT_MASTER = "districtMaster";
	public static final String REMARKS = "remarks";

}

