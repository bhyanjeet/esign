package com.hartron.esignharyana.domain;

import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Otp.class)
public abstract class Otp_ {

	public static volatile SingularAttribute<Otp, ZonedDateTime> requestTime;
	public static volatile SingularAttribute<Otp, Long> mobileNumber;
	public static volatile SingularAttribute<Otp, Integer> otp;
	public static volatile SingularAttribute<Otp, Long> id;

	public static final String REQUEST_TIME = "requestTime";
	public static final String MOBILE_NUMBER = "mobileNumber";
	public static final String OTP = "otp";
	public static final String ID = "id";

}

