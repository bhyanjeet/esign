package com.hartron.esignharyana.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(OrganisationTypeMaster.class)
public abstract class OrganisationTypeMaster_ {

	public static volatile SingularAttribute<OrganisationTypeMaster, String> lastUpdatedBy;
	public static volatile SingularAttribute<OrganisationTypeMaster, LocalDate> verifiedOn;
	public static volatile SingularAttribute<OrganisationTypeMaster, String> createdBy;
	public static volatile SingularAttribute<OrganisationTypeMaster, String> organisationTypeAbbreviation;
	public static volatile SingularAttribute<OrganisationTypeMaster, String> organisationTypeDetail;
	public static volatile SingularAttribute<OrganisationTypeMaster, LocalDate> lastUpdatedOn;
	public static volatile SingularAttribute<OrganisationTypeMaster, Long> id;
	public static volatile SingularAttribute<OrganisationTypeMaster, String> verifiedBy;
	public static volatile SingularAttribute<OrganisationTypeMaster, LocalDate> createdOn;
	public static volatile SingularAttribute<OrganisationTypeMaster, String> remarks;

	public static final String LAST_UPDATED_BY = "lastUpdatedBy";
	public static final String VERIFIED_ON = "verifiedOn";
	public static final String CREATED_BY = "createdBy";
	public static final String ORGANISATION_TYPE_ABBREVIATION = "organisationTypeAbbreviation";
	public static final String ORGANISATION_TYPE_DETAIL = "organisationTypeDetail";
	public static final String LAST_UPDATED_ON = "lastUpdatedOn";
	public static final String ID = "id";
	public static final String VERIFIED_BY = "verifiedBy";
	public static final String CREATED_ON = "createdOn";
	public static final String REMARKS = "remarks";

}

