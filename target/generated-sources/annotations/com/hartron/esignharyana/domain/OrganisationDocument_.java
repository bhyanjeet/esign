package com.hartron.esignharyana.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(OrganisationDocument.class)
public abstract class OrganisationDocument_ {

	public static volatile SingularAttribute<OrganisationDocument, String> documentRelatedTo;
	public static volatile SingularAttribute<OrganisationDocument, LocalDate> verifiedOn;
	public static volatile SingularAttribute<OrganisationDocument, String> createdBy;
	public static volatile SingularAttribute<OrganisationDocument, OrganisationTypeMaster> organisationTypeMaster;
	public static volatile SingularAttribute<OrganisationDocument, Long> id;
	public static volatile SingularAttribute<OrganisationDocument, String> documentTitle;
	public static volatile SingularAttribute<OrganisationDocument, String> verifiedBy;
	public static volatile SingularAttribute<OrganisationDocument, LocalDate> createdOn;
	public static volatile SingularAttribute<OrganisationDocument, String> remarks;

	public static final String DOCUMENT_RELATED_TO = "documentRelatedTo";
	public static final String VERIFIED_ON = "verifiedOn";
	public static final String CREATED_BY = "createdBy";
	public static final String ORGANISATION_TYPE_MASTER = "organisationTypeMaster";
	public static final String ID = "id";
	public static final String DOCUMENT_TITLE = "documentTitle";
	public static final String VERIFIED_BY = "verifiedBy";
	public static final String CREATED_ON = "createdOn";
	public static final String REMARKS = "remarks";

}

