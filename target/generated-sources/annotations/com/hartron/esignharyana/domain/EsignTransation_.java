package com.hartron.esignharyana.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EsignTransation.class)
public abstract class EsignTransation_ {

	public static volatile SingularAttribute<EsignTransation, String> requestType;
	public static volatile SingularAttribute<EsignTransation, String> applicationCodeId;
	public static volatile SingularAttribute<EsignTransation, String> responseXml;
	public static volatile SingularAttribute<EsignTransation, String> responseStatus;
	public static volatile SingularAttribute<EsignTransation, LocalDate> responseDate;
	public static volatile SingularAttribute<EsignTransation, String> responseCode;
	public static volatile SingularAttribute<EsignTransation, String> orgStatus;
	public static volatile SingularAttribute<EsignTransation, String> responseType;
	public static volatile SingularAttribute<EsignTransation, String> signerId;
	public static volatile SingularAttribute<EsignTransation, String> organisationIdCode;
	public static volatile SingularAttribute<EsignTransation, String> userCodeId;
	public static volatile SingularAttribute<EsignTransation, String> orgResponseFailURL;
	public static volatile SingularAttribute<EsignTransation, LocalDate> requestDate;
	public static volatile SingularAttribute<EsignTransation, String> orgResponseSuccessURL;
	public static volatile SingularAttribute<EsignTransation, Long> id;
	public static volatile SingularAttribute<EsignTransation, String> requestStatus;
	public static volatile SingularAttribute<EsignTransation, String> requestXml;
	public static volatile SingularAttribute<EsignTransation, String> txnId;
	public static volatile SingularAttribute<EsignTransation, String> ts;

	public static final String REQUEST_TYPE = "requestType";
	public static final String APPLICATION_CODE_ID = "applicationCodeId";
	public static final String RESPONSE_XML = "responseXml";
	public static final String RESPONSE_STATUS = "responseStatus";
	public static final String RESPONSE_DATE = "responseDate";
	public static final String RESPONSE_CODE = "responseCode";
	public static final String ORG_STATUS = "orgStatus";
	public static final String RESPONSE_TYPE = "responseType";
	public static final String SIGNER_ID = "signerId";
	public static final String ORGANISATION_ID_CODE = "organisationIdCode";
	public static final String USER_CODE_ID = "userCodeId";
	public static final String ORG_RESPONSE_FAIL_UR_L = "orgResponseFailURL";
	public static final String REQUEST_DATE = "requestDate";
	public static final String ORG_RESPONSE_SUCCESS_UR_L = "orgResponseSuccessURL";
	public static final String ID = "id";
	public static final String REQUEST_STATUS = "requestStatus";
	public static final String REQUEST_XML = "requestXml";
	public static final String TXN_ID = "txnId";
	public static final String TS = "ts";

}

