package com.hartron.esignharyana.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EsignError.class)
public abstract class EsignError_ {

	public static volatile SingularAttribute<EsignError, String> errorMessage;
	public static volatile SingularAttribute<EsignError, String> errorCode;
	public static volatile SingularAttribute<EsignError, Long> id;
	public static volatile SingularAttribute<EsignError, String> errorStage;

	public static final String ERROR_MESSAGE = "errorMessage";
	public static final String ERROR_CODE = "errorCode";
	public static final String ID = "id";
	public static final String ERROR_STAGE = "errorStage";

}

