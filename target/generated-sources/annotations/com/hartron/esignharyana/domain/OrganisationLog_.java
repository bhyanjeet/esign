package com.hartron.esignharyana.domain;

import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(OrganisationLog.class)
public abstract class OrganisationLog_ {

	public static volatile SingularAttribute<OrganisationLog, String> verifyByName;
	public static volatile SingularAttribute<OrganisationLog, ZonedDateTime> verifyDate;
	public static volatile SingularAttribute<OrganisationLog, String> createdByName;
	public static volatile SingularAttribute<OrganisationLog, Long> organisationId;
	public static volatile SingularAttribute<OrganisationLog, ZonedDateTime> createdDate;
	public static volatile SingularAttribute<OrganisationLog, String> verifyByLogin;
	public static volatile SingularAttribute<OrganisationLog, Long> id;
	public static volatile SingularAttribute<OrganisationLog, String> createdByLogin;
	public static volatile SingularAttribute<OrganisationLog, String> remarks;
	public static volatile SingularAttribute<OrganisationLog, String> status;

	public static final String VERIFY_BY_NAME = "verifyByName";
	public static final String VERIFY_DATE = "verifyDate";
	public static final String CREATED_BY_NAME = "createdByName";
	public static final String ORGANISATION_ID = "organisationId";
	public static final String CREATED_DATE = "createdDate";
	public static final String VERIFY_BY_LOGIN = "verifyByLogin";
	public static final String ID = "id";
	public static final String CREATED_BY_LOGIN = "createdByLogin";
	public static final String REMARKS = "remarks";
	public static final String STATUS = "status";

}

