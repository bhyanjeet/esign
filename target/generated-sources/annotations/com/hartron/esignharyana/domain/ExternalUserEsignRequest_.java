package com.hartron.esignharyana.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ExternalUserEsignRequest.class)
public abstract class ExternalUserEsignRequest_ {

	public static volatile SingularAttribute<ExternalUserEsignRequest, String> redirectUrl;
	public static volatile SingularAttribute<ExternalUserEsignRequest, String> docHash;
	public static volatile SingularAttribute<ExternalUserEsignRequest, String> applicationIdCode;
	public static volatile SingularAttribute<ExternalUserEsignRequest, String> docInfo;
	public static volatile SingularAttribute<ExternalUserEsignRequest, String> txnRef;
	public static volatile SingularAttribute<ExternalUserEsignRequest, String> txn;
	public static volatile SingularAttribute<ExternalUserEsignRequest, String> requestTime;
	public static volatile SingularAttribute<ExternalUserEsignRequest, String> responseUrl;
	public static volatile SingularAttribute<ExternalUserEsignRequest, String> signerId;
	public static volatile SingularAttribute<ExternalUserEsignRequest, String> userCodeId;
	public static volatile SingularAttribute<ExternalUserEsignRequest, String> docUrl;
	public static volatile SingularAttribute<ExternalUserEsignRequest, Long> id;
	public static volatile SingularAttribute<ExternalUserEsignRequest, String> requestStatus;
	public static volatile SingularAttribute<ExternalUserEsignRequest, String> ts;

	public static final String REDIRECT_URL = "redirectUrl";
	public static final String DOC_HASH = "docHash";
	public static final String APPLICATION_ID_CODE = "applicationIdCode";
	public static final String DOC_INFO = "docInfo";
	public static final String TXN_REF = "txnRef";
	public static final String TXN = "txn";
	public static final String REQUEST_TIME = "requestTime";
	public static final String RESPONSE_URL = "responseUrl";
	public static final String SIGNER_ID = "signerId";
	public static final String USER_CODE_ID = "userCodeId";
	public static final String DOC_URL = "docUrl";
	public static final String ID = "id";
	public static final String REQUEST_STATUS = "requestStatus";
	public static final String TS = "ts";

}

