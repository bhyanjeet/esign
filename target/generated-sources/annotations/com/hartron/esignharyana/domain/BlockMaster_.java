package com.hartron.esignharyana.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BlockMaster.class)
public abstract class BlockMaster_ {

	public static volatile SingularAttribute<BlockMaster, String> lastUpdatedBy;
	public static volatile SingularAttribute<BlockMaster, String> blockCode;
	public static volatile SingularAttribute<BlockMaster, StateMaster> stateMaster;
	public static volatile SingularAttribute<BlockMaster, LocalDate> verifiedOn;
	public static volatile SingularAttribute<BlockMaster, String> blockName;
	public static volatile SingularAttribute<BlockMaster, LocalDate> lastUpdatedOn;
	public static volatile SingularAttribute<BlockMaster, Long> id;
	public static volatile SingularAttribute<BlockMaster, String> verifiedBy;
	public static volatile SingularAttribute<BlockMaster, LocalDate> createdOn;
	public static volatile SingularAttribute<BlockMaster, DistrictMaster> districtMaster;
	public static volatile SingularAttribute<BlockMaster, String> remarks;

	public static final String LAST_UPDATED_BY = "lastUpdatedBy";
	public static final String BLOCK_CODE = "blockCode";
	public static final String STATE_MASTER = "stateMaster";
	public static final String VERIFIED_ON = "verifiedOn";
	public static final String BLOCK_NAME = "blockName";
	public static final String LAST_UPDATED_ON = "lastUpdatedOn";
	public static final String ID = "id";
	public static final String VERIFIED_BY = "verifiedBy";
	public static final String CREATED_ON = "createdOn";
	public static final String DISTRICT_MASTER = "districtMaster";
	public static final String REMARKS = "remarks";

}

