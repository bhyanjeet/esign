package com.hartron.esignharyana.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DesignationMaster.class)
public abstract class DesignationMaster_ {

	public static volatile SingularAttribute<DesignationMaster, String> designationName;
	public static volatile SingularAttribute<DesignationMaster, String> lastUpdatedBy;
	public static volatile SingularAttribute<DesignationMaster, LocalDate> verifiedOn;
	public static volatile SingularAttribute<DesignationMaster, String> createdBy;
	public static volatile SingularAttribute<DesignationMaster, LocalDate> lastUpdatedOn;
	public static volatile SingularAttribute<DesignationMaster, String> designationAbbreviation;
	public static volatile SingularAttribute<DesignationMaster, Long> id;
	public static volatile SingularAttribute<DesignationMaster, String> verifiedBy;
	public static volatile SingularAttribute<DesignationMaster, LocalDate> createdOn;
	public static volatile SingularAttribute<DesignationMaster, String> remarks;

	public static final String DESIGNATION_NAME = "designationName";
	public static final String LAST_UPDATED_BY = "lastUpdatedBy";
	public static final String VERIFIED_ON = "verifiedOn";
	public static final String CREATED_BY = "createdBy";
	public static final String LAST_UPDATED_ON = "lastUpdatedOn";
	public static final String DESIGNATION_ABBREVIATION = "designationAbbreviation";
	public static final String ID = "id";
	public static final String VERIFIED_BY = "verifiedBy";
	public static final String CREATED_ON = "createdOn";
	public static final String REMARKS = "remarks";

}

