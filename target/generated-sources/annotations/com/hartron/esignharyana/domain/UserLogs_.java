package com.hartron.esignharyana.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserLogs.class)
public abstract class UserLogs_ {

	public static volatile SingularAttribute<UserLogs, String> actionTaken;
	public static volatile SingularAttribute<UserLogs, String> actionTakenBy;
	public static volatile SingularAttribute<UserLogs, LocalDate> actionTakenOnDate;
	public static volatile SingularAttribute<UserLogs, Long> actionTakenOnUser;
	public static volatile SingularAttribute<UserLogs, Long> id;
	public static volatile SingularAttribute<UserLogs, String> remarks;

	public static final String ACTION_TAKEN = "actionTaken";
	public static final String ACTION_TAKEN_BY = "actionTakenBy";
	public static final String ACTION_TAKEN_ON_DATE = "actionTakenOnDate";
	public static final String ACTION_TAKEN_ON_USER = "actionTakenOnUser";
	public static final String ID = "id";
	public static final String REMARKS = "remarks";

}

