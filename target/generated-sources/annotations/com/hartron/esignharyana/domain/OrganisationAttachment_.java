package com.hartron.esignharyana.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(OrganisationAttachment.class)
public abstract class OrganisationAttachment_ {

	public static volatile SingularAttribute<OrganisationAttachment, String> attachment;
	public static volatile SingularAttribute<OrganisationAttachment, OrganisationDocument> organisationDocument;
	public static volatile SingularAttribute<OrganisationAttachment, Long> id;
	public static volatile SingularAttribute<OrganisationAttachment, OrganisationMaster> organisationMaster;

	public static final String ATTACHMENT = "attachment";
	public static final String ORGANISATION_DOCUMENT = "organisationDocument";
	public static final String ID = "id";
	public static final String ORGANISATION_MASTER = "organisationMaster";

}

