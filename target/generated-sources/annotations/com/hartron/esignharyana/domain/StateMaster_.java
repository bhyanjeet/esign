package com.hartron.esignharyana.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(StateMaster.class)
public abstract class StateMaster_ {

	public static volatile SingularAttribute<StateMaster, String> lastUpdatedBy;
	public static volatile SingularAttribute<StateMaster, LocalDate> verifiedOn;
	public static volatile SingularAttribute<StateMaster, String> stateName;
	public static volatile SingularAttribute<StateMaster, LocalDate> lastUpdatedOn;
	public static volatile SingularAttribute<StateMaster, String> stateCode;
	public static volatile SingularAttribute<StateMaster, Long> id;
	public static volatile SingularAttribute<StateMaster, String> verifiedBy;
	public static volatile SingularAttribute<StateMaster, LocalDate> createdOn;
	public static volatile SingularAttribute<StateMaster, String> remarks;

	public static final String LAST_UPDATED_BY = "lastUpdatedBy";
	public static final String VERIFIED_ON = "verifiedOn";
	public static final String STATE_NAME = "stateName";
	public static final String LAST_UPDATED_ON = "lastUpdatedOn";
	public static final String STATE_CODE = "stateCode";
	public static final String ID = "id";
	public static final String VERIFIED_BY = "verifiedBy";
	public static final String CREATED_ON = "createdOn";
	public static final String REMARKS = "remarks";

}

