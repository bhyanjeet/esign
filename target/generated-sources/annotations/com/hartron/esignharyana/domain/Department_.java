package com.hartron.esignharyana.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Department.class)
public abstract class Department_ {

	public static volatile SingularAttribute<Department, String> departmentName;
	public static volatile SingularAttribute<Department, String> whiteListIp2;
	public static volatile SingularAttribute<Department, String> updatedBy;
	public static volatile SingularAttribute<Department, String> createdBy;
	public static volatile SingularAttribute<Department, String> whiteListIp1;
	public static volatile SingularAttribute<Department, String> remark;
	public static volatile SingularAttribute<Department, Long> id;
	public static volatile SingularAttribute<Department, LocalDate> updatedOn;
	public static volatile SingularAttribute<Department, LocalDate> createdOn;
	public static volatile SingularAttribute<Department, String> status;

	public static final String DEPARTMENT_NAME = "departmentName";
	public static final String WHITE_LIST_IP2 = "whiteListIp2";
	public static final String UPDATED_BY = "updatedBy";
	public static final String CREATED_BY = "createdBy";
	public static final String WHITE_LIST_IP1 = "whiteListIp1";
	public static final String REMARK = "remark";
	public static final String ID = "id";
	public static final String UPDATED_ON = "updatedOn";
	public static final String CREATED_ON = "createdOn";
	public static final String STATUS = "status";

}

