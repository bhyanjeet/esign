package com.hartron.esignharyana.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(OrganisationMaster.class)
public abstract class OrganisationMaster_ {

	public static volatile SingularAttribute<OrganisationMaster, String> organisationNodalOfficerName;
	public static volatile SingularAttribute<OrganisationMaster, String> organisationCorrespondenceAddress;
	public static volatile SingularAttribute<OrganisationMaster, String> nodalOfficerOfficialEmail;
	public static volatile SingularAttribute<OrganisationMaster, String> gender;
	public static volatile SingularAttribute<OrganisationMaster, String> photographAttachment;
	public static volatile SingularAttribute<OrganisationMaster, String> organisationEmployeeId;
	public static volatile SingularAttribute<OrganisationMaster, String> orgnisationEmpolyeeCardAttchment;
	public static volatile SingularAttribute<OrganisationMaster, LocalDate> createdOn;
	public static volatile SingularAttribute<OrganisationMaster, String> organisationNodalOfficerAlternativeEmail;
	public static volatile SingularAttribute<OrganisationMaster, String> panAttachment;
	public static volatile SingularAttribute<OrganisationMaster, StateMaster> stateMaster;
	public static volatile SingularAttribute<OrganisationMaster, LocalDate> verifiedOn;
	public static volatile SingularAttribute<OrganisationMaster, LocalDate> lastUpdatedOn;
	public static volatile SingularAttribute<OrganisationMaster, String> aadhaar;
	public static volatile SingularAttribute<OrganisationMaster, Long> id;
	public static volatile SingularAttribute<OrganisationMaster, String> organisationCorrespondenceEmail;
	public static volatile SingularAttribute<OrganisationMaster, String> organisationAbbreviation;
	public static volatile SingularAttribute<OrganisationMaster, String> pan;
	public static volatile SingularAttribute<OrganisationMaster, SubDistrictMaster> subDistrictMaster;
	public static volatile SingularAttribute<OrganisationMaster, Department> department;
	public static volatile SingularAttribute<OrganisationMaster, String> organisationWebsite;
	public static volatile SingularAttribute<OrganisationMaster, String> organisationNodalOfficerPhoneMobile;
	public static volatile SingularAttribute<OrganisationMaster, String> lastUpdatedBy;
	public static volatile SingularAttribute<OrganisationMaster, LocalDate> dateOfBirth;
	public static volatile SingularAttribute<OrganisationMaster, String> verifiedBy;
	public static volatile SingularAttribute<OrganisationMaster, String> organisationNodalOfficerPhoneLandline;
	public static volatile SingularAttribute<OrganisationMaster, Integer> userId;
	public static volatile SingularAttribute<OrganisationMaster, String> organisationCorrespondencePhone1;
	public static volatile SingularAttribute<OrganisationMaster, String> organisationName;
	public static volatile SingularAttribute<OrganisationMaster, String> stage;
	public static volatile SingularAttribute<OrganisationMaster, String> createdBy;
	public static volatile SingularAttribute<OrganisationMaster, String> organisationIdCode;
	public static volatile SingularAttribute<OrganisationMaster, String> aadhaarAttachment;
	public static volatile SingularAttribute<OrganisationMaster, String> pinCode;
	public static volatile SingularAttribute<OrganisationMaster, OrganisationTypeMaster> organisationTypeMaster;
	public static volatile SingularAttribute<OrganisationMaster, DistrictMaster> districtMaster;
	public static volatile SingularAttribute<OrganisationMaster, String> remarks;
	public static volatile SingularAttribute<OrganisationMaster, String> organisationCorrespondencePhone2;
	public static volatile SingularAttribute<OrganisationMaster, String> status;

	public static final String ORGANISATION_NODAL_OFFICER_NAME = "organisationNodalOfficerName";
	public static final String ORGANISATION_CORRESPONDENCE_ADDRESS = "organisationCorrespondenceAddress";
	public static final String NODAL_OFFICER_OFFICIAL_EMAIL = "nodalOfficerOfficialEmail";
	public static final String GENDER = "gender";
	public static final String PHOTOGRAPH_ATTACHMENT = "photographAttachment";
	public static final String ORGANISATION_EMPLOYEE_ID = "organisationEmployeeId";
	public static final String ORGNISATION_EMPOLYEE_CARD_ATTCHMENT = "orgnisationEmpolyeeCardAttchment";
	public static final String CREATED_ON = "createdOn";
	public static final String ORGANISATION_NODAL_OFFICER_ALTERNATIVE_EMAIL = "organisationNodalOfficerAlternativeEmail";
	public static final String PAN_ATTACHMENT = "panAttachment";
	public static final String STATE_MASTER = "stateMaster";
	public static final String VERIFIED_ON = "verifiedOn";
	public static final String LAST_UPDATED_ON = "lastUpdatedOn";
	public static final String AADHAAR = "aadhaar";
	public static final String ID = "id";
	public static final String ORGANISATION_CORRESPONDENCE_EMAIL = "organisationCorrespondenceEmail";
	public static final String ORGANISATION_ABBREVIATION = "organisationAbbreviation";
	public static final String PAN = "pan";
	public static final String SUB_DISTRICT_MASTER = "subDistrictMaster";
	public static final String DEPARTMENT = "department";
	public static final String ORGANISATION_WEBSITE = "organisationWebsite";
	public static final String ORGANISATION_NODAL_OFFICER_PHONE_MOBILE = "organisationNodalOfficerPhoneMobile";
	public static final String LAST_UPDATED_BY = "lastUpdatedBy";
	public static final String DATE_OF_BIRTH = "dateOfBirth";
	public static final String VERIFIED_BY = "verifiedBy";
	public static final String ORGANISATION_NODAL_OFFICER_PHONE_LANDLINE = "organisationNodalOfficerPhoneLandline";
	public static final String USER_ID = "userId";
	public static final String ORGANISATION_CORRESPONDENCE_PHONE1 = "organisationCorrespondencePhone1";
	public static final String ORGANISATION_NAME = "organisationName";
	public static final String STAGE = "stage";
	public static final String CREATED_BY = "createdBy";
	public static final String ORGANISATION_ID_CODE = "organisationIdCode";
	public static final String AADHAAR_ATTACHMENT = "aadhaarAttachment";
	public static final String PIN_CODE = "pinCode";
	public static final String ORGANISATION_TYPE_MASTER = "organisationTypeMaster";
	public static final String DISTRICT_MASTER = "districtMaster";
	public static final String REMARKS = "remarks";
	public static final String ORGANISATION_CORRESPONDENCE_PHONE2 = "organisationCorrespondencePhone2";
	public static final String STATUS = "status";

}

