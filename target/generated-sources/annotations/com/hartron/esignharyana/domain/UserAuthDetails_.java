package com.hartron.esignharyana.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserAuthDetails.class)
public abstract class UserAuthDetails_ {

	public static volatile SingularAttribute<UserAuthDetails, String> authToken;
	public static volatile SingularAttribute<UserAuthDetails, Long> id;
	public static volatile SingularAttribute<UserAuthDetails, String> login;
	public static volatile SingularAttribute<UserAuthDetails, String> device;

	public static final String AUTH_TOKEN = "authToken";
	public static final String ID = "id";
	public static final String LOGIN = "login";
	public static final String DEVICE = "device";

}

