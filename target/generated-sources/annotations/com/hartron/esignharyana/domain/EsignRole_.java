package com.hartron.esignharyana.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EsignRole.class)
public abstract class EsignRole_ {

	public static volatile SingularAttribute<EsignRole, String> lastUpdatedBy;
	public static volatile SingularAttribute<EsignRole, String> eSignRoleDetail;
	public static volatile SingularAttribute<EsignRole, LocalDate> verifiedOn;
	public static volatile SingularAttribute<EsignRole, String> createdBy;
	public static volatile SingularAttribute<EsignRole, LocalDate> lastUpdatedOn;
	public static volatile SingularAttribute<EsignRole, Long> id;
	public static volatile SingularAttribute<EsignRole, String> verifiedBy;
	public static volatile SingularAttribute<EsignRole, LocalDate> createdOn;
	public static volatile SingularAttribute<EsignRole, String> remarks;

	public static final String LAST_UPDATED_BY = "lastUpdatedBy";
	public static final String E_SIGN_ROLE_DETAIL = "eSignRoleDetail";
	public static final String VERIFIED_ON = "verifiedOn";
	public static final String CREATED_BY = "createdBy";
	public static final String LAST_UPDATED_ON = "lastUpdatedOn";
	public static final String ID = "id";
	public static final String VERIFIED_BY = "verifiedBy";
	public static final String CREATED_ON = "createdOn";
	public static final String REMARKS = "remarks";

}

