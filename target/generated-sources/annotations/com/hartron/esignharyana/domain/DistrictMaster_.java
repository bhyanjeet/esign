package com.hartron.esignharyana.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DistrictMaster.class)
public abstract class DistrictMaster_ {

	public static volatile SingularAttribute<DistrictMaster, String> lastUpdatedBy;
	public static volatile SingularAttribute<DistrictMaster, StateMaster> stateMaster;
	public static volatile SingularAttribute<DistrictMaster, String> districtCode;
	public static volatile SingularAttribute<DistrictMaster, String> districtName;
	public static volatile SingularAttribute<DistrictMaster, LocalDate> verifiedOn;
	public static volatile SingularAttribute<DistrictMaster, LocalDate> lastUpdatedOn;
	public static volatile SingularAttribute<DistrictMaster, Long> id;
	public static volatile SingularAttribute<DistrictMaster, String> verifiedBy;
	public static volatile SingularAttribute<DistrictMaster, LocalDate> createdOn;
	public static volatile SingularAttribute<DistrictMaster, String> remarks;

	public static final String LAST_UPDATED_BY = "lastUpdatedBy";
	public static final String STATE_MASTER = "stateMaster";
	public static final String DISTRICT_CODE = "districtCode";
	public static final String DISTRICT_NAME = "districtName";
	public static final String VERIFIED_ON = "verifiedOn";
	public static final String LAST_UPDATED_ON = "lastUpdatedOn";
	public static final String ID = "id";
	public static final String VERIFIED_BY = "verifiedBy";
	public static final String CREATED_ON = "createdOn";
	public static final String REMARKS = "remarks";

}

