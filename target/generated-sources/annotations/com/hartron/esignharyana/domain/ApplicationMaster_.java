package com.hartron.esignharyana.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ApplicationMaster.class)
public abstract class ApplicationMaster_ {

	public static volatile SingularAttribute<ApplicationMaster, String> lastUpdatedBy;
	public static volatile SingularAttribute<ApplicationMaster, String> certPublicKey;
	public static volatile SingularAttribute<ApplicationMaster, String> applicationIdCode;
	public static volatile SingularAttribute<ApplicationMaster, String> verifiedBy;
	public static volatile SingularAttribute<ApplicationMaster, LocalDate> createdOn;
	public static volatile SingularAttribute<ApplicationMaster, OrganisationMaster> organisationMaster;
	public static volatile SingularAttribute<ApplicationMaster, String> applicationStatus;
	public static volatile SingularAttribute<ApplicationMaster, LocalDate> verifiedOn;
	public static volatile SingularAttribute<ApplicationMaster, String> createdBy;
	public static volatile SingularAttribute<ApplicationMaster, String> applicationURL;
	public static volatile SingularAttribute<ApplicationMaster, LocalDate> lastUpdatedOn;
	public static volatile SingularAttribute<ApplicationMaster, Long> id;
	public static volatile SingularAttribute<ApplicationMaster, String> applicationName;
	public static volatile SingularAttribute<ApplicationMaster, String> remarks;
	public static volatile SingularAttribute<ApplicationMaster, String> status;

	public static final String LAST_UPDATED_BY = "lastUpdatedBy";
	public static final String CERT_PUBLIC_KEY = "certPublicKey";
	public static final String APPLICATION_ID_CODE = "applicationIdCode";
	public static final String VERIFIED_BY = "verifiedBy";
	public static final String CREATED_ON = "createdOn";
	public static final String ORGANISATION_MASTER = "organisationMaster";
	public static final String APPLICATION_STATUS = "applicationStatus";
	public static final String VERIFIED_ON = "verifiedOn";
	public static final String CREATED_BY = "createdBy";
	public static final String APPLICATION_UR_L = "applicationURL";
	public static final String LAST_UPDATED_ON = "lastUpdatedOn";
	public static final String ID = "id";
	public static final String APPLICATION_NAME = "applicationName";
	public static final String REMARKS = "remarks";
	public static final String STATUS = "status";

}

