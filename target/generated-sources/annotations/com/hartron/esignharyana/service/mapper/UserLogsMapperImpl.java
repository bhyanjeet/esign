package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.UserLogs;
import com.hartron.esignharyana.service.dto.UserLogsDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class UserLogsMapperImpl implements UserLogsMapper {

    @Override
    public UserLogs toEntity(UserLogsDTO dto) {
        if ( dto == null ) {
            return null;
        }

        UserLogs userLogs = new UserLogs();

        userLogs.setId( dto.getId() );
        userLogs.setActionTaken( dto.getActionTaken() );
        userLogs.setActionTakenBy( dto.getActionTakenBy() );
        userLogs.setActionTakenOnUser( dto.getActionTakenOnUser() );
        userLogs.setActionTakenOnDate( dto.getActionTakenOnDate() );
        userLogs.setRemarks( dto.getRemarks() );

        return userLogs;
    }

    @Override
    public UserLogsDTO toDto(UserLogs entity) {
        if ( entity == null ) {
            return null;
        }

        UserLogsDTO userLogsDTO = new UserLogsDTO();

        userLogsDTO.setId( entity.getId() );
        userLogsDTO.setActionTaken( entity.getActionTaken() );
        userLogsDTO.setActionTakenBy( entity.getActionTakenBy() );
        userLogsDTO.setActionTakenOnUser( entity.getActionTakenOnUser() );
        userLogsDTO.setActionTakenOnDate( entity.getActionTakenOnDate() );
        userLogsDTO.setRemarks( entity.getRemarks() );

        return userLogsDTO;
    }

    @Override
    public List<UserLogs> toEntity(List<UserLogsDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<UserLogs> list = new ArrayList<UserLogs>( dtoList.size() );
        for ( UserLogsDTO userLogsDTO : dtoList ) {
            list.add( toEntity( userLogsDTO ) );
        }

        return list;
    }

    @Override
    public List<UserLogsDTO> toDto(List<UserLogs> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<UserLogsDTO> list = new ArrayList<UserLogsDTO>( entityList.size() );
        for ( UserLogs userLogs : entityList ) {
            list.add( toDto( userLogs ) );
        }

        return list;
    }
}
