package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.Otp;
import com.hartron.esignharyana.service.dto.OtpDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class OtpMapperImpl implements OtpMapper {

    @Override
    public Otp toEntity(OtpDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Otp otp = new Otp();

        otp.setId( dto.getId() );
        otp.setOtp( dto.getOtp() );
        otp.setMobileNumber( dto.getMobileNumber() );
        otp.setRequestTime( dto.getRequestTime() );

        return otp;
    }

    @Override
    public OtpDTO toDto(Otp entity) {
        if ( entity == null ) {
            return null;
        }

        OtpDTO otpDTO = new OtpDTO();

        otpDTO.setId( entity.getId() );
        otpDTO.setOtp( entity.getOtp() );
        otpDTO.setMobileNumber( entity.getMobileNumber() );
        otpDTO.setRequestTime( entity.getRequestTime() );

        return otpDTO;
    }

    @Override
    public List<Otp> toEntity(List<OtpDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Otp> list = new ArrayList<Otp>( dtoList.size() );
        for ( OtpDTO otpDTO : dtoList ) {
            list.add( toEntity( otpDTO ) );
        }

        return list;
    }

    @Override
    public List<OtpDTO> toDto(List<Otp> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<OtpDTO> list = new ArrayList<OtpDTO>( entityList.size() );
        for ( Otp otp : entityList ) {
            list.add( toDto( otp ) );
        }

        return list;
    }
}
