package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.Department;
import com.hartron.esignharyana.service.dto.DepartmentDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class DepartmentMapperImpl implements DepartmentMapper {

    @Override
    public Department toEntity(DepartmentDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Department department = new Department();

        department.setId( dto.getId() );
        department.setDepartmentName( dto.getDepartmentName() );
        department.setCreatedOn( dto.getCreatedOn() );
        department.setCreatedBy( dto.getCreatedBy() );
        department.setUpdatedOn( dto.getUpdatedOn() );
        department.setUpdatedBy( dto.getUpdatedBy() );
        department.setStatus( dto.getStatus() );
        department.setRemark( dto.getRemark() );
        department.setWhiteListIp1( dto.getWhiteListIp1() );
        department.setWhiteListIp2( dto.getWhiteListIp2() );

        return department;
    }

    @Override
    public DepartmentDTO toDto(Department entity) {
        if ( entity == null ) {
            return null;
        }

        DepartmentDTO departmentDTO = new DepartmentDTO();

        departmentDTO.setId( entity.getId() );
        departmentDTO.setDepartmentName( entity.getDepartmentName() );
        departmentDTO.setCreatedOn( entity.getCreatedOn() );
        departmentDTO.setCreatedBy( entity.getCreatedBy() );
        departmentDTO.setUpdatedOn( entity.getUpdatedOn() );
        departmentDTO.setUpdatedBy( entity.getUpdatedBy() );
        departmentDTO.setStatus( entity.getStatus() );
        departmentDTO.setRemark( entity.getRemark() );
        departmentDTO.setWhiteListIp1( entity.getWhiteListIp1() );
        departmentDTO.setWhiteListIp2( entity.getWhiteListIp2() );

        return departmentDTO;
    }

    @Override
    public List<Department> toEntity(List<DepartmentDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Department> list = new ArrayList<Department>( dtoList.size() );
        for ( DepartmentDTO departmentDTO : dtoList ) {
            list.add( toEntity( departmentDTO ) );
        }

        return list;
    }

    @Override
    public List<DepartmentDTO> toDto(List<Department> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<DepartmentDTO> list = new ArrayList<DepartmentDTO>( entityList.size() );
        for ( Department department : entityList ) {
            list.add( toDto( department ) );
        }

        return list;
    }
}
