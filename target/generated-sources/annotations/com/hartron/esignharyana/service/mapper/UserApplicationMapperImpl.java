package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.ApplicationMaster;
import com.hartron.esignharyana.domain.UserApplication;
import com.hartron.esignharyana.service.dto.UserApplicationDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class UserApplicationMapperImpl implements UserApplicationMapper {

    @Autowired
    private ApplicationMasterMapper applicationMasterMapper;

    @Override
    public List<UserApplication> toEntity(List<UserApplicationDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<UserApplication> list = new ArrayList<UserApplication>( dtoList.size() );
        for ( UserApplicationDTO userApplicationDTO : dtoList ) {
            list.add( toEntity( userApplicationDTO ) );
        }

        return list;
    }

    @Override
    public List<UserApplicationDTO> toDto(List<UserApplication> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<UserApplicationDTO> list = new ArrayList<UserApplicationDTO>( entityList.size() );
        for ( UserApplication userApplication : entityList ) {
            list.add( toDto( userApplication ) );
        }

        return list;
    }

    @Override
    public UserApplicationDTO toDto(UserApplication userApplication) {
        if ( userApplication == null ) {
            return null;
        }

        UserApplicationDTO userApplicationDTO = new UserApplicationDTO();

        userApplicationDTO.setApplicationMasterId( userApplicationApplicationMasterId( userApplication ) );
        userApplicationDTO.setId( userApplication.getId() );
        userApplicationDTO.setUserId( userApplication.getUserId() );
        userApplicationDTO.setUserLogin( userApplication.getUserLogin() );
        userApplicationDTO.setCreatedBy( userApplication.getCreatedBy() );
        userApplicationDTO.setCreatedOn( userApplication.getCreatedOn() );
        userApplicationDTO.setUpdatedBy( userApplication.getUpdatedBy() );
        userApplicationDTO.setUpdatedOn( userApplication.getUpdatedOn() );
        userApplicationDTO.setVerifiedBy( userApplication.getVerifiedBy() );
        userApplicationDTO.setVerifiedOn( userApplication.getVerifiedOn() );
        userApplicationDTO.setUserCodeId( userApplication.getUserCodeId() );
        userApplicationDTO.setApplicationCodeId( userApplication.getApplicationCodeId() );

        return userApplicationDTO;
    }

    @Override
    public UserApplication toEntity(UserApplicationDTO userApplicationDTO) {
        if ( userApplicationDTO == null ) {
            return null;
        }

        UserApplication userApplication = new UserApplication();

        userApplication.setApplicationMaster( applicationMasterMapper.fromId( userApplicationDTO.getApplicationMasterId() ) );
        userApplication.setId( userApplicationDTO.getId() );
        userApplication.setUserId( userApplicationDTO.getUserId() );
        userApplication.setUserLogin( userApplicationDTO.getUserLogin() );
        userApplication.setCreatedBy( userApplicationDTO.getCreatedBy() );
        userApplication.setCreatedOn( userApplicationDTO.getCreatedOn() );
        userApplication.setUpdatedBy( userApplicationDTO.getUpdatedBy() );
        userApplication.setUpdatedOn( userApplicationDTO.getUpdatedOn() );
        userApplication.setVerifiedBy( userApplicationDTO.getVerifiedBy() );
        userApplication.setVerifiedOn( userApplicationDTO.getVerifiedOn() );
        userApplication.setUserCodeId( userApplicationDTO.getUserCodeId() );
        userApplication.setApplicationCodeId( userApplicationDTO.getApplicationCodeId() );

        return userApplication;
    }

    private Long userApplicationApplicationMasterId(UserApplication userApplication) {
        if ( userApplication == null ) {
            return null;
        }
        ApplicationMaster applicationMaster = userApplication.getApplicationMaster();
        if ( applicationMaster == null ) {
            return null;
        }
        Long id = applicationMaster.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
