package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.DistrictMaster;
import com.hartron.esignharyana.domain.StateMaster;
import com.hartron.esignharyana.domain.SubDistrictMaster;
import com.hartron.esignharyana.service.dto.SubDistrictMasterDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class SubDistrictMasterMapperImpl implements SubDistrictMasterMapper {

    @Autowired
    private StateMasterMapper stateMasterMapper;
    @Autowired
    private DistrictMasterMapper districtMasterMapper;

    @Override
    public List<SubDistrictMaster> toEntity(List<SubDistrictMasterDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<SubDistrictMaster> list = new ArrayList<SubDistrictMaster>( dtoList.size() );
        for ( SubDistrictMasterDTO subDistrictMasterDTO : dtoList ) {
            list.add( toEntity( subDistrictMasterDTO ) );
        }

        return list;
    }

    @Override
    public List<SubDistrictMasterDTO> toDto(List<SubDistrictMaster> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<SubDistrictMasterDTO> list = new ArrayList<SubDistrictMasterDTO>( entityList.size() );
        for ( SubDistrictMaster subDistrictMaster : entityList ) {
            list.add( toDto( subDistrictMaster ) );
        }

        return list;
    }

    @Override
    public SubDistrictMasterDTO toDto(SubDistrictMaster subDistrictMaster) {
        if ( subDistrictMaster == null ) {
            return null;
        }

        SubDistrictMasterDTO subDistrictMasterDTO = new SubDistrictMasterDTO();

        subDistrictMasterDTO.setDistrictMasterId( subDistrictMasterDistrictMasterId( subDistrictMaster ) );
        subDistrictMasterDTO.setStateMasterId( subDistrictMasterStateMasterId( subDistrictMaster ) );
        subDistrictMasterDTO.setId( subDistrictMaster.getId() );
        subDistrictMasterDTO.setSubDistrictCode( subDistrictMaster.getSubDistrictCode() );
        subDistrictMasterDTO.setSubDistrictName( subDistrictMaster.getSubDistrictName() );
        subDistrictMasterDTO.setLastUpdatedBy( subDistrictMaster.getLastUpdatedBy() );
        subDistrictMasterDTO.setLastUpdatedOn( subDistrictMaster.getLastUpdatedOn() );
        subDistrictMasterDTO.setVerifiedBy( subDistrictMaster.getVerifiedBy() );
        subDistrictMasterDTO.setVerifiedOn( subDistrictMaster.getVerifiedOn() );
        subDistrictMasterDTO.setRemarks( subDistrictMaster.getRemarks() );

        return subDistrictMasterDTO;
    }

    @Override
    public SubDistrictMaster toEntity(SubDistrictMasterDTO subDistrictMasterDTO) {
        if ( subDistrictMasterDTO == null ) {
            return null;
        }

        SubDistrictMaster subDistrictMaster = new SubDistrictMaster();

        subDistrictMaster.setDistrictMaster( districtMasterMapper.fromId( subDistrictMasterDTO.getDistrictMasterId() ) );
        subDistrictMaster.setStateMaster( stateMasterMapper.fromId( subDistrictMasterDTO.getStateMasterId() ) );
        subDistrictMaster.setId( subDistrictMasterDTO.getId() );
        subDistrictMaster.setSubDistrictCode( subDistrictMasterDTO.getSubDistrictCode() );
        subDistrictMaster.setSubDistrictName( subDistrictMasterDTO.getSubDistrictName() );
        subDistrictMaster.setLastUpdatedBy( subDistrictMasterDTO.getLastUpdatedBy() );
        subDistrictMaster.setLastUpdatedOn( subDistrictMasterDTO.getLastUpdatedOn() );
        subDistrictMaster.setVerifiedBy( subDistrictMasterDTO.getVerifiedBy() );
        subDistrictMaster.setVerifiedOn( subDistrictMasterDTO.getVerifiedOn() );
        subDistrictMaster.setRemarks( subDistrictMasterDTO.getRemarks() );

        return subDistrictMaster;
    }

    private Long subDistrictMasterDistrictMasterId(SubDistrictMaster subDistrictMaster) {
        if ( subDistrictMaster == null ) {
            return null;
        }
        DistrictMaster districtMaster = subDistrictMaster.getDistrictMaster();
        if ( districtMaster == null ) {
            return null;
        }
        Long id = districtMaster.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long subDistrictMasterStateMasterId(SubDistrictMaster subDistrictMaster) {
        if ( subDistrictMaster == null ) {
            return null;
        }
        StateMaster stateMaster = subDistrictMaster.getStateMaster();
        if ( stateMaster == null ) {
            return null;
        }
        Long id = stateMaster.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
