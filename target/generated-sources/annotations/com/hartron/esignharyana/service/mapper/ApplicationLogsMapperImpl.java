package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.ApplicationLogs;
import com.hartron.esignharyana.service.dto.ApplicationLogsDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class ApplicationLogsMapperImpl implements ApplicationLogsMapper {

    @Override
    public ApplicationLogs toEntity(ApplicationLogsDTO dto) {
        if ( dto == null ) {
            return null;
        }

        ApplicationLogs applicationLogs = new ApplicationLogs();

        applicationLogs.setId( dto.getId() );
        applicationLogs.setActionTaken( dto.getActionTaken() );
        applicationLogs.setActionTakenBy( dto.getActionTakenBy() );
        applicationLogs.setActionTakenOnApplication( dto.getActionTakenOnApplication() );
        applicationLogs.setActionTakenOnDate( dto.getActionTakenOnDate() );
        applicationLogs.setRemarks( dto.getRemarks() );

        return applicationLogs;
    }

    @Override
    public ApplicationLogsDTO toDto(ApplicationLogs entity) {
        if ( entity == null ) {
            return null;
        }

        ApplicationLogsDTO applicationLogsDTO = new ApplicationLogsDTO();

        applicationLogsDTO.setId( entity.getId() );
        applicationLogsDTO.setActionTaken( entity.getActionTaken() );
        applicationLogsDTO.setActionTakenBy( entity.getActionTakenBy() );
        applicationLogsDTO.setActionTakenOnApplication( entity.getActionTakenOnApplication() );
        applicationLogsDTO.setActionTakenOnDate( entity.getActionTakenOnDate() );
        applicationLogsDTO.setRemarks( entity.getRemarks() );

        return applicationLogsDTO;
    }

    @Override
    public List<ApplicationLogs> toEntity(List<ApplicationLogsDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<ApplicationLogs> list = new ArrayList<ApplicationLogs>( dtoList.size() );
        for ( ApplicationLogsDTO applicationLogsDTO : dtoList ) {
            list.add( toEntity( applicationLogsDTO ) );
        }

        return list;
    }

    @Override
    public List<ApplicationLogsDTO> toDto(List<ApplicationLogs> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ApplicationLogsDTO> list = new ArrayList<ApplicationLogsDTO>( entityList.size() );
        for ( ApplicationLogs applicationLogs : entityList ) {
            list.add( toDto( applicationLogs ) );
        }

        return list;
    }
}
