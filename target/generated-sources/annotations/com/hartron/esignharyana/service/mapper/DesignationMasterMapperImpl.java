package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.DesignationMaster;
import com.hartron.esignharyana.service.dto.DesignationMasterDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class DesignationMasterMapperImpl implements DesignationMasterMapper {

    @Override
    public DesignationMaster toEntity(DesignationMasterDTO dto) {
        if ( dto == null ) {
            return null;
        }

        DesignationMaster designationMaster = new DesignationMaster();

        designationMaster.setId( dto.getId() );
        designationMaster.setDesignationName( dto.getDesignationName() );
        designationMaster.setDesignationAbbreviation( dto.getDesignationAbbreviation() );
        designationMaster.setCreatedBy( dto.getCreatedBy() );
        designationMaster.setCreatedOn( dto.getCreatedOn() );
        designationMaster.setLastUpdatedBy( dto.getLastUpdatedBy() );
        designationMaster.setLastUpdatedOn( dto.getLastUpdatedOn() );
        designationMaster.setVerifiedBy( dto.getVerifiedBy() );
        designationMaster.setVerifiedOn( dto.getVerifiedOn() );
        designationMaster.setRemarks( dto.getRemarks() );

        return designationMaster;
    }

    @Override
    public DesignationMasterDTO toDto(DesignationMaster entity) {
        if ( entity == null ) {
            return null;
        }

        DesignationMasterDTO designationMasterDTO = new DesignationMasterDTO();

        designationMasterDTO.setId( entity.getId() );
        designationMasterDTO.setDesignationName( entity.getDesignationName() );
        designationMasterDTO.setDesignationAbbreviation( entity.getDesignationAbbreviation() );
        designationMasterDTO.setCreatedBy( entity.getCreatedBy() );
        designationMasterDTO.setCreatedOn( entity.getCreatedOn() );
        designationMasterDTO.setLastUpdatedBy( entity.getLastUpdatedBy() );
        designationMasterDTO.setLastUpdatedOn( entity.getLastUpdatedOn() );
        designationMasterDTO.setVerifiedBy( entity.getVerifiedBy() );
        designationMasterDTO.setVerifiedOn( entity.getVerifiedOn() );
        designationMasterDTO.setRemarks( entity.getRemarks() );

        return designationMasterDTO;
    }

    @Override
    public List<DesignationMaster> toEntity(List<DesignationMasterDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<DesignationMaster> list = new ArrayList<DesignationMaster>( dtoList.size() );
        for ( DesignationMasterDTO designationMasterDTO : dtoList ) {
            list.add( toEntity( designationMasterDTO ) );
        }

        return list;
    }

    @Override
    public List<DesignationMasterDTO> toDto(List<DesignationMaster> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<DesignationMasterDTO> list = new ArrayList<DesignationMasterDTO>( entityList.size() );
        for ( DesignationMaster designationMaster : entityList ) {
            list.add( toDto( designationMaster ) );
        }

        return list;
    }
}
