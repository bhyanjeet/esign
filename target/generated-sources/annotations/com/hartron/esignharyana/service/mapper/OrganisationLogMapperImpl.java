package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.OrganisationLog;
import com.hartron.esignharyana.service.dto.OrganisationLogDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class OrganisationLogMapperImpl implements OrganisationLogMapper {

    @Override
    public OrganisationLog toEntity(OrganisationLogDTO dto) {
        if ( dto == null ) {
            return null;
        }

        OrganisationLog organisationLog = new OrganisationLog();

        organisationLog.setId( dto.getId() );
        organisationLog.setOrganisationId( dto.getOrganisationId() );
        organisationLog.setCreatedByLogin( dto.getCreatedByLogin() );
        organisationLog.setCreatedDate( dto.getCreatedDate() );
        organisationLog.setVerifyByName( dto.getVerifyByName() );
        organisationLog.setVerifyDate( dto.getVerifyDate() );
        organisationLog.setRemarks( dto.getRemarks() );
        organisationLog.setStatus( dto.getStatus() );
        organisationLog.setVerifyByLogin( dto.getVerifyByLogin() );
        organisationLog.setCreatedByName( dto.getCreatedByName() );

        return organisationLog;
    }

    @Override
    public OrganisationLogDTO toDto(OrganisationLog entity) {
        if ( entity == null ) {
            return null;
        }

        OrganisationLogDTO organisationLogDTO = new OrganisationLogDTO();

        organisationLogDTO.setId( entity.getId() );
        organisationLogDTO.setOrganisationId( entity.getOrganisationId() );
        organisationLogDTO.setCreatedByLogin( entity.getCreatedByLogin() );
        organisationLogDTO.setCreatedDate( entity.getCreatedDate() );
        organisationLogDTO.setVerifyByName( entity.getVerifyByName() );
        organisationLogDTO.setVerifyDate( entity.getVerifyDate() );
        organisationLogDTO.setRemarks( entity.getRemarks() );
        organisationLogDTO.setStatus( entity.getStatus() );
        organisationLogDTO.setVerifyByLogin( entity.getVerifyByLogin() );
        organisationLogDTO.setCreatedByName( entity.getCreatedByName() );

        return organisationLogDTO;
    }

    @Override
    public List<OrganisationLog> toEntity(List<OrganisationLogDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<OrganisationLog> list = new ArrayList<OrganisationLog>( dtoList.size() );
        for ( OrganisationLogDTO organisationLogDTO : dtoList ) {
            list.add( toEntity( organisationLogDTO ) );
        }

        return list;
    }

    @Override
    public List<OrganisationLogDTO> toDto(List<OrganisationLog> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<OrganisationLogDTO> list = new ArrayList<OrganisationLogDTO>( entityList.size() );
        for ( OrganisationLog organisationLog : entityList ) {
            list.add( toDto( organisationLog ) );
        }

        return list;
    }
}
