package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.DistrictMaster;
import com.hartron.esignharyana.domain.StateMaster;
import com.hartron.esignharyana.service.dto.DistrictMasterDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class DistrictMasterMapperImpl implements DistrictMasterMapper {

    @Autowired
    private StateMasterMapper stateMasterMapper;

    @Override
    public List<DistrictMaster> toEntity(List<DistrictMasterDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<DistrictMaster> list = new ArrayList<DistrictMaster>( dtoList.size() );
        for ( DistrictMasterDTO districtMasterDTO : dtoList ) {
            list.add( toEntity( districtMasterDTO ) );
        }

        return list;
    }

    @Override
    public List<DistrictMasterDTO> toDto(List<DistrictMaster> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<DistrictMasterDTO> list = new ArrayList<DistrictMasterDTO>( entityList.size() );
        for ( DistrictMaster districtMaster : entityList ) {
            list.add( toDto( districtMaster ) );
        }

        return list;
    }

    @Override
    public DistrictMasterDTO toDto(DistrictMaster districtMaster) {
        if ( districtMaster == null ) {
            return null;
        }

        DistrictMasterDTO districtMasterDTO = new DistrictMasterDTO();

        districtMasterDTO.setStateMasterId( districtMasterStateMasterId( districtMaster ) );
        districtMasterDTO.setId( districtMaster.getId() );
        districtMasterDTO.setDistrictName( districtMaster.getDistrictName() );
        districtMasterDTO.setDistrictCode( districtMaster.getDistrictCode() );
        districtMasterDTO.setCreatedOn( districtMaster.getCreatedOn() );
        districtMasterDTO.setLastUpdatedBy( districtMaster.getLastUpdatedBy() );
        districtMasterDTO.setLastUpdatedOn( districtMaster.getLastUpdatedOn() );
        districtMasterDTO.setVerifiedBy( districtMaster.getVerifiedBy() );
        districtMasterDTO.setVerifiedOn( districtMaster.getVerifiedOn() );
        districtMasterDTO.setRemarks( districtMaster.getRemarks() );

        return districtMasterDTO;
    }

    @Override
    public DistrictMaster toEntity(DistrictMasterDTO districtMasterDTO) {
        if ( districtMasterDTO == null ) {
            return null;
        }

        DistrictMaster districtMaster = new DistrictMaster();

        districtMaster.setStateMaster( stateMasterMapper.fromId( districtMasterDTO.getStateMasterId() ) );
        districtMaster.setId( districtMasterDTO.getId() );
        districtMaster.setDistrictName( districtMasterDTO.getDistrictName() );
        districtMaster.setDistrictCode( districtMasterDTO.getDistrictCode() );
        districtMaster.setCreatedOn( districtMasterDTO.getCreatedOn() );
        districtMaster.setLastUpdatedBy( districtMasterDTO.getLastUpdatedBy() );
        districtMaster.setLastUpdatedOn( districtMasterDTO.getLastUpdatedOn() );
        districtMaster.setVerifiedBy( districtMasterDTO.getVerifiedBy() );
        districtMaster.setVerifiedOn( districtMasterDTO.getVerifiedOn() );
        districtMaster.setRemarks( districtMasterDTO.getRemarks() );

        return districtMaster;
    }

    private Long districtMasterStateMasterId(DistrictMaster districtMaster) {
        if ( districtMaster == null ) {
            return null;
        }
        StateMaster stateMaster = districtMaster.getStateMaster();
        if ( stateMaster == null ) {
            return null;
        }
        Long id = stateMaster.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
