package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.ApplicationMaster;
import com.hartron.esignharyana.domain.OrganisationMaster;
import com.hartron.esignharyana.service.dto.ApplicationMasterDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class ApplicationMasterMapperImpl implements ApplicationMasterMapper {

    @Autowired
    private OrganisationMasterMapper organisationMasterMapper;

    @Override
    public List<ApplicationMaster> toEntity(List<ApplicationMasterDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<ApplicationMaster> list = new ArrayList<ApplicationMaster>( dtoList.size() );
        for ( ApplicationMasterDTO applicationMasterDTO : dtoList ) {
            list.add( toEntity( applicationMasterDTO ) );
        }

        return list;
    }

    @Override
    public List<ApplicationMasterDTO> toDto(List<ApplicationMaster> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ApplicationMasterDTO> list = new ArrayList<ApplicationMasterDTO>( entityList.size() );
        for ( ApplicationMaster applicationMaster : entityList ) {
            list.add( toDto( applicationMaster ) );
        }

        return list;
    }

    @Override
    public ApplicationMasterDTO toDto(ApplicationMaster applicationMaster) {
        if ( applicationMaster == null ) {
            return null;
        }

        ApplicationMasterDTO applicationMasterDTO = new ApplicationMasterDTO();

        applicationMasterDTO.setOrganisationMasterId( applicationMasterOrganisationMasterId( applicationMaster ) );
        applicationMasterDTO.setId( applicationMaster.getId() );
        applicationMasterDTO.setApplicationName( applicationMaster.getApplicationName() );
        applicationMasterDTO.setCreatedBy( applicationMaster.getCreatedBy() );
        applicationMasterDTO.setCreatedOn( applicationMaster.getCreatedOn() );
        applicationMasterDTO.setLastUpdatedBy( applicationMaster.getLastUpdatedBy() );
        applicationMasterDTO.setLastUpdatedOn( applicationMaster.getLastUpdatedOn() );
        applicationMasterDTO.setVerifiedBy( applicationMaster.getVerifiedBy() );
        applicationMasterDTO.setVerifiedOn( applicationMaster.getVerifiedOn() );
        applicationMasterDTO.setRemarks( applicationMaster.getRemarks() );
        applicationMasterDTO.setApplicationURL( applicationMaster.getApplicationURL() );
        applicationMasterDTO.setApplicationStatus( applicationMaster.getApplicationStatus() );
        applicationMasterDTO.setApplicationIdCode( applicationMaster.getApplicationIdCode() );
        applicationMasterDTO.setStatus( applicationMaster.getStatus() );

        return applicationMasterDTO;
    }

    @Override
    public ApplicationMaster toEntity(ApplicationMasterDTO applicationMasterDTO) {
        if ( applicationMasterDTO == null ) {
            return null;
        }

        ApplicationMaster applicationMaster = new ApplicationMaster();

        applicationMaster.setOrganisationMaster( organisationMasterMapper.fromId( applicationMasterDTO.getOrganisationMasterId() ) );
        applicationMaster.setId( applicationMasterDTO.getId() );
        applicationMaster.setApplicationName( applicationMasterDTO.getApplicationName() );
        applicationMaster.setCreatedBy( applicationMasterDTO.getCreatedBy() );
        applicationMaster.setCreatedOn( applicationMasterDTO.getCreatedOn() );
        applicationMaster.setLastUpdatedBy( applicationMasterDTO.getLastUpdatedBy() );
        applicationMaster.setLastUpdatedOn( applicationMasterDTO.getLastUpdatedOn() );
        applicationMaster.setVerifiedBy( applicationMasterDTO.getVerifiedBy() );
        applicationMaster.setVerifiedOn( applicationMasterDTO.getVerifiedOn() );
        applicationMaster.setRemarks( applicationMasterDTO.getRemarks() );
        applicationMaster.setApplicationURL( applicationMasterDTO.getApplicationURL() );
        applicationMaster.setApplicationStatus( applicationMasterDTO.getApplicationStatus() );
        applicationMaster.setApplicationIdCode( applicationMasterDTO.getApplicationIdCode() );
        applicationMaster.setStatus( applicationMasterDTO.getStatus() );

        return applicationMaster;
    }

    private Long applicationMasterOrganisationMasterId(ApplicationMaster applicationMaster) {
        if ( applicationMaster == null ) {
            return null;
        }
        OrganisationMaster organisationMaster = applicationMaster.getOrganisationMaster();
        if ( organisationMaster == null ) {
            return null;
        }
        Long id = organisationMaster.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
