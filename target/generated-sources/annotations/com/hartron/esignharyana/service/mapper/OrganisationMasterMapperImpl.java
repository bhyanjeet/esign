package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.Department;
import com.hartron.esignharyana.domain.DistrictMaster;
import com.hartron.esignharyana.domain.OrganisationMaster;
import com.hartron.esignharyana.domain.OrganisationTypeMaster;
import com.hartron.esignharyana.domain.StateMaster;
import com.hartron.esignharyana.domain.SubDistrictMaster;
import com.hartron.esignharyana.service.dto.OrganisationMasterDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class OrganisationMasterMapperImpl implements OrganisationMasterMapper {

    @Autowired
    private OrganisationTypeMasterMapper organisationTypeMasterMapper;
    @Autowired
    private StateMasterMapper stateMasterMapper;
    @Autowired
    private DistrictMasterMapper districtMasterMapper;
    @Autowired
    private SubDistrictMasterMapper subDistrictMasterMapper;
    @Autowired
    private DepartmentMapper departmentMapper;

    @Override
    public List<OrganisationMaster> toEntity(List<OrganisationMasterDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<OrganisationMaster> list = new ArrayList<OrganisationMaster>( dtoList.size() );
        for ( OrganisationMasterDTO organisationMasterDTO : dtoList ) {
            list.add( toEntity( organisationMasterDTO ) );
        }

        return list;
    }

    @Override
    public List<OrganisationMasterDTO> toDto(List<OrganisationMaster> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<OrganisationMasterDTO> list = new ArrayList<OrganisationMasterDTO>( entityList.size() );
        for ( OrganisationMaster organisationMaster : entityList ) {
            list.add( toDto( organisationMaster ) );
        }

        return list;
    }

    @Override
    public OrganisationMasterDTO toDto(OrganisationMaster organisationMaster) {
        if ( organisationMaster == null ) {
            return null;
        }

        OrganisationMasterDTO organisationMasterDTO = new OrganisationMasterDTO();

        organisationMasterDTO.setOrganisationTypeMasterId( organisationMasterOrganisationTypeMasterId( organisationMaster ) );
        organisationMasterDTO.setSubDistrictMasterId( organisationMasterSubDistrictMasterId( organisationMaster ) );
        organisationMasterDTO.setDistrictMasterId( organisationMasterDistrictMasterId( organisationMaster ) );
        organisationMasterDTO.setStateMasterId( organisationMasterStateMasterId( organisationMaster ) );
        organisationMasterDTO.setDepartmentId( organisationMasterDepartmentId( organisationMaster ) );
        organisationMasterDTO.setPinCode( organisationMaster.getPinCode() );
        organisationMasterDTO.setId( organisationMaster.getId() );
        organisationMasterDTO.setOrganisationName( organisationMaster.getOrganisationName() );
        organisationMasterDTO.setOrganisationAbbreviation( organisationMaster.getOrganisationAbbreviation() );
        organisationMasterDTO.setOrganisationCorrespondenceEmail( organisationMaster.getOrganisationCorrespondenceEmail() );
        organisationMasterDTO.setOrganisationCorrespondencePhone1( organisationMaster.getOrganisationCorrespondencePhone1() );
        organisationMasterDTO.setOrganisationCorrespondencePhone2( organisationMaster.getOrganisationCorrespondencePhone2() );
        organisationMasterDTO.setOrganisationCorrespondenceAddress( organisationMaster.getOrganisationCorrespondenceAddress() );
        organisationMasterDTO.setOrganisationWebsite( organisationMaster.getOrganisationWebsite() );
        organisationMasterDTO.setOrganisationNodalOfficerName( organisationMaster.getOrganisationNodalOfficerName() );
        organisationMasterDTO.setOrganisationNodalOfficerPhoneMobile( organisationMaster.getOrganisationNodalOfficerPhoneMobile() );
        organisationMasterDTO.setOrganisationNodalOfficerPhoneLandline( organisationMaster.getOrganisationNodalOfficerPhoneLandline() );
        organisationMasterDTO.setNodalOfficerOfficialEmail( organisationMaster.getNodalOfficerOfficialEmail() );
        organisationMasterDTO.setOrganisationNodalOfficerAlternativeEmail( organisationMaster.getOrganisationNodalOfficerAlternativeEmail() );
        organisationMasterDTO.setCreatedBy( organisationMaster.getCreatedBy() );
        organisationMasterDTO.setCreatedOn( organisationMaster.getCreatedOn() );
        organisationMasterDTO.setLastUpdatedBy( organisationMaster.getLastUpdatedBy() );
        organisationMasterDTO.setLastUpdatedOn( organisationMaster.getLastUpdatedOn() );
        organisationMasterDTO.setVerifiedBy( organisationMaster.getVerifiedBy() );
        organisationMasterDTO.setVerifiedOn( organisationMaster.getVerifiedOn() );
        organisationMasterDTO.setRemarks( organisationMaster.getRemarks() );
        organisationMasterDTO.setUserId( organisationMaster.getUserId() );
        organisationMasterDTO.setStage( organisationMaster.getStage() );
        organisationMasterDTO.setStatus( organisationMaster.getStatus() );
        organisationMasterDTO.setOrganisationIdCode( organisationMaster.getOrganisationIdCode() );
        organisationMasterDTO.setOrganisationEmployeeId( organisationMaster.getOrganisationEmployeeId() );
        organisationMasterDTO.setDateOfBirth( organisationMaster.getDateOfBirth() );
        organisationMasterDTO.setGender( organisationMaster.getGender() );
        organisationMasterDTO.setPan( organisationMaster.getPan() );
        organisationMasterDTO.setAadhaar( organisationMaster.getAadhaar() );
        organisationMasterDTO.setOrgnisationEmpolyeeCardAttchment( organisationMaster.getOrgnisationEmpolyeeCardAttchment() );
        organisationMasterDTO.setPanAttachment( organisationMaster.getPanAttachment() );
        organisationMasterDTO.setPhotographAttachment( organisationMaster.getPhotographAttachment() );
        organisationMasterDTO.setAadhaarAttachment( organisationMaster.getAadhaarAttachment() );

        return organisationMasterDTO;
    }

    @Override
    public OrganisationMaster toEntity(OrganisationMasterDTO organisationMasterDTO) {
        if ( organisationMasterDTO == null ) {
            return null;
        }

        OrganisationMaster organisationMaster = new OrganisationMaster();

        organisationMaster.setOrganisationTypeMaster( organisationTypeMasterMapper.fromId( organisationMasterDTO.getOrganisationTypeMasterId() ) );
        organisationMaster.setStateMaster( stateMasterMapper.fromId( organisationMasterDTO.getStateMasterId() ) );
        organisationMaster.setSubDistrictMaster( subDistrictMasterMapper.fromId( organisationMasterDTO.getSubDistrictMasterId() ) );
        organisationMaster.setDepartment( departmentMapper.fromId( organisationMasterDTO.getDepartmentId() ) );
        organisationMaster.setDistrictMaster( districtMasterMapper.fromId( organisationMasterDTO.getDistrictMasterId() ) );
        organisationMaster.setPinCode( organisationMasterDTO.getPinCode() );
        organisationMaster.setId( organisationMasterDTO.getId() );
        organisationMaster.setOrganisationName( organisationMasterDTO.getOrganisationName() );
        organisationMaster.setOrganisationAbbreviation( organisationMasterDTO.getOrganisationAbbreviation() );
        organisationMaster.setOrganisationCorrespondenceEmail( organisationMasterDTO.getOrganisationCorrespondenceEmail() );
        organisationMaster.setOrganisationCorrespondencePhone1( organisationMasterDTO.getOrganisationCorrespondencePhone1() );
        organisationMaster.setOrganisationCorrespondencePhone2( organisationMasterDTO.getOrganisationCorrespondencePhone2() );
        organisationMaster.setOrganisationCorrespondenceAddress( organisationMasterDTO.getOrganisationCorrespondenceAddress() );
        organisationMaster.setOrganisationWebsite( organisationMasterDTO.getOrganisationWebsite() );
        organisationMaster.setOrganisationNodalOfficerName( organisationMasterDTO.getOrganisationNodalOfficerName() );
        organisationMaster.setOrganisationNodalOfficerPhoneMobile( organisationMasterDTO.getOrganisationNodalOfficerPhoneMobile() );
        organisationMaster.setOrganisationNodalOfficerPhoneLandline( organisationMasterDTO.getOrganisationNodalOfficerPhoneLandline() );
        organisationMaster.setNodalOfficerOfficialEmail( organisationMasterDTO.getNodalOfficerOfficialEmail() );
        organisationMaster.setOrganisationNodalOfficerAlternativeEmail( organisationMasterDTO.getOrganisationNodalOfficerAlternativeEmail() );
        organisationMaster.setCreatedBy( organisationMasterDTO.getCreatedBy() );
        organisationMaster.setCreatedOn( organisationMasterDTO.getCreatedOn() );
        organisationMaster.setLastUpdatedBy( organisationMasterDTO.getLastUpdatedBy() );
        organisationMaster.setLastUpdatedOn( organisationMasterDTO.getLastUpdatedOn() );
        organisationMaster.setVerifiedBy( organisationMasterDTO.getVerifiedBy() );
        organisationMaster.setVerifiedOn( organisationMasterDTO.getVerifiedOn() );
        organisationMaster.setRemarks( organisationMasterDTO.getRemarks() );
        organisationMaster.setUserId( organisationMasterDTO.getUserId() );
        organisationMaster.setStage( organisationMasterDTO.getStage() );
        organisationMaster.setStatus( organisationMasterDTO.getStatus() );
        organisationMaster.setOrganisationIdCode( organisationMasterDTO.getOrganisationIdCode() );
        organisationMaster.setOrganisationEmployeeId( organisationMasterDTO.getOrganisationEmployeeId() );
        organisationMaster.setDateOfBirth( organisationMasterDTO.getDateOfBirth() );
        organisationMaster.setGender( organisationMasterDTO.getGender() );
        organisationMaster.setPan( organisationMasterDTO.getPan() );
        organisationMaster.setAadhaar( organisationMasterDTO.getAadhaar() );
        organisationMaster.setOrgnisationEmpolyeeCardAttchment( organisationMasterDTO.getOrgnisationEmpolyeeCardAttchment() );
        organisationMaster.setPanAttachment( organisationMasterDTO.getPanAttachment() );
        organisationMaster.setPhotographAttachment( organisationMasterDTO.getPhotographAttachment() );
        organisationMaster.setAadhaarAttachment( organisationMasterDTO.getAadhaarAttachment() );

        return organisationMaster;
    }

    private Long organisationMasterOrganisationTypeMasterId(OrganisationMaster organisationMaster) {
        if ( organisationMaster == null ) {
            return null;
        }
        OrganisationTypeMaster organisationTypeMaster = organisationMaster.getOrganisationTypeMaster();
        if ( organisationTypeMaster == null ) {
            return null;
        }
        Long id = organisationTypeMaster.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long organisationMasterSubDistrictMasterId(OrganisationMaster organisationMaster) {
        if ( organisationMaster == null ) {
            return null;
        }
        SubDistrictMaster subDistrictMaster = organisationMaster.getSubDistrictMaster();
        if ( subDistrictMaster == null ) {
            return null;
        }
        Long id = subDistrictMaster.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long organisationMasterDistrictMasterId(OrganisationMaster organisationMaster) {
        if ( organisationMaster == null ) {
            return null;
        }
        DistrictMaster districtMaster = organisationMaster.getDistrictMaster();
        if ( districtMaster == null ) {
            return null;
        }
        Long id = districtMaster.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long organisationMasterStateMasterId(OrganisationMaster organisationMaster) {
        if ( organisationMaster == null ) {
            return null;
        }
        StateMaster stateMaster = organisationMaster.getStateMaster();
        if ( stateMaster == null ) {
            return null;
        }
        Long id = stateMaster.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long organisationMasterDepartmentId(OrganisationMaster organisationMaster) {
        if ( organisationMaster == null ) {
            return null;
        }
        Department department = organisationMaster.getDepartment();
        if ( department == null ) {
            return null;
        }
        Long id = department.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
