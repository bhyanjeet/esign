package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.EsignError;
import com.hartron.esignharyana.service.dto.EsignErrorDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class EsignErrorMapperImpl implements EsignErrorMapper {

    @Override
    public EsignError toEntity(EsignErrorDTO dto) {
        if ( dto == null ) {
            return null;
        }

        EsignError esignError = new EsignError();

        esignError.setId( dto.getId() );
        esignError.setErrorCode( dto.getErrorCode() );
        esignError.setErrorMessage( dto.getErrorMessage() );
        esignError.setErrorStage( dto.getErrorStage() );

        return esignError;
    }

    @Override
    public EsignErrorDTO toDto(EsignError entity) {
        if ( entity == null ) {
            return null;
        }

        EsignErrorDTO esignErrorDTO = new EsignErrorDTO();

        esignErrorDTO.setId( entity.getId() );
        esignErrorDTO.setErrorCode( entity.getErrorCode() );
        esignErrorDTO.setErrorMessage( entity.getErrorMessage() );
        esignErrorDTO.setErrorStage( entity.getErrorStage() );

        return esignErrorDTO;
    }

    @Override
    public List<EsignError> toEntity(List<EsignErrorDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<EsignError> list = new ArrayList<EsignError>( dtoList.size() );
        for ( EsignErrorDTO esignErrorDTO : dtoList ) {
            list.add( toEntity( esignErrorDTO ) );
        }

        return list;
    }

    @Override
    public List<EsignErrorDTO> toDto(List<EsignError> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<EsignErrorDTO> list = new ArrayList<EsignErrorDTO>( entityList.size() );
        for ( EsignError esignError : entityList ) {
            list.add( toDto( esignError ) );
        }

        return list;
    }
}
