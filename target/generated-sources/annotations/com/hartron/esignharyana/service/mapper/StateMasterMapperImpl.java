package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.StateMaster;
import com.hartron.esignharyana.service.dto.StateMasterDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class StateMasterMapperImpl implements StateMasterMapper {

    @Override
    public StateMaster toEntity(StateMasterDTO dto) {
        if ( dto == null ) {
            return null;
        }

        StateMaster stateMaster = new StateMaster();

        stateMaster.setId( dto.getId() );
        stateMaster.setStateCode( dto.getStateCode() );
        stateMaster.setStateName( dto.getStateName() );
        stateMaster.setCreatedOn( dto.getCreatedOn() );
        stateMaster.setLastUpdatedOn( dto.getLastUpdatedOn() );
        stateMaster.setLastUpdatedBy( dto.getLastUpdatedBy() );
        stateMaster.setVerifiedBy( dto.getVerifiedBy() );
        stateMaster.setVerifiedOn( dto.getVerifiedOn() );
        stateMaster.setRemarks( dto.getRemarks() );

        return stateMaster;
    }

    @Override
    public StateMasterDTO toDto(StateMaster entity) {
        if ( entity == null ) {
            return null;
        }

        StateMasterDTO stateMasterDTO = new StateMasterDTO();

        stateMasterDTO.setId( entity.getId() );
        stateMasterDTO.setStateCode( entity.getStateCode() );
        stateMasterDTO.setStateName( entity.getStateName() );
        stateMasterDTO.setCreatedOn( entity.getCreatedOn() );
        stateMasterDTO.setLastUpdatedOn( entity.getLastUpdatedOn() );
        stateMasterDTO.setLastUpdatedBy( entity.getLastUpdatedBy() );
        stateMasterDTO.setVerifiedBy( entity.getVerifiedBy() );
        stateMasterDTO.setVerifiedOn( entity.getVerifiedOn() );
        stateMasterDTO.setRemarks( entity.getRemarks() );

        return stateMasterDTO;
    }

    @Override
    public List<StateMaster> toEntity(List<StateMasterDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<StateMaster> list = new ArrayList<StateMaster>( dtoList.size() );
        for ( StateMasterDTO stateMasterDTO : dtoList ) {
            list.add( toEntity( stateMasterDTO ) );
        }

        return list;
    }

    @Override
    public List<StateMasterDTO> toDto(List<StateMaster> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<StateMasterDTO> list = new ArrayList<StateMasterDTO>( entityList.size() );
        for ( StateMaster stateMaster : entityList ) {
            list.add( toDto( stateMaster ) );
        }

        return list;
    }
}
