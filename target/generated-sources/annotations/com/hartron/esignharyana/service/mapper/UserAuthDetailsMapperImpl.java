package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.UserAuthDetails;
import com.hartron.esignharyana.service.dto.UserAuthDetailsDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class UserAuthDetailsMapperImpl implements UserAuthDetailsMapper {

    @Override
    public UserAuthDetails toEntity(UserAuthDetailsDTO dto) {
        if ( dto == null ) {
            return null;
        }

        UserAuthDetails userAuthDetails = new UserAuthDetails();

        userAuthDetails.setId( dto.getId() );
        userAuthDetails.setLogin( dto.getLogin() );
        userAuthDetails.setDevice( dto.getDevice() );
        userAuthDetails.setAuthToken( dto.getAuthToken() );

        return userAuthDetails;
    }

    @Override
    public UserAuthDetailsDTO toDto(UserAuthDetails entity) {
        if ( entity == null ) {
            return null;
        }

        UserAuthDetailsDTO userAuthDetailsDTO = new UserAuthDetailsDTO();

        userAuthDetailsDTO.setId( entity.getId() );
        userAuthDetailsDTO.setLogin( entity.getLogin() );
        userAuthDetailsDTO.setDevice( entity.getDevice() );
        userAuthDetailsDTO.setAuthToken( entity.getAuthToken() );

        return userAuthDetailsDTO;
    }

    @Override
    public List<UserAuthDetails> toEntity(List<UserAuthDetailsDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<UserAuthDetails> list = new ArrayList<UserAuthDetails>( dtoList.size() );
        for ( UserAuthDetailsDTO userAuthDetailsDTO : dtoList ) {
            list.add( toEntity( userAuthDetailsDTO ) );
        }

        return list;
    }

    @Override
    public List<UserAuthDetailsDTO> toDto(List<UserAuthDetails> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<UserAuthDetailsDTO> list = new ArrayList<UserAuthDetailsDTO>( entityList.size() );
        for ( UserAuthDetails userAuthDetails : entityList ) {
            list.add( toDto( userAuthDetails ) );
        }

        return list;
    }
}
