package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.EsignTransation;
import com.hartron.esignharyana.service.dto.EsignTransationDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class EsignTransationMapperImpl implements EsignTransationMapper {

    @Override
    public EsignTransation toEntity(EsignTransationDTO dto) {
        if ( dto == null ) {
            return null;
        }

        EsignTransation esignTransation = new EsignTransation();

        esignTransation.setId( dto.getId() );
        esignTransation.setTxnId( dto.getTxnId() );
        esignTransation.setUserCodeId( dto.getUserCodeId() );
        esignTransation.setApplicationCodeId( dto.getApplicationCodeId() );
        esignTransation.setOrgResponseSuccessURL( dto.getOrgResponseSuccessURL() );
        esignTransation.setOrgResponseFailURL( dto.getOrgResponseFailURL() );
        esignTransation.setOrgStatus( dto.getOrgStatus() );
        esignTransation.setRequestDate( dto.getRequestDate() );
        esignTransation.setOrganisationIdCode( dto.getOrganisationIdCode() );
        esignTransation.setSignerId( dto.getSignerId() );
        esignTransation.setRequestType( dto.getRequestType() );
        esignTransation.setRequestStatus( dto.getRequestStatus() );
        esignTransation.setResponseType( dto.getResponseType() );
        esignTransation.setResponseStatus( dto.getResponseStatus() );
        esignTransation.setResponseCode( dto.getResponseCode() );
        esignTransation.setRequestXml( dto.getRequestXml() );
        esignTransation.setResponseXml( dto.getResponseXml() );
        esignTransation.setResponseDate( dto.getResponseDate() );
        esignTransation.setTs( dto.getTs() );

        return esignTransation;
    }

    @Override
    public EsignTransationDTO toDto(EsignTransation entity) {
        if ( entity == null ) {
            return null;
        }

        EsignTransationDTO esignTransationDTO = new EsignTransationDTO();

        esignTransationDTO.setId( entity.getId() );
        esignTransationDTO.setTxnId( entity.getTxnId() );
        esignTransationDTO.setUserCodeId( entity.getUserCodeId() );
        esignTransationDTO.setApplicationCodeId( entity.getApplicationCodeId() );
        esignTransationDTO.setOrgResponseSuccessURL( entity.getOrgResponseSuccessURL() );
        esignTransationDTO.setOrgResponseFailURL( entity.getOrgResponseFailURL() );
        esignTransationDTO.setOrgStatus( entity.getOrgStatus() );
        esignTransationDTO.setRequestDate( entity.getRequestDate() );
        esignTransationDTO.setOrganisationIdCode( entity.getOrganisationIdCode() );
        esignTransationDTO.setSignerId( entity.getSignerId() );
        esignTransationDTO.setRequestType( entity.getRequestType() );
        esignTransationDTO.setRequestStatus( entity.getRequestStatus() );
        esignTransationDTO.setResponseType( entity.getResponseType() );
        esignTransationDTO.setResponseStatus( entity.getResponseStatus() );
        esignTransationDTO.setResponseCode( entity.getResponseCode() );
        esignTransationDTO.setRequestXml( entity.getRequestXml() );
        esignTransationDTO.setResponseXml( entity.getResponseXml() );
        esignTransationDTO.setResponseDate( entity.getResponseDate() );
        esignTransationDTO.setTs( entity.getTs() );

        return esignTransationDTO;
    }

    @Override
    public List<EsignTransation> toEntity(List<EsignTransationDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<EsignTransation> list = new ArrayList<EsignTransation>( dtoList.size() );
        for ( EsignTransationDTO esignTransationDTO : dtoList ) {
            list.add( toEntity( esignTransationDTO ) );
        }

        return list;
    }

    @Override
    public List<EsignTransationDTO> toDto(List<EsignTransation> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<EsignTransationDTO> list = new ArrayList<EsignTransationDTO>( entityList.size() );
        for ( EsignTransation esignTransation : entityList ) {
            list.add( toDto( esignTransation ) );
        }

        return list;
    }
}
