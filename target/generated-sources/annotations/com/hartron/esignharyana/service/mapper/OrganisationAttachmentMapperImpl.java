package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.OrganisationAttachment;
import com.hartron.esignharyana.domain.OrganisationDocument;
import com.hartron.esignharyana.domain.OrganisationMaster;
import com.hartron.esignharyana.service.dto.OrganisationAttachmentDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class OrganisationAttachmentMapperImpl implements OrganisationAttachmentMapper {

    @Autowired
    private OrganisationMasterMapper organisationMasterMapper;
    @Autowired
    private OrganisationDocumentMapper organisationDocumentMapper;

    @Override
    public List<OrganisationAttachment> toEntity(List<OrganisationAttachmentDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<OrganisationAttachment> list = new ArrayList<OrganisationAttachment>( dtoList.size() );
        for ( OrganisationAttachmentDTO organisationAttachmentDTO : dtoList ) {
            list.add( toEntity( organisationAttachmentDTO ) );
        }

        return list;
    }

    @Override
    public List<OrganisationAttachmentDTO> toDto(List<OrganisationAttachment> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<OrganisationAttachmentDTO> list = new ArrayList<OrganisationAttachmentDTO>( entityList.size() );
        for ( OrganisationAttachment organisationAttachment : entityList ) {
            list.add( toDto( organisationAttachment ) );
        }

        return list;
    }

    @Override
    public OrganisationAttachmentDTO toDto(OrganisationAttachment organisationAttachment) {
        if ( organisationAttachment == null ) {
            return null;
        }

        OrganisationAttachmentDTO organisationAttachmentDTO = new OrganisationAttachmentDTO();

        organisationAttachmentDTO.setOrganisationDocumentId( organisationAttachmentOrganisationDocumentId( organisationAttachment ) );
        organisationAttachmentDTO.setOrganisationMasterId( organisationAttachmentOrganisationMasterId( organisationAttachment ) );
        organisationAttachmentDTO.setId( organisationAttachment.getId() );
        organisationAttachmentDTO.setAttachment( organisationAttachment.getAttachment() );

        return organisationAttachmentDTO;
    }

    @Override
    public OrganisationAttachment toEntity(OrganisationAttachmentDTO organisationAttachmentDTO) {
        if ( organisationAttachmentDTO == null ) {
            return null;
        }

        OrganisationAttachment organisationAttachment = new OrganisationAttachment();

        organisationAttachment.setOrganisationDocument( organisationDocumentMapper.fromId( organisationAttachmentDTO.getOrganisationDocumentId() ) );
        organisationAttachment.setOrganisationMaster( organisationMasterMapper.fromId( organisationAttachmentDTO.getOrganisationMasterId() ) );
        organisationAttachment.setId( organisationAttachmentDTO.getId() );
        organisationAttachment.setAttachment( organisationAttachmentDTO.getAttachment() );

        return organisationAttachment;
    }

    private Long organisationAttachmentOrganisationDocumentId(OrganisationAttachment organisationAttachment) {
        if ( organisationAttachment == null ) {
            return null;
        }
        OrganisationDocument organisationDocument = organisationAttachment.getOrganisationDocument();
        if ( organisationDocument == null ) {
            return null;
        }
        Long id = organisationDocument.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long organisationAttachmentOrganisationMasterId(OrganisationAttachment organisationAttachment) {
        if ( organisationAttachment == null ) {
            return null;
        }
        OrganisationMaster organisationMaster = organisationAttachment.getOrganisationMaster();
        if ( organisationMaster == null ) {
            return null;
        }
        Long id = organisationMaster.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
