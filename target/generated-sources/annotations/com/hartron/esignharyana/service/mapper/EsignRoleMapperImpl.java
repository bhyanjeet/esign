package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.EsignRole;
import com.hartron.esignharyana.service.dto.EsignRoleDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class EsignRoleMapperImpl implements EsignRoleMapper {

    @Override
    public EsignRole toEntity(EsignRoleDTO dto) {
        if ( dto == null ) {
            return null;
        }

        EsignRole esignRole = new EsignRole();

        esignRole.setId( dto.getId() );
        esignRole.seteSignRoleDetail( dto.geteSignRoleDetail() );
        esignRole.setCreatedBy( dto.getCreatedBy() );
        esignRole.setCreatedOn( dto.getCreatedOn() );
        esignRole.setLastUpdatedBy( dto.getLastUpdatedBy() );
        esignRole.setLastUpdatedOn( dto.getLastUpdatedOn() );
        esignRole.setVerifiedBy( dto.getVerifiedBy() );
        esignRole.setVerifiedOn( dto.getVerifiedOn() );
        esignRole.setRemarks( dto.getRemarks() );

        return esignRole;
    }

    @Override
    public EsignRoleDTO toDto(EsignRole entity) {
        if ( entity == null ) {
            return null;
        }

        EsignRoleDTO esignRoleDTO = new EsignRoleDTO();

        esignRoleDTO.setId( entity.getId() );
        esignRoleDTO.seteSignRoleDetail( entity.geteSignRoleDetail() );
        esignRoleDTO.setCreatedBy( entity.getCreatedBy() );
        esignRoleDTO.setCreatedOn( entity.getCreatedOn() );
        esignRoleDTO.setLastUpdatedBy( entity.getLastUpdatedBy() );
        esignRoleDTO.setLastUpdatedOn( entity.getLastUpdatedOn() );
        esignRoleDTO.setVerifiedBy( entity.getVerifiedBy() );
        esignRoleDTO.setVerifiedOn( entity.getVerifiedOn() );
        esignRoleDTO.setRemarks( entity.getRemarks() );

        return esignRoleDTO;
    }

    @Override
    public List<EsignRole> toEntity(List<EsignRoleDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<EsignRole> list = new ArrayList<EsignRole>( dtoList.size() );
        for ( EsignRoleDTO esignRoleDTO : dtoList ) {
            list.add( toEntity( esignRoleDTO ) );
        }

        return list;
    }

    @Override
    public List<EsignRoleDTO> toDto(List<EsignRole> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<EsignRoleDTO> list = new ArrayList<EsignRoleDTO>( entityList.size() );
        for ( EsignRole esignRole : entityList ) {
            list.add( toDto( esignRole ) );
        }

        return list;
    }
}
