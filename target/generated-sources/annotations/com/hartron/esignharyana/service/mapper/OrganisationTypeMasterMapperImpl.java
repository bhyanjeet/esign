package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.OrganisationTypeMaster;
import com.hartron.esignharyana.service.dto.OrganisationTypeMasterDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class OrganisationTypeMasterMapperImpl implements OrganisationTypeMasterMapper {

    @Override
    public OrganisationTypeMaster toEntity(OrganisationTypeMasterDTO dto) {
        if ( dto == null ) {
            return null;
        }

        OrganisationTypeMaster organisationTypeMaster = new OrganisationTypeMaster();

        organisationTypeMaster.setId( dto.getId() );
        organisationTypeMaster.setOrganisationTypeDetail( dto.getOrganisationTypeDetail() );
        organisationTypeMaster.setOrganisationTypeAbbreviation( dto.getOrganisationTypeAbbreviation() );
        organisationTypeMaster.setCreatedBy( dto.getCreatedBy() );
        organisationTypeMaster.setCreatedOn( dto.getCreatedOn() );
        organisationTypeMaster.setLastUpdatedBy( dto.getLastUpdatedBy() );
        organisationTypeMaster.setLastUpdatedOn( dto.getLastUpdatedOn() );
        organisationTypeMaster.setVerifiedBy( dto.getVerifiedBy() );
        organisationTypeMaster.setVerifiedOn( dto.getVerifiedOn() );
        organisationTypeMaster.setRemarks( dto.getRemarks() );

        return organisationTypeMaster;
    }

    @Override
    public OrganisationTypeMasterDTO toDto(OrganisationTypeMaster entity) {
        if ( entity == null ) {
            return null;
        }

        OrganisationTypeMasterDTO organisationTypeMasterDTO = new OrganisationTypeMasterDTO();

        organisationTypeMasterDTO.setId( entity.getId() );
        organisationTypeMasterDTO.setOrganisationTypeDetail( entity.getOrganisationTypeDetail() );
        organisationTypeMasterDTO.setOrganisationTypeAbbreviation( entity.getOrganisationTypeAbbreviation() );
        organisationTypeMasterDTO.setCreatedBy( entity.getCreatedBy() );
        organisationTypeMasterDTO.setCreatedOn( entity.getCreatedOn() );
        organisationTypeMasterDTO.setLastUpdatedBy( entity.getLastUpdatedBy() );
        organisationTypeMasterDTO.setLastUpdatedOn( entity.getLastUpdatedOn() );
        organisationTypeMasterDTO.setVerifiedBy( entity.getVerifiedBy() );
        organisationTypeMasterDTO.setVerifiedOn( entity.getVerifiedOn() );
        organisationTypeMasterDTO.setRemarks( entity.getRemarks() );

        return organisationTypeMasterDTO;
    }

    @Override
    public List<OrganisationTypeMaster> toEntity(List<OrganisationTypeMasterDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<OrganisationTypeMaster> list = new ArrayList<OrganisationTypeMaster>( dtoList.size() );
        for ( OrganisationTypeMasterDTO organisationTypeMasterDTO : dtoList ) {
            list.add( toEntity( organisationTypeMasterDTO ) );
        }

        return list;
    }

    @Override
    public List<OrganisationTypeMasterDTO> toDto(List<OrganisationTypeMaster> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<OrganisationTypeMasterDTO> list = new ArrayList<OrganisationTypeMasterDTO>( entityList.size() );
        for ( OrganisationTypeMaster organisationTypeMaster : entityList ) {
            list.add( toDto( organisationTypeMaster ) );
        }

        return list;
    }
}
