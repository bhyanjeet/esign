package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.ExternalUserEsignRequest;
import com.hartron.esignharyana.service.dto.ExternalUserEsignRequestDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class ExternalUserEsignRequestMapperImpl implements ExternalUserEsignRequestMapper {

    @Override
    public ExternalUserEsignRequest toEntity(ExternalUserEsignRequestDTO dto) {
        if ( dto == null ) {
            return null;
        }

        ExternalUserEsignRequest externalUserEsignRequest = new ExternalUserEsignRequest();

        externalUserEsignRequest.setTxn( dto.getTxn() );
        externalUserEsignRequest.setTxnRef( dto.getTxnRef() );
        externalUserEsignRequest.setId( dto.getId() );
        externalUserEsignRequest.setUserCodeId( dto.getUserCodeId() );
        externalUserEsignRequest.setApplicationIdCode( dto.getApplicationIdCode() );
        externalUserEsignRequest.setResponseUrl( dto.getResponseUrl() );
        externalUserEsignRequest.setRedirectUrl( dto.getRedirectUrl() );
        externalUserEsignRequest.setTs( dto.getTs() );
        externalUserEsignRequest.setSignerId( dto.getSignerId() );
        externalUserEsignRequest.setDocInfo( dto.getDocInfo() );
        externalUserEsignRequest.setDocUrl( dto.getDocUrl() );
        externalUserEsignRequest.setDocHash( dto.getDocHash() );
        externalUserEsignRequest.setRequestStatus( dto.getRequestStatus() );
        externalUserEsignRequest.setRequestTime( dto.getRequestTime() );

        return externalUserEsignRequest;
    }

    @Override
    public ExternalUserEsignRequestDTO toDto(ExternalUserEsignRequest entity) {
        if ( entity == null ) {
            return null;
        }

        ExternalUserEsignRequestDTO externalUserEsignRequestDTO = new ExternalUserEsignRequestDTO();

        externalUserEsignRequestDTO.setTxn( entity.getTxn() );
        externalUserEsignRequestDTO.setTxnRef( entity.getTxnRef() );
        externalUserEsignRequestDTO.setId( entity.getId() );
        externalUserEsignRequestDTO.setUserCodeId( entity.getUserCodeId() );
        externalUserEsignRequestDTO.setApplicationIdCode( entity.getApplicationIdCode() );
        externalUserEsignRequestDTO.setResponseUrl( entity.getResponseUrl() );
        externalUserEsignRequestDTO.setRedirectUrl( entity.getRedirectUrl() );
        externalUserEsignRequestDTO.setTs( entity.getTs() );
        externalUserEsignRequestDTO.setSignerId( entity.getSignerId() );
        externalUserEsignRequestDTO.setDocInfo( entity.getDocInfo() );
        externalUserEsignRequestDTO.setDocUrl( entity.getDocUrl() );
        externalUserEsignRequestDTO.setDocHash( entity.getDocHash() );
        externalUserEsignRequestDTO.setRequestStatus( entity.getRequestStatus() );
        externalUserEsignRequestDTO.setRequestTime( entity.getRequestTime() );

        return externalUserEsignRequestDTO;
    }

    @Override
    public List<ExternalUserEsignRequest> toEntity(List<ExternalUserEsignRequestDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<ExternalUserEsignRequest> list = new ArrayList<ExternalUserEsignRequest>( dtoList.size() );
        for ( ExternalUserEsignRequestDTO externalUserEsignRequestDTO : dtoList ) {
            list.add( toEntity( externalUserEsignRequestDTO ) );
        }

        return list;
    }

    @Override
    public List<ExternalUserEsignRequestDTO> toDto(List<ExternalUserEsignRequest> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ExternalUserEsignRequestDTO> list = new ArrayList<ExternalUserEsignRequestDTO>( entityList.size() );
        for ( ExternalUserEsignRequest externalUserEsignRequest : entityList ) {
            list.add( toDto( externalUserEsignRequest ) );
        }

        return list;
    }
}
