package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.ApplicationMaster;
import com.hartron.esignharyana.domain.DesignationMaster;
import com.hartron.esignharyana.domain.EsignRole;
import com.hartron.esignharyana.domain.OrganisationMaster;
import com.hartron.esignharyana.domain.UserDetails;
import com.hartron.esignharyana.service.dto.UserDetailsDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class UserDetailsMapperImpl implements UserDetailsMapper {

    @Autowired
    private DesignationMasterMapper designationMasterMapper;
    @Autowired
    private OrganisationMasterMapper organisationMasterMapper;
    @Autowired
    private EsignRoleMapper esignRoleMapper;
    @Autowired
    private ApplicationMasterMapper applicationMasterMapper;

    @Override
    public List<UserDetails> toEntity(List<UserDetailsDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<UserDetails> list = new ArrayList<UserDetails>( dtoList.size() );
        for ( UserDetailsDTO userDetailsDTO : dtoList ) {
            list.add( toEntity( userDetailsDTO ) );
        }

        return list;
    }

    @Override
    public List<UserDetailsDTO> toDto(List<UserDetails> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<UserDetailsDTO> list = new ArrayList<UserDetailsDTO>( entityList.size() );
        for ( UserDetails userDetails : entityList ) {
            list.add( toDto( userDetails ) );
        }

        return list;
    }

    @Override
    public UserDetailsDTO toDto(UserDetails userDetails) {
        if ( userDetails == null ) {
            return null;
        }

        UserDetailsDTO userDetailsDTO = new UserDetailsDTO();

        userDetailsDTO.setApplicationMasterId( userDetailsApplicationMasterId( userDetails ) );
        userDetailsDTO.setOrganisationMasterId( userDetailsOrganisationMasterId( userDetails ) );
        userDetailsDTO.setDesignationMasterId( userDetailsDesignationMasterId( userDetails ) );
        userDetailsDTO.setEsignRoleId( userDetailsEsignRoleId( userDetails ) );
        userDetailsDTO.setAccountStatus( userDetails.getAccountStatus() );
        userDetailsDTO.setSignerId( userDetails.getSignerId() );
        userDetailsDTO.setLastAction( userDetails.getLastAction() );
        userDetailsDTO.setEmployeeId( userDetails.getEmployeeId() );
        userDetailsDTO.setEmployeeCardAttachment( userDetails.getEmployeeCardAttachment() );
        userDetailsDTO.setId( userDetails.getId() );
        userDetailsDTO.setUserFullName( userDetails.getUserFullName() );
        userDetailsDTO.setUserPhoneMobile( userDetails.getUserPhoneMobile() );
        userDetailsDTO.setUserPhoneLandline( userDetails.getUserPhoneLandline() );
        userDetailsDTO.setUserOfficialEmail( userDetails.getUserOfficialEmail() );
        userDetailsDTO.setUserAlternativeEmail( userDetails.getUserAlternativeEmail() );
        userDetailsDTO.setUserLoginPassword( userDetails.getUserLoginPassword() );
        userDetailsDTO.setUserLoginPasswordSalt( userDetails.getUserLoginPasswordSalt() );
        userDetailsDTO.setUserTransactionPassword( userDetails.getUserTransactionPassword() );
        userDetailsDTO.setUserTransactionPasswordSalt( userDetails.getUserTransactionPasswordSalt() );
        userDetailsDTO.setCreatedBy( userDetails.getCreatedBy() );
        userDetailsDTO.setCreatedOn( userDetails.getCreatedOn() );
        userDetailsDTO.setLastUpdatedBy( userDetails.getLastUpdatedBy() );
        userDetailsDTO.setLastUpdatedOn( userDetails.getLastUpdatedOn() );
        userDetailsDTO.setVerifiedBy( userDetails.getVerifiedBy() );
        userDetailsDTO.setVerifiedOn( userDetails.getVerifiedOn() );
        userDetailsDTO.setRemarks( userDetails.getRemarks() );
        userDetailsDTO.setUserIdCode( userDetails.getUserIdCode() );
        userDetailsDTO.setStatus( userDetails.getStatus() );
        userDetailsDTO.setUsername( userDetails.getUsername() );
        userDetailsDTO.setUserPIN( userDetails.getUserPIN() );
        userDetailsDTO.setOrganisationUnit( userDetails.getOrganisationUnit() );
        userDetailsDTO.setAddress( userDetails.getAddress() );
        userDetailsDTO.setCounrty( userDetails.getCounrty() );
        userDetailsDTO.setPostalCode( userDetails.getPostalCode() );
        userDetailsDTO.setPhotograph( userDetails.getPhotograph() );
        userDetailsDTO.setDob( userDetails.getDob() );
        userDetailsDTO.setGender( userDetails.getGender() );
        userDetailsDTO.setPan( userDetails.getPan() );
        userDetailsDTO.setAadhaar( userDetails.getAadhaar() );
        userDetailsDTO.setPanAttachment( userDetails.getPanAttachment() );

        return userDetailsDTO;
    }

    @Override
    public UserDetails toEntity(UserDetailsDTO userDetailsDTO) {
        if ( userDetailsDTO == null ) {
            return null;
        }

        UserDetails userDetails = new UserDetails();

        userDetails.setApplicationMaster( applicationMasterMapper.fromId( userDetailsDTO.getApplicationMasterId() ) );
        userDetails.setEsignRole( esignRoleMapper.fromId( userDetailsDTO.getEsignRoleId() ) );
        userDetails.setDesignationMaster( designationMasterMapper.fromId( userDetailsDTO.getDesignationMasterId() ) );
        userDetails.setOrganisationMaster( organisationMasterMapper.fromId( userDetailsDTO.getOrganisationMasterId() ) );
        userDetails.setId( userDetailsDTO.getId() );
        userDetails.setUserFullName( userDetailsDTO.getUserFullName() );
        userDetails.setUserPhoneMobile( userDetailsDTO.getUserPhoneMobile() );
        userDetails.setUserPhoneLandline( userDetailsDTO.getUserPhoneLandline() );
        userDetails.setUserOfficialEmail( userDetailsDTO.getUserOfficialEmail() );
        userDetails.setUserAlternativeEmail( userDetailsDTO.getUserAlternativeEmail() );
        userDetails.setUserLoginPassword( userDetailsDTO.getUserLoginPassword() );
        userDetails.setUserLoginPasswordSalt( userDetailsDTO.getUserLoginPasswordSalt() );
        userDetails.setUserTransactionPassword( userDetailsDTO.getUserTransactionPassword() );
        userDetails.setUserTransactionPasswordSalt( userDetailsDTO.getUserTransactionPasswordSalt() );
        userDetails.setCreatedBy( userDetailsDTO.getCreatedBy() );
        userDetails.setCreatedOn( userDetailsDTO.getCreatedOn() );
        userDetails.setLastUpdatedBy( userDetailsDTO.getLastUpdatedBy() );
        userDetails.setLastUpdatedOn( userDetailsDTO.getLastUpdatedOn() );
        userDetails.setVerifiedBy( userDetailsDTO.getVerifiedBy() );
        userDetails.setVerifiedOn( userDetailsDTO.getVerifiedOn() );
        userDetails.setRemarks( userDetailsDTO.getRemarks() );
        userDetails.setUserIdCode( userDetailsDTO.getUserIdCode() );
        userDetails.setStatus( userDetailsDTO.getStatus() );
        userDetails.setUsername( userDetailsDTO.getUsername() );
        userDetails.setUserPIN( userDetailsDTO.getUserPIN() );
        userDetails.setOrganisationUnit( userDetailsDTO.getOrganisationUnit() );
        userDetails.setAddress( userDetailsDTO.getAddress() );
        userDetails.setCounrty( userDetailsDTO.getCounrty() );
        userDetails.setPostalCode( userDetailsDTO.getPostalCode() );
        userDetails.setPhotograph( userDetailsDTO.getPhotograph() );
        userDetails.setDob( userDetailsDTO.getDob() );
        userDetails.setGender( userDetailsDTO.getGender() );
        userDetails.setPan( userDetailsDTO.getPan() );
        userDetails.setAadhaar( userDetailsDTO.getAadhaar() );
        userDetails.setPanAttachment( userDetailsDTO.getPanAttachment() );
        userDetails.setEmployeeId( userDetailsDTO.getEmployeeId() );
        userDetails.setEmployeeCardAttachment( userDetailsDTO.getEmployeeCardAttachment() );
        userDetails.setSignerId( userDetailsDTO.getSignerId() );
        userDetails.setLastAction( userDetailsDTO.getLastAction() );
        userDetails.setAccountStatus( userDetailsDTO.getAccountStatus() );

        return userDetails;
    }

    private Long userDetailsApplicationMasterId(UserDetails userDetails) {
        if ( userDetails == null ) {
            return null;
        }
        ApplicationMaster applicationMaster = userDetails.getApplicationMaster();
        if ( applicationMaster == null ) {
            return null;
        }
        Long id = applicationMaster.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long userDetailsOrganisationMasterId(UserDetails userDetails) {
        if ( userDetails == null ) {
            return null;
        }
        OrganisationMaster organisationMaster = userDetails.getOrganisationMaster();
        if ( organisationMaster == null ) {
            return null;
        }
        Long id = organisationMaster.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long userDetailsDesignationMasterId(UserDetails userDetails) {
        if ( userDetails == null ) {
            return null;
        }
        DesignationMaster designationMaster = userDetails.getDesignationMaster();
        if ( designationMaster == null ) {
            return null;
        }
        Long id = designationMaster.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long userDetailsEsignRoleId(UserDetails userDetails) {
        if ( userDetails == null ) {
            return null;
        }
        EsignRole esignRole = userDetails.getEsignRole();
        if ( esignRole == null ) {
            return null;
        }
        Long id = esignRole.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
