package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.BlockMaster;
import com.hartron.esignharyana.domain.DistrictMaster;
import com.hartron.esignharyana.domain.StateMaster;
import com.hartron.esignharyana.service.dto.BlockMasterDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class BlockMasterMapperImpl implements BlockMasterMapper {

    @Autowired
    private StateMasterMapper stateMasterMapper;
    @Autowired
    private DistrictMasterMapper districtMasterMapper;

    @Override
    public List<BlockMaster> toEntity(List<BlockMasterDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<BlockMaster> list = new ArrayList<BlockMaster>( dtoList.size() );
        for ( BlockMasterDTO blockMasterDTO : dtoList ) {
            list.add( toEntity( blockMasterDTO ) );
        }

        return list;
    }

    @Override
    public List<BlockMasterDTO> toDto(List<BlockMaster> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<BlockMasterDTO> list = new ArrayList<BlockMasterDTO>( entityList.size() );
        for ( BlockMaster blockMaster : entityList ) {
            list.add( toDto( blockMaster ) );
        }

        return list;
    }

    @Override
    public BlockMasterDTO toDto(BlockMaster blockMaster) {
        if ( blockMaster == null ) {
            return null;
        }

        BlockMasterDTO blockMasterDTO = new BlockMasterDTO();

        blockMasterDTO.setDistrictMasterId( blockMasterDistrictMasterId( blockMaster ) );
        blockMasterDTO.setStateMasterId( blockMasterStateMasterId( blockMaster ) );
        blockMasterDTO.setId( blockMaster.getId() );
        blockMasterDTO.setBlockCode( blockMaster.getBlockCode() );
        blockMasterDTO.setBlockName( blockMaster.getBlockName() );
        blockMasterDTO.setCreatedOn( blockMaster.getCreatedOn() );
        blockMasterDTO.setLastUpdatedBy( blockMaster.getLastUpdatedBy() );
        blockMasterDTO.setLastUpdatedOn( blockMaster.getLastUpdatedOn() );
        blockMasterDTO.setVerifiedBy( blockMaster.getVerifiedBy() );
        blockMasterDTO.setVerifiedOn( blockMaster.getVerifiedOn() );
        blockMasterDTO.setRemarks( blockMaster.getRemarks() );

        return blockMasterDTO;
    }

    @Override
    public BlockMaster toEntity(BlockMasterDTO blockMasterDTO) {
        if ( blockMasterDTO == null ) {
            return null;
        }

        BlockMaster blockMaster = new BlockMaster();

        blockMaster.setDistrictMaster( districtMasterMapper.fromId( blockMasterDTO.getDistrictMasterId() ) );
        blockMaster.setStateMaster( stateMasterMapper.fromId( blockMasterDTO.getStateMasterId() ) );
        blockMaster.setId( blockMasterDTO.getId() );
        blockMaster.setBlockCode( blockMasterDTO.getBlockCode() );
        blockMaster.setBlockName( blockMasterDTO.getBlockName() );
        blockMaster.setCreatedOn( blockMasterDTO.getCreatedOn() );
        blockMaster.setLastUpdatedBy( blockMasterDTO.getLastUpdatedBy() );
        blockMaster.setLastUpdatedOn( blockMasterDTO.getLastUpdatedOn() );
        blockMaster.setVerifiedBy( blockMasterDTO.getVerifiedBy() );
        blockMaster.setVerifiedOn( blockMasterDTO.getVerifiedOn() );
        blockMaster.setRemarks( blockMasterDTO.getRemarks() );

        return blockMaster;
    }

    private Long blockMasterDistrictMasterId(BlockMaster blockMaster) {
        if ( blockMaster == null ) {
            return null;
        }
        DistrictMaster districtMaster = blockMaster.getDistrictMaster();
        if ( districtMaster == null ) {
            return null;
        }
        Long id = districtMaster.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long blockMasterStateMasterId(BlockMaster blockMaster) {
        if ( blockMaster == null ) {
            return null;
        }
        StateMaster stateMaster = blockMaster.getStateMaster();
        if ( stateMaster == null ) {
            return null;
        }
        Long id = stateMaster.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
