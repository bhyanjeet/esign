package com.hartron.esignharyana.service.mapper;

import com.hartron.esignharyana.domain.OrganisationDocument;
import com.hartron.esignharyana.domain.OrganisationTypeMaster;
import com.hartron.esignharyana.service.dto.OrganisationDocumentDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-04-20T18:42:27+0530",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class OrganisationDocumentMapperImpl implements OrganisationDocumentMapper {

    @Autowired
    private OrganisationTypeMasterMapper organisationTypeMasterMapper;

    @Override
    public List<OrganisationDocument> toEntity(List<OrganisationDocumentDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<OrganisationDocument> list = new ArrayList<OrganisationDocument>( dtoList.size() );
        for ( OrganisationDocumentDTO organisationDocumentDTO : dtoList ) {
            list.add( toEntity( organisationDocumentDTO ) );
        }

        return list;
    }

    @Override
    public List<OrganisationDocumentDTO> toDto(List<OrganisationDocument> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<OrganisationDocumentDTO> list = new ArrayList<OrganisationDocumentDTO>( entityList.size() );
        for ( OrganisationDocument organisationDocument : entityList ) {
            list.add( toDto( organisationDocument ) );
        }

        return list;
    }

    @Override
    public OrganisationDocumentDTO toDto(OrganisationDocument organisationDocument) {
        if ( organisationDocument == null ) {
            return null;
        }

        OrganisationDocumentDTO organisationDocumentDTO = new OrganisationDocumentDTO();

        organisationDocumentDTO.setOrganisationTypeMasterId( organisationDocumentOrganisationTypeMasterId( organisationDocument ) );
        organisationDocumentDTO.setId( organisationDocument.getId() );
        organisationDocumentDTO.setDocumentTitle( organisationDocument.getDocumentTitle() );
        organisationDocumentDTO.setCreatedBy( organisationDocument.getCreatedBy() );
        organisationDocumentDTO.setCreatedOn( organisationDocument.getCreatedOn() );
        organisationDocumentDTO.setVerifiedBy( organisationDocument.getVerifiedBy() );
        organisationDocumentDTO.setVerifiedOn( organisationDocument.getVerifiedOn() );
        organisationDocumentDTO.setRemarks( organisationDocument.getRemarks() );
        organisationDocumentDTO.setDocumentRelatedTo( organisationDocument.getDocumentRelatedTo() );

        return organisationDocumentDTO;
    }

    @Override
    public OrganisationDocument toEntity(OrganisationDocumentDTO organisationDocumentDTO) {
        if ( organisationDocumentDTO == null ) {
            return null;
        }

        OrganisationDocument organisationDocument = new OrganisationDocument();

        organisationDocument.setOrganisationTypeMaster( organisationTypeMasterMapper.fromId( organisationDocumentDTO.getOrganisationTypeMasterId() ) );
        organisationDocument.setId( organisationDocumentDTO.getId() );
        organisationDocument.setDocumentTitle( organisationDocumentDTO.getDocumentTitle() );
        organisationDocument.setCreatedBy( organisationDocumentDTO.getCreatedBy() );
        organisationDocument.setCreatedOn( organisationDocumentDTO.getCreatedOn() );
        organisationDocument.setVerifiedBy( organisationDocumentDTO.getVerifiedBy() );
        organisationDocument.setVerifiedOn( organisationDocumentDTO.getVerifiedOn() );
        organisationDocument.setRemarks( organisationDocumentDTO.getRemarks() );
        organisationDocument.setDocumentRelatedTo( organisationDocumentDTO.getDocumentRelatedTo() );

        return organisationDocument;
    }

    private Long organisationDocumentOrganisationTypeMasterId(OrganisationDocument organisationDocument) {
        if ( organisationDocument == null ) {
            return null;
        }
        OrganisationTypeMaster organisationTypeMaster = organisationDocument.getOrganisationTypeMaster();
        if ( organisationTypeMaster == null ) {
            return null;
        }
        Long id = organisationTypeMaster.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
